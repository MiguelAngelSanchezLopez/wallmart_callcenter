﻿Imports CapaNegocio
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports System.Xml

' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://ws.altotrack.com/TransporteWalmartMX/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service
  Inherits System.Web.Services.WebService

  'variable para setear cuando se esten haciendo modificaciones al webservices
  Private Const MODO_DEBUG As Boolean = False

#Region "Tipos definidos por usuario públicos"

  Public oAutentificacion As Autentificacion

#End Region

#Region "Métodos públicos"
  <WebMethod(Description:="Ingreso de los datos de la alarma"), SoapHeader("oAutentificacion", Direction:=SoapHeaderDirection.In)> _
  Public Function Alertas(ByVal NroTransporte As String, ByVal IdEmbarque As String, ByVal OrigenCodigo As String, ByVal PatenteTracto As String, _
                          ByVal PatenteTrailer As String, ByVal RutTransportista As String, ByVal NombreTransportista As String, _
                          ByVal RutConductor As String, ByVal NombreConductor As String, ByVal FechaUltimaTransmision As String, _
                          ByVal DescripcionAlerta As String, ByVal FechaInicioAlerta As String, ByVal LatTrailer As String, ByVal LonTrailer As String, _
                          ByVal LatTracto As String, ByVal LonTracto As String, ByVal TipoAlerta As String, _
                          ByVal Permiso As String, ByVal Criticidad As String, ByVal TipoAlertaDescripcion As String, ByVal AlertaMapa As String, _
                          ByVal NombreZona As String, ByVal Velocidad As String, ByVal EstadoGPSTracto As String, ByVal EstadoGPSRampla As String, _
                          ByVal EstadoGPS As String, ByVal LocalDestino As String, ByVal TipoPunto As String, ByVal Temp1 As String, _
                          ByVal Temp2 As String, ByVal DistanciaTT As String, ByVal TransportistaTrailer As String, ByVal CantidadSatelites As String, _
                          ByVal Ocurrencia As String, ByVal GrupoAlerta As String) As TipoDato.wsdAlerta

    Dim owsAlerta As New TipoDato.wsdAlerta
    Dim oRespuesta As New TipoDato._Respuesta

    Try
      'inicializa objeto
      owsAlerta.Respuesta = oRespuesta

      If MODO_DEBUG OrElse Me.EsAutentificacionValidaSoap() Then

        owsAlerta = CargarDatosAlerta(NroTransporte, IdEmbarque, OrigenCodigo, PatenteTracto, PatenteTrailer, RutTransportista, NombreTransportista, RutConductor, NombreConductor, _
                                      FechaUltimaTransmision, DescripcionAlerta, FechaInicioAlerta, LatTrailer, LonTrailer, LatTracto, LonTracto, TipoAlerta, Permiso, _
                                      Criticidad, TipoAlertaDescripcion, AlertaMapa, NombreZona, Velocidad, EstadoGPSTracto, EstadoGPSRampla, EstadoGPS, LocalDestino, _
                                      TipoPunto, Temp1, Temp2, DistanciaTT, TransportistaTrailer, CantidadSatelites, Ocurrencia, GrupoAlerta)
      Else
        owsAlerta.Respuesta.Codigo = Respuesta.eCodigo.AutentificacionNoValida
        owsAlerta.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.AutentificacionNoValida)
      End If
    Catch ex As Exception
      owsAlerta.Respuesta.Codigo = Respuesta.eCodigo.Error
      owsAlerta.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.Error, ex.Message & " / StackTrace: " & ex.StackTrace & " / Conexion: " & Conexion.StringConexion())
    End Try

    Return owsAlerta
  End Function

  <WebMethod(Description:="Ingreso de los datos de la traza del viaje"), SoapHeader("oAutentificacion", Direction:=SoapHeaderDirection.In)> _
  Public Function TrazaViaje(ByVal NroTransporte As String, ByVal IdEmbarque As String, ByVal Operacion As String, ByVal PatenteTracto As String, ByVal PatenteTrailer As String, ByVal RutTransportista As String, _
                             ByVal NombreTransportista As String, ByVal RutConductor As String, ByVal NombreConductor As String, ByVal OrigenCodigo As String, ByVal OrigenDescripcion As String, _
                             ByVal FHSalidaOrigen As String, ByVal LocalDestino As String, ByVal DestinoDescripcion As String, ByVal FHLlegadaDestino As String, _
                             ByVal FHAperturaPuerta As String, ByVal FHSalidaDestino As String, ByVal TipoViaje As String, ByVal ProveedorGPS As String, _
                             ByVal DestinoDomicilio As String, ByVal DestinoRegion As String, ByVal DestinoDescripcionRegion As String, _
                             ByVal DestinoComuna As String, ByVal DestinoLat As String, ByVal DestinoLon As String, ByVal EstadoViaje As String, _
                             ByVal SecuenciaDestino As String, ByVal RamplaProveedorGPS As String, ByVal EstadoPuerta As String, _
                             ByVal EstadoLat As String, ByVal EstadoLon As String, ByVal TempSalidaCD As String) As TipoDato.wsdTrazaViaje

    Dim owsTrazaViaje As New TipoDato.wsdTrazaViaje
    Dim oRespuesta As New TipoDato._Respuesta

    Try
      'inicializa objeto
      owsTrazaViaje.Respuesta = oRespuesta

      If MODO_DEBUG OrElse Me.EsAutentificacionValidaSoap() Then
        owsTrazaViaje = CargarDatosTrazaViaje(NroTransporte, IdEmbarque, Operacion, PatenteTracto, PatenteTrailer, RutTransportista, NombreTransportista, RutConductor, _
                                              NombreConductor, OrigenCodigo, OrigenDescripcion, FHSalidaOrigen, LocalDestino, DestinoDescripcion, FHLlegadaDestino, _
                                              FHAperturaPuerta, FHSalidaDestino, TipoViaje, ProveedorGPS, DestinoDomicilio, DestinoRegion, DestinoDescripcionRegion, _
                                              DestinoComuna, DestinoLat, DestinoLon, EstadoViaje, SecuenciaDestino, RamplaProveedorGPS, EstadoPuerta, EstadoLat, _
                                              EstadoLon, TempSalidaCD)
      Else
        owsTrazaViaje.Respuesta.Codigo = Respuesta.eCodigo.AutentificacionNoValida
        owsTrazaViaje.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.AutentificacionNoValida)
      End If
    Catch ex As Exception
      owsTrazaViaje.Respuesta.Codigo = Respuesta.eCodigo.Error
      owsTrazaViaje.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.Error, ex.Message)
    End Try

    Return owsTrazaViaje
  End Function

  <WebMethod(Description:="Ingreso de los datos de perdidas de señal"), SoapHeader("oAutentificacion", Direction:=SoapHeaderDirection.In)> _
  Public Function LogPerdidaSenal(ByVal NroTransporte As String, ByVal PatenteTracto As String, ByVal PatenteTrailer As String, ByVal LocalDestino As String, _
                                  ByVal FechaHoraInicio As String, ByVal FechaHoraTermino As String) As TipoDato.wsdLogPerdidaSenal

    Dim owsdLogPerdidaSenal As New TipoDato.wsdLogPerdidaSenal
    Dim oRespuesta As New TipoDato._Respuesta

    Try
      'inicializa objeto
      owsdLogPerdidaSenal.Respuesta = oRespuesta

      If MODO_DEBUG OrElse Me.EsAutentificacionValidaSoap() Then
        owsdLogPerdidaSenal = CargarDatosLogPerdidaSenal(NroTransporte, PatenteTracto, PatenteTrailer, LocalDestino, FechaHoraInicio, FechaHoraTermino)
      Else
        owsdLogPerdidaSenal.Respuesta.Codigo = Respuesta.eCodigo.AutentificacionNoValida
        owsdLogPerdidaSenal.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.AutentificacionNoValida)
      End If
    Catch ex As Exception
      owsdLogPerdidaSenal.Respuesta.Codigo = Respuesta.eCodigo.Error
      owsdLogPerdidaSenal.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.Error, ex.Message)
    End Try

    Return owsdLogPerdidaSenal
  End Function

#End Region

#Region "Métodos privados"

  ''' <summary>
  ''' determina si la autentificacion es valida para el ws
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ''' 
  Private Function EsAutentificacionValidaSoap() As Boolean
    Dim esValida As Boolean
    If oAutentificacion Is Nothing OrElse Not oAutentificacion.EsValidoAutentificacion() Then
      esValida = False
    Else
      esValida = True
    End If
    Return esValida
  End Function

  ''' <summary>
  ''' Graba las alertas enviadas para SMU
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CargarDatosAlerta(ByVal NroTransporte As String, ByVal IdEmbarque As String, ByVal OrigenCodigo As String, ByVal PatenteTracto As String, _
                                    ByVal PatenteTrailer As String, ByVal RutTransportista As String, ByVal NombreTransportista As String, _
                                    ByVal RutConductor As String, ByVal NombreConductor As String, ByVal FechaUltimaTransmision As String, _
                                    ByVal DescripcionAlerta As String, ByVal FechaInicioAlerta As String, ByVal LatTrailer As String, ByVal LonTrailer As String, _
                                    ByVal LatTracto As String, ByVal LonTracto As String, ByVal TipoAlerta As String, _
                                    ByVal Permiso As String, ByVal Criticidad As String, ByVal TipoAlertaDescripcion As String, ByVal AlertaMapa As String, _
                                    ByVal NombreZona As String, ByVal Velocidad As String, ByVal EstadoGPSTracto As String, ByVal EstadoGPSRampla As String, _
                                    ByVal EstadoGPS As String, ByVal LocalDestino As String, ByVal TipoPunto As String, ByVal Temp1 As String, _
                                    ByVal Temp2 As String, ByVal DistanciaTT As String, ByVal TransportistaTrailer As String, ByVal CantidadSatelites As String, _
                                    ByVal Ocurrencia As String, ByVal GrupoAlerta As String) As TipoDato.wsdAlerta

    Dim owsAlerta As New TipoDato.wsdAlerta
    Dim oRespuesta As New TipoDato._Respuesta
    Dim dateFechaUltimaTransmision, dateFechaInicioAlerta As DateTime
    Dim pathXml As String
    Dim xmlDoc As XmlDocument

    Try
      If MODO_DEBUG Then
        ''*** USAR ESTE METODOS PARA USAR DATOS DE PRUEBA ***
        Herramientas.DatosPruebaAlerta(NroTransporte, IdEmbarque, OrigenCodigo, PatenteTracto, PatenteTrailer, RutTransportista, NombreTransportista, RutConductor, NombreConductor, _
                                      FechaUltimaTransmision, DescripcionAlerta, FechaInicioAlerta, LatTrailer, LonTrailer, LatTracto, LonTracto, TipoAlerta, Permiso, _
                                      Criticidad, TipoAlertaDescripcion, AlertaMapa, NombreZona, Velocidad, EstadoGPSTracto, EstadoGPSRampla, EstadoGPS, LocalDestino, _
                                      TipoPunto, Temp1, Temp2, DistanciaTT, TransportistaTrailer, CantidadSatelites, Ocurrencia, GrupoAlerta)
        '***---------------------------------------------------------
      End If

      'inicializa objeto
      owsAlerta.Respuesta = oRespuesta

      'formatea valores
      NroTransporte = IIf(String.IsNullOrEmpty(NroTransporte), "-1", NroTransporte)
      IdEmbarque = IIf(String.IsNullOrEmpty(IdEmbarque), "-1", IdEmbarque)
      OrigenCodigo = IIf(String.IsNullOrEmpty(OrigenCodigo), "-1", OrigenCodigo)
      LocalDestino = IIf(String.IsNullOrEmpty(LocalDestino), "-1", LocalDestino)
      CantidadSatelites = IIf(String.IsNullOrEmpty(CantidadSatelites), "-1", CantidadSatelites)
      Ocurrencia = IIf(String.IsNullOrEmpty(Ocurrencia), "-1", Ocurrencia)
      PatenteTracto = Herramientas.FormatearPatente(PatenteTracto)
      PatenteTrailer = Herramientas.FormatearPatente(PatenteTrailer)

      If (String.IsNullOrEmpty(FechaUltimaTransmision)) Then
        dateFechaUltimaTransmision = Herramientas.MyNow()
      Else
        dateFechaUltimaTransmision = Convert.ToDateTime(FechaUltimaTransmision)
      End If

      If (String.IsNullOrEmpty(FechaInicioAlerta)) Then
        dateFechaInicioAlerta = Herramientas.MyNow()
      Else
        dateFechaInicioAlerta = Convert.ToDateTime(FechaInicioAlerta)
      End If

      '---------------------------------------------------------------------
      ' carga los datos en Alerta
      '---------------------------------------------------------------------
      Dim oAlertaWebservice As New AlertaWebservice()
      oAlertaWebservice.NroTransporte = NroTransporte
      oAlertaWebservice.IdEmbarque = IdEmbarque
      oAlertaWebservice.DescripcionAlerta = DescripcionAlerta
      oAlertaWebservice.LocalDestino = LocalDestino
      oAlertaWebservice.OrigenCodigo = OrigenCodigo
      oAlertaWebservice.PatenteTracto = PatenteTracto
      oAlertaWebservice.PatenteTrailer = PatenteTrailer
      oAlertaWebservice.RutTransportista = RutTransportista
      oAlertaWebservice.NombreTransportista = NombreTransportista
      oAlertaWebservice.RutConductor = RutConductor
      oAlertaWebservice.NombreConductor = NombreConductor
      oAlertaWebservice.FechaUltimaTransmision = dateFechaUltimaTransmision
      oAlertaWebservice.FechaInicioAlerta = dateFechaInicioAlerta
      oAlertaWebservice.LatTrailer = LatTrailer
      oAlertaWebservice.LonTrailer = LonTrailer
      oAlertaWebservice.LatTracto = LatTracto
      oAlertaWebservice.LonTracto = LonTracto
      oAlertaWebservice.TipoAlerta = TipoAlerta
      oAlertaWebservice.Permiso = Permiso
      oAlertaWebservice.Criticidad = Criticidad
      oAlertaWebservice.TipoAlertaDescripcion = TipoAlertaDescripcion
      oAlertaWebservice.AlertaMapa = AlertaMapa
      oAlertaWebservice.NombreZona = NombreZona
      oAlertaWebservice.Velocidad = Velocidad
      oAlertaWebservice.EstadoGPSTracto = EstadoGPSTracto
      oAlertaWebservice.EstadoGPSRampla = EstadoGPSRampla
      oAlertaWebservice.EstadoGPS = EstadoGPS
      oAlertaWebservice.TipoPunto = TipoPunto
      oAlertaWebservice.Temp1 = Temp1
      oAlertaWebservice.Temp2 = Temp2
      oAlertaWebservice.DistanciaTT = DistanciaTT
      oAlertaWebservice.TransportistaTrailer = TransportistaTrailer
      oAlertaWebservice.CantidadSatelites = CantidadSatelites
      oAlertaWebservice.FechaHoraCreacion = Herramientas.MyNow()
      oAlertaWebservice.FechaHoraActualizacion = Herramientas.MyNow()
      oAlertaWebservice.EnvioCorreo = False
      oAlertaWebservice.FueraDeHorario = False
      oAlertaWebservice.Activo = True
      oAlertaWebservice.ConLog = False
      oAlertaWebservice.Ocurrencia = Ocurrencia
      oAlertaWebservice.GrupoAlerta = GrupoAlerta

      '---------------------------------------------------------------
      'obtiene datos especiales de la alerta para realizar la gestion
      oAlertaWebservice.ObtenerDatosEspeciales()

      '---------------------------------------------------------------
      'graba los datos de la alerta
      oAlertaWebservice.GuardarNuevo()

      '---------------------------------------------------------------
      'si es la primera ocurrencia entonces se asigan como padre asi mismo
      If (oAlertaWebservice.IdAlertaPadre = -1) Then oAlertaWebservice.IdAlertaPadre = oAlertaWebservice.Id

      'si el viaje ya llego al local entonces se traspasa la alerta directo a la tabla AlertaHistoria
      If (oAlertaWebservice.ViajeEstaEnLocal) Then
        'si la alerta no esta en local entonces lo agrega en la tabla AlertaEnLocal
        If (Not oAlertaWebservice.AlertaEstaEnLocal) Then
          oAlertaWebservice.GrabarAlertaEnLocal()
        End If
        'traspasa las alertas a la historia
        oAlertaWebservice.TraspasarAlertaHistoria()
      Else
        If (oAlertaWebservice.Activo) Then
          'obtiene documento XML con las reglas
          pathXml = Server.MapPath("Reglas.xml")
          xmlDoc = New XmlDocument()
          xmlDoc.Load(pathXml)

          'aplica regla
          Me.AplicarReglas(xmlDoc, oAlertaWebservice)
        End If

        'si la alerta es "Local" entonces indica que la gestion de las alertas de ese local llego a su fin y se traspasan todas las alertas a la historia
        If (oAlertaWebservice.DescripcionAlerta.ToUpper() = "LOCAL" Or oAlertaWebservice.TipoAlerta.ToUpper() = "VIEW" Or oAlertaWebservice.TipoAlerta.ToUpper() = "CRUCE GEOCERCA PARA INGRESAR A TIENDA") Then
          oAlertaWebservice.GrabarAlertaEnLocal()
          oAlertaWebservice.TraspasarTodoAlertaHistoria()
        End If
      End If

      owsAlerta.Respuesta.Codigo = Respuesta.eCodigo.Exito
      owsAlerta.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.Exito)

      Return owsAlerta
    Catch ex As Exception
      Throw
    End Try

  End Function

  Private Function CargarDatosTrazaViaje(ByVal NroTransporte As String, ByVal IdEmbarque As String, ByVal Operacion As String, ByVal PatenteTracto As String, ByVal PatenteTrailer As String, _
                                         ByVal RutTransportista As String, ByVal NombreTransportista As String, ByVal RutConductor As String, ByVal NombreConductor As String, _
                                         ByVal OrigenCodigo As String, ByVal OrigenDescripcion As String, ByVal FHSalidaOrigen As String, ByVal LocalDestino As String, _
                                         ByVal DestinoDescripcion As String, ByVal FHLlegadaDestino As String, ByVal FHAperturaPuerta As String, ByVal FHSalidaDestino As String, _
                                         ByVal TipoViaje As String, ByVal ProveedorGPS As String, ByVal DestinoDomicilio As String, ByVal DestinoRegion As String, _
                                         ByVal DestinoDescripcionRegion As String, ByVal DestinoComuna As String, ByVal DestinoLat As String, ByVal DestinoLon As String, _
                                         ByVal EstadoViaje As String, ByVal SecuenciaDestino As String, ByVal RamplaProveedorGPS As String, ByVal EstadoPuerta As String, _
                                         ByVal EstadoLat As String, ByVal EstadoLon As String, ByVal TempSalidaCD As String) As TipoDato.wsdTrazaViaje

    Dim owsTrazaViaje As New TipoDato.wsdTrazaViaje
    Dim oRespuesta As New TipoDato._Respuesta
    Dim dateFHSalidaOrigen, dateFHLlegadaDestino, dateFHAperturaPuerta, dateFHSalidaDestino As DateTime

    Try
      If MODO_DEBUG Then
        '*** USAR ESTE METODOS PARA USAR DATOS DE PRUEBA ***
        Herramientas.DatosPruebaTrazaViaje(NroTransporte, IdEmbarque, Operacion, PatenteTracto, PatenteTrailer, RutTransportista, NombreTransportista, RutConductor, _
                                           NombreConductor, OrigenCodigo, OrigenDescripcion, FHSalidaOrigen, LocalDestino, DestinoDescripcion, FHLlegadaDestino, _
                                           FHAperturaPuerta, FHSalidaDestino, TipoViaje, ProveedorGPS, DestinoDomicilio, DestinoRegion, DestinoDescripcionRegion, _
                                           DestinoComuna, DestinoLat, DestinoLon, EstadoViaje, SecuenciaDestino, RamplaProveedorGPS, EstadoPuerta, EstadoLat, _
                                           EstadoLon, TempSalidaCD)
        '***---------------------------------------------------
      End If

      'inicializa objeto
      owsTrazaViaje.Respuesta = oRespuesta

      'formatea valores
      NroTransporte = IIf(String.IsNullOrEmpty(NroTransporte), "-1", NroTransporte)
      Operacion = IIf(String.IsNullOrEmpty(Operacion), "-1", Operacion)
      OrigenCodigo = IIf(String.IsNullOrEmpty(OrigenCodigo), "-1", OrigenCodigo)
      LocalDestino = IIf(String.IsNullOrEmpty(LocalDestino), "-1", LocalDestino)
      SecuenciaDestino = IIf(String.IsNullOrEmpty(SecuenciaDestino), "-1", SecuenciaDestino)
      PatenteTracto = Herramientas.FormatearPatente(PatenteTracto)
      PatenteTrailer = Herramientas.FormatearPatente(PatenteTrailer)

      If (String.IsNullOrEmpty(FHSalidaOrigen)) Then
        dateFHSalidaOrigen = Herramientas.MyNow()
      Else
        dateFHSalidaOrigen = Convert.ToDateTime(FHSalidaOrigen)
      End If

      If (String.IsNullOrEmpty(FHLlegadaDestino)) Then
        dateFHLlegadaDestino = Herramientas.MyNow()
      Else
        dateFHLlegadaDestino = Convert.ToDateTime(FHLlegadaDestino)
      End If

      If (String.IsNullOrEmpty(FHAperturaPuerta)) Then
        dateFHAperturaPuerta = Herramientas.MyNow()
      Else
        dateFHAperturaPuerta = Convert.ToDateTime(FHAperturaPuerta)
      End If

      If (String.IsNullOrEmpty(FHSalidaDestino)) Then
        dateFHSalidaDestino = Herramientas.MyNow()
      Else
        dateFHSalidaDestino = Convert.ToDateTime(FHSalidaDestino)
      End If

      '---------------------------------------------------------------------
      ' graba datos en TrazaViaje
      '---------------------------------------------------------------------
      Dim oTrazaViaje As New TrazaViaje(NroTransporte, LocalDestino, EstadoViaje)
      oTrazaViaje.NroTransporte = NroTransporte
      oTrazaViaje.IdEmbarque = IdEmbarque
      oTrazaViaje.Operacion = Operacion
      oTrazaViaje.PatenteTracto = PatenteTracto
      oTrazaViaje.PatenteTrailer = PatenteTrailer
      oTrazaViaje.RutTransportista = RutTransportista
      oTrazaViaje.NombreTransportista = NombreTransportista
      oTrazaViaje.RutConductor = RutConductor
      oTrazaViaje.NombreConductor = NombreConductor
      oTrazaViaje.OrigenCodigo = OrigenCodigo
      oTrazaViaje.OrigenDescripcion = OrigenDescripcion
      oTrazaViaje.FHSalidaOrigen = dateFHSalidaOrigen
      oTrazaViaje.LocalDestino = LocalDestino
      oTrazaViaje.DestinoDescripcion = DestinoDescripcion
      oTrazaViaje.FHLlegadaDestino = dateFHLlegadaDestino
      oTrazaViaje.FHAperturaPuerta = dateFHAperturaPuerta
      oTrazaViaje.FHSalidaDestino = dateFHSalidaDestino
      oTrazaViaje.TipoViaje = TipoViaje
      oTrazaViaje.ProveedorGPS = ProveedorGPS
      oTrazaViaje.DestinoDomicilio = DestinoDomicilio
      oTrazaViaje.DestinoRegion = DestinoRegion
      oTrazaViaje.DestinoDescripcionRegion = DestinoDescripcionRegion
      oTrazaViaje.DestinoComuna = DestinoComuna
      oTrazaViaje.DestinoLat = DestinoLat
      oTrazaViaje.DestinoLon = DestinoLon
      oTrazaViaje.EstadoViaje = EstadoViaje
      oTrazaViaje.SecuenciaDestino = SecuenciaDestino
      oTrazaViaje.RamplaProveedorGPS = RamplaProveedorGPS
      oTrazaViaje.EstadoPuerta = EstadoPuerta
      oTrazaViaje.EstadoLat = EstadoLat
      oTrazaViaje.EstadoLon = EstadoLon
      oTrazaViaje.TempSalidaCD = TempSalidaCD

      If Not oTrazaViaje.Existe Then
        oTrazaViaje.FechaHoraCreacion = Herramientas.MyNow()
        oTrazaViaje.FechaHoraActualizacion = Herramientas.MyNow()
        oTrazaViaje.IngresoSCAT = False
        oTrazaViaje.GuardarNuevo()
      Else
        oTrazaViaje.FechaHoraActualizacion = Herramientas.MyNow()
        oTrazaViaje.Actualizar()
      End If

      owsTrazaViaje.Respuesta.Codigo = Respuesta.eCodigo.Exito
      owsTrazaViaje.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.Exito)

      Return owsTrazaViaje
    Catch ex As Exception
      Throw
    End Try

  End Function

  Private Function CargarDatosLogPerdidaSenal(ByVal NroTransporte As String, ByVal PatenteTracto As String, ByVal PatenteTrailer As String, ByVal LocalDestino As String, _
                                              ByVal FechaHoraInicio As String, ByVal FechaHoraTermino As String) As TipoDato.wsdLogPerdidaSenal

    Dim owsdLogPerdidaSenal As New TipoDato.wsdLogPerdidaSenal
    Dim oRespuesta As New TipoDato._Respuesta
    Dim dateFechaHoraInicio, dateFechaHoraTermino As DateTime

    Try
      If MODO_DEBUG Then
        '*** USAR ESTE METODOS PARA USAR DATOS DE PRUEBA ***
        Herramientas.DatosPruebaLogPerdidaSenal(NroTransporte, PatenteTracto, PatenteTrailer, LocalDestino, FechaHoraInicio, FechaHoraTermino)
        '***---------------------------------------------------
      End If

      'inicializa objeto
      owsdLogPerdidaSenal.Respuesta = oRespuesta

      'formatea valores
      NroTransporte = IIf(String.IsNullOrEmpty(NroTransporte), "-1", NroTransporte)
      LocalDestino = IIf(String.IsNullOrEmpty(LocalDestino), "-1", LocalDestino)
      PatenteTracto = Herramientas.FormatearPatente(PatenteTracto)
      PatenteTrailer = Herramientas.FormatearPatente(PatenteTrailer)

      If (String.IsNullOrEmpty(FechaHoraInicio)) Then
        dateFechaHoraInicio = Herramientas.MyNow()
      Else
        dateFechaHoraInicio = Convert.ToDateTime(FechaHoraInicio)
      End If

      If (String.IsNullOrEmpty(FechaHoraTermino)) Then
        dateFechaHoraTermino = Herramientas.MyNow()
      Else
        dateFechaHoraTermino = Convert.ToDateTime(FechaHoraTermino)
      End If

      '---------------------------------------------------------------------
      ' graba datos en LogPerdidaSenal
      '---------------------------------------------------------------------
      Dim oLogPerdidaSenal As New LogPerdidaSenal()
      oLogPerdidaSenal.NroTransporte = NroTransporte
      oLogPerdidaSenal.PatenteTracto = PatenteTracto
      oLogPerdidaSenal.PatenteTrailer = PatenteTrailer
      oLogPerdidaSenal.LocalDestino = LocalDestino
      oLogPerdidaSenal.FechaHoraInicio = dateFechaHoraInicio
      oLogPerdidaSenal.FechaHoraTermino = dateFechaHoraTermino
      oLogPerdidaSenal.FechaHoraCreacion = Herramientas.MyNow()
      oLogPerdidaSenal.GuardarNuevo()

      owsdLogPerdidaSenal.Respuesta.Codigo = Respuesta.eCodigo.Exito
      owsdLogPerdidaSenal.Respuesta.Descripcion = Respuesta.ObtenerDescripcionPorCodigo(Respuesta.eCodigo.Exito)

      Return owsdLogPerdidaSenal
    Catch ex As Exception
      Throw
    End Try

  End Function

  ''' <summary>
  ''' aplica regla a la alerta segun corresponda para ser gestionada
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AplicarReglas(ByVal xmlDoc As XmlDocument, ByVal oAlertaWebservice As AlertaWebservice)
    Dim tipoAlerta, nodoTipoAlerta, nombreAlerta As String
    Dim xmlAlertas As XmlNodeList

    Try
      tipoAlerta = oAlertaWebservice.TipoAlerta
      Select Case tipoAlerta.ToUpper()
        Case "RUTA"
          nodoTipoAlerta = "ruta"
        Case "OPERACIONAL"
          nodoTipoAlerta = "operacional"
        Case "TIENDAS"
          nodoTipoAlerta = "tiendas"
        Case Else
          Throw New Exception("El tipo de alerta no está definido")
      End Select

      '-------------------------------------------------------------
      'obtiene las reglas de las alertas asociadas al tipoAlerta
      xmlAlertas = Regla.ObtenerAlertasXMLPorTipoAlerta(xmlDoc, nodoTipoAlerta)

      '--------------------------------------------------------------------
      'filtra las alerats del XML que coincidan con la alerta ingresada
      Dim xmlAlertasFiltradas = From xmlElem As XmlElement In xmlAlertas
                                Where xmlElem.GetAttribute("nombre").ToUpper() = oAlertaWebservice.DescripcionAlerta.ToUpper()

      For Each row As XmlElement In xmlAlertasFiltradas
        nombreAlerta = row.GetAttribute("nombre")

        If (nombreAlerta.ToUpper() = oAlertaWebservice.DescripcionAlerta.ToUpper()) Then
          Me.AplicarCriterios(oAlertaWebservice, row)
        End If
      Next

    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' aplica los criterios de la alerta segun corresponda
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AplicarCriterios(ByVal oAlertaWebservice As AlertaWebservice, ByVal row As XmlElement)
    Dim xmlCriterios As XmlNodeList
    Dim ocurrencias As Integer
    Dim condicion, canal, horarioAtencion As String

    Try
      '-------------------------------------------------------------
      'obtiene los criterios de la alerta
      xmlCriterios = row.GetElementsByTagName("criterio")

      For Each rowCriterio As XmlElement In xmlCriterios
        ocurrencias = rowCriterio.GetAttribute("ocurrencias")
        condicion = rowCriterio.GetAttribute("condicion")
        canal = rowCriterio.GetAttribute("canal")
        horarioAtencion = rowCriterio.GetAttribute("horarioAtencion")

        'revisa que la alerta se pueda gestionar en el dia y hora actual
        If (Herramientas.EstaDentroHorarioGestionAlerta(horarioAtencion)) Then

          'revisa que criterio corresponde a la ocurrencia actual de la alerta para aplicar la regla
          If (Regla.CumpleCondicionOcurrencia(oAlertaWebservice.Ocurrencia, ocurrencias, condicion)) Then

            Select Case canal.ToLower()
              Case "callcenter"
                Regla.EnviarPorCallCenter(oAlertaWebservice, rowCriterio)
              Case "email"
                Regla.EnviarPorEmail(oAlertaWebservice, rowCriterio)
              Case "sms"
                Regla.EnviarPorSMS(oAlertaWebservice, rowCriterio)
              Case Else
                Throw New Exception("El canal no está definido")
            End Select

          End If
        End If
      Next

    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

#End Region

End Class
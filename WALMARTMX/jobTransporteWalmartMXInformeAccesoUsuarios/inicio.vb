﻿Imports CapaNegocio
Imports System.Data
Imports System.Text
Imports System.Configuration
Imports Syncfusion.XlsIO

Module inicio

#Region "Configuracion Job"
    Const NOMBRE_JOB As String = "jobTransporteWalmartMXInformeAccesoUsuarios"
    Const KEY As String = "InformeAccesoUsuarios"
#End Region

#Region "Enum"
    Enum eTablaConfiguracion As Integer
        EmailPara = 0
        EmailCC = 1
        DiasEjecucion = 2
        AsuntoEmail = 3
        CuerpoEmail = 4
    End Enum

#End Region

#Region "Privado"
    ''' <summary>
    ''' determina si se puede ejecutar la consola en el dia actual
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>Por VSR, 03/08/2009</remarks>
    Private Function EjecutarDiaMes() As Boolean
        Dim sePuedeEjecutar As Boolean = False
        Dim dsConfiguracionXML As New DataSet
        Dim diasEjecucion, incluirFinMes As String
        Dim diaMesActual As String = Herramientas.MyNow().Day
        Dim ultimoDiaMes As Date

        Try
            dsConfiguracionXML = Herramientas.LeerXML("configuracion.xml")
            diasEjecucion = dsConfiguracionXML.Tables(eTablaConfiguracion.DiasEjecucion).Rows(0).Item("diasEjecucion")
            incluirFinMes = dsConfiguracionXML.Tables(eTablaConfiguracion.DiasEjecucion).Rows(0).Item("incluirFinMes")

            If (InStr(diasEjecucion, "{-1}") > 0) Or (InStr(diasEjecucion, "{" & diaMesActual & "}") > 0) Then
                sePuedeEjecutar = True
            Else
                If incluirFinMes.Trim.ToUpper = "TRUE" Then
                    ultimoDiaMes = Herramientas.ObtenerUltimoDiaMes(Herramientas.MyNow())
                    If Day(ultimoDiaMes) = diaMesActual Then
                        sePuedeEjecutar = True
                    End If
                End If
            End If
        Catch ex As Exception
            sePuedeEjecutar = False
        End Try
        Return sePuedeEjecutar
    End Function

    ''' <summary>
    ''' genera el archivo excel
    ''' </summary>
    ''' <param name="nombreArchivo"></param>
    ''' <returns></returns>
    ''' <remarks>Por VRS, 02/10/2009</remarks>
    Private Function GenerarExcel(ByVal ds As DataSet, ByVal nombreArchivo As String) As Boolean
        Dim fueGenerado As Boolean = False
        Dim excelEngine As ExcelEngine = New ExcelEngine()
        Dim application As IApplication = excelEngine.Excel
        Dim workbook As IWorkbook

        Try
            application.UseNativeStorage = False

            Dim nombreHoja As String() = {"CUADRO RESUMEN USABILIDAD", "GPSTRACK", "CALLCENTER", "CARRIER", "APP MOVIL", "LISTADO DATOS"}
            workbook = application.Workbooks.Create(nombreHoja)

            Dim sheet0 As IWorksheet = workbook.Worksheets(nombreHoja(0))
            GenerarHojaExcel(workbook, sheet0, ds.Tables(0))

            Dim sheet1 As IWorksheet = workbook.Worksheets(nombreHoja(1))
            GenerarHojaExcel(workbook, sheet1, ds.Tables(1))

            Dim sheet2 As IWorksheet = workbook.Worksheets(nombreHoja(2))
            GenerarHojaExcel(workbook, sheet2, ds.Tables(2))

            Dim sheet3 As IWorksheet = workbook.Worksheets(nombreHoja(3))
            GenerarHojaExcel(workbook, sheet3, ds.Tables(3))

            Dim sheet4 As IWorksheet = workbook.Worksheets(nombreHoja(4))
            GenerarHojaExcel(workbook, sheet4, ds.Tables(4))

            Dim sheet5 As IWorksheet = workbook.Worksheets(nombreHoja(5))
            GenerarHojaExcel(workbook, sheet5, ds.Tables(5))

            workbook.SaveAs(Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls")
            workbook.Close()
            fueGenerado = True

            Return fueGenerado
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            excelEngine.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' genera el archivo excel
    ''' </summary>
    ''' <param name="workbook"></param>
    ''' <param name="sheet"></param>
    ''' <param name="dt"></param>
    ''' <remarks></remarks>
    Private Sub GenerarHojaExcel(ByRef workbook As IWorkbook, ByRef sheet As IWorksheet, ByVal dt As DataTable)
        Dim fueGenerado As Boolean = False

        Try
            sheet.ImportDataTable(dt, True, 1, 1)
            With sheet.Range(1, 1, 1, dt.Columns.Count).CellStyle
                .Font.Bold = True
            End With

            sheet.UsedRange.AutofitColumns()
            sheet.IsGridLinesVisible = True
            fueGenerado = True
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region

    Sub Main()
        Dim msgProceso As String = ""
        'construye fecha inicio
        Dim ds As New DataSet
        Dim body, bodyMensaje As String
        Dim contenidoLog As String = ""
        Dim ColeccionEmailsPara, ColeccionEmailsCC As System.Net.Mail.MailAddressCollection
        Dim sb As New StringBuilder
        Dim contadorErrores As Integer = 0
        Dim nombreArchivo, asunto As String
        Dim listadoMailCC As String
        Dim listadoMailPara As String = ""
        Dim fueGenerado As Boolean
        Dim fechaIniISO, fechaFinISO As String
        Dim logEstado As String
        Dim logAccion As String = ""

        'inicia la ejecucion
        Console.WriteLine("Iniciando tarea: " & NOMBRE_JOB)
        Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
        Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug.ToString)

        Dim oInformacionConsola As Herramientas.InformacionConsola
        oInformacionConsola = Herramientas.ObtenerInformacionConsola(NOMBRE_JOB)

        Try
            If (oInformacionConsola.IdConsola <> -1 And oInformacionConsola.Activo) Then
                'crea el archivo en disco
                nombreArchivo = Archivo.CrearNombreUnicoArchivo(KEY)

                'construye las fechas para el filtro
                fechaIniISO = "-1"
                fechaFinISO = "-1"

                'obtiene listado de imputados
                ds = Sistema.ConsultaGenerica(fechaIniISO, fechaFinISO, "spu_Job_InformeAccesoUsuarios")

                fueGenerado = GenerarExcel(ds, nombreArchivo)

                'si hay abogados entonces verifica si hay incidentes que informar
                If fueGenerado Then
                    'construye cuerpo del mail
                    bodyMensaje = oInformacionConsola.EmailCuerpo
                    bodyMensaje = bodyMensaje.Replace("{BR}", vbCrLf)

                    'constuye contenido del excel y del log
                    body = Herramientas.ObtenerTablaHTMLDesdeDataSet(ds, 0)
                    body &= Herramientas.ObtenerTablaHTMLDesdeDataSet(ds, 1)
                    body &= Herramientas.ObtenerTablaHTMLDesdeDataSet(ds, 2)
                    body &= Herramientas.ObtenerTablaHTMLDesdeDataSet(ds, 3)
                    body &= Herramientas.ObtenerTablaHTMLDesdeDataSet(ds, 4)
                    'construye listado de email de destinos

                    If Herramientas.MailModoDebug Then
                        ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas)
                        ColeccionEmailsCC = Nothing
                    Else
                        'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
                        listadoMailPara = oInformacionConsola.EmailPara
                        listadoMailCC = oInformacionConsola.EmailCC
                        listadoMailCC &= IIf(String.IsNullOrEmpty(listadoMailCC), Herramientas.MailAdministrador, ";" & Herramientas.MailAdministrador)

                        'construye las colecciones
                        ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
                        ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailCC)
                    End If

                    'crea contenido para crear el log
                    contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(body & "<br/><div>Enviado a: " & listadoMailPara & "</div>", NOMBRE_JOB)
                    Dim path As String = Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls"

                    'envia el mail y retorna mensaje de exito(mensaje vacio) o de error
                    asunto = oInformacionConsola.EmailAsunto
                    msgProceso = Mail.Enviar(Constantes.mailHost, Constantes.mailEmailEmisor, Constantes.mailPasswordEmailEmisor, Constantes.mailEmailNombre, ColeccionEmailsPara, asunto, bodyMensaje, False, ColeccionEmailsCC, True, path)
                    If msgProceso <> "" Then
                        contadorErrores = contadorErrores + 1
                        contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & msgProceso, NOMBRE_JOB)
                        logAccion = msgProceso
                    End If

                    'limpia variable para los nuevos email
                    ColeccionEmailsPara = Nothing
                    ColeccionEmailsCC = Nothing
                Else
                    contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("No hay registros", NOMBRE_JOB)
                    logAccion = "No hay registros"
                End If
            End If
        Catch ex As Exception
            contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & ex.Message, NOMBRE_JOB)
            contadorErrores = 1
            logAccion = ex.Message
        End Try
        Console.WriteLine("Fin tarea: " & NOMBRE_JOB)

        'crea log del proceso
        Dim sufijoArchivo As String = IIf(contadorErrores > 0, "EJECUTADO_CON_ERRORES", "EJECUTADO_CON_EXITO")
        nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
        Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")

        '---------------------------------------------------
        'guarda el log de la consola desde API
        If (oInformacionConsola.IdConsola <> -1 And oInformacionConsola.Activo) Then
            logEstado = IIf(contadorErrores > 0, "ERROR", "EXITO")
            logAccion = IIf(String.IsNullOrEmpty(logAccion), "El proceso se ejecutó de manera exitosa", logAccion)
            Herramientas.GrabarLogConsola(oInformacionConsola.IdConsola, logEstado, logAccion)
        End If
    End Sub

End Module

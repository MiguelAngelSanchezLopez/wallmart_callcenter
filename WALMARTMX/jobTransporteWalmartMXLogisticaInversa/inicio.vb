﻿Imports System.IO
Imports CapaNegocio
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports Microsoft.ApplicationBlocks.Data

Module inicio

  Dim logMessage As String

#Region "Constantes"
  Private Const RUTA_CARPETA_FTP As String = "/ftp-walmart-mx/LogisticaInversa/"
  Private Const RUTA_CARPETA_FTP_ERRORES As String = RUTA_CARPETA_FTP & "Errores/"
  Private Const RUTA_CARPETA_FTP_PROCESADOS As String = RUTA_CARPETA_FTP & "Procesados/"
  Private Const RUTA_CARPETA_FTP_LOG As String = RUTA_CARPETA_FTP & "Log/"
  Private Const RUTA_CARPETA_SERVIDOR_TMP As String = "Temp"

  Private Const filter As String = "*.xlsx"
  Private Const jobName As String = "jobTransporteWalmartMXLogisticaInversa"
#End Region

  Sub Main()
    Dim logFileName As String
    Dim status As Boolean
    Dim countError As Integer

    logMessage = "Tarea: " & jobName & "</BR>"
    logMessage &= "Ambiente: " & Herramientas.ObtenerAmbienteEjecucion & "</BR>"
    logMessage &= "Modo Debug: " & Herramientas.MailModoDebug & "</BR>"

    Console.WriteLine("Tarea: " & jobName)
    Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
    Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug)

    'Creo carpeta destino
    If (Not Directory.Exists(RUTA_CARPETA_SERVIDOR_TMP)) Then
      Directory.CreateDirectory(RUTA_CARPETA_SERVIDOR_TMP)
    Else
      Directory.Delete(RUTA_CARPETA_SERVIDOR_TMP, True)
      Directory.CreateDirectory(RUTA_CARPETA_SERVIDOR_TMP)
    End If

    'Creo carpeta destino
    If (Not Directory.Exists("Temp_Excel")) Then
      Directory.CreateDirectory("Temp_Excel")
    Else
      Directory.Delete("Temp_Excel", True)
      Directory.CreateDirectory("Temp_Excel")
    End If

    'Obtengo archivos del FTP
    downloadFileFtp()

    'Obtengo archivos del servidor
    Dim folder As New DirectoryInfo(RUTA_CARPETA_SERVIDOR_TMP)
    Dim files As FileInfo() = folder.GetFiles(filter)
    If (files.Count > 0) Then

      For Each file As FileInfo In files

        status = readFile(file)

        'si no existe error
        If (status) Then
          moverArchivos(RUTA_CARPETA_FTP & file.Name, RUTA_CARPETA_FTP_PROCESADOS & file.Name)
        Else
          countError += 1

          moverArchivos(RUTA_CARPETA_FTP & file.Name, RUTA_CARPETA_FTP_ERRORES & file.Name)
        End If

        'Se elimina archivo descargado
        file.Delete()
      Next

      'Manejo de errores
      If (countError = 0) Then
        logFileName = "AltoTrackWM_MX_InformacionLogisticaInversa_EJECUTADO_CON_EXITO_" & Herramientas.MyNow().ToString("yyyyMMddHHmmss")
      Else
        logFileName = "AltoTrackWM_MX_InformacionLogisticaInversa_EJECUTADO_CON_ERRORES_" & Herramientas.MyNow().ToString("yyyyMMddHHmmss")
      End If

      'Manejo de archivo Log
      Dim pathLog As String = Archivo.CrearArchivoTextoEnDisco("<html><head></head><body>" & logMessage & "</body></html>", RUTA_CARPETA_SERVIDOR_TMP, jobName, logFileName, "html")
      uploadFileFtp(pathLog, RUTA_CARPETA_FTP_LOG & logFileName & ".html")
      System.IO.File.Delete(pathLog)
    End If

  End Sub

  Private Function readFile(ByVal file As FileInfo) As Boolean
    Try
      Dim conStr As String = ""
      Dim cmdExcel As New OleDbCommand()
      Dim oda As New OleDbDataAdapter()
      Dim dtExcel As New DataTable()


      Select Case file.Extension
        Case ".xls"
          conStr = "Provider=Microsoft.Jet.Oledb.4.0; data source= {0};Extended properties=""Excel 8.0;hdr=yes;imex=1"""
        Case ".xlsx"
          conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source= {0};Extended Properties=""Excel 12.0;hdr=YES;imex=1"""
      End Select

      Dim newName As String = "Temp_Excel" + "\" + file.Name
      System.IO.File.Copy(file.FullName, newName, True)
      conStr = String.Format(conStr, newName)

      Dim connExcel As New OleDbConnection(conStr)
      cmdExcel.Connection = connExcel

      'Get the name of First Sheet
      connExcel.Open()
      Dim dtExcelSchema As DataTable
      dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
      connExcel.Close()

      'Read Data from First Sheet
      connExcel.Open()
      cmdExcel.CommandText = "SELECT * From [FOLIOS TRANSITO$]"
      oda.SelectCommand = cmdExcel
      oda.Fill(dtExcel)
      connExcel.Close()

      logMessage &= Herramientas.MyNow() & " --> Leyendo archivo '" & file.Name & "'</BR>"

      Dim retn As Boolean = cargarArchivoBD(dtExcel, file)

      logMessage &= Herramientas.MyNow() & " --> Exitoso</BR>"
      Return retn

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
      Return False
    End Try
  End Function

  Private Function cargarArchivoBD(ByVal Dt As DataTable, ByVal file As FileInfo) As Boolean

    Dim FileName As String = file.Name 'Path.GetFileName(fileCarga.PostedFile.FileName)
    Dim extension As String = file.Extension ' Path.GetExtension(fileCarga.PostedFile.FileName)
    'Dim rootPath As String = Utilidades.RutaLogsSitio & "\" & NOMBRE_CARPETA_CARGA
    Dim filePath As String = file.FullName ' rootPath + "\" + FileName
    Dim listadoErrores As New List(Of String)

    Try

      'crea carpeta si es que no existe
      ' Archivo.CrearDirectorioEnDisco(rootPath)

      If (extension.ToLower() <> ".xlsx" And extension.ToLower() <> ".xls") Then
        logMessage &= Herramientas.MyNow() & "{ERROR} El tipo de archivo no es v&aacute;lido, debe ser un archivo *.xlsx | xls."
      Else

        Dim stopWatch As New Stopwatch()
        stopWatch.Start()

        'fileCarga.SaveAs(filePath)

        'Se cargan los datos del excel
        Dim dtExcel As DataTable = Dt ' cargarDataTable(filePath, extension)

        'Se validan los datos antes de guardar
        If (validarDataTable(dtExcel)) Then

          'Cargo los nuevos datos
          Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(Conexion.StringConexion)
            bulkCopy.DestinationTableName = "dbo.InformacionLogisticaInvTemporal"
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ID_FOLIO", "ID_FOLIO"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("FOLIO", "FOLIO"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("SALIDA_DET", "SALIDA_DET"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("DETERMINANTE", "DETERMINANTE"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("PLACA_REMOLQUE", "PLACA_REMOLQUE"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("PLACA_TRACTOR", "PLACA_TRACTOR"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("MARCHAMO_1", "MARCHAMO_1"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("MARCHAMO_2", "MARCHAMO_2"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("RECORRIDO", "RECORRIDO"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("LT", "LT"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("T_TARIMA", "TARIMAS"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("DON_TARIMA", "DONACION"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("T_RPC", "RPC"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("T_CARTON", "CARTON"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("T_PLAYO", "PLAYO"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("PAPEL", "PAPEL"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("REJILLAS", "REJILLA"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("BIDONES", "BIDONES"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("CUBETAS_PLASTICO", "CUBETAS"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("PET", "PET"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("LONA", "LONA"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("CELOFAN", "CELOFAN"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("MADERA", "MADERA"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("GANCHO", "GANCHO"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ASOCIADO", "ASOCIADO"))
            bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("CEDIS", "CEDIS"))
            bulkCopy.BulkCopyTimeout = 0
            ' Write from the source to the destination.
            bulkCopy.WriteToServer(dtExcel)
          End Using

          stopWatch.Stop()

          Dim ts As TimeSpan = stopWatch.Elapsed

          Try
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, "spu_jobLogisticaInvTemporalToOriginal")
          Catch ex As Exception
            logMessage &= Herramientas.MyNow() & "{ERROR} Al ejecutar procedimiento spu_jobLogisticaInvTemporalToOriginal (Tiempo operación " + ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":" + ts.Seconds.ToString() + ")."
          End Try

          logMessage &= Herramientas.MyNow() & "{INGRESADO} La carga fue grabada correctamente (Tiempo operación " + ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":" + ts.Seconds.ToString() + ")."

          Return True
        End If

      End If

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & "{ERROR}" + ex.Message + "."
    End Try

    Return False

  End Function

  Private Function validarDataTable(ByVal dtExcel As DataTable) As Boolean
    Dim columnasQueFaltan As String = ""
    Dim validacionDatosFila As String = ""
    Dim columns As New List(Of String)
    Dim columnsNotFound As New List(Of String)
    columns.Add("ID_FOLIO")
    columns.Add("FOLIO")
    columns.Add("SALIDA_DET")
    columns.Add("DETERMINANTE")
    columns.Add("PLACA_REMOLQUE")
    columns.Add("PLACA_TRACTOR")
    columns.Add("MARCHAMO_1")
    columns.Add("MARCHAMO_2")
    columns.Add("RECORRIDO")
    columns.Add("LT")
    columns.Add("T_TARIMA")
    columns.Add("DON_TARIMA")
    columns.Add("T_RPC")
    columns.Add("T_CARTON")
    columns.Add("T_PLAYO")
    columns.Add("PAPEL")
    columns.Add("REJILLAS")
    columns.Add("BIDONES")
    columns.Add("CUBETAS_PLASTICO")
    columns.Add("PET")
    columns.Add("LONA")
    columns.Add("CELOFAN")
    columns.Add("MADERA")
    columns.Add("GANCHO")
    columns.Add("ASOCIADO")
    columns.Add("CEDIS")

    Try

      'Valida error en la obtención
      If (dtExcel Is Nothing) Then
        logMessage &= Herramientas.MyNow() & "{ERROR} Ha ocurrido un error al intentar obtener los datos del archivo."
        Return False
      End If

      'Valida que existan registros
      If (dtExcel.Rows.Count = 0) Then
        logMessage &= Herramientas.MyNow() & "{ERROR} No se encontraron registros para cargar."
        Return False
      End If

      'Valida cantidad de columnas
      'If dtExcel.Columns.Count <> columns.Count Then
      '    logMessage &= Herramientas.MyNow() & "{ERROR} El n&uacute;mero de columnas no es válido, deben ser " & columns.Count
      '    Return False
      'End If

      'Valida que esten todas las columnas
      For Each nombre As String In columns
        If Not dtExcel.Columns.Contains(nombre) Then
          columnsNotFound.Add(nombre)
        End If
      Next

      If (columnsNotFound.Count > 0) Then
        Dim listadoErrores As New List(Of String)
        listadoErrores.Add("Faltan las siguientes columnas:<br />" & String.Join("-", columnsNotFound.ToArray()))

        '  Session(SESION_LISTADO_ERRORES) = listadoErrores
        logMessage &= Herramientas.MyNow() & "{ERROR} El archivo tiene errores, favor revisar y volver a cargar."

        Return False
      End If

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & "{ERROR} " & ex.Message & "."
      Return False
    End Try

    Return True
  End Function

  Private Sub downloadFileFtp()
    Try

      logMessage &= Herramientas.MyNow() & " --> Descargando archivos desde FTP</BR>"

      Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
      Dim dt As New DataTable
      Dim listDirectory, listFileFtp As FTPdirectory

      'obtiene contenido del directorio
      listDirectory = ftp.ListDirectoryDetail(RUTA_CARPETA_FTP)

      'filtra los archivos por la extension
      listFileFtp = listDirectory.GetFiles("xlsx")

      'obtiene los archivos del directorio
      For Each file As FTPfileInfo In listFileFtp

        Dim pathFileSource As String = RUTA_CARPETA_FTP & file.Filename
        Dim pathFileDestination As String = RUTA_CARPETA_SERVIDOR_TMP & "\" & file.Filename

        logMessage &= Herramientas.MyNow() & " --> Descargando " & file.Filename & "</BR>"
        ftp.Download(pathFileSource, pathFileDestination)
        logMessage &= Herramientas.MyNow() & " --> Exitoso</BR>"

      Next

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
    End Try

  End Sub

  Private Sub uploadFileFtp(ByVal pathFile As String, ByVal destinationFile As String)
    Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
    ftp.Upload(pathFile, destinationFile)
  End Sub

  Private Sub moverArchivos(ByVal source As String, ByVal destination As String)
    Try

      logMessage &= Herramientas.MyNow() & " --> Moviendo desde """ & source & """ a """ + destination + """</BR>"

      Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
      ftp.FtpRename(source, destination)

      logMessage &= Herramientas.MyNow() & " --> Exitoso</BR>"

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
    End Try

  End Sub
End Module

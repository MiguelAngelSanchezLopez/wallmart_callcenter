﻿Imports System.Data
Imports System.Text
Imports System.Configuration
Imports CapaNegocio
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Runtime.Serialization.Formatters


Module inicio

#Region "Enum"
    Enum eTablaConfiguracion As Integer
        EmailPara = 0
        EmailCC = 1
        MailAsunto = 2
        MailBody = 3
    End Enum
#End Region

#Region "Configuracion Job"
    Const NOMBRE_JOB As String = "JobTransporteWalmartMXFormCarga"
#End Region

#Region "Constantes"
    Private Const RUTA_CARPETA_SERVIDOR As String = "\\10.15.2.116\d$\FTP\ftp-transporte"
    'Private Const RUTA_CARPETA_SERVIDOR As String = "E:\altotrack\ftp-walmart-mx"
    Private Const RUTA_CARPETA_SERVIDOR_DESTINO As String = "\\10.15.2.116\d$\FTP\ftp-walmart-mx"
    Private Const RUTA_CARPETA_SERVIDOR_JSON As String = RUTA_CARPETA_SERVIDOR_DESTINO & "\FormCarga\Json\"
    Private Const RUTA_CARPETA_SERVIDOR_JSON_ERROR As String = RUTA_CARPETA_SERVIDOR_DESTINO & "\FormCarga\Json\Errores\"
    Private Const RUTA_CARPETA_SERVIDOR_PDF As String = RUTA_CARPETA_SERVIDOR_DESTINO & "\FormCarga\Pdf\"
    Private Const RUTA_CARPETA_SERVIDOR_IMG As String = RUTA_CARPETA_SERVIDOR_DESTINO & "\FormCarga\Img\"
    Private Const STARTS_WITH As String = "WalmartMx - Carga"

#End Region

#Region "Variables Globales"
    Dim identifier, msgLog As String
    Dim dCabecera, dPage1, dPage2, dPage3, dPage4, dPage5, dPage6, dPage7, dPage8, dPage9, dPage10, dPage11, dPage12, dPage13, _
      dPage14, dPage15, dPage16, dPage17 As New Dictionary(Of String, String)

    Dim dPage2_Fotos, dPage3_Fotos, dPage4_Fotos, dPage5_Fotos, dPage6_Fotos, dPage7_Fotos, dPage8_Fotos, dPage9_Fotos, dPage10_Fotos, _
      dPage11_Fotos, dPage12_Fotos, dPage13_Fotos, dPage14_Fotos, dPage15_Fotos, dPage16_Fotos, dPage17_Fotos As New List(Of Dictionary(Of String, String))
#End Region

#Region "Inicio"
    Sub Main()
        Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo("Carga")
        Dim folder As New DirectoryInfo(RUTA_CARPETA_SERVIDOR)
        Dim errores As String = ""
        Dim filesJson As FileInfo()
        Dim filesPdf As FileInfo()
        Dim filesJpg As FileInfo()
        Dim status As Boolean
        Dim countError As Integer
        Dim PATH_ERROR As String = RUTA_CARPETA_SERVIDOR_JSON_ERROR & Herramientas.MyNow().ToString("dd-MM-yyyy") & "\"


        msgLog = "Tarea: " & NOMBRE_JOB & "</BR>"
        msgLog &= "Ambiente: " & Herramientas.ObtenerAmbienteEjecucion & "</BR>"
        msgLog &= "Modo Debug: " & Herramientas.MailModoDebug & "</BR>"

        Console.WriteLine("Tarea: " & NOMBRE_JOB)
        Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
        Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug)

        'Obtengo el listado de archivos
        filesJson = folder.GetFiles(STARTS_WITH + "*.json")
        filesPdf = folder.GetFiles(STARTS_WITH + "*.pdf")
        filesJpg = folder.GetFiles(STARTS_WITH + "*.jpg").Union(folder.GetFiles(STARTS_WITH + "*.jpeg")).Union(folder.GetFiles(STARTS_WITH + "*.png")).ToArray()

        If (filesJson.Count > 0) Then
            'ARCHIVOS JSON
            '----------------------------------------------------------
            msgLog &= "</BR> -- Archivos JSON --"

            For Each file As FileInfo In filesJson

                status = grabarJSon(file, errores)

                'si no existe error en la lectura
                If (status) Then
                    'Muevo json a carpeta

                    moverArchivos(file, RUTA_CARPETA_SERVIDOR_JSON)

                Else

                    countError += 1

                    'Muevo archivo con error

                    moverArchivos(file, PATH_ERROR)
                End If

            Next

            'Manejo de errores
            If (countError > 0) Then
                nombreArchivo &= "_EJECUTADO_CON_ERRORES"

                'Envio correo con errores
                envioEmailError(errores)
            Else
                nombreArchivo &= "_EJECUTADO_CON_EXITO"
            End If


            'ARCHIVOS PDF
            '----------------------------------------------------------
            If (filesPdf.Count > 0) Then
                msgLog &= "</BR></BR> -- Archivos PDF --"
            End If

            For Each file As FileInfo In filesPdf
                moverArchivos(file, RUTA_CARPETA_SERVIDOR_PDF)
            Next

            'ARCHIVOS IMG
            '----------------------------------------------------------
            If (filesJpg.Count > 0) Then
                msgLog &= "</BR></BR> -- Archivos IMG --"
            End If

            For Each file As FileInfo In filesJpg
                moverArchivos(file, RUTA_CARPETA_SERVIDOR_IMG)
            Next

        Else
            msgLog &= "</BR></BR>- NO SE ENCONTRARON ARCHIVOS JSON - </BR>"
            nombreArchivo &= "_NO_EXISTEN_ARCHIVOS"
        End If

        Archivo.CrearArchivoTextoEnDisco(msgLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
    End Sub

#End Region

#Region "Grabar JSON"

    Private Function grabarJSon(ByVal file As FileInfo, ByRef errores As String) As Boolean
        Try

            msgLog &= "</BR>--> Grabando contenido de '" & file.Name & "' "

            Dim objReader As StreamReader
            Dim jsonDatos As String
            Dim objJSON As Object

            objReader = New StreamReader(file.FullName)
            jsonDatos = "[" & objReader.ReadLine() & "]"
            objReader.Close()

            objJSON = MyJSON.ConvertJSONToObject(jsonDatos)

            'Obtengo datos de cabecera
            getValuesCabecera(objJSON(0))


            'Obtengo datos Pages
            getValuesPages(objJSON(0))

            'dCabecera


            '    Dim queryString As String = ""
            '    For Each campo In dPage3 ' Remplaza con la pagina para ver si lo esta haciendo
            '      queryString &= "[" & campo.Key & "],"
            '     queryString &= "[" & campo.Value & "],"


            '    Next
            '      For Each conjuntofotos In dPage3_Fotos ' Remplaza con la pagina para ver si lo esta haciendo
            'For Each campo In conjuntofotos ' Remplaza con la pagina para ver si lo esta haciendo
            '          queryString &= "[" & campo.Key & "],"
            '         queryString &= "[" & campo.Value & "],"


            '       Next

            '    Next

            '    Console.WriteLine(queryString)
            '    Console.ReadLine()


            'grabo los diccionarios
            grabarDatos()

            msgLog &= " [Exitoso]"

            Return True

        Catch ex As Exception
            msgLog &= " [Error: " & ex.Message.ToString() & "]"
            errores &= "{$BR}{$BR}* " & file.Name
            errores &= "{$BR}  [Error: " & ex.Message.ToString() & "]"
            Return False
        End Try
    End Function

    Private Sub getValuesPages(ByVal obj As Object)
        Dim objPage, objAnswer As Object
        Dim index As Integer = 1

        objPage = MyJSON.ItemObjectJSON(obj, "pages")

        For Each rowPage In objPage

            objAnswer = MyJSON.ItemObjectJSON(rowPage, "answers")

            Console.WriteLine()
            Console.Write("Pagina ")
            Console.Write(index)
            Console.WriteLine()


            Select Case index

                Case 1
                    getValuesPage1(objAnswer)
                Case 2
                    getValuesPage2(objAnswer)
                Case 3
                    getValuesPage3(objAnswer)
                Case 4
                    getValuesPage4(objAnswer)
                Case 5
                    getValuesPageAndImages(objAnswer, dPage5, dPage5_Fotos) ' cambio para reutilizar codigo.
                Case 6
                    getValuesPageAndImages(objAnswer, dPage6, dPage6_Fotos)
                Case 7
                    getValuesPageAndImages(objAnswer, dPage7, dPage7_Fotos)
                Case 8
                    getValuesPageAndImages(objAnswer, dPage8, dPage8_Fotos)
                Case 9
                    getValuesPageAndImages(objAnswer, dPage9, dPage9_Fotos)
                Case 10
                    getValuesPageAndImages(objAnswer, dPage10, dPage10_Fotos)
                Case 11
                    getValuesPageAndImages(objAnswer, dPage11, dPage11_Fotos)
                Case 12
                    getValuesPageAndImages(objAnswer, dPage12, dPage12_Fotos)
                Case 13
                    getValuesPageAndImages(objAnswer, dPage13, dPage13_Fotos)
                Case 14
                    getValuesPageAndImages(objAnswer, dPage14, dPage14_Fotos)
                Case 15
                    getValuesPageAndImages(objAnswer, dPage15, dPage15_Fotos)
                Case 16
                    getValuesPageAndImages(objAnswer, dPage16, dPage16_Fotos)
                Case 17
                    getValuesPageAndImages(objAnswer, dPage17, dPage17_Fotos)
            End Select

            index += 1

        Next

    End Sub

    Private Sub getValuesCabecera(ByVal objRow As Object)
        Dim value As String
        Dim objUser As Object

        dCabecera.Clear()

        value = MyJSON.ItemObjectJSON(objRow, "identifier")
        If (Not value Is Nothing) Then
            dCabecera.Add("identifier", value)
            identifier = value 'variable global


        End If

        value = MyJSON.ItemObjectJSON(objRow, "shiftedDeviceSubmitDate")
        If (Not value Is Nothing) Then
            value = value.Substring(0, 10) & " " & value.Substring(11, 8)
            dCabecera.Add("shiftedDeviceSubmitDate", value)

        End If

        objUser = MyJSON.ItemObjectJSON(objRow, "user")

        dCabecera.Add("username", MyJSON.ItemObject(objUser, "username"))

    End Sub

    Private Sub getValuesPage1(ByVal objAnswer As Object)
        Dim objValues As Object
        Dim label, dataType As String
        dPage1.Clear()

        For Each rowAnswer In objAnswer
            'Se deja afuera el titulo de la seccion
            dataType = MyJSON.ItemObject(rowAnswer, "dataType")
            If (dataType <> "Information" And dataType <> "Barcode") Then

                label = MyJSON.ItemObject(rowAnswer, "label")
                objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")

                If (objValues.Length > 0) Then



                    dPage1.Add(label, objValues(0))
                    Console.WriteLine(label)

                End If

            End If
            If (dataType = "Barcode") Then ' Codigo de Barra
                label = MyJSON.ItemObject(rowAnswer, "label")
                objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")
                If (objValues.Length > 0) Then

                    Console.WriteLine()
                    Console.Write(label)

                    dPage1.Add(label, MyJSON.ItemObjectJSON(objValues(0), "data"))




                End If

            End If
        Next
    End Sub

    Private Sub getValuesPage2(ByVal objAnswer As Object)
        Dim objValues, objValuesImage As Object
        Dim label, dataType As String
        dPage2.Clear()

        For Each rowAnswer In objAnswer
            'Se deja afuera el titulo de la seccion
            dataType = MyJSON.ItemObject(rowAnswer, "dataType")
            If (dataType <> "Information" And dataType <> "Barcode" And dataType <> "Image") Then

                label = MyJSON.ItemObject(rowAnswer, "label")
                objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")

                If (objValues.Length > 0) Then
                    dPage2.Add(label, objValues(0))
                    Console.WriteLine()
                    Console.Write(label)

                End If
            Else
                If (dataType = "Image") Then ' fotografia
                    objValuesImage = MyJSON.ItemObjectJSON(rowAnswer, "values")

                    dPage2.Add(MyJSON.ItemObject(rowAnswer, "label"), "img_" + MyJSON.ItemObject(rowAnswer, "label"))

                    Dim dPage_Foto As New Dictionary(Of String, String)
                    For Each o As Object In objValuesImage

                        dPage_Foto.Clear()



                        dPage_Foto.Add("label", MyJSON.ItemObject(rowAnswer, "label"))
                        dPage_Foto.Add("identifier_foto", MyJSON.ItemObjectJSON(o, "identifier"))
                        dPage_Foto.Add("filename", MyJSON.ItemObjectJSON(o, "filename"))
                        dPage_Foto.Add("contentType", MyJSON.ItemObjectJSON(o, "contentType"))
                        dPage_Foto.Add("bytes", MyJSON.ItemObjectJSON(o, "bytes"))



                    Next

                    dPage2_Fotos.Add(dPage_Foto)


                End If
                If (dataType = "Barcode") Then ' Codigo de Barra
                    label = MyJSON.ItemObject(rowAnswer, "label")
                    objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")
                    If (objValues.Length > 0) Then

                        Console.WriteLine()
                        Console.Write(label)

                        dPage2.Add(label, MyJSON.ItemObjectJSON(objValues(0), "data"))




                    End If



                End If
            End If

        Next
    End Sub

    Private Sub getValuesPage3(ByVal objAnswer As Object)
        Dim objValues, objValuesImage As Object
        Dim label, dataType As String
        dPage3.Clear()

        For Each rowAnswer In objAnswer
            'Se deja afuera el titulo de la seccion
            dataType = MyJSON.ItemObject(rowAnswer, "dataType")
            If (dataType <> "Information" And dataType <> "Barcode" And dataType <> "Image") Then

                label = MyJSON.ItemObject(rowAnswer, "label")
                objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")

                If (objValues.Length > 0) Then
                    dPage3.Add(label, objValues(0))
                    Console.WriteLine()
                    Console.Write(label)

                End If
            Else
                If (dataType = "Image") Then ' fotografia
                    objValuesImage = MyJSON.ItemObjectJSON(rowAnswer, "values")


                    dPage3.Add(MyJSON.ItemObject(rowAnswer, "label"), "img_" + MyJSON.ItemObject(rowAnswer, "label"))

                    Dim dPage_Foto As New Dictionary(Of String, String)
                    For Each o As Object In objValuesImage

                        dPage_Foto.Clear()

                        dPage_Foto.Add("label", MyJSON.ItemObject(rowAnswer, "label"))
                        dPage_Foto.Add("identifier_foto", MyJSON.ItemObjectJSON(o, "identifier"))
                        dPage_Foto.Add("filename", MyJSON.ItemObjectJSON(o, "filename"))
                        dPage_Foto.Add("contentType", MyJSON.ItemObjectJSON(o, "contentType"))
                        dPage_Foto.Add("bytes", MyJSON.ItemObjectJSON(o, "bytes"))



                    Next

                    dPage3_Fotos.Add(dPage_Foto)


                End If
                If (dataType = "Barcode") Then ' Codigo de Barra
                    label = MyJSON.ItemObject(rowAnswer, "label")
                    objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")
                    If (objValues.Length > 0) Then

                        Console.WriteLine()
                        Console.Write(label)

                        dPage3.Add(label, MyJSON.ItemObjectJSON(objValues(0), "data"))




                    End If



                End If
            End If

        Next
    End Sub

    Private Sub getValuesPageAndImages(ByVal objAnswer As Object, ByVal dPage As Dictionary(Of String, String), ByVal dPages_fotos As List(Of Dictionary(Of String, String)))
        Dim objValues, objValuesImage As Object
        Dim label, dataType As String
        dPage.Clear()

        For Each rowAnswer In objAnswer
            'Se deja afuera el titulo de la seccion
            dataType = MyJSON.ItemObject(rowAnswer, "dataType")
            If (dataType <> "Information" And dataType <> "Barcode" And dataType <> "Image" And dataType <> "Signature") Then

                label = MyJSON.ItemObject(rowAnswer, "label")
                objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")

                If (objValues.Length > 0) Then
                    dPage.Add(label, objValues(0))
                    Console.WriteLine()
                    Console.Write(label)

                End If
            Else
                If (dataType = "Image" Or dataType = "Signature") Then ' fotografia
                    objValuesImage = MyJSON.ItemObjectJSON(rowAnswer, "values")


                    dPage.Add(MyJSON.ItemObject(rowAnswer, "label"), "img_" + MyJSON.ItemObject(rowAnswer, "label"))

                    Dim dPage_Foto As New Dictionary(Of String, String)
                    For Each o As Object In objValuesImage

                        dPage_Foto.Clear()

                        dPage_Foto.Add("label", MyJSON.ItemObject(rowAnswer, "label"))
                        dPage_Foto.Add("identifier_foto", MyJSON.ItemObjectJSON(o, "identifier"))
                        dPage_Foto.Add("filename", MyJSON.ItemObjectJSON(o, "filename"))
                        dPage_Foto.Add("contentType", MyJSON.ItemObjectJSON(o, "contentType"))
                        dPage_Foto.Add("bytes", MyJSON.ItemObjectJSON(o, "bytes"))


                        dPages_fotos.Add(dPage_Foto) ' agrego cada foto al arreglo de fotos para la bd
                    Next




                End If

                If (dataType = "Barcode") Then ' Codigo de Barra
                    label = MyJSON.ItemObject(rowAnswer, "label")
                    objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")
                    If (objValues.Length > 0) Then

                        Console.WriteLine()
                        Console.Write(label)

                        dPage.Add(label, MyJSON.ItemObjectJSON(objValues(0), "data"))




                    End If



                End If
            End If

        Next
    End Sub


    Private Sub getValuesPage4(ByVal objAnswer As Object)
        Dim objValues, objValuesImage As Object
        Dim label, dataType As String
        dPage4.Clear()

        For Each rowAnswer In objAnswer
            'Se deja afuera el titulo de la seccion
            dataType = MyJSON.ItemObject(rowAnswer, "dataType")
            If (dataType <> "Information" And dataType <> "Barcode" And dataType <> "Image") Then

                label = MyJSON.ItemObject(rowAnswer, "label")
                objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")

                If (objValues.Length > 0) Then
                    dPage4.Add(label, objValues(0))
                    Console.WriteLine(label)
                End If
            Else
                If (dataType = "Image") Then ' fotografia
                    objValuesImage = MyJSON.ItemObjectJSON(rowAnswer, "values")


                    dPage4.Add(MyJSON.ItemObject(rowAnswer, "label"), "img_" + MyJSON.ItemObject(rowAnswer, "label"))

                    Dim dPage_Foto As New Dictionary(Of String, String)
                    For Each o As Object In objValuesImage

                        dPage_Foto.Clear()

                        dPage_Foto.Add("label", MyJSON.ItemObject(rowAnswer, "label"))
                        dPage_Foto.Add("identifier_foto", MyJSON.ItemObjectJSON(o, "identifier"))
                        dPage_Foto.Add("filename", MyJSON.ItemObjectJSON(o, "filename"))
                        dPage_Foto.Add("contentType", MyJSON.ItemObjectJSON(o, "contentType"))
                        dPage_Foto.Add("bytes", MyJSON.ItemObjectJSON(o, "bytes"))



                    Next

                    dPage4_Fotos.Add(dPage_Foto)


                End If
            End If

        Next
    End Sub

    Private Sub getValuesPage5(ByVal objAnswer As Object)
        Dim objValues As Object
        Dim label, dataType As String
        dPage5.Clear()

        For Each rowAnswer In objAnswer

            'Se deja afuera el titulo de la seccion
            dataType = MyJSON.ItemObject(rowAnswer, "dataType")
            If (dataType <> "Information" And dataType <> "Barcode") Then

                label = MyJSON.ItemObject(rowAnswer, "label")
                objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")
                If (objValues.Length > 0) Then
                    Select Case label
                        Case "FíadlCeS3" 'Fotografia
                            dPage5.Add(label, MyJSON.ItemObjectJSON(objValues(0), "filename"))
                            Exit Select
                        Case Else
                            dPage5.Add(label, objValues(0))
                    End Select
                End If

            End If

        Next
    End Sub

    Private Sub getValuesPage6(ByVal objAnswer As Object)
        Dim objValues As Object
        Dim label, dataType As String
        dPage6.Clear()

        For Each rowAnswer In objAnswer

            'Se deja afuera el titulo de la seccion
            dataType = MyJSON.ItemObject(rowAnswer, "dataType")
            If (dataType <> "Information" And dataType <> "Barcode") Then

                label = MyJSON.ItemObject(rowAnswer, "label")
                objValues = MyJSON.ItemObjectJSON(rowAnswer, "values")
                If (objValues.Length > 0) Then
                    Select Case label
                        Case "FíadlCeS4" 'Fotografia
                            dPage6.Add(label, MyJSON.ItemObjectJSON(objValues(0), "filename"))
                            Exit Select
                        Case Else
                            dPage6.Add(label, objValues(0))
                    End Select
                End If

            End If

        Next
    End Sub



#End Region

#Region "Grabar"

    Private Sub grabarDatos()
        Dim iPage As Integer = 1

        'Validaciones
        If (validarIdFormularioExista(dCabecera)) Then
            Throw New Exception("El Json no registra ID Formulario")
        End If
        If (validarshiftedDeviceSubmitDateExista(dCabecera)) Then
            Throw New Exception("EL json no registra shifted Device Submit Date")
        End If


        If (validarIdFormularioDuplicado(dCabecera)) Then
            Throw New Exception("El ID Formulario se encuentra duplicado")
        End If

        If (validarCampoVacio(dPage1("RUT"))) Then
            Throw New Exception("La página 1 debe contener información en el campo Rut")
        End If

        If (validarCampoVacio(dPage1("Embarque"))) Then
            Throw New Exception("La página 1 debe contener información en el campo Embarque")
        End If
        If (validarCampoVacio(dPage1("Sldd"))) Then
            Throw New Exception("La página 1 debe contener información en el campo Sldd")
        End If





        'Grabo datos de cabecera
        grabarDatosCabecera(dCabecera)

        'Grabo datos de las Paginas
        grabarDatosPagina(dPage1, iPage)

        'por cada imagen se debe recorrer el arreglo en caso de traer fotos, debe ser antes 
        'de grabardatos porque ahí aumenta el autonumerico iPage
        For Each registroImagen As Object In dPage2_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage2, iPage)

        For Each registroImagen As Object In dPage3_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage3, iPage)


        For Each registroImagen As Object In dPage4_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage4, iPage)

        For Each registroImagen As Object In dPage5_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage5, iPage)


        For Each registroImagen As Object In dPage6_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage6, iPage)

        For Each registroImagen As Object In dPage7_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage7, iPage)

        For Each registroImagen As Object In dPage8_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage8, iPage)

        For Each registroImagen As Object In dPage9_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage9, iPage)

        For Each registroImagen As Object In dPage10_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage10, iPage)

        For Each registroImagen As Object In dPage11_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage11, iPage)

        For Each registroImagen As Object In dPage12_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage12, iPage)

        For Each registroImagen As Object In dPage13_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage13, iPage)

        For Each registroImagen As Object In dPage14_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage14, iPage)


        For Each registroImagen As Object In dPage15_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage15, iPage)

        For Each registroImagen As Object In dPage16_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage16, iPage)

        For Each registroImagen As Object In dPage17_Fotos
            guardarImagenesPagina(registroImagen, iPage)
        Next

        grabarDatosPagina(dPage17, iPage)


    End Sub

    Private Sub grabarDatosCabecera(ByVal dCampos As Dictionary(Of String, String))

        If (dCampos.Count > 0) Then



            Dim queryString As String = ""
            queryString &= "INSERT INTO FormCargaCabeceraMX ("
            For Each campo In dCampos
                queryString &= "[" & campo.Key & "],"
            Next
            'le agrego el campo de fecha
            queryString &= "[FECHACREACION]"

            'queryString = queryString.Remove(queryString.Length - 1, 1) 'remover ultima coma

            queryString &= ") VALUES("
            For Each campo In dCampos
                queryString &= "'" & campo.Value & "',"
            Next

            queryString &= "(select dbo.fnu_GETDATE())"
            'queryString = queryString.Remove(queryString.Length - 1, 1) 'remover ultima coma 
            queryString &= ")"


            'Ejecuta la query
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.Text, queryString)
        End If

    End Sub

    Private Sub grabarDatosPagina(ByVal dCampos As Dictionary(Of String, String), ByRef iPage As Integer)
        Dim queryString As String = ""

        If (dCampos.Count > 0) Then

            queryString &= "INSERT INTO FormCargaPagina" & iPage & " ("
            For Each campo In dCampos
                queryString &= "[" & campo.Key & "],"
            Next
            queryString &= "[identifier]"

            queryString &= ") VALUES("
            For Each campo In dCampos
                queryString &= "'" & campo.Value & "',"
            Next
            queryString &= "'" & identifier & "'"
            queryString &= ")"
            '    If (iPage > 11) Then

            'Console.WriteLine(queryString)
            '   Console.ReadLine()
            ' End If

            'Ejecuta la query
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.Text, queryString)

        End If

        iPage += 1

    End Sub

    Private Sub guardarImagenesPagina(ByVal dCampos As Dictionary(Of String, String), ByRef iPage As Integer)
        Dim queryString As String = ""

        If (dCampos.Count > 0) Then

            queryString &= "INSERT INTO FormCargaImagenPagina" & iPage & " ("
            For Each campo In dCampos
                queryString &= "[" & campo.Key & "],"
            Next
            queryString &= "[identifier]"

            queryString &= ") VALUES("
            For Each campo In dCampos

                queryString &= "'" & campo.Value.Replace("'", "''") & "'," ' le agrego un replace ' por '' porque el string64 puede traerlo
            Next
            queryString &= "'" & identifier & "'"
            queryString &= ")"

            '  Console.WriteLine(queryString)
            '  Console.ReadLine()

            'Ejecuta la query
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.Text, queryString)

        End If

        ' iPage += 1

    End Sub

#End Region

#Region "Metodos Internos"

    'Private Sub descargarArchivosFTP()
    '  'Dim ftp As New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
    '  Dim ftpContenidoDirectorio, ftpListadoArchivos As FTPdirectory

    '  Try

    '    msgLog &= "</BR>-->Descargando archivos .json desde FTP "

    '    'Crea el dicrectorio
    '    If (Not Directory.Exists(RUTA_CARPETA_SERVIDOR_TMP)) Then
    '      Directory.CreateDirectory(RUTA_CARPETA_SERVIDOR_TMP)
    '    End If

    '    'obtiene contenido del directorio
    '    ftpContenidoDirectorio = ftp.ListDirectoryDetail(RUTA_CARPETA_FTP)

    '    'filtra los archivos por la extension
    '    ftpListadoArchivos = ftpContenidoDirectorio.GetFiles("json")

    '    'descarga el archivo
    '    For Each file As FTPfileInfo In ftpListadoArchivos
    '      If (file.Filename.StartsWith(STARTS_WITH)) Then
    '        ftp.Download(file, RUTA_CARPETA_SERVIDOR_TMP & file.Filename, True)
    '      End If
    '    Next
    '    msgLog &= " [Exitoso]"

    '  Catch ex As Exception
    '    msgLog &= " [Error: " & ex.Message.ToString() & "]"
    '  End Try
    'End Sub

    Private Sub moverArchivos(ByVal archivo As FileInfo, ByVal destino As String)
        Try

            msgLog &= "</BR>--> Moviendo '" & archivo.Name & "' a " + destino + " "

            'Si no existe el directorio se crea
            If (Not Directory.Exists(destino)) Then
                Directory.CreateDirectory(destino)
            End If

            'si existe el archivo en el destino se borra
            If (File.Exists(destino & archivo.Name)) Then
                File.Delete(destino & archivo.Name)
            End If

            archivo.MoveTo(destino & archivo.Name)

            msgLog &= "[Exitoso]"

        Catch ex As Exception
            msgLog &= " [Error: " & ex.Message.ToString() & "]"
        End Try

    End Sub

    Private Sub envioEmailError(ByVal errores As String)
        Try
            Dim ColeccionEmailsPara, ColeccionEmailsCC As System.Net.Mail.MailAddressCollection
            Dim listadoMailPara As String = ""
            Dim listadoMailCC As String = ""
            Dim asunto As String
            Dim bodyMensaje, msgProceso As String
            Dim dsConfiguracionXML As New DataSet

            msgLog &= "</BR>--> Enviando Email "

            'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
            dsConfiguracionXML = Herramientas.LeerXML("configuracion.xml")

            bodyMensaje = dsConfiguracionXML.Tables(eTablaConfiguracion.MailBody).Rows(0).Item("texto")
            bodyMensaje = bodyMensaje.Replace("{$FORMULARIOS_ERROR}", errores)
            bodyMensaje = bodyMensaje.Replace("{$BR}", vbCrLf)

            If Herramientas.MailModoDebug Then
                listadoMailPara = Herramientas.MailPruebas()
                ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
                ColeccionEmailsCC = Nothing
            Else
                'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
                listadoMailPara = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailPara, "email")
                listadoMailCC = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailCC, "email")
                listadoMailCC &= IIf(String.IsNullOrEmpty(listadoMailCC), Herramientas.MailAdministrador(), ";" & Herramientas.MailAdministrador())
                'construye las colecciones
                ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
                ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailCC)
            End If

            'envia el mail y retorna mensaje de exito(mensaje vacio) o de error
            asunto = dsConfiguracionXML.Tables(eTablaConfiguracion.MailAsunto).Rows(0).Item("texto")
            msgProceso = Mail.Enviar(Constantes.mailHost, Constantes.mailEmailEmisor, Constantes.mailPasswordEmailEmisor, Constantes.mailEmailNombre, ColeccionEmailsPara, asunto, bodyMensaje, False, ColeccionEmailsCC, False)

            'limpia variable para los nuevos email
            ColeccionEmailsPara = Nothing
            ColeccionEmailsCC = Nothing

            If msgProceso <> "" Then
                msgLog &= "[Error: " & msgProceso & "]"
            Else
                msgLog &= " Para: " & listadoMailPara & " CC: " & listadoMailCC & " [Exitoso]"
            End If

        Catch ex As Exception
            msgLog &= " [Error: " & ex.Message.ToString() & "]"
        End Try

    End Sub
#End Region

#Region "Validaciones"

    Private Function validarIdFormularioExista(ByVal dCampos As Dictionary(Of String, String)) As Boolean
        Return (Not dCampos.ContainsKey("identifier"))
    End Function



    Private Function validarshiftedDeviceSubmitDateExista(ByVal dCampos As Dictionary(Of String, String)) As Boolean
        Return (Not dCampos.ContainsKey("shiftedDeviceSubmitDate"))
    End Function


    Private Function validarCampoVacio(ByVal campo As String) As Boolean

        Return (String.IsNullOrEmpty(campo))

    End Function





    Private Function validarNumEnvioExista(ByVal dCampos As Dictionary(Of String, String)) As Boolean
        If (Not dCampos.ContainsKey("ND") And Not dCampos.ContainsKey("ND 1")) Then
            Return True
        End If

        If (dCampos("ND") = "0" And dCampos("ND 1") = "0") Then
            Return True
        End If

        Return False
    End Function



    Private Function validarIdFormularioDuplicado(ByVal dCampos As Dictionary(Of String, String)) As Boolean

        Dim idFormulario, filas As Integer
        Dim queryString As String

        idFormulario = dCampos("identifier")
        queryString = "SELECT COUNT(1) FROM FormCargaCabeceraMX WHERE IDENTIFIER = " & idFormulario

        'Ejecuta la query
        filas = SqlHelper.ExecuteScalar(Conexion.StringConexion, CommandType.Text, queryString)

        If (filas > 0) Then
            Return True
        Else
            queryString = "SELECT COUNT(1) FROM FRMCargaWM  WHERE id_Formulario = " & idFormulario
            filas = SqlHelper.ExecuteScalar(Conexion.StringConexion, CommandType.Text, queryString)

            If (filas > 0) Then
                Return True
            Else
                Return False
            End If
        End If

    End Function

#End Region

End Module

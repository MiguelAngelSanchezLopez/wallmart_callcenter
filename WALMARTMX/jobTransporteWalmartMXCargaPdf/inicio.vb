﻿Imports System.IO
Imports CapaNegocio
Imports System.Data.SqlClient

'' Add's Externos
Imports Microsoft.ApplicationBlocks.Data

Module inicio

    Dim logMessage As String

#Region "Constantes"
    Private Const RUTA_CARPETA_FTP As String = "/ftp-walmart-mx/CarrierInforme/"
    Private Const RUTA_CARPETA_FTP_ERRORES As String = RUTA_CARPETA_FTP & "Errores/"
    Private Const RUTA_CARPETA_FTP_PROCESADOS As String = RUTA_CARPETA_FTP & "Procesados/"
    Private Const RUTA_CARPETA_FTP_LOG As String = RUTA_CARPETA_FTP & "Log/"
    Private Const RUTA_CARPETA_SERVIDOR_TMP As String = "Temp"

    Private Const filter As String = "*.pdf"
    Private Const jobName As String = "jobTransporteWalmartMXCargaPdf"
#End Region

    Sub Main()
        Dim logFileName As String
        Dim status As Boolean
        Dim countError As Integer

        logMessage = "Tarea: " & jobName & "</BR>"
        logMessage &= "Ambiente: " & Herramientas.ObtenerAmbienteEjecucion & "</BR>"
        logMessage &= "Modo Debug: " & Herramientas.MailModoDebug & "</BR>"

        Console.WriteLine("Tarea: " & jobName)
        Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
        Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug)

        'Creo carpeta destino
        If (Not Directory.Exists(RUTA_CARPETA_SERVIDOR_TMP)) Then
            Directory.CreateDirectory(RUTA_CARPETA_SERVIDOR_TMP)
        End If

        'Obtengo archivos del FTP
        downloadFileFtp()

        'Obtengo archivos del servidor
        Dim folder As New DirectoryInfo(RUTA_CARPETA_SERVIDOR_TMP)
        Dim files As FileInfo() = folder.GetFiles(filter)
        If (files.Count > 0) Then

            For Each file As FileInfo In files

                status = readFile(file)

                'si no existe error
                If (status) Then
                    moverArchivos(RUTA_CARPETA_FTP & file.Name, RUTA_CARPETA_FTP_PROCESADOS & file.Name)
                Else
                    countError += 1

                    moverArchivos(RUTA_CARPETA_FTP & file.Name, RUTA_CARPETA_FTP_ERRORES & file.Name)
                End If

                'Se elimina archivo descargado
                file.Delete()
            Next

            'Manejo de errores
            If (countError = 0) Then
                logFileName = "AltoTrackWM_MX_InformacionTracto_EJECUTADO_CON_EXITO_" & Herramientas.MyNow().ToString("yyyyMMddHHmmss")
            Else
                logFileName = "AltoTrackWM_MX_InformacionTracto_EJECUTADO_CON_ERRORES_" & Herramientas.MyNow().ToString("yyyyMMddHHmmss")
            End If

            'Manejo de archivo Log
            Dim pathLog As String = Archivo.CrearArchivoTextoEnDisco("<html><head></head><body>" & logMessage & "</body></html>", RUTA_CARPETA_SERVIDOR_TMP, jobName, logFileName, "html")
            uploadFileFtp(pathLog, RUTA_CARPETA_FTP_LOG & logFileName & ".html")
            System.IO.File.Delete(pathLog)
        End If

    End Sub

    Private Function readFile(ByVal file As FileInfo) As Boolean
        Try
            logMessage &= Herramientas.MyNow() & " --> Leyendo archivo '" & file.Name & "'</BR>"


            Dim AliasTrans, Fecha, TipoInforme As String

            Dim ArrStr() As String = file.Name.Split("_")

            AliasTrans = ArrStr(0)
            Fecha = ArrStr(1)
            TipoInforme = ArrStr(2).Replace(file.Extension, "")

            Dim arParms() As SqlParameter = New SqlParameter(3) {}
            arParms(0) = New SqlParameter("@SiglaTransportista", SqlDbType.VarChar)
            arParms(0).Value = AliasTrans
            arParms(1) = New SqlParameter("@Fecha", SqlDbType.VarChar)
            arParms(1).Value = Fecha
            arParms(2) = New SqlParameter("@TipoInforme", SqlDbType.VarChar)
            arParms(2).Value = TipoInforme

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, "spu_job_Carrier_CargaPDF", arParms)
            Dim IdRegistro As Integer = Integer.Parse(ds.Tables(0).Rows(0)("IdRegistro").ToString())

            GrabaContenido(file, IdRegistro, -1, 1, "CarrierInforme_PDF")

            Return True

        Catch ex As Exception
            logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
            Return False
        End Try
    End Function

    Private Function GrabaContenido(ByVal pFile As FileInfo, ByVal IdInforme As String, ByRef idArchivo As String, ByVal idUsuario As String, ByVal Entidad As String) As String
        Dim nombreArchivo, ruta, extension, ContentType As String
        ' Dim oUtilidades As New Utilidades
        Dim tamano, orden As Integer
        'Dim maxTamanoArchivoEnKb As Integer = Archivo.TamanoMaximoUploadEnKb()
        Dim tamanoEnKb As Integer
        Dim oRedimension As New Archivo.Redimension
        Dim anchoMax, altoMax As Integer
        Dim dicArchivo As New Dictionary(Of String, Integer)
        Dim rtn As String = String.Empty
        Try
            tamano = pFile.Length ' postedFile.ContentLength
            tamanoEnKb = (tamano / 1024)
            anchoMax = 1024
            altoMax = 0

            If (tamano > 0) Then
                ruta = pFile.FullName
                nombreArchivo = pFile.Name
                extension = Archivo.ObtenerExtension(nombreArchivo)
                ContentType = "application/pdf"
                orden = 1

                'lee el archivo en el arreglo de arreglo de Byte Contenido
                Dim Contenido() As Byte = File.ReadAllBytes(pFile.FullName)

                If Archivo.EsTipoArchivo(ContentType) = Archivo.eTipoArchivo.image Then
                    'asigna valores originales
                    With oRedimension
                        .Contenido = Contenido
                        .ContentType = ContentType
                        .Extension = extension
                        .NombreArchivo = nombreArchivo
                        .Tamano = tamano
                    End With
                    Contenido = Archivo.RedimensionarImagen2Bytes(Contenido, anchoMax, altoMax, False, oRedimension)
                    'asigna valores despues de la redimension
                    With oRedimension
                        tamano = .Tamano
                        nombreArchivo = .NombreArchivo
                        extension = .Extension
                        ContentType = .ContentType
                    End With
                End If

                'guarda archivo
                idArchivo = Archivo.GrabarArchivoEnBD(idUsuario, nombreArchivo, ruta, tamano, extension, ContentType, Contenido, orden, idArchivo)

                If idArchivo <> -200 Then
                    Archivo.AsociarIdArchivoConIdRegistro(Entidad, IdInforme, idArchivo)
                End If


            End If
        Catch ex As Exception
            idArchivo = -200
        End Try
        Return rtn
    End Function

    Private Sub downloadFileFtp()
        Try

            logMessage &= Herramientas.MyNow() & " --> Descargando archivos desde FTP</BR>"

            Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
            Dim dt As New DataTable
            Dim listDirectory, listFileFtp As FTPdirectory

            'obtiene contenido del directorio
            listDirectory = ftp.ListDirectoryDetail(RUTA_CARPETA_FTP)

            'filtra los archivos por la extension
            listFileFtp = listDirectory.GetFiles("pdf")

            'obtiene los archivos del directorio
            For Each file As FTPfileInfo In listFileFtp

                Dim pathFileSource As String = RUTA_CARPETA_FTP & file.Filename
                Dim pathFileDestination As String = RUTA_CARPETA_SERVIDOR_TMP & "\" & file.Filename

                logMessage &= Herramientas.MyNow() & " --> Descargando " & file.Filename & "</BR>"
                ftp.Download(pathFileSource, pathFileDestination)
                logMessage &= Herramientas.MyNow() & " --> Exitoso</BR>"

            Next

        Catch ex As Exception
            logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
        End Try

    End Sub

    Private Sub uploadFileFtp(ByVal pathFile As String, ByVal destinationFile As String)
        Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
        ftp.Upload(pathFile, destinationFile)
    End Sub

    Private Sub moverArchivos(ByVal source As String, ByVal destination As String)
        Try

            logMessage &= Herramientas.MyNow() & " --> Moviendo desde """ & source & """ a """ + destination + """</BR>"

            Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
            ftp.FtpRename(source, destination)

            logMessage &= Herramientas.MyNow() & " --> Exitoso</BR>"

        Catch ex As Exception
            logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
        End Try

    End Sub
End Module

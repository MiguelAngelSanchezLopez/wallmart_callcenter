﻿Imports System.IO
Imports CapaNegocio
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic.FileIO


Module inicio

  Dim logMessage As String

#Region "Constantes"
  Private Const RUTA_CARPETA_FTP As String = "/ftp-walmart-mx/Tracto/"
  Private Const RUTA_CARPETA_FTP_ERRORES As String = RUTA_CARPETA_FTP & "Errores/"
  Private Const RUTA_CARPETA_FTP_PROCESADOS As String = RUTA_CARPETA_FTP & "Procesados/"
  Private Const RUTA_CARPETA_FTP_LOG As String = RUTA_CARPETA_FTP & "Log/"
  Private Const RUTA_CARPETA_SERVIDOR_TMP As String = "Temp"

  Private Const filter As String = "*.csv"
  Private Const jobName As String = "jobTransporteWalmartMXCargaTracto"
#End Region

  Sub Main()
    Dim logFileName As String
    Dim status As Boolean
    Dim countError As Integer

    logMessage = "Tarea: " & jobName & "</BR>"
    logMessage &= "Ambiente: " & Herramientas.ObtenerAmbienteEjecucion & "</BR>"
    logMessage &= "Modo Debug: " & Herramientas.MailModoDebug & "</BR>"

    Console.WriteLine("Tarea: " & jobName)
    Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
    Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug)

    'Creo carpeta destino
    If (Not Directory.Exists(RUTA_CARPETA_SERVIDOR_TMP)) Then
      Directory.CreateDirectory(RUTA_CARPETA_SERVIDOR_TMP)
    Else
      Directory.Delete(RUTA_CARPETA_SERVIDOR_TMP, True)
      Directory.CreateDirectory(RUTA_CARPETA_SERVIDOR_TMP)
    End If

    'Creo carpeta destino
    If (Not Directory.Exists("Temp_Excel")) Then
      Directory.CreateDirectory("Temp_Excel")
    Else
      Directory.Delete("Temp_Excel", True)
      Directory.CreateDirectory("Temp_Excel")
    End If

    'Obtengo archivos del FTP
    downloadFileFtp()

    'Obtengo archivos del servidor
    Dim folder As New DirectoryInfo(RUTA_CARPETA_SERVIDOR_TMP)
    Dim files As FileInfo() = folder.GetFiles(filter)
    If (files.Count > 0) Then

      For Each file As FileInfo In files

        status = readFile(file)

        'si no existe error
        If (status) Then
          moverArchivos(RUTA_CARPETA_FTP & file.Name, RUTA_CARPETA_FTP_PROCESADOS & file.Name)
        Else
          countError += 1

          moverArchivos(RUTA_CARPETA_FTP & file.Name, RUTA_CARPETA_FTP_ERRORES & file.Name)
        End If

        'Se elimina archivo descargado
        file.Delete()
      Next

      'Manejo de errores
      If (countError = 0) Then
        logFileName = "AltoTrackWM_MX_InformacionTracto_EJECUTADO_CON_EXITO_" & Herramientas.MyNow().ToString("yyyyMMddHHmmss")
      Else
        logFileName = "AltoTrackWM_MX_InformacionTracto_EJECUTADO_CON_ERRORES_" & Herramientas.MyNow().ToString("yyyyMMddHHmmss")
      End If

      'Manejo de archivo Log
      Dim pathLog As String = Archivo.CrearArchivoTextoEnDisco("<html><head></head><body>" & logMessage & "</body></html>", RUTA_CARPETA_SERVIDOR_TMP, jobName, logFileName, "html")
      uploadFileFtp(pathLog, RUTA_CARPETA_FTP_LOG & logFileName & ".html")
      System.IO.File.Delete(pathLog)
    End If

  End Sub

  Private Function readFile(ByVal file As FileInfo) As Boolean
    Try
      Dim conStr As String = ""
      Dim cmdExcel As New OleDbCommand()
      Dim oda As New OleDbDataAdapter()
      Dim dtExcel As New DataTable()



      Dim newName As String = "Temp_Excel" + "\" + file.Name
      System.IO.File.Copy(file.FullName, newName, True)
      conStr = String.Format(conStr, newName)

      Dim table As DataTable = GetTable()

      Using parser As New TextFieldParser(newName)
        parser.TextFieldType = FieldType.Delimited
        parser.SetDelimiters(",")
        Dim cabecera As Boolean = True
        While Not parser.EndOfData
          'Processing row
          Dim fields As String() = parser.ReadFields()
          'TODO: Process field
          If (Not cabecera) Then
            Dim Row1 As DataRow
            Row1 = table.NewRow()
            Dim cnt = 0
            For Each field As String In fields
              If (cnt < table.Columns.Count) Then
                Row1.Item(cnt) = field
                cnt += 1
              End If
            Next
            table.Rows.Add(Row1)
          End If
          cabecera = False
        End While
      End Using

      logMessage &= Herramientas.MyNow() & " --> Leyendo archivo '" & file.Name & "'</BR>"

      Dim retn As Boolean = cargarArchivoBD(table, file)

      logMessage &= Herramientas.MyNow() & " --> Exitoso</BR>"
      Return retn

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
      Return False
    End Try
  End Function

  Private Function cargarArchivoBD(ByVal Dt As DataTable, ByVal file As FileInfo) As Boolean

    Dim FileName As String = file.Name 'Path.GetFileName(fileCarga.PostedFile.FileName)
    Dim extension As String = file.Extension ' Path.GetExtension(fileCarga.PostedFile.FileName)
    'Dim rootPath As String = Utilidades.RutaLogsSitio & "\" & NOMBRE_CARPETA_CARGA
    Dim filePath As String = file.FullName ' rootPath + "\" + FileName
    Dim listadoErrores As New List(Of String)

    Try

      'crea carpeta si es que no existe
      ' Archivo.CrearDirectorioEnDisco(rootPath)

      If (extension.ToLower() <> ".csv") Then
        logMessage &= Herramientas.MyNow() & "{ERROR} El tipo de archivo no es v&aacute;lido, debe ser un archivo *.csv"
      Else

        Dim stopWatch As New Stopwatch()
        stopWatch.Start()

        'Cargo los nuevos datos
        Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(Conexion.StringConexion)
          bulkCopy.DestinationTableName = "dbo.InformacionTractoTmp"
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ID_Master", "ID_Master"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Nave_Viaje", "Nave_Viaje"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Determinante_Gasto", "Determinante_Gasto"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Determinante_WMS_Viaje", "Determinante_WMS_Viaje"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tipo_Viaje", "Tipo_Viaje"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ID_Unidad_Viaje", "ID_Unidad_Viaje"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tipo_Flota", "Tipo_Flota"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Linea", "Linea"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tipo_Unidad_Viaje", "Tipo_Unidad_Viaje"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Operador", "Operador"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Carta_Porte", "Carta_Porte"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ID_GTS", "ID_GTS"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ID_Embarque", "ID_Embarque"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Nave_Embarque", "Nave_Embarque"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Determinante_WMS_Embarque", "Determinante_WMS_Embarque"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Embarque_GLS", "Embarque_GLS"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Cortina", "Cortina"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tipo_Servicio", "Tipo_Servicio"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tipo_Entrega", "Tipo_Entrega"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Numero_Entregas", "Numero_Entregas"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ID_Unidad_Embarque", "ID_Unidad_Embarque"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tipo_Flota_Embarque", "Tipo_Flota_Embarque"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Linea_Embarque", "Linea_Embarque"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tipo_Unidad_Embarque", "Tipo_Unidad_Embarque"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Prioridad_Entrega", "Prioridad_Entrega"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Destino", "Destino"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Factura_GLS", "Factura_GLS"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tarimas_Chep", "Tarimas_Chep"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tarimas_Blancas", "Tarimas_Blancas"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Tarimas_Desarmadas", "Tarimas_Desarmadas"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Vendor_Pack", "Vendor_Pack"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Warehouse_Pack", "Warehouse_Pack"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Marchamo1", "Marchamo1"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Marchamo2", "Marchamo2"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Marchamo3", "Marchamo3"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Solicitud", "Fecha_Solicitud"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Enrampe", "Fecha_Enrampe"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Apertura", "Fecha_Apertura"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Cierre", "Fecha_Cierre"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Retiro", "Fecha_Retiro"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Cita", "Fecha_Cita"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Generacion", "Fecha_Generacion"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Despacho", "Fecha_Despacho"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Check_In", "Fecha_Check_In"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Fecha_Check_Out", "Fecha_Check_Out"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Solicitud", "Usuario_Solicitud"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Asignacion", "Usuario_Asignacion"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Apertura", "Usuario_Apertura"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Cierre", "Usuario_Cierre"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Retiro", "Usuario_Retiro"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Generacion", "Usuario_Generacion"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Despacho", "Usuario_Despacho"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Check_In", "Usuario_Check_In"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Usuario_Check_Out", "Usuario_Check_Out"))
          bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("Status", "Status"))




          bulkCopy.BulkCopyTimeout = 0
          ' Write from the source to the destination.
          bulkCopy.WriteToServer(Dt)
        End Using

        stopWatch.Stop()

        Dim ts As TimeSpan = stopWatch.Elapsed

        Try
          Sistema.ConsultaGenerica("-1", "-1", "spu_jobTracoTmpToOriginal")
        Catch ex As Exception
          logMessage &= Herramientas.MyNow() & "{ERROR} Al ejecutar procedimiento spu_jobTracoTemporalToOriginal (Tiempo operación " + ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":" + ts.Seconds.ToString() + ")."
        End Try

        logMessage &= Herramientas.MyNow() & "{INGRESADO} La carga fue grabada correctamente (Tiempo operación " + ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":" + ts.Seconds.ToString() + ")."

        Return True


      End If

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & "{ERROR}" + ex.Message + "."
    End Try

    Return False

  End Function

  Private Function validarDataTable(ByVal dtExcel As DataTable) As Boolean
    Dim columnasQueFaltan As String = ""
    Dim validacionDatosFila As String = ""
    Dim columns As New List(Of String)
    Dim columnsNotFound As New List(Of String)
    columns.Add("Id_Master")
    columns.Add("Tipo_Viaje")
    columns.Add("CT_Tipo_Flota#Tipo_Flota")
    columns.Add("Determinante_WMS")
    columns.Add("Id_Unidad_Viaje")
    columns.Add("CT_Proveedores#Clave_MTS")
    columns.Add("Tipo_Unidad")
    columns.Add("Operador")
    columns.Add("Carta_Porte")
    columns.Add("Id_GTS")
    columns.Add("Id_Embarque")
    columns.Add("Embarque_GLS")
    columns.Add("Tipo_Servicio")
    columns.Add("Tipo_Entrega")
    columns.Add("Numero_Entregas")
    columns.Add("CT_Tipo_Flota_1#Tipo_Flota")
    columns.Add("Id_Unidad_Embarque")
    columns.Add("CT_Proveedores_1#Clave_MTS")
    '' columns.Add("CT_Tipo_Unidad_1#Tipo_Unidad")
    columns.Add("Factura_GLS")
    columns.Add("1")
    columns.Add("2")
    columns.Add("3")
    columns.Add("4")
    columns.Add("5")
    columns.Add("6")
    columns.Add("7")
    columns.Add("8")
    columns.Add("9")
    columns.Add("10")
    columns.Add("11")
    columns.Add("12")
    columns.Add("13")
    columns.Add("14")
    columns.Add("15")
    columns.Add("Fecha_Solicitud")
    columns.Add("Fecha_Enrampe")
    columns.Add("Fecha_Apertura_Embarque")
    columns.Add("Fecha_Cierre_Embarque")
    columns.Add("Fecha_Retiro")
    columns.Add("Fecha_Cita_Asignada")
    columns.Add("Fecha_Generacion")
    columns.Add("Fecha_Despacho")
    columns.Add("CT_Status_Embarques#Status")
    columns.Add("CT_Status_Viajes#Status")

    Try

      'Valida error en la obtención
      If (dtExcel Is Nothing) Then
        logMessage &= Herramientas.MyNow() & "{ERROR} Ha ocurrido un error al intentar obtener los datos del archivo."
        Return False
      End If

      'Valida que existan registros
      If (dtExcel.Rows.Count = 0) Then
        logMessage &= Herramientas.MyNow() & "{ERROR} No se encontraron registros para cargar."
        Return False
      End If

      'Valida cantidad de columnas
      'If dtExcel.Columns.Count <> columns.Count Then
      '    logMessage &= Herramientas.MyNow() & "{ERROR} El n&uacute;mero de columnas no es válido, deben ser " & columns.Count
      '    Return False
      'End If

      'Valida que esten todas las columnas
      For Each nombre As String In columns
        If Not dtExcel.Columns.Contains(nombre) Then
          columnsNotFound.Add(nombre)
        End If
      Next

      If (columnsNotFound.Count > 0) Then
        Dim listadoErrores As New List(Of String)
        listadoErrores.Add("Faltan las siguientes columnas:<br />" & String.Join("-", columnsNotFound.ToArray()))

        '  Session(SESION_LISTADO_ERRORES) = listadoErrores
        logMessage &= Herramientas.MyNow() & "{ERROR} El archivo tiene errores, favor revisar y volver a cargar."

        Return False
      End If

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & "{ERROR} " & ex.Message & "."
      Return False
    End Try

    Return True
  End Function

  Private Sub downloadFileFtp()
    Try

      logMessage &= Herramientas.MyNow() & " --> Descargando archivos desde FTP</BR>"

      Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
      Dim dt As New DataTable
      Dim listDirectory, listFileFtp As FTPdirectory

      'obtiene contenido del directorio
      listDirectory = ftp.ListDirectoryDetail(RUTA_CARPETA_FTP)

      'filtra los archivos por la extension
      listFileFtp = listDirectory.GetFiles("csv")

      'obtiene los archivos del directorio
      For Each file As FTPfileInfo In listFileFtp

        Dim pathFileSource As String = RUTA_CARPETA_FTP & file.Filename
        Dim pathFileDestination As String = RUTA_CARPETA_SERVIDOR_TMP & "\" & file.Filename

        logMessage &= Herramientas.MyNow() & " --> Descargando " & file.Filename & "</BR>"
        ftp.Download(pathFileSource, pathFileDestination)
        logMessage &= Herramientas.MyNow() & " --> Exitoso</BR>"

      Next

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
    End Try

  End Sub

  Private Sub uploadFileFtp(ByVal pathFile As String, ByVal destinationFile As String)
    Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
    ftp.Upload(pathFile, destinationFile)
  End Sub

  Private Sub moverArchivos(ByVal source As String, ByVal destination As String)
    Try

      logMessage &= Herramientas.MyNow() & " --> Moviendo desde """ & source & """ a """ + destination + """</BR>"

      Dim ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)
      ftp.FtpRename(source, destination)

      logMessage &= Herramientas.MyNow() & " --> Exitoso</BR>"

    Catch ex As Exception
      logMessage &= Herramientas.MyNow() & " --> Error [" & ex.Message.ToString() & "]" & "</BR>"
    End Try

  End Sub

  Function GetTable() As DataTable
    ' Create new DataTable instance.
    Dim table As New DataTable

    ' Create four typed columns in the DataTable.
    table.Columns.Add("ID_Master", GetType(String))
    table.Columns.Add("Nave_Viaje", GetType(String))
    table.Columns.Add("Determinante_Gasto", GetType(String))
    table.Columns.Add("Determinante_WMS_Viaje", GetType(String))
    table.Columns.Add("Tipo_Viaje", GetType(String))
    table.Columns.Add("ID_Unidad_Viaje", GetType(String))
    table.Columns.Add("Tipo_Flota", GetType(String))
    table.Columns.Add("Linea", GetType(String))
    table.Columns.Add("Tipo_Unidad_Viaje", GetType(String))
    table.Columns.Add("Operador", GetType(String))
    table.Columns.Add("Carta_Porte", GetType(String))
    table.Columns.Add("ID_GTS", GetType(String))
    table.Columns.Add("ID_Embarque", GetType(String))
    table.Columns.Add("Nave_Embarque", GetType(String))
    table.Columns.Add("Determinante_WMS_Embarque", GetType(String))
    table.Columns.Add("Embarque_GLS", GetType(String))
    table.Columns.Add("Cortina", GetType(String))
    table.Columns.Add("Tipo_Servicio", GetType(String))
    table.Columns.Add("Tipo_Entrega", GetType(String))
    table.Columns.Add("Numero_Entregas", GetType(String))
    table.Columns.Add("ID_Unidad_Embarque", GetType(String))
    table.Columns.Add("Tipo_Flota_Embarque", GetType(String))
    table.Columns.Add("Linea_Embarque", GetType(String))
    table.Columns.Add("Tipo_Unidad_Embarque", GetType(String))
    table.Columns.Add("Prioridad_Entrega", GetType(String))
    table.Columns.Add("Destino", GetType(String))
    table.Columns.Add("Factura_GLS", GetType(String))
    table.Columns.Add("Tarimas_Chep", GetType(String))
    table.Columns.Add("Tarimas_Blancas", GetType(String))
    table.Columns.Add("Tarimas_Desarmadas", GetType(String))
    table.Columns.Add("Vendor_Pack", GetType(String))
    table.Columns.Add("Warehouse_Pack", GetType(String))
    table.Columns.Add("Marchamo1", GetType(String))
    table.Columns.Add("Marchamo2", GetType(String))
    table.Columns.Add("Marchamo3", GetType(String))
    table.Columns.Add("Fecha_Solicitud", GetType(String))
    table.Columns.Add("Fecha_Enrampe", GetType(String))
    table.Columns.Add("Fecha_Apertura", GetType(String))
    table.Columns.Add("Fecha_Cierre", GetType(String))
    table.Columns.Add("Fecha_Retiro", GetType(String))
    table.Columns.Add("Fecha_Cita", GetType(String))
    table.Columns.Add("Fecha_Generacion", GetType(String))
    table.Columns.Add("Fecha_Despacho", GetType(String))
    table.Columns.Add("Fecha_Check_In", GetType(String))
    table.Columns.Add("Fecha_Check_Out", GetType(String))
    table.Columns.Add("Usuario_Solicitud", GetType(String))
    table.Columns.Add("Usuario_Asignacion", GetType(String))
    table.Columns.Add("Usuario_Apertura", GetType(String))
    table.Columns.Add("Usuario_Cierre", GetType(String))
    table.Columns.Add("Usuario_Retiro", GetType(String))
    table.Columns.Add("Usuario_Generacion", GetType(String))
    table.Columns.Add("Usuario_Despacho", GetType(String))
    table.Columns.Add("Usuario_Check_In", GetType(String))
    table.Columns.Add("Usuario_Check_Out", GetType(String))
    table.Columns.Add("Status", GetType(String))
    Return table
  End Function
End Module

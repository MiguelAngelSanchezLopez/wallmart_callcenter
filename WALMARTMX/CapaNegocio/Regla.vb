﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Text
Imports System.Reflection

Public Class Regla
#Region "Properties"
  Public Const NombreEntidad As String = "Regla"

#End Region

#Region "Constructor"
  Public Sub New()
    MyBase.New()
  End Sub

#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene la lista de nodos de un tipo alerta definido
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerAlertasXMLPorTipoAlerta(ByVal xmlDoc As XmlDocument, ByVal tipoAlerta As String) As XmlNodeList
    Dim nodeRaiz, nodeTipoAlerta, nodeAlertas As XmlNodeList

    Try
      nodeRaiz = xmlDoc.GetElementsByTagName("reglas")
      nodeTipoAlerta = CType(nodeRaiz(0), XmlElement).GetElementsByTagName(tipoAlerta)
      nodeAlertas = CType(nodeTipoAlerta(0), XmlElement).GetElementsByTagName("alerta")
      Return nodeAlertas
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  ''' <summary>
  ''' verifica la condicion de ocurrencia de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function CumpleCondicionOcurrencia(ByVal ocurrenciaAlerta As Integer, ByVal xmlOcurrencia As Integer, ByVal xmlCondicion As String) As Boolean
    Dim condicion As Boolean

    Try
      Select Case xmlCondicion.ToLower()
        Case "menor"
          condicion = ocurrenciaAlerta < xmlOcurrencia
        Case "menorigual"
          condicion = ocurrenciaAlerta <= xmlOcurrencia
        Case "igual"
          condicion = ocurrenciaAlerta = xmlOcurrencia
        Case "mayorigual"
          condicion = ocurrenciaAlerta >= xmlOcurrencia
        Case "mayor"
          condicion = ocurrenciaAlerta > xmlOcurrencia
        Case Else
          condicion = False
      End Select

    Catch ex As Exception
      condicion = False
    End Try

    Return condicion
  End Function

  ''' <summary>
  ''' envia alerta para gestion via SMS
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub EnviarPorSMS(ByVal oAlertaWebservice As AlertaWebservice, ByVal rowCriterio As XmlElement)
    Dim listContactos As New List(Of Dictionary(Of String, String))

    Try
      'graba la alerta en tabla SMSAlerta
      oAlertaWebservice.GrabarAlertaSMS()

      'obtiene listado de contactos a informar
      listContactos = ObtenerContactosParaInformar(oAlertaWebservice, rowCriterio, "SMS")

      If (listContactos.Count > 0) Then
        EnviarAlertaSMS(oAlertaWebservice, listContactos)
      End If
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' envia alerta para gestion via CallCenter
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub EnviarPorCallCenter(ByVal oAlertaWebservice As AlertaWebservice, ByVal rowCriterio As XmlElement)
    Dim listContactos As New List(Of Dictionary(Of String, String))

    Try
      oAlertaWebservice.GrabarAlertaCallCenter()

      '---------------------------------------------------------
      'obtiene listado de contactos a informar por Email
      listContactos = ObtenerContactosParaInformar(oAlertaWebservice, rowCriterio, "Email")
      If (listContactos.Count > 0) Then EnviarAlertaEmail(oAlertaWebservice, listContactos)

      '---------------------------------------------------------
      'obtiene listado de contactos a informar por SMS
      listContactos = ObtenerContactosParaInformar(oAlertaWebservice, rowCriterio, "SMS")
      If (listContactos.Count > 0) Then EnviarAlertaSMS(oAlertaWebservice, listContactos)
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' envia alerta para gestion via Email
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub EnviarPorEmail(ByVal oAlertaWebservice As AlertaWebservice, ByVal rowCriterio As XmlElement)
    Dim listContactos As New List(Of Dictionary(Of String, String))

    Try
      'graba la alerta en tabla EmailAlerta
      oAlertaWebservice.GrabarAlertaEmail()

      'obtiene el tipo de viaje
      oAlertaWebservice.ObtenerDatosTrazaViaje()

      'Si la alerta es de tipo Tradicional
      If (oAlertaWebservice.TipoViaje.ToUpper() = "TRADICIONAL") Then

        Select oAlertaWebservice.DescripcionAlerta
          Case "INICIO DE RUTA", "PROXIMIDAD DE DESTINO", "FIN DE RUTA"
            EnviarAlertaEmailAreaDeRecibo(oAlertaWebservice)
          Case "SOBRETIEMPO EN DESCARGA DROP"
            EnviarAlertaEmailSobretiempoCarga(oAlertaWebservice)
          Case "CAMION DESPACHADO SIN SALIR A RUTA"
            ' este se tiene que enviar el correo a al area de despacho de cada cedis, falta buscar ese campo.
            EnviarAlertaEmailDespachadoSinSalirARuta(oAlertaWebservice)
        End Select

        'Sino sigue el curso normal
      Else

        'obtiene listado de contactos a informar
        listContactos = ObtenerContactosParaInformar(oAlertaWebservice, rowCriterio, "Email")

        If (listContactos.Count > 0) Then
          EnviarAlertaEmail(oAlertaWebservice, listContactos)
        End If

      End If

    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' obtiene los contactos del local que esten marcados con el canal consultado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerContactosLocalPorCanalComunicacion(ByVal oAlertaWebservice As AlertaWebservice) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(0).Value = oAlertaWebservice.LocalDestino
      arParms(1) = New SqlParameter("@CodigoCentroDistribucion", SqlDbType.Int)
      arParms(1).Value = oAlertaWebservice.OrigenCodigo

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Regla_ObtenerContactosLocalPorCanalComunicacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try

    Return ds
  End Function

  ''' <summary>
  ''' obtiene un listado con los contactos relacionados al local o personalizados
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function ObtieneListadoContactos(oAlertaWebservice As AlertaWebservice, dsContactosLocal As DataSet, ByVal listContactos As List(Of Dictionary(Of String, String)), _
                                                  xmlContactos As XmlNodeList, ByVal canalComunicacion As String, ByVal tipoEnvio As String) As List(Of Dictionary(Of String, String))
    Dim totalContactos As Integer
    Dim dcContacto As New Dictionary(Of String, String)
    Dim nombrePerfil, llavePerfil, nombreUsuario, telefono, email, cargo, horaAtencionInicio, horaAtencionTermino As String
    Dim rowContactos() As DataRow
    Dim dsContactosPorPerfil As New DataSet

    Try
      '--------------------------------------------------------------------
      'filtra los contactos del XML segun el formato y el tipo de envio
      Dim xmlContactosFiltrados = From xmlElem As XmlElement In xmlContactos
                                  Where xmlElem.GetAttribute("llaveFormato") = oAlertaWebservice.LlaveFormato _
                                  And xmlElem.GetAttribute("canalComunicacion").Contains("(" & canalComunicacion & ")") _
                                  And xmlElem.GetAttribute("tipoEnvio") = tipoEnvio

      'obtiene total de contactos encontrados
      totalContactos = xmlContactosFiltrados.Count

      If (totalContactos > 0) Then
        'busca los contactos que esten relacionados con el local
        For Each rowContacto As XmlElement In xmlContactosFiltrados
          dcContacto = New Dictionary(Of String, String)
          llavePerfil = rowContacto.GetAttribute("llavePerfil")
          nombrePerfil = rowContacto.GetAttribute("perfil")
          horaAtencionInicio = rowContacto.GetAttribute("horaAtencionInicio")
          horaAtencionTermino = rowContacto.GetAttribute("horaAtencionTermino")

          'si la llave esta vacia indica que es un perfil Personalizado,
          'sino busca los perfiles relacionados al local
          If (String.IsNullOrEmpty(llavePerfil) And nombrePerfil = "Personalizado") Then
            nombreUsuario = rowContacto.GetAttribute("nombre")
            telefono = rowContacto.GetAttribute("telefono")
            email = rowContacto.GetAttribute("email")
            cargo = rowContacto.GetAttribute("cargo")

            dcContacto.Add("EnviadoA", nombreUsuario)
            dcContacto.Add("Telefono", telefono)
            dcContacto.Add("Email", email)
            dcContacto.Add("Cargo", cargo)
            dcContacto.Add("HoraAtencionInicio", horaAtencionInicio)
            dcContacto.Add("HoraAtencionTermino", horaAtencionTermino)
            listContactos.Add(dcContacto)

          Else
            'filtra los contactos del local segun el perfil
            rowContactos = dsContactosLocal.Tables(0).Select("LlavePerfil = '" & llavePerfil & "'")

            'si no encuentra contactos en los perfiles del local, entonces lo busca directo en la base de datos
            If (rowContactos.Length = 0) Then
              dsContactosPorPerfil = Regla.ObtieneListadoContactosPorPerfil(oAlertaWebservice, llavePerfil)
              rowContactos = dsContactosPorPerfil.Tables(0).Select("LlavePerfil = '" & llavePerfil & "'")
            End If

            For Each dr In rowContactos
              dcContacto = New Dictionary(Of String, String)
              nombreUsuario = dr.Item("NombreUsuario")
              telefono = dr.Item("Telefono")
              email = dr.Item("Email")
              cargo = dr.Item("NombrePerfil")

              dcContacto.Add("EnviadoA", nombreUsuario)
              dcContacto.Add("Telefono", telefono)
              dcContacto.Add("Email", email)
              dcContacto.Add("Cargo", cargo)
              dcContacto.Add("HoraAtencionInicio", horaAtencionInicio)
              dcContacto.Add("HoraAtencionTermino", horaAtencionTermino)
              listContactos.Add(dcContacto)
            Next
          End If
        Next
      End If
    Catch ex As Exception
      'no hace nada
    End Try

    Return listContactos
  End Function

  ''' <summary>
  ''' envia SMS con la informacion de alerta
  ''' </summary>
  ''' <remarks></remarks>
  Private Shared Sub EnviarAlertaSMS(ByVal oAlertaWebservice As AlertaWebservice, ByVal listContactos As List(Of Dictionary(Of String, String)))
    Dim mensaje, telefono, nroTransporte, patenteTracto, patenteTrailer, localDestino, descripcionAlerta, tipoAlerta, enviadoA, cargo As String
    Dim smsFolio, smsCodigo, smsMensaje, ocurrencia, grupoAlerta, horaAtencionInicio, horaAtencionTermino As String
    Dim patente As String = ""
    Dim codigoRetorno As String = ""
    Dim mensajeRetorno As String = ""
    Dim lstTelefonosEnviados As New List(Of String)
    Dim sl As New SortedList
    Dim seDebeEnviar As Boolean
    Dim mensajeError As String = ""

    Try
      mensaje = "ALTOTRACK El IdMaster {NRO_TRANSPORTE}{PATENTE}, Tienda {LOCAL_DESTINO_CODIGO}, presenta un tipo alerta {TIPO_ALERTA}: {NOMBRE_ALERTA} (Ocurrencia {OCURRENCIA})"

      nroTransporte = oAlertaWebservice.NroTransporte
      patenteTracto = oAlertaWebservice.PatenteTracto
      patenteTrailer = oAlertaWebservice.PatenteTrailer
      localDestino = oAlertaWebservice.LocalDestino
      descripcionAlerta = oAlertaWebservice.DescripcionAlerta
      tipoAlerta = oAlertaWebservice.TipoAlerta
      ocurrencia = oAlertaWebservice.Ocurrencia
      grupoAlerta = oAlertaWebservice.GrupoAlerta

      patente &= IIf(String.IsNullOrEmpty(patenteTracto), "", "tracto " & patenteTracto & " ")
      patente &= IIf(String.IsNullOrEmpty(patenteTrailer), "", "remolque " & patenteTrailer)
      patente = IIf(String.IsNullOrEmpty(patente.Trim()), "", ", " & patente)

      'reemplaza marcas
      mensaje = mensaje.Replace("{NRO_TRANSPORTE}", nroTransporte)
      mensaje = mensaje.Replace("{PATENTE}", patente.Trim())
      mensaje = mensaje.Replace("{LOCAL_DESTINO_CODIGO}", localDestino)
      mensaje = mensaje.Replace("{NOMBRE_ALERTA}", descripcionAlerta)
      mensaje = mensaje.Replace("{TIPO_ALERTA}", tipoAlerta)
      mensaje = mensaje.Replace("{PATENTE_TRACTO}", patenteTracto)
      mensaje = mensaje.Replace("{OCURRENCIA}", ocurrencia)

      'obtiene los contactos a quienes enviarles los SMS
      For Each row In listContactos
        seDebeEnviar = True 'asume que el telefono es valido en primera instancia
        enviadoA = row.Item("EnviadoA")
        telefono = row.Item("Telefono")
        cargo = row.Item("Cargo")
        horaAtencionInicio = row.Item("HoraAtencionInicio")
        horaAtencionTermino = row.Item("HoraAtencionTermino")

        'envia el SMS cuando corresponda
        If Not Herramientas.MailModoDebug() Then
          'valida que el telefono sea numerico
          If (seDebeEnviar AndAlso Not Herramientas.EsNumerico(telefono)) Then seDebeEnviar = False : mensajeError = "El teléfono no es numérico"

          'valida que el telefono tenga largo 8
          If (seDebeEnviar AndAlso telefono.Length <> 8) Then seDebeEnviar = False : mensajeError = "El largo del teléfono no es válido"

          'valida que el telefono no comience con 2
          If (seDebeEnviar AndAlso telefono.StartsWith("2")) Then seDebeEnviar = False : mensajeError = "El teléfono es un número de red fija"

          'valida que el telefono no haya enviado mensaje con anterioridad
          If (seDebeEnviar AndAlso lstTelefonosEnviados.Contains(telefono)) Then seDebeEnviar = False : mensajeError = "El mensaje ya fue enviado a este teléfono"

          'valida que se pueda enviar el mensaje dentro del horario de atencion
          If (seDebeEnviar AndAlso Not Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then seDebeEnviar = False : mensajeError = "Se encuentra fuera de horario de atención, que es de " & horaAtencionInicio & " a " & horaAtencionTermino

          'si es valido entonces envia el mensaje
          If (seDebeEnviar) Then
            sl = Herramientas.EnviarSMS(telefono, mensaje)
            smsFolio = sl.Item("Folio")
            smsCodigo = sl.Item("Codigo")
            smsMensaje = sl.Item("Mensaje")
          Else
            smsFolio = ""
            smsCodigo = ""
            smsMensaje = mensajeError
          End If

          'si el telefono no esta dentro de los enviados, entonces lo agrega
          If (Not lstTelefonosEnviados.Contains(telefono)) Then
            lstTelefonosEnviados.Add(telefono)
          End If

        Else
          smsFolio = ""
          smsCodigo = ""
          smsMensaje = "Sistema en modo debug"
        End If

        'graba el historial de alerta sms
        Alerta.GrabarHistorialAlertaSMS(descripcionAlerta, nroTransporte, localDestino, telefono, enviadoA, cargo, grupoAlerta, ocurrencia, smsFolio.Trim(), smsCodigo.Trim(), smsMensaje.Trim(), "WEBSERVICES - Ocurrencia " & ocurrencia)
      Next
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  ''' <summary>
  ''' envia email con la informacion de la alerta
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Sub EnviarAlertaEmail(ByVal oAlertaWebservice As AlertaWebservice, ByVal listContactos As List(Of Dictionary(Of String, String)))
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, nroTransporte, nombreAlerta, email, descripcionAlerta, localDestino As String
    Dim enviadoA, cargo, grupoAlerta, ocurrencia, msgProcesoAux, horaAtencionInicio, horaAtencionTermino As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim listadoMailConCopia As String = ""
    Dim listEmailContactos As New List(Of String)

    Try
      nombreAlerta = oAlertaWebservice.DescripcionAlerta
      nroTransporte = oAlertaWebservice.NroTransporte
      descripcionAlerta = oAlertaWebservice.DescripcionAlerta
      localDestino = oAlertaWebservice.LocalDestino
      grupoAlerta = oAlertaWebservice.GrupoAlerta
      ocurrencia = oAlertaWebservice.Ocurrencia

      'construye asunto del mail
      asuntoMail = "[WALMART MX] Aviso Alerta {NOMBRE_ALERTA}: IdMaster {NRO_TRANSPORTE}"
      asuntoMail = asuntoMail.Replace("{NOMBRE_ALERTA}", nombreAlerta)
      asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)

      'construye listado de email
      For Each row In listContactos
        email = row.Item("Email")
        horaAtencionInicio = row.Item("HoraAtencionInicio")
        horaAtencionTermino = row.Item("HoraAtencionTermino")

        If (Not listEmailContactos.Contains(email) AndAlso Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then
          listEmailContactos.Add(email)
        End If
      Next
      listadoMailPara = String.Join(";", listEmailContactos.ToArray())
      listadoMailConCopiaOculta = Herramientas.MailAdministrador()

      'construye cuerpo del mail
      bodyMensaje = EnviarAlertaEmail_Detalle(oAlertaWebservice)

      'si esta en modo debug entonces envia el mail al correo de pruebas, sino lo envia a quien corresponda
      If Herramientas.MailModoDebug Then
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas)
        ColeccionEmailsCC = Nothing
        ColeccionEmailsCCO = Nothing
      Else
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
        ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
        ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
      End If

      'envia el mail
      msgProceso = Mail.Enviar(Herramientas.MailHostHD, _
                               Herramientas.MailEmisor, _
                               Herramientas.MailPasswordEmisor, _
                               Herramientas.MailNombreEmisor, _
                               ColeccionEmailsPara, _
                               asuntoMail, _
                               bodyMensaje, _
                               True, _
                               ColeccionEmailsCC, _
                               False, _
                               "", _
                               "", _
                               ColeccionEmailsCCO)
      'graba el mensaje enviado por mail en el disco
      If msgProceso <> String.Empty Then contadorErrores = 1
      Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
      Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_" & sufijoArchivo)
      Archivo.CrearArchivoTextoEnDisco(bodyMensaje & "<br / >" & msgProceso, Herramientas.RutaLogsSitio(), "AvisoAlertaEmail", nombreArchivo, "html")

      '-----------------------------------------------
      'graba el historial de alerta email
      msgProceso = IIf(String.IsNullOrEmpty(msgProceso), "Email enviado satisfactoriamente", msgProceso.Replace("<br/>", " "))
      For Each row In listContactos
        msgProcesoAux = msgProceso
        email = row.Item("Email")
        enviadoA = row.Item("EnviadoA")
        cargo = row.Item("Cargo")
        horaAtencionInicio = row.Item("HoraAtencionInicio")
        horaAtencionTermino = row.Item("HoraAtencionTermino")

        If (Not Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then
          msgProcesoAux = "Se encuentra fuera de horario de atención, que es de " & horaAtencionInicio & " a " & horaAtencionTermino
        End If

        Alerta.GrabarHistorialAlertaEmail(descripcionAlerta, nroTransporte, localDestino, email, enviadoA, cargo, grupoAlerta, ocurrencia, msgProcesoAux, "WEBSERVICES - Ocurrencia " & ocurrencia)
      Next

    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  ''' <summary>
  ''' construye html con el detalle de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function EnviarAlertaEmail_Detalle(ByVal oAlertaWebservice As AlertaWebservice) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim templateTitulo, templateFila, templateFilaAux, templatePrioridad, key, label, texto, tipoAlerta As String
    Dim diccionario As New Dictionary(Of String, String)
    Dim slColorBackground As New SortedList
    Dim slColorFuente As New SortedList
    Dim propInfo As PropertyInfo

    Try
      templateTitulo = "<div style=""font-family:Arial; font-size:14px; font-weight:bold; color:#fff; background-color:#333; padding:10px;"">{TITULO}</div>"

      templateFila = "<tr valign=""top"">" & _
                     "  <td style=""font-family:Arial; font-size:12px; font-weight: bold; width:180px"">{LABEL}:</td>" & _
                     "  <td style=""font-family:Arial; font-size:12px;"">{TEXTO}</td>" & _
                     "</tr>"

      'define colores de la prioridad
      slColorBackground.Add("success", "#73A839")
      slColorBackground.Add("warning", "#fcf8e3")
      slColorBackground.Add("danger", "#C71C22")
      slColorBackground.Add("black", "#000")
      slColorFuente.Add("success", "#fff")
      slColorFuente.Add("warning", "#c09853")
      slColorFuente.Add("danger", "#fff")
      slColorFuente.Add("black", "#fff")

      'Titulo
      templateTitulo = templateTitulo.Replace("{TITULO}", "DATOS DE LA ALERTA")
      sb.Append(templateTitulo)

      'agrega las columnas a mostrar
      diccionario.Add("Criticidad", "Prioridad")
      diccionario.Add("DescripcionAlerta", "Alerta")
      diccionario.Add("NroTransporte", "IdMaster")
      diccionario.Add("TipoAlerta", "Tipo Alerta")
      diccionario.Add("OrigenCodigo", "Cedis Origen Código")
      diccionario.Add("LocalDestino", "Determinante Tienda Destino")
      diccionario.Add("NombreConductor", "Operador")
      diccionario.Add("NombreTransportista", "Línea de Transporte")
      diccionario.Add("FechaHoraCreacion", "Fecha Creación")
      diccionario.Add("AlertaMapa", "Mapa")
      diccionario.Add("Ocurrencia", "Ocurrencia")

      'dibuja las columnas
      sb.Append("<div>")
      sb.Append("  <table border=""0"" cellpadding=""0"" cellspacing=""5"">")

      For Each item In diccionario
        templateFilaAux = templateFila
        key = item.Key
        label = item.Value

        'obtiene valor de la propiedad de la clase
        propInfo = oAlertaWebservice.GetType.GetProperty(key)
        texto = propInfo.GetValue(oAlertaWebservice, Nothing)

        If (key = "Criticidad") Then
          templatePrioridad = "<span style=""background-color:{BACKGROUND}; color:{COLOR}; padding:0px 5px; font-weight:bold; font-size:13px;"">{TEXTO}</span>"
          tipoAlerta = Herramientas.ObtenerStyleAlertaPorPrioridad(texto)

          templatePrioridad = templatePrioridad.Replace("{BACKGROUND}", slColorBackground.Item(tipoAlerta))
          templatePrioridad = templatePrioridad.Replace("{COLOR}", slColorFuente.Item(tipoAlerta))
          templatePrioridad = templatePrioridad.Replace("{TEXTO}", texto)
          texto = templatePrioridad
        End If

        If (key = "LocalDestino" And texto = "-1") Then
          texto = ""
        End If

        If (key = "AlertaMapa") Then
          texto = IIf(String.IsNullOrEmpty(texto), "<i>Sin mapa</i>", "<a href=""" & texto & """ target=""_blank"">Ver mapa</a>")
        End If

        If (key = "FechaHoraCreacion") Then
          texto = Replace(oAlertaWebservice.FechaHoraCreacion.ToString("dd/MM/yyyy H:mm"), "-", "/") & " hrs."
        End If

        templateFilaAux = templateFilaAux.Replace("{LABEL}", label)
        templateFilaAux = templateFilaAux.Replace("{TEXTO}", texto)
        sb.Append(templateFilaAux)
      Next

      sb.Append("  </table>")
      sb.Append("</div>")
      sb.Append("<br />")

      html = sb.ToString()
      Return html
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  ''' <summary>
  ''' obtiene listado de contactos a los cuales se les informara de la alerta ingresada
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function ObtenerContactosParaInformar(oAlertaWebservice As AlertaWebservice, ByVal rowCriterio As XmlElement, ByVal canalComunicacion As String) As List(Of Dictionary(Of String, String))
    Dim dsContactosLocal As New DataSet
    Dim xmlContactos As XmlNodeList
    Dim totalContactos As Integer
    Dim listContactos As New List(Of Dictionary(Of String, String))

    Try
      'obtiene los contactos del local que esten relacionados con este canal de comunicacion
      dsContactosLocal = Regla.ObtenerContactosLocalPorCanalComunicacion(oAlertaWebservice)

      'obtiene los contactos
      xmlContactos = rowCriterio.GetElementsByTagName("contacto")
      totalContactos = xmlContactos.Count

      If (totalContactos > 0) Then
        '--------------------------------------------------------------------
        '1) filtra los contactos con envios exclusivos asociados al local y los agrega a la lista
        listContactos = ObtieneListadoContactos(oAlertaWebservice, dsContactosLocal, listContactos, xmlContactos, canalComunicacion, "Exclusivo")

        '--------------------------------------------------------------------
        '2) obtiene los contactos ConCopia
        listContactos = ObtieneListadoContactos(oAlertaWebservice, dsContactosLocal, listContactos, xmlContactos, canalComunicacion, "ConCopia")
      End If
    Catch ex As Exception
      listContactos = New List(Of Dictionary(Of String, String))
    End Try

    Return listContactos
  End Function

  ''' <summary>
  ''' obtiene los contactos asociados al perfil
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtieneListadoContactosPorPerfil(ByVal oAlertaWebservice As AlertaWebservice, ByVal llavePerfil As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@LlavePerfil", SqlDbType.VarChar)
      arParms(0).Value = llavePerfil
      arParms(1) = New SqlParameter("@RutTransportista", SqlDbType.VarChar)
      arParms(1).Value = oAlertaWebservice.RutTransportista
      arParms(2) = New SqlParameter("@RutConductor", SqlDbType.VarChar)
      arParms(2).Value = oAlertaWebservice.RutConductor

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Regla_ObtieneListadoContactosPorPerfil", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try

    Return ds
  End Function

  Public Shared Sub EnviarAlertaEmailSobretiempoCarga(ByVal oAlertaWebservice As AlertaWebservice)
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, nroTransporte, nombreAlerta, descripcionAlerta, localDestino, grupoAlerta, ocurrencia As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim listadoMailConCopia As String = ""
    Dim listEmailContactos As New List(Of String)

    Try
      nroTransporte = oAlertaWebservice.NroTransporte
      nombreAlerta = oAlertaWebservice.DescripcionAlerta
      descripcionAlerta = oAlertaWebservice.DescripcionAlerta
      localDestino = oAlertaWebservice.LocalDestino
      grupoAlerta = oAlertaWebservice.GrupoAlerta
      ocurrencia = oAlertaWebservice.Ocurrencia

      'construye asunto del mail
      asuntoMail = "[WALMART MX] Aviso Alerta {NOMBRE_ALERTA}: IdMaster {NRO_TRANSPORTE}"
      asuntoMail = asuntoMail.Replace("{NOMBRE_ALERTA}", oAlertaWebservice.DescripcionAlerta)
      asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)

      'construye listado de email
      'For Each row In listContactos
      '  email = row.Item("Email")
      '  horaAtencionInicio = row.Item("HoraAtencionInicio")
      '  horaAtencionTermino = row.Item("HoraAtencionTermino")

      '  If (Not listEmailContactos.Contains(email) AndAlso Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then
      '    listEmailContactos.Add(email)
      '  End If
      'Next
      listadoMailPara = String.Join(";", listEmailContactos.ToArray())
      listadoMailConCopiaOculta = Herramientas.MailAdministrador()

      'construye cuerpo del mail
      bodyMensaje = "Área de atención de tiendas de """ & oAlertaWebservice.Cedis & """ se informa que ALTOTRACK ha detectado unidad """ & oAlertaWebservice.PatenteTracto & """ """ & oAlertaWebservice.NombreTransportista & """ con sobretiempo: más de 60 minutos en tienda drop " & oAlertaWebservice.NombreDeterminante
      bodyMensaje += "<br/><br/>"
      bodyMensaje += "<strong>Fecha y hora del correo:</strong> " + DateTime.Now.ToString("dd-MM-yyyy HH:mm")
      bodyMensaje += "<br/>"
      bodyMensaje += "<strong>Determinante tienda drop:</strong> " + oAlertaWebservice.NombreDeterminante
      bodyMensaje += "<br/>"
      bodyMensaje += "<strong>No. de placa:</strong> " + oAlertaWebservice.PatenteTracto
      bodyMensaje += "<br/>"
      bodyMensaje += "<strong>Línea transportista:</strong> " + oAlertaWebservice.NombreTransportista
      bodyMensaje += "<br/>"
      bodyMensaje += "<strong>CEDIS:</strong> " + oAlertaWebservice.Cedis
      bodyMensaje += "<br/><br/>"
      bodyMensaje += "<div style=""font-family:Arial; font-size:14px; font-style: italic;""><center>(este mail se ejecutó automáticamente por el sistema. No responder por favor.)</center></div>"

      'si esta en modo debug entonces envia el mail al correo de pruebas, sino lo envia a quien corresponda
      If Herramientas.MailModoDebug Then
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas)
        ColeccionEmailsCC = Nothing
        ColeccionEmailsCCO = Nothing
      Else
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
        ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
        ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
      End If

      'envia el mail
      msgProceso = Mail.Enviar(Herramientas.MailHostHD, _
                               Herramientas.MailEmisor, _
                               Herramientas.MailPasswordEmisor, _
                               Herramientas.MailNombreEmisor, _
                               ColeccionEmailsPara, _
                               asuntoMail, _
                               bodyMensaje, _
                               True, _
                               ColeccionEmailsCC, _
                               False, _
                               "", _
                               "", _
                               ColeccionEmailsCCO)
      'graba el mensaje enviado por mail en el disco
      If msgProceso <> String.Empty Then contadorErrores = 1
      Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
      Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_" & sufijoArchivo)
      Archivo.CrearArchivoTextoEnDisco(bodyMensaje & "<br / >" & msgProceso, Herramientas.RutaLogsSitio(), "AvisoAlertaEmail", nombreArchivo, "html")

      '-----------------------------------------------
      'graba el historial de alerta email
      msgProceso = IIf(String.IsNullOrEmpty(msgProceso), "Email enviado satisfactoriamente", msgProceso.Replace("<br/>", " "))
      'Alerta.GrabarHistorialAlertaEmail(descripcionAlerta, nroTransporte, localDestino, listadoMailPara, String.Empty, String.Empty, grupoAlerta, ocurrencia, msgProceso, "WEBSERVICES - Ocurrencia " & ocurrencia)

      'For Each row In listContactos
      '  msgProcesoAux = msgProceso
      '  email = row.Item("Email")
      '  enviadoA = row.Item("EnviadoA")
      '  cargo = row.Item("Cargo")
      '  horaAtencionInicio = row.Item("HoraAtencionInicio")
      '  horaAtencionTermino = row.Item("HoraAtencionTermino")

      '  If (Not Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then
      '    msgProcesoAux = "Se encuentra fuera de horario de atención, que es de " & horaAtencionInicio & " a " & horaAtencionTermino
      '  End If

      '  Alerta.GrabarHistorialAlertaEmail(descripcionAlerta, nroTransporte, localDestino, email, enviadoA, cargo, grupoAlerta, ocurrencia, msgProcesoAux, "WEBSERVICES - Ocurrencia " & ocurrencia)
      'Next

    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  Public Shared Sub EnviarAlertaEmailAreaDeRecibo(ByVal oAlertaWebservice As AlertaWebservice)
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, nroTransporte, nombreAlerta, email, descripcionAlerta, localDestino As String
    Dim enviadoA, cargo, grupoAlerta, ocurrencia, msgProcesoAux, horaAtencionInicio, horaAtencionTermino, titulo As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim listadoMailConCopia As String = ""
    Dim templateFila As String = ""
    Dim listEmailContactos As New List(Of String)
    Dim sb As New StringBuilder

    Try
      nombreAlerta = oAlertaWebservice.DescripcionAlerta
      nroTransporte = oAlertaWebservice.NroTransporte
      descripcionAlerta = oAlertaWebservice.DescripcionAlerta
      localDestino = oAlertaWebservice.LocalDestino
      grupoAlerta = oAlertaWebservice.GrupoAlerta
      ocurrencia = oAlertaWebservice.Ocurrencia

      'construye asunto del mail
      asuntoMail = "[WALMART MX] Aviso Alerta {NOMBRE_ALERTA}: IdMaster {NRO_TRANSPORTE}"
      asuntoMail = asuntoMail.Replace("{NOMBRE_ALERTA}", nombreAlerta)
      asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)

      titulo = "[WALMART MX]  Área de recibo {TIENDA}, {DETERMINANTE} se informa que ALTOTRACK ha detectado unidad {PLACAS} {LINEA TRANSPORTISTAS} en {DESCRIPCION ALERTA}."
      titulo = titulo.Replace("{TIENDA}", oAlertaWebservice.Cedis) 'descripcion del destino
      titulo = titulo.Replace("{DETERMINANTE}", oAlertaWebservice.NombreDeterminante)
      titulo = titulo.Replace("{PLACAS}", oAlertaWebservice.PatenteTracto)
      titulo = titulo.Replace("{LINEA TRANSPORTISTAS}", oAlertaWebservice.NombreTransportista)
      titulo = titulo.Replace("{DESCRIPCION ALERTA}", descripcionAlerta)

      '        asuntoMail = asuntoMail.Replace("{NOMBRE_ALERTA}", nombreAlerta)
      '  asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)

      'construye listado de email
      'For Each row In listContactos
      '  email = row.Item("Email")
      '  horaAtencionInicio = row.Item("HoraAtencionInicio")
      '  horaAtencionTermino = row.Item("HoraAtencionTermino")

      '  If (Not listEmailContactos.Contains(email) AndAlso Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then
      '    listEmailContactos.Add(email)
      '  End If
      'Next


      listadoMailPara = String.Join(";", listEmailContactos.ToArray())
      listadoMailConCopiaOculta = Herramientas.MailAdministrador()

      'construye cuerpo del mail
      '  bodyMensaje = EnviarAlertaEmail_Detalle(oAlertaWebservice)


      sb.AppendLine("<div style=""font-family:Arial; font-size:14px; font-weight:bold; color:#fff; background-color:#333; padding:10px;"">" & titulo & "</div>") 'Titulo
      sb.AppendLine("<br />") 'Titulo

      templateFila = "<tr valign=""top"">" & _
                "  <td style=""font-family:Arial; font-size:12px; font-weight: bold; width:180px"">{LABEL}:</td>" & _
                "  <td style=""font-family:Arial; font-size:12px;"">{TEXTO}</td>" & _
                "</tr>"
      ' aqui tenemos que modificar
      sb.AppendLine(templateFila.Replace("{LABEL}", "Fecha y hora del correo").Replace("{TEXTO}", DateTime.Now.ToString("dd-MM-yyyy HH:mm"))) '
      sb.AppendLine(templateFila.Replace("{LABEL}", "ID Master").Replace("{TEXTO}", nroTransporte)) '
      sb.AppendLine(templateFila.Replace("{LABEL}", "No. de placa").Replace("{TEXTO}", oAlertaWebservice.PatenteTracto)) '
      sb.AppendLine(templateFila.Replace("{LABEL}", "Línea transportista").Replace("{TEXTO}", oAlertaWebservice.NombreTransportista)) '
      sb.AppendLine(templateFila.Replace("{LABEL}", "Cedis Origen").Replace("{TEXTO}", oAlertaWebservice.Cedis)) '
      sb.AppendLine(templateFila.Replace("{LABEL}", " Nombre y Determinante tienda destino").Replace("{TEXTO}", oAlertaWebservice.NombreDeterminante)) '


      bodyMensaje = sb.ToString()

      'si esta en modo debug entonces envia el mail al correo de pruebas, sino lo envia a quien corresponda
      If Herramientas.MailModoDebug Then
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas)
        ColeccionEmailsCC = Nothing
        ColeccionEmailsCCO = Nothing
      Else
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
        ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
        ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
      End If

      'envia el mail
      msgProceso = Mail.Enviar(Herramientas.MailHostHD, _
                               Herramientas.MailEmisor, _
                               Herramientas.MailPasswordEmisor, _
                               Herramientas.MailNombreEmisor, _
                               ColeccionEmailsPara, _
                               asuntoMail, _
                               bodyMensaje, _
                               True, _
                               ColeccionEmailsCC, _
                               False, _
                               "", _
                               "", _
                               ColeccionEmailsCCO)
      'graba el mensaje enviado por mail en el disco
      If msgProceso <> String.Empty Then contadorErrores = 1
      Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
      Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_" & sufijoArchivo)
      Archivo.CrearArchivoTextoEnDisco(bodyMensaje & "<br / >" & msgProceso, Herramientas.RutaLogsSitio(), "AvisoAlertaEmail", nombreArchivo, "html")

      '-----------------------------------------------
      'graba el historial de alerta email
      msgProceso = IIf(String.IsNullOrEmpty(msgProceso), "Email enviado satisfactoriamente", msgProceso.Replace("<br/>", " "))
      'For Each row In listContactos
      '  msgProcesoAux = msgProceso
      '  email = row.Item("Email")
      '  enviadoA = row.Item("EnviadoA")
      '  cargo = row.Item("Cargo")
      '  horaAtencionInicio = row.Item("HoraAtencionInicio")
      '  horaAtencionTermino = row.Item("HoraAtencionTermino")

      '  If (Not Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then
      '    msgProcesoAux = "Se encuentra fuera de horario de atención, que es de " & horaAtencionInicio & " a " & horaAtencionTermino
      '  End If

      '  Alerta.GrabarHistorialAlertaEmail(descripcionAlerta, nroTransporte, localDestino, email, enviadoA, cargo, grupoAlerta, ocurrencia, msgProcesoAux, "WEBSERVICES - Ocurrencia " & ocurrencia)
      'Next

    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  Public Shared Sub EnviarAlertaEmailDespachadoSinSalirARuta(ByVal oAlertaWebservice As AlertaWebservice)
    Dim msgProceso As String = ""
    Dim contadorErrores As Integer
    Dim listadoMailPara, listadoMailConCopiaOculta, asuntoMail, nroTransporte, nombreAlerta, email, descripcionAlerta, localDestino As String
    Dim enviadoA, cargo, grupoAlerta, ocurrencia, msgProcesoAux, horaAtencionInicio, horaAtencionTermino, titulo As String
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim bodyMensaje As String = ""
    Dim listadoMailConCopia As String = ""
    Dim templateFila As String = ""
    Dim listEmailContactos As New List(Of String)
    Dim sb As New StringBuilder

    Try
      nombreAlerta = oAlertaWebservice.DescripcionAlerta
      nroTransporte = oAlertaWebservice.NroTransporte
      descripcionAlerta = oAlertaWebservice.DescripcionAlerta
      localDestino = oAlertaWebservice.LocalDestino
      grupoAlerta = oAlertaWebservice.GrupoAlerta
      ocurrencia = oAlertaWebservice.Ocurrencia


      'construye asunto del mail
      asuntoMail = "[WALMART MX] Aviso Alerta {NOMBRE_ALERTA}: IdMaster {NRO_TRANSPORTE}"
      asuntoMail = asuntoMail.Replace("{NOMBRE_ALERTA}", nombreAlerta)  '
      asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)  '

      titulo = "[WALMART MX]  Área de {AREA ENVIO} de {CEDIS} se informa que ALTOTRACK ha detectado unidad despachada, con más de {FRECUENCIA MAIL} minutos sin salir a ruta {PLACAS} {LINEA TRANSPORTISTAS} ."
      titulo = titulo.Replace("{AREA ENVIO}", "DESPACHO")  'descripcion del destino (DESPACHO, SUBGERENCIA, GERENCIA)
      titulo = titulo.Replace("{CEDIS}", oAlertaWebservice.Cedis)
      titulo = titulo.Replace("{FRECUENCIA MAIL}", "60") '(60,120, 180)
      titulo = titulo.Replace("{PLACAS}", oAlertaWebservice.PatenteTracto)
      titulo = titulo.Replace("{LINEA TRANSPORTISTAS}", oAlertaWebservice.NombreTransportista)
      titulo = titulo.Replace("{DESCRIPCION ALERTA}", oAlertaWebservice.DescripcionAlerta)

      '        asuntoMail = asuntoMail.Replace("{NOMBRE_ALERTA}", nombreAlerta)
      '  asuntoMail = asuntoMail.Replace("{NRO_TRANSPORTE}", nroTransporte)

      'construye listado de email
      'For Each row In listContactos
      '  email = row.Item("Email")
      '  horaAtencionInicio = row.Item("HoraAtencionInicio")
      '  horaAtencionTermino = row.Item("HoraAtencionTermino")

      '  If (Not listEmailContactos.Contains(email) AndAlso Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then
      '    listEmailContactos.Add(email)
      '  End If
      'Next


      listadoMailPara = String.Join(";", listEmailContactos.ToArray())
      listadoMailConCopiaOculta = Herramientas.MailAdministrador()

      'construye cuerpo del mail
      '  bodyMensaje = EnviarAlertaEmail_Detalle(oAlertaWebservice)


      sb.AppendLine("<div style=""font-family:Arial; font-size:14px; font-weight:bold; color:#fff; background-color:#333; padding:10px;"">" & titulo & "</div>") 'Titulo
      sb.AppendLine("<br />") 'Titulo

      templateFila = "<tr valign=""top"">" & _
                "  <td style=""font-family:Arial; font-size:12px; font-weight: bold; width:180px"">{LABEL}:</td>" & _
                "  <td style=""font-family:Arial; font-size:12px;"">{TEXTO}</td>" & _
                "</tr>"
      ' aqui tenemos que modificar
      sb.AppendLine(templateFila.Replace("{LABEL}", "Fecha y hora del correo").Replace("{TEXTO}", DateTime.Now.ToString("dd-MM-yyyy HH:mm"))) '
      sb.AppendLine(templateFila.Replace("{LABEL}", "ID Master").Replace("{TEXTO}", nroTransporte)) '
      sb.AppendLine(templateFila.Replace("{LABEL}", "No. de placa").Replace("{TEXTO}", oAlertaWebservice.PatenteTracto)) '
      sb.AppendLine(templateFila.Replace("{LABEL}", "Línea transportista").Replace("{TEXTO}", oAlertaWebservice.NombreTransportista)) '
      sb.AppendLine(templateFila.Replace("{LABEL}", "Cedis Origen").Replace("{TEXTO}", oAlertaWebservice.Cedis)) '


      bodyMensaje = sb.ToString()

      'si esta en modo debug entonces envia el mail al correo de pruebas, sino lo envia a quien corresponda
      If Herramientas.MailModoDebug Then
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas)
        ColeccionEmailsCC = Nothing
        ColeccionEmailsCCO = Nothing
      Else
        ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
        ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailConCopia)
        ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailConCopiaOculta)
      End If

      'envia el mail
      msgProceso = Mail.Enviar(Herramientas.MailHostHD, _
                               Herramientas.MailEmisor, _
                               Herramientas.MailPasswordEmisor, _
                               Herramientas.MailNombreEmisor, _
                               ColeccionEmailsPara, _
                               asuntoMail, _
                               bodyMensaje, _
                               True, _
                               ColeccionEmailsCC, _
                               False, _
                               "", _
                               "", _
                               ColeccionEmailsCCO)
      'graba el mensaje enviado por mail en el disco
      If msgProceso <> String.Empty Then contadorErrores = 1
      Dim sufijoArchivo As String = IIf(contadorErrores > 0, "ENVIADO_CON_ERRORES", "ENVIADO_CON_EXITO")
      Dim nombreArchivo As String = Archivo.CrearNombreUnicoArchivo("__NroTransporte_" & nroTransporte & "_" & sufijoArchivo)
      Archivo.CrearArchivoTextoEnDisco(bodyMensaje & "<br / >" & msgProceso, Herramientas.RutaLogsSitio(), "AvisoAlertaEmail", nombreArchivo, "html")

      '-----------------------------------------------
      'graba el historial de alerta email
      msgProceso = IIf(String.IsNullOrEmpty(msgProceso), "Email enviado satisfactoriamente", msgProceso.Replace("<br/>", " "))
      'For Each row In listContactos
      '  msgProcesoAux = msgProceso
      '  email = row.Item("Email")
      '  enviadoA = row.Item("EnviadoA")
      '  cargo = row.Item("Cargo")
      '  horaAtencionInicio = row.Item("HoraAtencionInicio")
      '  horaAtencionTermino = row.Item("HoraAtencionTermino")

      '  If (Not Herramientas.EstaDentroHorarioAtencion(horaAtencionInicio, horaAtencionTermino)) Then
      '    msgProcesoAux = "Se encuentra fuera de horario de atención, que es de " & horaAtencionInicio & " a " & horaAtencionTermino
      '  End If

      '    Alerta.GrabarHistorialAlertaEmail(descripcionAlerta, nroTransporte, localDestino, email, enviadoA, cargo, grupoAlerta, ocurrencia, msgProcesoAux, "WEBSERVICES - Ocurrencia " & ocurrencia)
      'Next

    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region
End Class

﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Planificacion
#Region "Properties"
  Public Const NombreEntidad As String = "Planificacion"

  Public Property Id As Integer
  Public Property IdUsuarioCreacion As Integer
  Public Property FechaCreacion As DateTime

  Public Property Existe As Boolean

  Public Enum eTablasBasicas As Integer
    T00_Transportistas = 0
    T01_Locales = 1
    T02_PlanificacionesCargadas = 2
  End Enum
#End Region

#Region "Constructor"

  Public Sub New()
    MyBase.New()
  End Sub

  Public Sub New(ByVal Id As Integer)
    Try
      Me.Id = Id
      ObtenerPorId()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Publicos"

  Public Sub GuardarNuevo()

    Dim storedProcedure As String = "spu_Planificacion_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(2) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@IdUsuarioCreacion", SqlDbType.Int)
      parms(2) = New SqlParameter("@FechaCreacion", SqlDbType.DateTime)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = Me.IdUsuarioCreacion
      parms(2).Value = Me.FechaCreacion

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      Me.Id = parms(0).Value
      Me.Existe = True

      ObtenerPorId() ' refrescar la instancia
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

#End Region

#Region "Metodos Privados"

  Private Sub ObtenerPorId()
    Try
      Existe = False

      Dim storedProcedure As String = "spu_Planificacion_ObtenerPorId"
      Dim parms() As SqlParameter = New SqlParameter(0) {}
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("IdUsuarioCreacion")) Then Me.IdUsuarioCreacion = row("IdUsuarioCreacion")
        If Not IsDBNull(row("FechaCreacion")) Then Me.FechaCreacion = row("FechaCreacion")
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try

  End Sub

#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene datos de las tablas basicas para comparar si existen los datos a cargar
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerTablasBasicasSistema() As DataSet
    Dim ds As New DataSet
    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Planificacion_ObtenerTablasBasicasSistema")
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene los datos de la carga realizada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerCarga(ByVal idPlanificacion As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      arParms(0).Value = idPlanificacion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Planificacion_ObtenerCarga", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

End Class

﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class ClasificacionConductor

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "ClasificacionConductor"

  Public Enum eTabla As Integer
    Detalle = 0
  End Enum
#End Region

#Region "Metodos compartidos"
  ''' <summary>
  ''' obtiene listado de ClasificacionConductor
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoConFiltro(ByVal idUsuario As String, ByVal filtroNombre As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = filtroNombre

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_ClasificacionConductor_ObtenerListadoConFiltro", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle del ClasificacionConductor consultado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal idClasificacionConductor As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdClasificacionConductor", SqlDbType.Int)
      arParms(0).Value = idClasificacionConductor

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_ClasificacionConductor_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba una clasificacion Conductor
  ''' </summary>
  Public Shared Function GrabarDatos(ByRef idClasificacionConductor As String, ByVal nombre As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdClasificacionConductorOutput", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdClasificacionConductor", SqlDbType.Int)
      arParms(2).Value = idClasificacionConductor
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_ClasificacionConductor_GrabarDatos", arParms)
      valor = arParms(0).Value
      idClasificacionConductor = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar ClasificacionConductor.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar ClasificacionConductor.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para la clasificacion Conductor seleccionada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function VerificarDuplicidad(ByVal idClasificacionConductor As String, ByVal valor As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_ClasificacionConductor_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdClasificacionConductor", SqlDbType.Int)
      arParms(1).Value = idClasificacionConductor
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' elimina una clasificacion Conductor
  ''' </summary>
  Public Shared Function EliminarDatos(ByVal idClasificacionConductor As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdClasificacionConductor", SqlDbType.Int)
      arParms(1).Value = idClasificacionConductor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_ClasificacionConductor_EliminarDatos", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar ClasificacionConductor.EliminarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene los cargos asociados al centro distribucion
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerCargos(ByVal idCentroDistribucion As String, ByVal categoria As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@idCentroDistribucion", SqlDbType.Int)
      arParms(0).Value = idCentroDistribucion
      arParms(1) = New SqlParameter("@Categoria", SqlDbType.VarChar)
      arParms(1).Value = categoria

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_CentroDistribucion_ObtenerCargos", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

End Class

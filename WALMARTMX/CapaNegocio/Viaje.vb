﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class Viaje
  Public Property Id As Integer
  Public Property FechaProgramacion As String
  Public Property CantidadPkt As Integer
  Public Property Zona As String
  Public Property Placa As String
  Public Property Conductor As String
  Public Property Correlativo As String

  Public Shared Function ObtenerEcommerce(ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal pkt As String, ByVal upc As String, ByVal ciudad As String, ByVal zip As String, ByVal zona As String, ByVal numeroOrden As String) As DataTable
    Try
      Dim ds As DataSet

      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(8) {}
      arParms(0) = New SqlParameter("@fechaDesde", SqlDbType.VarChar)
      arParms(0).Value = fechaDesde
      arParms(1) = New SqlParameter("@fechaHasta", SqlDbType.VarChar)
      arParms(1).Value = fechaHasta
      arParms(2) = New SqlParameter("@pkt", SqlDbType.VarChar)
      arParms(2).Value = pkt
      arParms(3) = New SqlParameter("@upc", SqlDbType.VarChar)
      arParms(3).Value = upc
      arParms(4) = New SqlParameter("@ciudad", SqlDbType.VarChar)
      arParms(4).Value = ciudad
      arParms(5) = New SqlParameter("@zip", SqlDbType.Int)
      arParms(5).Value = zip
      arParms(6) = New SqlParameter("@zona", SqlDbType.VarChar)
      arParms(6).Value = zona
      arParms(7) = New SqlParameter("@numeroOrden", SqlDbType.Int)
      arParms(7).Value = numeroOrden

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Viaje_ObtenerEcommerce", arParms)
      If (Not ds Is Nothing) Then
        Return ds.Tables(0)
      End If

      Return Nothing

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Public Shared Function Grabar(ByVal oViaje As Viaje)

    Try

      Dim dataBase As Data.Database = DatabaseFactory.CreateDatabase(Conexion.StrConexionBdAltotrackWalmartMX)
      Dim db As DbCommand = dataBase.GetStoredProcCommand("spu_Viaje_AsignarDatos")
      dataBase.AddInParameter(db, "id", SqlDbType.Int, oViaje.Id)
      dataBase.AddInParameter(db, "fechaProgramacion", SqlDbType.VarChar, oViaje.FechaProgramacion)
      dataBase.AddInParameter(db, "cantidadPkt", SqlDbType.Int, oViaje.CantidadPkt)
      dataBase.AddInParameter(db, "zona", SqlDbType.VarChar, oViaje.Zona)
      dataBase.AddInParameter(db, "placa", SqlDbType.VarChar, oViaje.Placa)
      dataBase.AddInParameter(db, "conductor", SqlDbType.VarChar, oViaje.Conductor)
      dataBase.AddInParameter(db, "correlativo", SqlDbType.Int, oViaje.Correlativo)
      dataBase.ExecuteNonQuery(db)

    Catch ex As Exception

    End Try
  End Function

  Public Shared Function ObtenerInfoPatenteEcommerce(ByVal patente As String) As DataSet
    Try

      Dim dataBase As Data.Database = DatabaseFactory.CreateDatabase(Conexion.StrConexionBdAltotrackWalmartMX)
      Dim db As DbCommand = dataBase.GetStoredProcCommand("spu_viaje_ObtenerInfoPatenteEcommerce")
      dataBase.AddInParameter(db, "patente", SqlDbType.VarChar, patente)
      Return dataBase.ExecuteDataSet(db)

    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

End Class

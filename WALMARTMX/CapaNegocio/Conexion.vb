Imports System.IO

Public Class Conexion
#Region "Enum"
  Enum eOrigenDatos As Integer
    SistemaBase = 0
  End Enum

  Enum eAmbienteInstalacion As Integer
    'Desarrollo = 0
    'Demo = 1
    Produccion = 2
  End Enum
#End Region

#Region "Constante"
  'Constante para obtener el string de conexion con EnterpriseLibrary
  '---------------------------------------------------------------------------------------------
  Public Const StrConexionBdAltotrackWalmartMX = "strConexionBD_AltotrackWalmartMX"
#End Region

#Region "shared"
  ''' <summary>
  ''' obtiene string conexion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function StringConexion(Optional ByVal origenDatos As eOrigenDatos = eOrigenDatos.SistemaBase) As String
    Dim sConexion As String = String.Empty
    Dim key As String = ""

    Try

      'dependiendo de la fuente de datos seleccionada la key respectiva
      Select Case origenDatos
        Case eOrigenDatos.SistemaBase
          key = "strConexionBD_AltotrackWalmartMX"
        Case Else
          key = "strConexionBD_AltotrackWalmartMX"
      End Select

            sConexion = System.Configuration.ConfigurationManager.ConnectionStrings(key).ConnectionString + ";Connection Timeout=60000;"

        Catch ex As Exception
      Return String.Empty
    End Try
    ' devolver el string de conexion
    Return sConexion
  End Function
#End Region

End Class

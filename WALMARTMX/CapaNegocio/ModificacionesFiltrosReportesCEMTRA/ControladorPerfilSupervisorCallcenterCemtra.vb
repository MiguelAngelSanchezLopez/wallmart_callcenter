﻿Public Class ControladorPerfilSupervisorCallcenterCemtra
    Inherits ControladorGeneral

    Public Function ObtenerIdPerfilSupervisorCallcenterCemtra() As Int32
        Dim IdPerfilSupervisorCallcenterCemtra As Int32

        Try
            InicializarConexion()
            conexionSQL.Open()
            EstablecerProcedimientoAlmacenado("GetIdPerfilSupervisorCallcenterCemtra")
            IdPerfilSupervisorCallcenterCemtra = ObtenerDatoEnteroUnico()
            conexionSQL.Close()
        Catch ex As Exception
            Throw ex
        End Try

        Return IdPerfilSupervisorCallcenterCemtra
    End Function

End Class

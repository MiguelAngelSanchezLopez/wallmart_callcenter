﻿Public Class SupervisorCallcenterCemtraRegistrado

    Private Shared unicaInstancia As SupervisorCallcenterCemtraRegistrado = New SupervisorCallcenterCemtraRegistrado
    Private Shared idPerfil As Int32
    Private Shared idUsuario As Int32
    Private Shared usuarioLogueadoEsSupervisorCallcenterCemtra As Boolean
    Private idPerfilSupervisorCallcenterCemtra As Int32

    Public Function VerificarSiUsuarioLogueadoEsSupervisorCallcenterCemtra() As Boolean

        Dim obtenedorIdSupervisorCemtra As ControladorPerfilSupervisorCallcenterCemtra
        obtenedorIdSupervisorCemtra = New ControladorPerfilSupervisorCallcenterCemtra

        Dim IdPerfilSupervisorCallcenterCemtra As Int32

        IdPerfilSupervisorCallcenterCemtra = obtenedorIdSupervisorCemtra.ObtenerIdPerfilSupervisorCallcenterCemtra()

        If idPerfil = IdPerfilSupervisorCallcenterCemtra Then
            usuarioLogueadoEsSupervisorCallcenterCemtra = True
        Else
            usuarioLogueadoEsSupervisorCallcenterCemtra = False
        End If

        Return usuarioLogueadoEsSupervisorCallcenterCemtra

    End Function

    Public Sub EstablecerIdPerfilSupervisorCallcenterCemtra(idPerfil As Int32)
        Me.idPerfilSupervisorCallcenterCemtra = idPerfil
    End Sub

    Public Sub EstablecerIdPerfil(idPerfil As Int32)
        Me.idPerfil = idPerfil
    End Sub

    Public Function ObtenerIdPerfil() As Int32
        Return idPerfil
    End Function

    Public Sub EstablecerIdUsuario(idUsuario As Int32)
        Me.idUsuario = idUsuario
    End Sub

    Public Function ObtenerIdUsuario() As Int32
        Return idUsuario
    End Function

    Private Sub New()
        usuarioLogueadoEsSupervisorCallcenterCemtra = False
        idPerfilSupervisorCallcenterCemtra = 0
    End Sub

    Public Shared ReadOnly Property ObtenerInstancia As SupervisorCallcenterCemtraRegistrado
        Get
            Return unicaInstancia
        End Get
    End Property

End Class

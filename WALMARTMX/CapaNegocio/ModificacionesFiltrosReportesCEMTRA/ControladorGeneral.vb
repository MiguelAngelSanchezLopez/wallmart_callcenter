﻿Imports System.Data.SqlClient

Public Class ControladorGeneral
    Protected conexionSQL As SqlConnection
    Protected comandoSQL As SqlCommand
    Protected lectorDatosSQL As SqlDataReader

    Public CADENA_CONEXION As String =
        System.Configuration.ConfigurationManager.ConnectionStrings("strConexionBD_AltotrackWalmartMX").ConnectionString
    '"Data Source=altotrack-sql-desarrollo.clhc4wjn055a.us-east-1.rds.amazonaws.com;Initial Catalog=TRANSPORTE_WALMART_MX;User ID=sa_sqlaltotrack;Password=sa_sqlaltotrack2018" 'DESARROLLO
    '"Data Source=altotrack-sql.clhc4wjn055a.us-east-1.rds.amazonaws.com;Initial Catalog=TRANSPORTE_WALMART_MX;User ID=transportewalmartmx;Password=#WalmartMexico2016#" 'PRODUCCION


    Public Sub InicializarConexion()
        Try
            Me.conexionSQL = New SqlConnection(CADENA_CONEXION)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EstablecerProcedimientoAlmacenado(ByVal nombreProcedimientoAlmacenado As String)
        Try
            Me.comandoSQL = New SqlCommand(nombreProcedimientoAlmacenado, conexionSQL)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub EjecutarProcedimientoAlmacenado()
        Me.comandoSQL.ExecuteNonQuery()
    End Sub

    Public Sub EstablecerParametroProcedimientoAlmacenado(ByVal valorParametro As Object, ByVal descripcionTipo As String, ByVal nombreParametro As String)
        Me.comandoSQL.CommandType = CommandType.StoredProcedure
        Me.comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro
    End Sub

    Public Function ObtenerTipoDatoParaConsultaSQL(ByVal descripcionTipo As String) As SqlDbType
        Dim tipoDatoDevolver = New SqlDbType()
        Select Case descripcionTipo
            Case "int"
                tipoDatoDevolver = SqlDbType.Int
            Case "string"
                tipoDatoDevolver = SqlDbType.NVarChar
            Case "bool"
                tipoDatoDevolver = SqlDbType.Bit
            Case "datetime"
                tipoDatoDevolver = SqlDbType.DateTime
        End Select

        Return tipoDatoDevolver

    End Function

    Public Function ObtenerDatoEnteroUnico()
        Dim datoEnteroUnico = 0
        Try
            datoEnteroUnico = Convert.ToInt32(Me.comandoSQL.ExecuteScalar())
        Catch ex As Exception
            Throw ex
        End Try
        Return datoEnteroUnico
    End Function

    Public Sub LlenarLectorDatosSQL()
        Try
            Me.lectorDatosSQL = Me.comandoSQL.ExecuteReader
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
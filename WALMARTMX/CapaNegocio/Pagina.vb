Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Pagina

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Pagina"

  Public Enum eTabla As Integer
    Detalle = 0
    ListadoFunciones = 1
    ListadoPerfilesNoPertenece = 2
    ListadoPerfilesSiPertenece = 3
  End Enum
#End Region

#Region "Metodos compartidos"
  ''' <summary>
  ''' obtiene listado de paginas
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <param name="filtroNombre"></param>
  ''' <param name="filtroCategoria"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 15/09/2009</remarks>
  Public Shared Function ObtenerListadoConFiltro(ByVal idUsuario As String, ByVal filtroNombre As String, ByVal filtroCategoria As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = filtroNombre
      arParms(2) = New SqlParameter("@Categoria", SqlDbType.VarChar)
      arParms(2).Value = filtroCategoria

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Pagina_ObtenerListadoConFiltro", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle de la pagina consultada
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 15/09/2009</remarks>
  Public Shared Function ObtenerDetalle(ByVal idPagina As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdPagina", SqlDbType.Int)
      arParms(0).Value = idPagina

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Pagina_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba una pagina
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <param name="titulo"></param>
  ''' <param name="nombreArchivo"></param>
  ''' <param name="nombreCategoria"></param>
  ''' <param name="estado"></param>
  ''' <param name="mostrarEnMenu"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarDatos(ByRef idPagina As String, ByVal titulo As String, ByVal nombreArchivo As String, ByVal nombreCategoria As String, _
                                     ByVal estado As String, ByVal mostrarEnMenu As String, ByVal listadoIdPerfiles As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Pagina_GrabarDatos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(7) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPagina", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@Titulo", SqlDbType.VarChar)
      arParms(2).Value = titulo
      arParms(3) = New SqlParameter("@NombreArchivo", SqlDbType.VarChar)
      arParms(3).Value = nombreArchivo
      arParms(4) = New SqlParameter("@NombreCategoria", SqlDbType.VarChar)
      arParms(4).Value = nombreCategoria
      arParms(5) = New SqlParameter("@Estado", SqlDbType.Int)
      arParms(5).Value = estado
      arParms(6) = New SqlParameter("@MostrarEnMenu", SqlDbType.Int)
      arParms(6).Value = mostrarEnMenu
      arParms(7) = New SqlParameter("@ListadoIdPerfiles", SqlDbType.VarChar)
      arParms(7).Value = listadoIdPerfiles

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      idPagina = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Pagina.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Pagina.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' modifica una pagina
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <param name="titulo"></param>
  ''' <param name="nombreArchivo"></param>
  ''' <param name="nombreCategoria"></param>
  ''' <param name="estado"></param>
  ''' <param name="mostrarEnMenu"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ModificarDatos(ByVal idPagina As String, ByVal titulo As String, ByVal nombreArchivo As String, ByVal nombreCategoria As String, _
                                        ByVal estado As String, ByVal mostrarEnMenu As String, ByVal listadoIdPerfiles As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Pagina_ModificarDatos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(7) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPagina", SqlDbType.Int)
      arParms(1).Value = idPagina
      arParms(2) = New SqlParameter("@Titulo", SqlDbType.VarChar)
      arParms(2).Value = titulo
      arParms(3) = New SqlParameter("@NombreArchivo", SqlDbType.VarChar)
      arParms(3).Value = nombreArchivo
      arParms(4) = New SqlParameter("@NombreCategoria", SqlDbType.VarChar)
      arParms(4).Value = nombreCategoria
      arParms(5) = New SqlParameter("@Estado", SqlDbType.Int)
      arParms(5).Value = estado
      arParms(6) = New SqlParameter("@MostrarEnMenu", SqlDbType.Int)
      arParms(6).Value = mostrarEnMenu
      arParms(7) = New SqlParameter("@ListadoIdPerfiles", SqlDbType.VarChar)
      arParms(7).Value = listadoIdPerfiles

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Pagina.ModificarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Pagina.ModificarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para la pagina seleccionada
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <param name="valor"></param>
  ''' <param name="campoARevisar"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 15/09/2009</remarks>
  Public Shared Function VerificarDuplicidad(ByVal idPagina As String, ByVal valor As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Pagina_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPagina", SqlDbType.Int)
      arParms(1).Value = idPagina
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' graba o actualiza una funcion de una pagina
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <param name="idFuncion"></param>
  ''' <param name="nombre"></param>
  ''' <param name="llave"></param>
  ''' <param name="orden"></param>
  ''' <returns></returns>
  ''' <remarks>POr VSR, 16/09/2009</remarks>
  Public Shared Function GrabarFuncion(ByVal idPagina As String, ByVal idFuncion As String, ByVal nombre As String, ByVal llave As String, ByVal orden As String) As String
    Dim retorno As String
    Try
      Dim storedProcedure As String = "spu_Pagina_GrabarFuncion"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPagina", SqlDbType.Int)
      arParms(1).Value = idPagina
      arParms(2) = New SqlParameter("@IdFuncion", SqlDbType.Int)
      arParms(2).Value = idFuncion
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre
      arParms(4) = New SqlParameter("@Llave", SqlDbType.VarChar)
      arParms(4).Value = llave
      arParms(5) = New SqlParameter("@Orden", SqlDbType.Int)
      arParms(5).Value = orden

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      retorno = arParms(0).Value
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

  ''' <summary>
  ''' elimina las funciones que no esten en la lista
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <param name="listadoIdFuncionAMantener"></param>
  ''' <returns></returns>
  ''' <remarks>POr VSR, 16/09/2009</remarks>
  Public Shared Function EliminarFuncionesNoExistentes(ByVal idPagina As String, ByVal listadoIdFuncionAMantener As String) As String
    Dim retorno As String
    Try
      Dim storedProcedure As String = "spu_Pagina_EliminarFuncionesNoExistentes"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPagina", SqlDbType.Int)
      arParms(1).Value = idPagina
      arParms(2) = New SqlParameter("@ListadoIdFuncion", SqlDbType.VarChar)
      arParms(2).Value = listadoIdFuncionAMantener

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      retorno = arParms(0).Value
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

  ''' <summary>
  ''' elimina una pagina
  ''' </summary>
  ''' <param name="idPagina"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 16/09/2009</remarks>
  Public Shared Function EliminarDatos(ByVal idPagina As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Pagina_EliminarDatos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPagina", SqlDbType.Int)
      arParms(1).Value = idPagina

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Pagina.EliminarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' Obtiene Listado de categorias para poder autocompletar la busqueda
  ''' </summary>
  ''' <param name="Texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AutocompletarCategoria(ByVal texto As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@Texto", SqlDbType.VarChar)
      arParms(0).Value = texto

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Pagina_AutocompletarCategoria", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' verifica si se puede eliminar la funcion al tener referencias asociadas
  ''' </summary>
  ''' <param name="idFuncion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function VerificarEliminacionFuncion(ByVal idFuncion As String) As String
    Dim valor As String
    Try
      Dim storedProcedure As String = "spu_Pagina_VerificarEliminacionFuncion"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@idFuncion", SqlDbType.Int)
      arParms(1).Value = idFuncion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try
    Return valor
  End Function
#End Region

End Class

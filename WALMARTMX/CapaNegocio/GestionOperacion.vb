﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class GestionOperacion
#Region "Constantes y Enum"
  Public Const NombreEntidad As String = "GestionOperacion"
  Public Const SIN_TELEFONO As String = "SIN TELEFONO"
  Public Const ESTADO_VIAJE_ASIGNADO As String = "ASIGNADO"
  Public Const IMPORTACION As String = "Importacion"
  Public Const EXPORTACION As String = "Exportacion"
  Public Const BLOQUE_DATOS_LLEGADA As String = "DatosLlegada"
  Public Const BLOQUE_CARGA_SUELTA As String = "CargaSuelta"
  Public Const CAMPO_MOSTRAR_OBSERVACION As String = "Observacion"
  Public Const CAMPO_MOSTRAR_FECHA_LLEGADA As String = "FechaLlegada"
  Public Const CAMPO_MOSTRAR_EXPORTACION As String = "Exportacion"

#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene listado asignación viajes importación
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerListadoAsignacionViajesImportacion(ByVal idUsuario As String, ByVal fechaPresentacion As String, ByVal horaPresentacion As String, ByVal nombreCliente As String, _
                                                                   ByVal tipoCarga As String, ByVal dimension As String, ByVal retiro As String, ByVal puertoDescarga As String, _
                                                                   ByVal diasLibres As String, ByVal nroOrdenServicio As String, ByVal fechaProgramacion As String, ByVal clasificacionConductor As String, _
                                                                   ByVal nroGuiaDespacho As String, ByVal soloGuiaDespacho As String, ByVal contenedor As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(14) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@FechaPresentacion", SqlDbType.VarChar)
      arParms(1).Value = fechaPresentacion
      arParms(2) = New SqlParameter("@HoraPresentacion", SqlDbType.VarChar)
      arParms(2).Value = horaPresentacion
      arParms(3) = New SqlParameter("@NombreCliente", SqlDbType.VarChar)
      arParms(3).Value = nombreCliente
      arParms(4) = New SqlParameter("@TipoCarga", SqlDbType.VarChar)
      arParms(4).Value = tipoCarga
      arParms(5) = New SqlParameter("@Dimension", SqlDbType.VarChar)
      arParms(5).Value = dimension
      arParms(6) = New SqlParameter("@Retiro", SqlDbType.VarChar)
      arParms(6).Value = retiro
      arParms(7) = New SqlParameter("@PuertoDescarga", SqlDbType.VarChar)
      arParms(7).Value = puertoDescarga
      arParms(8) = New SqlParameter("@DiasLibres", SqlDbType.VarChar)
      arParms(8).Value = diasLibres
      arParms(9) = New SqlParameter("@NumeroOrdenServicio", SqlDbType.VarChar)
      arParms(9).Value = nroOrdenServicio
      arParms(10) = New SqlParameter("@FechaCreacion", SqlDbType.VarChar)
      arParms(10).Value = fechaProgramacion
      arParms(11) = New SqlParameter("@ClasificacionConductor", SqlDbType.VarChar)
      arParms(11).Value = clasificacionConductor
      arParms(12) = New SqlParameter("@NumeroGuiaDespacho", SqlDbType.Int)
      arParms(12).Value = nroGuiaDespacho
      arParms(13) = New SqlParameter("@SoloGuiaDespacho", SqlDbType.Int)
      arParms(13).Value = soloGuiaDespacho
      arParms(14) = New SqlParameter("@Contenedor", SqlDbType.VarChar)
      arParms(14).Value = contenedor

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerListadoAsignacionViajesImportacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle asignacion viaje importacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalleAsignacionViajeImportacion(ByVal idImportacion As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdImportacion", SqlDbType.Int)
      arParms(0).Value = idImportacion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerDetalleAsignacionViajeImportacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba las asignacion del conductor en importacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarAsignarConductorImportacion(ByVal idUsuario As String, ByVal idImportacion As String, ByVal nombreConductor As String, ByVal rutConductor As String, _
                                                           ByVal telefono As String, ByVal patenteTracto As String, ByVal patenteTrailer As String, ByVal tipoCamion As String, _
                                                           ByVal fechaEntrada As String, ByVal origenDatoTabla As String, ByVal origenDatoIdRegistro As String) As String
    Dim valor As String

    Try
      Dim arParms() As SqlParameter = New SqlParameter(11) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@IdImportacion", SqlDbType.Int)
      arParms(2).Value = idImportacion
      arParms(3) = New SqlParameter("@NombreConductor", SqlDbType.VarChar)
      arParms(3).Value = nombreConductor
      arParms(4) = New SqlParameter("@RutConductor", SqlDbType.VarChar)
      arParms(4).Value = rutConductor
      arParms(5) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(5).Value = telefono
      arParms(6) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      arParms(6).Value = patenteTracto
      arParms(7) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      arParms(7).Value = patenteTrailer
      arParms(8) = New SqlParameter("@TipoCamion", SqlDbType.VarChar)
      arParms(8).Value = tipoCamion
      arParms(9) = New SqlParameter("@FechaEntrada", SqlDbType.DateTime)
      arParms(9).Value = fechaEntrada
      arParms(10) = New SqlParameter("@OrigenDatoTabla", SqlDbType.VarChar)
      arParms(10).Value = origenDatoTabla
      arParms(11) = New SqlParameter("@OrigenDatoIdRegistro", SqlDbType.VarChar)
      arParms(11).Value = origenDatoIdRegistro

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_GrabarAsignarConductorImportacion", arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar GestionOperacion.GrabarAsignarConductorImportacion" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar GestionOperacion.GrabarAsignarConductorImportacion" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene listado de conductores para asignacion manual
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerListadoConductoresAsignacionManual(ByVal rutConductorAsignado As String, ByVal nombre As String, ByVal rut As String, ByVal patente As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@RutConductorAsignado", SqlDbType.VarChar)
      arParms(0).Value = rutConductorAsignado
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = nombre
      arParms(2) = New SqlParameter("@Rut", SqlDbType.VarChar)
      arParms(2).Value = rut
      arParms(3) = New SqlParameter("@Patente", SqlDbType.VarChar)
      arParms(3).Value = patente

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerListadoConductoresAsignacionManual", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' actualiza registro en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ActualizarRegistroEnBaseDatos(ByVal origenDatoTabla As String, ByVal origenDatoIdRegistro As String, ByVal telefono As String, ByVal patenteTracto As String, ByVal patenteTrailer As String) As String
    Dim valor As String

    Try
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@OrigenDatoTabla", SqlDbType.VarChar)
      arParms(1).Value = origenDatoTabla
      arParms(2) = New SqlParameter("@OrigenDatoIdRegistro", SqlDbType.Int)
      arParms(2).Value = origenDatoIdRegistro
      arParms(3) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(3).Value = telefono
      arParms(4) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      arParms(4).Value = Sistema.Capitalize(patenteTracto, Sistema.eTipoCapitalizacion.TodoMayuscula)
      arParms(5) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      arParms(5).Value = Sistema.Capitalize(patenteTrailer, Sistema.eTipoCapitalizacion.TodoMayuscula)

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ActualizarRegistroEnBaseDatos", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' desasigna conductor en importación
  ''' </summary>
  Public Shared Function DesasignarConductorImportacion(ByVal idImportacion As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdImportacion", SqlDbType.Int)
      arParms(1).Value = idImportacion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_DesasignarConductorImportacion", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar GestionOperacion.DesasignarConductorImportacion" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene listado asignación viajes exportación
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerListadoAsignacionViajesExportacion(ByVal idUsuario As String, ByVal fechaPresentacion As String, ByVal horaPresentacion As String, ByVal tipoContenedor As String, _
                                                                   ByVal nombreNaviera As String, ByVal tipoCarga As String, ByVal dimension As String, ByVal puertoDescarga As String, _
                                                                   ByVal puertoEmbarque As String, ByVal nroOrdenServicio As String, ByVal fechaProgramacion As String, ByVal clasificacionConductor As String, _
                                                                   ByVal nroGuiaDespacho As String, ByVal soloGuiaDespacho As String, ByVal contenedor As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(14) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@FechaPresentacion", SqlDbType.VarChar)
      arParms(1).Value = fechaPresentacion
      arParms(2) = New SqlParameter("@HoraPresentacion", SqlDbType.VarChar)
      arParms(2).Value = horaPresentacion
      arParms(3) = New SqlParameter("@TipoContenedor", SqlDbType.VarChar)
      arParms(3).Value = tipoContenedor
      arParms(4) = New SqlParameter("@NombreNaviera", SqlDbType.VarChar)
      arParms(4).Value = nombreNaviera
      arParms(5) = New SqlParameter("@TipoCarga", SqlDbType.VarChar)
      arParms(5).Value = tipoCarga
      arParms(6) = New SqlParameter("@Dimension", SqlDbType.VarChar)
      arParms(6).Value = dimension
      arParms(7) = New SqlParameter("@PuertoDescarga", SqlDbType.VarChar)
      arParms(7).Value = puertoDescarga
      arParms(8) = New SqlParameter("@PuertoEmbarque", SqlDbType.VarChar)
      arParms(8).Value = puertoEmbarque
      arParms(9) = New SqlParameter("@NumeroOrdenServicio", SqlDbType.VarChar)
      arParms(9).Value = nroOrdenServicio
      arParms(10) = New SqlParameter("@FechaCreacion", SqlDbType.VarChar)
      arParms(10).Value = fechaProgramacion
      arParms(11) = New SqlParameter("@ClasificacionConductor", SqlDbType.VarChar)
      arParms(11).Value = clasificacionConductor
      arParms(12) = New SqlParameter("@NumeroGuiaDespacho", SqlDbType.Int)
      arParms(12).Value = nroGuiaDespacho
      arParms(13) = New SqlParameter("@SoloGuiaDespacho", SqlDbType.Int)
      arParms(13).Value = soloGuiaDespacho
      arParms(14) = New SqlParameter("@Contenedor", SqlDbType.VarChar)
      arParms(14).Value = contenedor

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerListadoAsignacionViajesExportacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle asignacion viaje exportacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalleAsignacionViajeExportacion(ByVal idExportacion As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdExportacion", SqlDbType.Int)
      arParms(0).Value = idExportacion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerDetalleAsignacionViajeExportacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba las asignacion del conductor en exportacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarAsignarConductorExportacion(ByVal idUsuario As String, ByVal idExportacion As String, ByVal nombreConductor As String, ByVal rutConductor As String, _
                                                           ByVal telefono As String, ByVal patenteTracto As String, ByVal patenteTrailer As String, ByVal tipoCamion As String, _
                                                           ByVal fechaEntrada As String, ByVal origenDatoTabla As String, ByVal origenDatoIdRegistro As String) As String
    Dim valor As String

    Try
      Dim arParms() As SqlParameter = New SqlParameter(11) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@IdExportacion", SqlDbType.Int)
      arParms(2).Value = idExportacion
      arParms(3) = New SqlParameter("@NombreConductor", SqlDbType.VarChar)
      arParms(3).Value = nombreConductor
      arParms(4) = New SqlParameter("@RutConductor", SqlDbType.VarChar)
      arParms(4).Value = rutConductor
      arParms(5) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(5).Value = telefono
      arParms(6) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      arParms(6).Value = patenteTracto
      arParms(7) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      arParms(7).Value = patenteTrailer
      arParms(8) = New SqlParameter("@TipoCamion", SqlDbType.VarChar)
      arParms(8).Value = tipoCamion
      arParms(9) = New SqlParameter("@FechaEntrada", SqlDbType.DateTime)
      arParms(9).Value = fechaEntrada
      arParms(10) = New SqlParameter("@OrigenDatoTabla", SqlDbType.VarChar)
      arParms(10).Value = origenDatoTabla
      arParms(11) = New SqlParameter("@OrigenDatoIdRegistro", SqlDbType.VarChar)
      arParms(11).Value = origenDatoIdRegistro

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_GrabarAsignarConductorExportacion", arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar GestionOperacion.GrabarAsignarConductorExportacion" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar GestionOperacion.GrabarAsignarConductorExportacion" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' desasigna conductor en exportación
  ''' </summary>
  Public Shared Function DesasignarConductorExportacion(ByVal idExportacion As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdExportacion", SqlDbType.Int)
      arParms(1).Value = idExportacion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_DesasignarConductorExportacion", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar GestionOperacion.DesasignarConductorExportacion" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene historial asignación viajes importación
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerHistorialAsignacionViajesImportacion(ByVal numeroOrdenServicio As String, ByVal numeroLineaMO As String, ByVal versionMO As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@NumeroOrdenServicio", SqlDbType.Int)
      arParms(0).Value = numeroOrdenServicio
      arParms(1) = New SqlParameter("@NumeroLineaMO", SqlDbType.Int)
      arParms(1).Value = numeroLineaMO
      arParms(2) = New SqlParameter("@VersionMO", SqlDbType.Int)
      arParms(2).Value = versionMO

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerHistorialAsignacionViajesImportacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene historial asignación viajes exportación
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerHistorialAsignacionViajesExportacion(ByVal numeroOrdenServicio As String, ByVal numeroLineaMO As String, ByVal versionMO As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@NumeroOrdenServicio", SqlDbType.Int)
      arParms(0).Value = numeroOrdenServicio
      arParms(1) = New SqlParameter("@NumeroLineaMO", SqlDbType.Int)
      arParms(1).Value = numeroLineaMO
      arParms(2) = New SqlParameter("@VersionMO", SqlDbType.Int)
      arParms(2).Value = versionMO

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerHistorialAsignacionViajesExportacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba datos extras importacion
  ''' </summary>
  Public Shared Function GrabarDatosExtraImportacion(ByVal idImportacion As String, ByVal fechaLlegada As String, ByVal horaLlegada As String, ByVal observacionLlegada As String, _
                                                     ByVal cantidadPallet As String, ByVal pesoReal As String, ByVal ordenCompra As String, ByVal fechaSalida As String, ByVal horaSalida As String, _
                                                     ByVal nroSello As String, ByVal nroContenedor As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(11) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdImportacion", SqlDbType.Int)
      arParms(1).Value = idImportacion
      arParms(2) = New SqlParameter("@FechaLlegada", SqlDbType.VarChar)
      arParms(2).Value = fechaLlegada
      arParms(3) = New SqlParameter("@HoraLlegada", SqlDbType.VarChar)
      arParms(3).Value = horaLlegada
      arParms(4) = New SqlParameter("@ObservacionLlegada", SqlDbType.Text)
      arParms(4).Value = observacionLlegada
      arParms(5) = New SqlParameter("@CantidadPallet", SqlDbType.Int)
      arParms(5).Value = cantidadPallet
      arParms(6) = New SqlParameter("@PesoReal", SqlDbType.Float)
      arParms(6).Value = pesoReal
      arParms(7) = New SqlParameter("@OrdenCompra", SqlDbType.BigInt)
      arParms(7).Value = ordenCompra
      arParms(8) = New SqlParameter("@FechaSalida", SqlDbType.VarChar)
      arParms(8).Value = fechaSalida
      arParms(9) = New SqlParameter("@HoraSalida", SqlDbType.VarChar)
      arParms(9).Value = horaSalida
      arParms(10) = New SqlParameter("@NumeroSello", SqlDbType.VarChar)
      arParms(10).Value = nroSello
      arParms(11) = New SqlParameter("@NumeroContenedor", SqlDbType.VarChar)
      arParms(11).Value = Sistema.Capitalize(nroContenedor, Sistema.eTipoCapitalizacion.TodoMayuscula)

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_GrabarDatosExtraImportacion", arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar GestionOperacion.GrabarDatosExtraImportacion" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar GestionOperacion.GrabarDatosExtraImportacion" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene datos extra de importacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDatoExtraImportacion(ByVal idImportacion As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdImportacion", SqlDbType.Int)
      arParms(0).Value = idImportacion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerDatoExtraImportacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene datos extra de exportacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDatoExtraExportacion(ByVal idExportacion As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdExportacion", SqlDbType.Int)
      arParms(0).Value = idExportacion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_ObtenerDatoExtraExportacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba datos extras exportacion
  ''' </summary>
  Public Shared Function GrabarDatosExtraExportacion(ByVal idExportacion As String, ByVal fechaLlegada As String, ByVal horaLlegada As String, ByVal observacionLlegada As String, _
                                                     ByVal cantidadPallet As String, ByVal pesoReal As String, ByVal ordenCompra As String, ByVal fechaSalida As String, ByVal horaSalida As String, _
                                                     ByVal nroSello As String, ByVal nroContenedor As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(11) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdExportacion", SqlDbType.Int)
      arParms(1).Value = idExportacion
      arParms(2) = New SqlParameter("@FechaLlegada", SqlDbType.VarChar)
      arParms(2).Value = fechaLlegada
      arParms(3) = New SqlParameter("@HoraLlegada", SqlDbType.VarChar)
      arParms(3).Value = horaLlegada
      arParms(4) = New SqlParameter("@ObservacionLlegada", SqlDbType.Text)
      arParms(4).Value = observacionLlegada
      arParms(5) = New SqlParameter("@CantidadPallet", SqlDbType.Int)
      arParms(5).Value = cantidadPallet
      arParms(6) = New SqlParameter("@PesoReal", SqlDbType.Float)
      arParms(6).Value = pesoReal
      arParms(7) = New SqlParameter("@OrdenCompra", SqlDbType.BigInt)
      arParms(7).Value = ordenCompra
      arParms(8) = New SqlParameter("@FechaSalida", SqlDbType.VarChar)
      arParms(8).Value = fechaSalida
      arParms(9) = New SqlParameter("@HoraSalida", SqlDbType.VarChar)
      arParms(9).Value = horaSalida
      arParms(10) = New SqlParameter("@NumeroSello", SqlDbType.VarChar)
      arParms(10).Value = nroSello
      arParms(11) = New SqlParameter("@NumeroContenedor", SqlDbType.VarChar)
      arParms(11).Value = Sistema.Capitalize(nroContenedor, Sistema.eTipoCapitalizacion.TodoMayuscula)

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_GestionOperacion_GrabarDatosExtraExportacion", arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar GestionOperacion.GrabarDatosExtraExportacion" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar GestionOperacion.GrabarDatosExtraExportacion" & vbCrLf & Err.Description)
    End Try
  End Function
#End Region

End Class

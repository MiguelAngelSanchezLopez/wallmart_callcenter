﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Carrier
#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Carrier"
  Public Const ENTIDAD As String = "Carrier_ViajeSolicitado"
  Public Const ESTADO_TRANSPORTISTA_COBRO_RECLAMO As String = "Cobro reclamo"
  '-- estado usados 
  Public Const ESTADO_ENVIAR_TRANSPORTISTA As String = "ENVIAR TRANSPORTISTA"
  Public Const ESTADO_FINALIZADO As String = "FINALIZADO"
  '-- estado usados por Prevension Perdida (PP)
  Public Const ESTADO_VENTA_NEGATIVA As String = "VENTA NEGATIVA"
  Public Const ESTADO_AJUSTE_PI As String = "AJUSTE PI"
  Public Const ESTADO_SOLICITO_REVERSA As String = "SOLICITO REVERSA"
  Public Const PERFIL_LINEA_TRANSPORTE As String = "LDT"
#End Region

#Region "Constructores"
  Public Sub New()
    MyBase.New()
  End Sub

#End Region

#Region "Metodos compartidos"
  ''' <summary>
  ''' obtiene listado de viajes para analizar
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoViajeAnalizar(ByVal nombreTransportista As String, ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal numeroViaje As Integer, _
                                                     ByVal soloConAlertas As String) As DataSet
    Dim ds As New DataSet

    Try
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@nombreTransportista", SqlDbType.VarChar)
      arParms(0).Value = nombreTransportista
      arParms(1) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
      arParms(1).Value = fechaDesde
      arParms(2) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
      arParms(2).Value = fechaHasta
      arParms(3) = New SqlParameter("@numeroViaje", SqlDbType.Int)
      arParms(3).Value = numeroViaje
      arParms(4) = New SqlParameter("@SoloConAlertas", SqlDbType.Int)
      arParms(4).Value = soloConAlertas

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerViajesAnalizar", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de viajes para analizar
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalleViajeAnalizar(ByVal nombreTransportista As String, ByVal numeroViaje As String, _
                                                     ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal conAlertas As String) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(4) {}
    arParms(0) = New SqlParameter("@nombreTransportista", SqlDbType.VarChar)
    arParms(0).Value = nombreTransportista
    arParms(1) = New SqlParameter("@numeroViaje", SqlDbType.Int)
    arParms(1).Value = numeroViaje
    arParms(2) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
    arParms(2).Value = fechaDesde
    arParms(3) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
    arParms(3).Value = fechaHasta
    arParms(4) = New SqlParameter("@SoloConAlertas", SqlDbType.Int)
    arParms(4).Value = conAlertas

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerViajesAnalizarDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de viajes con solicitud
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalleViajeSolicitado(ByVal nTransportista As String, ByVal fechaSolicitudCF As String, ByVal fechaCitaTransportista As String, ByVal estado As String) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(3) {}
    arParms(0) = New SqlParameter("@nombreTransportista", SqlDbType.VarChar)
    arParms(0).Value = nTransportista
    arParms(1) = New SqlParameter("@fechaSolicitudCF", SqlDbType.VarChar)
    arParms(1).Value = fechaSolicitudCF
    arParms(2) = New SqlParameter("@fechaCitaTransportista", SqlDbType.VarChar)
    arParms(2).Value = fechaCitaTransportista
    arParms(3) = New SqlParameter("@Estado", SqlDbType.VarChar)
    arParms(3).Value = estado

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerViajesSolicitadoDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' Graba los viajes que han sido solicitados para su informe
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GrabarViajeSolicitado(ByVal numeroViaje As String, ByVal estado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(4) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@numeroViaje", SqlDbType.Int)
      arParms(2).Value = numeroViaje
      arParms(3) = New SqlParameter("@estado", SqlDbType.VarChar)
      arParms(3).Value = estado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_GrabarViajeSolicitado", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor

  End Function

  ''' <summary>
  ''' obtiene listado de viajes solicitados
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoViajeSolicitado(ByVal nombreTransportista As String, ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal estado As String) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(3) {}
    arParms(0) = New SqlParameter("@nombreTransportista", SqlDbType.VarChar)
    arParms(0).Value = nombreTransportista
    arParms(1) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
    arParms(1).Value = fechaDesde
    arParms(2) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
    arParms(2).Value = fechaHasta
    arParms(3) = New SqlParameter("@Estado", SqlDbType.VarChar)
    arParms(3).Value = estado

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerViajesSolicitar", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de alertas para el viajes con solicitud
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalleAlertaViajeSolicitado(ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@nroTransporte", SqlDbType.VarChar)
    arParms(0).Value = nroTransporte

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerViajesSolicitadoAlertaDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' Graba alerta de viaje que han sido solicitado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GrabarAlertaViajeSolicitado(ByVal idAlerta As String, ByVal idViajeSolicitado As String, ByVal estado As Boolean) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@idAlerta", SqlDbType.VarChar)
      arParms(1).Value = idAlerta
      arParms(2) = New SqlParameter("@idViajeSolicitado", SqlDbType.Int)
      arParms(2).Value = idViajeSolicitado
      arParms(3) = New SqlParameter("@estado", SqlDbType.Int)
      arParms(3).Value = estado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_GrabarAlertaViajeSolicitado", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor

  End Function

  ''' <summary>
  ''' obtiene informacion plan accion del transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalleAlertaCitaTransportista(ByVal idViajeSolicitado As String) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@nroTransporte", SqlDbType.VarChar)
    arParms(0).Value = idViajeSolicitado

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerDetalleAlertaCitaTransportista", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba el plan de accion asignado al transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GrabarPlanAccionTransportista(ByVal nroTransporte As Integer, ByVal listaPlanAccion As String, ByVal montoCobroReclamo As String, ByVal estado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte
      arParms(2) = New SqlParameter("@ListadoPlanAccion", SqlDbType.VarChar)
      arParms(2).Value = listaPlanAccion
      arParms(3) = New SqlParameter("@MontoCobroReclamo", SqlDbType.BigInt)
      arParms(3).Value = montoCobroReclamo
      arParms(4) = New SqlParameter("@Estado", SqlDbType.VarChar)
      arParms(4).Value = estado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_GrabarPlanAccionTransportista", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor

  End Function

  ''' <summary>
  ''' obtiene listado de PDF de auditoria de los locales relacionados al viaje
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoPDFAuditoria(ByVal nroViaje As String) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(0) {}
    arParms(0) = New SqlParameter("@nroViaje", SqlDbType.VarChar)
    arParms(0).Value = nroViaje

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerListadoPDFAuditoria", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de viajes para analizar
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoPrevensionPerdida(ByVal nombreTransportista As String, ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal numeroViaje As Integer, _
                                                         ByVal estado As String) As DataSet
    Dim ds As New DataSet

    Try
      Dim arParms() As SqlParameter = New SqlParameter(4) {}
      arParms(0) = New SqlParameter("@nombreTransportista", SqlDbType.VarChar)
      arParms(0).Value = nombreTransportista
      arParms(1) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
      arParms(1).Value = fechaDesde
      arParms(2) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
      arParms(2).Value = fechaHasta
      arParms(3) = New SqlParameter("@numeroViaje", SqlDbType.Int)
      arParms(3).Value = numeroViaje
      arParms(4) = New SqlParameter("@Estado", SqlDbType.VarChar)
      arParms(4).Value = estado

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerListadoPrevensionPerdida", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle del viaje para prevension de perdida
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetallePrevensionPerdida(ByVal nombreTransportista As String, ByVal numeroViaje As String, ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal estado As String) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(4) {}
    arParms(0) = New SqlParameter("@nombreTransportista", SqlDbType.VarChar)
    arParms(0).Value = nombreTransportista
    arParms(1) = New SqlParameter("@numeroViaje", SqlDbType.Int)
    arParms(1).Value = numeroViaje
    arParms(2) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
    arParms(2).Value = fechaDesde
    arParms(3) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
    arParms(3).Value = fechaHasta
    arParms(4) = New SqlParameter("@Estado", SqlDbType.VarChar)
    arParms(4).Value = estado

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ObtenerDetallePrevensionPerdida", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  Public Shared Function getTractosDisponibles(ByVal idTransportista As Integer) As DataTable

    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@idUsuarioTransportista", SqlDbType.Int)
    arParms(0).Value = idTransportista

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_getTractoDisponibles", arParms)

      'retorna valor
      Return ds.Tables(0)

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Public Shared Function getRemolquesEnPatio(ByVal idTransportista As Integer) As DataSet

    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@idUsuarioTransportista", SqlDbType.Int)
    arParms(0).Value = idTransportista

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_getRemolquesEnPatio", arParms)

      'retorna valor
      Return ds

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Public Shared Function getRemolquesEnCortina(ByVal idTransportista As Integer) As DataSet

    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@idUsuarioTransportista", SqlDbType.Int)
    arParms(0).Value = idTransportista

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_getRemolquesEnCortina", arParms)

      'retorna valor
      Return ds

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Public Shared Function getEmbarqueEnRuta(ByVal idTransportista As Integer) As DataSet

    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@idUsuarioTransportista", SqlDbType.Int)
    arParms(0).Value = idTransportista

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_getEmbarqueEnRuta", arParms)

      'retorna valor
      Return ds

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Public Shared Function getEmbarqueEnTienda(ByVal idTransportista As Integer) As DataSet

    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@idUsuarioTransportista", SqlDbType.Int)
    arParms(0).Value = idTransportista

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_getEmbarqueEnTienda", arParms)

      'retorna valor
      Return ds

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Public Shared Function getTractoRegresando(ByVal idTransportista As Integer) As DataSet

    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@idUsuarioTransportista", SqlDbType.Int)
    arParms(0).Value = idTransportista

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_getTractoRegresando", arParms)

      'retorna valor
      Return ds

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  Public Shared Function GetDetalleAlertaEmbarqueEnRuta(ByVal idMaster As Long, ByVal idEmbarque As Long) As DataTable

    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(2) {}
    arParms(0) = New SqlParameter("@IdMaster", SqlDbType.BigInt)
    arParms(0).Value = idMaster
    arParms(1) = New SqlParameter("@IdEmbarque", SqlDbType.BigInt)
    arParms(1).Value = idEmbarque

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_getDetalleAlertaEmbarqueEnRuta", arParms)

      'retorna valor
      Return ds.Tables(0)

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  ''' <summary>
  ''' Retorna infomración de los informes
  ''' </summary>
  ''' <param name="pIdUsuario"> Id del usuario asosicado al reporte</param>
  ''' <param name="pFecIni">Fecha inicial de busqueda</param>
  ''' <param name="pFecFin">Fecha final de busqueda</param>
  ''' <param name="pTipoInforme">Tipo Informe</param>
  ''' <returns>Retorna un DataSet con la información</returns>
  Public Shared Function ListarInforme(ByVal pIdUsuario As Integer, ByVal pFecIni As String, ByVal pFecFin As String, ByVal pTipoInforme As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(4) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = pIdUsuario
      arParms(1) = New SqlParameter("@Fech_Ini", SqlDbType.VarChar)
      arParms(1).Value = pFecIni
      arParms(2) = New SqlParameter("@Fech_Fin", SqlDbType.VarChar)
      arParms(2).Value = pFecFin
      arParms(3) = New SqlParameter("@TipoInforme", SqlDbType.VarChar)
      arParms(3).Value = pTipoInforme
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_ListarInforme", arParms)

    Catch ex As Exception
      Dim err As String = ex.Message.ToString()
    End Try

    Return ds
  End Function

  ''' <summary>
  ''' Retorna los datos del archivo
  ''' </summary>
  ''' <param name="pIdInforme">Id del informe del archivo</param>
  ''' <returns>Retorna un DataSet con la información</returns>
  ''' <remarks></remarks>
  Public Shared Function InformeArchivo(ByVal pIdInforme As Integer) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(4) {}
      arParms(0) = New SqlParameter("@IdInforme", SqlDbType.Int)
      arParms(0).Value = pIdInforme

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Carrier_InformeArchivo", arParms)
    Catch ex As Exception
      Dim err As String = ex.Message.ToString()
    End Try

    Return ds
  End Function


#End Region
End Class

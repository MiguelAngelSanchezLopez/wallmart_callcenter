Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Perfil

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Perfil"
  Public Const KEY_ADMINISTRADOR As String = "ADM"
  Public Const KEY_CONDUCTOR As String = "CON"
  Public Const KEY_TELEOPERADOR As String = "TO"
  Public Const KEY_SUPERVISOR As String = "SUP"
  Public Const KEY_GENERICO As String = "GEN"
  Public Const KEY_TRANSPORTISTA As String = "TRA"
  Public Const KEY_SUPERVISOR_PROTECCION_ACTIVOS As String = "SPA"
  Public Const KEY_JEFE_AREA_PROTECCION_ACTIVOS As String = "JPA"
  Public Const KEY_SUBGERENTE_PROTECCION_ACTIVOS As String = "SBPA"
  Public Const KEY_JEFE_AREA_TRANSPORTES As String = "JAT"
  Public Const KEY_SUBGERENTE_TRANSPORTES As String = "SDT"
  Public Const KEY_SUPERVISOR_CONTROL_FLOTA_CEDIS As String = "SCFC"
  Public Const KEY_GERENCIA_TRANSPORTES As String = "GT"
  Public Const KEY_COMMANAGER As String = "CM"
  Public Const KEY_GERENTE_TIENDA As String = "GRT"
  Public Const KEY_GERENTE_DISTRITAL_TIENDA As String = "GDT"
  Public Const KEY_LIDER_RECIBO_TIENDA As String = "LRT"
  Public Const KEY_COORDINADOR_LINEA_TRANSPORTE As String = "CLT"
  Public Const KEY_SUPERVISOR_LINEA_TRANSPORTE As String = "LDT"

  Public Enum eTipo As Integer
    Administrador = 1
    Conductor = 2
    TeleOperador = 3
    Supervisor = 4
    Generico = 5
    Transportista = 15
    SupervisorProteccionActivos = 44
    JefeAreaProteccionActivos = 45
    SubGerenteProteccionActivos = 46
    JefeAreaTransportes = 47
    SubgerenteTransportes = 48
    SupervisorControlFlotaCedis = 49
    GerenciaTransportes = 50
    ComManager = 51
    GerenteTienda = 52
    GerenteDistritalTienda = 53
    LiderReciboTienda = 54
    CoordinadorLineaTransporte = 55
    SupervisorLineaTransporte = 56
  End Enum

  Public Enum ePagina As Integer
    ListadoCategorias = 0
    ListadoPaginas = 1
    ListadoFuncionesPagina = 2
  End Enum

#End Region

#Region "Campos"
  Private _id As Integer
  Private _nombre As String
  Private _llave As String
  Private _existe As Boolean
  Private _paginaInicio As String
#End Region

#Region "Properties"
  Public Property [Id]() As Integer
    Get
      Return _id
    End Get
    Set(ByVal Value As Integer)
      _id = Value
    End Set
  End Property

  Public Property Nombre() As String
    Get
      Return _nombre
    End Get
    Set(ByVal Value As String)
      _nombre = Value
    End Set
  End Property

  Public Property Llave() As String
    Get
      Return _llave
    End Get
    Set(ByVal Value As String)
      _llave = Value
    End Set
  End Property

  Public ReadOnly Property Existe() As Boolean
    Get
      Return _existe
    End Get
  End Property

  Public Property PaginaInicio() As String
    Get
      Return _paginaInicio
    End Get
    Set(ByVal Value As String)
      _paginaInicio = Value
    End Set
  End Property

#End Region

#Region "Constructores"
  Public Sub New()
    MyBase.New()
  End Sub

  ''' <summary>
  ''' obtiene perfil a partir de su ID
  ''' </summary>
  ''' <param name="Id"></param>
  ''' <remarks></remarks>
  Public Sub New(ByVal Id As Integer)
    Dim storedProcedure As String = "spu_" & NombreEntidad & "_ObtenerPorId"
    Dim descripcionError As String = "Error al crear una instancia de " & NombreEntidad & " (" & Id & ") "
    Dim ds As DataSet = Nothing
    Dim dr As DataRow = Nothing
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        dr = ds.Tables(0).Rows(0)
        If Not IsDBNull(dr.Item("Id")) Then _id = dr.Item("Id")
        If Not IsDBNull(dr.Item("Nombre")) Then _nombre = dr.Item("Nombre")
        If Not IsDBNull(dr.Item("Llave")) Then _llave = dr.Item("Llave")
        If Not IsDBNull(dr.Item("PaginaInicio")) Then _paginaInicio = dr.Item("PaginaInicio")
        _existe = True
      End If
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      ds = Nothing
    End Try
  End Sub

  ''' <summary>
  ''' obtiene perfil a partir de su LLAVE
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New(ByVal llave As String)
    Dim storedProcedure As String = "spu_" & NombreEntidad & "_ObtenerPorLlave"
    Dim descripcionError As String = "Error al crear una instancia de " & NombreEntidad & " (" & llave & ") "
    Dim ds As DataSet = Nothing
    Dim dr As DataRow = Nothing
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Llave", SqlDbType.VarChar)
      parms(0).Value = llave
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        dr = ds.Tables(0).Rows(0)
        If Not IsDBNull(dr.Item("Id")) Then _id = dr.Item("Id")
        If Not IsDBNull(dr.Item("Nombre")) Then _nombre = dr.Item("Nombre")
        If Not IsDBNull(dr.Item("Llave")) Then _llave = dr.Item("Llave")
        If Not IsDBNull(dr.Item("PaginaInicio")) Then _paginaInicio = dr.Item("PaginaInicio")
        _existe = True
      End If
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      ds = Nothing
    End Try
  End Sub

#End Region

#Region "Metodos Publicos"
  ''' <summary>
  ''' guarda las propiedades de la instancia como un nuevo registro y obtiene su Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub GuardarNuevo()
    Dim storedProcedure As String = "spu_" & NombreEntidad & "_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(3) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Llave", SqlDbType.VarChar)
      parms(3) = New SqlParameter("@PaginaInicio", SqlDbType.VarChar)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = _nombre
      parms(2).Value = _llave
      parms(3).Value = _paginaInicio

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      _id = parms(0).Value
      _existe = True

      Me.Refrescar()
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  ''' <summary>
  ''' guarda (update) todas las propiedades de la instancia en su registro en la BDD (usando el Id)
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Actualizar()
    ' si la instancia no existe no podemos actualizar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_" & NombreEntidad & "_Actualizar"
    Dim descripcionError As String = "Error al actualizar " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(3) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Llave", SqlDbType.VarChar)
      parms(3) = New SqlParameter("@PaginaInicio", SqlDbType.VarChar)

      parms(0).Value = _id
      parms(1).Value = _nombre
      parms(2).Value = _llave
      parms(3).Value = _paginaInicio

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  ''' <summary>
  ''' elimina fisicamente (delete) el registro que corresponde a la instancia
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Eliminar()
    ' si la instancia contiene un identificador no valido no se puede eliminar
    If _id < 1 Then Exit Sub
    ' si la instancia no existe no podemos eliminar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_" & NombreEntidad & "_Eliminar"
    Dim descripcionError As String = "Error al eliminar " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = _id
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try

  End Sub
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' busca sus datos en la BDD (usando el Id) y refresca todas las propiedades de la instancia
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub Refrescar()
    ' si la instancia no existe no podemos refrescar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_" & NombreEntidad & "_ObtenerPorId"
    Dim descripcionError As String = "Error al refrescar una instancia de " & NombreEntidad & " (" & _id & ") "

    Dim ds As DataSet = Nothing
    Dim dr As DataRow = Nothing
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = _id
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        dr = ds.Tables(0).Rows(0)
        'If Not IsDBNull(dr.Item("Id")) Then _id = dr.Item("Id")
        If Not IsDBNull(dr.Item("Nombre")) Then _nombre = dr.Item("Nombre")
        If Not IsDBNull(dr.Item("Llave")) Then _llave = dr.Item("Llave")
        If Not IsDBNull(dr.Item("PaginaInicio")) Then _paginaInicio = dr.Item("PaginaInicio")
        '_existe = True
      End If
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      ds = Nothing
    End Try
  End Sub

#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene listado de paginas asociadas al perfil
  ''' </summary>
  ''' <param name="idPerfil"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 28/07/2009</remarks>
  Public Shared Function ObtenerPaginasAsociadas(ByVal idPerfil As String, ByVal idUsuario As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdPerfil", SqlDbType.Int)
      arParms(0).Value = idPerfil
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Perfil_ObtenerPaginasAsociadas", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de perfiles segun un tipo de perfil especifico
  ''' </summary>
  ''' <param name="llavePerfil"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 10/09/2009</remarks>
  Public Shared Function ObtenerListadoPorTipoPerfil(ByVal llavePerfil As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@LlavePerfil", SqlDbType.VarChar)
      arParms(0).Value = llavePerfil

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Perfil_ObtenerListadoPorTipoPerfil", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de Perfil
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoConFiltro(ByVal idUsuario As String, ByVal nombre As String, ByVal llave As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = nombre
      arParms(2) = New SqlParameter("@Llave", SqlDbType.VarChar)
      arParms(2).Value = llave

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Perfil_ObtenerListadoConFiltro", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle del Perfil consultado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal idPerfil As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdPerfil", SqlDbType.Int)
      arParms(0).Value = idPerfil

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Perfil_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba un Perfil
  ''' </summary>
  Public Shared Function GrabarDatos(ByRef idPerfil As String, ByVal nombre As String, ByVal llave As String, ByVal paginaInicio As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPerfilOutput", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdPerfil", SqlDbType.Int)
      arParms(2).Value = idPerfil
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre
      arParms(4) = New SqlParameter("@Llave", SqlDbType.VarChar)
      arParms(4).Value = Sistema.Capitalize(llave, Sistema.eTipoCapitalizacion.TodoMayuscula)
      arParms(5) = New SqlParameter("@PaginaInicio", SqlDbType.VarChar)
      arParms(5).Value = paginaInicio

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Perfil_GrabarDatos", arParms)
      valor = arParms(0).Value
      idPerfil = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Perfil.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Perfil.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para el perfil seleccionado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function VerificarDuplicidad(ByVal idPerfil As String, ByVal valor As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Perfil_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPerfil", SqlDbType.Int)
      arParms(1).Value = idPerfil
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

#End Region

End Class

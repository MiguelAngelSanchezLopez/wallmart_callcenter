Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web
Imports System.Text
Imports System.Configuration
Imports System.Drawing

Public Class Archivo
  ' el nombre de la entidad en el sistema (el nombre de la tabla tambien)
  Public Const NombreEntidad As String = "Archivo"
  Public Const TamanoMaximoArchivoUploadEnKb As Integer = 10240
  Private sl As New SortedList

#Region "Constantes y Enum"
  Public Enum eTipoArchivo
    image
    audio
    video
    text
    application
    office
    zip
    others
    unknow
  End Enum

#End Region

#Region "Campos"
  Private _id As Integer
  Private _nombre As String
  Private _contentType As String
  Private _fechaCreacion As String
  Private _horaCreacion As String
  Private _idDocumento As Integer
  Private _idUsuario As Integer
  Private _existe As Boolean
#End Region

#Region "Propiedades"

  Public ReadOnly Property Id() As Integer
    Get
      Return _id
    End Get

  End Property

  Public Property Nombre() As String
    Get
      Return _nombre
    End Get
    Set(ByVal value As String)
      _nombre = value
    End Set
  End Property

  Public Property ContentType() As String
    Get
      Return _contentType
    End Get
    Set(ByVal value As String)
      _contentType = value
    End Set
  End Property

  Public Property FechaCreacion() As String
    Get
      Return _fechaCreacion
    End Get
    Set(ByVal value As String)
      _fechaCreacion = value
    End Set
  End Property

  Public Property HoraCreacion() As String
    Get
      Return _horaCreacion
    End Get
    Set(ByVal value As String)
      _horaCreacion = value
    End Set
  End Property

  Public Property IdDocumento() As Integer
    Get
      Return _idDocumento
    End Get
    Set(ByVal value As Integer)
      _idDocumento = value
    End Set
  End Property

  Public Property IdUsuario() As Integer
    Get
      Return _idUsuario
    End Get
    Set(ByVal value As Integer)
      _idUsuario = value
    End Set
  End Property

  Public ReadOnly Property Existe() As Boolean
    Get
      Return _existe
    End Get
  End Property

#End Region

#Region "Clases"
  Public Class Redimension
    Public Contenido As Byte()
    Public Tamano As Integer
    Public NombreArchivo As String
    Public Extension As String
    Public ContentType As String
  End Class
#End Region

#Region "Constructores"
  Public Sub New()
    MyBase.New()
  End Sub

  Public Sub New(ByVal id As Integer)

    Try

      _id = id
      ObtenerPorId()

    Catch ex As System.Exception

      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)

    End Try

  End Sub



#End Region

#Region "Metodos Publicos"

  Public Sub GuardarNuevo()

    Try

      Dim sp As String = "spu_InmuneLibreria_Archivo_GuardarNuevo"
      Dim parms() As SqlParameter = New SqlParameter(6) {}

      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar, 200)
      parms(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 512)
      parms(3) = New SqlParameter("@FechaCreacion", SqlDbType.VarChar, 8)
      parms(4) = New SqlParameter("@HoraCreacion", SqlDbType.VarChar, 4)
      parms(5) = New SqlParameter("@IdDocumento", SqlDbType.Int)
      parms(6) = New SqlParameter("@IdUsuario", SqlDbType.Int)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = _nombre
      parms(2).Value = _contentType
      parms(3).Value = _fechaCreacion
      parms(4).Value = _horaCreacion
      parms(5).Value = _idDocumento
      parms(6).Value = _idUsuario

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, sp, parms)

      _id = parms(0).Value
      _existe = True
      ObtenerPorId() ' refrescar la instancia

    Catch ex As Exception

      Throw New System.Exception("Error al guardar nuevo " & NombreEntidad & vbCrLf & ex.Message)

    End Try

  End Sub

  Public Sub Actualizar()
    Try
      If Not _existe Then Throw New Exception("Instancia no existe o no es valida")

      Dim sp As String = "spu_InmuneLibreria_Archivo_Actualizar"
      Dim parms() As SqlParameter = New SqlParameter(6) {}

      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar, 200)
      parms(2) = New SqlParameter("@ContentType", SqlDbType.VarChar, 512)
      parms(3) = New SqlParameter("@FechaCreacion", SqlDbType.VarChar, 8)
      parms(4) = New SqlParameter("@HoraCreacion", SqlDbType.VarChar, 4)
      parms(5) = New SqlParameter("@IdDocumento", SqlDbType.Int)
      parms(6) = New SqlParameter("@IdUsuario", SqlDbType.Int)


      parms(0).Value = _id
      parms(1).Value = _nombre
      parms(2).Value = _contentType
      parms(3).Value = _fechaCreacion
      parms(4).Value = _horaCreacion
      parms(5).Value = _idDocumento
      parms(6).Value = _idUsuario


      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, sp, parms)

    Catch ex As Exception

      Throw New System.Exception("Error al actualizar " & NombreEntidad & vbCrLf & ex.Message)

    End Try
  End Sub

  Public Sub Eliminar()
    Try

      If Not _existe Then Throw New Exception("Instancia no existe o no es valida")

      Dim sp As String = "spu_InmuneLibreria_Archivo_Eliminar"
      Dim parms() As SqlParameter = New SqlParameter(0) {}

      parms(0) = New SqlParameter("@id", SqlDbType.Int)
      parms(0).Value = _id

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, sp, parms)

      _existe = False

    Catch ex As Exception

      Throw New System.Exception("Error al eliminar " & NombreEntidad & vbCrLf & ex.Message)

    End Try
  End Sub


#End Region

#Region "Metodos Privados"

  Private Sub ObtenerPorId()

    Try

      _existe = False

      Dim sp As String = "spu_InmuneLibreria_Archivo_ObtenerPorId"
      Dim parms() As SqlParameter = New SqlParameter(0) {}

      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = _id

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, sp, parms)

      If ds.Tables(0).Rows.Count > 0 Then

        Dim row As DataRow = ds.Tables(0).Rows(0)

        _existe = True

        If Not IsDBNull(row("Nombre")) Then _nombre = row("Nombre")
        If Not IsDBNull(row("ContentType")) Then _contentType = row("ContentType")
        If Not IsDBNull(row("FechaCreacion")) Then _fechaCreacion = row("FechaCreacion")
        If Not IsDBNull(row("HoraCreacion")) Then _horaCreacion = row("HoraCreacion")
        If Not IsDBNull(row("IdDocumento")) Then _idDocumento = row("IdDocumento")
        If Not IsDBNull(row("IdUsuario")) Then _idUsuario = row("IdUsuario")

      End If

    Catch ex As Exception

      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)

    End Try

  End Sub

#End Region

#Region "Shared"

  'Metodo     : EsTipoArchivo()
  'Descripcion: determina el tipo de archivo
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 22/04/2008
  Public Shared Function EsTipoArchivo(ByVal contentType As String) As eTipoArchivo
    If (String.IsNullOrEmpty(contentType)) Then
      Return eTipoArchivo.unknow
    ElseIf (contentType.StartsWith("image/")) Then
      Return eTipoArchivo.image
    ElseIf (contentType.StartsWith("audio/")) Then
      Return eTipoArchivo.audio
    ElseIf (contentType.StartsWith("video/")) Then
      Return eTipoArchivo.video
    ElseIf (contentType.Equals("application/vnd.ms-excel") _
         Or contentType.Equals("application/vnd.ms-powerpoint") _
         Or contentType.Equals("application/msword") _
         Or contentType.Equals("application/x-mspublisher") _
         Or contentType.Equals("application/vnd.ms-project") _
         Or contentType.Equals("application/x-msaccess")) Then
      Return eTipoArchivo.office
    ElseIf (contentType.Equals("application/zip") _
         Or contentType.Equals("application/x-zip") _
         Or contentType.Equals("application/x-zip-compressed")) Then
      Return eTipoArchivo.zip
    ElseIf (contentType.StartsWith("application/")) Then
      Return eTipoArchivo.application
    ElseIf (contentType.StartsWith("text/")) Then
      Return eTipoArchivo.text
    Else
      Return eTipoArchivo.others
    End If
  End Function


  Public Shared Function ObtenerIdRutaArchivo(ByVal IdArchivo As String) As DataSet
    Try
      Dim ds As New DataSet
      Dim storedProcedure As String = "spu_InmuneLibreria_Archivo_ObtenerIdRutaArchivo"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@idArchivo", SqlDbType.Int)
      arParms(0).Value = IdArchivo

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      'retorna valor
      Return ds

    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  ''' <summary>
  ''' crea nombre de archivo a partir de uno que ya existe. deja el nombre igual que Windows (ej: prueba.jpg; prueba(1).jpg; prueba(2).jpg; ...)
  ''' </summary>
  ''' <param name="nombreArchivo"></param>
  ''' <param name="indice"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function CrearNombreUnicoDeArchivoRepetido(ByVal nombreArchivo As String, ByVal rutaArchivo As String, ByVal sufijoDiferenciador As String, Optional ByVal indice As Integer = 1) As String
    Dim nuevoNombre As String = ""
    Dim aNombreArchivo As String()
    Dim ultimaPalabraNombreArchivo As String = ""
    Dim nombreSinPunto As String = ""
    Dim extension As String = ""
    Dim i As Integer

    Try
      'separa el nombre del archivo por el punto de la extension
      aNombreArchivo = nombreArchivo.Split(".")
      'crea el nuevo nombre sin puntos y sin la extension
      For i = 0 To aNombreArchivo.Length - 2
        nombreSinPunto &= aNombreArchivo(i)
      Next
      'obtiene la extension del archivo
      extension = aNombreArchivo(aNombreArchivo.Length - 1)

      'crea el nuevo nombre
      nuevoNombre = nombreSinPunto & sufijoDiferenciador & "." & extension

      'verifica si el archivo existe
      If Archivo.ExisteArchivoEnDisco(rutaArchivo & nuevoNombre) Then
        sufijoDiferenciador = "(" & indice & ")"
        'llama recursivamente a la funcion hasta que encuentre un nombre unico
        nuevoNombre = CrearNombreUnicoDeArchivoRepetido(nombreArchivo, rutaArchivo, sufijoDiferenciador, indice + 1)
      End If
    Catch ex As Exception
      nuevoNombre = CrearNombreUnicoArchivo() & "_" & nombreArchivo
    End Try
    Return nuevoNombre
  End Function

  'Metodo     : EsTipoImagen()
  'Descripcion: verifica si el archivo es de tipo imagen o no
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 02/06/2008
  Public Shared Function EsTipoImagen(ByVal contentType As String) As Boolean
    Dim esImagen As Boolean = False
    'si contiene la palabra "image" entonces representa una imagen
    If InStr(contentType, "image", CompareMethod.Binary) > 0 Then
      esImagen = True
    End If
    'retorna valor
    Return esImagen
  End Function

  'Metodo     : ExisteArchivoEnDisco()
  'Descripcion: verifica si existe el archivo
  'Parametros : <>
  'Retorno    : <>
  'Por        : VSR, 27/06/2008
  Public Shared Function ExisteArchivoEnDisco(ByVal rutaArchivo As String) As Boolean
    Return File.Exists(rutaArchivo)
  End Function

  'Metodo     : EliminarArchivoEnDisco()
  'Descripcion: elimina un archivo
  'Parametros : <>
  'Retorno    : <>
  'Por        : VSR, 27/06/2008
  Public Shared Sub EliminarArchivoEnDisco(ByVal rutaArchivo As String)
    'si el archivo existe entonces lo elimina
    Try
      If ExisteArchivoEnDisco(rutaArchivo) Then
        File.Delete(rutaArchivo)
      End If
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  'Metodo     : ExisteDirectorioEnDisco()
  'Descripcion: verifica si existe directorio
  'Parametros : <>
  'Retorno    : <>
  'Por        : VSR, 27/06/2008
  Public Shared Function ExisteDirectorioEnDisco(ByVal rutaDirectorio As String) As Boolean
    Return Directory.Exists(rutaDirectorio)
  End Function

  'Metodo     : CrearDirectorioEnDisco()
  'Descripcion: crea el directorio en la ruta especificada
  'Parametros : <>
  'Retorno    : <>
  'Por        : VSR, 27/06/2008
  Public Shared Sub CrearDirectorioEnDisco(ByVal rutaDirectorio As String)
    'si el directorio no existe entonces lo crea
    If Not ExisteDirectorioEnDisco(rutaDirectorio) Then
      Directory.CreateDirectory(rutaDirectorio)
    End If
  End Sub

  'Metodo     : EliminarDirectorioEnDisco()
  'Descripcion: elimina un directorio
  'Parametros : <>
  'Retorno    : <>
  'Por        : VSR, 27/06/2008
  Public Shared Sub EliminarDirectorioEnDisco(ByVal rutaDirectorio As String, Optional ByVal eliminarFormaRecursiva As Boolean = True)
    'si el directorio existe entonces elimina su contenido
    'si el parametro "eliminarFormaRecursiva" es TRUE entonces elimina si contenido de manera recursiva
    If ExisteDirectorioEnDisco(rutaDirectorio) Then
      Directory.Delete(rutaDirectorio, eliminarFormaRecursiva)
    End If
  End Sub

  ''' <summary>
  ''' Crea un archivo de texto en el disco
  ''' </summary>
  ''' <remarks>Por VSR, 26/06/2008</remarks>
  ''' <param name="contenido">texto que se guardar� en el archivo</param>
  ''' <param name="rutaRaiz">carpeta ra�z en donde se crear� la carpeta y el archivo</param>
  ''' <param name="nombreCarpetaDestino">nombre de la carpeta donde se guardar� el archivo</param>
  ''' <param name="nombreArchivo">nombre del archivo sin la extensi�n</param>
  ''' <param name="extension">define con que extensi�n se guardar� el archivo</param>
  Public Shared Function CrearArchivoTextoEnDisco(ByVal contenido As String, ByVal rutaRaiz As String, Optional ByVal nombreCarpetaDestino As String = "", Optional ByVal nombreArchivo As String = "", Optional ByVal extension As String = "txt")
    Dim fso As Object = CreateObject("Scripting.FileSystemObject")
    Dim rutaCarpetaDestino As String = ""
    Dim rutaArchivoDestino As String = ""

    'si no viene un nombre de archivo o carpeta entonces crea uno por defecto
    If nombreCarpetaDestino = String.Empty Then nombreCarpetaDestino = CrearNombreUnicoArchivo()
    If nombreArchivo = String.Empty Then nombreArchivo = CrearNombreUnicoArchivo()

    'construye ruta destino
    rutaCarpetaDestino = rutaRaiz & "\" & nombreCarpetaDestino
    rutaArchivoDestino = rutaRaiz & "\" & nombreCarpetaDestino & "\" & nombreArchivo & "." & extension

    'crea directorio destino
    If Not ExisteDirectorioEnDisco(rutaCarpetaDestino) Then
      CrearDirectorioEnDisco(rutaCarpetaDestino)
    End If

    'verifica si existe el archivo, si existe lo elimina para crearlo de nuevo
    If ExisteArchivoEnDisco(rutaArchivoDestino) Then
      EliminarArchivoEnDisco(rutaArchivoDestino)
    End If

    Dim textStreamObject As Object = fso.CreateTextFile(rutaArchivoDestino, True, False)
    textStreamObject.WriteLine(contenido)
    textStreamObject.Close()
    textStreamObject = Nothing
    fso = Nothing

    Return rutaArchivoDestino
  End Function

  'Metodo     : CrearNombreUnicoArchivo()
  'Descripcion: crea un nombre unico para un archivo
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 26/06/2008
  Public Shared Function CrearNombreUnicoArchivo(Optional ByVal sufijo As String = "", Optional ByVal usarSeparador As Boolean = True) As String
    If sufijo <> String.Empty Then sufijo = "_" & QuitarCaracteresInvalidos(sufijo)
    Dim ano As String = Herramientas.MyNow().Year.ToString & "-"
    Dim mes As String = Right("0" & Herramientas.MyNow().Month.ToString, 2) & "-"
    Dim dia As String = Right("0" & Herramientas.MyNow().Day.ToString, 2) & "_"
    Dim hora As String = Right("0" & Herramientas.MyNow().Hour.ToString, 2) & "-"
    Dim minutos As String = Right("0" & Herramientas.MyNow().Minute.ToString, 2) & "-"
    Dim segundos As String = Right("0" & Herramientas.MyNow().Second.ToString, 2) & "-"
    Dim milisegundos As String = Herramientas.MyNow().Millisecond.ToString
    Dim fecha As String = ano & mes & dia & hora & minutos & segundos & milisegundos

    If (Not usarSeparador) Then fecha = fecha.Replace("-", "").Replace("_", "")
    Return "WalmartMX_" & fecha & sufijo
  End Function

  'Metodo     : QuitarCaracteresInvalidos()
  'Descripcion: quita caracteres que no son permitidos en el nombre de un archivo
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 26/06/2008
  Public Shared Function QuitarCaracteresInvalidos(ByVal s As String) As String
    Dim nuevoTexto As String = ""
    nuevoTexto = s.Replace("\", "")
    nuevoTexto = nuevoTexto.Replace("/", "")
    nuevoTexto = nuevoTexto.Replace(":", "")
    nuevoTexto = nuevoTexto.Replace("*", "")
    nuevoTexto = nuevoTexto.Replace("?", "")
    nuevoTexto = nuevoTexto.Replace("""", "")
    nuevoTexto = nuevoTexto.Replace("<", "")
    nuevoTexto = nuevoTexto.Replace(">", "")
    nuevoTexto = nuevoTexto.Replace("|", "")
    'retorna valor
    Return nuevoTexto
  End Function

  ''' <summary>
  ''' obtiene el contenido de una plantilla
  ''' </summary>
  ''' <param name="rutaPlantilla"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerStringPlantilla(ByVal rutaPlantilla As String) As String
    'si el archivo no existe entonces devuelve vacio
    If Not ExisteArchivoEnDisco(rutaPlantilla) Then
      Return ""
      Exit Function
    End If

    'leer la plantilla
    Dim sb As New StringBuilder
    Dim reader As New System.IO.StreamReader(rutaPlantilla)
    'Leer el contenido mientras no se llegue al final
    While reader.Peek() <> -1
      Dim s As String = reader.ReadLine()
      If Not String.IsNullOrEmpty(s) Then sb.Append(s.Trim)
    End While
    reader.Close()
    'retorna valor
    Return sb.ToString
  End Function

  ''' <summary>
  ''' obtiene informacion del archivo por id
  ''' </summary>
  ''' <param name="idArchivo"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetallePorId(ByVal idArchivo As String) As DataSet
    Dim ds As New DataSet

    'arreglo de parametros para el procedimiento almacenado
    Dim arParms() As SqlParameter = New SqlParameter(0) {}
    arParms(0) = New SqlParameter("@IdArchivo", SqlDbType.Int)
    arParms(0).Value = idArchivo

    'ejecuta procedimiento almacenado
    ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Archivo_ObtenerDetallePorId", arParms)

    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' convierte datos binarios en una imagen
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function Bytes2Image(ByVal bytes() As Byte) As System.Drawing.Image
    If bytes Is Nothing Then Return Nothing

    Dim ms As New MemoryStream(bytes)
    Dim img As Image = Nothing
    Try
      img = Bitmap.FromStream(ms, True, False)
    Catch ex As Exception
      img = Nothing
    End Try

    Return img
  End Function

  ''' <summary>
  ''' genera una thumb a partir de una imagen original
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GeneraThumbnail(ByVal imageThumbnail As Image, ByVal anchoMax As Integer, ByVal altoMax As Integer) As System.Drawing.Image
    Dim myThumbnail As System.Drawing.Image
    myThumbnail = Nothing
    ResizeImage(imageThumbnail, anchoMax, altoMax)
    myThumbnail = imageThumbnail.GetThumbnailImage(anchoMax, altoMax, Nothing, IntPtr.Zero)
    Return myThumbnail
  End Function

  ''' <summary>
  ''' redimensiona una imagen escalarmente
  ''' </summary>
  ''' <param name="imagen"></param>
  ''' <param name="width"></param>
  ''' <param name="height"></param>
  ''' <remarks>Por VSR, 21/09/2009</remarks>
  Public Shared Sub ResizeImage(ByVal imagen As System.Drawing.Image, ByRef width As Integer, ByRef height As Integer)
    If width <= 0 AndAlso height <= 0 Then
      Throw New ArgumentException("El alto o ancho tiene que ser mayor que cero")
    End If

    'si el ancho o alto es igual a cero entonces se calcula el valor de manera proporcional
    If width <= 0 OrElse height <= 0 Then
      If width <= 0 Then
        width = imagen.Width / (imagen.Height / height)
      ElseIf height <= 0 Then
        height = imagen.Height / (imagen.Width / width)
      End If
    End If
  End Sub

  ''' <summary>
  ''' transforma una imagen en bytes
  ''' </summary>
  ''' <param name="img"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function Image2Bytes(ByRef img As Image) As Byte()
    Dim sTemp As String = Path.GetTempFileName()
    Dim fs As New FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite)
    img.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg)
    fs.Position = 0
    '
    Dim imgLength As Integer = CInt(fs.Length)
    Dim bytes(0 To imgLength - 1) As Byte
    fs.Read(bytes, 0, imgLength)
    fs.Close()
    Return bytes
  End Function

  'Metodo     : TamanoMaximoUploadEnKb()
  'Descripcion: obtiene el tama�o maximo permitido para subir un archivo configurado en web.config
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 22/04/2008
  Public Shared Function TamanoMaximoUploadEnKb() As String
    Dim _tamano As String = System.Configuration.ConfigurationManager.AppSettings("tamanoMaximoArchivoUpload").ToString
    If _tamano = "" Or _tamano Is Nothing Then _tamano = TamanoMaximoArchivoUploadEnKb
    Return _tamano
  End Function

  'Funcion    : ObtenerNombreArchivoDesdeRuta
  'Descripcion: Recibe un path y retorna solo el nombre del archivo
  'Por        : VSR, 30/06/2008
  Public Shared Function ObtenerNombreArchivoDesdeRuta(ByVal ruta As String) As String
    Dim nombreArchivo As String
    Dim posLastSlash As Integer = ruta.LastIndexOf("\")
    If (posLastSlash = "-1") Then posLastSlash = ruta.LastIndexOf("/")


    If posLastSlash >= 0 Then
      'saca el path de directorios
      nombreArchivo = ruta.Substring(posLastSlash + 1, ruta.Length - posLastSlash - 1)
    Else
      nombreArchivo = ruta
    End If
    'retorna valor
    Return nombreArchivo
  End Function

  ''' <summary>
  ''' redimensiona una imagen y la devuelve en un arreglo de bytes
  ''' </summary>
  ''' <param name="Contenido"></param>
  ''' <param name="anchoMax"></param>
  ''' <param name="altoMax"></param>
  ''' <param name="redimensionarSiempre"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function RedimensionarImagen2Bytes(ByVal contenido As Byte(), ByVal anchoMax As Integer, ByVal altoMax As Integer, _
                                                   ByVal redimensionarSiempre As Boolean, ByRef oRedimension As Archivo.Redimension) As Byte()
    Dim foto As System.Drawing.Image = Nothing
    ProcesoRedimensionarImagen(foto, contenido, anchoMax, altoMax, redimensionarSiempre, oRedimension)
    Return contenido
  End Function

  ''' <summary>
  ''' redimensiona la imagen
  ''' </summary>
  ''' <param name="contenido"></param>
  ''' <param name="anchoMax"></param>
  ''' <param name="altoMax"></param>
  ''' <param name="redimensionarSiempre"></param>
  ''' <param name="oRedimension"></param>
  ''' <remarks></remarks>
  Private Shared Sub ProcesoRedimensionarImagen(ByRef foto As System.Drawing.Image, ByRef contenido As Byte(), ByVal anchoMax As Integer, ByVal altoMax As Integer, _
                                                ByVal redimensionarSiempre As Boolean, ByRef oRedimension As Archivo.Redimension)
    Dim anchoImagen As Integer = 0
    Dim altoImagen As Integer = 0
    Dim nGraphic As Drawing.Graphics
    Dim bitmapPhoto As Drawing.Bitmap

    foto = Bytes2Image(contenido)
    altoImagen = foto.Size.Height
    anchoImagen = foto.Size.Width

    If redimensionarSiempre Or anchoImagen > anchoMax Then
      ResizeImage(foto, anchoMax, altoMax)
      bitmapPhoto = New Drawing.Bitmap(anchoMax, altoMax)
      nGraphic = Drawing.Graphics.FromImage(bitmapPhoto)
      nGraphic.DrawImage(foto, 0, 0, bitmapPhoto.Width, bitmapPhoto.Height)
      foto = Nothing
      foto = bitmapPhoto
      contenido = Image2Bytes(foto)

      If Not oRedimension Is Nothing Then
        With oRedimension
          .Contenido = contenido
          .Tamano = contenido.Length
          .NombreArchivo = .NombreArchivo.Replace("." & .Extension, ".jpg")
          .ContentType = "image/pjpeg"
          .Extension = "jpg"
        End With
      End If
    End If
  End Sub

  'Metodo     : ObtenerExtension()
  'Descripcion: obtiene la extension a partir del nombre del archivo
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 22/04/2008
  Public Shared Function ObtenerExtension(ByVal nombreArchivo As String) As String
    Dim extension As String
    If nombreArchivo.Trim = "" Or nombreArchivo Is Nothing Then
      extension = ""
    Else
      'transforma el nombre del archivo en un arreglo
      Dim aNombreArchivo() As String = nombreArchivo.Split(".")
      'obtiene el largo del arreglo
      Dim totalRegistros As Integer = aNombreArchivo.Length()
      'obtiene el ultimo registro del arreglo que contiene la extension del archivo
      extension = aNombreArchivo(totalRegistros - 1)
      If Not EsExtensionValida(extension) Then extension = ""
    End If

    'retorna la extension
    Return extension
  End Function

  ''' <summary>
  ''' verifica si la extension del archivo es valida o no
  ''' </summary>
  ''' <param name="extension"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EsExtensionValida(ByVal extension As String) As Boolean
    Dim esValida As Boolean
    Dim tipoImage As String = "|bmp|cod|gif|ief|jpe|jpeg|jpg|jfif|svg|tif|tiff|ras|cmx|ico|png|pnm|pbm|pgm|ppm|rgb|xbm|xpm|xwd|"
    Dim tipoOffice As String = "|doc|docx|dot|xla|xlc|xlm|xls|xlsx|xlt|xlw|msg|sst|cat|stl|pdf|pot|pps|ppt|pptx|mpp|wcm|wdb|wks|wps|mdb|"
    Dim tipoApplication As String = "|evy|fif|spl|hta|acx|hqx|bin|class|dms|exe|lha|lzh|oda|axs|prf|p10|crl|ai|eps|ps|rtf|setpay|setreg|hlp|bcpio|cdf|z|tgz|cpio|csh|dcr|dir|dxr|dvi|gtar|gz|hdf|ins|isp|iii|js|latex|crd|clp|dll|m13|m14|mvb|wmf|mny|pub|scd|trm|wri|cdf|nc|pma|pmc|pml|pmr|pmw|p12|pfx|p7b|spc|p7r|p7c|p7m|p7s|sh|shar|swf|sit|sv4cpio|sv4crc|tar|tcl|tex|texi|texinfo|roff|t|tr|man|me|ms|ustar|src|cer|crt|der|pko|zip|"
    Dim tipoAudio As String = "|au|snd|mid|rmi|mp3|mp4|aif|aifc|aiff|m3u|ra|ram|wav|"
    Dim tipoMessage As String = "|mht|mhtml|nws|"
    Dim tipoText As String = "|css|323|htm|html|stm|uls|bas|c|h|txt|rtx|sct|tsv|htt|htc|etx|vcf|csv|"
    Dim tipoVideo As String = "|mp2|mpa|mpe|mpeg|mpg|mpv2|mov|qt|lsf|lsx|asf|asr|asx|avi|movie|"
    Dim tipoOtros As String = "|flr|vrml|wrl|wrz|xaf|xof|"
    Dim todosTipos As String

    Try
      todosTipos = tipoImage & tipoOffice & tipoApplication & tipoAudio & tipoMessage & tipoText & tipoVideo & tipoOtros
      If InStr(todosTipos, "|" & extension & "|") > 0 Then
        esValida = True
      Else
        esValida = False
      End If
    Catch ex As Exception
      esValida = True
    End Try
    Return esValida
  End Function

  ''' <summary>
  ''' graba un archivo asociado a un registro en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarArchivoEnBD(ByVal idUsuario As String, _
                                       ByVal nombreArchivo As String, _
                                       ByVal ruta As String, _
                                       ByVal tamano As String, _
                                       ByVal extension As String, _
                                       ByVal contentType As String, _
                                       ByVal contenido() As Byte, _
                                       ByVal orden As String, _
                                       Optional ByVal idArchivo As String = "-1") As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(9) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdArchivo", SqlDbType.Int)
      arParms(1).Value = idArchivo
      arParms(2) = New SqlParameter("@NombreArchivo", SqlDbType.VarChar)
      arParms(2).Value = nombreArchivo
      arParms(3) = New SqlParameter("@Ruta", SqlDbType.VarChar)
      arParms(3).Value = ruta
      arParms(4) = New SqlParameter("@Tamano", SqlDbType.Int)
      arParms(4).Value = tamano
      arParms(5) = New SqlParameter("@Extension", SqlDbType.VarChar)
      arParms(5).Value = extension
      arParms(6) = New SqlParameter("@ContentType", SqlDbType.VarChar)
      arParms(6).Value = contentType
      arParms(7) = New SqlParameter("@Contenido", SqlDbType.Image)
      arParms(7).Value = contenido
      arParms(8) = New SqlParameter("@Orden", SqlDbType.Int)
      arParms(8).Value = orden
      arParms(9) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(9).Value = idUsuario

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Archivo_GrabarArchivoEnBD", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = -200
    End Try

    Return status
  End Function

  ''' <summary>
  ''' Asocia el id del archivo al id del registro
  ''' </summary>
  ''' <remarks></remarks>
  Public Shared Function AsociarIdArchivoConIdRegistro(ByVal entidad As String, ByVal idRegistro As String, ByVal idArchivo As String, Optional ByVal fechaVinculoISO As String = "-1", Optional ByVal horaVinculoISO As String = "-1") As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@Entidad", SqlDbType.VarChar)
      arParms(1).Value = entidad
      arParms(2) = New SqlParameter("@IdRegistro", SqlDbType.Int)
      arParms(2).Value = idRegistro
      arParms(3) = New SqlParameter("@IdArchivo", SqlDbType.Int)
      arParms(3).Value = idArchivo
      arParms(4) = New SqlParameter("@FechaVinculo", SqlDbType.VarChar)
      arParms(4).Value = fechaVinculoISO
      arParms(5) = New SqlParameter("@HoraVinculo", SqlDbType.VarChar)
      arParms(5).Value = horaVinculoISO

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Archivo_AsociarIdArchivoConIdRegistro", arParms)

      status = arParms(0).Value
    Catch ex As Exception
      status = -200
    End Try

    'retorna valor
    Return status
  End Function

#End Region

End Class


Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports log4net
Imports log4net.Config

Public Class Sistema

#Region "enum y constantes"
  Enum eTipoCapitalizacion As Integer
    NombrePropio = 0
    SinFormato = 1
    TodoMayuscula = 2
    TodoMinuscula = 3
    Oracion = 4
  End Enum

  Public Enum eCodigoSql As Integer
    ValorDuplicado = -100
    [Error] = -200
    SinConexion = -201
    SinSesion = -500
    Exito = 200
    SinDatos = 404
  End Enum

  Public Enum eLog4NetNivelVerbosidad As Integer
    Debug = 0
    Info = 1
    Warning = 2
    [Error] = 3
    Fatal = 4
  End Enum

  Public Enum eLog4NetTipoAplicacion
    Consola
    Web
    AplicacionEscritorio
  End Enum

  Public Enum eTablasBasicas As Integer
    T00_CentroDistribucion = 0
    T01_Transportista = 1
    T02_CentroDistribucionAsociados = 2
    T03_ReportabilidadPatentes = 3
  End Enum

    Public Class PoolPlaca
        Property Placa As String
        Property TipoVehiculo As String
        Property Transporte As String
        Property Cedis As String
        Property Determinante As Integer
        Property IdUsuarioCreacion As Integer
        Property FechaCarga As DateTime
        Property NumeroEconomico As String
    End Class

    Public Class PoolPlacaRemolque
        Property Placa As String
        Property Economico As String
        Property TipoPuerta As String
        Property Categoria_Unidad As String
        Property Capacidad_Unidad As String
        Property Serie As String
        Property Marca As String
        Property CedisMadre As String
        Property Cedis As String
        Property Operaci�n As String
        Property TipoContrato As String
        Property TituloComercial As String
        Property GestioPagos As String
        Property Determinante As Integer
        Property Carrier As String
    End Class

#End Region

#Region "Constructores"
    ''' <summary>
    ''' constructor de la clase
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
    MyBase.New()
  End Sub
#End Region

#Region "Metodo Capitalize"
  ''' <summary>
  ''' formatea el texto segun el tipo de capitalizacion
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <param name="tipoCapitalizacion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function Capitalize(ByVal texto As String, Optional ByVal tipoCapitalizacion As eTipoCapitalizacion = eTipoCapitalizacion.SinFormato) As String
    Dim sTexto As String
    Try
      'si es el texto es vacio o no se quiere aplicar formato entonces devuelve el mismo texto
      If (String.IsNullOrEmpty(texto) Or tipoCapitalizacion = eTipoCapitalizacion.SinFormato) Then
        sTexto = texto
      Else
        texto = texto.Trim
        Select Case tipoCapitalizacion
          Case eTipoCapitalizacion.NombrePropio
            sTexto = CapitalizeNombrePropio(texto)
          Case eTipoCapitalizacion.Oracion
            sTexto = CapitalizePrimeraEnMayuscula(texto, True)
          Case eTipoCapitalizacion.TodoMayuscula
            sTexto = CapitalizeTodoMayusculaMinuscula(texto, True)
          Case eTipoCapitalizacion.TodoMinuscula
            sTexto = CapitalizeTodoMayusculaMinuscula(texto, False)
          Case Else
            sTexto = texto
        End Select
      End If
    Catch ex As Exception
      sTexto = texto
    End Try
    'retorna valor
    Return sTexto
  End Function

  ''' <summary>
  ''' formatea el texto como nombre propio, es decir coloca en mayuscula la primera letra despues de un espacio en blanco
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function CapitalizeNombrePropio(ByVal texto As String) As String
    Dim sTexto As String = ""
    Dim sb As StringBuilder
    Dim i, indiceAnterior, largoTexto As Integer

    Try
      sb = New StringBuilder(texto.ToLower)
      largoTexto = sb.Length
      For i = 0 To largoTexto - 1
        indiceAnterior = i - 1
        If (i = 0) Then
          sb(i) = Char.ToUpper(sb(i))
        ElseIf (indiceAnterior >= 0) Then
          If Char.IsWhiteSpace(sb(indiceAnterior)) Then
            sb(i) = Char.ToUpper(sb(i))
          End If
        End If
      Next
      sTexto = ReemplazarPalabrasNoCapitalizables(sb.ToString)
    Catch ex As Exception
      sTexto = texto
    End Try
    'retorna valor
    Return sTexto
  End Function

  ''' <summary>
  ''' formatea el texto para mostrar solamente la primera letra en mayuscula
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function CapitalizePrimeraEnMayuscula(ByVal texto As String, ByVal restoEnMinuscula As Boolean) As String
    Dim sTexto As String
    Try
      If restoEnMinuscula Then
        sTexto = Char.ToUpper(texto(0)) + texto.Substring(1, texto.Length - 1).ToLower
      Else
        sTexto = Char.ToUpper(texto(0)) + texto.Substring(1, texto.Length - 1)
      End If
    Catch ex As Exception
      sTexto = texto
    End Try
    'retorna valor
    Return sTexto
  End Function

  ''' <summary>
  ''' formatea el texto todo en mayuscula o en minuscula segun corresponda
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <param name="esTodoMayuscula"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function CapitalizeTodoMayusculaMinuscula(ByVal texto As String, ByVal esTodoMayuscula As Boolean) As String
    Dim sTexto As String
    Try
      If esTodoMayuscula Then
        sTexto = texto.ToUpper
      Else
        sTexto = texto.ToLower
      End If
    Catch ex As Exception
      sTexto = texto
    End Try
    'retorna valor
    Return sTexto
  End Function

  ''' <summary>
  ''' reemplaza los textos que son con capitalizables en un nombre propio
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function ReemplazarPalabrasNoCapitalizables(ByVal texto As String) As String
    Dim sTexto As String
    Try
      texto = texto.Replace(" De ", " de ")
      texto = texto.Replace(" Del ", " del ")
      texto = texto.Replace(" Las ", " las ")
      texto = texto.Replace(" La ", " la ")
      texto = texto.Replace(" Y ", " y ")
      texto = texto.Replace(" A ", " a ")
      texto = texto.Replace(" E ", " e ")
      texto = texto.Replace(" O ", " o ")
      texto = texto.Replace(" � ", " � ")
      texto = texto.Replace(" U ", " u ")
      sTexto = texto
    Catch ex As Exception
      sTexto = texto
    End Try
    'retorna valor
    Return sTexto
  End Function
#End Region

#Region "Shared"
  ''' <summary>
  ''' obtiene la estructura de la tabla como dataset
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerEstructuraTablaComoDataSet(ByVal nombreTabla As String, Optional ByVal origenDatos As Conexion.eOrigenDatos = Conexion.eOrigenDatos.SistemaBase) As DataSet
    Dim ds As New DataSet
    Dim query As String = "SELECT TOP 1 * FROM " & nombreTabla
    'ejecuta la query
    ds = SqlHelper.ExecuteDataset(Conexion.StringConexion(origenDatos), CommandType.Text, query)
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' determina si tiene asignado el web user control consultado dentro del dataset
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="nombreWebUserControl"></param>
  ''' <param name="tieneFullAcceso"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function TieneAccesoWebUserControl(ByVal ds As DataSet, ByVal nombreWebUserControl As String, Optional ByVal tieneFullAcceso As Boolean = False) As Boolean
    Dim tienePermiso As Boolean = False
    Dim totalRegistros, totalCoincidencias As Integer
    Dim dv As New DataView

    Try
      If tieneFullAcceso Then
        tienePermiso = True
      Else
        If Not ds Is Nothing Then
          totalRegistros = ds.Tables(0).Rows.Count
          If totalRegistros > 0 Then
            dv = ds.Tables(0).DefaultView
            dv.RowFilter = "NombreWUC ='" & nombreWebUserControl & "'"
            totalCoincidencias = dv.Count
            tienePermiso = IIf(totalCoincidencias > 0, True, False)
          End If
        End If
      End If
    Catch ex As Exception
      tienePermiso = False
    End Try
    'retorna valor
    Return tienePermiso
  End Function

  ''' <summary>
  ''' obtiene datos para llenar un combo desde una tabla dada
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerComboDinamico(ByVal tipoCombo As String, ByVal filtro As String, Optional ByVal origenDatos As Conexion.eOrigenDatos = Conexion.eOrigenDatos.SistemaBase) As DataSet
    Dim ds As New DataSet

        'arreglo de parametros para el procedimiento almacenado
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@Filtro", SqlDbType.VarChar)
        arParms(0).Value = filtro

        Dim sp_obtenido = "spu_Combo_" & tipoCombo

        If (sp_obtenido = "spu_Combo_CentroDistribucion_POR_ID_USUARIO_210818") Then

            'Dim unicaInstancia = SupervisorCallcenterCemtraRegistrado.ObtenerInstancia()


            Dim arParmsCentroDistribucion_POR_ID_USUARIO_21081() As SqlParameter = New SqlParameter(1) {}
            arParmsCentroDistribucion_POR_ID_USUARIO_21081(0) = New SqlParameter("@Filtro", SqlDbType.VarChar)
            arParmsCentroDistribucion_POR_ID_USUARIO_21081(0).Value = filtro

            arParmsCentroDistribucion_POR_ID_USUARIO_21081(1) = New SqlParameter("@idUsuarioConsultor", SqlDbType.BigInt)
            arParmsCentroDistribucion_POR_ID_USUARIO_21081(1).Value = idUsuarioTemporal 'unicaInstancia.ObtenerIdUsuario '35244 '--filtro

            ds = SqlHelper.ExecuteDataset(Conexion.StringConexion(origenDatos), CommandType.StoredProcedure, "spu_Combo_" & tipoCombo, arParmsCentroDistribucion_POR_ID_USUARIO_21081)
        Else
            'ejecuta procedimiento almacenado
            ds = SqlHelper.ExecuteDataset(Conexion.StringConexion(origenDatos), CommandType.StoredProcedure, "spu_Combo_" & tipoCombo, arParms)
        End If



        'retorna valor
        Return ds
    End Function
    Public Shared Function ObtenerComboDinamicoCEMTRA(ByVal tipoCombo As String,
                                            ByVal filtro As String,
                                            Optional ByVal origenDatos As Conexion.eOrigenDatos = Conexion.eOrigenDatos.SistemaBase,
                                            Optional ByVal idUsuario As Integer = 0) As DataSet
        Dim ds As New DataSet

        'arreglo de parametros para el procedimiento almacenado
        Dim arParms() As SqlParameter = New SqlParameter(1) {}
        arParms(0) = New SqlParameter("@Filtro", SqlDbType.VarChar)
        arParms(0).Value = filtro
        arParms(1) = New SqlParameter("@usuario", SqlDbType.VarChar)
        arParms(1).Value = idUsuario

        'ejecuta procedimiento almacenado
        ds = SqlHelper.ExecuteDataset(Conexion.StringConexion(origenDatos), CommandType.StoredProcedure, "spu_Combo_" & tipoCombo, arParms)

        'retorna valor
        Return ds
    End Function

    ''' <summary>
    ''' obtiene datos para llenar un combo desde la tabla "TiposGeneral"
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ObtenerComboEstatico(ByVal tipoCombo As String, Optional ByVal origenDatos As Conexion.eOrigenDatos = Conexion.eOrigenDatos.SistemaBase) As DataSet
        Dim ds As New DataSet
        'arreglo de parametros para el procedimiento almacenado
        Dim arParms() As SqlParameter = New SqlParameter(0) {}
        arParms(0) = New SqlParameter("@TipoCombo", SqlDbType.VarChar)
        arParms(0).Value = tipoCombo

        'ejecuta procedimiento almacenado
        ds = SqlHelper.ExecuteDataset(Conexion.StringConexion(origenDatos), CommandType.StoredProcedure, "spu_Combo_Estatico", arParms)

        'retorna valor
        Return ds
    End Function

    ''' <summary>
    ''' valida la conexion del servidor usando ajax
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ValidarConexionServidor() As DataSet
        Dim ds As New DataSet
        'ejecuta procedimiento almacenado
        ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_ValidarConexion")
        'retorna valor
        Return ds
    End Function

    ''' <summary>
    ''' Permite registrar un evento en un log usando log4net 
    ''' </summary>
    ''' <param name="mensaje"></param>
    ''' <param name="sistema"></param>
    ''' <param name="tipoAplicacion"></param>
    ''' <param name="nivelVerbosidad"></param>
    ''' <remarks></remarks>
    Public Shared Sub EscribirLog4Net(ByVal mensaje As String, ByVal sistema As Conexion.eOrigenDatos, ByVal tipoAplicacion As eLog4NetTipoAplicacion, ByVal nivelVerbosidad As eLog4NetNivelVerbosidad)
        Try
            If Not String.IsNullOrEmpty(mensaje) Then
                XmlConfigurator.Configure(New System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\log4netconfig.xml"))
                Dim log As ILog = log4net.LogManager.GetLogger(sistema.ToString() & "-" & tipoAplicacion.ToString())
                mensaje = String.Format("[{0}]", mensaje)
                Select Case nivelVerbosidad
                    Case eLog4NetNivelVerbosidad.Debug : log.Debug(mensaje)
                    Case eLog4NetNivelVerbosidad.Error : log.Error(mensaje)
                    Case eLog4NetNivelVerbosidad.Fatal : log.Fatal(mensaje)
                    Case eLog4NetNivelVerbosidad.Info : log.Info(mensaje)
                    Case eLog4NetNivelVerbosidad.Warning : log.Warn(mensaje)
                End Select
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    ''' <summary>
    ''' graba registro tipo general
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GrabarTipoGeneral(ByVal nombre As String, ByVal tipo As String, ByVal extra1 As String, ByVal extra2 As String) As String
        Dim valor As String

        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(4) {}
            arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 255)
            arParms(0).Direction = ParameterDirection.Output
            arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
            arParms(1).Value = nombre
            arParms(2) = New SqlParameter("@Tipo", SqlDbType.VarChar)
            arParms(2).Value = tipo
            arParms(3) = New SqlParameter("@Extra1", SqlDbType.VarChar)
            arParms(3).Value = extra1
            arParms(4) = New SqlParameter("@Extra2", SqlDbType.VarChar)
            arParms(4).Value = extra2

            'ejecuta procedimiento almacenado
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_GrabarTipoGeneral", arParms)
            valor = arParms(0).Value
        Catch ex As Exception
            valor = "error"
        End Try

        Return valor
    End Function

    Public Shared idUsuarioTemporal = New Int32()

    ''' <summary>
    ''' graba las acciones realizadas por el usuario
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub GrabarLogSesion(ByVal idUsuario As String, ByVal accion As String)
        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            arParms(0).Value = idUsuario

            idUsuarioTemporal = Convert.ToInt32(idUsuario)

            arParms(1) = New SqlParameter("@Accion", SqlDbType.VarChar)
            arParms(1).Value = accion

            'ejecuta procedimiento almacenado
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_GrabarLogSesion", arParms)
        Catch ex As Exception
            'no hace nada
        End Try
    End Sub

    ''' <summary>
    ''' ejecuta consulta generica con fechaInicio, fechaTermino y procedimiento almacenado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConsultaGenerica(ByVal fechaISOInicio As String, ByVal fechaISOTermino As String, ByVal procedimientoAlmacenados As String) As DataSet
        Try
            Dim objCommand As New SqlClient.SqlCommand(procedimientoAlmacenados)
            Dim conn As String = Conexion.StringConexion
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandTimeout = 0
            objCommand.Parameters.Add("@FechaISOInicio", SqlDbType.VarChar, 8)
            objCommand.Parameters("@FechaISOInicio").Value = fechaISOInicio
            objCommand.Parameters.Add("@FechaISOTermino", SqlDbType.VarChar, 8)
            objCommand.Parameters("@FechaISOTermino").Value = fechaISOTermino
            objCommand.Connection = New SqlClient.SqlConnection(conn)
            objCommand.Connection.Open()

            Dim objAdapter As New SqlDataAdapter(objCommand)
            Dim objDataSet As New DataSet
            objAdapter.Fill(objDataSet, "Tabla")
            objCommand.Connection.Close()

            Return objDataSet
        Catch ex As Exception
            Throw New Exception("Error al ejecutar Sistema.ConsultaGenerica" & vbCrLf & ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' obtiene informacion del ultimo log del usuario
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function ObtenerUltimoLogSesion(ByVal idUsuario As String) As String
        Dim valor As String

        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(0) {}
            arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            arParms(0).Value = idUsuario

            'ejecuta procedimiento almacenado
            valor = SqlHelper.ExecuteScalar(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_ObtenerUltimoLogSesion", arParms)
        Catch ex As Exception
            valor = ""
        End Try
        'retorna valor
        Return valor
    End Function

    ''' <summary>
    ''' graba los nro transporte que han sido notificadas por email
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GrabarHistorialReporteGerenteVenta(ByVal nroTransporte As String, ByVal enviadoA As String, ByVal cargo As String, ByVal nombreLocal As String, _
                                                              ByVal codigoLocal As String, ByVal formato As String) As Integer
        Dim valor As Integer

        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(6) {}
            arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
            arParms(0).Direction = ParameterDirection.Output
            arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
            arParms(1).Value = nroTransporte
            arParms(2) = New SqlParameter("@EnviadoA", SqlDbType.VarChar)
            arParms(2).Value = enviadoA
            arParms(3) = New SqlParameter("@Cargo", SqlDbType.VarChar)
            arParms(3).Value = cargo
            arParms(4) = New SqlParameter("@NombreLocal", SqlDbType.VarChar)
            arParms(4).Value = nombreLocal
            arParms(5) = New SqlParameter("@CodigoLocal", SqlDbType.VarChar)
            arParms(5).Value = codigoLocal
            arParms(6) = New SqlParameter("@Formato", SqlDbType.VarChar)
            arParms(6).Value = formato

            'ejecuta procedimiento almacenado
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_GrabarHistorialReporteGerenteVenta", arParms)
            valor = arParms(0).Value
        Catch ex As Exception
            valor = Sistema.eCodigoSql.Error
        End Try

        Return valor
    End Function

    ''' <summary>
    ''' Ejecuta llamada a SP para inserci�n de Alertas
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetAlertas(ByVal procedimientoAlmacenado As String) As DataSet
        Try
            Dim objCommand As New SqlClient.SqlCommand(procedimientoAlmacenado)
            Dim conn As String = Conexion.StringConexion
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandTimeout = 0
            objCommand.Connection = New SqlClient.SqlConnection(conn)
            objCommand.Connection.Open()

            Dim objAdapter As New SqlDataAdapter(objCommand)
            Dim objDataSet As New DataSet
            objAdapter.Fill(objDataSet, "Tabla")
            objCommand.Connection.Close()

            Return objDataSet
        Catch ex As Exception
            Throw New Exception("Error al ejecutar GetAlertas" & vbCrLf & ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' Elimina la alerta de la tabla AlertaWebService_Temp
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function EliminarAlerta(ByVal idAlerta As String) As String
        Try
            Dim valor As String
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@Response", SqlDbType.VarChar, 50)
            arParms(0).Direction = ParameterDirection.Output
            arParms(1) = New SqlParameter("@Id", SqlDbType.Int)
            arParms(1).Value = idAlerta

            'ejecuta procedimiento almacenado
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "Track_DeleteAlertasWebService_Temp", arParms)
            valor = arParms(0).Value
            Return valor
        Catch ex As Exception
            Throw New System.Exception("Error al ejecutar EliminarAlerta" & vbCrLf & Err.Description)
        End Try
    End Function

    ''' <summary>
    ''' obtiene datos de las tablas basicas para comparar si existen los datos a cargar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ObtenerTablasBasicasSistema(ByVal idUsuario As String) As DataSet
        Dim ds As New DataSet
        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(0) {}
            arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            arParms(0).Value = idUsuario

            'ejecuta procedimiento almacenado
            ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_ObtenerTablasBasicasSistema", arParms)
        Catch ex As Exception
            ds = Nothing
        End Try
        'retorna valor
        Return ds
    End Function

    ''' <summary>
    ''' graba los datos del pool de placas
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub GuardarPoolPlaca(ByVal oPoolPlaca As Sistema.PoolPlaca)
        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(7) {}
            arParms(0) = New SqlParameter("@Placa", SqlDbType.VarChar)
            arParms(0).Value = oPoolPlaca.Placa
            arParms(1) = New SqlParameter("@TipoVehiculo", SqlDbType.VarChar)
            arParms(1).Value = oPoolPlaca.TipoVehiculo
            arParms(2) = New SqlParameter("@Transporte", SqlDbType.VarChar)
            arParms(2).Value = oPoolPlaca.Transporte
            arParms(3) = New SqlParameter("@Cedis", SqlDbType.VarChar)
            arParms(3).Value = oPoolPlaca.Cedis
            arParms(4) = New SqlParameter("@Determinante", SqlDbType.Int)
            arParms(4).Value = oPoolPlaca.Determinante
            arParms(5) = New SqlParameter("@IdUsuarioCreacion", SqlDbType.Int)
            arParms(5).Value = oPoolPlaca.IdUsuarioCreacion
            arParms(6) = New SqlParameter("@FechaCarga", SqlDbType.DateTime)
            arParms(6).Value = oPoolPlaca.FechaCarga
            arParms(7) = New SqlParameter("@NumeroEconomico", SqlDbType.VarChar)
            arParms(7).Value = oPoolPlaca.NumeroEconomico

            'ejecuta procedimiento almacenado
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_GuardarPoolPlaca", arParms)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' graba los datos del pool de placas remolque
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub GuardarPoolPlacaRemolque(ByVal oPoolPlaca As Sistema.PoolPlacaRemolque)
        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(14) {}
            arParms(0) = New SqlParameter("@Placa", SqlDbType.VarChar)
            arParms(0).Value = oPoolPlaca.Placa

            If oPoolPlaca.Economico = "" Then
                arParms(1) = New SqlParameter("@Economico", SqlDbType.VarChar)
                arParms(1).Value = "0"
            Else
                arParms(1) = New SqlParameter("@Economico", SqlDbType.VarChar)
                arParms(1).Value = oPoolPlaca.Economico
            End If

            arParms(2) = New SqlParameter("@TipoPuerta", SqlDbType.VarChar)
            arParms(2).Value = oPoolPlaca.TipoPuerta

            arParms(3) = New SqlParameter("@Categoria_Unidad", SqlDbType.VarChar)
            arParms(3).Value = oPoolPlaca.Categoria_Unidad

            arParms(4) = New SqlParameter("@Capacidad_Unidad", SqlDbType.VarChar)
            arParms(4).Value = oPoolPlaca.Capacidad_Unidad

            arParms(5) = New SqlParameter("@Serie", SqlDbType.VarChar)
            arParms(5).Value = oPoolPlaca.Serie

            arParms(6) = New SqlParameter("@Marca", SqlDbType.VarChar)
            arParms(6).Value = oPoolPlaca.Marca

            arParms(7) = New SqlParameter("@CedisMadre", SqlDbType.VarChar)
            arParms(7).Value = oPoolPlaca.CedisMadre

            arParms(8) = New SqlParameter("@Cedis", SqlDbType.VarChar)
            arParms(8).Value = oPoolPlaca.Cedis

            arParms(9) = New SqlParameter("@Operaci�n", SqlDbType.VarChar)
            arParms(9).Value = oPoolPlaca.Operaci�n

            arParms(10) = New SqlParameter("@TipoContrato", SqlDbType.VarChar)
            arParms(10).Value = oPoolPlaca.TipoContrato

            arParms(11) = New SqlParameter("@TituloComercial", SqlDbType.VarChar)
            arParms(11).Value = oPoolPlaca.TituloComercial

            arParms(12) = New SqlParameter("@GestioPagos", SqlDbType.VarChar)
            arParms(12).Value = oPoolPlaca.GestioPagos

            arParms(13) = New SqlParameter("@Determinante", SqlDbType.VarChar)
            arParms(13).Value = oPoolPlaca.Determinante

            arParms(14) = New SqlParameter("@Carrier", SqlDbType.VarChar)
            arParms(14).Value = oPoolPlaca.Carrier

            'ejecuta procedimiento almacenado
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_GuardarPoolPlacaRemolque", arParms)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' graba los datos del pool de placas historia
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub GrabarPoolPlacaHistoria(ByVal listadoDeterminantes As String, ByVal fechaCarga As DateTime)
        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@ListadoDeterminantes", SqlDbType.VarChar)
            arParms(0).Value = listadoDeterminantes
            arParms(1) = New SqlParameter("@FechaCarga", SqlDbType.DateTime)
            arParms(1).Value = fechaCarga

            'ejecuta procedimiento almacenado
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_GrabarPoolPlacaHistoria", arParms)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' graba los datos del pool de placas historia
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub GrabarPoolPlacaHistoriaRemolque(ByVal listadoDeterminantes As String, ByVal fechaCarga As DateTime)
        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@ListadoDeterminantes", SqlDbType.VarChar)
            arParms(0).Value = listadoDeterminantes
            arParms(1) = New SqlParameter("@FechaCarga", SqlDbType.DateTime)
            arParms(1).Value = fechaCarga

            'ejecuta procedimiento almacenado
            SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_GrabarPoolPlacaHistoriaRemolque", arParms)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' obtiene datos de las tablas basicas para comparar si existen los datos a cargar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ObtenerCargaPoolPlaca(ByVal idUsuario As Integer, ByVal fechaCarga As DateTime) As DataSet
        Dim ds As New DataSet
        Try
            'arreglo de parametros para el procedimiento almacenado
            Dim arParms() As SqlParameter = New SqlParameter(1) {}
            arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
            arParms(0).Value = idUsuario
            arParms(1) = New SqlParameter("@FechaCarga", SqlDbType.DateTime)
            arParms(1).Value = fechaCarga

            'ejecuta procedimiento almacenado
            ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Sistema_ObtenerCargaPoolPlaca", arParms)
        Catch ex As Exception
            ds = Nothing
        End Try
        'retorna valor
        Return ds
    End Function


#End Region

End Class


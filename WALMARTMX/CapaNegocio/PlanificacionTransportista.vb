﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

<Serializable()>
Public Class PlanificacionTransportista
#Region "Properties"
  Public Const NombreEntidad As String = "PlanificacionTransportista"

  Public Property Id As Integer
  Public Property IdPlanificacion As Integer
  Public Property IdLocal As Integer
  Public Property IdUsuarioTransportista As Integer
  Public Property NroTransporte As Integer
  Public Property FechaPresentacion As DateTime
  Public Property Carga As String
  Public Property OrdenEntrega As Integer
  Public Property TipoCamion As String
  Public Property PatenteTracto As String
  Public Property PatenteTrailer As String
  Public Property NombreConductor As String
  Public Property RutConductor As String
  Public Property Celular As String
  Public Property CerradoPorTransportista As Boolean
  Public Property Anden As String
  Public Property FechaAsignacionAnden As DateTime
  Public Property IdUsuarioAsignacionAnden As Integer
  Public Property ConfirmacionAnden As Boolean
  Public Property FechaConfirmacionAnden As DateTime
  Public Property IdUsuarioConfirmacionAnden As Integer
  Public Property Sello As Long
  Public Property FechaLecturaSello As DateTime
  Public Property IdUsuarioLecturaSello As Integer
  Public Property Finalizada As Boolean
  Public Property Criterio As String
  Public Property Observacion As String
  Public Property BloqueadoPorFueraHorario As Boolean
  Public Property FechaBloqueoFueraHorario As DateTime
  Public Property Existe As Boolean
#End Region

#Region "Constructor"

  Public Sub New()
    MyBase.New()
  End Sub

  Public Sub New(ByVal Id As Integer)
    Try
      Me.Id = Id
      ObtenerPorId()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Publicos"

  Public Sub GuardarNuevo()
    Dim storedProcedure As String = "spu_PlanificacionTransportista_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(28) {}
    Dim fechaNula As New DateTime(1900, 1, 1)

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      parms(2) = New SqlParameter("@IdLocal", SqlDbType.Int)
      parms(3) = New SqlParameter("@IdUsuarioTransportista", SqlDbType.Int)
      parms(4) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(5) = New SqlParameter("@FechaPresentacion", SqlDbType.DateTime)
      parms(6) = New SqlParameter("@Carga", SqlDbType.VarChar)
      parms(7) = New SqlParameter("@OrdenEntrega", SqlDbType.Int)
      parms(8) = New SqlParameter("@TipoCamion", SqlDbType.VarChar)
      parms(9) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      parms(10) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      parms(11) = New SqlParameter("@NombreConductor", SqlDbType.VarChar)
      parms(12) = New SqlParameter("@RutConductor", SqlDbType.VarChar)
      parms(13) = New SqlParameter("@Celular", SqlDbType.VarChar)
      parms(14) = New SqlParameter("@CerradoPorTransportista", SqlDbType.Bit)
      parms(15) = New SqlParameter("@Anden", SqlDbType.VarChar)
      parms(16) = New SqlParameter("@FechaAsignacionAnden", SqlDbType.DateTime)
      parms(17) = New SqlParameter("@IdUsuarioAsignacionAnden", SqlDbType.Int)
      parms(18) = New SqlParameter("@ConfirmacionAnden", SqlDbType.Bit)
      parms(19) = New SqlParameter("@FechaConfirmacionAnden", SqlDbType.DateTime)
      parms(20) = New SqlParameter("@IdUsuarioConfirmacionAnden", SqlDbType.Int)
      parms(21) = New SqlParameter("@Sello", SqlDbType.BigInt)
      parms(22) = New SqlParameter("@FechaLecturaSello", SqlDbType.DateTime)
      parms(23) = New SqlParameter("@IdUsuarioLecturaSello", SqlDbType.Int)
      parms(24) = New SqlParameter("@Finalizada", SqlDbType.Bit)
      parms(25) = New SqlParameter("@Criterio", SqlDbType.VarChar)
      parms(26) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      parms(27) = New SqlParameter("@BloqueadoPorFueraHorario", SqlDbType.Bit)
      parms(28) = New SqlParameter("@FechaBloqueoFueraHorario", SqlDbType.DateTime)

      'formatea valores
      If (Me.FechaAsignacionAnden.Year = 1) Then Me.FechaAsignacionAnden = fechaNula
      If (Me.FechaConfirmacionAnden.Year = 1) Then Me.FechaConfirmacionAnden = fechaNula
      If (Me.FechaLecturaSello.Year = 1) Then Me.FechaLecturaSello = fechaNula
      If (Me.FechaPresentacion.Year = 1) Then Me.FechaPresentacion = fechaNula
      If (Me.FechaBloqueoFueraHorario.Year = 1) Then Me.FechaBloqueoFueraHorario = fechaNula
      If (Me.Sello = 0) Then Me.Sello = -1
      If (Me.IdUsuarioAsignacionAnden = 0) Then Me.IdUsuarioAsignacionAnden = -1
      If (Me.IdUsuarioConfirmacionAnden = 0) Then Me.IdUsuarioConfirmacionAnden = -1
      If (Me.IdUsuarioLecturaSello = 0) Then Me.IdUsuarioLecturaSello = -1
      If (Me.IdUsuarioTransportista = 0) Then Me.IdUsuarioTransportista = -1

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = Me.IdPlanificacion
      parms(2).Value = Me.IdLocal
      parms(3).Value = Me.IdUsuarioTransportista
      parms(4).Value = Me.NroTransporte
      parms(5).Value = Me.FechaPresentacion
      parms(6).Value = Me.Carga
      parms(7).Value = Me.OrdenEntrega
      parms(8).Value = Me.TipoCamion
      parms(9).Value = Me.PatenteTracto
      parms(10).Value = Me.PatenteTrailer
      parms(11).Value = Me.NombreConductor
      parms(12).Value = Me.RutConductor
      parms(13).Value = Me.Celular
      parms(14).Value = Me.CerradoPorTransportista
      parms(15).Value = Me.Anden
      parms(16).Value = Me.FechaAsignacionAnden
      parms(17).Value = Me.IdUsuarioAsignacionAnden
      parms(18).Value = Me.ConfirmacionAnden
      parms(19).Value = Me.FechaConfirmacionAnden
      parms(20).Value = Me.IdUsuarioConfirmacionAnden
      parms(21).Value = Me.Sello
      parms(22).Value = Me.FechaLecturaSello
      parms(23).Value = Me.IdUsuarioLecturaSello
      parms(24).Value = Me.Finalizada
      parms(25).Value = Me.Criterio
      parms(26).Value = Me.Observacion
      parms(27).Value = Me.BloqueadoPorFueraHorario
      parms(28).Value = Me.FechaBloqueoFueraHorario

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      Me.Id = parms(0).Value
      Me.Existe = True

      ObtenerPorId() ' refrescar la instancia
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  Public Sub Actualizar()
    Dim fechaNula As New DateTime(1900, 1, 1)

    Try
      If Not Me.Existe Then Throw New Exception("Instancia no existe o no es valida")

      Dim storedProcedure As String = "spu_PlanificacionTransportista_Actualizar"
      Dim parms() As SqlParameter = New SqlParameter(28) {}

      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      parms(2) = New SqlParameter("@IdLocal", SqlDbType.Int)
      parms(3) = New SqlParameter("@IdUsuarioTransportista", SqlDbType.Int)
      parms(4) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(5) = New SqlParameter("@FechaPresentacion", SqlDbType.DateTime)
      parms(6) = New SqlParameter("@Carga", SqlDbType.VarChar)
      parms(7) = New SqlParameter("@OrdenEntrega", SqlDbType.Int)
      parms(8) = New SqlParameter("@TipoCamion", SqlDbType.VarChar)
      parms(9) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      parms(10) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      parms(11) = New SqlParameter("@NombreConductor", SqlDbType.VarChar)
      parms(12) = New SqlParameter("@RutConductor", SqlDbType.VarChar)
      parms(13) = New SqlParameter("@Celular", SqlDbType.VarChar)
      parms(14) = New SqlParameter("@CerradoPorTransportista", SqlDbType.Bit)
      parms(15) = New SqlParameter("@Anden", SqlDbType.VarChar)
      parms(16) = New SqlParameter("@FechaAsignacionAnden", SqlDbType.DateTime)
      parms(17) = New SqlParameter("@IdUsuarioAsignacionAnden", SqlDbType.Int)
      parms(18) = New SqlParameter("@ConfirmacionAnden", SqlDbType.Bit)
      parms(19) = New SqlParameter("@FechaConfirmacionAnden", SqlDbType.DateTime)
      parms(20) = New SqlParameter("@IdUsuarioConfirmacionAnden", SqlDbType.Int)
      parms(21) = New SqlParameter("@Sello", SqlDbType.BigInt)
      parms(22) = New SqlParameter("@FechaLecturaSello", SqlDbType.DateTime)
      parms(23) = New SqlParameter("@IdUsuarioLecturaSello", SqlDbType.Int)
      parms(24) = New SqlParameter("@Finalizada", SqlDbType.Bit)
      parms(25) = New SqlParameter("@Criterio", SqlDbType.VarChar)
      parms(26) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      parms(27) = New SqlParameter("@BloqueadoPorFueraHorario", SqlDbType.Bit)
      parms(28) = New SqlParameter("@FechaBloqueoFueraHorario", SqlDbType.DateTime)

      'formatea valores
      If (Me.FechaAsignacionAnden.Year = 1) Then Me.FechaAsignacionAnden = fechaNula
      If (Me.FechaConfirmacionAnden.Year = 1) Then Me.FechaConfirmacionAnden = fechaNula
      If (Me.FechaLecturaSello.Year = 1) Then Me.FechaLecturaSello = fechaNula
      If (Me.FechaPresentacion.Year = 1) Then Me.FechaPresentacion = fechaNula
      If (Me.FechaBloqueoFueraHorario.Year = 1) Then Me.FechaBloqueoFueraHorario = fechaNula
      If (Me.Sello = 0) Then Me.Sello = -1
      If (Me.IdUsuarioAsignacionAnden = 0) Then Me.IdUsuarioAsignacionAnden = -1
      If (Me.IdUsuarioConfirmacionAnden = 0) Then Me.IdUsuarioConfirmacionAnden = -1
      If (Me.IdUsuarioLecturaSello = 0) Then Me.IdUsuarioLecturaSello = -1
      If (Me.IdUsuarioTransportista = 0) Then Me.IdUsuarioTransportista = -1

      parms(0).Value = Me.Id
      parms(1).Value = Me.IdPlanificacion
      parms(2).Value = Me.IdLocal
      parms(3).Value = Me.IdUsuarioTransportista
      parms(4).Value = Me.NroTransporte
      parms(5).Value = Me.FechaPresentacion
      parms(6).Value = Me.Carga
      parms(7).Value = Me.OrdenEntrega
      parms(8).Value = Me.TipoCamion
      parms(9).Value = Me.PatenteTracto
      parms(10).Value = Me.PatenteTrailer
      parms(11).Value = Me.NombreConductor
      parms(12).Value = Me.RutConductor
      parms(13).Value = Me.Celular
      parms(14).Value = Me.CerradoPorTransportista
      parms(15).Value = Me.Anden
      parms(16).Value = Me.FechaAsignacionAnden
      parms(17).Value = Me.IdUsuarioAsignacionAnden
      parms(18).Value = Me.ConfirmacionAnden
      parms(19).Value = Me.FechaConfirmacionAnden
      parms(20).Value = Me.IdUsuarioConfirmacionAnden
      parms(21).Value = Me.Sello
      parms(22).Value = Me.FechaLecturaSello
      parms(23).Value = Me.IdUsuarioLecturaSello
      parms(24).Value = Me.Finalizada
      parms(25).Value = Me.Criterio
      parms(26).Value = Me.Observacion
      parms(27).Value = Me.BloqueadoPorFueraHorario
      parms(28).Value = Me.FechaBloqueoFueraHorario

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)
    Catch ex As Exception
      Throw New System.Exception("Error al actualizar " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

  Public Sub AsignarTrazaViaje()

    Try
      If Not Me.Existe Then Throw New Exception("Instancia no existe o no es valida")

      Dim storedProcedure As String = "spu_PlanificacionTransportista_AsignarTrazaViaje"
      Dim parms() As SqlParameter = New SqlParameter(28) {}

      parms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(1) = New SqlParameter("@IdUsuarioTransportista", SqlDbType.Int)
      parms(2) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      parms(3) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      parms(4) = New SqlParameter("@IdLocal", SqlDbType.Int)
      parms(5) = New SqlParameter("@RutConductor", SqlDbType.VarChar)
      parms(6) = New SqlParameter("@NombreConductor", SqlDbType.VarChar)
      parms(7) = New SqlParameter("@OrdenEntrega", SqlDbType.Int)

      parms(0).Value = Me.NroTransporte
      parms(1).Value = Me.IdUsuarioTransportista
      parms(2).Value = Me.PatenteTrailer
      parms(3).Value = Me.PatenteTracto
      parms(4).Value = Me.IdLocal
      parms(5).Value = Me.RutConductor
      parms(6).Value = Me.NombreConductor
      parms(7).Value = Me.OrdenEntrega

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)
    Catch ex As Exception
      Throw New System.Exception("Error al actualizar " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Privados"

  Private Sub ObtenerPorId()
    Try
      Existe = False

      Dim storedProcedure As String = "spu_PlanificacionTransportista_ObtenerPorId"
      Dim parms() As SqlParameter = New SqlParameter(0) {}
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("IdPlanificacion")) Then Me.IdPlanificacion = row("IdPlanificacion")
        If Not IsDBNull(row("IdLocal")) Then Me.IdLocal = row("IdLocal")
        If Not IsDBNull(row("IdUsuarioTransportista")) Then Me.IdUsuarioTransportista = row("IdUsuarioTransportista")
        If Not IsDBNull(row("NroTransporte")) Then Me.NroTransporte = row("NroTransporte")
        If Not IsDBNull(row("FechaPresentacion")) Then Me.FechaPresentacion = row("FechaPresentacion")
        If Not IsDBNull(row("Carga")) Then Me.Carga = row("Carga")
        If Not IsDBNull(row("OrdenEntrega")) Then Me.OrdenEntrega = row("OrdenEntrega")
        If Not IsDBNull(row("TipoCamion")) Then Me.TipoCamion = row("TipoCamion")
        If Not IsDBNull(row("PatenteTracto")) Then Me.PatenteTracto = row("PatenteTracto")
        If Not IsDBNull(row("PatenteTrailer")) Then Me.PatenteTrailer = row("PatenteTrailer")
        If Not IsDBNull(row("NombreConductor")) Then Me.NombreConductor = row("NombreConductor")
        If Not IsDBNull(row("RutConductor")) Then Me.RutConductor = row("RutConductor")
        If Not IsDBNull(row("Celular")) Then Me.Celular = row("Celular")
        If Not IsDBNull(row("CerradoPorTransportista")) Then Me.CerradoPorTransportista = row("CerradoPorTransportista")
        If Not IsDBNull(row("Anden")) Then Me.Anden = row("Anden")
        If Not IsDBNull(row("FechaAsignacionAnden")) Then Me.FechaAsignacionAnden = row("FechaAsignacionAnden")
        If Not IsDBNull(row("IdUsuarioAsignacionAnden")) Then Me.IdUsuarioAsignacionAnden = row("IdUsuarioAsignacionAnden")
        If Not IsDBNull(row("ConfirmacionAnden")) Then Me.ConfirmacionAnden = row("ConfirmacionAnden")
        If Not IsDBNull(row("FechaConfirmacionAnden")) Then Me.FechaConfirmacionAnden = row("FechaConfirmacionAnden")
        If Not IsDBNull(row("IdUsuarioConfirmacionAnden")) Then Me.IdUsuarioConfirmacionAnden = row("IdUsuarioConfirmacionAnden")
        If Not IsDBNull(row("Sello")) Then Me.Sello = row("Sello")
        If Not IsDBNull(row("FechaLecturaSello")) Then Me.FechaLecturaSello = row("FechaLecturaSello")
        If Not IsDBNull(row("IdUsuarioLecturaSello")) Then Me.IdUsuarioLecturaSello = row("IdUsuarioLecturaSello")
        If Not IsDBNull(row("Finalizada")) Then Me.Finalizada = row("Finalizada")
        If Not IsDBNull(row("Criterio")) Then Me.Criterio = row("Criterio")
        If Not IsDBNull(row("Observacion")) Then Me.Observacion = row("Observacion")
        If Not IsDBNull(row("BloqueadoPorFueraHorario")) Then Me.BloqueadoPorFueraHorario = row("BloqueadoPorFueraHorario")
        If Not IsDBNull(row("FechaBloqueoFueraHorario")) Then Me.FechaBloqueoFueraHorario = row("FechaBloqueoFueraHorario")
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try

  End Sub

#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene planificacion asociada al transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListado(ByVal idUsuario As String, ByVal nroTransporte As String, ByVal fechaPresentacion As String, ByVal patenteTracto As String, _
                                        ByVal patenteTrailer As String, ByVal carga As String, ByVal codLocal As String, ByVal horaPresentacion As String, ByVal minutoPresentacion As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(9) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte
      arParms(2) = New SqlParameter("@FechaPresentacion", SqlDbType.VarChar)
      arParms(2).Value = fechaPresentacion
      arParms(3) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      arParms(3).Value = patenteTracto
      arParms(4) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      arParms(4).Value = patenteTrailer
      arParms(5) = New SqlParameter("@Carga", SqlDbType.VarChar)
      arParms(5).Value = carga
      arParms(6) = New SqlParameter("@CodLocal", SqlDbType.Int)
      arParms(6).Value = codLocal
      arParms(7) = New SqlParameter("@HoraPresentacion", SqlDbType.VarChar)
      arParms(7).Value = horaPresentacion
      arParms(8) = New SqlParameter("@MinutoPresentacion", SqlDbType.VarChar)
      arParms(8).Value = minutoPresentacion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_ObtenerListado", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba el historial de campos modificados en la reasignacion del camion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarHistorialReasignacionCamion(ByVal oPlanificacionTransportista As PlanificacionTransportista, ByVal oUsuario As Usuario, ByVal campoModificado As String, ByVal observacion As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      arParms(1).Value = oPlanificacionTransportista.IdPlanificacion
      arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(2).Value = oPlanificacionTransportista.NroTransporte
      arParms(3) = New SqlParameter("@CampoModificado", SqlDbType.Text)
      arParms(3).Value = campoModificado
      arParms(4) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      arParms(4).Value = observacion
      arParms(5) = New SqlParameter("@IdUsuarioModificacion", SqlDbType.Int)
      arParms(5).Value = oUsuario.Id

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_GrabarHistorialReasignacionCamion", arParms)

      'retorna valor
      status = arParms(0).Value
      Return status
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

  ''' <summary>
  ''' obtiene el historial de reasignacion del camion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerHistorialReasignacionCamion(ByVal idPlanificacion As String, ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      arParms(0).Value = idPlanificacion
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_ObtenerHistorialReasignacionCamion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' elimina planificacion de un transportista
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EliminarDatos(ByVal idPlanificacion As String, ByVal nroTransporte As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      arParms(1).Value = idPlanificacion
      arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(2).Value = nroTransporte

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_EliminarDatos", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar PlanificacionTransportista.EliminarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' finaliza el proceso de asignacion de camiones
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function CerrarAsignacionCamiones(ByVal id As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@Id", SqlDbType.Int)
      arParms(1).Value = id

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_CerrarAsignacionCamiones", arParms)

      'retorna valor
      status = arParms(0).Value
      Return status
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  ''' <summary>
  ''' obtiene la asignacion de camiones realizada por el transportista
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerAsignacionCamionesTransportista(ByVal listIdUsuarioTransportista As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@listIdUsuarioTransportista", SqlDbType.VarChar)
      arParms(0).Value = listIdUsuarioTransportista

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_ObtenerAsignacionCamionesTransportista", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene informacion del patio
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerInformacionPatio(ByVal idPlanificacion As String, ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      arParms(0).Value = idPlanificacion
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_ObtenerInformacionPatio", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle de la planificacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalle(ByVal idPlanificacion As String, ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      arParms(0).Value = idPlanificacion
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de locales para poder autocompletar la busqueda
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AutocompletarLocales(ByVal texto As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@Texto", SqlDbType.VarChar)
      arParms(0).Value = texto

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_AutocompletarLocales", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' elimina las planificaciones que no estan referenciadas
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarPlanificacionesNoReferenciadas(ByVal idPlanificacion As String, ByVal nroTransporte As String, ByVal listado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      arParms(1).Value = idPlanificacion
      arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(2).Value = nroTransporte
      arParms(3) = New SqlParameter("@Listado", SqlDbType.VarChar)
      arParms(3).Value = listado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_EliminarPlanificacionesNoReferenciadas", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para el usuario seleccionado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function VerificarDuplicidad(ByVal idPlanificacion As String, ByVal valor As String, ByVal nroTransporteValorActual As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_PlanificacionTransportista_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdPlanificacion", SqlDbType.Int)
      arParms(1).Value = idPlanificacion
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor
      arParms(3) = New SqlParameter("@NroTransporteValorActual", SqlDbType.Int)
      arParms(3).Value = nroTransporteValorActual

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' asocia el historial al nuevo NroTransporte
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function AsociarHistorialAlNuevoNroTransporte(ByVal nroTransporteActual As String, ByVal nuevoNroTransporte As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@NroTransporteActual", SqlDbType.Int)
      arParms(1).Value = nroTransporteActual
      arParms(2) = New SqlParameter("@NuevoNroTransporte", SqlDbType.Int)
      arParms(2).Value = nuevoNroTransporte

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_AsociarHistorialAlNuevoNroTransporte", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' bloquea la planificacion que esta fuera de horario
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function BloquearPlanificacionTransportistaFueraHorario(ByVal listado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@Listado", SqlDbType.VarChar)
      arParms(1).Value = listado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_BloquearPlanificacionTransportistaFueraHorario", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' obtiene listado de conductores asociados al transportista para autocompletar
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AutocompletarConductoresPorTransportista(ByVal texto As String, ByVal idUsuarioTransportista As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Texto", SqlDbType.VarChar)
      arParms(0).Value = texto
      arParms(1) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(1).Value = idUsuarioTransportista

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_PlanificacionTransportista_AutocompletarConductoresPorTransportista", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function


#End Region

End Class

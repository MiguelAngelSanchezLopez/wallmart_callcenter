﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Encuesta

#Region "Metodos compartidos"

  Public Shared Function ObtenerProveedores(Optional ByVal idProveedor As Integer = -1) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Id", SqlDbType.Int)
      arParms(0).Value = idProveedor

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Encuesta_ObtenerProveedores", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  Public Shared Function ValidarEncuestaRealizada(ByVal idTransportista As Integer) As Integer
    Dim filas As Integer = 0
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Id", SqlDbType.Int)
      arParms(0).Value = idTransportista

      'ejecuta procedimiento almacenado
      filas = SqlHelper.ExecuteScalar(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Encuesta_ObtenerRespuestas", arParms)
    Catch ex As Exception
      Return filas
    End Try
    'retorna valor
    Return filas
  End Function

  Public Shared Function ValidarPatentesSinGps(ByVal idTransportista As Integer) As Integer
    Dim filas As Integer = 0
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Id", SqlDbType.Int)
      arParms(0).Value = idTransportista

      'ejecuta procedimiento almacenado
      filas = SqlHelper.ExecuteScalar(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Encuesta_ObtenerPatentesSinGps", arParms)
    Catch ex As Exception
      Return filas
    End Try
    'retorna valor
    Return filas
  End Function

  ''' <summary>
  ''' graba una encuesta
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GrabarPatentesSinGps(ByVal idTransportista As Integer, ByVal patente As String) As String

    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Encuesta_GrabarPatenteSinGps"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(1).Value = idTransportista
      arParms(2) = New SqlParameter("@Patente", SqlDbType.VarChar)
      arParms(2).Value = patente

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      If valor = "-200" Then
        Throw New System.Exception("Error al ejecutar Transportista.GrabarPatentes" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Transportista.GrabarPatentes" & vbCrLf & Err.Description)
    End Try
  End Function


  ''' <summary>
  ''' graba una encuesta
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GrabarEncuesta(ByVal NumCategoria As String, ByVal NumRespuesta As String, ByVal Si As Boolean, ByVal No As Boolean, ByVal Na As Boolean, _
                                        ByVal idProveedor As Integer, ByVal idTransportista As Integer) As String

    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Encuesta_GrabarEncuesta"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(7) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@NumCategoria", SqlDbType.VarChar)
      arParms(1).Value = NumCategoria
      arParms(2) = New SqlParameter("@NumRespuesta", SqlDbType.VarChar)
      arParms(2).Value = NumRespuesta
      arParms(3) = New SqlParameter("@Si", SqlDbType.Bit)
      arParms(3).Value = Si
      arParms(4) = New SqlParameter("@No", SqlDbType.Bit)
      arParms(4).Value = No
      arParms(5) = New SqlParameter("@Na", SqlDbType.Bit)
      arParms(5).Value = Na
      arParms(6) = New SqlParameter("@IdEncuestaProveedor", SqlDbType.Int)
      arParms(6).Value = idProveedor
      arParms(7) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(7).Value = idTransportista

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      If valor = "-200" Then
        Throw New System.Exception("Error al ejecutar Transportista.GrabarPatentes" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Transportista.GrabarPatentes" & vbCrLf & Err.Description)
    End Try
  End Function
#End Region
End Class

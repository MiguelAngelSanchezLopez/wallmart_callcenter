Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Imports System.Web.HttpUtility
Imports System.Text

Public Class Criptografia

#Region "Constantes"
  Const KEY_SEGURIDAD As String = "ZAQ89XSW524"
  Public Const KEY_CLAVE_WEBSERVICES As String = "ALKTM78D258"
  Public Const KEY_USUARIO_WEBSERVICES As String = "QAnalytics"

#End Region

#Region "Constructor"
  'Metodo     : New()
  'Descripcion: constructor de la clase
  'Parametros : <ninguno>
  'Retorno    : <nada>
  'Por        : VSR, 14/05/2008
  Sub New()
    MyBase.New()
  End Sub
#End Region

#Region "Privado"
  ''' <summary>
  ''' reemplaza marcas especiales por el valor que corresponde
  ''' </summary>
  ''' <param name="texto"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Shared Function ReemplazarMarcasEspeciales(ByVal texto As String) As String
    Dim nuevoTexto As String
    Try
      nuevoTexto = texto.Replace("{$igual}", "=")
    Catch ex As Exception
      nuevoTexto = texto
    End Try
    Return nuevoTexto
  End Function
#End Region

#Region "Shared"
  ''' <summary>
  ''' encriptar un string
  ''' </summary>
  ''' <param name="s"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function obtenerSHA1(ByVal s As String) As String
    Dim laSal As String = "!4lt0_$#"
    ' -copiado del Guille-
    ' Crear una clave SHA1 como la generada por 
    ' FormsAuthentication.HashPasswordForStoringInConfigFile
    ' Adaptada del ejemplo de la ayuda en la descripci�n de SHA1 (Clase)
    Dim enc As New UTF8Encoding
    Dim data() As Byte = enc.GetBytes(s & laSal)
    Dim result() As Byte

    Dim sha As New SHA1CryptoServiceProvider
    ' This is one implementation of the abstract class SHA1.
    result = sha.ComputeHash(data)
    '
    ' Convertir los valores en hexadecimal
    ' cuando tiene una cifra hay que rellenarlo con cero
    ' para que siempre ocupen dos d�gitos.
    Dim sb As New StringBuilder
    For i As Integer = 0 To result.Length - 1
      If result(i) < 16 Then
        sb.Append("0")
      End If
      sb.Append(result(i).ToString("x"))
    Next
    '
    Return sb.ToString.ToUpper
  End Function

  'Metodo     : EncriptarTexto()
  'Descripcion: encriptar un string
  'Parametros : texto - string a encriptar
  'Retorno    : string encriptado
  'Por        : VSR, 14/05/2008
  Public Shared Function EncriptarTripleDES(ByVal texto As String, Optional ByVal usarUrlEncode As Boolean = True) As String
    Dim DES As New TripleDESCryptoServiceProvider()
    Dim hashMD5 As New MD5CryptoServiceProvider()
    ' Compute the MD5 hash.
    DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(KEY_SEGURIDAD))
    ' Set the cipher mode.
    DES.Mode = CipherMode.ECB
    ' Create the encryptor.
    Dim DESEncrypt As ICryptoTransform = DES.CreateEncryptor()
    ' Get a byte array of the string.
    Dim Buffer As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(texto)
    ' Transform and return the string.
    If usarUrlEncode Then
      Return UrlEncode(Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length)))
    Else
      Return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length))
    End If
  End Function


  'Metodo     : DesencriptarTripleDES()
  'Descripcion: desencripta un string
  'Parametros : textoEncriptado - string a desencriptar
  'Retorno    : string desencriptado
  'Por        : VSR, 14/05/2008
  Public Shared Function DesencriptarTripleDES(ByVal textoEncriptado As String, Optional ByVal usarUrlDecode As Boolean = False) As String
    Dim DES As New TripleDESCryptoServiceProvider()
    Dim hashMD5 As New MD5CryptoServiceProvider()

    Try
      If usarUrlDecode Then
        textoEncriptado = UrlDecode(textoEncriptado)
      End If
      ' Compute the MD5 hash.
      DES.Key = hashMD5.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(KEY_SEGURIDAD))
      ' Set the cipher mode.
      DES.Mode = CipherMode.ECB
      ' Create the decryptor.
      Dim DESDecrypt As ICryptoTransform = DES.CreateDecryptor()
      Dim Buffer As Byte() = Convert.FromBase64String(textoEncriptado)
      ' Transform and return the string.
      Return System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length))
    Catch ex As Exception
      Return ""
    End Try
  End Function

  'Metodo     : ObtenerArrayQueryString()
  'Descripcion: obtiene un arreglo con los parametros obtenidos desde el texto encriptado
  'Parametros : textoDesencriptado - string que contiene los parametros a traspasar al arreglo
  'Retorno    : arreglo con los parametros y sus valores
  'Por        : VSR, 14/05/2008
  Public Shared Function ObtenerArrayQueryString(ByVal textoDesencriptado As String) As Array
    Dim i As Integer
    'indica la cantidad de columnas que tendra el arreglo. en este caso son dos columnas
    'la col(0) es donde se guardara el nombre del parametros y la col(1) es donde se guardara el valor del parametro
    Dim TOTAL_COLUMNAS As Integer = 1
    Dim COL_NOMBRE_PARAMETRO As Integer = 0
    Dim COL_VALOR_PARAMETRO As Integer = 1

    'si el texto viene vacio entonces retorna nothing
    'sino obtiene los parametros
    If textoDesencriptado = "" Then
      Return Nothing
    Else
      'obtiene el total de parametros a guardar en el arreglo
      Dim arrParametros As Array = textoDesencriptado.Split("&")
      Dim totalFilas As Integer = arrParametros.Length - 1
      Dim arrListadoParametros(TOTAL_COLUMNAS, totalFilas) As String
      'recorre el arreglo de los parametros para ir guardando sus valores
      For i = 0 To totalFilas
        'obitene el parametro del arreglo. ejm: SolicitudId=1
        Dim strParametro As String = arrParametros(i)
        'separa el parametro en un arreglo para poder almacenarlo. ejm: arrValorParametro(0) = "SolicitudId"; arrValorParametro(1)="1"
        Dim arrValorParametro As Array = strParametro.Split("=")
        'guarda los valores en el arreglo general siempre y cuando el formato del parametro venga correcto
        If arrValorParametro.Length = 2 Then
          arrListadoParametros(COL_NOMBRE_PARAMETRO, i) = arrValorParametro(0)
          arrListadoParametros(COL_VALOR_PARAMETRO, i) = arrValorParametro(1)
        Else
          arrListadoParametros = Nothing
          Exit For
        End If
      Next
      'retorna arreglos con los valores de los parametros
      Return arrListadoParametros
    End If
  End Function

  'Metodo     : RequestQueryString()
  'Descripcion: obtiene el valor de un parametro dentro de un arreglo
  'Parametros
  ' - arrayParametros         - arreglo con todos los valores de los parametros
  ' - nombreParametroBuscar   - nombre del parametro a buscar el valor
  'Retorno    : valor del parametro buscado
  'Por        : VSR, 14/05/2008
  Public Shared Function RequestQueryString(ByVal arrayParametros As Array, ByVal nombreParametroBuscar As String) As String
    Dim totalFilas As Integer, i As Integer
    Dim nombreParametro As String
    Dim valorParametro As String = ""

    'indice de las columnas del arreglo
    Dim COL_NOMBRE_PARAMETRO As Integer = 0
    Dim COL_VALOR_PARAMETRO As Integer = 1

    If arrayParametros Is Nothing Then
      valorParametro = ""
    Else
      If nombreParametroBuscar = "" Then
        valorParametro = ""
      Else
        'obtiene total de filas del arreglo
        totalFilas = UBound(arrayParametros, 2)
        'recorre el arreglo para buscar el valor
        For i = 0 To totalFilas
          nombreParametro = arrayParametros(COL_NOMBRE_PARAMETRO, i)
          'si el nombre del parametro a buscar se encuentra entonces asigna su valor
          If nombreParametro.ToUpper = nombreParametroBuscar.ToUpper Then
            valorParametro = arrayParametros(COL_VALOR_PARAMETRO, i)
            valorParametro = ReemplazarMarcasEspeciales(valorParametro)
            Exit For
          End If
        Next
      End If
    End If
    'retorna valor
    Return valorParametro
  End Function


#End Region

End Class

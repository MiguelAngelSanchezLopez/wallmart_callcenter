﻿Imports System.Web.Services.Protocols
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Autentificacion
  Inherits SoapHeader

#Region "Propiedades"
  Public Usuario As String
  Public Clave As String
#End Region

#Region "Publico"
  ''' <summary>
  ''' valida que el usuario y clave ingresado sea valido
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function EsValidoAutentificacion() As Boolean
    Dim esValido As Boolean = False
    Try
      If String.IsNullOrEmpty(Clave) Or String.IsNullOrEmpty(Usuario) Then
        esValido = False
      Else
        If Clave = Criptografia.KEY_CLAVE_WEBSERVICES And Usuario = Criptografia.KEY_USUARIO_WEBSERVICES Then
          esValido = True
        Else
          esValido = False
        End If
      End If
    Catch ex As Exception
      esValido = False
    End Try
    Return esValido
  End Function

#End Region

End Class

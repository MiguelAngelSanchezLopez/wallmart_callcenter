﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class TrazaViaje

#Region "Properties"
  Public Const NombreEntidad = "TrazaViaje"
  Public Const Directorio = "TrazaViaje\"

  Public Property Id As Integer
  Public Property NroTransporte As Long
  Public Property IdEmbarque As Long
  Public Property Operacion As Integer
  Public Property PatenteTracto As String
  Public Property PatenteTrailer As String
  Public Property RutTransportista As String
  Public Property NombreTransportista As String
  Public Property RutConductor As String
  Public Property NombreConductor As String
  Public Property OrigenCodigo As Integer
  Public Property OrigenDescripcion As String
  Public Property FHSalidaOrigen As DateTime
  Public Property LocalDestino As Integer
  Public Property DestinoDescripcion As String
  Public Property FHLlegadaDestino As DateTime
  Public Property FHAperturaPuerta As DateTime
  Public Property FHSalidaDestino As DateTime
  Public Property TipoViaje As String
  Public Property ProveedorGPS As String
  Public Property DestinoDomicilio As String
  Public Property DestinoRegion As String
  Public Property DestinoDescripcionRegion As String
  Public Property DestinoComuna As String
  Public Property DestinoLat As String
  Public Property DestinoLon As String
  Public Property EstadoViaje As String
  Public Property SecuenciaDestino As Integer
  Public Property RamplaProveedorGPS As String
  Public Property EstadoPuerta As String
  Public Property EstadoLat As String
  Public Property EstadoLon As String
  Public Property TempSalidaCD As String
  Public Property FechaHoraCreacion As DateTime
  Public Property FechaHoraActualizacion As DateTime
  Public Property IngresoSCAT As Boolean
  Public Property Existe As Boolean

#End Region

#Region "Constructor"

  Public Sub New()
    MyBase.New()
  End Sub

  Public Sub New(ByVal Id As Integer)
    Try
      Me.Id = Id
      ObtenerPorId()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

  Public Sub New(ByVal NroTransporte As String, ByVal LocalDestino As String, ByVal EstadoViaje As String)
    Try
      Me.NroTransporte = NroTransporte
      Me.LocalDestino = LocalDestino
      Me.EstadoViaje = EstadoViaje
      ObtenerPorNroTransporteDestinoCodigoEstadoViaje()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Constantes & Enums"
  Public Const RUTA As String = "RUTA"

#End Region

#Region "Metodos Publicos"

  Public Sub GuardarNuevo()
    Dim storedProcedure As String = "spu_TrazaViaje_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(34) {}

    Try
      parms(0) = New SqlParameter("Id", SqlDbType.Int)
      parms(1) = New SqlParameter("NroTransporte", SqlDbType.Int)
      parms(2) = New SqlParameter("Operacion", SqlDbType.Int)
      parms(3) = New SqlParameter("PatenteTracto", SqlDbType.VarChar, 255)
      parms(4) = New SqlParameter("PatenteTrailer", SqlDbType.VarChar, 255)
      parms(5) = New SqlParameter("RutTransportista", SqlDbType.VarChar, 255)
      parms(6) = New SqlParameter("NombreTransportista", SqlDbType.VarChar, 255)
      parms(7) = New SqlParameter("RutConductor", SqlDbType.VarChar, 255)
      parms(8) = New SqlParameter("NombreConductor", SqlDbType.VarChar, 255)
      parms(9) = New SqlParameter("OrigenCodigo", SqlDbType.Int)
      parms(10) = New SqlParameter("OrigenDescripcion", SqlDbType.VarChar, 255)
      parms(11) = New SqlParameter("FHSalidaOrigen", SqlDbType.DateTime)
      parms(12) = New SqlParameter("LocalDestino", SqlDbType.Int)
      parms(13) = New SqlParameter("DestinoDescripcion", SqlDbType.VarChar, 255)
      parms(14) = New SqlParameter("FHLlegadaDestino", SqlDbType.DateTime)
      parms(15) = New SqlParameter("FHAperturaPuerta", SqlDbType.DateTime)
      parms(16) = New SqlParameter("FHSalidaDestino", SqlDbType.DateTime)
      parms(17) = New SqlParameter("TipoViaje", SqlDbType.VarChar, 255)
      parms(18) = New SqlParameter("ProveedorGPS", SqlDbType.VarChar, 255)
      parms(19) = New SqlParameter("DestinoDomicilio", SqlDbType.VarChar, 255)
      parms(20) = New SqlParameter("DestinoRegion", SqlDbType.VarChar, 255)
      parms(21) = New SqlParameter("DestinoDescripcionRegion", SqlDbType.VarChar, 255)
      parms(22) = New SqlParameter("DestinoComuna", SqlDbType.VarChar, 255)
      parms(23) = New SqlParameter("DestinoLat", SqlDbType.VarChar, 255)
      parms(24) = New SqlParameter("DestinoLon", SqlDbType.VarChar, 255)
      parms(25) = New SqlParameter("EstadoViaje", SqlDbType.VarChar, 255)
      parms(26) = New SqlParameter("SecuenciaDestino", SqlDbType.Int)
      parms(27) = New SqlParameter("RamplaProveedorGPS", SqlDbType.VarChar, 255)
      parms(28) = New SqlParameter("EstadoPuerta", SqlDbType.VarChar, 255)
      parms(29) = New SqlParameter("EstadoLat", SqlDbType.VarChar, 255)
      parms(30) = New SqlParameter("EstadoLon", SqlDbType.VarChar, 255)
      parms(31) = New SqlParameter("TempSalidaCD", SqlDbType.VarChar, 255)
      parms(32) = New SqlParameter("FechaHoraCreacion", SqlDbType.DateTime)
      parms(33) = New SqlParameter("FechaHoraActualizacion", SqlDbType.DateTime)
      parms(34) = New SqlParameter("IngresoSCAT", SqlDbType.Bit)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = Me.NroTransporte
      parms(2).Value = Me.Operacion
      parms(3).Value = Me.PatenteTracto
      parms(4).Value = Me.PatenteTrailer
      parms(5).Value = Me.RutTransportista
      parms(6).Value = Me.NombreTransportista
      parms(7).Value = Me.RutConductor
      parms(8).Value = Me.NombreConductor
      parms(9).Value = Me.OrigenCodigo
      parms(10).Value = Me.OrigenDescripcion
      parms(11).Value = Me.FHSalidaOrigen
      parms(12).Value = Me.LocalDestino
      parms(13).Value = Me.DestinoDescripcion
      parms(14).Value = Me.FHLlegadaDestino
      parms(15).Value = Me.FHAperturaPuerta
      parms(16).Value = Me.FHSalidaDestino
      parms(17).Value = Me.TipoViaje
      parms(18).Value = Me.ProveedorGPS
      parms(19).Value = Me.DestinoDomicilio
      parms(20).Value = Me.DestinoRegion
      parms(21).Value = Me.DestinoDescripcionRegion
      parms(22).Value = Me.DestinoComuna
      parms(23).Value = Me.DestinoLat
      parms(24).Value = Me.DestinoLon
      parms(25).Value = Me.EstadoViaje
      parms(26).Value = Me.SecuenciaDestino
      parms(27).Value = Me.RamplaProveedorGPS
      parms(28).Value = Me.EstadoPuerta
      parms(29).Value = Me.EstadoLat
      parms(30).Value = Me.EstadoLon
      parms(31).Value = Me.TempSalidaCD
      parms(32).Value = Me.FechaHoraCreacion
      parms(33).Value = Me.FechaHoraActualizacion
      parms(34).Value = Me.IngresoSCAT

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      Me.Id = parms(0).Value
      Me.Existe = True
      ObtenerPorId() ' refrescar la instancia
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try

  End Sub

  Public Sub Actualizar()
    Try
      If Not Me.Existe Then Throw New Exception("Instancia no existe o no es valida")

      Dim storedProcedure As String = "spu_TrazaViaje_Actualizar"
      Dim parms() As SqlParameter = New SqlParameter(36) {}

      parms(0) = New SqlParameter("Id", SqlDbType.Int)
      parms(1) = New SqlParameter("NroTransporte", SqlDbType.Int)
      parms(2) = New SqlParameter("Operacion", SqlDbType.Int)
      parms(3) = New SqlParameter("PatenteTracto", SqlDbType.VarChar, 255)
      parms(4) = New SqlParameter("PatenteTrailer", SqlDbType.VarChar, 255)
      parms(5) = New SqlParameter("RutTransportista", SqlDbType.VarChar, 255)
      parms(6) = New SqlParameter("NombreTransportista", SqlDbType.VarChar, 255)
      parms(7) = New SqlParameter("RutConductor", SqlDbType.VarChar, 255)
      parms(8) = New SqlParameter("NombreConductor", SqlDbType.VarChar, 255)
      parms(9) = New SqlParameter("OrigenCodigo", SqlDbType.Int)
      parms(10) = New SqlParameter("OrigenDescripcion", SqlDbType.VarChar, 255)
      parms(11) = New SqlParameter("FHSalidaOrigen", SqlDbType.DateTime)
      parms(12) = New SqlParameter("LocalDestino", SqlDbType.Int)
      parms(13) = New SqlParameter("DestinoDescripcion", SqlDbType.VarChar, 255)
      parms(14) = New SqlParameter("FHLlegadaDestino", SqlDbType.DateTime)
      parms(15) = New SqlParameter("FHAperturaPuerta", SqlDbType.DateTime)
      parms(16) = New SqlParameter("FHSalidaDestino", SqlDbType.DateTime)
      parms(17) = New SqlParameter("TipoViaje", SqlDbType.VarChar, 255)
      parms(18) = New SqlParameter("ProveedorGPS", SqlDbType.VarChar, 255)
      parms(19) = New SqlParameter("DestinoDomicilio", SqlDbType.VarChar, 255)
      parms(20) = New SqlParameter("DestinoRegion", SqlDbType.VarChar, 255)
      parms(21) = New SqlParameter("DestinoDescripcionRegion", SqlDbType.VarChar, 255)
      parms(22) = New SqlParameter("DestinoComuna", SqlDbType.VarChar, 255)
      parms(23) = New SqlParameter("DestinoLat", SqlDbType.VarChar, 255)
      parms(24) = New SqlParameter("DestinoLon", SqlDbType.VarChar, 255)
      parms(25) = New SqlParameter("EstadoViaje", SqlDbType.VarChar, 255)
      parms(26) = New SqlParameter("SecuenciaDestino", SqlDbType.Int)
      parms(27) = New SqlParameter("RamplaProveedorGPS", SqlDbType.VarChar, 255)
      parms(28) = New SqlParameter("EstadoPuerta", SqlDbType.VarChar, 255)
      parms(29) = New SqlParameter("EstadoLat", SqlDbType.VarChar, 255)
      parms(30) = New SqlParameter("EstadoLon", SqlDbType.VarChar, 255)
      parms(31) = New SqlParameter("TempSalidaCD", SqlDbType.VarChar, 255)
      parms(32) = New SqlParameter("FechaHoraCreacion", SqlDbType.DateTime)
      parms(33) = New SqlParameter("FechaHoraActualizacion", SqlDbType.DateTime)
      parms(34) = New SqlParameter("IngresoSCAT", SqlDbType.Bit)

      parms(0).Value = Me.Id
      parms(1).Value = Me.NroTransporte
      parms(2).Value = Me.Operacion
      parms(3).Value = Me.PatenteTracto
      parms(4).Value = Me.PatenteTrailer
      parms(5).Value = Me.RutTransportista
      parms(6).Value = Me.NombreTransportista
      parms(7).Value = Me.RutConductor
      parms(8).Value = Me.NombreConductor
      parms(9).Value = Me.OrigenCodigo
      parms(10).Value = Me.OrigenDescripcion
      parms(11).Value = Me.FHSalidaOrigen
      parms(12).Value = Me.LocalDestino
      parms(13).Value = Me.DestinoDescripcion
      parms(14).Value = Me.FHLlegadaDestino
      parms(15).Value = Me.FHAperturaPuerta
      parms(16).Value = Me.FHSalidaDestino
      parms(17).Value = Me.TipoViaje
      parms(18).Value = Me.ProveedorGPS
      parms(19).Value = Me.DestinoDomicilio
      parms(20).Value = Me.DestinoRegion
      parms(21).Value = Me.DestinoDescripcionRegion
      parms(22).Value = Me.DestinoComuna
      parms(23).Value = Me.DestinoLat
      parms(24).Value = Me.DestinoLon
      parms(25).Value = Me.EstadoViaje
      parms(26).Value = Me.SecuenciaDestino
      parms(27).Value = Me.RamplaProveedorGPS
      parms(28).Value = Me.EstadoPuerta
      parms(29).Value = Me.EstadoLat
      parms(30).Value = Me.EstadoLon
      parms(31).Value = Me.TempSalidaCD
      parms(32).Value = Me.FechaHoraCreacion
      parms(33).Value = Me.FechaHoraActualizacion
      parms(34).Value = Me.IngresoSCAT

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)
    Catch ex As Exception
      Throw New System.Exception("Error al actualizar " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Privados"

  Private Sub ObtenerPorId()
    Try
      Me.Existe = False

      Dim storedProcedure As String = "spu_TrazaViaje_ObtenerPorId"
      Dim parms() As SqlParameter = New SqlParameter(0) {}
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("NroTransporte")) Then Me.NroTransporte = row("NroTransporte")
        If Not IsDBNull(row("Operacion")) Then Me.Operacion = row("Operacion")
        If Not IsDBNull(row("PatenteTracto")) Then Me.PatenteTracto = row("PatenteTracto")
        If Not IsDBNull(row("PatenteTrailer")) Then Me.PatenteTrailer = row("PatenteTrailer")
        If Not IsDBNull(row("RutTransportista")) Then Me.RutTransportista = row("RutTransportista")
        If Not IsDBNull(row("NombreTransportista")) Then Me.NombreTransportista = row("NombreTransportista")
        If Not IsDBNull(row("RutConductor")) Then Me.RutConductor = row("RutConductor")
        If Not IsDBNull(row("NombreConductor")) Then Me.NombreConductor = row("NombreConductor")
        If Not IsDBNull(row("OrigenCodigo")) Then Me.OrigenCodigo = row("OrigenCodigo")
        If Not IsDBNull(row("OrigenDescripcion")) Then Me.OrigenDescripcion = row("OrigenDescripcion")
        If Not IsDBNull(row("FHSalidaOrigen")) Then Me.FHSalidaOrigen = row("FHSalidaOrigen")
        If Not IsDBNull(row("LocalDestino")) Then Me.LocalDestino = row("LocalDestino")
        If Not IsDBNull(row("DestinoDescripcion")) Then Me.DestinoDescripcion = row("DestinoDescripcion")
        If Not IsDBNull(row("FHLlegadaDestino")) Then Me.FHLlegadaDestino = row("FHLlegadaDestino")
        If Not IsDBNull(row("FHAperturaPuerta")) Then Me.FHAperturaPuerta = row("FHAperturaPuerta")
        If Not IsDBNull(row("FHSalidaDestino")) Then Me.FHSalidaDestino = row("FHSalidaDestino")
        If Not IsDBNull(row("TipoViaje")) Then Me.TipoViaje = row("TipoViaje")
        If Not IsDBNull(row("ProveedorGPS")) Then Me.ProveedorGPS = row("ProveedorGPS")
        If Not IsDBNull(row("DestinoDomicilio")) Then Me.DestinoDomicilio = row("DestinoDomicilio")
        If Not IsDBNull(row("DestinoRegion")) Then Me.DestinoRegion = row("DestinoRegion")
        If Not IsDBNull(row("DestinoDescripcionRegion")) Then Me.DestinoDescripcionRegion = row("DestinoDescripcionRegion")
        If Not IsDBNull(row("DestinoComuna")) Then Me.DestinoComuna = row("DestinoComuna")
        If Not IsDBNull(row("DestinoLat")) Then Me.DestinoLat = row("DestinoLat")
        If Not IsDBNull(row("DestinoLon")) Then Me.DestinoLon = row("DestinoLon")
        If Not IsDBNull(row("EstadoViaje")) Then Me.EstadoViaje = row("EstadoViaje")
        If Not IsDBNull(row("SecuenciaDestino")) Then Me.SecuenciaDestino = row("SecuenciaDestino")
        If Not IsDBNull(row("RamplaProveedorGPS")) Then Me.RamplaProveedorGPS = row("RamplaProveedorGPS")
        If Not IsDBNull(row("EstadoPuerta")) Then Me.EstadoPuerta = row("EstadoPuerta")
        If Not IsDBNull(row("EstadoLat")) Then Me.EstadoLat = row("EstadoLat")
        If Not IsDBNull(row("EstadoLon")) Then Me.EstadoLon = row("EstadoLon")
        If Not IsDBNull(row("TempSalidaCD")) Then Me.TempSalidaCD = row("TempSalidaCD")
        If Not IsDBNull(row("FechaHoraCreacion")) Then Me.FechaHoraCreacion = row("FechaHoraCreacion")
        If Not IsDBNull(row("FechaHoraActualizacion")) Then Me.FechaHoraActualizacion = row("FechaHoraActualizacion")
        If Not IsDBNull(row("IngresoSCAT")) Then Me.IngresoSCAT = row("IngresoSCAT")

      End If
    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try

  End Sub

  Private Sub ObtenerPorNroTransporteDestinoCodigoEstadoViaje()
    Try
      Me.Existe = False

      Dim storedProcedure As String = "spu_TrazaViaje_ObtenerPorNroTransporteDestinoCodigoEstadoViaje"
      Dim parms() As SqlParameter = New SqlParameter(2) {}

      parms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(0).Value = Me.NroTransporte
      parms(1) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      parms(1).Value = Me.LocalDestino
      parms(2) = New SqlParameter("@EstadoViaje", SqlDbType.VarChar)
      parms(2).Value = Me.EstadoViaje

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("NroTransporte")) Then Me.NroTransporte = row("NroTransporte")
        If Not IsDBNull(row("Operacion")) Then Me.Operacion = row("Operacion")
        If Not IsDBNull(row("PatenteTracto")) Then Me.PatenteTracto = row("PatenteTracto")
        If Not IsDBNull(row("PatenteTrailer")) Then Me.PatenteTrailer = row("PatenteTrailer")
        If Not IsDBNull(row("RutTransportista")) Then Me.RutTransportista = row("RutTransportista")
        If Not IsDBNull(row("NombreTransportista")) Then Me.NombreTransportista = row("NombreTransportista")
        If Not IsDBNull(row("RutConductor")) Then Me.RutConductor = row("RutConductor")
        If Not IsDBNull(row("NombreConductor")) Then Me.NombreConductor = row("NombreConductor")
        If Not IsDBNull(row("OrigenCodigo")) Then Me.OrigenCodigo = row("OrigenCodigo")
        If Not IsDBNull(row("OrigenDescripcion")) Then Me.OrigenDescripcion = row("OrigenDescripcion")
        If Not IsDBNull(row("FHSalidaOrigen")) Then Me.FHSalidaOrigen = row("FHSalidaOrigen")
        If Not IsDBNull(row("LocalDestino")) Then Me.LocalDestino = row("LocalDestino")
        If Not IsDBNull(row("DestinoDescripcion")) Then Me.DestinoDescripcion = row("DestinoDescripcion")
        If Not IsDBNull(row("FHLlegadaDestino")) Then Me.FHLlegadaDestino = row("FHLlegadaDestino")
        If Not IsDBNull(row("FHAperturaPuerta")) Then Me.FHAperturaPuerta = row("FHAperturaPuerta")
        If Not IsDBNull(row("FHSalidaDestino")) Then Me.FHSalidaDestino = row("FHSalidaDestino")
        If Not IsDBNull(row("TipoViaje")) Then Me.TipoViaje = row("TipoViaje")
        If Not IsDBNull(row("ProveedorGPS")) Then Me.ProveedorGPS = row("ProveedorGPS")
        If Not IsDBNull(row("DestinoDomicilio")) Then Me.DestinoDomicilio = row("DestinoDomicilio")
        If Not IsDBNull(row("DestinoRegion")) Then Me.DestinoRegion = row("DestinoRegion")
        If Not IsDBNull(row("DestinoDescripcionRegion")) Then Me.DestinoDescripcionRegion = row("DestinoDescripcionRegion")
        If Not IsDBNull(row("DestinoComuna")) Then Me.DestinoComuna = row("DestinoComuna")
        If Not IsDBNull(row("DestinoLat")) Then Me.DestinoLat = row("DestinoLat")
        If Not IsDBNull(row("DestinoLon")) Then Me.DestinoLon = row("DestinoLon")
        If Not IsDBNull(row("EstadoViaje")) Then Me.EstadoViaje = row("EstadoViaje")
        If Not IsDBNull(row("SecuenciaDestino")) Then Me.SecuenciaDestino = row("SecuenciaDestino")
        If Not IsDBNull(row("RamplaProveedorGPS")) Then Me.RamplaProveedorGPS = row("RamplaProveedorGPS")
        If Not IsDBNull(row("EstadoPuerta")) Then Me.EstadoPuerta = row("EstadoPuerta")
        If Not IsDBNull(row("EstadoLat")) Then Me.EstadoLat = row("EstadoLat")
        If Not IsDBNull(row("EstadoLon")) Then Me.EstadoLon = row("EstadoLon")
        If Not IsDBNull(row("TempSalidaCD")) Then Me.TempSalidaCD = row("TempSalidaCD")
        If Not IsDBNull(row("FechaHoraCreacion")) Then Me.FechaHoraCreacion = row("FechaHoraCreacion")
        If Not IsDBNull(row("FechaHoraActualizacion")) Then Me.FechaHoraActualizacion = row("FechaHoraActualizacion")
        If Not IsDBNull(row("IngresoSCAT")) Then Me.IngresoSCAT = row("IngresoSCAT")

      End If
    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene listado de traza viajes
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function Buscador(ByVal idUsuario As String, ByVal nroTransporte As String, ByVal cdOrigen As String, ByVal localDestino As String, _
                                  ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal estadoViaje As String, ByVal patente As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(7) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte
      arParms(2) = New SqlParameter("@CDOrigen", SqlDbType.Int)
      arParms(2).Value = cdOrigen
      arParms(3) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      arParms(3).Value = localDestino
      arParms(4) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
      arParms(4).Value = fechaDesde
      arParms(5) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
      arParms(5).Value = fechaHasta
      arParms(6) = New SqlParameter("@EstadoViaje", SqlDbType.VarChar)
      arParms(6).Value = estadoViaje
      arParms(7) = New SqlParameter("@Patente", SqlDbType.VarChar)
      arParms(7).Value = patente

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_TrazaViaje_Buscador", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba la recepcion del camion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarHistorialRecepcionCamion(ByVal idUsuario As String, ByVal nroTransporte As String, ByVal patente As String, ByVal estado As String, ByVal observacion As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(2).Value = nroTransporte
      arParms(3) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      arParms(3).Value = patente
      arParms(4) = New SqlParameter("@Estado", SqlDbType.VarChar)
      arParms(4).Value = estado
      arParms(5) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      arParms(5).Value = observacion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_TrazaViaje_GrabarHistorialRecepcionCamion", arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar TrazaViaje.GrabarHistorialRecepcionCamion" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar TrazaViaje.GrabarHistorialRecepcionCamion" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene historial de recepcion del camion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerHistorialRecepcionCamion(ByVal nroTransporte As String, ByVal patente As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(0).Value = nroTransporte
      arParms(1) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      arParms(1).Value = patente

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_TrazaViaje_ObtenerHistorialRecepcionCamion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

End Class

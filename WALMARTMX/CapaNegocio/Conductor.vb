﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Conductor

#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Conductor"

  Public Shared IdUsuario As String

  Public Enum eTablaPermiso
    T00_ListadoCategorias = 0
    T01_ListadoPaginas = 1
    T02_ListadoFunciones = 2
  End Enum
#End Region

#Region "Campos"
  Private _id As Integer
  Private _username As String
  Private _password As String
  Private _rut As String
  Private _dv As String
  Private _nombre As String
  Private _paterno As String
  Private _materno As String
  Private _email As String
  Private _telefono As String
  Private _idDireccion As Integer
  Private _fechaHoraUltimoAcceso As String
  Private _ipUltimoAcceso As String
  Private _fechaHoraCreacion As String
  Private _fechaHoraModificacion As String
  Private _loginDias As String
  Private _estado As Integer
  Private _super As Boolean
  Private _idPerfil As Integer
  Private _perfilNombre As String
  Private _perfilLlave As String
  Private _existe As Boolean

#End Region

#Region "Properties"
  Public Property [Id]() As Integer
    Get
      Return _id
    End Get
    Set(ByVal Value As Integer)
      _id = Value
    End Set
  End Property

  Public Property Username() As String
    Get
      Return _username
    End Get
    Set(ByVal Value As String)
      _username = Value
    End Set
  End Property

  Public Property Password() As String
    Get
      Return _password
    End Get
    Set(ByVal Value As String)
      _password = Value
    End Set
  End Property

  Public Property Rut() As String
    Get
      Return _rut
    End Get
    Set(ByVal Value As String)
      _rut = Value
    End Set
  End Property

  Public Property Dv() As String
    Get
      Return _dv
    End Get
    Set(ByVal Value As String)
      _dv = Value
    End Set
  End Property

  Public Property Nombre() As String
    Get
      Return _nombre
    End Get
    Set(ByVal Value As String)
      _nombre = Value
    End Set
  End Property

  Public Property Paterno() As String
    Get
      Return _paterno
    End Get
    Set(ByVal Value As String)
      _paterno = Value
    End Set
  End Property

  Public Property Materno() As String
    Get
      Return _materno
    End Get
    Set(ByVal Value As String)
      _materno = Value
    End Set
  End Property

  Public Property Email() As String
    Get
      Return _email
    End Get
    Set(ByVal Value As String)
      _email = Value
    End Set
  End Property

  Public Property Telefono() As String
    Get
      Return _telefono
    End Get
    Set(ByVal Value As String)
      _telefono = Value
    End Set
  End Property

  Public Property IdDireccion() As Integer
    Get
      Return _idDireccion
    End Get
    Set(ByVal Value As Integer)
      _idDireccion = Value
    End Set
  End Property

  Public Property FechaHoraUltimoAcceso() As String
    Get
      Return _fechaHoraUltimoAcceso
    End Get
    Set(ByVal Value As String)
      _fechaHoraUltimoAcceso = Value
    End Set
  End Property

  Public Property IpUltimoAcceso() As String
    Get
      Return _ipUltimoAcceso
    End Get
    Set(ByVal Value As String)
      _ipUltimoAcceso = Value
    End Set
  End Property

  Public Property FechHoraCreacion() As String
    Get
      Return _fechaHoraCreacion
    End Get
    Set(ByVal Value As String)
      _fechaHoraCreacion = Value
    End Set
  End Property

  Public Property FechHoraModificacion() As String
    Get
      Return _fechaHoraModificacion
    End Get
    Set(ByVal Value As String)
      _fechaHoraModificacion = Value
    End Set
  End Property

  Public Property LoginDias() As String
    Get
      Return _loginDias
    End Get
    Set(ByVal value As String)
      _loginDias = value
    End Set
  End Property

  Public Property Super() As Boolean
    Get
      Return _super
    End Get
    Set(ByVal Value As Boolean)
      _super = Value
    End Set
  End Property

  Public Property Estado() As Integer
    Get
      Return _estado
    End Get
    Set(ByVal Value As Integer)
      _estado = Value
    End Set
  End Property

  Public Property IdPerfil() As Integer
    Get
      Return _idPerfil
    End Get
    Set(ByVal Value As Integer)
      _idPerfil = Value
    End Set
  End Property

  Public Property PerfilNombre() As String
    Get
      Return _perfilNombre
    End Get
    Set(ByVal Value As String)
      _perfilNombre = Value
    End Set
  End Property

  Public Property PerfilLlave() As String
    Get
      Return _perfilLlave
    End Get
    Set(ByVal Value As String)
      _perfilLlave = Value
    End Set
  End Property

  Public ReadOnly Property NombreCompleto() As String
    Get
      Return Trim(IIf(String.IsNullOrEmpty(_nombre), "", _nombre) & IIf(String.IsNullOrEmpty(_paterno), "", " " & _paterno) & IIf(String.IsNullOrEmpty(_materno), "", " " & _materno))
    End Get
  End Property

  Public ReadOnly Property Existe() As Boolean
    Get
      Return _existe
    End Get
  End Property

#End Region

#Region "Constructores"
  Public Sub New()
    MyBase.New()
  End Sub

  ''' <summary>
  ''' obtiene usuario a partir de su ID
  ''' </summary>
  ''' <param name="Id"></param>
  ''' <remarks></remarks>
  Public Sub New(ByVal Id As Integer)
    Dim storedProcedure As String = "spu_" & NombreEntidad & "_ObtenerPorId"
    Dim descripcionError As String = "Error al crear una instancia de " & NombreEntidad & " (" & Id & ") "
    Dim ds As DataSet = Nothing
    Dim dr As DataRow = Nothing
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        dr = ds.Tables(0).Rows(0)
        If Not IsDBNull(dr.Item("Id")) Then _id = dr.Item("Id")
        If Not IsDBNull(dr.Item("Username")) Then _username = dr.Item("Username")
        If Not IsDBNull(dr.Item("Password")) Then _password = dr.Item("Password")
        If Not IsDBNull(dr.Item("Rut")) Then _rut = dr.Item("Rut")
        If Not IsDBNull(dr.Item("Dv")) Then _dv = dr.Item("Dv")
        If Not IsDBNull(dr.Item("Nombre")) Then _nombre = dr.Item("Nombre")
        If Not IsDBNull(dr.Item("Paterno")) Then _paterno = dr.Item("Paterno")
        If Not IsDBNull(dr.Item("Materno")) Then _materno = dr.Item("Materno")
        If Not IsDBNull(dr.Item("Email")) Then _email = dr.Item("Email")
        If Not IsDBNull(dr.Item("Telefono")) Then _telefono = dr.Item("Telefono")
        If Not IsDBNull(dr.Item("IdDireccion")) Then _idDireccion = dr.Item("IdDireccion")
        If Not IsDBNull(dr.Item("FechaHoraUltimoAcceso")) Then _fechaHoraUltimoAcceso = dr.Item("FechaHoraUltimoAcceso")
        If Not IsDBNull(dr.Item("IpUltimoAcceso")) Then _ipUltimoAcceso = dr.Item("IpUltimoAcceso")
        If Not IsDBNull(dr.Item("FechaHoraCreacion")) Then _fechaHoraCreacion = dr.Item("FechaHoraCreacion")
        If Not IsDBNull(dr.Item("FechaHoraModificacion")) Then _fechaHoraModificacion = dr.Item("FechaHoraModificacion")
        If Not IsDBNull(dr.Item("LoginDias")) Then _loginDias = dr.Item("LoginDias")
        If Not IsDBNull(dr.Item("Super")) Then _super = dr.Item("Super")
        If Not IsDBNull(dr.Item("Estado")) Then _estado = dr.Item("Estado")
        If Not IsDBNull(dr.Item("IdPerfil")) Then _idPerfil = dr.Item("IdPerfil")
        _existe = True
      End If
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      ds = Nothing
    End Try
  End Sub

#End Region

#Region "Metodos Publicos"
  ''' <summary>
  ''' guarda las propiedades de la instancia como un nuevo registro y obtiene su Id
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub GuardarNuevo()
    Dim storedProcedure As String = "spu_" & NombreEntidad & "_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(18) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@Username", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Password", SqlDbType.VarChar)
      parms(3) = New SqlParameter("@Rut", SqlDbType.VarChar)
      parms(4) = New SqlParameter("@Dv", SqlDbType.VarChar)
      parms(5) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      parms(6) = New SqlParameter("@Paterno", SqlDbType.VarChar)
      parms(7) = New SqlParameter("@Materno", SqlDbType.VarChar)
      parms(8) = New SqlParameter("@Email", SqlDbType.VarChar)
      parms(9) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      parms(10) = New SqlParameter("@IdDireccion", SqlDbType.VarChar)
      parms(11) = New SqlParameter("@FechaHoraUltimoAcceso", SqlDbType.VarChar)
      parms(12) = New SqlParameter("@IpUltimoAcceso", SqlDbType.VarChar)
      parms(13) = New SqlParameter("@FechaHoraCreacion", SqlDbType.VarChar)
      parms(14) = New SqlParameter("@FechaHoraModificacion", SqlDbType.VarChar)
      parms(15) = New SqlParameter("@LoginDias", SqlDbType.VarChar)
      parms(16) = New SqlParameter("@Super", SqlDbType.Bit)
      parms(17) = New SqlParameter("@Estado", SqlDbType.Int)
      parms(18) = New SqlParameter("@IdPerfil", SqlDbType.Int)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = _username
      parms(2).Value = _password
      parms(3).Value = Sistema.Capitalize(_rut, Sistema.eTipoCapitalizacion.TodoMayuscula)
      parms(4).Value = Sistema.Capitalize(_dv, Sistema.eTipoCapitalizacion.TodoMayuscula)
      parms(5).Value = Sistema.Capitalize(_nombre, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(6).Value = Sistema.Capitalize(_paterno, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(7).Value = Sistema.Capitalize(_materno, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(8).Value = Sistema.Capitalize(_email, Sistema.eTipoCapitalizacion.TodoMinuscula)
      parms(9).Value = _telefono
      parms(10).Value = _idDireccion
      parms(11).Value = _fechaHoraUltimoAcceso
      parms(12).Value = _ipUltimoAcceso
      parms(13).Value = _fechaHoraCreacion
      parms(14).Value = _fechaHoraModificacion
      parms(15).Value = _loginDias
      parms(16).Value = _super
      parms(17).Value = _estado
      parms(18).Value = _idPerfil

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      _id = parms(0).Value
      _existe = True

      Me.Refrescar()
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  ''' <summary>
  ''' guarda (update) todas las propiedades de la instancia en su registro en la BDD (usando el Id)
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Actualizar()
    ' si la instancia no existe no podemos actualizar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_" & NombreEntidad & "_Actualizar"
    Dim descripcionError As String = "Error al actualizar " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(18) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@Username", SqlDbType.VarChar)
      parms(2) = New SqlParameter("@Password", SqlDbType.VarChar)
      parms(3) = New SqlParameter("@Rut", SqlDbType.VarChar)
      parms(4) = New SqlParameter("@Dv", SqlDbType.VarChar)
      parms(5) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      parms(6) = New SqlParameter("@Paterno", SqlDbType.VarChar)
      parms(7) = New SqlParameter("@Materno", SqlDbType.VarChar)
      parms(8) = New SqlParameter("@Email", SqlDbType.VarChar)
      parms(9) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      parms(10) = New SqlParameter("@IdDireccion", SqlDbType.VarChar)
      parms(11) = New SqlParameter("@FechaHoraUltimoAcceso", SqlDbType.VarChar)
      parms(12) = New SqlParameter("@IpUltimoAcceso", SqlDbType.VarChar)
      parms(13) = New SqlParameter("@FechaHoraCreacion", SqlDbType.VarChar)
      parms(14) = New SqlParameter("@FechaHoraModificacion", SqlDbType.VarChar)
      parms(15) = New SqlParameter("@LoginDias", SqlDbType.VarChar)
      parms(16) = New SqlParameter("@Super", SqlDbType.Bit)
      parms(17) = New SqlParameter("@Estado", SqlDbType.Int)
      parms(18) = New SqlParameter("@IdPerfil", SqlDbType.Int)

      parms(0).Value = _id
      parms(1).Value = _username
      parms(2).Value = _password
      parms(3).Value = Sistema.Capitalize(_rut, Sistema.eTipoCapitalizacion.TodoMayuscula)
      parms(4).Value = Sistema.Capitalize(_dv, Sistema.eTipoCapitalizacion.TodoMayuscula)
      parms(5).Value = Sistema.Capitalize(_nombre, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(6).Value = Sistema.Capitalize(_paterno, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(7).Value = Sistema.Capitalize(_materno, Sistema.eTipoCapitalizacion.NombrePropio)
      parms(8).Value = Sistema.Capitalize(_email, Sistema.eTipoCapitalizacion.TodoMinuscula)
      parms(9).Value = _telefono
      parms(10).Value = _idDireccion
      parms(11).Value = _fechaHoraUltimoAcceso
      parms(12).Value = _ipUltimoAcceso
      parms(13).Value = _fechaHoraCreacion
      parms(14).Value = _fechaHoraModificacion
      parms(15).Value = _loginDias
      parms(16).Value = _super
      parms(17).Value = _estado
      parms(18).Value = _idPerfil

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  ''' <summary>
  ''' elimina fisicamente (delete) el registro que corresponde a la instancia
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Eliminar()
    ' si la instancia contiene un identificador no valido no se puede eliminar
    If _id < 1 Then Exit Sub
    ' si la instancia no existe no podemos eliminar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_" & NombreEntidad & "_Eliminar"
    Dim descripcionError As String = "Error al eliminar " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = _id
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try

  End Sub


#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' busca sus datos en la BDD (usando el Id) y refresca todas las propiedades de la instancia
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub Refrescar()
    ' si la instancia no existe no podemos refrescar (no tenemos seguridad si esta el Id)
    If Not _existe Then Exit Sub

    Dim storedProcedure As String = "spu_" & NombreEntidad & "_ObtenerPorId"
    Dim descripcionError As String = "Error al refrescar una instancia de " & NombreEntidad & " (" & _id & ") "

    Dim ds As DataSet = Nothing
    Dim dr As DataRow = Nothing
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = _id
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        dr = ds.Tables(0).Rows(0)
        'If Not IsDBNull(dr.Item("Id")) Then _id = dr.Item("Id")
        If Not IsDBNull(dr.Item("Username")) Then _username = dr.Item("Username")
        If Not IsDBNull(dr.Item("Password")) Then _password = dr.Item("Password")
        If Not IsDBNull(dr.Item("Rut")) Then _rut = dr.Item("Rut")
        If Not IsDBNull(dr.Item("Nombre")) Then _nombre = dr.Item("Nombre")
        If Not IsDBNull(dr.Item("Paterno")) Then _paterno = dr.Item("Paterno")
        If Not IsDBNull(dr.Item("Materno")) Then _materno = dr.Item("Materno")
        If Not IsDBNull(dr.Item("Email")) Then _email = dr.Item("Email")
        If Not IsDBNull(dr.Item("Telefono")) Then _telefono = dr.Item("Telefono")
        If Not IsDBNull(dr.Item("IdDireccion")) Then _idDireccion = dr.Item("IdDireccion")
        If Not IsDBNull(dr.Item("FechaHoraUltimoAcceso")) Then _fechaHoraUltimoAcceso = dr.Item("FechaHoraUltimoAcceso")
        If Not IsDBNull(dr.Item("IpUltimoAcceso")) Then _ipUltimoAcceso = dr.Item("IpUltimoAcceso")
        If Not IsDBNull(dr.Item("FechaHoraCreacion")) Then _fechaHoraCreacion = dr.Item("FechaHoraCreacion")
        If Not IsDBNull(dr.Item("FechaHoraModificacion")) Then _fechaHoraModificacion = dr.Item("FechaHoraModificacion")
        If Not IsDBNull(dr.Item("LoginDias")) Then _loginDias = dr.Item("LoginDias")
        If Not IsDBNull(dr.Item("Super")) Then _super = dr.Item("Super")
        If Not IsDBNull(dr.Item("Estado")) Then _estado = dr.Item("Estado")
        If Not IsDBNull(dr.Item("IdPerfil")) Then _idPerfil = dr.Item("IdPerfil")
        '_existe = True
      End If
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      ds = Nothing
    End Try
  End Sub

#End Region

#Region "Metodos compartidos"
  ''' <summary>
  ''' obtiene listado de usuarios
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <param name="filtroNombre"></param>
  ''' <param name="filtroRUT"></param>
  ''' <returns></returns>
  ''' <remarks>Por HML, 07/04/2015</remarks>
  Public Shared Function ObtenerListadoConFiltro(ByVal idUsuario As String, ByVal filtroNombre As String, ByVal filtroRUT As String, ByVal idPerfil As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = filtroNombre
      arParms(2) = New SqlParameter("@RUT", SqlDbType.VarChar)
      arParms(2).Value = filtroRUT
      arParms(3) = New SqlParameter("@IdPerfil", SqlDbType.VarChar)
      arParms(3).Value = idPerfil

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Conductor_ObtenerListadoConFiltro", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle del usuario consultado
  ''' </summary>
  ''' <param name="idConductor"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Public Shared Function ObtenerDetalle(ByVal idConductor As String, ByVal idTransportista As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdConductor", SqlDbType.Int)
      arParms(0).Value = idConductor
      arParms(1) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(1).Value = idTransportista

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Conductor_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  'graba un conductor
  Public Shared Function GrabarDatos(ByRef idConductor As String, ByVal idTransportista As String, ByVal rut As String, ByVal dv As String, ByVal nombre As String, _
                                         ByVal paterno As String, ByVal materno As String, ByVal nacionalidad As String, ByVal estadoCivil As String, _
                                         ByVal sexo As String, ByVal email As String, ByVal telefono As String, ByVal telefono2 As String, _
                                         ByVal fechaNacimiento As String, ByVal direccion As String, ByVal fechaIngreso As String, _
                                         ByVal codTelefono As String, ByVal codTelefono2 As String, ByVal nivelEstudio As String, ByVal nacionalidadOtra As String, ByVal idComuna As String
                                         ) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Conductor_GrabarDatos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(21) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdConductor", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(2).Value = idTransportista
      arParms(3) = New SqlParameter("@Rut", SqlDbType.VarChar)
      arParms(3).Value = rut
      arParms(4) = New SqlParameter("@DV", SqlDbType.VarChar)
      arParms(4).Value = dv
      arParms(5) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(5).Value = nombre
      arParms(6) = New SqlParameter("@Paterno", SqlDbType.VarChar)
      arParms(6).Value = paterno
      arParms(7) = New SqlParameter("@Materno", SqlDbType.VarChar)
      arParms(7).Value = materno
      arParms(8) = New SqlParameter("@Nacionalidad", SqlDbType.VarChar)
      arParms(8).Value = nacionalidad
      arParms(9) = New SqlParameter("@estadoCivil", SqlDbType.VarChar)
      arParms(9).Value = estadoCivil
      arParms(10) = New SqlParameter("@sexo", SqlDbType.VarChar)
      arParms(10).Value = sexo
      arParms(11) = New SqlParameter("@email", SqlDbType.VarChar)
      arParms(11).Value = email
      arParms(12) = New SqlParameter("@telefono", SqlDbType.VarChar)
      arParms(12).Value = telefono
      arParms(13) = New SqlParameter("@telefono2", SqlDbType.VarChar)
      arParms(13).Value = telefono2
      arParms(14) = New SqlParameter("@fechaNacimiento", SqlDbType.VarChar)
      arParms(14).Value = fechaNacimiento
      arParms(15) = New SqlParameter("@direccion", SqlDbType.VarChar)
      arParms(15).Value = direccion
      arParms(16) = New SqlParameter("@fechaIngreso", SqlDbType.VarChar)
      arParms(16).Value = fechaIngreso
      arParms(17) = New SqlParameter("@codTelefono", SqlDbType.VarChar)
      arParms(17).Value = codTelefono
      arParms(18) = New SqlParameter("@codTelefono2", SqlDbType.VarChar)
      arParms(18).Value = codTelefono2
      arParms(19) = New SqlParameter("@nivelEstudio", SqlDbType.VarChar)
      arParms(19).Value = nivelEstudio
      arParms(20) = New SqlParameter("@nacionalidadOtra", SqlDbType.VarChar)
      arParms(20).Value = nacionalidadOtra
      arParms(21) = New SqlParameter("@idComuna", SqlDbType.VarChar)
      arParms(21).Value = idComuna


      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      idConductor = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Conductor.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Conductor.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function


  'actualiza un usuario
  Public Shared Function ModificarDatos(ByVal idConductor As String, ByVal idTransportista As String, ByVal rut As String, ByVal dv As String, ByVal nombre As String, _
                                         ByVal paterno As String, ByVal materno As String, ByVal nacionalidad As String, ByVal estadoCivil As String, _
                                         ByVal sexo As String, ByVal email As String, ByVal telefono As String, ByVal telefono2 As String, _
                                         ByVal fechaNacimiento As String, ByVal direccion As String, ByVal fechaIngreso As String, _
                                         ByVal codTelefono As String, ByVal codTelefono2 As String, ByVal nivelEstudio As String, ByVal nacionalidadOtra As String, ByVal idComuna As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Conductor_ModificarDatos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(21) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdConductor", SqlDbType.Int)
      arParms(1).Value = idConductor
      arParms(2) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(2).Value = idTransportista
      arParms(3) = New SqlParameter("@Rut", SqlDbType.VarChar)
      arParms(3).Value = rut
      arParms(4) = New SqlParameter("@DV", SqlDbType.VarChar)
      arParms(4).Value = dv
      arParms(5) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(5).Value = nombre
      arParms(6) = New SqlParameter("@Paterno", SqlDbType.VarChar)
      arParms(6).Value = paterno
      arParms(7) = New SqlParameter("@Materno", SqlDbType.VarChar)
      arParms(7).Value = materno
      arParms(8) = New SqlParameter("@Nacionalidad", SqlDbType.VarChar)
      arParms(8).Value = nacionalidad
      arParms(9) = New SqlParameter("@estadoCivil", SqlDbType.VarChar)
      arParms(9).Value = estadoCivil
      arParms(10) = New SqlParameter("@sexo", SqlDbType.VarChar)
      arParms(10).Value = sexo
      arParms(11) = New SqlParameter("@email", SqlDbType.VarChar)
      arParms(11).Value = email
      arParms(12) = New SqlParameter("@telefono", SqlDbType.VarChar)
      arParms(12).Value = telefono
      arParms(13) = New SqlParameter("@telefono2", SqlDbType.VarChar)
      arParms(13).Value = telefono2
      arParms(14) = New SqlParameter("@fechaNacimiento", SqlDbType.VarChar)
      arParms(14).Value = fechaNacimiento
      arParms(15) = New SqlParameter("@direccion", SqlDbType.VarChar)
      arParms(15).Value = direccion
      arParms(16) = New SqlParameter("@fechaIngreso", SqlDbType.VarChar)
      arParms(16).Value = fechaIngreso
      arParms(17) = New SqlParameter("@codTelefono", SqlDbType.VarChar)
      arParms(17).Value = codTelefono
      arParms(18) = New SqlParameter("@codTelefono2", SqlDbType.VarChar)
      arParms(18).Value = codTelefono2
      arParms(19) = New SqlParameter("@nivelEstudio", SqlDbType.VarChar)
      arParms(19).Value = nivelEstudio
      arParms(20) = New SqlParameter("@nacionalidadOtra", SqlDbType.VarChar)
      arParms(20).Value = nacionalidadOtra
      arParms(21) = New SqlParameter("@idComuna", SqlDbType.VarChar)
      arParms(21).Value = idComuna

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Conductor.ModificarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Conductor.ModificarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' elimina una pagina
  ''' </summary>
  ''' <param name="idConductor"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 16/09/2009</remarks>
  Public Shared Function EliminarDatos(ByVal idConductor As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Conductor_EliminarDatos"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdConductor", SqlDbType.Int)
      arParms(1).Value = idConductor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Conductor.EliminarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  'graba un Documento
  Public Shared Function GrabarDatosDocumento(ByRef idTipoDocumentoXFichaConductor As Integer, ByVal IdTipoDocumento As Integer, ByVal IdTransporte As Integer, ByVal IdConductor As Integer, ByVal intIndefinidos As Boolean, _
                                         ByVal FechaExpiracion As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Conductor_GrabarDatosDocumento"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(6) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdTipoDocumentoXFichaConductor", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
      arParms(2).Value = IdTipoDocumento
      arParms(3) = New SqlParameter("@IdTransporte", SqlDbType.VarChar)
      arParms(3).Value = IdTransporte
      arParms(4) = New SqlParameter("@IdConductor", SqlDbType.VarChar)
      arParms(4).Value = IdConductor
      arParms(5) = New SqlParameter("@intIndefinidos", SqlDbType.Bit)
      arParms(5).Value = intIndefinidos
      arParms(6) = New SqlParameter("@FechaExpiracion", SqlDbType.VarChar)
      arParms(6).Value = FechaExpiracion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      idTipoDocumentoXFichaConductor = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Conductor.GrabarDatos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Conductor.GrabarDatos" & vbCrLf & Err.Description)
    End Try
  End Function

  'modifica un Documento
  Public Shared Function ModificarDatosDocumento(ByVal idTipoDocumentoXFichaConductor As Integer, ByVal IdTipoDocumento As Integer, ByVal IdTransporte As Integer, ByVal IdConductor As Integer, ByVal intIndefinidos As Boolean, _
                                         ByVal FechaExpiracion As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Conductor_ModificarDocumento"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(6) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdTipoDocumentoXFichaConductor", SqlDbType.Int)
      arParms(1).Value = idTipoDocumentoXFichaConductor
      arParms(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
      arParms(2).Value = IdTipoDocumento
      arParms(3) = New SqlParameter("@IdTransporte", SqlDbType.VarChar)
      arParms(3).Value = IdTransporte
      arParms(4) = New SqlParameter("@IdConductor", SqlDbType.VarChar)
      arParms(4).Value = IdConductor
      arParms(5) = New SqlParameter("@intIndefinidos", SqlDbType.Bit)
      arParms(5).Value = intIndefinidos
      arParms(6) = New SqlParameter("@FechaExpiracion", SqlDbType.VarChar)
      arParms(6).Value = FechaExpiracion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Conductor.ModificarDocumentos" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Conductor.ModificarDocumentos" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' elimina un Documento
  ''' </summary>
  ''' <param name="idTipoDocumentoXFichaConductor"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 16/09/2009</remarks>
  Public Shared Function EliminarDocumento(ByVal idTipoDocumentoXFichaConductor As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Conductor_EliminarDocumento"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdTipoDocumentoXFichaConductor", SqlDbType.Int)
      arParms(1).Value = idTipoDocumentoXFichaConductor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Conductor.EliminarDocumento" & vbCrLf & Err.Description)
    End Try
  End Function


  'determina si un valor ya existe en base de datos para el docuemto seleccionado
  Public Shared Function VerificarTipoDocumentoXFichaConductor(ByVal IdConductor As String, ByVal IdTransportista As String, ByVal IdTipoDocumento As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Conductor_Existe_TipoDocumentoXFichaConductor"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 20)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdConductor", SqlDbType.Int)
      arParms(1).Value = IdConductor
      arParms(2) = New SqlParameter("@IdTipoDocumento", SqlDbType.Int)
      arParms(2).Value = IdTipoDocumento
      arParms(3) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(3).Value = IdTransportista

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function


  'determina si un valor ya existe en base de datos para el docuemto seleccionado
  Public Shared Function VerificarIdArchivo(ByVal IdTipoDocumentoXFichaConductor As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Conductor_Existe_IdArchivo"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 20)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdTipoDocumentoXFichaConductor", SqlDbType.Int)
      arParms(1).Value = IdTipoDocumentoXFichaConductor

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para el usuario seleccionado
  ''' </summary>
  ''' <param name="idConductor"></param>
  ''' <param name="valor"></param>
  ''' <param name="campoARevisar"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function VerificarDuplicidad(ByVal idConductor As String, ByVal valor As String, ByVal campoARevisar As String, ByVal idTransportista As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Conductor_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdConductor", SqlDbType.Int)
      arParms(1).Value = idConductor
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor
      arParms(3) = New SqlParameter("@IdTransportista", SqlDbType.VarChar)
      arParms(3).Value = idTransportista

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' obtiene si el conductor esta bloqueado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerConductorBloqueado(ByVal rut As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@rut", SqlDbType.VarChar)
      arParms(0).Value = rut

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Conductor_ObtenerConductorBloqueado", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function
#End Region

End Class

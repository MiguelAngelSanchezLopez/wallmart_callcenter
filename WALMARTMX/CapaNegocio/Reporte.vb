﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Reporte
#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Reporte"
  Public Const KEY_SESION_REPORTE_GENERADO As String = "ReporteGenerado"
  Public Const KEY_SESION_MENSAJE As String = "ReporteMensaje"
  Public Const KEY_SESION_DATASET As String = "ReporteDataset"

#End Region

#Region "Reportes Genericos"
  ''' <summary>
  ''' obtiene detalle del reporte consultado
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal idReporte As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdReporte", SqlDbType.Int)
      arParms(0).Value = idReporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reporte_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function


    'Public Shared Function ObtenerDatosGenericos(ByVal procedimientoAlmacenado As String, ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal idUsuario As String) As DataSet
    '    Dim ds As New DataSet
    '    Try



    '        'arreglo de parametros para el procedimiento almacenado
    '        Dim arParms() As SqlParameter = New SqlParameter(2) {}
    '        arParms(0) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
    '        arParms(0).Value = fechaDesde
    '        arParms(1) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
    '        arParms(1).Value = fechaHasta
    '        arParms(2) = New SqlParameter("@idUsuario", SqlDbType.Int)
    '        arParms(2).Value = idUsuario

    '        'ejecuta procedimiento almacenado
    '        ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, procedimientoAlmacenado, arParms)
    '    Catch ex As Exception
    '        ds = Nothing
    '    End Try
    '    'retorna valor
    '    Return ds
    'End Function
    Public Shared Function ObtenerDatosGenericos(ByVal procedimientoAlmacenado As String, ByVal fechaDesde As String, ByVal fechaHasta As String) As DataSet
        Dim ds As New DataSet
        Try

            'arreglo de parametros para el procedimiento almacenado
            Dim longitudParametros = 1

            Dim unicaInstanciaSupervisorCallcenterCemtraRegistrado = SupervisorCallcenterCemtraRegistrado.ObtenerInstancia()
            Dim usuarioEsSupervisorCemtra = unicaInstanciaSupervisorCallcenterCemtraRegistrado.VerificarSiUsuarioLogueadoEsSupervisorCallcenterCemtra

            'longitudParametros = EstablecerLongitudParametros(procedimientoAlmacenado, usuarioEsSupervisorCemtra)
            longitudParametros = EstablecerLongitudParametros(procedimientoAlmacenado, True) 'forzado para busqueda de resultados de cedis con la relacion del usuario

            Dim arParms() As SqlParameter = New SqlParameter(longitudParametros) {} '1
            arParms(0) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
            arParms(0).Value = fechaDesde
            arParms(1) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
            arParms(1).Value = fechaHasta

            'arParms = EstablecerIdUsuario(arParms, procedimientoAlmacenado, usuarioEsSupervisorCemtra)
            arParms = EstablecerIdUsuario(arParms, procedimientoAlmacenado, True) 'forzado para busqueda de resultados de cedis con la relacion del usuario

            procedimientoAlmacenado = EstablecerProcedimientoAlmacenado(procedimientoAlmacenado, usuarioEsSupervisorCemtra)

            'ejecuta procedimiento almacenado
            ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, procedimientoAlmacenado, arParms)
        Catch ex As Exception
            ds = Nothing
        End Try
        'retorna valor
        Return ds
    End Function

    ''' <summary>
    ''' obtiene detalle del reporte consultado por llave
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function ObtenerDetallePorLLave(ByVal llave As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@llave", SqlDbType.VarChar)
      arParms(0).Value = llave

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reporte_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region


#Region "Reportes Formulario"
  ''' <summary>
  ''' obtiene detalle del reporte consultado por llave
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerProgramacion(ByVal idUsuario As String, ByVal nroTransporte As String, ByVal fechaPresentacion As String, ByVal patenteTracto As String, _
                                        ByVal patenteTrailer As String, ByVal carga As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte
      arParms(2) = New SqlParameter("@FechaPresentacion", SqlDbType.VarChar)
      arParms(2).Value = fechaPresentacion
      arParms(3) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar)
      arParms(3).Value = patenteTracto
      arParms(4) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar)
      arParms(4).Value = patenteTrailer
      arParms(5) = New SqlParameter("@Carga", SqlDbType.VarChar)
      arParms(5).Value = carga

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Reporte_Programacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

    Public Shared Function EstablecerProcedimientoEquivalente(ByVal procedimientoAlmacenado As String) As String
        LlenarProcedimientosOriginales()
        LlenarProcedimientosEquivalentes()

        Dim contador As Int16 = 0

        For Each procedimiento As String In procedimientosOriginales
            If procedimientoAlmacenado = procedimiento Then
                procedimientoAlmacenado = procedimientosEquivalentes(contador)
            End If
            contador += 1
        Next

        Return procedimientoAlmacenado
    End Function

    Public Shared Function EstablecerProcedimientoTiempoConexionPorEjecutivo_SupervisorCemtra(ByVal procedimientoAlmacenado As String, ByVal usuarioEsSupervisorCemtra As Boolean) As String
        If usuarioEsSupervisorCemtra Then
            If procedimientoAlmacenado = "spu_Reporte_TiempoConexionPorEjecutivo" Then
                procedimientoAlmacenado = "spu_Reporte_TiempoConexionPorEjecutivo_SupervisorCemtra"
            End If
        End If
        Return procedimientoAlmacenado
    End Function

    Public Shared Function EstablecerProcedimientoAlmacenado(ByVal procedimientoAlmacenado As String, ByVal usuarioEsSupervisorCemtra As Boolean) As String
        procedimientoAlmacenado = EstablecerProcedimientoEquivalente(procedimientoAlmacenado)
        procedimientoAlmacenado = EstablecerProcedimientoTiempoConexionPorEjecutivo_SupervisorCemtra(procedimientoAlmacenado, usuarioEsSupervisorCemtra)
        Return procedimientoAlmacenado
    End Function

    Private Shared procedimientosOriginales As New List(Of String)
    Private Shared procedimientosEquivalentes As New List(Of String)

    Public Shared Sub LlenarProcedimientosOriginales()
        procedimientosOriginales.Clear()

        procedimientosOriginales.Add("spu_Reporte_AlertasEntrantes")
        procedimientosOriginales.Add("spu_Reporte_AlertasPorEjecutivo")
        procedimientosOriginales.Add("spu_Reporte_AlertasAtendidas")
        procedimientosOriginales.Add("spu_Reporte_AlertasContactadas")
        procedimientosOriginales.Add("spu_Reporte_HistorialAccionesPorAlerta")
        procedimientosOriginales.Add("spu_Reporte_ProductividadPorEjecutivo")
        procedimientosOriginales.Add("spu_Reporte_TotalesAlertas")
        procedimientosOriginales.Add("spu_Reporte_TotalesTipoAlerta")
        procedimientosOriginales.Add("spu_Reporte_TiempoConexionPorEjecutivo")
    End Sub

    Public Shared Sub LlenarProcedimientosEquivalentes()
        procedimientosEquivalentes.Clear()

        'procedimientosEquivalentes.Add("spu_Reporte_AlertasEntrantes_110718")'
        'procedimientosEquivalentes.Add("spu_Reporte_AlertasPorEjecutivo_110718")'
        'procedimientosEquivalentes.Add("spu_Reporte_AlertasAtendidas_110718")'
        'procedimientosEquivalentes.Add("spu_Reporte_AlertasContactadas_110718")'
        'procedimientosEquivalentes.Add("spu_Reporte_HistorialAccionesPorAlerta_110718")'
        'procedimientosEquivalentes.Add("spu_Reporte_ProductividadPorEjecutivo_110718")'
        'procedimientosEquivalentes.Add("spu_Reporte_TotalesAlertas_110718")'
        'procedimientosEquivalentes.Add("spu_Reporte_TotalesTipoAlerta_110718")'
        'procedimientosEquivalentes.Add("spu_Reporte_TiempoConexionPorEjecutivo")

        procedimientosEquivalentes.Add("spu_Reporte_AlertasEntrantes_CON_FILTRO_CEDIS_POR_USUARIO_140818") '
        procedimientosEquivalentes.Add("spu_Reporte_AlertasPorEjecutivo_CON_FILTRO_CEDIS_POR_USUARIO_140818") '
        procedimientosEquivalentes.Add("spu_Reporte_AlertasAtendidas_CON_FILTRO_CEDIS_POR_USUARIO_140818") '
        procedimientosEquivalentes.Add("spu_Reporte_AlertasContactadas_CON_FILTRO_CEDIS_POR_USUARIO_140818") '
        procedimientosEquivalentes.Add("spu_Reporte_HistorialAccionesPorAlerta_CON_FILTRO_CEDIS_POR_USUARIO_140818") '
        procedimientosEquivalentes.Add("spu_Reporte_ProductividadPorEjecutivo_110718") '("spu_Reporte_ProductividadPorEjecutivo_CON_FILTRO_CEDIS_POR_USUARIO_140818")
        procedimientosEquivalentes.Add("spu_Reporte_TotalesAlertas_CON_FILTRO_CEDIS_POR_USUARIO_140818") '
        procedimientosEquivalentes.Add("spu_Reporte_TotalesTipoAlerta_CON_FILTRO_CEDIS_POR_USUARIO_140818") '
        procedimientosEquivalentes.Add("spu_Reporte_TiempoConexionPorEjecutivo") '("spu_Reporte_TiempoConexionPorEjecutivo_CON_FILTRO_CEDIS_POR_USUARIO_140818")


    End Sub

    Public Shared Function EstablecerLongitudParametros(ByVal procedimientoAlmacenado As String, ByVal usuarioEsSupervisorCemtra As Boolean) As Int32
        Dim longitudParametros = 1
        If usuarioEsSupervisorCemtra Then
            If procedimientoAlmacenado = "spu_Reporte_AlertasEntrantes" Or procedimientoAlmacenado = "spu_Reporte_AlertasPorEjecutivo" Or procedimientoAlmacenado = "spu_Reporte_AlertasAtendidas" Or procedimientoAlmacenado = "spu_Reporte_AlertasContactadas" Or procedimientoAlmacenado = "spu_Reporte_HistorialAccionesPorAlerta" Or procedimientoAlmacenado = "spu_Reporte_ProductividadPorEjecutivo" Or procedimientoAlmacenado = "spu_Reporte_TotalesAlertas" Or procedimientoAlmacenado = "spu_Reporte_TotalesTipoAlerta" Or procedimientoAlmacenado = "spu_Reporte_TiempoConexionPorEjecutivo" Then
                longitudParametros = 2
            End If
        End If
        Return longitudParametros
    End Function

    Public Shared Function EstablecerIdUsuario(ByVal arParms() As SqlParameter, ByVal procedimientoAlmacenado As String, ByVal usuarioEsSupervisorCemtra As Boolean) As SqlParameter()
        Dim unicaInstanciaSupervisorCallcenterCemtraRegistrado = SupervisorCallcenterCemtraRegistrado.ObtenerInstancia()

        If usuarioEsSupervisorCemtra Then
            If procedimientoAlmacenado = "spu_Reporte_AlertasEntrantes" Or procedimientoAlmacenado = "spu_Reporte_AlertasPorEjecutivo" Or procedimientoAlmacenado = "spu_Reporte_AlertasAtendidas" Or procedimientoAlmacenado = "spu_Reporte_AlertasContactadas" Or procedimientoAlmacenado = "spu_Reporte_HistorialAccionesPorAlerta" Or procedimientoAlmacenado = "spu_Reporte_ProductividadPorEjecutivo" Or procedimientoAlmacenado = "spu_Reporte_TotalesAlertas" Or procedimientoAlmacenado = "spu_Reporte_TotalesTipoAlerta" Or procedimientoAlmacenado = "spu_Reporte_TiempoConexionPorEjecutivo" Then
                arParms(2) = New SqlParameter("@idUsuario", SqlDbType.VarChar)
                arParms(2).Value = unicaInstanciaSupervisorCallcenterCemtraRegistrado.ObtenerIdUsuario
            End If
        End If
        Return arParms
    End Function

End Class

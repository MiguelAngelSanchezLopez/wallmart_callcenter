﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Alerta
  Inherits AlertaMasterClass

#Region "Properties"
  Public Const NombreEntidad As String = "Alerta"
  Public Const Directorio As String = "Alerta\"

  Public Property Existe As Boolean
#End Region

#Region "Constantes & Enums"
  Public Const NO_CONTESTA As String = "NO CONTESTA"
  Public Const EN_LOCAL As String = "EN LOCAL"
  Public Const SUPERVISOR As String = "SUPERVISOR"
  Public Const GRUPO_CONTACTO_CONDUCTOR As String = "CONDUCTOR"
  Public Const GRUPO_CONTACTO_TRANSPORTISTA As String = "TRANSPORTISTA"
  Public Const MULTIPUNTO As String = "MULTIPUNTO"
  Public Const LOCAL As String = "LOCAL"
  Public Const CALLCENTER_ESTADO_LLAMADA_LLAMANDO As String = "LLAMANDO"
  Public Const CALLCENTER_ESTADO_LLAMADA_HABLANDO As String = "HABLANDO"
  Public Const CALLCENTER_ESTADO_LLAMADA_FINALIZADO As String = "FINALIZADO"
  Public Const ESTADO_ALERTA_ROJA_ENVIADO_TRANSPORTISTA As String = "ENVIADO TRANSPORTISTA"
  Public Const ESTADO_ALERTA_ROJA_ENVIADO_OPERACION_TRANSPORTE As String = "ENVIADO OPERACION TRANSPORTE"
  Public Const ESTADO_ALERTA_ROJA_FINALIZADO As String = "FINALIZADO"
  Public Const ESTADO_ALERTA_ROJA_RECHAZADO As String = "RECHAZADO"
  Public Const ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION As String = "POR APROBAR PLAN ACCION"
  Public Const ENTIDAD_ALERTA_ROJA_RESPUESTA As String = "AlertaRojaRespuesta"
  Public Const TIPO_GRUPO_PERSONALIZADO As String = "Personalizado"
  Public Const CIERRE_VIAJE As String = "CIERRE VIAJE"
  Public Const REPROGRAMAR As String = "REPROGRAMAR"

  Public Enum eTabla As Integer
    T00_Detalle = 0
    T01_HistorialEscalamientos = 1
    T02_HistorialEscalamientosContactos = 2
    T03_ContactosProximoEscalamiento = 3
    T04_ScriptGrupoContacto = 4
    T05_HistorialLlamadas = 5
    T06_HistorialGestionAlertas = 6
  End Enum

  Public Enum eTablaAlertasSinAtender As Integer
    T00_Listado = 0
  End Enum

  Public Enum eTablaAlertasPorGestionar As Integer
    T00_Listado = 0
  End Enum

  Public Enum eAccionHistorial
    TELEOPERADOR_ALERTA_ASIGNADA
    TELEOPERADOR_ALERTA_GRABADA
    TELEOPERADOR_OCUPADO
    TELEOPERADOR_CERRAR_SESION
    SUPERVISOR_ALERTA_CERRADO_PENDIENTE
    SUPERVISOR_ALERTA_CERRADO_SATISFACTORIO
    SUPERVISOR_ALERTA_CERRADO_NO_SATISFACTORIO
  End Enum

  Public Enum eTablaConfiguracion As Integer
    T00_Detalle = 0
    T01_Escalamientos = 1
    T02_GrupoContactos = 2
    T03_UsuarioGrupoContactos = 3
    T04_Script = 4
  End Enum

  Enum eTablaRespuestaAlerta As Integer
    T00_ListadoAlertas = 0
    T01_ListadoCategoriasPorAlerta = 1
    T02_ArchivosAdjuntos = 2
    T03_EstadoAlertaRoja = 3
    T04_EstadoTransportista = 4
    T05_RespuestaTransportista = 5
  End Enum

  Public Class AlertaEnColaAtencion
    Property IdAlerta As Integer
    Property IdAlertaHija As Integer
    Property NroTransporte As Int64
    Property IdEmbarque As Int64
    Property NombreAlerta As String
    Property LocalDestino As Integer
    Property IdFormato As Integer
    Property Prioridad As String
    Property OrdenPrioridad As Integer
    Property FechaHoraCreacion As DateTime
    Property FechaHoraCreacionDMA As String
  End Class

#End Region

#Region "Constructor"

  Public Sub New()
    MyBase.New()
  End Sub

  Public Sub New(ByVal Id As Integer)
    Try
      Me.Id = Id
      ObtenerPorId()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

  Public Sub New(ByVal NroTransporte As String)
    Try
      Me.NroTransporte = NroTransporte
      ObtenerPorNroTransporte()
    Catch ex As System.Exception
      Throw New System.Exception("Error al crear una instancia de " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Publicos"
  Public Sub GuardarNuevo()

    Dim storedProcedure As String = "spu_Alerta_GuardarNuevo"
    Dim descripcionError As String = "Error al guardar nuevo " & NombreEntidad
    Dim parms() As SqlParameter = New SqlParameter(40) {}

    Try
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(2) = New SqlParameter("@DescripcionAlerta", SqlDbType.VarChar, 255)
      parms(3) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      parms(4) = New SqlParameter("@OrigenCodigo", SqlDbType.Int)
      parms(5) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar, 255)
      parms(6) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar, 255)
      parms(7) = New SqlParameter("@RutTransportista", SqlDbType.VarChar, 255)
      parms(8) = New SqlParameter("@NombreTransportista", SqlDbType.VarChar, 255)
      parms(9) = New SqlParameter("@RutConductor", SqlDbType.VarChar, 255)
      parms(10) = New SqlParameter("@NombreConductor", SqlDbType.VarChar, 255)
      parms(11) = New SqlParameter("@FechaUltimaTransmision", SqlDbType.DateTime)
      parms(12) = New SqlParameter("@FechaInicioAlerta", SqlDbType.DateTime)
      parms(13) = New SqlParameter("@LatTrailer", SqlDbType.VarChar, 255)
      parms(14) = New SqlParameter("@LonTrailer", SqlDbType.VarChar, 255)
      parms(15) = New SqlParameter("@LatTracto", SqlDbType.VarChar, 255)
      parms(16) = New SqlParameter("@LonTracto", SqlDbType.VarChar, 255)
      parms(17) = New SqlParameter("@TipoAlerta", SqlDbType.VarChar, 255)
      parms(18) = New SqlParameter("@Permiso", SqlDbType.VarChar, 255)
      parms(19) = New SqlParameter("@Criticidad", SqlDbType.VarChar, 255)
      parms(20) = New SqlParameter("@TipoAlertaDescripcion", SqlDbType.VarChar, 255)
      parms(21) = New SqlParameter("@AlertaMapa", SqlDbType.VarChar, 255)
      parms(22) = New SqlParameter("@NombreZona", SqlDbType.VarChar, 255)
      parms(23) = New SqlParameter("@Velocidad", SqlDbType.VarChar, 255)
      parms(24) = New SqlParameter("@EstadoGPSTracto", SqlDbType.VarChar, 255)
      parms(25) = New SqlParameter("@EstadoGPSRampla", SqlDbType.VarChar, 255)
      parms(26) = New SqlParameter("@EstadoGPS", SqlDbType.VarChar, 255)
      parms(27) = New SqlParameter("@TipoPunto", SqlDbType.VarChar, 255)
      parms(28) = New SqlParameter("@Temp1", SqlDbType.VarChar, 255)
      parms(29) = New SqlParameter("@Temp2", SqlDbType.VarChar, 255)
      parms(30) = New SqlParameter("@DistanciaTT", SqlDbType.VarChar, 255)
      parms(31) = New SqlParameter("@TransportistaTrailer", SqlDbType.VarChar, 255)
      parms(32) = New SqlParameter("@CantidadSatelites", SqlDbType.Int)
      parms(33) = New SqlParameter("@FechaHoraCreacion", SqlDbType.DateTime)
      parms(34) = New SqlParameter("@FechaHoraActualizacion", SqlDbType.DateTime)
      parms(35) = New SqlParameter("@EnvioCorreo", SqlDbType.Bit)
      parms(36) = New SqlParameter("@FueraDeHorario", SqlDbType.Bit)
      parms(37) = New SqlParameter("@Activo", SqlDbType.Bit)
      parms(38) = New SqlParameter("@ConLog", SqlDbType.Bit)
      parms(39) = New SqlParameter("@Ocurrencia", SqlDbType.Int)
      parms(40) = New SqlParameter("@GrupoAlerta", SqlDbType.VarChar, 255)

      parms(0).Direction = ParameterDirection.Output
      parms(1).Value = Me.NroTransporte
      parms(2).Value = Me.DescripcionAlerta
      parms(3).Value = Me.LocalDestino
      parms(4).Value = Me.OrigenCodigo
      parms(5).Value = Me.PatenteTracto
      parms(6).Value = Me.PatenteTrailer
      parms(7).Value = Me.RutTransportista
      parms(8).Value = Me.NombreTransportista
      parms(9).Value = Me.RutConductor
      parms(10).Value = Me.NombreConductor
      parms(11).Value = Me.FechaUltimaTransmision
      parms(12).Value = Me.FechaInicioAlerta
      parms(13).Value = Me.LatTrailer
      parms(14).Value = Me.LonTrailer
      parms(15).Value = Me.LatTracto
      parms(16).Value = Me.LonTracto
      parms(17).Value = Me.TipoAlerta
      parms(18).Value = Me.Permiso
      parms(19).Value = Me.Criticidad
      parms(20).Value = Me.TipoAlertaDescripcion
      parms(21).Value = Me.AlertaMapa
      parms(22).Value = Me.NombreZona
      parms(23).Value = Me.Velocidad
      parms(24).Value = Me.EstadoGPSTracto
      parms(25).Value = Me.EstadoGPSRampla
      parms(26).Value = Me.EstadoGPS
      parms(27).Value = Me.TipoPunto
      parms(28).Value = Me.Temp1
      parms(29).Value = Me.Temp2
      parms(30).Value = Me.DistanciaTT
      parms(31).Value = Me.TransportistaTrailer
      parms(32).Value = Me.CantidadSatelites
      parms(33).Value = Me.FechaHoraCreacion
      parms(34).Value = Me.FechaHoraActualizacion
      parms(35).Value = Me.EnvioCorreo
      parms(36).Value = Me.FueraDeHorario
      parms(37).Value = Me.Activo
      parms(38).Value = Me.ConLog
      parms(39).Value = Me.Ocurrencia
      parms(40).Value = Me.GrupoAlerta

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      Me.Id = parms(0).Value
      Me.Existe = True

      ObtenerPorId() ' refrescar la instancia
    Catch
      Throw New System.Exception(descripcionError & Err.Description)
    Finally
      'Nothing
    End Try
  End Sub

  Public Sub Actualizar()
    Try
      If Not Me.Existe Then Throw New Exception("Instancia no existe o no es valida")

      Dim storedProcedure As String = "spu_Alerta_Actualizar"
      Dim parms() As SqlParameter = New SqlParameter(40) {}

      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(2) = New SqlParameter("@DescripcionAlerta", SqlDbType.VarChar, 255)
      parms(3) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      parms(4) = New SqlParameter("@OrigenCodigo", SqlDbType.Int)
      parms(5) = New SqlParameter("@PatenteTracto", SqlDbType.VarChar, 255)
      parms(6) = New SqlParameter("@PatenteTrailer", SqlDbType.VarChar, 255)
      parms(7) = New SqlParameter("@RutTransportista", SqlDbType.VarChar, 255)
      parms(8) = New SqlParameter("@NombreTransportista", SqlDbType.VarChar, 255)
      parms(9) = New SqlParameter("@RutConductor", SqlDbType.VarChar, 255)
      parms(10) = New SqlParameter("@NombreConductor", SqlDbType.VarChar, 255)
      parms(11) = New SqlParameter("@FechaUltimaTransmision", SqlDbType.DateTime)
      parms(12) = New SqlParameter("@FechaInicioAlerta", SqlDbType.DateTime)
      parms(13) = New SqlParameter("@LatTrailer", SqlDbType.VarChar, 255)
      parms(14) = New SqlParameter("@LonTrailer", SqlDbType.VarChar, 255)
      parms(15) = New SqlParameter("@LatTracto", SqlDbType.VarChar, 255)
      parms(16) = New SqlParameter("@LonTracto", SqlDbType.VarChar, 255)
      parms(17) = New SqlParameter("@TipoAlerta", SqlDbType.VarChar, 255)
      parms(18) = New SqlParameter("@Permiso", SqlDbType.VarChar, 255)
      parms(19) = New SqlParameter("@Criticidad", SqlDbType.VarChar, 255)
      parms(20) = New SqlParameter("@TipoAlertaDescripcion", SqlDbType.VarChar, 255)
      parms(21) = New SqlParameter("@AlertaMapa", SqlDbType.VarChar, 255)
      parms(22) = New SqlParameter("@NombreZona", SqlDbType.VarChar, 255)
      parms(23) = New SqlParameter("@Velocidad", SqlDbType.VarChar, 255)
      parms(24) = New SqlParameter("@EstadoGPSTracto", SqlDbType.VarChar, 255)
      parms(25) = New SqlParameter("@EstadoGPSRampla", SqlDbType.VarChar, 255)
      parms(26) = New SqlParameter("@EstadoGPS", SqlDbType.VarChar, 255)
      parms(27) = New SqlParameter("@TipoPunto", SqlDbType.VarChar, 255)
      parms(28) = New SqlParameter("@Temp1", SqlDbType.VarChar, 255)
      parms(29) = New SqlParameter("@Temp2", SqlDbType.VarChar, 255)
      parms(30) = New SqlParameter("@DistanciaTT", SqlDbType.VarChar, 255)
      parms(31) = New SqlParameter("@TransportistaTrailer", SqlDbType.VarChar, 255)
      parms(32) = New SqlParameter("@CantidadSatelites", SqlDbType.Int)
      parms(33) = New SqlParameter("@FechaHoraCreacion", SqlDbType.DateTime)
      parms(34) = New SqlParameter("@FechaHoraActualizacion", SqlDbType.DateTime)
      parms(35) = New SqlParameter("@EnvioCorreo", SqlDbType.Bit)
      parms(36) = New SqlParameter("@FueraDeHorario", SqlDbType.Bit)
      parms(37) = New SqlParameter("@Activo", SqlDbType.Bit)
      parms(38) = New SqlParameter("@ConLog", SqlDbType.Bit)
      parms(39) = New SqlParameter("@Ocurrencia", SqlDbType.Int)
      parms(40) = New SqlParameter("@GrupoAlerta", SqlDbType.VarChar, 255)

      parms(0).Value = Me.Id
      parms(1).Value = Me.NroTransporte
      parms(2).Value = Me.DescripcionAlerta
      parms(3).Value = Me.LocalDestino
      parms(4).Value = Me.OrigenCodigo
      parms(5).Value = Me.PatenteTracto
      parms(6).Value = Me.PatenteTrailer
      parms(7).Value = Me.RutTransportista
      parms(8).Value = Me.NombreTransportista
      parms(9).Value = Me.RutConductor
      parms(10).Value = Me.NombreConductor
      parms(11).Value = Me.FechaUltimaTransmision
      parms(12).Value = Me.FechaInicioAlerta
      parms(13).Value = Me.LatTrailer
      parms(14).Value = Me.LonTrailer
      parms(15).Value = Me.LatTracto
      parms(16).Value = Me.LonTracto
      parms(17).Value = Me.TipoAlerta
      parms(18).Value = Me.Permiso
      parms(19).Value = Me.Criticidad
      parms(20).Value = Me.TipoAlertaDescripcion
      parms(21).Value = Me.AlertaMapa
      parms(22).Value = Me.NombreZona
      parms(23).Value = Me.Velocidad
      parms(24).Value = Me.EstadoGPSTracto
      parms(25).Value = Me.EstadoGPSRampla
      parms(26).Value = Me.EstadoGPS
      parms(27).Value = Me.TipoPunto
      parms(28).Value = Me.Temp1
      parms(29).Value = Me.Temp2
      parms(30).Value = Me.DistanciaTT
      parms(31).Value = Me.TransportistaTrailer
      parms(32).Value = Me.CantidadSatelites
      parms(33).Value = Me.FechaHoraCreacion
      parms(34).Value = Me.FechaHoraActualizacion
      parms(35).Value = Me.EnvioCorreo
      parms(36).Value = Me.FueraDeHorario
      parms(37).Value = Me.Activo
      parms(38).Value = Me.ConLog
      parms(39).Value = Me.Ocurrencia
      parms(40).Value = Me.GrupoAlerta

      SqlHelper.ExecuteNonQuery(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)
    Catch ex As Exception
      Throw New System.Exception("Error al actualizar " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Privados"
  Private Sub ObtenerPorId()
    Try
      Existe = False

      Dim storedProcedure As String = "spu_Alerta_ObtenerPorId"
      Dim parms() As SqlParameter = New SqlParameter(0) {}
      parms(0) = New SqlParameter("@Id", SqlDbType.Int)
      parms(0).Value = Id

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("NroTransporte")) Then Me.NroTransporte = row("NroTransporte")
        If Not IsDBNull(row("DescripcionAlerta")) Then Me.DescripcionAlerta = row("DescripcionAlerta")
        If Not IsDBNull(row("LocalDestino")) Then Me.LocalDestino = row("LocalDestino")
        If Not IsDBNull(row("OrigenCodigo")) Then Me.OrigenCodigo = row("OrigenCodigo")
        If Not IsDBNull(row("PatenteTracto")) Then Me.PatenteTracto = row("PatenteTracto")
        If Not IsDBNull(row("PatenteTrailer")) Then Me.PatenteTrailer = row("PatenteTrailer")
        If Not IsDBNull(row("RutTransportista")) Then Me.RutTransportista = row("RutTransportista")
        If Not IsDBNull(row("NombreTransportista")) Then Me.NombreTransportista = row("NombreTransportista")
        If Not IsDBNull(row("RutConductor")) Then Me.RutConductor = row("RutConductor")
        If Not IsDBNull(row("NombreConductor")) Then Me.NombreConductor = row("NombreConductor")
        If Not IsDBNull(row("FechaUltimaTransmision")) Then Me.FechaUltimaTransmision = row("FechaUltimaTransmision")
        If Not IsDBNull(row("FechaInicioAlerta")) Then Me.FechaInicioAlerta = row("FechaInicioAlerta")
        If Not IsDBNull(row("LatTrailer")) Then Me.LatTrailer = row("LatTrailer")
        If Not IsDBNull(row("LonTrailer")) Then Me.LonTrailer = row("LonTrailer")
        If Not IsDBNull(row("LatTracto")) Then Me.LatTracto = row("LatTracto")
        If Not IsDBNull(row("LonTracto")) Then Me.LonTracto = row("LonTracto")
        If Not IsDBNull(row("TipoAlerta")) Then Me.TipoAlerta = row("TipoAlerta")
        If Not IsDBNull(row("Permiso")) Then Me.Permiso = row("Permiso")
        If Not IsDBNull(row("Criticidad")) Then Me.Criticidad = row("Criticidad")
        If Not IsDBNull(row("TipoAlertaDescripcion")) Then Me.TipoAlertaDescripcion = row("TipoAlertaDescripcion")
        If Not IsDBNull(row("AlertaMapa")) Then Me.AlertaMapa = row("AlertaMapa")
        If Not IsDBNull(row("NombreZona")) Then Me.NombreZona = row("NombreZona")
        If Not IsDBNull(row("Velocidad")) Then Me.Velocidad = row("Velocidad")
        If Not IsDBNull(row("EstadoGPSTracto")) Then Me.EstadoGPSTracto = row("EstadoGPSTracto")
        If Not IsDBNull(row("EstadoGPSRampla")) Then Me.EstadoGPSRampla = row("EstadoGPSRampla")
        If Not IsDBNull(row("EstadoGPS")) Then Me.EstadoGPS = row("EstadoGPS")
        If Not IsDBNull(row("TipoPunto")) Then Me.TipoPunto = row("TipoPunto")
        If Not IsDBNull(row("Temp1")) Then Me.Temp1 = row("Temp1")
        If Not IsDBNull(row("Temp2")) Then Me.Temp2 = row("Temp2")
        If Not IsDBNull(row("DistanciaTT")) Then Me.DistanciaTT = row("DistanciaTT")
        If Not IsDBNull(row("TransportistaTrailer")) Then Me.TransportistaTrailer = row("TransportistaTrailer")
        If Not IsDBNull(row("CantidadSatelites")) Then Me.CantidadSatelites = row("CantidadSatelites")
        If Not IsDBNull(row("FechaHoraCreacion")) Then Me.FechaHoraCreacion = row("FechaHoraCreacion")
        If Not IsDBNull(row("FechaHoraActualizacion")) Then Me.FechaHoraActualizacion = row("FechaHoraActualizacion")
        If Not IsDBNull(row("EnvioCorreo")) Then Me.EnvioCorreo = row("EnvioCorreo")
        If Not IsDBNull(row("FueraDeHorario")) Then Me.FueraDeHorario = row("FueraDeHorario")
        If Not IsDBNull(row("Activo")) Then Me.Activo = row("Activo")
        If Not IsDBNull(row("ConLog")) Then Me.ConLog = row("ConLog")
        If Not IsDBNull(row("Ocurrencia")) Then Me.ConLog = row("Ocurrencia")
        If Not IsDBNull(row("GrupoAlerta")) Then Me.GrupoAlerta = row("GrupoAlerta")
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try

  End Sub

  Private Sub ObtenerPorNroTransporte()
    Try
      Me.Existe = False

      Dim storedProcedure As String = "spu_Alerta_ObtenerPorNroTransporte"
      Dim parms() As SqlParameter = New SqlParameter(0) {}
      parms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      parms(0).Value = Me.NroTransporte

      Dim ds As DataSet = SqlHelper.ExecuteDataset(Conexion.StringConexion(), CommandType.StoredProcedure, storedProcedure, parms)

      If ds.Tables(0).Rows.Count > 0 Then
        Dim row As DataRow = ds.Tables(0).Rows(0)

        Me.Existe = True
        If Not IsDBNull(row("Id")) Then Me.Id = row("Id")
        If Not IsDBNull(row("NroTransporte")) Then Me.NroTransporte = row("NroTransporte")
        If Not IsDBNull(row("DescripcionAlerta")) Then Me.DescripcionAlerta = row("DescripcionAlerta")
        If Not IsDBNull(row("LocalDestino")) Then Me.LocalDestino = row("LocalDestino")
        If Not IsDBNull(row("OrigenCodigo")) Then Me.OrigenCodigo = row("OrigenCodigo")
        If Not IsDBNull(row("PatenteTracto")) Then Me.PatenteTracto = row("PatenteTracto")
        If Not IsDBNull(row("PatenteTrailer")) Then Me.PatenteTrailer = row("PatenteTrailer")
        If Not IsDBNull(row("RutTransportista")) Then Me.RutTransportista = row("RutTransportista")
        If Not IsDBNull(row("NombreTransportista")) Then Me.NombreTransportista = row("NombreTransportista")
        If Not IsDBNull(row("RutConductor")) Then Me.RutConductor = row("RutConductor")
        If Not IsDBNull(row("NombreConductor")) Then Me.NombreConductor = row("NombreConductor")
        If Not IsDBNull(row("FechaUltimaTransmision")) Then Me.FechaUltimaTransmision = row("FechaUltimaTransmision")
        If Not IsDBNull(row("FechaInicioAlerta")) Then Me.FechaInicioAlerta = row("FechaInicioAlerta")
        If Not IsDBNull(row("LatTrailer")) Then Me.LatTrailer = row("LatTrailer")
        If Not IsDBNull(row("LonTrailer")) Then Me.LonTrailer = row("LonTrailer")
        If Not IsDBNull(row("LatTracto")) Then Me.LatTracto = row("LatTracto")
        If Not IsDBNull(row("LonTracto")) Then Me.LonTracto = row("LonTracto")
        If Not IsDBNull(row("TipoAlerta")) Then Me.TipoAlerta = row("TipoAlerta")
        If Not IsDBNull(row("Permiso")) Then Me.Permiso = row("Permiso")
        If Not IsDBNull(row("Criticidad")) Then Me.Criticidad = row("Criticidad")
        If Not IsDBNull(row("TipoAlertaDescripcion")) Then Me.TipoAlertaDescripcion = row("TipoAlertaDescripcion")
        If Not IsDBNull(row("AlertaMapa")) Then Me.AlertaMapa = row("AlertaMapa")
        If Not IsDBNull(row("NombreZona")) Then Me.NombreZona = row("NombreZona")
        If Not IsDBNull(row("Velocidad")) Then Me.Velocidad = row("Velocidad")
        If Not IsDBNull(row("EstadoGPSTracto")) Then Me.EstadoGPSTracto = row("EstadoGPSTracto")
        If Not IsDBNull(row("EstadoGPSRampla")) Then Me.EstadoGPSRampla = row("EstadoGPSRampla")
        If Not IsDBNull(row("EstadoGPS")) Then Me.EstadoGPS = row("EstadoGPS")
        If Not IsDBNull(row("TipoPunto")) Then Me.TipoPunto = row("TipoPunto")
        If Not IsDBNull(row("Temp1")) Then Me.Temp1 = row("Temp1")
        If Not IsDBNull(row("Temp2")) Then Me.Temp2 = row("Temp2")
        If Not IsDBNull(row("DistanciaTT")) Then Me.DistanciaTT = row("DistanciaTT")
        If Not IsDBNull(row("TransportistaTrailer")) Then Me.TransportistaTrailer = row("TransportistaTrailer")
        If Not IsDBNull(row("CantidadSatelites")) Then Me.CantidadSatelites = row("CantidadSatelites")
        If Not IsDBNull(row("FechaHoraCreacion")) Then Me.FechaHoraCreacion = row("FechaHoraCreacion")
        If Not IsDBNull(row("FechaHoraActualizacion")) Then Me.FechaHoraActualizacion = row("FechaHoraActualizacion")
        If Not IsDBNull(row("EnvioCorreo")) Then Me.EnvioCorreo = row("EnvioCorreo")
        If Not IsDBNull(row("FueraDeHorario")) Then Me.FueraDeHorario = row("FueraDeHorario")
        If Not IsDBNull(row("Activo")) Then Me.Activo = row("Activo")
        If Not IsDBNull(row("ConLog")) Then Me.ConLog = row("ConLog")
        If Not IsDBNull(row("Ocurrencia")) Then Me.ConLog = row("Ocurrencia")
        If Not IsDBNull(row("GrupoAlerta")) Then Me.GrupoAlerta = row("GrupoAlerta")
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al obtener registro " & NombreEntidad & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene listado alertas por atender
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerAlertasPorAtender(ByVal idUsuario As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerAlertasPorAtender", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado alertas sin atender
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerAlertasSinAtender() As DataSet
    Dim ds As New DataSet
    Try
      Dim objCommand As New SqlClient.SqlCommand("spu_Alerta_ObtenerAlertasSinAtender")
      Dim conn As String = Conexion.StringConexion
      objCommand.CommandType = CommandType.StoredProcedure
      objCommand.CommandTimeout = 0
      objCommand.Connection = New SqlClient.SqlConnection(conn)
      objCommand.Connection.Open()

      Dim objAdapter As New SqlDataAdapter(objCommand)
      objAdapter.Fill(ds)
      objCommand.Connection.Close()
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de alertas atendidas el dia de hoy
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerAlertasAtendidasHoy() As DataSet
    Dim ds As New DataSet
    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerAlertasAtendidasHoy")
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle de la alerta
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal idAlerta As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(0).Value = idAlerta

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba la gestion de la alerta realizada por el teleoperador
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarGestionTeleOperador(ByVal idAlerta As String, ByVal idUsuario As String, ByVal proximoEscalamiento As String, ByVal nombreAlerta As String, _
                                                   ByVal explicacion As String, ByVal explicacionOtro As String, ByVal observacion As String, ByVal idFormato As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(8) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(1).Value = idAlerta
      arParms(2) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(2).Value = idUsuario
      arParms(3) = New SqlParameter("@ProximoEscalamiento", SqlDbType.Int)
      arParms(3).Value = proximoEscalamiento
      arParms(4) = New SqlParameter("@NombreAlerta", SqlDbType.VarChar)
      arParms(4).Value = nombreAlerta
      arParms(5) = New SqlParameter("@Explicacion", SqlDbType.VarChar)
      arParms(5).Value = explicacion
      arParms(6) = New SqlParameter("@ExplicacionOtro", SqlDbType.VarChar)
      arParms(6).Value = explicacionOtro
      arParms(7) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      arParms(7).Value = observacion
      arParms(8) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(8).Value = idFormato

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarGestionTeleOperador", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' graba los contactos de la gestion realizada
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarGestionContactos(ByVal idHistorialEscalamiento As String, ByVal idEscalamientoPorAlertaContacto As String, _
                                                ByVal explicacion As String, ByVal explicacionOtro As String, ByVal observacion As String, _
                                                ByVal idUsuarioSinGrupoContacto As String, ByVal idEscalamientoPorAlertaGrupoContacto As String, _
                                                ByVal categoriaAlerta As String, ByVal tipoObservacion As String, ByVal clasificacionAlerta As String, ByVal clasificacionAlertaSist As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(11) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdHistorialEscalamiento", SqlDbType.Int)
      arParms(1).Value = idHistorialEscalamiento
      arParms(2) = New SqlParameter("@IdEscalamientoPorAlertaContacto", SqlDbType.Int)
      arParms(2).Value = idEscalamientoPorAlertaContacto
      arParms(3) = New SqlParameter("@Explicacion", SqlDbType.VarChar)
      arParms(3).Value = explicacion
      arParms(4) = New SqlParameter("@ExplicacionOtro", SqlDbType.VarChar)
      arParms(4).Value = explicacionOtro
      arParms(5) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      arParms(5).Value = observacion
      arParms(6) = New SqlParameter("@IdUsuarioSinGrupoContacto", SqlDbType.Int)
      arParms(6).Value = idUsuarioSinGrupoContacto
      arParms(7) = New SqlParameter("@IdEscalamientoPorAlertaGrupoContacto", SqlDbType.Int)
      arParms(7).Value = idEscalamientoPorAlertaGrupoContacto
      arParms(8) = New SqlParameter("@categoriaAlerta", SqlDbType.VarChar)
      arParms(8).Value = categoriaAlerta
      arParms(9) = New SqlParameter("@tipoObservacion", SqlDbType.VarChar)
      arParms(9).Value = tipoObservacion
      arParms(10) = New SqlParameter("@clasificacionAlerta", SqlDbType.VarChar)
      arParms(10).Value = clasificacionAlerta
      arParms(11) = New SqlParameter("@clasificacionAlertaSist", SqlDbType.VarChar)
      arParms(11).Value = clasificacionAlertaSist

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarGestionContactos", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' graba alerta acumulada
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarAlertaGestionAcumulada(ByVal idAlertaGestionAcumulada As String, ByRef fechaAsignacion As String, ByVal idAlerta As String, ByVal idAlertaHija As String, _
                                                      ByVal idUsuarioAsignado As String, ByVal cerrado As String, ByVal cerradoSatisfactorio As String, ByVal idHistorialEscalamiento As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(8) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@FechaAsignacion", SqlDbType.VarChar, 50)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdAlertaGestionAcumulada", SqlDbType.Int)
      arParms(2).Value = idAlertaGestionAcumulada
      arParms(3) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(3).Value = idAlerta
      arParms(4) = New SqlParameter("@IdAlertaHija", SqlDbType.Int)
      arParms(4).Value = idAlertaHija
      arParms(5) = New SqlParameter("@IdUsuarioAsignado", SqlDbType.Int)
      arParms(5).Value = idUsuarioAsignado
      arParms(6) = New SqlParameter("@Cerrado", SqlDbType.Int)
      arParms(6).Value = cerrado
      arParms(7) = New SqlParameter("@CerradoSatisfactorio", SqlDbType.Int)
      arParms(7).Value = cerradoSatisfactorio
      arParms(8) = New SqlParameter("@IdHistorialEscalamiento", SqlDbType.Int)
      arParms(8).Value = idHistorialEscalamiento

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarAlertaGestionAcumulada", arParms)

      'retorna valor
      status = arParms(0).Value
      fechaAsignacion = arParms(1).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' graba el historial de acciones para la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarHistorialAlertaGestion(ByVal idAlertaPadre As String, ByVal idAlertaHija As String, ByVal idUsuarioCreacion As String, _
                                                      ByVal accion As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(4) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaPadre", SqlDbType.Int)
      arParms(1).Value = idAlertaPadre
      arParms(2) = New SqlParameter("@IdAlertaHija", SqlDbType.Int)
      arParms(2).Value = idAlertaHija
      arParms(3) = New SqlParameter("@IdUsuarioCreacion", SqlDbType.Int)
      arParms(3).Value = idUsuarioCreacion
      arParms(4) = New SqlParameter("@Accion", SqlDbType.VarChar)
      arParms(4).Value = accion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarHistorialAlertaGestion", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' obtiene alerta que tiene asignada actualmente el usuario conectado
  ''' </summary>
  ''' <param name="idUsuarioAsignado"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerAsignadaActualmente(ByVal idUsuarioAsignado As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdUsuarioAsignado", SqlDbType.Int)
      arParms(0).Value = idUsuarioAsignado

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerAsignadaActualmente", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' desasigna alerta del usuario
  ''' </summary>
  ''' <param name="idAlertaGestionAcumulada"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function DesvincularDeUsuario(ByVal idAlertaGestionAcumulada As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaGestionAcumulada", SqlDbType.Int)
      arParms(1).Value = idAlertaGestionAcumulada

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_DesvincularDeUsuario", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' obtiene listado alertas por gestionar
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerAlertasPorGestionar() As DataSet
    Dim ds As New DataSet
    Try
      Dim objCommand As New SqlClient.SqlCommand("spu_Alerta_ObtenerAlertasPorGestionar")
      Dim conn As String = Conexion.StringConexion
      objCommand.CommandType = CommandType.StoredProcedure
      objCommand.CommandTimeout = 0
      objCommand.Connection = New SqlClient.SqlConnection(conn)
      objCommand.Connection.Open()

      Dim objAdapter As New SqlDataAdapter(objCommand)
      objAdapter.Fill(ds)
      objCommand.Connection.Close()
    Catch ex As Exception
      ds = Nothing
    End Try

    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba la gestion de la alerta realizada por el supervisor
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarGestionSupervisor(ByVal idAlerta As String, ByVal idUsuario As String, ByVal explicacion As String, ByVal explicacionOtro As String, ByVal observacion As String, ByVal categoriaAlerta As String, ByVal tipoObservacion As String, ByVal clasificacionAlerta As String, ByVal ClasificacionAlertaSist As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(9) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(1).Value = idAlerta
      arParms(2) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(2).Value = idUsuario
      arParms(3) = New SqlParameter("@Explicacion", SqlDbType.VarChar)
      arParms(3).Value = explicacion
      arParms(4) = New SqlParameter("@ExplicacionOtro", SqlDbType.VarChar)
      arParms(4).Value = explicacionOtro
      arParms(5) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      arParms(5).Value = observacion
      arParms(6) = New SqlParameter("@categoriaAlerta", SqlDbType.VarChar)
      arParms(6).Value = categoriaAlerta
      arParms(7) = New SqlParameter("@tipoObservacion", SqlDbType.VarChar)
      arParms(7).Value = tipoObservacion
      arParms(8) = New SqlParameter("@clasificacionAlerta", SqlDbType.VarChar)
      arParms(8).Value = clasificacionAlerta
      arParms(9) = New SqlParameter("@ClasificacionAlertaSist", SqlDbType.VarChar)
      arParms(9).Value = ClasificacionAlertaSist

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarGestionSupervisor", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' obtiene listado de conductores
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerConductores(ByVal rutConductor As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@RutConductor", SqlDbType.VarChar)
      arParms(0).Value = rutConductor

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerConductores", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene el listado de alertas que se deben configurar
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerListadoConfiguracion(ByVal idUsuario As String, ByVal nombre As String, ByVal prioridad As String, ByVal soloActivos As String, _
                                                     ByVal tipoAlerta As String, ByVal idFormato As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = nombre
      arParms(2) = New SqlParameter("@Prioridad", SqlDbType.VarChar)
      arParms(2).Value = prioridad
      arParms(3) = New SqlParameter("@SoloActivos", SqlDbType.Int)
      arParms(3).Value = soloActivos
      arParms(4) = New SqlParameter("@TipoAlerta", SqlDbType.VarChar)
      arParms(4).Value = tipoAlerta
      arParms(5) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(5).Value = idFormato

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerListadoConfiguracion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene los datos del detalle de la configuración de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalleConfiguracion(ByVal idAlertaConfiguracion As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdAlertaConfiguracion", SqlDbType.Int)
      arParms(0).Value = idAlertaConfiguracion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerDetalleConfiguracion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' determina si un valor ya existe en base de datos para la pagina seleccionada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function VerificarDuplicidad(ByVal idAlerta As String, ByVal valor As String, ByVal idFormato As String, ByVal campoARevisar As String) As String
    Dim estaDuplicado As String = "0"
    Try
      Dim storedProcedure As String = "spu_Alerta_VerificarDuplicidad" & campoARevisar
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(1).Value = idAlerta
      arParms(2) = New SqlParameter("@Valor", SqlDbType.VarChar)
      arParms(2).Value = valor
      arParms(3) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(3).Value = idFormato

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      estaDuplicado = arParms(0).Value
    Catch ex As Exception
      estaDuplicado = "0"
    End Try
    Return estaDuplicado
  End Function

  ''' <summary>
  ''' graba los datos de la configuracion de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarDatosConfiguracion(ByRef idAlertaConfiguracion As String, ByVal idAlertaDefinicionPorFormato As String, ByVal prioridad As String, ByVal frecuenciaRepeticionMinutos As String, _
                                                  ByVal activo As String, ByVal tipoAlerta As String, ByVal fechaInicio As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(8) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaConfiguracionOutput", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdAlertaConfiguracion", SqlDbType.Int)
      arParms(2).Value = idAlertaConfiguracion
      arParms(3) = New SqlParameter("@IdAlertaDefinicionPorFormato", SqlDbType.Int)
      arParms(3).Value = idAlertaDefinicionPorFormato
      arParms(4) = New SqlParameter("@Prioridad", SqlDbType.VarChar)
      arParms(4).Value = prioridad
      arParms(5) = New SqlParameter("@FrecuenciaRepeticionMinutos", SqlDbType.Int)
      arParms(5).Value = frecuenciaRepeticionMinutos
      arParms(6) = New SqlParameter("@Activo", SqlDbType.Int)
      arParms(6).Value = activo
      arParms(7) = New SqlParameter("@TipoAlerta", SqlDbType.VarChar)
      arParms(7).Value = tipoAlerta
      arParms(8) = New SqlParameter("@FechaInicio", SqlDbType.VarChar)
      arParms(8).Value = fechaInicio

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarDatosConfiguracion", arParms)
      valor = arParms(0).Value
      idAlertaConfiguracion = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Alerta.GrabarDatosConfiguracion" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Alerta.GrabarDatosConfiguracion" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' elimina la configuracion de la alerta
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarDatosConfiguracion(ByVal idAlertaConfiguracion As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaConfiguracion", SqlDbType.Int)
      arParms(1).Value = idAlertaConfiguracion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarDatosConfiguracion", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Alerta.EliminarDatosConfiguracion" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' graba los datos del escalamiento
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarConfiguracionEscalamiento(ByVal idEscalamientoPorAlerta As String, ByVal idAlertaConfiguracion As String, ByVal orden As String, ByVal emailCopia As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(4) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdEscalamientoPorAlerta", SqlDbType.Int)
      arParms(1).Value = idEscalamientoPorAlerta
      arParms(2) = New SqlParameter("@IdAlertaConfiguracion", SqlDbType.Int)
      arParms(2).Value = idAlertaConfiguracion
      arParms(3) = New SqlParameter("@Orden", SqlDbType.Int)
      arParms(3).Value = orden
      arParms(4) = New SqlParameter("@EmailCopia", SqlDbType.VarChar)
      arParms(4).Value = emailCopia

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarConfiguracionEscalamiento", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' graba los datos del grupo contacto
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarConfiguracionGrupoContacto(ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal idEscalamientoPorAlerta As String, ByVal nombre As String, _
                                                          ByVal orden As String, ByVal mostrarCuandoEscalamientoAnteriorNoContesta As String, ByVal tieneDependenciaGrupoEscalamientoAnterior As String, _
                                                          ByVal idGrupoContactoEscalamientoAnterior As String, ByVal mostrarSiempre As String, ByVal idGrupoContactoPadre As String, ByVal notificarPorEmail As String, _
                                                          ByVal horaAtencionInicio As String, ByVal horaAtencionTermino As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(12) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdEscalamientoPorAlertaGrupoContacto", SqlDbType.Int)
      arParms(1).Value = idEscalamientoPorAlertaGrupoContacto
      arParms(2) = New SqlParameter("@IdEscalamientoPorAlerta", SqlDbType.Int)
      arParms(2).Value = idEscalamientoPorAlerta
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre
      arParms(4) = New SqlParameter("@Orden", SqlDbType.Int)
      arParms(4).Value = orden
      arParms(5) = New SqlParameter("@MostrarCuandoEscalamientoAnteriorNoContesta", SqlDbType.Int)
      arParms(5).Value = mostrarCuandoEscalamientoAnteriorNoContesta
      arParms(6) = New SqlParameter("@TieneDependenciaGrupoEscalamientoAnterior", SqlDbType.Int)
      arParms(6).Value = tieneDependenciaGrupoEscalamientoAnterior
      arParms(7) = New SqlParameter("@IdGrupoContactoEscalamientoAnterior", SqlDbType.Int)
      arParms(7).Value = idGrupoContactoEscalamientoAnterior
      arParms(8) = New SqlParameter("@MostrarSiempre", SqlDbType.Int)
      arParms(8).Value = mostrarSiempre
      arParms(9) = New SqlParameter("@IdGrupoContactoPadre", SqlDbType.Int)
      arParms(9).Value = idGrupoContactoPadre
      arParms(10) = New SqlParameter("@NotificarPorEmail", SqlDbType.Int)
      arParms(10).Value = notificarPorEmail
      arParms(11) = New SqlParameter("@HoraAtencionInicio", SqlDbType.VarChar)
      arParms(11).Value = horaAtencionInicio
      arParms(12) = New SqlParameter("@HoraAtencionTermino", SqlDbType.VarChar)
      arParms(12).Value = horaAtencionTermino

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarConfiguracionGrupoContacto", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' graba los datos del contacto
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarConfiguracionContacto(ByVal idEscalamientoPorAlertaContacto As String, ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal idUsuario As String, _
                                                     ByVal cargo As String, ByVal orden As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdEscalamientoPorAlertaContacto", SqlDbType.Int)
      arParms(1).Value = idEscalamientoPorAlertaContacto
      arParms(2) = New SqlParameter("@IdEscalamientoPorAlertaGrupoContacto", SqlDbType.Int)
      arParms(2).Value = idEscalamientoPorAlertaGrupoContacto
      arParms(3) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(3).Value = idUsuario
      arParms(4) = New SqlParameter("@Cargo", SqlDbType.VarChar)
      arParms(4).Value = cargo
      arParms(5) = New SqlParameter("@Orden", SqlDbType.Int)
      arParms(5).Value = orden

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarConfiguracionContacto", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' graba los datos del script
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarConfiguracionScript(ByVal idScript As String, ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal nombre As String, ByVal descripcion As String, _
                                                   ByVal activo As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdScript", SqlDbType.Int)
      arParms(1).Value = idScript
      arParms(2) = New SqlParameter("@IdEscalamientoPorAlertaGrupoContacto", SqlDbType.Int)
      arParms(2).Value = idEscalamientoPorAlertaGrupoContacto
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre
      arParms(4) = New SqlParameter("@Descripcion", SqlDbType.VarChar)
      arParms(4).Value = descripcion
      arParms(5) = New SqlParameter("@Activo", SqlDbType.Int)
      arParms(5).Value = activo

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarConfiguracionScript", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' elimina los script que no estan referenciados
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarConfiguracionScriptNoReferenciados(ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal listado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdEscalamientoPorAlertaGrupoContacto", SqlDbType.Int)
      arParms(1).Value = idEscalamientoPorAlertaGrupoContacto
      arParms(2) = New SqlParameter("@Listado", SqlDbType.VarChar)
      arParms(2).Value = listado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarConfiguracionScriptNoReferenciados", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' elimina los contactos que no estan referenciados
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarConfiguracionContactosNoReferenciados(ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal listado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdEscalamientoPorAlertaGrupoContacto", SqlDbType.Int)
      arParms(1).Value = idEscalamientoPorAlertaGrupoContacto
      arParms(2) = New SqlParameter("@Listado", SqlDbType.VarChar)
      arParms(2).Value = listado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarConfiguracionContactosNoReferenciados", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' elimina los grupos que no estan referenciados
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarConfiguracionGruposNoReferenciados(ByVal idEscalamientoPorAlerta As String, ByVal listado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdEscalamientoPorAlerta", SqlDbType.Int)
      arParms(1).Value = idEscalamientoPorAlerta
      arParms(2) = New SqlParameter("@Listado", SqlDbType.VarChar)
      arParms(2).Value = listado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarConfiguracionGruposNoReferenciados", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' elimina los escalamientos que no estan referenciados
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarConfiguracionEscalamientosNoReferenciados(ByVal idAlertaConfiguracion As String, ByVal listado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaConfiguracion", SqlDbType.Int)
      arParms(1).Value = idAlertaConfiguracion
      arParms(2) = New SqlParameter("@Listado", SqlDbType.VarChar)
      arParms(2).Value = listado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarConfiguracionEscalamientosNoReferenciados", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' asocia el usuario al local
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AsociarLocalUsuario(ByVal idUsuario As String, ByVal localDestinoCodigo As String, ByVal localDestinoNombre As String, ByVal llavePerfil As String, _
                                             ByVal idFormato As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@LocalDestinoCodigo", SqlDbType.VarChar)
      arParms(2).Value = localDestinoCodigo
      arParms(3) = New SqlParameter("@LocalDestinoNombre", SqlDbType.VarChar)
      arParms(3).Value = localDestinoNombre
      arParms(4) = New SqlParameter("@LlavePerfil", SqlDbType.VarChar)
      arParms(4).Value = llavePerfil
      arParms(5) = New SqlParameter("@IdFormato", SqlDbType.VarChar)
      arParms(5).Value = idFormato

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_AsociarLocalUsuario", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' asocia el usuario al centro distribucion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function AsociarCentroDistribucionUsuario(ByVal idUsuario As String, ByVal localOrigenCodigo As String, ByVal localOrigenNombre As String, ByVal llavePerfil As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(4) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@LocalOrigenCodigo", SqlDbType.VarChar)
      arParms(2).Value = localOrigenCodigo
      arParms(3) = New SqlParameter("@LocalOrigenNombre", SqlDbType.VarChar)
      arParms(3).Value = localOrigenNombre
      arParms(4) = New SqlParameter("@LlavePerfil", SqlDbType.VarChar)
      arParms(4).Value = llavePerfil

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_AsociarCentroDistribucionUsuario", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' obtiene listado de transportistas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerTransportistas(ByVal rutTransportista As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@RutTransportista", SqlDbType.VarChar)
      arParms(0).Value = rutTransportista

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerTransportistas", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba la asociacion de grupo escalamiento anterior y padre
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ActualizarAsociacionGrupo(ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal idGrupoContactoEscalamientoAnterior As String, ByVal idGrupoContactoPadre As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdEscalamientoPorAlertaGrupoContacto", SqlDbType.Int)
      arParms(1).Value = idEscalamientoPorAlertaGrupoContacto
      arParms(2) = New SqlParameter("@IdGrupoContactoEscalamientoAnterior", SqlDbType.Int)
      arParms(2).Value = idGrupoContactoEscalamientoAnterior
      arParms(3) = New SqlParameter("@IdGrupoContactoPadre", SqlDbType.Int)
      arParms(3).Value = idGrupoContactoPadre

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ActualizarAsociacionGrupo", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' graba una alerta futura para MULTIPUNTO dependiendo de la explicacion registrada
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarNuevaAlertaFuturaMultipunto(ByVal idAlerta As String, ByVal explicacion As String, ByVal tipoExplicacion As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(1).Value = idAlerta
      arParms(2) = New SqlParameter("@Explicacion", SqlDbType.VarChar)
      arParms(2).Value = explicacion
      arParms(3) = New SqlParameter("@TipoExplicacion", SqlDbType.VarChar)
      arParms(3).Value = tipoExplicacion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarNuevaAlertaFuturaMultipunto", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' graba el historial de la llamada del callcenter
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarHistorialLlamadaCallCenter(ByVal idLlamada As String, ByVal idAlertaPadre As String, ByVal idAlertaHija As String, ByVal idEscalamientoPorAlertaGrupoContacto As String, _
                                                          ByVal nombreContacto As String, ByVal telefono As String, ByVal destinoLocal As String, ByVal destinoLocalCodigo As String, ByVal idUsuario As String, _
                                                          ByVal accion As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(10) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdLlamada", SqlDbType.VarChar)
      arParms(1).Value = idLlamada
      arParms(2) = New SqlParameter("@IdAlertaPadre", SqlDbType.Int)
      arParms(2).Value = idAlertaPadre
      arParms(3) = New SqlParameter("@IdAlertaHija", SqlDbType.Int)
      arParms(3).Value = idAlertaHija
      arParms(4) = New SqlParameter("@IdEscalamientoPorAlertaGrupoContacto", SqlDbType.Int)
      arParms(4).Value = idEscalamientoPorAlertaGrupoContacto
      arParms(5) = New SqlParameter("@NombreContacto", SqlDbType.VarChar)
      arParms(5).Value = nombreContacto
      arParms(6) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(6).Value = telefono
      arParms(7) = New SqlParameter("@DestinoLocal", SqlDbType.VarChar)
      arParms(7).Value = destinoLocal
      arParms(8) = New SqlParameter("@DestinoLocalCodigo", SqlDbType.VarChar)
      arParms(8).Value = destinoLocalCodigo
      arParms(9) = New SqlParameter("@Accion", SqlDbType.VarChar)
      arParms(9).Value = accion
      arParms(10) = New SqlParameter("@IdUsuarioCreacion", SqlDbType.Int)
      arParms(10).Value = idUsuario

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarHistorialLlamadaCallCenter", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' obtiene listado de alertas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function Buscador(ByVal nombre As String, ByVal nroTransporte As String, ByVal localDestino As String, ByVal prioridad As String, _
                                  ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal transportista As String, ByVal idFormato As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(7) {}
      arParms(0) = New SqlParameter("@NombreAlerta", SqlDbType.VarChar)
      arParms(0).Value = nombre
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte
      arParms(2) = New SqlParameter("@LocalDestino", SqlDbType.VarChar)
      arParms(2).Value = localDestino
      arParms(3) = New SqlParameter("@Prioridad", SqlDbType.VarChar)
      arParms(3).Value = prioridad
      arParms(4) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
      arParms(4).Value = fechaDesde
      arParms(5) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
      arParms(5).Value = fechaHasta
      arParms(6) = New SqlParameter("@NombreTransportista", SqlDbType.VarChar)
      arParms(6).Value = transportista
      arParms(7) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(7).Value = idFormato

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_Buscador", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene informacion de la ultima gestion de la alerta
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerUltimaGestion(ByVal idAlerta As String, ByVal idUsuario As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(0).Value = idAlerta
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario

      'ejecuta procedimiento almacenado
      valor = SqlHelper.ExecuteScalar(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerUltimaGestion", arParms)
    Catch ex As Exception
      valor = ""
    End Try
    'retorna valor
    Return valor
  End Function

  ''' <summary>
  ''' obtiene listado de auxiliares usados por teleoperador
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerAuxiliaresTeleoperador() As DataSet
    Dim ds As New DataSet

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerAuxiliaresTeleoperador")
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene datos de estado auxiliar segun su accion
  ''' </summary>
  ''' <param name="accion"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDatosAuxiliarPorAccion(ByVal accion As String, ByVal idUsuario As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Accion", SqlDbType.VarChar)
      arParms(0).Value = accion
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerDatosAuxiliarPorAccion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function
  ''' <summary>
  ''' graba la respuesta a una alerta enviada por correo
  ''' </summary>
  Public Shared Sub GrabarRespuesta(ByRef idAlerta As String, ByVal idRespuestaCategoria As String, ByVal observacion As String)
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@idAlerta", SqlDbType.Int)
      arParms(0).Value = idAlerta
      arParms(1) = New SqlParameter("@idRespuestaCategoria", SqlDbType.Int)
      arParms(1).Value = idRespuestaCategoria
      arParms(2) = New SqlParameter("@observacion", SqlDbType.VarChar)
      arParms(2).Value = observacion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarRespuestaEmail", arParms)

    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Alerta.GrabarRespuesta" & vbCrLf & Err.Description)
    End Try
  End Sub

  ''' <summary>
  ''' obtiene listado alertas rojas
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoRespuestaAlertaRoja(ByVal idUsuario As String, ByVal numeroTransporte As String, ByVal estado As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = numeroTransporte
      arParms(2) = New SqlParameter("@Estado", SqlDbType.VarChar)
      arParms(2).Value = estado

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerListadoRespuestaAlertaRoja", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle de la respuesta de la alerta roja
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalleRespuestaAlertaRoja(ByVal nroTransporte As String, ByVal idAlertaRojaEstadoActual As String, ByVal entidad As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(0).Value = nroTransporte
      arParms(1) = New SqlParameter("@IdAlertaRojaEstadoActual", SqlDbType.Int)
      arParms(1).Value = idAlertaRojaEstadoActual
      arParms(2) = New SqlParameter("@Entidad", SqlDbType.VarChar)
      arParms(2).Value = entidad

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerDetalleRespuestaAlertaRoja", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba el estado de la alerta roja
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarEstadoAlertaRoja(ByVal idAlertaRojaEstadoActual As String, ByVal idUsuario As String, ByVal nroTransporte As String, ByVal estado As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaRojaEstadoActual", SqlDbType.Int)
      arParms(1).Value = idAlertaRojaEstadoActual
      arParms(2) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(2).Value = idUsuario
      arParms(3) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(3).Value = nroTransporte
      arParms(4) = New SqlParameter("@Estado", SqlDbType.VarChar)
      arParms(4).Value = estado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarEstadoAlertaRoja", arParms)

      'retorna valor
      status = arParms(0).Value
      Return status
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

  ''' <summary>
  ''' graba el estado actual del transportista
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarEstadoTransportista(ByVal idAlertaRojaEstadoActual As String, ByVal rutTransportista As String, ByVal listadoIdEstados As String, ByVal observacion As String, _
                                                   ByVal montoUnoTransportista As String, ByVal montoDosTransportista As String, ByVal estadoAlertaRoja As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(7) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaRojaEstadoActual", SqlDbType.Int)
      arParms(1).Value = idAlertaRojaEstadoActual
      arParms(2) = New SqlParameter("@RutTransportista", SqlDbType.VarChar)
      arParms(2).Value = rutTransportista
      arParms(3) = New SqlParameter("@ListadoIdEstados", SqlDbType.VarChar)
      arParms(3).Value = listadoIdEstados
      arParms(4) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      arParms(4).Value = observacion
      arParms(5) = New SqlParameter("@MontoUnoTransportista", SqlDbType.Int)
      arParms(5).Value = montoUnoTransportista
      arParms(6) = New SqlParameter("@MontoDosTransportista", SqlDbType.Int)
      arParms(6).Value = montoDosTransportista
      arParms(7) = New SqlParameter("@EstadoAlertaRoja", SqlDbType.VarChar)
      arParms(7).Value = estadoAlertaRoja

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarEstadoTransportista", arParms)

      'retorna valor
      status = arParms(0).Value
      Return status
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

  ''' <summary>
  ''' obtiene el listado de alertas rojas agrupados por numero de viaje
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerAlertasRojasPorTransportista(ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal gestionadas As Boolean) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(2) {}
    arParms(0) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
    arParms(0).Value = fechaDesde
    arParms(1) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
    arParms(1).Value = fechaHasta
    arParms(2) = New SqlParameter("@Gestionadas", SqlDbType.Bit)
    arParms(2).Value = gestionadas

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerAlertasRojasPorTransportista", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene el listado de alertas rojas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalleAlertasRojasPorTransportista(ByVal rutTransportista As String, ByVal gestionadas As Boolean) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@rutTransportista", SqlDbType.VarChar)
      arParms(0).Value = rutTransportista
      arParms(1) = New SqlParameter("@gestionadas", SqlDbType.Bit)
      arParms(1).Value = gestionadas

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerDetalleAlertasRojasPorTransportista", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try

    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba los datos de la alerta que se envia por SMS
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarHistorialAlertaSMS(ByVal descripcionAlerta As String, ByVal nroTransporte As String, ByVal localDestino As String, ByVal telefono As String, ByVal enviadoA As String, _
                                                  ByVal cargo As String, ByVal grupoAlerta As String, ByVal ocurrencia As String, ByVal smsFolio As String, ByVal smsCodigo As String, ByVal smsMensaje As String, _
                                                  ByVal enviadoDesde As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(12) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@DescripcionAlerta", SqlDbType.VarChar)
      arParms(1).Value = descripcionAlerta
      arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(2).Value = nroTransporte
      arParms(3) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      arParms(3).Value = localDestino
      arParms(4) = New SqlParameter("@Telefono", SqlDbType.VarChar)
      arParms(4).Value = telefono
      arParms(5) = New SqlParameter("@EnviadoA", SqlDbType.VarChar)
      arParms(5).Value = enviadoA
      arParms(6) = New SqlParameter("@Cargo", SqlDbType.VarChar)
      arParms(6).Value = cargo
      arParms(7) = New SqlParameter("@GrupoAlerta", SqlDbType.VarChar)
      arParms(7).Value = grupoAlerta
      arParms(8) = New SqlParameter("@Ocurrencia", SqlDbType.Int)
      arParms(8).Value = ocurrencia
      arParms(9) = New SqlParameter("@SMSFolio", SqlDbType.VarChar)
      arParms(9).Value = smsFolio
      arParms(10) = New SqlParameter("@SMSCodigo", SqlDbType.VarChar)
      arParms(10).Value = smsCodigo
      arParms(11) = New SqlParameter("@SMSMensaje", SqlDbType.VarChar)
      arParms(11).Value = smsMensaje
      arParms(12) = New SqlParameter("@EnviadoDesde", SqlDbType.VarChar)
      arParms(12).Value = enviadoDesde

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarHistorialAlertaSMS", arParms)

      'retorna valor
      status = arParms(0).Value
      Return status
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

  ''' <summary>
  ''' obtiene el listado de alertas rojas que pueden ser cerradas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerAlertasRojasPorCerrar(ByVal fechaDesde As String, ByVal fechaHasta As String, ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Dim arParms() As SqlParameter = New SqlParameter(2) {}
    arParms(0) = New SqlParameter("@FechaDesde", SqlDbType.VarChar)
    arParms(0).Value = fechaDesde
    arParms(1) = New SqlParameter("@FechaHasta", SqlDbType.VarChar)
    arParms(1).Value = fechaHasta
    arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.VarChar)
    arParms(2).Value = nroTransporte

    Try
      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerAlertasRojasPorCerrar", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de definicion de alertas
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoDefinicionAlertas(ByVal idUsuario As String, ByVal filtroNombre As String, ByVal gestionarPorCemtra As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(1).Value = filtroNombre
      arParms(2) = New SqlParameter("@GestionarPorCemtra", SqlDbType.Int)
      arParms(2).Value = gestionarPorCemtra

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerListadoDefinicionAlertas", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene detalle de la definicion de alerta consultada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalleDefincionAlerta(ByVal idAlertaDefinicion As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@IdAlertaDefinicion", SqlDbType.Int)
      arParms(0).Value = idAlertaDefinicion

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerDetalleDefincionAlerta", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba una definicion de alerta
  ''' </summary>
  Public Shared Function GrabarDatosDefinicionAlerta(ByRef idAlertaDefinicion As String, ByVal nombre As String, ByVal formatos As String, ByVal gestionarPorCemtra As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@idAlertaDefinicionOutput", SqlDbType.Int)
      arParms(1).Direction = ParameterDirection.Output
      arParms(2) = New SqlParameter("@IdAlertaDefinicion", SqlDbType.Int)
      arParms(2).Value = idAlertaDefinicion
      arParms(3) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(3).Value = nombre
      arParms(4) = New SqlParameter("@Formatos", SqlDbType.VarChar)
      arParms(4).Value = formatos
      arParms(5) = New SqlParameter("@GestionarPorCemtra", SqlDbType.Int)
      arParms(5).Value = gestionarPorCemtra

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarDatosDefinicionAlerta", arParms)
      valor = arParms(0).Value
      idAlertaDefinicion = arParms(1).Value
      If valor = "error" Then
        Throw New System.Exception("Error al ejecutar Alerta.GrabarDatosDefinicionAlerta" & vbCrLf & Err.Description)
      Else
        Return valor
      End If
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Alerta.GrabarDatosDefinicionAlerta" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' elimina los contactos que no estan referenciados
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarFormatoDefinicionAlertaNoReferenciados(ByVal idAlertaDefinicion As String, ByVal listado As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaDefinicion", SqlDbType.Int)
      arParms(1).Value = idAlertaDefinicion
      arParms(2) = New SqlParameter("@Listado", SqlDbType.VarChar)
      arParms(2).Value = listado

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarFormatoDefinicionAlertaNoReferenciados", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' elimina la defincion de alerta
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarDatosDefinicionAlerta(ByVal idAlertaDefinicion As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaDefinicion", SqlDbType.Int)
      arParms(1).Value = idAlertaDefinicion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarDatosDefinicionAlerta", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Alerta.EliminarDatosDefinicionAlerta" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene listado de cargo por centro distribucion y perfil seleccionado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerCargoPorCentroDistribucionyPerfil(ByVal cdOrigenCodigo As String, ByVal llavePerfil As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@CDOrigenCodigo", SqlDbType.VarChar)
      arParms(0).Value = cdOrigenCodigo
      arParms(1) = New SqlParameter("@LlavePerfil", SqlDbType.VarChar)
      arParms(1).Value = llavePerfil

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerCargoPorCentroDistribucionyPerfil", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de cargo por local y perfil seleccionado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerCargoPorLocalyPerfil(ByVal localDestinoCodigo As String, ByVal llavePerfil As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@LocalDestinoCodigo", SqlDbType.VarChar)
      arParms(0).Value = localDestinoCodigo
      arParms(1) = New SqlParameter("@LlavePerfil", SqlDbType.VarChar)
      arParms(1).Value = llavePerfil

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerCargoPorLocalyPerfil", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' graba los datos de la alerta que se envia por SMS
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarHistorialAlertaEmail(ByVal descripcionAlerta As String, ByVal nroTransporte As String, ByVal localDestino As String, ByVal email As String, ByVal enviadoA As String, _
                                                    ByVal cargo As String, ByVal grupoAlerta As String, ByVal ocurrencia As String, ByVal emailMensaje As String, ByVal enviadoDesde As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(10) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@DescripcionAlerta", SqlDbType.VarChar)
      arParms(1).Value = descripcionAlerta
      arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(2).Value = nroTransporte
      arParms(3) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      arParms(3).Value = localDestino
      arParms(4) = New SqlParameter("@Email", SqlDbType.VarChar)
      arParms(4).Value = email
      arParms(5) = New SqlParameter("@EnviadoA", SqlDbType.VarChar)
      arParms(5).Value = enviadoA
      arParms(6) = New SqlParameter("@Cargo", SqlDbType.VarChar)
      arParms(6).Value = cargo
      arParms(7) = New SqlParameter("@GrupoAlerta", SqlDbType.VarChar)
      arParms(7).Value = grupoAlerta
      arParms(8) = New SqlParameter("@Ocurrencia", SqlDbType.Int)
      arParms(8).Value = ocurrencia
      arParms(9) = New SqlParameter("@EmailMensaje", SqlDbType.Text)
      arParms(9).Value = emailMensaje
      arParms(10) = New SqlParameter("@EnviadoDesde", SqlDbType.VarChar)
      arParms(10).Value = enviadoDesde

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarHistorialAlertaEmail", arParms)

      'retorna valor
      status = arParms(0).Value
      Return status
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

  ''' <summary>
  ''' desactiva las alertas que estan en cola de atencion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function DesactivarAlertasEnCola(ByVal idUsuario As String, ByVal nroTransporte As String, ByVal localDestino As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuarioDesactivacion", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(2).Value = nroTransporte
      arParms(3) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      arParms(3).Value = localDestino

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_DesactivarAlertasEnCola", arParms)

      'retorna valor
      status = arParms(0).Value
      Return status
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try

  End Function

  ''' <summary>
  ''' obtiene resumen del viaje consultado
  ''' </summary>
  ''' <returns></returns>̱
  ''' <remarks></remarks>̵
  Public Shared Function ResumenViaje(ByVal nroTransporte As String, ByVal localDestino As String) As DataSet
    Dim ds As New DataSet

    Try
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(0).Value = nroTransporte
      arParms(1) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      arParms(1).Value = localDestino

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ResumenViaje", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' elimina la ultima gestion del usuario cuando esta en estado auxiliar
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EliminarUltimoHistorialGestionPorUsuario(ByVal idUsuario As String) As String
    Try
      Dim valor As String
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarUltimoHistorialGestionPorUsuario", arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Alerta.EliminarDatosDefinicionAlerta" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' graba la respuesta de la alerta roja
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarRespuestaAlertaRoja(ByRef idUsuario As String, ByVal nroTransporte As String, ByVal idAlerta As String, ByVal observacion As String, ByVal rechazo As Boolean) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@idUsuario", SqlDbType.Int)
      arParms(1).Value = idUsuario
      arParms(2) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(2).Value = nroTransporte
      arParms(3) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(3).Value = idAlerta
      arParms(4) = New SqlParameter("@Observacion", SqlDbType.VarChar)
      arParms(4).Value = observacion
      arParms(5) = New SqlParameter("@Rechazo", SqlDbType.Bit)
      arParms(5).Value = rechazo

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarRespuestaAlertaRoja", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = -200
    End Try

    Return status
  End Function

  ''' <summary>
  ''' verifica si el viaje tiene certificaciones
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function TieneCertificacion(ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(0).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_TieneCertificacion", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds

  End Function

  ''' <summary>
  ''' obtiene motivos de la recepcion rechazada 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerMotivoRecepcionRechazada(ByVal nroViaje As String) As DataSet
    Dim ds As New DataSet

    Try
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@NroViaje", SqlDbType.Int)
      arParms(0).Value = nroViaje

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerMotivoRecepcionRechazada", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds

  End Function

  ''' <summary>
  ''' graba la alerta en la cola de atencion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function GrabarAlertaEnColaAtencion(ByVal oAlertaEnColaAtencion As Alerta.AlertaEnColaAtencion) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(11) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(1).Value = oAlertaEnColaAtencion.IdAlerta
      arParms(2) = New SqlParameter("@IdAlertaHija", SqlDbType.Int)
      arParms(2).Value = oAlertaEnColaAtencion.IdAlertaHija
      arParms(3) = New SqlParameter("@NroTransporte", SqlDbType.BigInt)
      arParms(3).Value = oAlertaEnColaAtencion.NroTransporte
      arParms(4) = New SqlParameter("@IdEmbarque", SqlDbType.BigInt)
      arParms(4).Value = oAlertaEnColaAtencion.IdEmbarque
      arParms(5) = New SqlParameter("@NombreAlerta", SqlDbType.VarChar)
      arParms(5).Value = oAlertaEnColaAtencion.NombreAlerta
      arParms(6) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      arParms(6).Value = oAlertaEnColaAtencion.LocalDestino
      arParms(7) = New SqlParameter("@IdFormato", SqlDbType.Int)
      arParms(7).Value = oAlertaEnColaAtencion.IdFormato
      arParms(8) = New SqlParameter("@Prioridad", SqlDbType.VarChar)
      arParms(8).Value = oAlertaEnColaAtencion.Prioridad
      arParms(9) = New SqlParameter("@OrdenPrioridad", SqlDbType.Int)
      arParms(9).Value = oAlertaEnColaAtencion.OrdenPrioridad
      arParms(10) = New SqlParameter("@FechaHoraCreacion", SqlDbType.DateTime)
      arParms(10).Value = oAlertaEnColaAtencion.FechaHoraCreacion
      arParms(11) = New SqlParameter("@FechaHoraCreacionDMA", SqlDbType.VarChar)
      arParms(11).Value = oAlertaEnColaAtencion.FechaHoraCreacionDMA

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_GrabarAlertaEnColaAtencion", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' desactiva las alertas en base de datos y las elimina de la cola de atencion
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function DesactivarAlertaEnColaAtencionLocalesRMyOtros(ByVal idAlerta As String, ByVal nombreAlerta As String, ByVal nroTransporte As String, ByVal idEmbarque As String, _
                                                                       ByVal localDestino As String) As String
    Dim valor As String

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlerta", SqlDbType.Int)
      arParms(1).Value = idAlerta
      arParms(2) = New SqlParameter("@NombreAlerta", SqlDbType.VarChar)
      arParms(2).Value = nombreAlerta
      arParms(3) = New SqlParameter("@NroTransporte", SqlDbType.BigInt)
      arParms(3).Value = nroTransporte
      arParms(4) = New SqlParameter("@IdEmbarque", SqlDbType.BigInt)
      arParms(4).Value = idEmbarque
      arParms(5) = New SqlParameter("@LocalDestino", SqlDbType.Int)
      arParms(5).Value = localDestino

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Job_DesactivarAlertaEnColaAtencionLocalesRMyOtros", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' actualiza la asignacion de la alerta en cola
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ActualizarAsignacionAlertaEnCola(ByVal idAlertaPadre As String, ByVal idAlertaHija As String, ByVal idUsuarioAsignada As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaPadre", SqlDbType.Int)
      arParms(1).Value = idAlertaPadre
      arParms(2) = New SqlParameter("@IdAlertaHija", SqlDbType.Int)
      arParms(2).Value = idAlertaHija
      arParms(3) = New SqlParameter("@IdUsuarioAsignada", SqlDbType.Int)
      arParms(3).Value = idUsuarioAsignada

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ActualizarAsignacionAlertaEnCola", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' elimina la alerta gestionada de la cola de atencion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function EliminarAlertaEnColaAtencion(ByVal idAlertaPadre As String, ByVal idAlertaHija As String) As Integer
    Dim status As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdAlertaPadre", SqlDbType.Int)
      arParms(1).Value = idAlertaPadre
      arParms(2) = New SqlParameter("@IdAlertaHija", SqlDbType.Int)
      arParms(2).Value = idAlertaHija

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_EliminarAlertaEnColaAtencion", arParms)

      'retorna valor
      status = arParms(0).Value
    Catch ex As Exception
      status = Sistema.eCodigoSql.Error
    End Try

    Return status
  End Function

  ''' <summary>
  ''' obtiene listado de coordinador linea transporte
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerCoordinadorLineaTransporte(ByVal nombreTransportista As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@NombreTransportista", SqlDbType.VarChar)
      arParms(0).Value = nombreTransportista

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Alerta_ObtenerCoordinadorLineaTransporte", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

End Class

﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Discrepancia
#Region "Constantes & Enums"
  Public Const NombreEntidad As String = "Discrepancia"
  Public Const KEY_SESION_DISCREPANCIA_MENSAJE As String = "DiscrepanciaMensaje"
  Public Const NOMBRE_CARPETA_PDF_CARGA As String = "SMU_PDF_Carga"
  Public Const NOMBRE_CARPETA_PDF_RECEPCION As String = "SMU_PDF_Recepcion"
  Public Const RUTA_CARPETA_SERVIDOR_PDF_CARGAR As String = Constantes.logPathDirectorioSitio & "\" & NombreEntidad & "\" & NOMBRE_CARPETA_PDF_CARGA
  Public Const RUTA_CARPETA_SERVIDOR_PDF_RECEPCION As String = Constantes.logPathDirectorioSitio & "\" & NombreEntidad & "\" & NOMBRE_CARPETA_PDF_RECEPCION
  Public Const RUTA_CARPETA_FTP As String = "/ftp_transporte_smu/"
  Public Const RUTA_CARPETA_FTP_PDF_CARGA As String = RUTA_CARPETA_FTP & "Carga/PDF/"
  Public Const RUTA_CARPETA_FTP_PDF_RECEPCION As String = RUTA_CARPETA_FTP & "Recepcion/PDF/"
#End Region

#Region "Metodos Compartidos"
  ''' <summary>
  ''' obtiene listado de discrepancias
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function Buscador(ByVal idUsuario As String, ByVal codigoLocal As String, ByVal seccion As String, ByVal codigoCentroDistribucion As String, ByVal dm As String, ByVal fechaContableDesde As String, _
                                  ByVal fechaContableHasta As String, ByVal resolucionFinal As String, ByVal nroTransporte As String, ByVal carga As String, ByVal diferenciaDias As String, ByVal soloFocoTablet As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(11) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(1).Value = codigoLocal
      arParms(2) = New SqlParameter("@Seccion", SqlDbType.VarChar)
      arParms(2).Value = seccion
      arParms(3) = New SqlParameter("@CodigoCentroDistribucion", SqlDbType.Int)
      arParms(3).Value = codigoCentroDistribucion
      arParms(4) = New SqlParameter("@DM", SqlDbType.BigInt)
      arParms(4).Value = dm
      arParms(5) = New SqlParameter("@FechaContableDesde", SqlDbType.VarChar)
      arParms(5).Value = fechaContableDesde
      arParms(6) = New SqlParameter("@FechaContableHasta", SqlDbType.VarChar)
      arParms(6).Value = fechaContableHasta
      arParms(7) = New SqlParameter("@ResolucionFinal", SqlDbType.VarChar)
      arParms(7).Value = resolucionFinal
      arParms(8) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(8).Value = nroTransporte
      arParms(9) = New SqlParameter("@Carga", SqlDbType.VarChar)
      arParms(9).Value = carga
      arParms(10) = New SqlParameter("@DiferenciaDias", SqlDbType.Int)
      arParms(10).Value = diferenciaDias
      arParms(11) = New SqlParameter("@SoloFocoTablet", SqlDbType.Int)
      arParms(11).Value = soloFocoTablet

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Discrepancia_Buscador", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene el detalle de la discrepancia
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalle(ByVal nroTransporte As String, ByVal codigoLocal As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(0).Value = nroTransporte
      arParms(1) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(1).Value = codigoLocal

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Discrepancia_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de discrepancias
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerListadoExcel(ByRef procedimientoAlmacenado As String, ByVal idUsuario As String, ByVal codigoLocal As String, ByVal seccion As String, ByVal codigoCentroDistribucion As String, ByVal dm As String, _
                                             ByVal fechaContableDesde As String, ByVal fechaContableHasta As String, ByVal resolucionFinal As String, ByVal nroTransporte As String, ByVal carga As String, ByVal diferenciaDias As String, _
                                             ByVal soloFocoTablet As String) As DataSet
    Dim ds As New DataSet
    Try
      procedimientoAlmacenado = "spu_Discrepancia_ObtenerListadoExcel"

      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(11) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(1).Value = codigoLocal
      arParms(2) = New SqlParameter("@Seccion", SqlDbType.VarChar)
      arParms(2).Value = seccion
      arParms(3) = New SqlParameter("@CodigoCentroDistribucion", SqlDbType.Int)
      arParms(3).Value = codigoCentroDistribucion
      arParms(4) = New SqlParameter("@DM", SqlDbType.Int)
      arParms(4).Value = dm
      arParms(5) = New SqlParameter("@FechaContableDesde", SqlDbType.VarChar)
      arParms(5).Value = fechaContableDesde
      arParms(6) = New SqlParameter("@FechaContableHasta", SqlDbType.VarChar)
      arParms(6).Value = fechaContableHasta
      arParms(7) = New SqlParameter("@ResolucionFinal", SqlDbType.VarChar)
      arParms(7).Value = resolucionFinal
      arParms(8) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(8).Value = nroTransporte
      arParms(9) = New SqlParameter("@Carga", SqlDbType.VarChar)
      arParms(9).Value = carga
      arParms(10) = New SqlParameter("@DiferenciaDias", SqlDbType.Int)
      arParms(10).Value = diferenciaDias
      arParms(11) = New SqlParameter("@SoloFocoTablet", SqlDbType.Int)
      arParms(11).Value = soloFocoTablet

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, procedimientoAlmacenado, arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene historial de alertas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerHistorialAlertas(ByRef procedimientoAlmacenado As String, ByVal nroTransporte As String, ByVal codigoLocal As String) As DataSet
    Dim ds As New DataSet
    Try
      procedimientoAlmacenado = "spu_Discrepancia_ObtenerHistorialAlertas"

      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(0).Value = nroTransporte
      arParms(1) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(1).Value = codigoLocal

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, procedimientoAlmacenado, arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de discrepancias sin alertas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function BuscadorSinAlertas(ByVal idUsuario As String, ByVal codigoLocal As String, ByVal seccion As String, ByVal codigoCentroDistribucion As String, ByVal dm As String, _
                                            ByVal fechaContableDesde As String, ByVal fechaContableHasta As String, ByVal resolucionFinal As String, ByVal nroTransporte As String, _
                                            ByVal diferenciaDias As String, ByVal soloFocoTablet As String) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(10) {}
      arParms(0) = New SqlParameter("@IdUsuario", SqlDbType.Int)
      arParms(0).Value = idUsuario
      arParms(1) = New SqlParameter("@CodigoLocal", SqlDbType.Int)
      arParms(1).Value = codigoLocal
      arParms(2) = New SqlParameter("@Seccion", SqlDbType.VarChar)
      arParms(2).Value = seccion
      arParms(3) = New SqlParameter("@CodigoCentroDistribucion", SqlDbType.Int)
      arParms(3).Value = codigoCentroDistribucion
      arParms(4) = New SqlParameter("@DM", SqlDbType.BigInt)
      arParms(4).Value = dm
      arParms(5) = New SqlParameter("@FechaContableDesde", SqlDbType.VarChar)
      arParms(5).Value = fechaContableDesde
      arParms(6) = New SqlParameter("@FechaContableHasta", SqlDbType.VarChar)
      arParms(6).Value = fechaContableHasta
      arParms(7) = New SqlParameter("@ResolucionFinal", SqlDbType.VarChar)
      arParms(7).Value = resolucionFinal
      arParms(8) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(8).Value = nroTransporte
      arParms(9) = New SqlParameter("@DiferenciaDias", SqlDbType.Int)
      arParms(9).Value = diferenciaDias
      arParms(10) = New SqlParameter("@SoloFocoTablet", SqlDbType.Int)
      arParms(10).Value = soloFocoTablet

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Discrepancia_BuscadorSinAlertas", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

End Class

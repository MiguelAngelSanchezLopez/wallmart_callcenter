﻿Public Class AlertaMasterClass

#Region "Properties"
  Public Property Id As Integer
  Public Property NroTransporte As Long
  Public Property IdEmbarque As Long
  Public Property DescripcionAlerta As String
  Public Property LocalDestino As Integer
  Public Property OrigenCodigo As Integer
  Public Property PatenteTracto As String
  Public Property PatenteTrailer As String
  Public Property RutTransportista As String
  Public Property NombreTransportista As String
  Public Property RutConductor As String
  Public Property NombreConductor As String
  Public Property FechaUltimaTransmision As DateTime
  Public Property FechaInicioAlerta As DateTime
  Public Property LatTrailer As String
  Public Property LonTrailer As String
  Public Property LatTracto As String
  Public Property LonTracto As String
  Public Property TipoAlerta As String
  Public Property Permiso As String
  Public Property Criticidad As String
  Public Property TipoAlertaDescripcion As String
  Public Property AlertaMapa As String
  Public Property NombreZona As String
  Public Property Velocidad As String
  Public Property EstadoGPSTracto As String
  Public Property EstadoGPSRampla As String
  Public Property EstadoGPS As String
  Public Property TipoPunto As String
  Public Property Temp1 As String
  Public Property Temp2 As String
  Public Property DistanciaTT As String
  Public Property TransportistaTrailer As String
  Public Property CantidadSatelites As Integer
  Public Property FechaHoraCreacion As DateTime
  Public Property FechaHoraActualizacion As DateTime
  Public Property EnvioCorreo As Boolean
  Public Property FueraDeHorario As Boolean
  Public Property Activo As Boolean
  Public Property ConLog As Boolean
  Public Property Ocurrencia As Integer
  Public Property GrupoAlerta As String

#End Region

#Region "Constructor"

  Public Sub New()
    MyBase.New()
  End Sub

#End Region

End Class

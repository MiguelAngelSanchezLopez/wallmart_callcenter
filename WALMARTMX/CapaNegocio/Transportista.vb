﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Transportista

#Region "Constantes & Enums"

#End Region

#Region "Campos"

#End Region

#Region "Properties"

#End Region

#Region "Constructores"
  Public Sub New()
    MyBase.New()
  End Sub

#End Region

#Region "Metodos Publicos"
  ' ''' <summary>
  ' ''' guarda las propiedades de la instancia como un nuevo registro y obtiene su Id
  ' ''' </summary>
  ' ''' <remarks></remarks>
  'Public Sub GuardarNuevo()

  'End Sub

  ' ''' <summary>
  ' ''' guarda (update) todas las propiedades de la instancia en su registro  (usando el Id)
  ' ''' </summary>
  ' ''' <remarks></remarks>
  'Public Sub Actualizar()

  'End Sub

  ' ''' <summary>
  ' ''' elimina fisicamente (delete) el registro que corresponde a la instancia
  ' ''' </summary>
  ' ''' <remarks></remarks>
  'Public Sub Eliminar()

  'End Sub

#End Region

#Region "Metodos Privados"

#End Region

#Region "Metodos compartidos"

  ''' <summary>
  ''' obtiene listado de usuarios
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoPorDocumento(ByVal idDocumento As Integer) As DataSet
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@idDocTransportista", SqlDbType.VarChar)
      arParms(0).Value = idDocumento

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerListadoXDocumento", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene listado de archivos por entidad
  ''' </summary>
  Public Shared Function ObtenerListadoDocumento(ByVal filtroNombre As String)
    Dim ds As New DataSet
    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@Nombre", SqlDbType.VarChar)
      arParms(0).Value = filtroNombre

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerDocumento", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene informacion del documento segun id
  ''' </summary>
  ''' <param name="id"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalleDocumentoPorId(ByVal id As String) As DataSet
    Dim ds As New DataSet

    'arreglo de parametros para el procedimiento almacenado
    Dim arParms() As SqlParameter = New SqlParameter(0) {}
    arParms(0) = New SqlParameter("@id", SqlDbType.Int)
    arParms(0).Value = id

    'ejecuta procedimiento almacenado
    ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerDetalleDocumentoPorId", arParms)

    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene los documentos asignados al transportista
  ''' </summary>
  ''' <param name="idTransportista"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDocumentosAsignados(ByVal idTransportista As Integer, ByVal filtroNombre As String) As DataSet

    Dim arParms() As SqlParameter = New SqlParameter(1) {}
    arParms(0) = New SqlParameter("@idTransportista", SqlDbType.Int)
    arParms(0).Value = idTransportista
    arParms(1) = New SqlParameter("@nombre", SqlDbType.Int)
    arParms(1).Value = filtroNombre

    Try
      Dim ds As DataSet
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerDocumentosAsignados", arParms)
      Return ds
    Catch
      Return New DataSet
    End Try
  End Function

  ''' <summary>
  ''' obtiene detalle de las alertas rojas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerDetalleAlertaRojaTransportista(ByVal rut As String, ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Rut", SqlDbType.VarChar)
      arParms(0).Value = rut
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerDetalleAlertaRojaTransportista", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  Public Shared Function GrabarDocumentoTransportista(ByVal nombre As String, ByVal Descripcion As String) As String
    Dim valor As String

    Try
      Dim storedProcedure As String = "spu_Transportista_GrabarDocumento"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@nombre", SqlDbType.VarChar)
      arParms(1).Value = nombre
      arParms(2) = New SqlParameter("@Descripcion", SqlDbType.VarChar)
      arParms(2).Value = Descripcion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor

  End Function

  Public Shared Function ModificarDocumentoTransportista(ByVal id As Integer, ByVal nombre As String, ByVal Descripcion As String) As String
    Dim valor As String

    Try
      Dim storedProcedure As String = "spu_Transportista_GrabarDocumento"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@idDocumentoTransportista", SqlDbType.VarChar)
      arParms(1).Value = id
      arParms(2) = New SqlParameter("@nombre", SqlDbType.VarChar)
      arParms(2).Value = nombre
      arParms(3) = New SqlParameter("@Descripcion", SqlDbType.VarChar)
      arParms(3).Value = Descripcion

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor

  End Function

  Public Shared Function AsignarDocumentoATransportista(ByVal idDocumento As String, ByVal listadoIdTransportista As String) As String
    Dim valor As String

    Try
      Dim storedProcedure As String = "spu_Transportista_AsignarDocumento"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(3) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdDocTransportista", SqlDbType.VarChar)
      arParms(1).Value = idDocumento
      arParms(2) = New SqlParameter("@ListadoIdTransportista", SqlDbType.VarChar)
      arParms(2).Value = listadoIdTransportista

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor

  End Function

  ''' <summary>
  ''' elimina una pagina
  ''' </summary>
  ''' <param name="idDocumento"></param>
  ''' <returns></returns>
  Public Shared Function EliminarDocumento(ByVal idDocumento As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Transportista_EliminarDocumento"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@Id", SqlDbType.Int)
      arParms(1).Value = idDocumento

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Transportista.EliminarDocumento" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene listado alertas rojas
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerAlertasRojasTransportista(ByVal rut As String, ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Rut", SqlDbType.VarChar)
      arParms(0).Value = rut
      arParms(1) = New SqlParameter("@NroTransporte", SqlDbType.Int)
      arParms(1).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerAlertasRojasTransportista", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' Graba los datos del transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GrabarDatos(idFicha As Integer, VolumenFlota As Integer, NombreDueño As String, Telefono1Dueño As String, Telefono2Dueño As String, EmailDueño As String, NombreGerenteOperacion As String, _
                                     TelefonoGerenteOperacion As String, EmailGerenteOperacion As String, Telefono1Escalamiento As String, Telefono2Escalamiento As String, Telefono3Escalamiento As String, _
                                     Telefono4Escalamiento As String, idTransportista As Integer) As String
    Dim valor As String


    Try
      Dim storedProcedure As String = "spu_Transportista_GrabarDatosFicha"

      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(14) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@idFicha", SqlDbType.Int)
      arParms(1).Value = idFicha
      arParms(2) = New SqlParameter("@VolumenFlota", SqlDbType.Int)
      arParms(2).Value = VolumenFlota
      arParms(3) = New SqlParameter("@NombreDueño", SqlDbType.VarChar)
      arParms(3).Value = NombreDueño
      arParms(4) = New SqlParameter("@Telefono1Dueño", SqlDbType.VarChar)
      arParms(4).Value = Telefono1Dueño
      arParms(5) = New SqlParameter("@Telefono2Dueño", SqlDbType.VarChar)
      arParms(5).Value = Telefono2Dueño
      arParms(6) = New SqlParameter("@EmailDueño", SqlDbType.VarChar)
      arParms(6).Value = EmailDueño
      arParms(7) = New SqlParameter("@NombreGerenteOperacion", SqlDbType.VarChar)
      arParms(7).Value = NombreGerenteOperacion
      arParms(8) = New SqlParameter("@TelefonoGerenteOperacion", SqlDbType.VarChar)
      arParms(8).Value = TelefonoGerenteOperacion
      arParms(9) = New SqlParameter("@EmailGerenteOperacion", SqlDbType.VarChar)
      arParms(9).Value = EmailGerenteOperacion
      arParms(10) = New SqlParameter("@Telefono1Escalamiento", SqlDbType.VarChar)
      arParms(10).Value = Telefono1Escalamiento
      arParms(11) = New SqlParameter("@Telefono2Escalamiento", SqlDbType.VarChar)
      arParms(11).Value = Telefono2Escalamiento
      arParms(12) = New SqlParameter("@Telefono3Escalamiento", SqlDbType.VarChar)
      arParms(12).Value = Telefono3Escalamiento
      arParms(13) = New SqlParameter("@Telefono4Escalamiento", SqlDbType.VarChar)
      arParms(13).Value = Telefono4Escalamiento
      arParms(14) = New SqlParameter("@idTransportista", SqlDbType.Int)
      arParms(14).Value = idTransportista

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor

  End Function

  ''' <summary>
  ''' obtiene el detalle del transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetalle(ByVal id As Integer) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@id", SqlDbType.VarChar)
      arParms(0).Value = id

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerDetalle", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene el detalle del transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerListadoPatentes(ByVal idTransportista As String, ByVal placaPatente As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@IdTransportista", SqlDbType.VarChar)
      arParms(0).Value = idTransportista
      arParms(1) = New SqlParameter("@PlacaPatente", SqlDbType.VarChar)
      arParms(1).Value = placaPatente

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerListadoPatentes", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' obtiene el detalle del transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerDetallePatente(ByVal id As Integer) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@id", SqlDbType.VarChar)
      arParms(0).Value = id

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerDetallePatente", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' Graba los datos del transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GrabarDatosPatente(id As Integer, PlacaPatente As String, Marca As String, Tipo As String, idTransportista As Integer) As String
    Dim valor As String

    Try
      Dim storedProcedure As String = "spu_Transportista_GrabarDatosPatente"

      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(5) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@id", SqlDbType.Int)
      arParms(1).Value = id
      arParms(2) = New SqlParameter("@PlacaPatente", SqlDbType.VarChar)
      arParms(2).Value = PlacaPatente
      arParms(3) = New SqlParameter("@Marca", SqlDbType.VarChar)
      arParms(3).Value = Marca
      arParms(4) = New SqlParameter("@Tipo", SqlDbType.VarChar)
      arParms(4).Value = Tipo
      arParms(5) = New SqlParameter("@idTransportista", SqlDbType.Int)
      arParms(5).Value = idTransportista

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    Return valor

  End Function

  ''' <summary>
  ''' elimina una pagina
  ''' </summary>
  ''' <param name="id"></param>
  ''' <returns></returns>
  Public Shared Function EliminarPatente(ByVal id As String) As String
    Try
      Dim valor As String
      Dim storedProcedure As String = "spu_Transportista_EliminarPatente"
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.VarChar, 50)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@Id", SqlDbType.Int)
      arParms(1).Value = id

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, storedProcedure, arParms)
      valor = arParms(0).Value
      Return valor
    Catch ex As Exception
      Throw New System.Exception("Error al ejecutar Transportista.EliminarPatente" & vbCrLf & Err.Description)
    End Try
  End Function

  ''' <summary>
  ''' obtiene si la patente esta bloqueada
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function ObtenerPatenteBloqueada(ByVal patente As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(0) {}
      arParms(0) = New SqlParameter("@patente", SqlDbType.VarChar)
      arParms(0).Value = patente

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_ObtenerPatenteBloqueada", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  ''' <summary>
  ''' valida que la patente pertenezca al transportista
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function PertenecePatente(ByVal idTransportista As String, ByVal patente As String) As Integer
    Dim valor As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(1).Value = idTransportista
      arParms(2) = New SqlParameter("@Patente", SqlDbType.VarChar)
      arParms(2).Value = patente

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_PertenecePatente", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    'retorna valor
    Return valor
  End Function

  ''' <summary>
  ''' valida que la patente emita reportabilidad
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function EmiteReportabilidad(ByVal idTransportista As String, ByVal patente As String) As Integer
    Dim valor As Integer

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(2) {}
      arParms(0) = New SqlParameter("@Status", SqlDbType.Int)
      arParms(0).Direction = ParameterDirection.Output
      arParms(1) = New SqlParameter("@IdTransportista", SqlDbType.Int)
      arParms(1).Value = idTransportista
      arParms(2) = New SqlParameter("@Patente", SqlDbType.VarChar)
      arParms(2).Value = patente

      'ejecuta procedimiento almacenado
      SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, "spu_Transportista_EmiteReportabilidad", arParms)
      valor = arParms(0).Value
    Catch ex As Exception
      valor = Sistema.eCodigoSql.Error
    End Try

    'retorna valor
    Return valor
  End Function

  Public Shared Function ObtenerDetalleViaje(ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@nroTransporte", SqlDbType.Int)
      arParms(0).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "Track_GetInformeViajesPDF", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  Public Shared Function ObtenerDetalleTrayecto(ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@nroTransporte", SqlDbType.VarChar)
      arParms(0).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "Track_GetDetalleTrayectoPDF", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

  Public Shared Function ObtenerAlertasInformeViaje(ByVal nroTransporte As String) As DataSet
    Dim ds As New DataSet

    Try
      'arreglo de parametros para el procedimiento almacenado
      Dim arParms() As SqlParameter = New SqlParameter(1) {}
      arParms(0) = New SqlParameter("@nroTransporte", SqlDbType.VarChar)
      arParms(0).Value = nroTransporte

      'ejecuta procedimiento almacenado
      ds = SqlHelper.ExecuteDataset(Conexion.StringConexion, CommandType.StoredProcedure, "Track_GetAlertasInformeViajePDF", arParms)
    Catch ex As Exception
      ds = Nothing
    End Try
    'retorna valor
    Return ds
  End Function

#End Region

End Class
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class Configuracion

#Region "Compartidas"
  ''' <summary>
  ''' obtiene el valor de una clave
  ''' </summary>
  ''' <param name="clave"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function Leer(ByVal clave As String) As String
    Dim valor As String
    Dim sp As String = "spu_Configuracion_Leer"
    Dim parms() As SqlParameter = New SqlParameter(0) {}

    Try
      parms(0) = New SqlParameter("@Clave", SqlDbType.VarChar)
      parms(0).Value = clave
      valor = SqlHelper.ExecuteScalar(Conexion.StringConexion, CommandType.StoredProcedure, sp, parms)
      Return valor
    Catch ex As Exception
      Return String.Empty
    End Try
  End Function

  ''' <summary>
  ''' ingresa o actualiza el valor de una clave
  ''' </summary>
  ''' <param name="clave"></param>
  ''' <param name="valor"></param>
  ''' <remarks></remarks>
  Public Shared Sub Escribir(ByVal clave As String, ByVal valor As String)
    Dim sp As String = "spu_Configuracion_Escribir"
    Dim parms() As SqlParameter = New SqlParameter(1) {}
    Dim actualizado As Integer

    Try
      parms(0) = New SqlParameter("@Clave", SqlDbType.VarChar)
      parms(1) = New SqlParameter("@Valor", SqlDbType.VarChar)
      parms(0).Value = clave
      parms(1).Value = valor
      actualizado = SqlHelper.ExecuteNonQuery(Conexion.StringConexion, CommandType.StoredProcedure, sp, parms)
      If actualizado = 0 Then Throw New Exception("La clave no existe")
    Catch ex As Exception
      Throw New Exception("Ha ocurrido un error al Escribir el valor de la configuracion" & vbCrLf & ex.Message)
    End Try
  End Sub

#End Region


End Class

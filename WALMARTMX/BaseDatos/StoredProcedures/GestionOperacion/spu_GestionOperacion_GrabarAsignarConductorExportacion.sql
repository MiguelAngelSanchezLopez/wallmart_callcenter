﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_GrabarAsignarConductorExportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_GrabarAsignarConductorExportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_GrabarAsignarConductorExportacion
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_GrabarAsignarConductorExportacion'
GO
CREATE Procedure [dbo].[spu_GestionOperacion_GrabarAsignarConductorExportacion]  
/******************************************************************************  
**    Descripcion  : graba las asignacion del conductor en exportación  
**    Por          : VSR, 26/05/2016  
*******************************************************************************/  
@Status AS VARCHAR(50) OUTPUT,  
@IdUsuario AS INT,  
@IdExportacion AS INT,  
@NombreConductor AS VARCHAR(255),  
@RutConductor AS VARCHAR(255),  
@Telefono AS VARCHAR(255),  
@PatenteTracto AS VARCHAR(255),  
@PatenteTrailer AS VARCHAR(255),  
@TipoCamion AS VARCHAR(255),  
@FechaEntrada AS DATETIME,  
@OrigenDatoTabla AS VARCHAR(255),  
@OrigenDatoIdRegistro AS VARCHAR(255)  
AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  BEGIN TRANSACTION  
    DECLARE @totalRegistros INT  
    SET @totalRegistros = (SELECT COUNT(IdExportacion) FROM OS_Exportacion_AsignacionConductor WHERE IdExportacion = @IdExportacion )  
    IF (@totalRegistros IS NULL) SET @totalRegistros = 0  
  
    IF (@totalRegistros = 0) BEGIN  
      INSERT INTO OS_Exportacion_AsignacionConductor  
      (  
       IdExportacion,  
       NombreConductor,  
       RutConductor,  
       Telefono,  
       PatenteTracto,  
       PatenteTrailer,  
       TipoCamion,  
       FechaEntrada,  
       IdUsuarioCreacion,  
       FechaCreacion,  
       OrigenDatoTabla,  
       OrigenDatoIdRegistro,  
       UltReporteTracto,  
       UltReporteTrailer  
      )  
      Select  
       @IdExportacion,  
       @NombreConductor,  
       @RutConductor,  
       @Telefono,  
       @PatenteTracto,  
       @PatenteTrailer,  
       @TipoCamion,  
       @FechaEntrada,  
       @IdUsuario,  
       dbo.fnu_GETDATE(),  
       @OrigenDatoTabla,  
       @OrigenDatoIdRegistro,  
       (Select top 1 Fecha From Track_LastPositions where Patente = @PatenteTracto),  
       (Select top 1 Fecha From Track_LastPositions where Patente = @PatenteTrailer)  
  
      SET @Status = 'ingresado'  
      IF @@ERROR<>0 BEGIN  
        ROLLBACK  
        SET @Status = 'error'  
        RETURN  
      END  
    END ELSE BEGIN  
      UPDATE OS_Exportacion_AsignacionConductor  
      SET  
       NombreConductor = @NombreConductor,  
       RutConductor = @RutConductor,  
       Telefono = @Telefono,  
       PatenteTracto = @PatenteTracto,  
       PatenteTrailer = @PatenteTrailer,  
       TipoCamion = @TipoCamion,  
       FechaEntrada = @FechaEntrada,  
       IdUsuarioModificacion = @IdUsuario,  
       FechaModificacion = dbo.fnu_GETDATE(),  
       OrigenDatoTabla = @OrigenDatoTabla,  
       OrigenDatoIdRegistro = @OrigenDatoIdRegistro,  
       UltReporteTracto = (Select top 1 Fecha From Track_LastPositions where Patente = @PatenteTracto),  
       UltReporteTrailer = (Select top 1 Fecha From Track_LastPositions where Patente = @PatenteTrailer)  
      WHERE  
       IdExportacion = @IdExportacion  
  
      SET @Status = 'modificado'  
      IF @@ERROR<>0 BEGIN  
        ROLLBACK  
        SET @Status = 'error'  
        RETURN  
      END  
    END  
  COMMIT  
  
END  
       
	    

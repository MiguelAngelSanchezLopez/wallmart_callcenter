﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ObtenerListadoConductoresAsignacionManual')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ObtenerListadoConductoresAsignacionManual'
  DROP  Procedure  dbo.spu_GestionOperacion_ObtenerListadoConductoresAsignacionManual
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ObtenerListadoConductoresAsignacionManual'
GO
CREATE Procedure dbo.spu_GestionOperacion_ObtenerListadoConductoresAsignacionManual
/******************************************************************************
**    Descripcion  : obtiene listado de conductores para asignacion manual
**    Por          : VSR, 22/04/2016
*******************************************************************************/
@RutConductorAsignado AS VARCHAR(255),
@Nombre AS VARCHAR(255),
@Rut AS VARCHAR(255),
@Patente AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON
  
  ------------------------------------------
  -- obtiene el conductor asociado
  SELECT
    IdConductor = U.Id,
    Indice = 0,
	  NombreConductor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
	  RutConductor = ISNULL(U.Rut,'') + ISNULL('-' + U.DV,''),
	  PatenteTracto = ISNULL(UDE.PatenteTracto,''),
	  PatenteTrailer = ISNULL(UDE.PatenteTrailer,''),
	  Telefono = ISNULL(U.Telefono,''),
    FechaEntrada = dbo.fnu_ConvertirDatetimeToDDMMYYYY(dbo.fnu_GETDATE(),'FECHA_COMPLETA'),
    FechaEntradaOrden = dbo.fnu_GETDATE(),
    OrigenDatoTabla = 'Usuario',
    OrigenDatoIdRegistro = U.Id,
    FechaLicenciaConducir = ISNULL(UDE.FechaLicConducir,''),
    FechaRevisionTecnica = ISNULL(UDE.FechaRevTecnica,''),
    ReportabilidadPatenteTracto = ISNULL(TLPTracto.Estado,'NoIntegrada'),
    ReportabilidadPatenteTractoFecha = ISNULL(TLPTracto.Fecha,''),
    ReportabilidadPatenteTrailer = ISNULL(TLPTrailer.Estado,'NoIntegrada'),
    ReportabilidadPatenteTrailerFecha = ISNULL(TLPTrailer.Fecha,'')
  FROM
	  Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    INNER JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
    LEFT JOIN vwu_ReportabilidadPatente AS TLPTracto ON (UDE.PatenteTracto = TLPTracto.Patente)
    LEFT JOIN vwu_ReportabilidadPatente AS TLPTrailer ON (UDE.PatenteTrailer = TLPTrailer.Patente)
  WHERE
      ( P.Llave = 'CON' )
  AND ( ISNULL(U.Rut,'') + ISNULL('-' + U.DV,'') = @RutConductorAsignado )

  ------------------------------------------
  -- obtiene el resto de los conductores sin el asignado
  UNION
  SELECT
    IdConductor = U.Id,
    Indice = ROW_NUMBER() OVER( ORDER BY ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,'') ),
	  NombreConductor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
	  RutConductor = ISNULL(U.Rut,'') + ISNULL('-' + U.DV,''),
	  PatenteTracto = ISNULL(UDE.PatenteTracto,''),
	  PatenteTrailer = ISNULL(UDE.PatenteTrailer,''),
	  Telefono = ISNULL(U.Telefono,''),
    FechaEntrada = dbo.fnu_ConvertirDatetimeToDDMMYYYY(dbo.fnu_GETDATE(),'FECHA_COMPLETA'),
    FechaEntradaOrden = dbo.fnu_GETDATE(),
    OrigenDatoTabla = 'Usuario',
    OrigenDatoIdRegistro = U.Id,
    FechaLicenciaConducir = ISNULL(UDE.FechaLicConducir,''),
    FechaRevisionTecnica = ISNULL(UDE.FechaRevTecnica,''),
    ReportabilidadPatenteTracto = ISNULL(TLPTracto.Estado,'NoIntegrada'),
    ReportabilidadPatenteTractoFecha = ISNULL(TLPTracto.Fecha,''),
    ReportabilidadPatenteTrailer = ISNULL(TLPTrailer.Estado,'NoIntegrada'),
    ReportabilidadPatenteTrailerFecha = ISNULL(TLPTrailer.Fecha,'')
  FROM
	  Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    INNER JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
    LEFT JOIN vwu_ReportabilidadPatente AS TLPTracto ON (UDE.PatenteTracto = TLPTracto.Patente)
    LEFT JOIN vwu_ReportabilidadPatente AS TLPTrailer ON (UDE.PatenteTrailer = TLPTrailer.Patente)
  WHERE
      ( P.Llave = 'CON' )
  AND ( ISNULL(U.Rut,'') + ISNULL('-' + U.DV,'') <> @RutConductorAsignado )
  AND ( @Nombre = '-1' OR RTRIM(LTRIM(ISNULL(U.Nombre,''))) +' '+ RTRIM(LTRIM(ISNULL(U.Paterno,''))) +' '+ RTRIM(LTRIM(ISNULL(U.Materno,''))) LIKE '%'+ REPLACE(@Nombre,' ','%') +'%' )
  AND ( @Rut = '-1' OR ISNULL(U.Rut,'') + ISNULL(U.DV,'') = REPLACE(REPLACE(@Rut,'.',''),'-','') )
  AND ( @Patente = '-1' OR ISNULL(UDE.PatenteTracto,'') = @Patente OR ISNULL(UDE.PatenteTrailer,'') = @Patente )
  /*
  AND (
        ISNULL(U.Rut,'') + ISNULL('-' + U.DV,'') NOT IN (
          ------------------------------------------
          -- saca los conductores que estan asignado en otros viajes o que esten en ruta
          SELECT
            IAC.RutConductor
          FROM
            OS_Importacion AS I
            INNER JOIN OS_Importacion_AsignacionConductor AS IAC ON (I.Id = IAC.IdImportacion)
          WHERE
            ( ISNULL(IAC.EstadoViaje,'') IN ('','ASIGNADO','RUTA') )

          ------------------------------------------
          -- saca los conductores que estan asignado en otros viajes o que esten en ruta en exportacion
          UNION ALL
          SELECT
            EAC.RutConductor
          FROM
            OS_Exportacion AS E
            INNER JOIN OS_Exportacion_AsignacionConductor AS EAC ON (E.Id = EAC.IdExportacion)
          WHERE
            ( ISNULL(EAC.EstadoViaje,'') IN ('','ASIGNADO','RUTA') )
        )  
      )
  */
  ORDER BY
    Indice

END
	    

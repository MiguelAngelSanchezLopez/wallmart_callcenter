﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ObtenerListadoAsignacionViajesExportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ObtenerListadoAsignacionViajesExportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_ObtenerListadoAsignacionViajesExportacion
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ObtenerListadoAsignacionViajesExportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_ObtenerListadoAsignacionViajesExportacion
/******************************************************************************
**    Descripcion  : obtiene listado asignación viajes exportación
**    Por          : VSR, 26/05/2016
*******************************************************************************/
@IdUsuario AS INT,
@FechaPresentacion AS VARCHAR(255),
@HoraPresentacion AS VARCHAR(255),
@TipoContenedor AS VARCHAR(255),
@NombreNaviera AS VARCHAR(255),
@TipoCarga AS VARCHAR(255),
@Dimension AS VARCHAR(255),
@PuertoDescarga AS VARCHAR(255),
@PuertoEmbarque AS VARCHAR(255),
@NumeroOrdenServicio AS VARCHAR(255),
@FechaCreacion AS VARCHAR(255),
@ClasificacionConductor AS VARCHAR(255),
@NumeroGuiaDespacho AS INT,
@SoloGuiaDespacho AS INT,
@Contenedor AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @FechaInicio DATETIME, @FechaTermino DATETIME, @FechaCreacionInicio DATETIME, @FechaCreacionTermino DATETIME
  IF (@FechaPresentacion <> '-1') BEGIN
    IF (@HoraPresentacion = '-1') BEGIN
      SET @FechaInicio = @FechaPresentacion + ' 00:00'
      SET @FechaTermino = @FechaPresentacion + ' 23:59'
    END ELSE BEGIN
      SET @FechaInicio = @FechaPresentacion + ' ' + @HoraPresentacion
      SET @FechaTermino = @FechaPresentacion + ' ' + @HoraPresentacion + ':59.997'
    END
  END

  IF (@FechaCreacion <> '-1') BEGIN
    SET @FechaCreacionInicio = @FechaCreacion + ' 00:00'
    SET @FechaCreacionTermino = @FechaCreacion + ' 23:59:59.997'
  END

  DECLARE @TablaViaje AS TABLE(
    IdExportacion INT,
    NumeroOrdenServicio INT,
    NumeroLineaMO INT,
    VersionMO INT,
    NombreCliente VARCHAR(255),
    LugarPresentacion VARCHAR(255),
    SegundoDestino VARCHAR(255),
	  FechaHoraPresentacion VARCHAR(255),
	  MedidaContenedor INT,
    DescripcionTipoCarga VARCHAR(255),
    PesoNetoCarga FLOAT,
    NumeroContenedor VARCHAR(255),
    DepositoRetiro VARCHAR(255),
    Reserva VARCHAR(255),
    NombreNave VARCHAR(255),
    FechaHoraProgramacion VARCHAR(255),
    Observaciones VARCHAR(255),
    NombreConductor VARCHAR(255),
    PatenteTracto VARCHAR(255),
    PatenteTrailer VARCHAR(255),
    TelefonoConductor VARCHAR(255),
    EstadoViaje VARCHAR(255),
    FechaHoraPresentacionOrden DATETIME,
    FechaLlegada VARCHAR(255),
    ObservacionLlegada VARCHAR(8000),
    CantidadPallet INT,
    PesoReal FLOAT,
    OrdenCompra BIGINT,
    NumeroGuiaDespacho VARCHAR(255),
    FechaSalida VARCHAR(255),
    NumeroSello VARCHAR(255),
    NumeroContenedorCargaSuelta VARCHAR(255),
    ReportabilidadPatenteTracto VARCHAR(255),
    ReportabilidadPatenteTractoFecha VARCHAR(255),
    ReportabilidadPatenteTrailer VARCHAR(255),
    ReportabilidadPatenteTrailerFecha VARCHAR(255),
    FechaAsignacionConductor DATETIME,
    EstadoRuta VARCHAR(50)
  )

  DECLARE @TablaSalida AS TABLE(
    IdExportacion INT,
    NumeroOrdenServicio INT,
    NumeroLineaMO INT,
    VersionMO INT,
    NombreCliente VARCHAR(255),
    LugarPresentacion VARCHAR(255),
    SegundoDestino VARCHAR(255),
	  FechaHoraPresentacion VARCHAR(255),
	  MedidaContenedor INT,
    DescripcionTipoCarga VARCHAR(255),
    PesoNetoCarga FLOAT,
    NumeroContenedor VARCHAR(255),
    DepositoRetiro VARCHAR(255),
    Reserva VARCHAR(255),
    NombreNave VARCHAR(255),
    FechaHoraProgramacion VARCHAR(255),
    Observaciones VARCHAR(255),
    NombreConductor VARCHAR(255),
    PatenteTracto VARCHAR(255),
    PatenteTrailer VARCHAR(255),
    TelefonoConductor VARCHAR(255),
    EstadoViaje VARCHAR(255),
    FechaHoraPresentacionOrden DATETIME,
    FechaLlegada VARCHAR(255),
    ObservacionLlegada VARCHAR(8000),
    CantidadPallet INT,
    PesoReal FLOAT,
    OrdenCompra BIGINT,
    NumeroGuiaDespacho VARCHAR(255),
    FechaSalida VARCHAR(255),
    NumeroSello VARCHAR(255),
    NumeroContenedorCargaSuelta VARCHAR(255),
    ReportabilidadPatenteTracto VARCHAR(255),
    ReportabilidadPatenteTractoFecha VARCHAR(255),
    ReportabilidadPatenteTrailer VARCHAR(255),
    ReportabilidadPatenteTrailerFecha VARCHAR(255),
    FechaAsignacionConductor DATETIME,
    EstadoRuta VARCHAR(50)
  )
  
  -- obtiene los viajes que cumplen con los criterios de busqueda ------------------------
  INSERT INTO @TablaViaje(
    IdExportacion,
    NumeroOrdenServicio,
    NumeroLineaMO,
    VersionMO,
    NombreCliente,
    LugarPresentacion,
    SegundoDestino,
	  FechaHoraPresentacion,
	  MedidaContenedor,
    DescripcionTipoCarga,
    PesoNetoCarga,
    NumeroContenedor,
    DepositoRetiro,
    Reserva,
    NombreNave,
    FechaHoraProgramacion,
    Observaciones,
    NombreConductor,
    PatenteTracto,
    PatenteTrailer,
    TelefonoConductor,
    EstadoViaje,
    FechaHoraPresentacionOrden,
    FechaLlegada,
    ObservacionLlegada,
    CantidadPallet,
    PesoReal,
    OrdenCompra,
    NumeroGuiaDespacho,
    FechaSalida,
    NumeroSello,
    NumeroContenedorCargaSuelta,
    ReportabilidadPatenteTracto,
    ReportabilidadPatenteTractoFecha,
    ReportabilidadPatenteTrailer,
    ReportabilidadPatenteTrailerFecha,
    FechaAsignacionConductor,
    EstadoRuta
  )
  SELECT
    T.*
  FROM (
        SELECT
          IdExportacion = E.Id,
	        E.NumeroOrdenServicio,
          E.NumeroLineaMO,
          E.VersionMO,
          NombreCliente = ISNULL(E.NombreCliente,''),
          LugarPresentacion = CASE WHEN OROrigen.DescripcionDestino IS NULL THEN 'SIN RUTA ASIGNADA'
                              ELSE OROrigen.DescripcionDestino + CASE WHEN OROrigen.CodDestino = '' THEN '' ELSE ' - ' + OROrigen.CodDestino END
                              END,
          SegundoDestino = CASE WHEN ORDestino.DescripcionDestino IS NULL THEN 'SIN RUTA ASIGNADA'
                           ELSE ORDestino.DescripcionDestino + CASE WHEN ORDestino.CodDestino = '' THEN '' ELSE ' - ' + ORDestino.CodDestino END
                           END,
	        FechaHoraPresentacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(E.FechaHoraStacking, 'FECHA_COMPLETA'),
	        MedidaContenedor = E.MedidaContenedor,
          DescripcionTipoCarga = ISNULL(E.DescripcionTipoCarga,''),
          PesoNetoCarga = ROUND(ISNULL(E.PesoNetoCarga,0),2),
          NumeroContenedor = ISNULL(E.MarcaContenedor,'') + ISNULL(E.NumeroContenedor,'') + ISNULL(E.DigitoContenedor,''),
          DepositoRetiro = 'FALTA DATO',
          Reserva = ISNULL(E.Reserva,''),
          NombreNave = ISNULL(E.NombreNave,''),
          FechaHoraProgramacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(E.FechaCreacion, 'FECHA_COMPLETA'),
          Observaciones = ISNULL(E.Observaciones,''),
          NombreConductor = ISNULL(EAC.NombreConductor,''),
          PatenteTracto = ISNULL(EAC.PatenteTracto,''),
          PatenteTrailer = ISNULL(EAC.PatenteTrailer,''),
          TelefonoConductor = ISNULL(EAC.Telefono,''),
          EstadoViaje = CASE ISNULL(EAC.EstadoViaje,'')
                          WHEN 'ENLOCAL-R' THEN 'EN TIENDA'
                          WHEN 'ENLOCAL-P' THEN 'FINALIZADO'
                          ELSE UPPER(ISNULL(EAC.EstadoViaje,''))
                        END,
          FechaHoraPresentacionOrden = E.FechaHoraStacking,
          FechaLlegada = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(EDE.FechaHoraLlegada, 'FECHA_COMPLETA'),''),
          ObservacionLlegada = ISNULL(EDE.ObservacionLlegada,''),
          CantidadPallet = ISNULL(EDE.CantidadPallet,-1),
          PesoReal = ISNULL(EDE.PesoReal,-1),
          OrdenCompra = ISNULL(EDE.OrdenCompra,-1),
          NumeroGuiaDespacho = ISNULL(CONVERT(VARCHAR,GD.NumeroGuiaDespacho),'SIN ID EMBARQUE'),
          FechaSalida = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(EDE.FechaHoraSalida, 'FECHA_COMPLETA'),''),
          NumeroSello = ISNULL(EDE.NumeroSello,''),
          NumeroContenedorCargaSuelta = ISNULL(EDE.NumeroContenedor,''),
		      /*
          Modificación 2016-08-10. Se obtiene el reporte del momento en que se asignó el conductor.
          Modificación 2016-08-22. Se revierte modificación a su estado anterior.
          ReportabilidadPatenteTracto = Case	when EAC.UltReporteTracto is null then 'NoIntegrada'
												when EAC.UltReporteTracto < dateadd(hour, -12, dbo.fnu_GETDATE()) then 'Offline'
												Else 'Online'
											End,	 
          ReportabilidadPatenteTractoFecha = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(EAC.UltReporteTracto, 'FECHA_COMPLETA'), ''),
		      ReportabilidadPatenteTrailer = Case	when EAC.UltReporteTrailer is null then 'NoIntegrada'
												when EAC.UltReporteTrailer < dateadd(hour, -12, dbo.fnu_GETDATE()) then 'Offline'
												Else 'Online' 
											End,
          ReportabilidadPatenteTrailerFecha = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(EAC.UltReportetrailer, 'FECHA_COMPLETA'), ''),
          */
          ReportabilidadPatenteTracto = ISNULL(TLPTracto.Estado,'NoIntegrada'),
          ReportabilidadPatenteTractoFecha = ISNULL(TLPTracto.Fecha,''),
          ReportabilidadPatenteTrailer = ISNULL(TLPTrailer.Estado,'NoIntegrada'),
          ReportabilidadPatenteTrailerFecha = ISNULL(TLPTrailer.Fecha,''),
          FechaAsignacionConductor = ISNULL(E.FechaCreacion,'19000101'),
          EstadoRuta = ''
        FROM
	        OS_Exportacion AS E
          LEFT JOIN OS_Exportacion_AsignacionConductor AS EAC ON (E.Id = EAC.IdExportacion)
          LEFT JOIN OS_Exportacion_DatoExtra AS EDE ON (E.Id = EDE.IdExportacion)
          LEFT JOIN OS_GuiaDespacho AS GD ON (E.NumeroOrdenServicio = GD.NumeroOrdenServicio AND E.NumeroLineaMO = GD.NumeroLineaMO AND E.VersionMO = GD.VersionMO)
          LEFT JOIN vwu_ReportabilidadPatente AS TLPTracto ON (EAC.PatenteTracto = TLPTracto.Patente)
          LEFT JOIN vwu_ReportabilidadPatente AS TLPTrailer ON (EAC.PatenteTrailer = TLPTrailer.Patente)
          OUTER APPLY (
            SELECT TOP 1
              DescripcionOrigen = ISNULL(aux.DescripcionOrigen,''),
              DescripcionDestino = ISNULL(aux.DescripcionDestino,ISNULL(aux.DescripcionOrigen,'')),
              CodDestino = ISNULL(aux.CodDestino,'')
            FROM
              OS_Rutas AS aux
            WHERE
                aux.NumeroOrdenServicio = E.NumeroOrdenServicio
            AND aux.NumeroLineaMO = E.NumeroLineaMO
            AND aux.VersionMO = E.VersionMO
          ) AS OROrigen
          OUTER APPLY (
            SELECT TOP 1
              DescripcionDestino = ISNULL(aux.DescripcionDestino,ISNULL(aux.DescripcionOrigen,'')),
              CodDestino = ISNULL(aux.CodDestino,'')
            FROM
              OS_Rutas AS aux
            WHERE
                aux.NumeroOrdenServicio = E.NumeroOrdenServicio
            AND aux.NumeroLineaMO = E.NumeroLineaMO
            AND aux.VersionMO = E.VersionMO
            ORDER BY
              aux.Id DESC
          ) AS ORDestino
           OUTER APPLY (
				Select top 1 R.Id, R.CodOrigen, R.CodDestino FROM OS_Rutas R WHERE R.NumeroOrdenServicio = E.NumeroOrdenServicio AND R.NumeroLineaMO = E.NumeroLineaMO AND R.VersionMO = E.VersionMO
			) AS RF
        WHERE
            (@FechaPresentacion = '-1' OR E.FechaHoraStacking BETWEEN @FechaInicio AND @FechaTermino)
        AND (@TipoContenedor = '-1' OR E.MarcaContenedor = @TipoContenedor)
        AND (@NombreNaviera = '-1' OR E.NombreNaviera = @NombreNaviera)
        AND (@TipoCarga = '-1' OR E.DescripcionTipoCarga = @TipoCarga)
        AND (@Dimension = '-1' OR E.MedidaContenedor = @Dimension)
        AND (@PuertoDescarga = '-1' OR E.NombrePuertoDescarga = @PuertoDescarga)
        AND (@PuertoEmbarque = '-1' OR E.NombrePuertoEmbarque = @PuertoEmbarque)
        AND (@NumeroOrdenServicio = '-1' OR E.NumeroOrdenServicio = @NumeroOrdenServicio)
        AND (@FechaCreacion = '-1' OR E.FechaCreacion BETWEEN @FechaCreacionInicio AND @FechaCreacionTermino)
        AND (@ClasificacionConductor = '-1' OR E.NombreTerminalGestionServicio = @ClasificacionConductor)
        AND (@NumeroGuiaDespacho = -1 OR GD.NumeroGuiaDespacho = @NumeroGuiaDespacho)
        AND LEFT(CONVERT(VARCHAR, E.FechaHoraStacking, 108), 5) > '00:00'
        AND (@Contenedor = '-1' OR ISNULL(E.MarcaContenedor,'') + ISNULL(E.NumeroContenedor,'') + ISNULL(E.DigitoContenedor,'') LIKE '%'+ @Contenedor +'%')
        AND (E.EstadoLinea <> 'A')
  ) AS T
  WHERE
    (
         ( @SoloGuiaDespacho = 0 )
      OR ( @SoloGuiaDespacho = 1 AND T.NumeroGuiaDespacho <> 'SIN ID EMBARQUE' )
    )
  
  -- obtiene los viajes con la ultima version, y los que tienen guia de despacho -----------------
  INSERT INTO @TablaSalida(
    IdExportacion,
    NumeroOrdenServicio,
    NumeroLineaMO,
    VersionMO,
    NombreCliente,
    LugarPresentacion,
    SegundoDestino,
	  FechaHoraPresentacion,
	  MedidaContenedor,
    DescripcionTipoCarga,
    PesoNetoCarga,
    NumeroContenedor,
    DepositoRetiro,
    Reserva,
    NombreNave,
    FechaHoraProgramacion,
    Observaciones,
    NombreConductor,
    PatenteTracto,
    PatenteTrailer,
    TelefonoConductor,
    EstadoViaje,
    FechaHoraPresentacionOrden,
    FechaLlegada,
    ObservacionLlegada,
    CantidadPallet,
    PesoReal,
    OrdenCompra,
    NumeroGuiaDespacho,
    FechaSalida,
    NumeroSello,
    NumeroContenedorCargaSuelta,
    ReportabilidadPatenteTracto,
    ReportabilidadPatenteTractoFecha,
    ReportabilidadPatenteTrailer,
    ReportabilidadPatenteTrailerFecha,
    FechaAsignacionConductor,
    EstadoRuta
  )
  -- obtiene los viajes con la ultima version --------------------
  SELECT
    T.*
  FROM
    @TablaViaje AS T
    INNER JOIN (
      SELECT
        NumeroOrdenServicio,
        NumeroLineaMO,
        VersionMO = MAX(VersionMO)
      FROM
        @TablaViaje
      GROUP BY
        NumeroOrdenServicio,
        NumeroLineaMO
    ) AS aux ON (T.NumeroOrdenServicio = aux.NumeroOrdenServicio AND T.NumeroLineaMO = aux.NumeroLineaMO AND T.VersionMO = aux.VersionMO)

  -- obtiene los viajes que tienen guia de despacho independiente de la version --------------------
  UNION
  SELECT
    T.*
  FROM
    @TablaViaje AS T
  WHERE
    T.NumeroGuiaDespacho <> 'SIN ID EMBARQUE'

  -- elimina los viajes que para lineas repetidas de un mismo NumeroOrdenServicio solo se muestre el que tenga guia de despacho asociada ----------
  DELETE @TablaSalida
  FROM
    @TablaSalida AS TS
    INNER JOIN (
      SELECT
        aux.NumeroOrdenServicio,
        aux.NumeroLineaMO,
        aux.VersionMO
      FROM
        @TablaViaje AS aux
      WHERE
        aux.NumeroGuiaDespacho <> 'SIN ID EMBARQUE'
    ) AS T ON (TS.NumeroOrdenServicio = T.NumeroOrdenServicio AND TS.NumeroLineaMO = T.NumeroLineaMO)
  WHERE
    TS.VersionMO <> T.VersionMO


  ------------------------------------------------------------------
  -- TABLA 0: Listado datos
  ------------------------------------------------------------------
  SELECT
    T.*
  FROM
    @TablaSalida AS T
  ORDER BY
    T.FechaAsignacionConductor 

  ------------------------------------------------------------------
  -- TABLA 1: Listado datos para exportar
  ------------------------------------------------------------------
  SELECT
    [NUMERO ORDEN SERVICIO] = T.NumeroOrdenServicio,
    --[NUMERO LINEA] = T.NumeroLineaMO,
    --[VERSION] = T.VersionMO,
    [NUMERO GUIA DESPACHO] = T.NumeroGuiaDespacho,
    [NOMBRE CLIENTE] = T.NombreCliente,
    [LUGAR PRESENTACION] = T.LugarPresentacion,
    [SEGUNDO DESTINO] = T.SegundoDestino,
	  [FECHA PRESENTACION] = T.FechaHoraPresentacion,
	  [MEDIDA] = T.MedidaContenedor,
    --[TIPO] = T.DescripcionTipoCarga,
    [PESO] = T.PesoNetoCarga,
    [CONTENEDOR] = T.NumeroContenedor,
    [DEPOSITO RETIRO] = T.DepositoRetiro,
    [RESERVA] = T.Reserva,
    [NAVE] = T.NombreNave,
    [FECHA PROGRAMACION] = T.FechaHoraProgramacion,
    [OBSERVACIONES] = T.Observaciones,
    [NOMBRE OPERADOR] = T.NombreConductor,
    [PLACA TRACTO] = T.PatenteTracto,
    [PLACA REMOLQUE] = T.PatenteTrailer,
    [TELEFONO OPERADOR] = T.TelefonoConductor,
    [ESTADO VIAJE] = T.EstadoViaje,
    [FECHA LLEGADA] = T.FechaLlegada,
    [FECHA SALIDA] = T.FechaSalida,
    [OBSERVACION LLEGADA] = T.ObservacionLlegada,
    [CANTIDAD PALLET] = CASE WHEN T.CantidadPallet = -1 THEN '' ELSE CONVERT(VARCHAR,T.CantidadPallet) END,
    [PESO REAL] = CASE WHEN T.PesoReal = -1 THEN '' ELSE CONVERT(VARCHAR,T.PesoReal) END,
    [ORDEN COMPRA] = CASE WHEN T.OrdenCompra = -1 THEN '' ELSE '''' + CONVERT(VARCHAR,T.OrdenCompra) END,
    [NUMERO SELLO] = T.NumeroSello,
    [NUMERO CONTENEDOR CARGA SUELTA] = T.NumeroContenedorCargaSuelta
  FROM
    @TablaSalida AS T
  ORDER BY
    T.FechaAsignacionConductor 

END
	    

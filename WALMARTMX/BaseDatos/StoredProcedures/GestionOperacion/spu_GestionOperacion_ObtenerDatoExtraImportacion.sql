﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ObtenerDatoExtraImportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ObtenerDatoExtraImportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_ObtenerDatoExtraImportacion
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ObtenerDatoExtraImportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_ObtenerDatoExtraImportacion
/******************************************************************************
**    Descripcion  : obtiene dato extra de importacion
**    Por          : VSR, 16/06/2016
******************************************************************************/
@IdImportacion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
	  FechaLlegada = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(IDE.FechaHoraLlegada, 'SOLO_FECHA'),''),
	  HoraLlegada = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(IDE.FechaHoraLlegada, 'SOLO_HORA'),''),
    ObservacionLlegada = ISNULL(IDE.ObservacionLlegada,''),
    CantidadPallet = ISNULL(IDE.CantidadPallet, -1),
    PesoReal = ISNULL(IDE.PesoReal, -1),
    OrdenCompra = ISNULL(IDE.OrdenCompra, -1),
	  FechaSalida = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(IDE.FechaHoraSalida, 'SOLO_FECHA'),''),
	  HoraSalida = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(IDE.FechaHoraSalida, 'SOLO_HORA'),''),
    NumeroSello = ISNULL(IDE.NumeroSello,''),
    NumeroContenedor = ISNULL(IDE.NumeroContenedor,'')
  FROM
	  OS_Importacion_DatoExtra AS IDE
  WHERE
    IDE.IdImportacion = @IdImportacion

END
	    
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_DesasignarConductorExportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_DesasignarConductorExportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_DesasignarConductorExportacion
END

GO

PRINT  'Creating Procedure spu_GestionOperacion_DesasignarConductorExportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_DesasignarConductorExportacion
/******************************************************************************
**  Descripcion  : desasigna conductor en exportación
**  Fecha        : 26/05/2016
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdExportacion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM OS_Exportacion_AsignacionConductor WHERE IdExportacion = @IdExportacion

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO           
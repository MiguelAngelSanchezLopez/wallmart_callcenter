﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ObtenerHistorialAsignacionViajesImportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ObtenerHistorialAsignacionViajesImportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_ObtenerHistorialAsignacionViajesImportacion
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ObtenerHistorialAsignacionViajesImportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_ObtenerHistorialAsignacionViajesImportacion
/******************************************************************************
**    Descripcion  : obtiene historial asignación viajes importación
**    Por          : VSR, 16/06/2016
*******************************************************************************/
@NumeroOrdenServicio AS INT,
@NumeroLineaMO AS INT,
@VersionMO AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    IdImportacion = I.Id,
	  I.NumeroOrdenServicio,
    I.VersionMO,
    I.NumeroLineaMO,
    EstadoLinea = ISNULL(I.EstadoLinea,''),
    NombreCliente = ISNULL(I.NombreCliente,''),
    LugarPresentacion = CASE WHEN OROrigen.DescripcionDestino IS NULL THEN 'SIN RUTA ASIGNADA'
                        ELSE OROrigen.DescripcionDestino + CASE WHEN OROrigen.CodDestino = '' THEN '' ELSE ' - ' + OROrigen.CodDestino END
                        END,
    SegundoDestino = CASE WHEN ORDestino.DescripcionDestino IS NULL THEN 'SIN RUTA ASIGNADA'
                     ELSE ORDestino.DescripcionDestino + CASE WHEN ORDestino.CodDestino = '' THEN '' ELSE ' - ' + ORDestino.CodDestino END
                     END,
	  FechaHoraPresentacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(I.FechaHoraPresentacion, 'FECHA_COMPLETA'),
	  MedidaContenedor = I.MedidaContenedor,
    --DescripcionTipoCarga = ISNULL(I.DescripcionTipoCarga,''),
    I.PesoNetoCarga,
    NumeroContenedor = ISNULL(I.MarcaContenedor,'') + ISNULL(I.NumeroContenedor,'') + ISNULL(I.DigitoContenedor,''),
    NombreTerminalOrigen = OROrigen.DescripcionOrigen,
    NombreNave = ISNULL(I.NombreNave,''),
    FechaHoraProgramacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(I.FechaCreacion, 'FECHA_COMPLETA'),
    Observaciones = ISNULL(I.Observaciones,'')
  FROM
	  OS_ImportacionHistoria AS I
    OUTER APPLY (
      SELECT TOP 1
        DescripcionOrigen = ISNULL(aux.DescripcionOrigen,''),
        DescripcionDestino = ISNULL(aux.DescripcionDestino,ISNULL(aux.DescripcionOrigen,'')),
        CodDestino = ISNULL(aux.CodDestino,'')
      FROM
        OS_Rutas AS aux
      WHERE
          aux.NumeroOrdenServicio = I.NumeroOrdenServicio
      AND aux.NumeroLineaMO = I.NumeroLineaMO
      AND aux.VersionMO = I.VersionMO
    ) AS OROrigen
    OUTER APPLY (
      SELECT TOP 1
        DescripcionDestino = ISNULL(aux.DescripcionDestino,ISNULL(aux.DescripcionOrigen,'')),
        CodDestino = ISNULL(aux.CodDestino,'')
      FROM
        OS_Rutas AS aux
      WHERE
          aux.NumeroOrdenServicio = I.NumeroOrdenServicio
      AND aux.NumeroLineaMO = I.NumeroLineaMO
      AND aux.VersionMO = I.VersionMO
      ORDER BY
        aux.Id DESC
    ) AS ORDestino
  WHERE
      (I.NumeroOrdenServicio = @NumeroOrdenServicio)
  AND (I.NumeroLineaMO = @NumeroLineaMO)
  AND (I.VersionMO <> @VersionMO)
  ORDER BY
    I.VersionMO, I.NumeroLineaMO

END
	    

﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ObtenerDatoExtraExportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ObtenerDatoExtraExportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_ObtenerDatoExtraExportacion
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ObtenerDatoExtraExportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_ObtenerDatoExtraExportacion
/******************************************************************************
**    Descripcion  : obtiene dato extra de exportacion
**    Por          : VSR, 17/06/2016
******************************************************************************/
@IdExportacion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
	  FechaLlegada = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(EDE.FechaHoraLlegada, 'SOLO_FECHA'),''),
	  HoraLlegada = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(EDE.FechaHoraLlegada, 'SOLO_HORA'),''),
    ObservacionLlegada = ISNULL(EDE.ObservacionLlegada,''),
    CantidadPallet = ISNULL(EDE.CantidadPallet, -1),
    PesoReal = ISNULL(EDE.PesoReal, -1),
    OrdenCompra = ISNULL(EDE.OrdenCompra, -1),
	  FechaSalida = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(EDE.FechaHoraSalida, 'SOLO_FECHA'),''),
	  HoraSalida = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(EDE.FechaHoraSalida, 'SOLO_HORA'),''),
    NumeroSello = ISNULL(EDE.NumeroSello,''),
    NumeroContenedor = ISNULL(EDE.NumeroContenedor,'')
  FROM
	  OS_Exportacion_DatoExtra AS EDE
  WHERE
    EDE.IdExportacion = @IdExportacion

END
	    
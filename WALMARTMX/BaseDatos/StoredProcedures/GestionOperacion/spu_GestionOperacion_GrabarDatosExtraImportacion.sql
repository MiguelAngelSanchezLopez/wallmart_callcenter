﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_GrabarDatosExtraImportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_GrabarDatosExtraImportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_GrabarDatosExtraImportacion
END

GO

PRINT  'Creating Procedure spu_GestionOperacion_GrabarDatosExtraImportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_GrabarDatosExtraImportacion
/******************************************************************************
**  Descripcion  : graba datos extras importacion
**  Fecha        : 16/06/2016
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdImportacion AS INT,
@FechaLlegada AS VARCHAR(255),
@HoraLlegada AS VARCHAR(255),
@ObservacionLlegada AS TEXT,
@CantidadPallet AS INT,
@PesoReal AS FLOAT,
@OrdenCompra AS BIGINT,
@FechaSalida AS VARCHAR(255),
@HoraSalida AS VARCHAR(255),
@NumeroSello AS VARCHAR(255),
@NumeroContenedor AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @TotalRegistros AS INT
    SET @TotalRegistros = (SELECT COUNT(IdImportacion) FROM OS_Importacion_DatoExtra WHERE IdImportacion = @IdImportacion)

    IF (@FechaLlegada = '') BEGIN
      SET @FechaLlegada = NULL
    END ELSE BEGIN
      SET @FechaLlegada = @FechaLlegada + ' ' + @HoraLlegada
    END

    IF (@FechaSalida = '') BEGIN
      SET @FechaSalida = NULL
    END ELSE BEGIN
      SET @FechaSalida = @FechaSalida + ' ' + @HoraSalida
    END

    IF (@CantidadPallet = -1) SET @CantidadPallet = NULL
    IF (@PesoReal = -1) SET @PesoReal = NULL
    IF (@OrdenCompra = -1) SET @OrdenCompra = NULL
    
    -- ingresa nuevo registro
    IF (@TotalRegistros = 0) BEGIN
      INSERT INTO OS_Importacion_DatoExtra
      (
	      IdImportacion,
	      FechaHoraLlegada,
	      ObservacionLlegada,
        CantidadPallet,
        PesoReal,
        OrdenCompra,
        FechaHoraSalida,
        NumeroSello,
        NumeroContenedor
      )
      VALUES
      (
	      @IdImportacion,
	      @FechaLlegada,
	      @ObservacionLlegada,
        @CantidadPallet,
        @PesoReal,
        @OrdenCompra,
        @FechaSalida,
        @NumeroSello,
        @NumeroContenedor
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        RETURN
      END

      SET @Status = 'ingresado'
    END ELSE BEGIN
      UPDATE OS_Importacion_DatoExtra
      SET
	      FechaHoraLlegada = @FechaLlegada,
	      ObservacionLlegada = @ObservacionLlegada,
        CantidadPallet = @CantidadPallet,
        PesoReal = @PesoReal,
        OrdenCompra = @OrdenCompra,
        FechaHoraSalida = @FechaSalida,
        NumeroSello = @NumeroSello,
        NumeroContenedor = @NumeroContenedor
      WHERE
 	      IdImportacion = @IdImportacion

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        RETURN
      END

      SET @Status = 'modificado'
    END

  COMMIT
END
GO        
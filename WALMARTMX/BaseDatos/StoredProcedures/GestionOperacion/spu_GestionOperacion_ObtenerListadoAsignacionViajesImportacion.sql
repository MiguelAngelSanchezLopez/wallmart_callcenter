﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ObtenerListadoAsignacionViajesImportacion')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ObtenerListadoAsignacionViajesImportacion'
  DROP  Procedure  dbo.spu_GestionOperacion_ObtenerListadoAsignacionViajesImportacion
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ObtenerListadoAsignacionViajesImportacion'
GO
CREATE Procedure dbo.spu_GestionOperacion_ObtenerListadoAsignacionViajesImportacion
/******************************************************************************
**    Descripcion  : obtiene listado asignación viajes importación
**    Por          : VSR, 15/04/2016
*******************************************************************************/
@IdUsuario AS INT,
@FechaPresentacion AS VARCHAR(255),
@HoraPresentacion AS VARCHAR(255),
@NombreCliente AS VARCHAR(255),
@TipoCarga AS VARCHAR(255),
@Dimension AS VARCHAR(255),
@Retiro AS VARCHAR(255),
@PuertoDescarga AS VARCHAR(255),
@DiasLibres AS VARCHAR(255),
@NumeroOrdenServicio AS VARCHAR(255),
@FechaCreacion AS VARCHAR(255),
@ClasificacionConductor AS VARCHAR(255),
@NumeroGuiaDespacho AS INT,
@SoloGuiaDespacho AS INT,
@Contenedor AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @FechaInicio DATETIME, @FechaTermino DATETIME, @FechaCreacionInicio DATETIME, @FechaCreacionTermino DATETIME
  IF (@FechaPresentacion <> '-1') BEGIN
    IF (@HoraPresentacion = '-1') BEGIN
      SET @FechaInicio = @FechaPresentacion + ' 00:00'
      SET @FechaTermino = @FechaPresentacion + ' 23:59'
    END ELSE BEGIN
      SET @FechaInicio = @FechaPresentacion + ' ' + @HoraPresentacion
      SET @FechaTermino = @FechaPresentacion + ' ' + @HoraPresentacion + ':59.997'
    END
  END

  IF (@FechaCreacion <> '-1') BEGIN
    SET @FechaCreacionInicio = @FechaCreacion + ' 00:00'
    SET @FechaCreacionTermino = @FechaCreacion + ' 23:59:59.997'
  END

  DECLARE @TablaViaje AS TABLE(
    IdImportacion INT,
    NumeroOrdenServicio INT,
    NumeroLineaMO INT,
    VersionMO INT,
    NombreCliente VARCHAR(255),
    LugarPresentacion VARCHAR(255),
    SegundoDestino VARCHAR(255),
	  FechaHoraPresentacion VARCHAR(255),
	  MedidaContenedor INT,
    DescripcionTipoCarga VARCHAR(255),
    PesoNetoCarga FLOAT,
    NumeroContenedor VARCHAR(255),
    NombreTerminalOrigen VARCHAR(255),
    NombreNave VARCHAR(255),
    FechaHoraProgramacion VARCHAR(255),
    Observaciones VARCHAR(255),
    NombreConductor VARCHAR(255),
    PatenteTracto VARCHAR(255),
    PatenteTrailer VARCHAR(255),
    TelefonoConductor VARCHAR(255),
    EstadoViaje VARCHAR(255),
    FechaHoraPresentacionOrden DATETIME,
    FechaLlegada VARCHAR(255),
    ObservacionLlegada VARCHAR(8000),
    CantidadPallet INT,
    PesoReal FLOAT,
    OrdenCompra BIGINT,
    NumeroGuiaDespacho VARCHAR(255),
    FechaSalida VARCHAR(255),
    NumeroSello VARCHAR(255),
    NumeroContenedorCargaSuelta VARCHAR(255),
    ReportabilidadPatenteTracto VARCHAR(255),
    ReportabilidadPatenteTractoFecha VARCHAR(255),
    ReportabilidadPatenteTrailer VARCHAR(255),
    ReportabilidadPatenteTrailerFecha VARCHAR(255),
    FechaAsignacionConductor DATETIME,
    EstadoRuta VARCHAR(50)
  )
  
  DECLARE @TablaSalida AS TABLE(
    IdImportacion INT,
    NumeroOrdenServicio INT,
    NumeroLineaMO INT,
    VersionMO INT,
    NombreCliente VARCHAR(255),
    LugarPresentacion VARCHAR(255),
    SegundoDestino VARCHAR(255),
	  FechaHoraPresentacion VARCHAR(255),
	  MedidaContenedor INT,
    DescripcionTipoCarga VARCHAR(255),
    PesoNetoCarga FLOAT,
    NumeroContenedor VARCHAR(255),
    NombreTerminalOrigen VARCHAR(255),
    NombreNave VARCHAR(255),
    FechaHoraProgramacion VARCHAR(255),
    Observaciones VARCHAR(255),
    NombreConductor VARCHAR(255),
    PatenteTracto VARCHAR(255),
    PatenteTrailer VARCHAR(255),
    TelefonoConductor VARCHAR(255),
    EstadoViaje VARCHAR(255),
    FechaHoraPresentacionOrden DATETIME,
    FechaLlegada VARCHAR(255),
    ObservacionLlegada VARCHAR(8000),
    CantidadPallet INT,
    PesoReal FLOAT,
    OrdenCompra BIGINT,
    NumeroGuiaDespacho VARCHAR(255),
    FechaSalida VARCHAR(255),
    NumeroSello VARCHAR(255),
    NumeroContenedorCargaSuelta VARCHAR(255),
    ReportabilidadPatenteTracto VARCHAR(255),
    ReportabilidadPatenteTractoFecha VARCHAR(255),
    ReportabilidadPatenteTrailer VARCHAR(255),
    ReportabilidadPatenteTrailerFecha VARCHAR(255),
    FechaAsignacionConductor DATETIME,
    EstadoRuta VARCHAR(50)
  )  

  -- obtiene los viajes que cumplen con los criterios de busqueda ------------------------
  INSERT INTO @TablaViaje(
    IdImportacion,
    NumeroOrdenServicio,
    NumeroLineaMO,
    VersionMO,
    NombreCliente,
    LugarPresentacion,
    SegundoDestino,
	  FechaHoraPresentacion,
	  MedidaContenedor,
    DescripcionTipoCarga,
    PesoNetoCarga,
    NumeroContenedor,
    NombreTerminalOrigen,
    NombreNave,
    FechaHoraProgramacion,
    Observaciones,
    NombreConductor,
    PatenteTracto,
    PatenteTrailer,
    TelefonoConductor,
    EstadoViaje,
    FechaHoraPresentacionOrden,
    FechaLlegada,
    ObservacionLlegada,
    CantidadPallet,
    PesoReal,
    OrdenCompra,
    NumeroGuiaDespacho,
    FechaSalida,
    NumeroSello,
    NumeroContenedorCargaSuelta,
    ReportabilidadPatenteTracto,
    ReportabilidadPatenteTractoFecha,
    ReportabilidadPatenteTrailer,
    ReportabilidadPatenteTrailerFecha,
    FechaAsignacionConductor,
    EstadoRuta
  )
  SELECT
    T.*
  FROM (
        SELECT
          IdImportacion = I.Id,
	        I.NumeroOrdenServicio,
          I.NumeroLineaMO,
          I.VersionMO,
          NombreCliente = ISNULL(I.NombreCliente,''),
          LugarPresentacion = CASE WHEN OROrigen.DescripcionDestino IS NULL THEN 'SIN RUTA ASIGNADA'
                              ELSE OROrigen.DescripcionDestino + CASE WHEN OROrigen.CodDestino = '' THEN '' ELSE ' - ' + OROrigen.CodDestino END
                              END,
          SegundoDestino = CASE WHEN ORDestino.DescripcionDestino IS NULL THEN 'SIN RUTA ASIGNADA'
                           ELSE ORDestino.DescripcionDestino + CASE WHEN ORDestino.CodDestino = '' THEN '' ELSE ' - ' + ORDestino.CodDestino END
                           END,
	        FechaHoraPresentacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(I.FechaHoraPresentacion, 'FECHA_COMPLETA'),
	        MedidaContenedor = I.MedidaContenedor,
          DescripcionTipoCarga = ISNULL(I.DescripcionTipoCarga,''),
          PesoNetoCarga = ROUND(ISNULL(I.PesoNetoCarga,0),2),
          NumeroContenedor = ISNULL(I.MarcaContenedor,'') + ISNULL(I.NumeroContenedor,'') + ISNULL(I.DigitoContenedor,''),
          NombreTerminalOrigen = OROrigen.DescripcionOrigen,
          NombreNave = ISNULL(I.NombreNave,''),
          FechaHoraProgramacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(I.FechaCreacion, 'FECHA_COMPLETA'),
          Observaciones = ISNULL(I.Observaciones,''),
          NombreConductor = ISNULL(IAC.NombreConductor,''),
          PatenteTracto = ISNULL(IAC.PatenteTracto,''),
          PatenteTrailer = ISNULL(IAC.PatenteTrailer,''),
          TelefonoConductor = ISNULL(IAC.Telefono,''),
          EstadoViaje = CASE ISNULL(IAC.EstadoViaje,'')
                          WHEN 'ENLOCAL-R' THEN 'EN TIENDA'
                          WHEN 'ENLOCAL-P' THEN 'FINALIZADO'
                          ELSE UPPER(ISNULL(IAC.EstadoViaje,''))
                        END,
          FechaHoraPresentacionOrden = I.FechaHoraPresentacion,
          FechaLlegada = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(IDE.FechaHoraLlegada, 'FECHA_COMPLETA'),''),
          ObservacionLlegada = ISNULL(IDE.ObservacionLlegada,''),
          CantidadPallet = ISNULL(IDE.CantidadPallet,-1),
          PesoReal = ISNULL(IDE.PesoReal,-1),
          OrdenCompra = ISNULL(IDE.OrdenCompra,-1),
          NumeroGuiaDespacho = ISNULL(CONVERT(VARCHAR,GD.NumeroGuiaDespacho),'SIN ID EMBARQUE'),
          FechaSalida = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(IDE.FechaHoraSalida, 'FECHA_COMPLETA'),''),
          NumeroSello = ISNULL(IDE.NumeroSello,''),
          NumeroContenedorCargaSuelta = ISNULL(IDE.NumeroContenedor,''),
		      /*
          Modificación 2016-08-10. Se obtiene el reporte del momento en que se asignó el conductor.
          ReportabilidadPatenteTracto = Case	when IAC.UltReporteTracto is null then 'NoIntegrada'
												when IAC.UltReporteTracto < dateadd(hour, -12, dbo.fnu_GETDATE()) then 'Offline'
												Else 'Online'
											End,	 
          ReportabilidadPatenteTractoFecha = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(IAC.UltReporteTracto, 'FECHA_COMPLETA'), ''),
		      ReportabilidadPatenteTrailer = Case	when IAC.UltReporteTrailer is null then 'NoIntegrada'
												when IAC.UltReporteTrailer < dateadd(hour, -12, dbo.fnu_GETDATE()) then 'Offline'
												Else 'Online' 
											End,
          ReportabilidadPatenteTrailerFecha = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(IAC.UltReportetrailer, 'FECHA_COMPLETA'), ''),
          */
          ReportabilidadPatenteTracto = ISNULL(TLPTracto.Estado,'NoIntegrada'),
          ReportabilidadPatenteTractoFecha = ISNULL(TLPTracto.Fecha,''),
          ReportabilidadPatenteTrailer = ISNULL(TLPTrailer.Estado,'NoIntegrada'),
          ReportabilidadPatenteTrailerFecha = ISNULL(TLPTrailer.Fecha,''),
          FechaAsignacionConductor = ISNULL(I.FechaCreacion,'19000101'),
		      EstadoRuta = ''
        FROM
	        OS_Importacion AS I
          LEFT JOIN OS_Importacion_AsignacionConductor AS IAC ON (I.Id = IAC.IdImportacion)
          LEFT JOIN OS_Importacion_DatoExtra AS IDE ON (I.Id = IDE.IdImportacion)
          LEFT JOIN OS_GuiaDespacho AS GD ON (I.NumeroOrdenServicio = GD.NumeroOrdenServicio AND I.NumeroLineaMO = GD.NumeroLineaMO AND I.VersionMO = GD.VersionMO)
          LEFT JOIN vwu_ReportabilidadPatente AS TLPTracto ON (IAC.PatenteTracto = TLPTracto.Patente)
          LEFT JOIN vwu_ReportabilidadPatente AS TLPTrailer ON (IAC.PatenteTrailer = TLPTrailer.Patente)
          OUTER APPLY (
            SELECT TOP 1
              DescripcionOrigen = ISNULL(aux.DescripcionOrigen,''),
              DescripcionDestino = ISNULL(aux.DescripcionDestino,ISNULL(aux.DescripcionOrigen,'')),
              CodDestino = ISNULL(aux.CodDestino,'')
            FROM
              OS_Rutas AS aux
            WHERE
                aux.NumeroOrdenServicio = I.NumeroOrdenServicio
            AND aux.NumeroLineaMO = I.NumeroLineaMO
            AND aux.VersionMO = I.VersionMO
          ) AS OROrigen
          OUTER APPLY (
            SELECT TOP 1
              DescripcionDestino = ISNULL(aux.DescripcionDestino,ISNULL(aux.DescripcionOrigen,'')),
              CodDestino = ISNULL(aux.CodDestino,'')
            FROM
              OS_Rutas AS aux
            WHERE
                aux.NumeroOrdenServicio = I.NumeroOrdenServicio
            AND aux.NumeroLineaMO = I.NumeroLineaMO
            AND aux.VersionMO = I.VersionMO
            ORDER BY
              aux.Id DESC
          ) AS ORDestino
          OUTER APPLY (
				Select top 1 R.Id, R.CodOrigen, R.CodDestino FROM OS_Rutas R WHERE R.NumeroOrdenServicio = I.NumeroOrdenServicio AND R.NumeroLineaMO = I.NumeroLineaMO AND R.VersionMO = I.VersionMO
			) AS RF
        WHERE
            (@FechaPresentacion = '-1' OR I.FechaHoraPresentacion BETWEEN @FechaInicio AND @FechaTermino)
        AND (@NombreCliente = '-1' OR I.NombreCliente = @NombreCliente)
        AND (@TipoCarga = '-1' OR I.DescripcionTipoCarga = @TipoCarga)
        AND (@Dimension = '-1' OR I.MedidaContenedor = @Dimension)
        AND (@Retiro = '-1' OR I.DirectoIndirecto = @Retiro)
        AND (@PuertoDescarga = '-1' OR I.NombrePuertoDescarga = @PuertoDescarga)
        AND (@DiasLibres = '-1' OR I.DiasDemurrage = @DiasLibres)
        AND (@NumeroOrdenServicio = '-1' OR I.NumeroOrdenServicio = @NumeroOrdenServicio)
        AND (@FechaCreacion = '-1' OR I.FechaCreacion BETWEEN @FechaCreacionInicio AND @FechaCreacionTermino)
        AND (@ClasificacionConductor = '-1' OR I.NombreTerminalGestionServicio = @ClasificacionConductor)
        AND (@NumeroGuiaDespacho = -1 OR GD.NumeroGuiaDespacho = @NumeroGuiaDespacho)
        AND LEFT(CONVERT(VARCHAR, I.FechaHoraPresentacion, 108), 5) > '00:00'
        AND (@Contenedor = '-1' OR ISNULL(I.MarcaContenedor,'') + ISNULL(I.NumeroContenedor,'') + ISNULL(I.DigitoContenedor,'') LIKE '%'+ @Contenedor +'%')
        AND (I.EstadoLinea <> 'A')
  ) AS T
  WHERE
    (
         ( @SoloGuiaDespacho = 0 )
      OR ( @SoloGuiaDespacho = 1 AND T.NumeroGuiaDespacho <> 'SIN ID EMBARQUE' )
    )
  
  -- obtiene los viajes con la ultima version, y los que tienen guia de despacho -----------------
  INSERT INTO @TablaSalida(
    IdImportacion,
    NumeroOrdenServicio,
    NumeroLineaMO,
    VersionMO,
    NombreCliente,
    LugarPresentacion,
    SegundoDestino,
	  FechaHoraPresentacion,
	  MedidaContenedor,
    DescripcionTipoCarga,
    PesoNetoCarga,
    NumeroContenedor,
    NombreTerminalOrigen,
    NombreNave,
    FechaHoraProgramacion,
    Observaciones,
    NombreConductor,
    PatenteTracto,
    PatenteTrailer,
    TelefonoConductor,
    EstadoViaje,
    FechaHoraPresentacionOrden,
    FechaLlegada,
    ObservacionLlegada,
    CantidadPallet,
    PesoReal,
    OrdenCompra,
    NumeroGuiaDespacho,
    FechaSalida,
    NumeroSello,
    NumeroContenedorCargaSuelta,
    ReportabilidadPatenteTracto,
    ReportabilidadPatenteTractoFecha,
    ReportabilidadPatenteTrailer,
    ReportabilidadPatenteTrailerFecha,
    FechaAsignacionConductor,
    EstadoRuta
  )
  -- obtiene los viajes con la ultima version --------------------
  SELECT
    T.*
  FROM
    @TablaViaje AS T
    INNER JOIN (
      SELECT
        NumeroOrdenServicio,
        NumeroLineaMO,
        VersionMO = MAX(VersionMO)
      FROM
        @TablaViaje
      GROUP BY
        NumeroOrdenServicio,
        NumeroLineaMO
    ) AS aux ON (T.NumeroOrdenServicio = aux.NumeroOrdenServicio AND T.NumeroLineaMO = aux.NumeroLineaMO AND T.VersionMO = aux.VersionMO)

  -- obtiene los viajes que tienen guia de despacho independiente de la version --------------------
  UNION
  SELECT
    T.*
  FROM
    @TablaViaje AS T
  WHERE
    T.NumeroGuiaDespacho <> 'SIN ID EMBARQUE'

  -- elimina los viajes que para lineas repetidas de un mismo NumeroOrdenServicio solo se muestre el que tenga guia de despacho asociada ----------
  DELETE @TablaSalida
  FROM
    @TablaSalida AS TS
    INNER JOIN (
      SELECT
        aux.NumeroOrdenServicio,
        aux.NumeroLineaMO,
        aux.VersionMO
      FROM
        @TablaViaje AS aux
      WHERE
        aux.NumeroGuiaDespacho <> 'SIN ID EMBARQUE'
    ) AS T ON (TS.NumeroOrdenServicio = T.NumeroOrdenServicio AND TS.NumeroLineaMO = T.NumeroLineaMO)
  WHERE
    TS.VersionMO <> T.VersionMO


  ------------------------------------------------------------------
  -- TABLA 0: Listado datos
  ------------------------------------------------------------------
  SELECT
    T.*
  FROM
    @TablaSalida AS T
  ORDER BY
    T.FechaHoraPresentacionOrden 

  ------------------------------------------------------------------
  -- TABLA 1: Listado datos para exportar
  ------------------------------------------------------------------
  SELECT
    [NUMERO ORDEN SERVICIO] = T.NumeroOrdenServicio,
    --[NUMERO LINEA] = T.NumeroLineaMO,
    --[VERSION] = T.VersionMO,
    [NUMERO GUIA DESPACHO] = T.NumeroGuiaDespacho,
    [NOMBRE CLIENTE] = T.NombreCliente,
    [LUGAR PRESENTACION] = T.LugarPresentacion,
    [SEGUNDO DESTINO] = T.SegundoDestino,
	  [FECHA PRESENTACION] = T.FechaHoraPresentacion,
	  [MEDIDA] = T.MedidaContenedor,
    --[TIPO] = T.DescripcionTipoCarga,
    [PESO] = T.PesoNetoCarga,
    [CONTENEDOR] = T.NumeroContenedor,
    [ORIGEN] = T.NombreTerminalOrigen,
    [NAVE] = T.NombreNave,
    [FECHA PROGRAMACION] = T.FechaHoraProgramacion,
    [OBSERVACIONES] = T.Observaciones,
    [NOMBRE OPERADOR] = T.NombreConductor,
    [PLACA TRACTO] = T.PatenteTracto,
    [PLACA REMOLQUE] = T.PatenteTrailer,
    [TELEFONO OPERADOR] = T.TelefonoConductor,
    [ESTADO VIAJE] = T.EstadoViaje,
    [FECHA LLEGADA] = T.FechaLlegada,
    [FECHA SALIDA] = T.FechaSalida,
    [OBSERVACION LLEGADA] = T.ObservacionLlegada,
    [CANTIDAD PALLET] = CASE WHEN T.CantidadPallet = -1 THEN '' ELSE CONVERT(VARCHAR,T.CantidadPallet) END,
    [PESO REAL] = CASE WHEN T.PesoReal = -1 THEN '' ELSE CONVERT(VARCHAR,T.PesoReal) END,
    [ORDEN COMPRA] = CASE WHEN T.OrdenCompra = -1 THEN '' ELSE '''' + CONVERT(VARCHAR,T.OrdenCompra) END
  FROM
    @TablaSalida AS T
  ORDER BY
    T.FechaHoraPresentacionOrden 

END
	    

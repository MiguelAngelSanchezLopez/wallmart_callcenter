﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_GestionOperacion_ActualizarRegistroEnBaseDatos')
BEGIN
  PRINT  'Dropping Procedure spu_GestionOperacion_ActualizarRegistroEnBaseDatos'
  DROP  Procedure  dbo.spu_GestionOperacion_ActualizarRegistroEnBaseDatos
END
GO

PRINT  'Creating Procedure spu_GestionOperacion_ActualizarRegistroEnBaseDatos'
GO
CREATE Procedure dbo.spu_GestionOperacion_ActualizarRegistroEnBaseDatos
/******************************************************************************
**    Descripcion  : actualiza registro en base de datos
**    Por          : VSR, 17/05/2016
*******************************************************************************/
@Status AS INT OUTPUT,
@OrigenDatoTabla AS VARCHAR(255),
@OrigenDatoIdRegistro AS INT,
@Telefono AS VARCHAR(255),
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    IF (@OrigenDatoTabla = 'ControlAccesoValidacionEntradaManual') BEGIN
      UPDATE ControlAccesoValidacionEntradaManual
      SET
        Celular = @Telefono
      WHERE
        Id = @OrigenDatoIdRegistro

    END ELSE IF (@OrigenDatoTabla = 'Usuario') BEGIN
      UPDATE Usuario
      SET
        Telefono = @Telefono
      WHERE
        Id = @OrigenDatoIdRegistro

      UPDATE UsuarioDatoExtra
      SET
        PatenteTracto = @PatenteTracto,
        PatenteTrailer = @PatenteTrailer
      WHERE
        IdUsuario = @OrigenDatoIdRegistro

    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END
    SET @Status = 1

  COMMIT  

END
	    

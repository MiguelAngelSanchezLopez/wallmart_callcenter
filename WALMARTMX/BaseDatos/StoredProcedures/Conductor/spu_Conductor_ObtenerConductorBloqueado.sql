﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_ObtenerConductorBloqueado')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_ObtenerConductorBloqueado'
  DROP  Procedure  dbo.spu_Conductor_ObtenerConductorBloqueado
END

GO

PRINT  'Creating Procedure spu_Conductor_ObtenerConductorBloqueado'
GO
CREATE Procedure dbo.spu_Conductor_ObtenerConductorBloqueado
/******************************************************************************
**    Descripcion  : Obtiene listado de conductores bloqueados
**    Por          : DG, 23/06/2015
*******************************************************************************/
 @rut AS VARCHAR(10)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT MotivoBloqueo = Motivo
  FROM ConductorBloqueado
  WHERE rutConductor = @rut
  
END
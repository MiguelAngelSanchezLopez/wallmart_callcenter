﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_Existe_IdArchivo')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_Existe_IdArchivo'
  DROP  Procedure  dbo.spu_Conductor_Existe_IdArchivo
END

GO

PRINT  'Creating Procedure spu_Conductor_Existe_IdArchivo'
GO
CREATE Procedure [dbo].[spu_Conductor_Existe_IdArchivo]
/******************************************************************************
**    Descripcion  : Obtiene detalle de IdArchivo
**    Por          : VSR, 24/07/2009
*******************************************************************************/
@Status AS Varchar(20) OUTPUT,
@IdTipoDocumentoXFichaConductor AS INT

AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON
  
    -- asume que el registro no existe
	SET @Status = 0
	
                SET @Status = (SELECT
					AXR.IdArchivo
				  FROM
					ArchivosXRegistro AS AXR
				  WHERE 
	  				AXR.IdRegistro = @IdTipoDocumentoXFichaConductor
	  				AND AXR.Entidad = 'DocumentoConductor')
    
IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

  END
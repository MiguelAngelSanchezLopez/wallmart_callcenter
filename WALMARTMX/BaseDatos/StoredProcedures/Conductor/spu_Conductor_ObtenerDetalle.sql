﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Conductor_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Conductor_ObtenerDetalle'
GO
CREATE Procedure [dbo].[spu_Conductor_ObtenerDetalle]
/******************************************************************************
**    Descripcion  : Obtiene detalle de Conductores consultado
**    Por          : VSR, 24/07/2009
*******************************************************************************/
@IdConductor AS INT,
@IdTransportista AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON
  
  ---------------------------------------------------------
  -- Tabla Detalle FichaConductor
  ---------------------------------------------------------
	SELECT
		IdConductor = FC.Id,
		IdTransportista = FC.IdTransportista,
		Nombre = ISNULL(FC.Nombre,''),
		Paterno = ISNULL(FC.Paterno,''),
		Materno = ISNULL(FC.Materno,''),
		NombreCompleto = ISNULL(RTRIM(LTRIM(FC.Nombre)),'') + ISNULL(' '+ RTRIM(LTRIM(FC.Paterno)),'') + ISNULL(' '+ RTRIM(LTRIM(FC.Materno)),''),
		Rut = ISNULL(FC.Rut,''),
		Dv = ISNULL(FC.Dv,''),
		RutCompleto = CASE WHEN ISNULL(FC.Rut,'') = '' THEN '' ELSE ISNULL(RTRIM(LTRIM(FC.Rut)),'') + ISNULL('-'+ RTRIM(LTRIM(FC.Dv)),'') END,
		Nacionalidad = ISNULL(FC.Nacionalidad,''),
		EstadoCivil = ISNULL(FC.EstadoCivil,''),
		Sexo = ISNULL(FC.Sexo,''),
		Email = ISNULL(FC.Email,''),
		Telefono = ISNULL(FC.Telefono,''),
		Telefono2 = ISNULL(FC.Telefono2,''),
		FechaNacimiento = ISNULL(FC.FechaNacimiento,''),
		Direccion = ISNULL(FC.Direccion,''),
		FechaIngreso = ISNULL(FC.FechaIngreso,''),
		CodTelefono = ISNULL(FC.CodTelefono,''),
		CodTelefono2 = ISNULL(FC.CodTelefono2,''),
		NivelEstudio = ISNULL(FC.NivelEstudio,''),
		NacionalidadOtra = ISNULL(FC.NacionalidadOtra,''),
		IdRegion = C.IdRegion,
		IdComuna = C.Id
	FROM
		FichaConductor AS FC
		LEFT JOIN Comuna AS C ON C.Id = FC.IdComuna
	WHERE
		FC.Id = @IdConductor
		AND FC.IdTransportista = @IdTransportista 
		
		
		
		
	---------------------------------------------------------
	-- Tabla Detalle TipoDocumentoXFichaConductor
	---------------------------------------------------------	
	  SELECT
		Id = T.Id,
		IdTipoDocumentoXFichaConductor = TD.Id,
		IdConductor = TD.IdFichaConductor,
		IdTipoDocumento = TD.IdTipoDocumento,
		IdTransportista  = TD.IdTransportista,
		NombreDocumento = ISNULL(T.NombreDocumento,''),
		Indefinido = ISNULL(TD.Indefinido,''),
		FechaExpiracion = ISNULL(TD.FechaExpiracion,''),
		idArchivo = a.Id,
		NombreArchivo = a.Nombre,
		Extension = a.Extension,
		ContentType = a.ContentType,
		Tamano = a.Tamano
	  FROM
		TipoDocumentoXFichaConductor AS TD
		RIGHT JOIN TipoDocumento AS T ON (TD.IdTipoDocumento = T.Id AND TD.IdFichaConductor = @IdConductor AND TD.IdTransportista = @IdTransportista AND TD.Estado =1)--@IdConductor)
		LEFT JOIN ArchivosXRegistro AxR ON (TD.Id = AXR.IdRegistro AND AxR.Entidad = 'DocumentoConductor')
		LEFT JOIN Archivo a ON (a.id = axr.IdArchivo)
	ORDER BY
		T.Id
		
		
	
	---------------------------------------------------------
	-- Tabla Detalle TipoDocumento
	---------------------------------------------------------		
	SELECT
		Id = TD.Id,
		NombreDocumento = ISNULL(TD.NombreDocumento,'')
		
	FROM
		TipoDocumento AS TD
	WHERE 
		Estado = 1
	ORDER BY
		Orden

END
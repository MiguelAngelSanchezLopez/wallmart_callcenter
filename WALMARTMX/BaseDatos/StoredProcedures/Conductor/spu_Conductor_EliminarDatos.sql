﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_EliminarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_EliminarDatos'
  DROP  Procedure  dbo.spu_Conductor_EliminarDatos
END

GO

PRINT  'Creating Procedure spu_Conductor_EliminarDatos'
GO
CREATE Procedure [dbo].[spu_Conductor_EliminarDatos]
/******************************************************************************
**  Descripcion  : elimina un Conductor
**  Fecha        : 15/09/2009
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdConductor AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    
    UPDATE FichaConductor SET Estado = 0 WHERE Id = @IdConductor

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
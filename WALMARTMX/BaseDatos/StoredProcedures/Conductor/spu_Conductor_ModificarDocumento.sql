﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_ModificarDocumento')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_ModificarDocumento'
  DROP  Procedure  dbo.spu_Conductor_ModificarDocumento
END

GO

PRINT  'Creating Procedure spu_Conductor_ModificarDocumento'
GO
CREATE Procedure [dbo].[spu_Conductor_ModificarDocumento]
/******************************************************************************
**  Descripcion  : modifica un documento
**  Fecha        : 27/07/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdTipoDocumentoXFichaConductor AS INT,
@IdTipoDocumento AS INT,
@IdTransporte AS INT,
@IdConductor AS INT,
@intIndefinidos AS BIT,
@FechaExpiracion AS VARCHAR(20)

AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

	DECLARE @fechaHoraActual AS VARCHAR(20)
    SET @fechaHoraActual = dbo.fnu_ObtenerFechaISO()

  BEGIN TRANSACTION
    -- actualiza Documento
      UPDATE TipoDocumentoXFichaConductor
      SET
        IdTipoDocumento = @IdTipoDocumento,
        IdTransportista = @IdTransporte,
        IdFichaConductor = @IdConductor,
        Indefinido = @intIndefinidos,
        FechaExpiracion = @FechaExpiracion,
        FechaHoraModificacion = @fechaHoraActual
      WHERE
        Id = @IdTipoDocumentoXFichaConductor

      SET @Status = 'modificado'

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_ObtenerListadoConFiltro')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_ObtenerListadoConFiltro'
  DROP  Procedure  dbo.spu_Conductor_ObtenerListadoConFiltro
END

GO

PRINT  'Creating Procedure spu_Conductor_ObtenerListadoConFiltro'
GO
CREATE Procedure [dbo].[spu_Conductor_ObtenerListadoConFiltro]
/******************************************************************************
**    Descripcion  : Obtiene listado de Conductores usando filtros de busqueda
**    Por          : HML, 06-04-2015
*******************************************************************************/
@IdUsuario AS INT,
@Nombre AS VARCHAR(150),
@RUT AS VARCHAR(12),
@IdPerfil AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
	  IdConductor = C.Id,
	  RutCompleto = CASE WHEN ISNULL(C.Rut,'') = '' THEN '' ELSE ISNULL(RTRIM(LTRIM(C.Rut)),'') + ISNULL('-'+ RTRIM(LTRIM(C.Dv)),'') END,
	  NombreCompleto = ISNULL(RTRIM(LTRIM(C.Nombre)),'') + ISNULL(' '+ RTRIM(LTRIM(C.Paterno)),'') + ISNULL(' '+ RTRIM(LTRIM(C.Materno)),'')
  FROM
    FichaConductor AS C
  WHERE
	(@IdUsuario = 1 OR C.IdTransportista = @IdUsuario)
	AND (@RUT = '-1' OR C.RUT = @RUT)
	AND (@Nombre = '-1' OR RTRIM(LTRIM(ISNULL(C.Nombre,''))) +' '+ RTRIM(LTRIM(ISNULL(C.Paterno,''))) +' '+ RTRIM(LTRIM(ISNULL(C.Materno,''))) LIKE '%'+ REPLACE(@Nombre,' ','%') +'%')
	AND (C.Estado = 1)
  ORDER BY
    IdConductor DESC

END
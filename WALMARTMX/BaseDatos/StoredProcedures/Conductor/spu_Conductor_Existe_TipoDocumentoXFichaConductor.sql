﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_Existe_TipoDocumentoXFichaConductor')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_Existe_TipoDocumentoXFichaConductor'
  DROP  Procedure  dbo.spu_Conductor_Existe_TipoDocumentoXFichaConductor
END

GO

PRINT  'Creating Procedure spu_Conductor_Existe_TipoDocumentoXFichaConductor'
GO
CREATE Procedure [dbo].[spu_Conductor_Existe_TipoDocumentoXFichaConductor]
/******************************************************************************
**    Descripcion  : Obtiene detalle de TipoDocumentoXFichaConductor
**    Por          : VSR, 24/07/2009
*******************************************************************************/
@Status AS Varchar(20) OUTPUT,
@IdConductor AS INT,
@IdTipoDocumento AS INT,
@IdTransportista AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON
  
    -- asume que el registro no existe
	SET @Status = 0
	
                SET @Status = (SELECT
					TD.Id
				  FROM
					TipoDocumentoXFichaConductor AS TD
				  WHERE 
	  				TD.IdFichaConductor = @IdConductor
					AND TD.IdTransportista = @IdTransportista 
					AND TD.IdTipoDocumento = @IdTipoDocumento
					AND TD.Estado = 1)
    
IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

  END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_GrabarDatosDocumento')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_GrabarDatosDocumento'
  DROP  Procedure  dbo.spu_Conductor_GrabarDatosDocumento
END

GO

PRINT  'Creating Procedure spu_Conductor_GrabarDatosDocumento'
GO
CREATE Procedure [dbo].[spu_Conductor_GrabarDatosDocumento]
/******************************************************************************
**  Descripcion  : graba Documento
**  Fecha        : 27/07/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdTipoDocumentoXFichaConductor AS INT OUTPUT,
@IdTipoDocumento AS INT,
@IdTransporte AS INT,
@IdConductor AS INT,
@intIndefinidos AS BIT,
@FechaExpiracion AS VARCHAR(20)

AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @fechaHoraActual AS VARCHAR(20)
    DECLARE @Estado AS INT
    SET @fechaHoraActual = dbo.fnu_ObtenerFechaISO()
    SET @Estado = 1
    SET @IdTipoDocumentoXFichaConductor = -1


    -- graba Documento
      INSERT INTO TipoDocumentoXFichaConductor
      (
		idTipoDocumento,
        idTransportista,
        idFichaConductor,
        Indefinido,
        FechaExpiracion,
        FechaHoraCreacion,
        FechaHoraModificacion,
        Estado
      )
      VALUES
      (
		@IdTipoDocumento,
        @IdTransporte,
        @IdConductor,
        @intIndefinidos,
        @FechaExpiracion,
        @fechaHoraActual,
        @fechaHoraActual,
        @Estado
      )
      SET @Status = 'ingresado'
      SET @IdTipoDocumentoXFichaConductor = SCOPE_IDENTITY()


    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_ModificarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_ModificarDatos'
  DROP  Procedure  dbo.spu_Conductor_ModificarDatos
END

GO

PRINT  'Creating Procedure spu_Conductor_ModificarDatos'
GO
CREATE Procedure [dbo].[spu_Conductor_ModificarDatos]
/******************************************************************************
**  Descripcion  : modifica un conductor
**  Fecha        : 27/07/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdConductor AS INT,
@IdTransportista AS INT,
@Rut AS VARCHAR(20),
@DV AS VARCHAR(1),
@Nombre AS VARCHAR(255),
@Paterno AS VARCHAR(255),
@Materno AS VARCHAR(255),
@Nacionalidad AS VARCHAR(50),
@estadoCivil AS VARCHAR(50),
@sexo AS VARCHAR(50),
@email AS VARCHAR(50),
@telefono AS VARCHAR(50),
@telefono2 AS VARCHAR(50),
@fechaNacimiento AS VARCHAR(20),
@direccion AS VARCHAR(255),
@fechaIngreso AS VARCHAR(14),
@codTelefono AS VARCHAR(2),
@codTelefono2 AS VARCHAR(2),
@nivelEstudio AS VARCHAR(20),
@nacionalidadOtra AS VARCHAR(50),
@idComuna AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

	DECLARE @existeRut AS INT
	DECLARE @fechaHoraActual AS VARCHAR(20)
    SET @fechaHoraActual = dbo.fnu_ObtenerFechaISO()
	-- verifica si el Rut existe
	EXEC dbo.spu_Conductor_VerificarDuplicidadRut @existeRut OUTPUT, @IdConductor, @Rut, @IdTransportista

	-- formatea valores
	--IF(@IdPerfil = 0) SET @IdPerfil=NULL

  BEGIN TRANSACTION
    -- actualiza Conductor si corresponde
    IF(@existeRut = 1)BEGIN
      UPDATE FichaConductor
      SET
        Rut = @Rut,
        DV = @Dv,
        Nombre = @Nombre,
        Paterno = @Paterno,
        Materno = @Materno,
        Nacionalidad = @Nacionalidad,
        EstadoCivil = @estadoCivil,
        Sexo = @sexo,
        Email = @email,
        Telefono = @telefono,
        Telefono2 = @telefono2,
        FechaNacimiento = @fechaNacimiento,
        Direccion = @direccion,
        FechaIngreso = @fechaIngreso,
        FechaHoraModificacion = @fechaHoraActual,
        codTelefono = @codTelefono,
        codTelefono2 = @codTelefono2,
        nivelEstudio = @nivelEstudio,
        nacionalidadOtra = @nacionalidadOtra,
        idComuna = @idComuna
      WHERE
        Id = @IdConductor

      SET @Status = 'modificado'
    END ELSE BEGIN
      SET @Status = 'no modificado'
    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Conductor_GrabarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Conductor_GrabarDatos'
  DROP  Procedure  dbo.spu_Conductor_GrabarDatos
END

GO

PRINT  'Creating Procedure spu_Conductor_GrabarDatos'
GO
CREATE Procedure [dbo].[spu_Conductor_GrabarDatos]
/******************************************************************************
**  Descripcion  : graba conductor
**  Fecha        : 27/07/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdConductor AS INT OUTPUT,
@IdTransportista AS INT,
@Rut AS VARCHAR(20),
@DV AS VARCHAR(1),
@Nombre AS VARCHAR(255),
@Paterno AS VARCHAR(255),
@Materno AS VARCHAR(255),
@Nacionalidad AS VARCHAR(50),
@estadoCivil AS VARCHAR(50),
@sexo AS VARCHAR(50),
@email AS VARCHAR(50),
@telefono AS VARCHAR(10),
@telefono2 AS VARCHAR(10),
@fechaNacimiento AS VARCHAR(20),
@direccion AS VARCHAR(255),
@fechaIngreso AS VARCHAR(14),
@codTelefono AS VARCHAR(2),
@codTelefono2 AS VARCHAR(2),
@nivelEstudio AS VARCHAR(20),
@nacionalidadOtra AS VARCHAR(50),
@idComuna AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @existeRut AS INT
    DECLARE @fechaHoraActual AS VARCHAR(20)
    DECLARE @Estado AS INT
    SET @fechaHoraActual = dbo.fnu_ObtenerFechaISO()
    SET @Estado = 1
    SET @IdConductor = -1

    -- verifica si el Rut existe
    EXEC dbo.spu_Conductor_VerificarDuplicidadRut @existeRut OUTPUT, -1, @Rut, @IdTransportista

    -- formatea valores
    --IF(@IdPerfil = 0) SET @IdPerfil=NULL

    -- graba Usuario si corresponde
    IF(@existeRut = 1) BEGIN
      INSERT INTO FichaConductor
      (
		IdTransportista,
        Rut,
        Dv,
        Nombre,
        Paterno,
        Materno,
        Nacionalidad,
        estadoCivil,
        sexo,
        email,
        telefono,
        telefono2,
        fechaNacimiento,
        direccion,
        fechaIngreso,
        FechaHoraCreacion,
        FechaHoraModificacion,
        Estado,
        codTelefono,
        codTelefono2,
        nivelEstudio,
        nacionalidadOtra,
        idComuna
      )
      VALUES
      (
		@IdTransportista,
        @Rut,
        @Dv,
        @Nombre,
        @Paterno,
        @Materno,
        @Nacionalidad,
        @estadoCivil,
        @sexo,
        @email,
        @telefono,
        @telefono2,
        @fechaNacimiento,
        @direccion,
        @fechaIngreso,
        @fechaHoraActual,
        @fechaHoraActual,
        @Estado,
        @codTelefono,
        @codTelefono2,
        @nivelEstudio,
        @nacionalidadOtra,
        @idComuna
      )
      SET @Status = 'ingresado'
      SET @IdConductor = SCOPE_IDENTITY()

    END ELSE IF (@existeRut = -1) BEGIN
      SET @Status = 'duplicado'
    END ELSE BEGIN
      SET @Status = 'error'
    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
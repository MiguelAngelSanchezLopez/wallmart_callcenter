﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_ClasificacionConductor_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_ClasificacionConductor_ObtenerDetalle'
  DROP  Procedure  dbo.spu_ClasificacionConductor_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_ClasificacionConductor_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_ClasificacionConductor_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle de la clasificacion Conductor consultado
**    Por          : VSR, 25/04/2016
*******************************************************************************/
@IdClasificacionConductor AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  ----------------------------------------------------------------
  -- TABLA 0: DETALLE
  ----------------------------------------------------------------
  SELECT
    IdClasificacionConductor = CC.Id,
    NombreClasificacionConductor = ISNULL(CC.Nombre,''),
    TieneRegistrosAsociados = 0
    /*
                              CASE WHEN (
                                (SELECT COUNT(aux.IdAlertaDefinicion) FROM AlertaDefinicionPorFormato AS aux WHERE aux.IdAlertaDefinicion = AD.Id)
                              ) = 0 THEN '0' ELSE '1' END
    */
  FROM
    ClasificacionConductor AS CC
  WHERE
    CC.Id = @IdClasificacionConductor
  ORDER BY
    IdClasificacionConductor DESC

END



   
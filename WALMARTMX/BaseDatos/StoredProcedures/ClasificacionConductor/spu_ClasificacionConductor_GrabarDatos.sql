﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_ClasificacionConductor_GrabarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_ClasificacionConductor_GrabarDatos'
  DROP  Procedure  dbo.spu_ClasificacionConductor_GrabarDatos
END

GO

PRINT  'Creating Procedure spu_ClasificacionConductor_GrabarDatos'
GO
CREATE Procedure dbo.spu_ClasificacionConductor_GrabarDatos
/******************************************************************************
**  Descripcion  : graba una clasificacion Conductor
**  Fecha        : 25/04/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdClasificacionConductorOutput AS INT OUTPUT,
@IdClasificacionConductor AS INT,
@Nombre AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    -- ingresa nuevo registro
    IF (@IdClasificacionConductor = -1) BEGIN
      INSERT INTO ClasificacionConductor
      (
	      Nombre
      )
      VALUES
      (
	      @Nombre
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdClasificacionConductorOutput = @IdClasificacionConductor
        RETURN
      END

      SET @Status = 'ingresado'
      SET @IdClasificacionConductorOutput = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE ClasificacionConductor
      SET
	      Nombre = @Nombre
      WHERE
        Id = @IdClasificacionConductor

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdClasificacionConductorOutput = @IdClasificacionConductor
        RETURN
      END

      SET @Status = 'modificado'
      SET @IdClasificacionConductorOutput = @IdClasificacionConductor

    END

  COMMIT
END
GO        
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_ClasificacionConductor_ObtenerListadoConFiltro')
BEGIN
  PRINT  'Dropping Procedure spu_ClasificacionConductor_ObtenerListadoConFiltro'
  DROP  Procedure  dbo.spu_ClasificacionConductor_ObtenerListadoConFiltro
END

GO

PRINT  'Creating Procedure spu_ClasificacionConductor_ObtenerListadoConFiltro'
GO
CREATE Procedure dbo.spu_ClasificacionConductor_ObtenerListadoConFiltro
/******************************************************************************
**    Descripcion  : Obtiene listado de clasificacion Conductor usando filtros de busqueda
**    Por          : VSR, 25/04/2016
*******************************************************************************/
@IdUsuario AS INT,
@Nombre AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    IdClasificacionConductor = CC.Id,
    NombreClasificacionConductor = ISNULL(CC.Nombre,''),
    TieneRegistrosAsociados = 0
    /*
                              CASE WHEN (
                                (SELECT COUNT(aux.IdAlertaDefinicion) FROM AlertaDefinicionPorFormato AS aux WHERE aux.IdAlertaDefinicion = AD.Id)
                              ) = 0 THEN '0' ELSE '1' END
    */
  FROM
    ClasificacionConductor AS CC
  WHERE
    (@Nombre = '-1' OR CC.Nombre LIKE '%'+ @Nombre +'%')
  ORDER BY
    IdClasificacionConductor DESC

END



  
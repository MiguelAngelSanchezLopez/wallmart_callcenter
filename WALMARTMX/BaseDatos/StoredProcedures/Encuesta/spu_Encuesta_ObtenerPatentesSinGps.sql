﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Encuesta_ObtenerPatentesSinGps')
BEGIN
  PRINT  'Dropping Procedure spu_Encuesta_ObtenerPatentesSinGps'
  DROP  Procedure  dbo.spu_Encuesta_ObtenerPatentesSinGps
END

GO

PRINT  'Creating Procedure spu_Encuesta_ObtenerPatentesSinGps'
GO
CREATE Procedure dbo.spu_Encuesta_ObtenerPatentesSinGps
/******************************************************************************
**    Descripcion  : Obtiene las patentes sin gps de los transportistas
**    Autor        : DG
**    Fecha        : 24/06/2015
*******************************************************************************/
@id AS INTEGER
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	SELECT 
		 Id
		,Patente
	FROM EncuestaPatentesSinGps 
	WHERE IdTransportista = @id
END
GO 
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Encuesta_ObtenerRespuestas')
BEGIN
  PRINT  'Dropping Procedure spu_Encuesta_ObtenerRespuestas'
  DROP  Procedure  dbo.spu_Encuesta_ObtenerRespuestas
END

GO

PRINT  'Creating Procedure spu_Encuesta_ObtenerRespuestas'
GO
CREATE Procedure dbo.spu_Encuesta_ObtenerRespuestas
/******************************************************************************
**    Descripcion  : Obtiene las respuesta de los transportistas
**    Autor        : DG
**    Fecha        : 24/06/2015
*******************************************************************************/
@id AS INTEGER
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON

	SELECT 
		 Id
		,NumCategoria
		,NumRespuesta
		,Si
		,No
		,Na
		,IdEncuestaProveedor
		,IdTransportista 
	FROM EncuestaRespuesta 
	WHERE IdTransportista = @id
END
GO 
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Encuesta_GrabarPatenteSinGps')
BEGIN
  PRINT  'Dropping Procedure spu_Encuesta_GrabarPatenteSinGps'
  DROP  Procedure  dbo.spu_Encuesta_GrabarPatenteSinGps
END

GO

PRINT  'Creating Procedure spu_Encuesta_GrabarPatenteSinGps'
GO
CREATE Procedure [dbo].[spu_Encuesta_GrabarPatenteSinGps]
/******************************************************************************
**  Descripcion  : graba patente
**  Fecha        : 05/06/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdTransportista AS INT,
@Patente AS VARCHAR(20)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
  
	 INSERT INTO EncuestaPatentesSinGps
	 (
 		IdTransportista,
 		Patente
	 )
	 VALUES
	 (
	 	@IdTransportista,
 		@Patente
	 )

	SET @Status	= SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
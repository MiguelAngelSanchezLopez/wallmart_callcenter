﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Discrepancia_BuscadorSinAlertas')
BEGIN
  PRINT  'Dropping Procedure spu_Discrepancia_BuscadorSinAlertas'
  DROP  Procedure  dbo.spu_Discrepancia_BuscadorSinAlertas
END

GO

PRINT  'Creating Procedure spu_Discrepancia_BuscadorSinAlertas'
GO
CREATE Procedure dbo.spu_Discrepancia_BuscadorSinAlertas
/******************************************************************************
**    Descripcion  : obtiene listado de discrepancias que no tuvieron alertas durante el viaje
**    Por          : VSR, 19/08/2015
*******************************************************************************/
@IdUsuario AS INT,
@CodigoLocal AS INT,
@Seccion AS VARCHAR(255),
@CodigoCentroDistribucion AS INT,
@DM AS BIGINT,
@FechaContableDesde AS VARCHAR(255),
@FechaContableHasta AS VARCHAR(255),
@ResolucionFinal AS VARCHAR(255),
@NroTransporte AS INT,
@DiferenciaDias AS INT,
@SoloFocoTablet AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  -- si filtra por NroTransporte o DM entonces busca en toda la historia
  IF (@NroTransporte <> -1 OR @DM <> -1) BEGIN
    IF (@FechaContableDesde = '-1') SET @FechaContableDesde = '01/01/2014'
    IF (@FechaContableHasta = '-1') SET @FechaContableHasta = '31/12/2100'
  END ELSE BEGIN
    -- si no hay filtro de fechas entonces se toma hasta X dias atras
    IF (@FechaContableDesde = '-1') SET @FechaContableDesde = CONVERT(VARCHAR,DATEADD(dd,-@DiferenciaDias,dbo.fnu_GETDATE()),103)
    IF (@FechaContableHasta = '-1') SET @FechaContableHasta = CONVERT(VARCHAR,dbo.fnu_GETDATE(),103)
  END

  SET @FechaContableDesde = @FechaContableDesde + ' 00:00'
  SET @FechaContableHasta = @FechaContableHasta + ' 23:59'

  --------------------------------------------------------------------
  -- obtiene los reclamos que no tuvieron alertas
  --------------------------------------------------------------------
  DECLARE @TablaDiscrepanciasSinAlertas AS TABLE(LocalDestino INT, NroTransporte INT)
  INSERT INTO @TablaDiscrepanciasSinAlertas(LocalDestino, NroTransporte)
  SELECT
    T.LocalDestino,
    T.NroTransporte
  FROM (
        SELECT
          D.LocalDestino,
          D.NroTransporte,
          Total = COUNT(DISTINCT A.ID)
        FROM
          Discrepancia AS D
          LEFT HASH JOIN Alerta AS A ON (D.NroTransporte = A.NroTransporte AND D.LocalDestino = A.LocalDestino)
          LEFT JOIN CallCenterAlerta AS CCA ON (A.Id = CCA.IdAlerta)
        WHERE
          (ISNULL(D.NroTransporte,0) > 0)
        GROUP BY
          D.LocalDestino, D.NroTransporte
  ) AS T
  WHERE
    T.Total = 0

  --------------------------------------------------------------------
  -- obtiene los motivos por lo que no genero alerta
  --------------------------------------------------------------------
  DECLARE @TablaMotivoSinAlertas AS TABLE(LocalDestino INT, NroTransporte INT, Motivo VARCHAR(255))

  -- obtiene los motivos: SIN ESTADO RUTA
  INSERT INTO @TablaMotivoSinAlertas(LocalDestino, NroTransporte, Motivo)
  SELECT DISTINCT
    T.LocalDestino,
    T.NroTransporte,
    T.Motivo
  FROM (
        SELECT
          DSA.LocalDestino,
          DSA.NroTransporte,
          Motivo = CASE WHEN (SELECT COUNT(aux.Id) FROM TrazaViaje AS aux WHERE aux.LocalDestino = DSA.LocalDestino AND aux.NroTransporte = DSA.NroTransporte AND aux.EstadoViaje <> 'ASIGNADO') = 0 THEN 'Sin estado en ruta' ELSE '' END
        FROM
          @TablaDiscrepanciasSinAlertas AS DSA
  ) AS T
  WHERE
    T.Motivo <> ''

  /*
  -- obtiene los motivos: SIN INTEGRAR, NO EXISTE EN PLATAFORMA
  INSERT INTO @TablaMotivoSinAlertas(LocalDestino, NroTransporte, Motivo)
  SELECT DISTINCT
    T.LocalDestino,
    T.NroTransporte,
    T.Motivo
  FROM (
        SELECT DISTINCT
          T.LocalDestino,
          T.NroTransporte,
          Motivo = CASE 
                      WHEN (T.TieneTraza = 0 AND T.EstaEnDespacho = 0) THEN 'No existe en plataforma' 
                      WHEN (T.TieneTraza = 0 AND T.EstaEnDespacho = 1) THEN 'Sin integrar' 
                   ELSE '' END
        FROM (
              SELECT DISTINCT
                DSA.LocalDestino,
                DSA.NroTransporte,
                TieneTraza = CASE WHEN TV.Id IS NULL THEN 0 ELSE 1 END,
                EstaEnDespacho = CASE WHEN D.NroTransporte IS NULL THEN 0 ELSE 1 END
              FROM
                @TablaDiscrepanciasSinAlertas AS DSA
                LEFT HASH JOIN TrazaViaje AS TV ON (DSA.LocalDestino = TV.LocalDestino AND DSA.NroTransporte = TV.NroTransporte)
                LEFT HASH JOIN FRMDespachoWM D ON (DSA.LocalDestino = D.Local AND DSA.NroTransporte = D.NroTransporte)
        ) AS T
  ) AS T
  WHERE
    T.Motivo <> ''
  */
  
  -----------------------------------------------------------------------
  -- TABLA 0: obtiene los reclamos sin alertas
  -----------------------------------------------------------------------
  SELECT
    T.NroTransporte,
    T.DM,
    T.NombreLocal,
    T.CodigoLocal,
    T.FechaContable,
    MontoDiscrepancia = SUM(T.MontoDiscrepancia),
    T.Seccion
  FROM (
        SELECT
          NroTransporte = ISNULL(D.NroTransporte,-1),
          DM = ISNULL(D.DM,-1),
          NombreLocal = L.Nombre + ISNULL(' - ' + CONVERT(VARCHAR,L.CodigoInterno),''),
          CodigoLocal = ISNULL(L.CodigoInterno,-1),
          FechaContable = D.FechaContable,
          MontoDiscrepancia = ISNULL(SUM(CONVERT(NUMERIC,D.ImporteML)),0),
          Seccion = ISNULL(D.Seccion,'')
        FROM
          Discrepancia AS D
          INNER MERGE JOIN @TablaDiscrepanciasSinAlertas AS DSA ON (D.NroTransporte = DSA.NroTransporte AND D.LocalDestino = DSA.LocalDestino)
          INNER MERGE JOIN Local AS L ON (D.LocalDestino = L.CodigoInterno)
        WHERE
            ( @CodigoLocal = -1 OR L.CodigoInterno = @CodigoLocal )
        AND ( @Seccion = '-1' OR D.Seccion = @Seccion )
        AND ( @CodigoCentroDistribucion = -1 OR D.OrigenCD = @CodigoCentroDistribucion )
        AND ( @DM = -1 OR D.DM = @DM )
        AND ( D.FechaContable BETWEEN @FechaContableDesde AND @FechaContableHasta )
        AND ( @ResolucionFinal = '-1' OR D.ResolucionFinal = @ResolucionFinal)
        AND ( @NroTransporte = -1 OR D.NroTransporte = @NroTransporte)
        AND ( @SoloFocoTablet = 0 OR L.FocoTablet = @SoloFocoTablet)
        GROUP BY
          ISNULL(D.NroTransporte,-1), ISNULL(D.DM,-1), L.Nombre + ISNULL(' - ' + CONVERT(VARCHAR,L.CodigoInterno),''), ISNULL(L.CodigoInterno,-1), 
          D.FechaContable, ISNULL(D.Seccion,'')
  ) AS T
  GROUP BY
    T.NroTransporte, T.DM, T.NombreLocal, T.CodigoLocal, T.FechaContable, T.Seccion
  ORDER BY
    MontoDiscrepancia DESC

  -----------------------------------------------------------------------
  -- TABLA 1: obtiene los motivos de los reclamos que no generaron alerta
  -----------------------------------------------------------------------
  SELECT
    MSA.LocalDestino,
    MSA.NroTransporte,
    MSA.Motivo
  FROM 
    @TablaMotivoSinAlertas AS MSA
  ORDER BY 
    NroTransporte, LocalDestino

END   
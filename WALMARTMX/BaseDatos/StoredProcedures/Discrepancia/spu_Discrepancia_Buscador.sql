﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Discrepancia_Buscador')
BEGIN
  PRINT  'Dropping Procedure spu_Discrepancia_Buscador'
  DROP  Procedure  dbo.spu_Discrepancia_Buscador
END

GO

PRINT  'Creating Procedure spu_Discrepancia_Buscador'
GO
CREATE Procedure dbo.spu_Discrepancia_Buscador
/******************************************************************************
**    Descripcion  : obtiene listado de discrepancias
**    Por          : VSR, 18/08/2015
*******************************************************************************/
@IdUsuario AS INT,
@CodigoLocal AS INT,
@Seccion AS VARCHAR(255),
@CodigoCentroDistribucion AS INT,
@DM AS BIGINT,
@FechaContableDesde AS VARCHAR(255),
@FechaContableHasta AS VARCHAR(255),
@ResolucionFinal AS VARCHAR(255),
@NroTransporte AS INT,
@Carga AS VARCHAR(255),
@DiferenciaDias AS INT,
@SoloFocoTablet AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  -- si filtra por NroTransporte o DM entonces busca en toda la historia
  IF (@NroTransporte <> -1 OR @DM <> -1) BEGIN
    IF (@FechaContableDesde = '-1') SET @FechaContableDesde = '01/01/2014'
    IF (@FechaContableHasta = '-1') SET @FechaContableHasta = '31/12/2100'
  END ELSE BEGIN
    -- si no hay filtro de fechas entonces se toma hasta X dias atras
    IF (@FechaContableDesde = '-1') SET @FechaContableDesde = CONVERT(VARCHAR,DATEADD(dd,-@DiferenciaDias,dbo.fnu_GETDATE()),103)
    IF (@FechaContableHasta = '-1') SET @FechaContableHasta = CONVERT(VARCHAR,dbo.fnu_GETDATE(),103)
  END

  SET @FechaContableDesde = @FechaContableDesde + ' 00:00'
  SET @FechaContableHasta = @FechaContableHasta + ' 23:59'

  SELECT
    NroTransporte = ISNULL(D.NroTransporte,-1),
    DM = ISNULL(D.DM,-1),
    NombreLocal = L.Nombre + ISNULL(' - ' + CONVERT(VARCHAR,L.CodigoInterno),''),
    CodigoLocal = ISNULL(L.CodigoInterno,-1),
    FechaContable = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(D.FechaContable, 'FECHA_COMPLETA'),''),
    MontoDiscrepancia = ISNULL(SUM(CONVERT(NUMERIC,D.ImporteML)),0),
    Seccion = ISNULL(D.Seccion,'')
  FROM
    Discrepancia AS D
    INNER MERGE JOIN Local AS L ON (D.LocalDestino = L.CodigoInterno)
    LEFT JOIN (
                SELECT DISTINCT NroTransporte, LocalDestino, TipoViaje FROM TrazaViaje	
    ) AS TV ON (D.NroTransporte = TV.NroTransporte AND L.CodigoInterno = TV.LocalDestino)
  WHERE
      ( @CodigoLocal = -1 OR L.CodigoInterno = @CodigoLocal )
  AND ( @Seccion = '-1' OR D.Seccion = @Seccion )
  AND ( @CodigoCentroDistribucion = -1 OR D.OrigenCD = @CodigoCentroDistribucion )
  AND ( @DM = -1 OR D.DM = @DM )
  AND ( D.FechaContable BETWEEN @FechaContableDesde AND @FechaContableHasta )
  AND ( @ResolucionFinal = '-1' OR D.ResolucionFinal = @ResolucionFinal)
  AND ( @NroTransporte = -1 OR D.NroTransporte = @NroTransporte)
  AND ( @Carga = '-1' OR TV.TipoViaje = @Carga)
  AND ( @SoloFocoTablet = 0 OR L.FocoTablet = @SoloFocoTablet)
  GROUP BY
    ISNULL(D.NroTransporte,-1), ISNULL(D.DM,-1), L.Nombre + ISNULL(' - ' + CONVERT(VARCHAR,L.CodigoInterno),''), ISNULL(L.CodigoInterno,-1), 
    ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(D.FechaContable, 'FECHA_COMPLETA'),''), ISNULL(D.Seccion,'')
  ORDER BY
    DM
END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Discrepancia_ObtenerHistorialAlertas')
BEGIN
  PRINT  'Dropping Procedure spu_Discrepancia_ObtenerHistorialAlertas'
  DROP  Procedure  dbo.spu_Discrepancia_ObtenerHistorialAlertas
END

GO

PRINT  'Creating Procedure spu_Discrepancia_ObtenerHistorialAlertas'
GO
CREATE Procedure dbo.spu_Discrepancia_ObtenerHistorialAlertas
/******************************************************************************
**    Descripcion  : obtiene el historial de alertas
**    Por          : VSR, 18/08/2015
*******************************************************************************/
@NroTransporte AS INT,
@CodigoLocal AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  --------------------------------------------------------------------
  -- TABLA 0: Total alertas
  --------------------------------------------------------------------
  SELECT
    ALERTA = A.DescripcionAlerta,
    TOTAL = COUNT(A.DescripcionAlerta)
  FROM 
    Alerta AS A
    INNER JOIN CallCenterAlerta AS CCA ON (A.Id = CCA.IdAlerta)
  WHERE
      A.NroTransporte = @NroTransporte
  AND A.LocalDestino = @CodigoLocal
  GROUP BY
    A.DescripcionAlerta
  ORDER BY
    ALERTA  

  --------------------------------------------------------------------
  -- TABLA 1: detalle alertas
  --------------------------------------------------------------------
  SELECT
    IDALERTA = A.Id,
    [IDMASTER] = A.NroTransporte,
    ALERTA = A.DescripcionAlerta,
    [LINEA DE TRANSPORTE] = ISNULL(A.NombreTransportista,''),
    [RUT LINEA DE TRANSPORTE] = ISNULL(A.RutTransportista,''),
    [NOMBRE OPERADOR] = ISNULL(A.NombreConductor,''),
    [RUT OPERADOR] = ISNULL(A.RutConductor,''),
    [PLACA TRACTO] = ISNULL(A.PatenteTracto,''),
    [PLACA REMOLQUE] = ISNULL(A.PatenteTrailer,''),
    LATITUD = ISNULL(A.LatTracto,''),
    LONGITUD = ISNULL(A.LonTracto,''),
    [LINK MAPA] = ISNULL(A.AlertaMapa,''),
    [CEDIS ORIGEN] = ISNULL(CD.Nombre,''),
    [CEDIS ORIGEN CODIGO] = ISNULL(A.OrigenCodigo,-1),
    [TIENDA DESTINO] = ISNULL(L.CodigoInterno,-1),
    [DETERMINANTE DESTINO] = ISNULL(L.Nombre,''),
    [TIPO VIAJE] = ISNULL(TV.TipoViaje,''),
    [FECHA CREACION] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(A.FechaHoraCreacion, 'FECHA_COMPLETA'),
    [TIPO ALERTA] = ISNULL(A.TipoAlerta,''),
    [GESTIONADO POR CALLCENTER] = CASE WHEN EXISTS(SELECT TOP 1 aux.IdAlerta FROM CallCenterHistorialEscalamiento AS aux WHERE aux.IdAlerta = A.Id) THEN 'SI' ELSE 'NO' END,
    [FECHA ULTIMA GESTION ALERTA] = ISNULL((SELECT dbo.fnu_ConvertirDatetimeToDDMMYYYY(MAX(aux.Fecha), 'FECHA_COMPLETA') FROM CallCenterHistorialEscalamiento AS aux WHERE aux.IdAlerta = A.Id AND aux.IdEscalamientoPorAlerta IS NOT NULL),'')
  FROM
    Alerta AS A
    INNER JOIN CallCenterAlerta AS CCA ON (A.Id = CCA.IdAlerta)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    LEFT JOIN CentroDistribucion AS CD ON (A.OrigenCodigo = CD.CodigoInterno)
    LEFT JOIN (
                SELECT TOP 1
                  NroTransporte = TV.NroTransporte,
                  TipoViaje = ISNULL(TV.TipoViaje,'')
                FROM
                  TrazaViaje AS TV
                WHERE
                    (TV.NroTransporte = @NroTransporte)
                AND (TV.LocalDestino = @CodigoLocal)
    ) AS TV ON (A.NroTransporte = TV.NroTransporte)
  WHERE
      A.NroTransporte = @NroTransporte
  AND A.LocalDestino = @CodigoLocal
  ORDER BY
    [ALERTA]

END
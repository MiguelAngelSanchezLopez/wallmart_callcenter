﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Discrepancia_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Discrepancia_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Discrepancia_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Discrepancia_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Discrepancia_ObtenerDetalle
/******************************************************************************
**    Descripcion  : obtiene el detalle de la discrepancia
**    Por          : VSR, 18/08/2015
*******************************************************************************/
@NroTransporte AS NUMERIC,
@CodigoLocal AS NUMERIC
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  --------------------------------------------------------------------
  -- TABLA 0: Auditoria CD
  --------------------------------------------------------------------
  SELECT DISTINCT
    Fecha = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(CC.FECHACREACION,'FECHA_COMPLETA'),''),
    RealizadoPor = ISNULL(FCP.CLD,''),
    IdFormulario = ISNULL(FCP.IDENTIFIER,'-1')
  FROM 
    SMUFormCargaPagina2 AS FCP
    INNER JOIN SMUFormCargaCabecera AS CC ON (FCP.IDENTIFIER = CC.IDENTIFIER)
  WHERE
      FCP.LD = @CodigoLocal
  AND (
           FCP.NT = @NroTransporte
        OR FCP.OPCIO = @NroTransporte
      )

  --------------------------------------------------------------------
  -- TABLA 1: Recepcion local
  --------------------------------------------------------------------
  SELECT DISTINCT
    Fecha = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(RC.FECHACREACION,'FECHA_COMPLETA'),''),
    RealizadoPor = ISNULL(FRP.USERC,''),
    IdFormulario = ISNULL(FRP.IDENTIFIER,'-1')
  FROM 
    SMUFormRecepcionPagina2 AS FRP
    INNER JOIN SMUFormRecepcionCabecera AS RC ON (FRP.IDENTIFIER = RC.IDENTIFIER)
  WHERE
      FRP.LD = @CodigoLocal
  AND (
           FRP.NT = @NroTransporte
        OR FRP.NHR = @NroTransporte
        OR FRP.NT2 = @NroTransporte
        OR FRP.NTOPC = @NroTransporte
      )

  --------------------------------------------------------------------
  -- TABLA 2: Historial alertas
  --------------------------------------------------------------------
  SELECT
    NombreAlerta = A.DescripcionAlerta,
    Total = COUNT(A.DescripcionAlerta)
  FROM 
    Alerta AS A
    INNER JOIN CallCenterAlerta AS CCA ON (A.Id = CCA.IdAlerta)
  WHERE
      A.NroTransporte = @NroTransporte
  AND A.LocalDestino = @CodigoLocal
  GROUP BY
    A.DescripcionAlerta
  ORDER BY
    NombreAlerta  

  --------------------------------------------------------------------
  -- TABLA 3: Plan accion
  --------------------------------------------------------------------
  SELECT
    NombreEstado = ISNULL(TG.Nombre,'')
  FROM
    AlertaRojaEstadoActual AS AREA
    INNER JOIN EstadoTransportistaPorAlertaRojaEstadoActual AS ET ON (AREA.Id = ET.IdAlertaRojaEstadoActual)
    INNER JOIN TipoGeneral AS TG ON (ET.IdTipoGeneral = TG.Id)
  WHERE
      AREA.NroTransporte = @NroTransporte
  AND AREA.Estado = 'FINALIZADO'
  AND ET.Aprobada = 1
  ORDER BY
    NombreEstado

  --------------------------------------------------------------------
  -- TABLA 4: Estado actual transportista
  --------------------------------------------------------------------
  SELECT
    NroTransporte = AREA.NroTransporte,
    Observacion = ISNULL(AREA.ObservacionEstado,'')
  FROM
      AlertaRojaEstadoActual AREA
  WHERE
      AREA.NroTransporte = @NroTransporte
  AND AREA.Estado = 'FINALIZADO'
END   
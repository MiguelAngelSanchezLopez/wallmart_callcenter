IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_GrabarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_GrabarDatos'
  DROP  Procedure  dbo.spu_Pagina_GrabarDatos
END

GO

PRINT  'Creating Procedure spu_Pagina_GrabarDatos'
GO
CREATE Procedure dbo.spu_Pagina_GrabarDatos
/******************************************************************************
**  Descripcion  : graba una pagina
**  Fecha        : 15/09/2009
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdPagina AS INT OUTPUT,
@Titulo AS VARCHAR(50),
@NombreArchivo AS VARCHAR(1028),
@NombreCategoria AS VARCHAR(50),
@Estado AS INT,
@MostrarEnMenu AS INT,
@ListadoIdPerfiles AS VARCHAR(8000)

AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @idCategoria AS INT
    DECLARE @ultimoOrdenCategoria AS INT
    SET @IdPagina = -1

    -- graba pagina si corresponde
    IF(NOT EXISTS(SELECT Id FROM Pagina WHERE NombreArchivo = @NombreArchivo)) BEGIN
      ------------------------------------------------------------------------------------
      -- VERIFICA LOS DATOS DE LA CATEGORIA PARA CREAR UNA U OBTENER SU ID SI EXISTE
      ------------------------------------------------------------------------------------
      -- verifica si existe
      IF(@NombreCategoria='') BEGIN
        SET @idCategoria = NULL
      END
      ELSE BEGIN
        -- verifica si la categoria existe, si existe obtiene su id, sino lo agrega
        IF(EXISTS(SELECT Id FROM CategoriaPagina WHERE Nombre = @NombreCategoria)) BEGIN
          SELECT @idCategoria = Id FROM CategoriaPagina WHERE Nombre = @NombreCategoria
        END
        ELSE BEGIN
          SELECT @ultimoOrdenCategoria = MAX(Orden) FROM CategoriaPagina
          IF(@ultimoOrdenCategoria IS NULL) SET @ultimoOrdenCategoria = 0
          
          INSERT INTO CategoriaPagina(Nombre, Orden) VALUES(@NombreCategoria,@ultimoOrdenCategoria+1)
          SET @idCategoria = SCOPE_IDENTITY()
        END
      END

      ------------------------------------------------------------------------------------
      -- INSERTA LA NUEVA PAGINA
      ------------------------------------------------------------------------------------
      -- verifica que orden tiene la categoria en la encuesta, si no existe la crea
      IF(@idCategoria IS NOT NULL) BEGIN
        SELECT @ultimoOrdenCategoria = MAX(Orden) FROM Pagina WHERE IdCategoria = @IdCategoria
        IF(@ultimoOrdenCategoria IS NULL) SET @ultimoOrdenCategoria = 0
      END
      ELSE BEGIN
        SET @ultimoOrdenCategoria = 0
      END
      
      INSERT INTO Pagina
      (
	      Titulo,
	      NombreArchivo,
	      IdCategoria,
	      Orden,
	      Estado,
	      MostrarEnMenu
      )
      VALUES
      (
	      @Titulo,
	      @NombreArchivo,
	      @IdCategoria,
	      @ultimoOrdenCategoria+1,
	      @Estado,
	      @MostrarEnMenu
      )
      SET @Status = 'ingresado'
      SET @IdPagina = SCOPE_IDENTITY()

      ------------------------------------------------------------------------------------
      -- INSERTA PERFILES ASOCIADOS A LA PAGINA
      ------------------------------------------------------------------------------------
      IF(@ListadoIdPerfiles <> '') BEGIN
        INSERT INTO PaginasXPerfil
        (
	        IdPagina,
	        IdPerfil
        )
        SELECT
          @IdPagina,
          valor
        FROM
          dbo.fnu_InsertaListaTabla(@ListadoIdPerfiles)
      END

    END
    ELSE BEGIN
      SET @Status = 'duplicado'
    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO        
IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_VerificarEliminacionFuncion')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_VerificarEliminacionFuncion'
  DROP  Procedure  dbo.spu_Pagina_VerificarEliminacionFuncion
END

GO

PRINT  'Creating Procedure spu_Pagina_VerificarEliminacionFuncion'
GO
CREATE Procedure dbo.spu_Pagina_VerificarEliminacionFuncion
/******************************************************************************
**  Descripcion  : verifica si se puede eliminar una funcion
**  Fecha        : 15/09/2009
*******************************************************************************/
@Status AS INT OUTPUT,
@IdFuncion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @total AS INT
  SET @total = 0
  
  -- cuenta cuantos registros tiene asociados
  SELECT @total = COUNT(IdFuncionPagina) FROM FuncionesPaginaXUsuario WHERE IdFuncionPagina = @IdFuncion
  IF(@total IS NULL) SET @total = 0
  
  IF(@total = 0) BEGIN
   SET @Status = 0
  END
  ELSE BEGIN
   SET @Status = 1
  END

END
GO            
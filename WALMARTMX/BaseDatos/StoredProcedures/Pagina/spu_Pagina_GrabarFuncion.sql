IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_GrabarFuncion')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_GrabarFuncion'
  DROP  Procedure  dbo.spu_Pagina_GrabarFuncion
END

GO

PRINT  'Creating Procedure spu_Pagina_GrabarFuncion'
GO
CREATE Procedure dbo.spu_Pagina_GrabarFuncion
/******************************************************************************
**  Descripcion  : graba una pagina
**  Fecha        : 15/09/2009
*******************************************************************************/
@Status AS INT OUTPUT,
@IdPagina AS INT,
@IdFuncion AS INT,
@Nombre AS VARCHAR(100),
@Llave AS VARCHAR(100),
@Orden AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    -- INGRESA UNA NUEVA
    IF(@IdFuncion=-1) BEGIN
      INSERT INTO FuncionPagina
      (
	      IdPagina,
	      Nombre,
	      LlaveIdentificador,
	      Orden
      )
      VALUES
      (
	      @IdPagina,
	      @Nombre,
	      @Llave,
	      @Orden
      )
    END
    -- ACTUALIZA UNA YA EXISTENTE
    ELSE BEGIN
      UPDATE FuncionPagina
      SET
	      IdPagina = @IdPagina,
	      Nombre = @Nombre,
	      LlaveIdentificador = @Llave,
	      Orden = @Orden
      WHERE
        Id = @IdFuncion
    END

    -- retorna que todo estuvo bien
    SET @Status = 1

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO         
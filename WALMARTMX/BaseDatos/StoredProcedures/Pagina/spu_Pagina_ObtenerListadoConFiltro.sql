IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_ObtenerListadoConFiltro')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_ObtenerListadoConFiltro'
  DROP  Procedure  dbo.spu_Pagina_ObtenerListadoConFiltro
END

GO

PRINT  'Creating Procedure spu_Pagina_ObtenerListadoConFiltro'
GO
CREATE Procedure dbo.spu_Pagina_ObtenerListadoConFiltro
/******************************************************************************
**    Descripcion  : Obtiene listado de paginas usando filtros de busqueda
**    Por          : VSR, 15/09/2009
*******************************************************************************/
@IdUsuario  AS INT,
@Nombre     AS VARCHAR(150),
@Categoria  AS VARCHAR(50)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
	  IdPagina = P.Id,
	  Titulo = ISNULL(P.Titulo,''),
	  NombreArchivo = ISNULL(P.NombreArchivo,''),
	  IdCategoria = CP.Id,
	  NombreCategoria = ISNULL(CP.Nombre,''),
	  PerfilesAsociados = dbo.fnu_ObtenerListadoPerfilesPorPagina(P.Id,1)
  FROM
    Pagina AS P
    LEFT JOIN CategoriaPagina AS CP ON (P.IdCategoria = CP.Id)
  WHERE
      (@Nombre = '-1' OR P.Titulo LIKE '%'+ @Nombre +'%')
  AND (@Categoria = '-1' OR CP.Nombre LIKE '%'+ @Categoria +'%')
  ORDER BY
    IdPagina DESC

END



  
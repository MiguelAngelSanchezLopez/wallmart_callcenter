IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_EliminarFuncionesNoExistentes')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_EliminarFuncionesNoExistentes'
  DROP  Procedure  dbo.spu_Pagina_EliminarFuncionesNoExistentes
END

GO

PRINT  'Creating Procedure spu_Pagina_EliminarFuncionesNoExistentes'
GO
CREATE Procedure dbo.spu_Pagina_EliminarFuncionesNoExistentes
/******************************************************************************
**  Descripcion  : graba una pagina
**  Fecha        : 15/09/2009
*******************************************************************************/
@Status AS INT OUTPUT,
@IdPagina AS INT,
@ListadoIdFuncion AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM FuncionesPaginaxUsuario WHERE IdFuncionPagina IN( SELECT Id FROM FuncionPagina WHERE idPagina=@IdPagina AND Id NOT IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@ListadoIdFuncion)) )
    DELETE FROM FuncionPagina WHERE idPagina=@IdPagina AND Id NOT IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@ListadoIdFuncion))

    -- retorna que todo estuvo bien
    SET @Status = 1

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO          
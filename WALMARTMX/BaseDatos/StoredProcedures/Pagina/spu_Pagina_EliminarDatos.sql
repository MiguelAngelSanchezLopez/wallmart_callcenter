IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Pagina_EliminarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Pagina_EliminarDatos'
  DROP  Procedure  dbo.spu_Pagina_EliminarDatos
END

GO

PRINT  'Creating Procedure spu_Pagina_EliminarDatos'
GO
CREATE Procedure dbo.spu_Pagina_EliminarDatos
/******************************************************************************
**  Descripcion  : elimina una pagina
**  Fecha        : 15/09/2009
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdPagina AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM FuncionesPaginaXUsuario WHERE IdFuncionPagina IN (SELECT Id FROM FuncionPagina WHERE IdPagina=@IdPagina)
    DELETE FROM FuncionPagina WHERE IdPagina=@IdPagina
    DELETE FROM PaginasXPerfil WHERE IdPagina=@IdPagina
    DELETE FROM Pagina WHERE id=@IdPagina

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO           
﻿
 IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_ObtenerEstadoConexionTeleoperador')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_ObtenerEstadoConexionTeleoperador'
  DROP  Procedure  dbo.spu_Usuario_ObtenerEstadoConexionTeleoperador
END

GO

PRINT  'Creating Procedure spu_Usuario_ObtenerEstadoConexionTeleoperador'
GO
CREATE Procedure [dbo].[spu_Usuario_ObtenerEstadoConexionTeleoperador]
/******************************************************************************
**    Descripcion  : obtiene el estado de conexion de los teleoperadores
**    Por          : VSR, 07/11/2014
*******************************************************************************/
AS
BEGIN

  -----------------------------------------------------------------
  -- obtiene la ultima accion realizada por el usuario
  -----------------------------------------------------------------
  DECLARE @TablaUltimaAccion AS TABLE(IdUsuario INT, Fecha DATETIME, Accion VARCHAR(255))
  INSERT INTO @TablaUltimaAccion(IdUsuario, Fecha, Accion)
  SELECT
    T.IdUsuario,
    T.Fecha,
    LS.Accion
  FROM (
        SELECT
          IdUsuario = LS.IdUsuario,
          Fecha = MAX(LS.Fecha)
        FROM
          LogSesion AS LS
          INNER JOIN Usuario AS U ON (LS.IdUsuario = U.Id)
          INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
		      INNER JOIN UsuarioDatoExtra AS Ud ON (U.Id = Ud.IdUsuario)
        WHERE
            P.Llave = 'TO'
        AND U.Estado = 1
		    AND Ud.GestionarSoloCemtra <> 'SoloCemtra'
        AND LS.Accion NOT LIKE '%GPSTrack%'
        GROUP BY
          LS.IdUsuario
  ) AS T
  INNER JOIN LogSesion AS LS ON (T.IdUsuario = LS.IdUsuario AND T.Fecha = LS.Fecha)

  -----------------------------------------------------------------
  -- obtiene el ultimo inicio de sesion del usuario
  -----------------------------------------------------------------
  DECLARE @TablaUltimoInicioSesion AS TABLE(IdUsuario INT, Fecha DATETIME, Accion VARCHAR(255))
  INSERT INTO @TablaUltimoInicioSesion(IdUsuario, Fecha, Accion)
  SELECT
    T.IdUsuario,
    T.Fecha,
    LS.Accion
  FROM (
        SELECT
          IdUsuario = LS.IdUsuario,
          Fecha = MAX(LS.Fecha)
        FROM
          LogSesion AS LS
          INNER JOIN Usuario AS U ON (LS.IdUsuario = U.Id)
          INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
		      INNER JOIN UsuarioDatoExtra AS Ud ON (U.Id = Ud.IdUsuario)
        WHERE
            P.Llave = 'TO'
        AND U.Estado = 1
        AND LS.Accion = 'Inicia sesión'
		    AND Ud.GestionarSoloCemtra <> 'SoloCemtra'
        AND LS.Accion NOT LIKE '%GPSTrack%'
        GROUP BY
          LS.IdUsuario
  ) AS T
  INNER JOIN LogSesion AS LS ON (T.IdUsuario = LS.IdUsuario AND T.Fecha = LS.Fecha)

  -----------------------------------------------------------------
  -- obtiene la informacion del log del usuario
  -----------------------------------------------------------------
  DECLARE @TablaLog AS TABLE (IdUsuario INT, Username VARCHAR(255), NombreUsuario VARCHAR(255), Estado VARCHAR(255), UltimaAccion VARCHAR(255), UltimaFechaAccion VARCHAR(255),
                              SegundosUltimaAccionFecha NUMERIC, FechaUltimoInicioSesion VARCHAR(255), SegundosFechaUltimoInicioSesion NUMERIC)
  INSERT @TablaLog
  (
	  IdUsuario,
	  Username,
	  NombreUsuario,
	  Estado,
	  UltimaAccion,
	  UltimaFechaAccion,
	  SegundosUltimaAccionFecha,
	  FechaUltimoInicioSesion,
	  SegundosFechaUltimoInicioSesion
  )
  SELECT
    T.IdUsuario,
    T.Username,
    T.NombreUsuario,
    Estado = CASE 
              WHEN PATINDEX('LLAMANDO %', T.UltimaAccion) > 0 THEN 'LLAMANDO'
              WHEN PATINDEX('HABLANDO %', T.UltimaAccion) > 0 THEN 'HABLANDO'
              WHEN PATINDEX('Cierra sesión', T.UltimaAccion) > 0 THEN 'DESCONECTADO'
              WHEN PATINDEX('%TELEOPERADOR_AUXILIAR%', T.UltimaAccion) > 0 THEN 'AUXILIAR'
              WHEN T.UltimaAccion = '' THEN 'DESCONECTADO'
              ELSE 'DISPONIBLE'
            END,
    UltimaAccion = CASE
                    WHEN PATINDEX('%TELEOPERADOR_AUXILIAR%', T.UltimaAccion) > 0 THEN REPLACE(REPLACE(REPLACE(T.UltimaAccion,'Estado auxiliar: ',''),'TELEOPERADOR_AUXILIAR_',''),'_',' ')
                    ELSE T.UltimaAccion
                  END,
    UltimaFechaAccion = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.UltimaFechaAccion,'FECHA_COMPLETA'),''),
    SegundosUltimaAccionFecha = CASE WHEN T.UltimaFechaAccion IS NULL THEN -1 ELSE DATEDIFF(ss,T.UltimaFechaAccion,dbo.fnu_GETDATE()) END,
    FechaUltimoInicioSesion = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaUltimoInicioSesion,'FECHA_COMPLETA'),''),
    SegundosFechaUltimoInicioSesion = CASE WHEN T.FechaUltimoInicioSesion IS NULL THEN -1 ELSE DATEDIFF(ss,T.FechaUltimoInicioSesion,dbo.fnu_GETDATE()) END
  FROM (
        SELECT
          IdUsuario = U.Id,
          Username = U.Username,
          NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
          UltimaAccion = ISNULL(TUA.Accion,''),
          UltimaFechaAccion = TUA.Fecha,
          FechaUltimoInicioSesion = TUIA.Fecha
        FROM
          Usuario AS U
          INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
          LEFT JOIN @TablaUltimaAccion AS TUA ON (U.Id = TUA.IdUsuario)
          LEFT JOIN @TablaUltimoInicioSesion AS TUIA ON (U.Id = TUIA.IdUsuario)
		  INNER JOIN UsuarioDatoExtra AS Ud ON (U.Id = Ud.IdUsuario)
        WHERE
            P.Llave = 'TO'
        AND U.Estado = 1
		AND Ud.GestionarSoloCemtra <> 'SoloCemtra'
  ) AS T
  ORDER BY
    T.UltimaFechaAccion DESC

  -----------------------------------------------------------------------
  -- TABLA 0: Listado usuarios
  -----------------------------------------------------------------------
  SELECT
    T.*,
    Orden = CASE WHEN (T.Estado = 'DESCONECTADO') THEN 1 ELSE 0 END
  FROM
    @TablaLog AS T
  WHERE
    T.Estado NOT IN ('DESCONECTADO')
  ORDER BY
    Orden, T.NombreUsuario

  -----------------------------------------------------------------------
  -- TABLA 1: Alertas Pendientes de Gestionar
  -----------------------------------------------------------------------
  DECLARE @TotalRegistros AS INT
  SET @TotalRegistros = (SELECT COUNT(1) FROM alertaencolaatencion  WHERE NombreAlerta not IN ('EXCESO DE VELOCIDAD','PERDIDA SEÑAL','PERDIDA SEÑAL ZONA GRIS','DETENCION EN ZONA DE RIESGO'))

  select  
	NombreAlerta
	,Porcentaje = CASE WHEN  COUNT(NombreAlerta) = 0 THEN 0 ELSE
                  ROUND((CONVERT(FLOAT, COUNT(NombreAlerta)) / @TotalRegistros) * 100,2)
                 END   
  from alertaencolaatencion
  WHERE NombreAlerta not IN ('EXCESO DE VELOCIDAD','PERDIDA SEÑAL','PERDIDA SEÑAL ZONA GRIS','DETENCION EN ZONA DE RIESGO')
  GROUP BY
    NombreAlerta

  -----------------------------------------------------------------------
  -- TABLA 2: Alertas Atendidas
  -----------------------------------------------------------------------
  SET @TotalRegistros = (SELECT COUNT(1) FROM CallCenterHistorialEscalamiento inner join alerta a on idalerta = a.id where convert(varchar(10),fecha,103) = convert(varchar(10),dbo.fnu_getdate(),103) and a.descripcionalerta not IN ('EXCESO DE VELOCIDAD','PERDIDA SEÑAL','PERDIDA SEÑAL ZONA GRIS','DETENCION EN ZONA DE RIESGO'))
  --SET @TotalRegistros = (SELECT COUNT(1) FROM CallCenterHistorialEscalamiento inner join alerta a on idalerta = a.id where convert(varchar(10),fecha,103) = '09/02/2018' and descripcionalerta not IN ('EXCESO DE VELOCIDAD','PERDIDA SEÑAL','PERDIDA SEÑAL ZONA GRIS','DETENCION EN ZONA DE RIESGO'))

 select 
	Usuario = u.nombre
	,Porcentaje = CASE WHEN  COUNT(u.nombre) = 0 THEN 0 ELSE
                  ROUND((CONVERT(FLOAT, COUNT(u.nombre)) / @TotalRegistros) * 100,2)
                 END    
 from CallCenterHistorialEscalamiento che
 inner join usuario u on che.idUsuarioCreacion = u.id
 inner join alerta a on che.idalerta = a.id
 where 
	convert(varchar(10),fecha,103) = convert(varchar(10),dbo.fnu_getdate(),103)
	--convert(varchar(10),fecha,103) = '09/02/2018'
	and a.descripcionalerta not IN ('EXCESO DE VELOCIDAD','PERDIDA SEÑAL','PERDIDA SEÑAL ZONA GRIS','DETENCION EN ZONA DE RIESGO')
 group by  u.nombre

  -----------------------------------------------------------------------
  -- TABLA 3: Alertas pendientes por atender
  -----------------------------------------------------------------------
	select  
		NombreAlerta = t.descripcionAlerta
		,CantAlertasAtendidas = t.CantidadAtendidas
		,CantPorAtender = t.CantidadPorAtender
		,CantidadTotal = ( t.CantidadAtendidas +  t.CantidadPorAtender)
	from (
	  select  
		a.descripcionAlerta
		,CantidadAtendidas = count(cche.Id)
		,CantidadPorAtender = apa.Cantidad
	  from CallCenterHistorialEscalamiento cche
	  inner join Alerta a on cche.idAlerta = a.id
	  outer apply( --alertas por atender
		select Cantidad = count(1) 
		from alertaencolaatencion 
		where upper(nombrealerta) = upper(a.descripcionAlerta)
	  ) as apa
	  WHERE a.descripcionalerta not IN ('EXCESO DE VELOCIDAD','PERDIDA SEÑAL','PERDIDA SEÑAL ZONA GRIS','DETENCION EN ZONA DE RIESGO')
	  GROUP BY
		descripcionAlerta, apa.Cantidad
	) as t

	-----------------------------------------------------------------------
	-- TABLA 4: Alertas atentidas por usuario
	-----------------------------------------------------------------------
	 select 
		Usuario = u.nombre + ' ' + isnull(u.paterno,'')
		,Cantidad = count(1)
	 from CallCenterHistorialEscalamiento che
	 inner join usuario u on che.idUsuarioCreacion = u.id	 
	 inner join alerta a on che.idalerta = a.id
	 where a.descripcionalerta not IN ('EXCESO DE VELOCIDAD','PERDIDA SEÑAL','PERDIDA SEÑAL ZONA GRIS','DETENCION EN ZONA DE RIESGO')
	 group by  u.nombre,u.paterno

END

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_ObtenerPorId'
  DROP  Procedure  dbo.spu_Usuario_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_Usuario_ObtenerPorId'
GO
CREATE Procedure dbo.spu_Usuario_ObtenerPorId
/******************************************************************************
**  Descripcion  : obtiene informacion por su id
**  Fecha        : 23/07/2009
*******************************************************************************/
@Id AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    U.*,
    ClasificacionConductor = ISNULL(UDE.ClasificacionConductor,''),
	  idUsuarioTransportista = ISNULL(UDE.idUsuarioTransportista,0),
    PasswordCaduco = CASE WHEN U.FechaCaducidadPassword IS NULL THEN 1 ELSE
                       CASE WHEN dbo.fnu_GETDATE() > U.FechaCaducidadPassword THEN 1 ELSE 0 END
                     END
  FROM
    Usuario AS U
    LEFT JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
  WHERE
    U.Id = @Id

END
GO   
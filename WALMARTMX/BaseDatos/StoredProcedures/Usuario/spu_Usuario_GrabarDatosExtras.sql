﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_GrabarDatosExtras')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_GrabarDatosExtras'
  DROP  Procedure  dbo.spu_Usuario_GrabarDatosExtras
END

GO

PRINT  'Creating Procedure spu_Usuario_GrabarDatosExtras'
GO
CREATE Procedure dbo.spu_Usuario_GrabarDatosExtras
/******************************************************************************
**  Descripcion  : graba datos extras asociados al usuario
**  Fecha        : VSR, 07/10/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT,
@EmailConCopia AS TEXT,
@NotificarPorEmailHorarioNocturno AS INT,
@IdUsuarioTransportista AS INT,
@GestionarSoloCemtra AS VARCHAR(255),
@TransportistasAsociados AS VARCHAR(4000),
@CentroDistribucionAsociados AS VARCHAR(8000),
@LocalesAsociados AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @totalRegistros INT
    SELECT @totalRegistros = COUNT(IdUsuario) FROM UsuarioDatoExtra WHERE IdUsuario = @IdUsuario
    IF (@totalRegistros IS NULL) SET @totalRegistros = 0
    
    SET @Status = @IdUsuario

    IF (@IdUsuarioTransportista = -1) SET @IdUsuarioTransportista = NULL

    IF (@totalRegistros = 0) BEGIN
      INSERT INTO UsuarioDatoExtra
      (
	      IdUsuario,
	      EmailConCopia,
        NotificarPorEmailHorarioNocturno,
        IdUsuarioTransportista,
        GestionarSoloCemtra,
        TransportistasAsociados
      )
      VALUES
      (
	      @IdUsuario,
	      @EmailConCopia,
        @NotificarPorEmailHorarioNocturno,
        @IdUsuarioTransportista,
        @GestionarSoloCemtra,
        @TransportistasAsociados
      )
      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END
    END ELSE BEGIN
      UPDATE UsuarioDatoExtra
      SET
	      EmailConCopia = @EmailConCopia,
        NotificarPorEmailHorarioNocturno = @NotificarPorEmailHorarioNocturno,
        IdUsuarioTransportista = @IdUsuarioTransportista,
        GestionarSoloCemtra = @GestionarSoloCemtra,
        TransportistasAsociados = @TransportistasAsociados
      WHERE
        IdUsuario = @IdUsuario

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

    END

    ----------------------------------------------------------------
    -- elimina todas las referencias de usuarios y carga los nuevos
    ----------------------------------------------------------------
    DELETE FROM CentroDistribucionPorUsuario WHERE IdUsuario = @IdUsuario
    IF (@CentroDistribucionAsociados <> '') BEGIN
      INSERT INTO CentroDistribucionPorUsuario(IdUsuario, IdCentroDistribucion)
      SELECT @IdUsuario, valor FROM dbo.fnu_InsertaListaTabla(@CentroDistribucionAsociados)
    END

    DELETE FROM LocalesPorUsuario WHERE IdUsuario = @IdUsuario
    IF (@LocalesAsociados <> '') BEGIN
      INSERT INTO LocalesPorUsuario(IdUsuario, IdLocal)
      SELECT @IdUsuario, valor FROM dbo.fnu_InsertaListaTabla(@LocalesAsociados)
    END

  COMMIT
END
GO       
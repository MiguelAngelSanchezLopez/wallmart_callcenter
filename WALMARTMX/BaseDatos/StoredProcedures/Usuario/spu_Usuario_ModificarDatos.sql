 IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_ModificarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_ModificarDatos'
  DROP  Procedure  dbo.spu_Usuario_ModificarDatos
END

GO

PRINT  'Creating Procedure spu_Usuario_ModificarDatos'
GO
CREATE Procedure dbo.spu_Usuario_ModificarDatos
/******************************************************************************
**  Descripcion  : modifica un usuario
**  Fecha        : 27/07/2009
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdUsuario AS INT,
@Rut AS VARCHAR(255),
@DV AS VARCHAR(1),
@Nombre AS VARCHAR(50),
@Paterno AS VARCHAR(50),
@Materno AS VARCHAR(50),
@Username AS VARCHAR(50),
@Password AS VARCHAR(50),
@Email AS VARCHAR(50),
@Telefono AS VARCHAR(4000),
@LoginDias AS VARCHAR(7),
@Super AS INT,
@Estado AS INT,
@IdPerfil AS INT,
@ListadoPermisos AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @existeRut AS INT
  DECLARE @existeUsername AS INT
  DECLARE @MesesCaducaClave AS INT, @FechaCaducidadPassword DATETIME
  SET @MesesCaducaClave = (SELECT TOP 1 CONVERT(INT, Valor) FROM Configuracion WHERE Clave = 'webMesesCaducaClave')

  -- verifica si el Rut existe
  EXEC dbo.spu_Usuario_VerificarDuplicidadRut @existeRut OUTPUT, @IdUsuario, @RUT
  -- verifica si el Username existe
  EXEC dbo.spu_Usuario_VerificarDuplicidadUsername @existeUsername OUTPUT, @IdUsuario, @Username

  -- formatea valores
  IF(@IdPerfil = 0) SET @IdPerfil=NULL

  -- obtiene la antigua password por si no se modifica la mantenga
  IF(@Password = '') BEGIN
    SELECT @Password = [Password], @FechaCaducidadPassword = FechaCaducidadPassword FROM Usuario WHERE Id = @IdUsuario
  END ELSE BEGIN
    SET @FechaCaducidadPassword = CONVERT(DATETIME, CONVERT(VARCHAR, DATEADD(mm, @MesesCaducaClave, dbo.fnu_GETDATE()), 112) + ' 23:59:59.997')
  END

  BEGIN TRANSACTION
    -- actualiza Usuario si corresponde
    IF(@existeRut = 1 AND @existeUsername = 1)BEGIN
      UPDATE Usuario
      SET
        Rut = @Rut,
        Dv = @Dv,
        Nombre = @Nombre,
        Paterno = @Paterno,
        Materno = @Materno,
        Username = @Username,
        Password = @Password,
        Email = @Email,
        Telefono = @Telefono,
        LoginDias = @LoginDias,
        Super = @Super,
        Estado = @Estado,
        FechaHoraModificacion = dbo.fnu_ObtenerFechaISO(),
        IdPerfil = @IdPerfil,
        FechaCaducidadPassword = @FechaCaducidadPassword
      WHERE
        Id = @IdUsuario

      --elimina todos los permisos asociados al usuario
      DELETE FROM FuncionesPaginaXUsuario WHERE IdUsuario = @IdUsuario

      -- inserta los nuevos permisos
      INSERT INTO FuncionesPaginaXUsuario
      (
	      IdFuncionPagina,
	      IdUsuario
      )
      SELECT
        valor,
        @IdUsuario
      FROM
        dbo.fnu_InsertaListaTabla(@ListadoPermisos)

      ------------------------------------------------------
      -- grabar todas las funciones "VER" que no han sido asignadas en las paginas donde tiene permiso
      ------------------------------------------------------
      INSERT INTO FuncionesPaginaXUsuario
      (
	      IdFuncionPagina,
	      IdUsuario
      )
      SELECT
        T.IdFuncion,
        @IdUsuario
      FROM (
           -- obtiene todas las funciones "VER" de las paginas donde el usuario tiene permisos asignados y marca las que estan seleccionadas
            SELECT
              IdFuncion = FP.Id,
              NombreFuncion = FP.Nombre,
              Asignada = CASE WHEN (SELECT COUNT(aux.IdFuncionPagina) FROM FuncionesPaginaXUsuario AS aux WHERE aux.IdFuncionPagina = FP.Id) > 0 THEN 1 ELSE 0 END
            FROM
              FuncionPagina AS FP
              INNER JOIN (
        	                -- obtiene las paginas de las funciones asignadas del usuario
                          SELECT DISTINCT
                            IdPagina = FP.IdPagina
                          FROM
                            FuncionesPaginaXUsuario AS FPxU
                            INNER JOIN FuncionPagina AS FP ON (FPxU.IdFuncionPagina = FP.Id)
                          WHERE
                            FPxU.IdUsuario = @IdUsuario
              ) AS T ON (FP.IdPagina = T.IdPagina)
            WHERE
              FP.Nombre = 'VER'  	
      ) AS T
      WHERE
        T.Asignada = 0

      SET @Status = 'modificado'
    END ELSE IF (@existeRut = 1 AND @existeUsername = 1)BEGIN
      SET @Status = 'duplicado'
    END ELSE BEGIN
      SET @Status = 'error'
    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO       
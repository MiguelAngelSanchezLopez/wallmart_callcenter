﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_GrabarTelefonoPorUsuario')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_GrabarTelefonoPorUsuario'
  DROP  Procedure  dbo.spu_Usuario_GrabarTelefonoPorUsuario
END

GO

PRINT  'Creating Procedure spu_Usuario_GrabarTelefonoPorUsuario'
GO
CREATE Procedure dbo.spu_Usuario_GrabarTelefonoPorUsuario
/******************************************************************************
**  Descripcion  : graba los telefonos asociados al usuario
**  Fecha        : VSR, 29/04/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdTelefonoPorUsuario AS INT,
@IdUsuario AS INT,
@Telefono AS VARCHAR(255),
@HoraInicio AS VARCHAR(255),
@HoraTermino AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    IF (@IdTelefonoPorUsuario = -1) BEGIN
      INSERT INTO TelefonosPorUsuario
      (
	      IdUsuario,
	      Telefono,
	      HoraInicio,
	      HoraTermino
      )
      VALUES
      (
	      @IdUsuario,
	      @Telefono,
	      @HoraInicio,
	      @HoraTermino
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE TelefonosPorUsuario
      SET
	      IdUsuario = @IdUsuario,
	      Telefono = @Telefono,
	      HoraInicio = @HoraInicio,
	      HoraTermino = @HoraTermino
      WHERE
        Id = @IdTelefonoPorUsuario

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = @IdTelefonoPorUsuario

    END 

  COMMIT
END
GO       
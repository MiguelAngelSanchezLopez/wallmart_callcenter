 IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_ObtenerMatrizPermisos')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_ObtenerMatrizPermisos'
  DROP  Procedure  dbo.spu_Usuario_ObtenerMatrizPermisos
END

GO

PRINT  'Creating Procedure spu_Usuario_ObtenerMatrizPermisos'
GO
CREATE Procedure dbo.spu_Usuario_ObtenerMatrizPermisos
/******************************************************************************
**    Descripcion  : Obtiene matriz de permisos asociados al usuario
**    Por          : VSR, 30/07/2009
*******************************************************************************/
@IdUsuario AS INT,
@EsSuper AS BIT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @TablaFuncion AS TABLE(IdCategoria INT, Categoria VARCHAR(255), OrdenCategoria INT, IdPagina INT, TituloPagina VARCHAR(255), NombreArchivo VARCHAR(255),
                                MostrarEnMenu BIT, OrdenPagina INT, IdFuncionPagina INT, NombreFuncion VARCHAR(255), LlaveIdentificador VARCHAR(255), OrdenFuncion INT)

  IF(@EsSuper = 0) BEGIN
      INSERT INTO @TablaFuncion (IdCategoria, Categoria, OrdenCategoria, IdPagina, TituloPagina, NombreArchivo, MostrarEnMenu, OrdenPagina, IdFuncionPagina, NombreFuncion, LlaveIdentificador, OrdenFuncion)
      SELECT DISTINCT
	      IdCategoria = VWP.IdCategoria,
	      Categoria = VWP.Categoria,
	      OrdenCategoria = VWP.OrdenCategoria,
        IdPagina = VWP.IdPagina,
        TituloPagina = VWP.TituloPagina,
        NombreArchivo = VWP.NombreArchivo,
        MostrarEnMenu = VWP.MostrarEnMenu,
        OrdenPagina = VWP.OrdenPagina,
	      IdFuncionPagina = VWP.IdFuncionPagina,
	      NombreFuncion = VWP.NombreFuncion,
	      LlaveIdentificador = VWP.LlaveIdentificador,
	      OrdenFuncion = VWP.OrdenFuncion
      FROM
	      vwu_Permisos AS VWP
      WHERE
          (VWP.IdUsuario = @IdUsuario)
      AND (VWP.EstadoPagina = 1)
  END
  ELSE BEGIN
      INSERT INTO @TablaFuncion (IdCategoria, Categoria, OrdenCategoria, IdPagina, TituloPagina, NombreArchivo, MostrarEnMenu, OrdenPagina, IdFuncionPagina, NombreFuncion, LlaveIdentificador, OrdenFuncion)
      SELECT DISTINCT
        IdCategoria = CP.Id,
        Categoria = CP.Nombre,
        OrdenCategoria = CP.Orden,
        IdPagina = P.Id,
        TituloPagina = P.Titulo,
        NombreArchivo = P.NombreArchivo,
        MostrarEnMenu = P.MostrarEnMenu,
        OrdenPagina = P.Orden,
        IdFuncionPagina = ISNULL(FP.Id,0),
        NombreFuncion = ISNULL(FP.Nombre,''),
        LlaveIdentificador = ISNULL(FP.LlaveIdentificador,''),
        OrdenFuncion = ISNULL(FP.Orden,0)
      FROM
        Pagina AS P
        INNER MERGE JOIN CategoriaPagina AS CP ON (P.IdCategoria = CP.Id)
        LEFT JOIN FuncionPagina AS FP ON (P.Id = FP.IdPagina)    
      WHERE
        P.Estado = 1
  END

    /*****************************************************************************
    * TABLA 0 = LISTADO CATEGORIAS
    *****************************************************************************/
    SELECT DISTINCT
	    IdCategoria = TF.IdCategoria,
	    Categoria = TF.Categoria,
	    OrdenCategoria = TF.OrdenCategoria,
      TotalPaginasEnMenu = (SELECT COUNT(DISTINCT aux.IdPagina) FROM @TablaFuncion AS aux WHERE aux.IdCategoria = TF.IdCategoria AND aux.MostrarEnMenu = 1)
    FROM
	    @TablaFuncion AS TF
    ORDER BY
      OrdenCategoria
        
    /*****************************************************************************
    * TABLA 1 = LISTADO PAGINAS
    *****************************************************************************/
    SELECT DISTINCT
	    IdCategoria = TF.IdCategoria,
	    Categoria = TF.Categoria,
	    OrdenCategoria = TF.OrdenCategoria,
      IdPagina = TF.IdPagina,
      TituloPagina = TF.TituloPagina,
      NombreArchivo = TF.NombreArchivo,
      MostrarEnMenu = TF.MostrarEnMenu,
      OrdenPagina = TF.OrdenPagina
    FROM
	    @TablaFuncion AS TF
    ORDER BY
      OrdenCategoria, OrdenPagina

    /*****************************************************************************
    * TABLA 2 = LISTADO FUNCIONES
    *****************************************************************************/
    SELECT DISTINCT
	    IdCategoria = TF.IdCategoria,
	    Categoria = TF.Categoria,
	    OrdenCategoria = TF.OrdenCategoria,
      IdPagina = TF.IdPagina,
      TituloPagina = TF.TituloPagina,
      NombreArchivo = TF.NombreArchivo,
      MostrarEnMenu = TF.MostrarEnMenu,
      OrdenPagina = TF.OrdenPagina,
	    IdFuncionPagina = TF.IdFuncionPagina,
	    NombreFuncion = TF.NombreFuncion,
	    LlaveIdentificador = TF.LlaveIdentificador,
	    OrdenFuncion = TF.OrdenFuncion
    FROM
      @TablaFuncion AS TF
    ORDER BY
      OrdenCategoria, OrdenPagina, OrdenFuncion



END



 
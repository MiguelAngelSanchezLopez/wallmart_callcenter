 IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_Actualizar')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_Actualizar'
  DROP  Procedure  dbo.spu_Usuario_Actualizar
END

GO

PRINT  'Creating Procedure spu_Usuario_Actualizar'
GO
CREATE Procedure dbo.spu_Usuario_Actualizar
/******************************************************************************
**  Descripcion  : actualiza registro
**  Fecha        : 17/03/2009
*******************************************************************************/
@Id int,  
@Username varchar(50),  
@Password varchar(50),  
@Rut varchar(20),
@Dv varchar(1),
@Nombre varchar(50),  
@Paterno varchar(50),  
@Materno varchar(50),  
@Email varchar(50),  
@Telefono varchar(50),  
@IdDireccion int,  
@FechaHoraUltimoAcceso varchar(12),
@IpUltimoAcceso varchar(15),
@FechaHoraCreacion varchar(12),
@FechaHoraModificacion varchar(12),
@LoginDias AS varchar(7),
@Super AS bit,  
@Estado AS int,
@IdPerfil AS int
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @MesesCaducaClave AS INT, @FechaCaducidadPassword DATETIME
  SET @MesesCaducaClave = (SELECT TOP 1 CONVERT(INT, Valor) FROM Configuracion WHERE Clave = 'webMesesCaducaClave')
  SET @FechaCaducidadPassword = CONVERT(DATETIME, CONVERT(VARCHAR, DATEADD(mm, @MesesCaducaClave, dbo.fnu_GETDATE()), 112) + ' 23:59:59.997')

  IF(@IdPerfil = 0) SET @IdPerfil=NULL
  
  UPDATE Usuario
  SET
	  Username = @Username,
	  Password = @Password,
	  Nombre = @Nombre,
	  Paterno = @Paterno,
	  Materno = @Materno,
	  Email = @Email,
	  Telefono = @Telefono,
	  IdDireccion = @IdDireccion,
	  FechaHoraUltimoAcceso = @FechaHoraUltimoAcceso,
	  IpUltimoAcceso = @IpUltimoAcceso,
	  FechaHoraCreacion = @FechaHoraCreacion,
	  FechaHoraModificacion = @FechaHoraModificacion,
	  LoginDias = @LoginDias,
	  Super = @Super,
	  Estado = @Estado,
	  IdPerfil = @IdPerfil,
    FechaCaducidadPassword = CASE WHEN Password <> @Password THEN @FechaCaducidadPassword ELSE FechaCaducidadPassword END
  WHERE
    Id = @Id
END
GO  
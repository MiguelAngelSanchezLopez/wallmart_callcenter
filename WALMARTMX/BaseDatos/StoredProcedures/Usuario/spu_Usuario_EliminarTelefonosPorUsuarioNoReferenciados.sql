﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_EliminarTelefonosPorUsuarioNoReferenciados')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_EliminarTelefonosPorUsuarioNoReferenciados'
  DROP  Procedure  dbo.spu_Usuario_EliminarTelefonosPorUsuarioNoReferenciados
END

GO

PRINT  'Creating Procedure spu_Usuario_EliminarTelefonosPorUsuarioNoReferenciados'
GO
CREATE Procedure dbo.spu_Usuario_EliminarTelefonosPorUsuarioNoReferenciados
/******************************************************************************
**  Descripcion  : elimina los telefonos que no estan referenciados
**  Fecha        : 06/08/2014 
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT,
@Listado AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM TelefonosPorUsuario WHERE IdUsuario = @IdUsuario AND NOT Id IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    -- retorna que todo estuvo bien
    SET @Status = 1

  COMMIT
END
GO
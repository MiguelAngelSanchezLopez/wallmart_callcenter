IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Usuario_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Usuario_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Usuario_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Usuario_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Usuario_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle de usuarios consultado
**    Por          : VSR, 24/07/2009
*******************************************************************************/
@IdUsuario AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @ListadoTransportistasAsociados VARCHAR(4000)
  SET @ListadoTransportistasAsociados = (SELECT TransportistasAsociados FROM UsuarioDatoExtra WHERE IdUsuario = @IdUsuario)
  IF (@ListadoTransportistasAsociados IS NULL) SET @ListadoTransportistasAsociados = ''

  -----------------------------------------------------------------------------------
  -- TABLA 0: detalle del usuario
  -----------------------------------------------------------------------------------
  SELECT
	  IdUsuario = U.Id,
	  Nombre = ISNULL(U.Nombre,''),
	  Paterno = ISNULL(U.Paterno,''),
	  Materno = ISNULL(U.Materno,''),
	  NombreCompleto = ISNULL(RTRIM(LTRIM(U.Nombre)),'') + ISNULL(' '+ RTRIM(LTRIM(U.Paterno)),'') + ISNULL(' '+ RTRIM(LTRIM(U.Materno)),''),
	  Rut = ISNULL(U.Rut,''),
	  Dv = ISNULL(U.Dv,''),
	  RutCompleto = ISNULL(U.Rut,''),
	  Username = ISNULL(U.Username,''),
	  Email =  ISNULL(U.Email,''),
	  Telefono =  ISNULL(U.Telefono,''),
    IdDireccion = ISNULL(U.IdDireccion,0),
	  LoginDias =  ISNULL(U.LoginDias,''),
	  Super =  ISNULL(U.Super,0),
	  Estado =  ISNULL(U.Estado,0),
	  IdPerfil = ISNULL(U.IdPerfil,0),
	  NombrePerfil = ISNULL(P.Nombre,''),
	  LlavePerfil = ISNULL(P.Llave,''),
    -----------------------------------------
    -- datos extras del usuario
    -----------------------------------------
    EmailConCopia = ISNULL(UDE.EmailConCopia,''),
    NotificarPorEmailHorarioNocturno = ISNULL(UDE.NotificarPorEmailHorarioNocturno,0),
    PatenteTracto = ISNULL(UDE.PatenteTracto,''),
    PatenteTrailer = ISNULL(UDE.PatenteTrailer,''),
    ClasificacionConductor = ISNULL(UDE.ClasificacionConductor,''),
    IdUsuarioTransportista = ISNULL(UDE.IdUsuarioTransportista, -1),
    GestionarSoloCemtra = ISNULL(UDE.GestionarSoloCemtra, 'Todas')
  FROM
    Usuario AS U
    LEFT JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    LEFT JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
  WHERE
    U.Id = @IdUsuario


  -----------------------------------------------------------------------------------
  -- TABLA 1: telefonos por usuario
  -----------------------------------------------------------------------------------
  SELECT
	  IdTelefonoPorUsuario = TPU.Id,
	  Telefono = ISNULL(TPU.Telefono,''),
	  HoraInicio = ISNULL(TPU.HoraInicio,''),
	  HoraTermino = ISNULL(TPU.HoraTermino,'')
  FROM
    TelefonosPorUsuario AS TPU
  WHERE
    TPU.IdUsuario = @IdUsuario
  ORDER BY
    HoraInicio

  ----------------------------------------------------------------
  -- TABLA 2: Transportistas asociados perfil Teleoperador
  ----------------------------------------------------------------
  SELECT
    Texto = RTRIM(LTRIM(ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''))),
    Valor = U.Id,
    Seleccionado = CASE WHEN TablaTransportistasAsociados.Valor IS NULL THEN 0 ELSE 1 END
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    OUTER APPLY (
      SELECT DISTINCT
        TH.IdUsuario
      FROM
        Track_Homoclave AS TH
      WHERE
        TH.Activo = 1
    ) AS TablaHomoclave
    OUTER APPLY (
      SELECT
        FNU.Valor
      FROM
        dbo.fnu_InsertaListaTabla(@ListadoTransportistasAsociados) AS FNU
      WHERE
        FNU.Valor = U.Id
    ) AS TablaTransportistasAsociados
  WHERE
      U.Estado = 1
  AND P.Llave = 'TRA' -- obtiene el perfil Transportista
  AND TablaHomoclave.IdUsuario = U.Id
  ORDER BY
    Texto

  ----------------------------------------------------------------
  -- TABLA 3: Centros de distribucion asociados
  ----------------------------------------------------------------
  SELECT
    Texto = CD.Nombre + ISNULL(' - ' + CONVERT(VARCHAR, CD.CodigoInterno), ''),
    Valor = CD.Id,
    Seleccionado = CASE WHEN CDPU.IdCentroDistribucion IS NULL THEN 0 ELSE 1 END
  FROM
    CentroDistribucion AS CD
    LEFT JOIN CentroDistribucionPorUsuario AS CDPU ON (CD.Id = CDPU.IdCentroDistribucion AND CDPU.IdUsuario = @IdUsuario)
  ORDER BY
    Texto

  ----------------------------------------------------------------
  -- TABLA 4: Locales asociados
  ----------------------------------------------------------------
  SELECT
    Texto = F.Nombre + ' - ' + L.Nombre + ISNULL(' - ' + CONVERT(VARCHAR, L.CodigoInterno), ''),
    Valor = L.Id,
    Seleccionado = CASE WHEN LPU.IdLocal IS NULL THEN 0 ELSE 1 END
  FROM
    Local AS L
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    LEFT JOIN LocalesPorUsuario AS LPU ON (L.Id = LPU.IdLocal AND LPU.IdUsuario = @IdUsuario)
  ORDER BY
    Texto

END



  
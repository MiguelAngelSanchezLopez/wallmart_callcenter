﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_DesactivarAlertaEnColaAtencionLocalesRMyOtros')
BEGIN
  PRINT  'Dropping Procedure spu_Job_DesactivarAlertaEnColaAtencionLocalesRMyOtros'
  DROP  Procedure  dbo.spu_Job_DesactivarAlertaEnColaAtencionLocalesRMyOtros
END

GO

PRINT  'Creating Procedure spu_Job_DesactivarAlertaEnColaAtencionLocalesRMyOtros'
GO
CREATE Procedure dbo.spu_Job_DesactivarAlertaEnColaAtencionLocalesRMyOtros    
/******************************************************************************    
**    Descripcion  : desactiva las alertas en base de datos y las elimina de la cola de atencion
**    Por          : VSR, 20/01/2016
*******************************************************************************/    
@Status AS INT OUTPUT,
@IdAlerta AS INT,
@NombreAlerta AS VARCHAR(255),
@NroTransporte AS BIGINT,
@IdEmbarque AS BIGINT,
@LocalDestino AS INT
AS    
BEGIN    
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
  SET NOCOUNT ON    
    
  -------------------------------------------------------------    
  -- PASO 1: desactiva las alertas
  -------------------------------------------------------------    
  UPDATE Alerta
  SET Activo = 0
  WHERE
      DescripcionAlerta = @NombreAlerta
  AND NroTransporte = @NroTransporte
  AND IdEmbarque = @IdEmbarque
  AND LocalDestino = @LocalDestino

  -------------------------------------------------------------    
  -- PASO 2: traspasa alertas en cola de atencion al historial
  -------------------------------------------------------------
  INSERT INTO HistorialAlertaEnColaAtencion
  SELECT * FROM AlertaEnColaAtencion WHERE IdAlerta IN (
    SELECT Id FROM Alerta
    WHERE
        DescripcionAlerta = @NombreAlerta
    AND NroTransporte = @NroTransporte
    AND IdEmbarque = @IdEmbarque
    AND LocalDestino = @LocalDestino
    AND Activo = 0
  )

  -------------------------------------------------------------    
  -- PASO 2: desactiva la alerta de la cola de atencion
  -------------------------------------------------------------    
  UPDATE CallCenterAlerta
  SET
    Desactivada = 1,
    FechaDesactivacion = dbo.fnu_GETDATE()
  WHERE
      IdAlerta = @IdAlerta
    
  -- elimina la alerta de la cola de atencion
  DELETE FROM AlertaEnColaAtencion WHERE IdAlerta IN (
    SELECT Id FROM Alerta
    WHERE
        DescripcionAlerta = @NombreAlerta
    AND NroTransporte = @NroTransporte
    AND IdEmbarque = @IdEmbarque
    AND LocalDestino = @LocalDestino
    AND Activo = 0
  )

  -------------------------------------------------------------    
  -- PASO 3: activa las alertas que ya habian sido gestionadas y fueron desactivadas en este proceso
  -------------------------------------------------------------
  UPDATE Alerta SET Activo = 1 WHERE Id IN (
    SELECT T.Id
    FROM
      Alerta AS T
      INNER JOIN CallCenterHistorialEscalamiento AS HE ON (T.Id = HE.IdAlerta)
    WHERE
        T.DescripcionAlerta = @NombreAlerta
    AND T.NroTransporte = @NroTransporte
    AND T.IdEmbarque = @IdEmbarque
    AND T.LocalDestino = @LocalDestino
    AND T.Activo = 0	
  )
   
  -- si hay error devuelve codigo
  SET @Status = 1
  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END
   
    
END

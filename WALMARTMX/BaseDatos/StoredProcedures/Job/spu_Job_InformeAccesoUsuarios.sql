﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_InformeAccesoUsuarios')
BEGIN
  PRINT  'Dropping Procedure spu_Job_InformeAccesoUsuarios'
  DROP  Procedure  dbo.spu_Job_InformeAccesoUsuarios
END

GO

PRINT  'Creating Procedure spu_Job_InformeAccesoUsuarios'
GO
CREATE Procedure dbo.spu_Job_InformeAccesoUsuarios
/******************************************************************************
**    Descripcion  : obtiene listado de acceso de usuarios
**    Por          : VSR, 17/02/2017
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaISOInicio = '-1') SET @FechaISOInicio = '20170201'
  IF (@FechaISOTermino = '-1') SET @FechaISOTermino = '21001231'

  DECLARE @TablaAccesos AS TABLE(Aplicacion VARCHAR(255), NombreUsuario VARCHAR(255), Username VARCHAR(255), NombrePerfil VARCHAR(255), Fecha VARCHAR(255), TieneAcceso INT,
                                 NombreLineaTransporte VARCHAR(255), Hora VARCHAR(255))

  ------------------------------------------
  -- obtiene informacion de GPSTrack
  INSERT INTO @TablaAccesos(Aplicacion, NombreUsuario, Username, NombrePerfil, Fecha, TieneAcceso, Hora)
  SELECT
    Aplicacion = 'GPSTRACK',
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Username = TU.Usuario,
    NombrePerfil = 'GPSTrack',
    Fecha = ISNULL(TablaLogSesion.Fecha,''),
    TieneAcceso = CASE WHEN TablaLogSesion.IdLogSesion IS NULL THEN 0 ELSE 1 END,
    Hora = ISNULL(TablaLogSesion.Hora,'')
  FROM
    Usuario AS U
    INNER JOIN Track_Usuarios AS TU ON (U.Id = TU.Rut)
    OUTER APPLY (
      SELECT
        IdLogSesion = auxLG.Id,
        Fecha = REPLACE(CONVERT(VARCHAR, auxLG.Fecha, 111),'/','-'),
        Hora = LEFT(CONVERT(VARCHAR, auxLG.Fecha, 108),2)
      FROM
        LogSesion AS auxLG
      WHERE
          auxLG.Accion LIKE 'GPSTrack Walmart MX%'
      AND auxLG.Fecha >= CONVERT(DATETIME, @FechaISOInicio)
      AND auxLG.IdUsuario = U.Id
    ) AS TablaLogSesion
  WHERE
    U.Estado = 1
    
  ------------------------------------------
  -- obtiene informacion de CallCenter
  INSERT INTO @TablaAccesos(Aplicacion, NombreUsuario, Username, NombrePerfil, Fecha, TieneAcceso, Hora)
  SELECT
    Aplicacion = 'CALLCENTER',
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Username = U.Username,
    NombrePerfil = P.Nombre,
    Fecha = ISNULL(TablaLogSesion.Fecha,''),
    TieneAcceso = CASE WHEN TablaLogSesion.IdLogSesion IS NULL THEN 0 ELSE 1 END,
    Hora = ISNULL(TablaLogSesion.Hora,'')
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON U.IdPerfil = P.Id
    OUTER APPLY (
      SELECT
        IdLogSesion = auxLG.Id,
        Fecha = REPLACE(CONVERT(VARCHAR, auxLG.Fecha, 111),'/','-'),
        Hora = LEFT(CONVERT(VARCHAR, auxLG.Fecha, 108),2)
      FROM
        LogSesion AS auxLG
      WHERE
          auxLG.Accion LIKE 'Inicia sesión%'
      AND auxLG.Fecha >= CONVERT(DATETIME, @FechaISOInicio)
      AND auxLG.IdUsuario = U.Id
    ) AS TablaLogSesion
  WHERE
      P.Llave = 'TO'
  AND U.Estado = 1     

  ------------------------------------------
  -- obtiene informacion de Carrier
  INSERT INTO @TablaAccesos(Aplicacion, NombreUsuario, Username, NombrePerfil, Fecha, TieneAcceso, NombreLineaTransporte, Hora)
  SELECT
    Aplicacion = 'CARRIER',
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Username = U.Username,
    NombrePerfil = P.Nombre,
    Fecha = ISNULL(TablaLogSesion.Fecha,''),
    TieneAcceso = CASE WHEN TablaLogSesion.IdLogSesion IS NULL THEN 0 ELSE 1 END,
    NombreLineaTransporte = ISNULL(UTransportista.Nombre,'') + ISNULL(' ' + UTransportista.Paterno,'') + ISNULL(' ' + UTransportista.Materno,''),
    Hora = ISNULL(TablaLogSesion.Hora,'')
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON U.IdPerfil = P.Id
    INNER JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
    LEFT JOIN Usuario AS UTransportista ON (UDE.IdUsuarioTransportista = UTransportista.Id)
    OUTER APPLY (
      SELECT
        IdLogSesion = auxLG.Id,
        Fecha = REPLACE(CONVERT(VARCHAR, auxLG.Fecha, 111),'/','-'),
        Hora = LEFT(CONVERT(VARCHAR, auxLG.Fecha, 108),2)
      FROM
        LogSesion AS auxLG
      WHERE
          auxLG.Accion LIKE 'Inicia sesión%'
      AND auxLG.Fecha >= CONVERT(DATETIME, @FechaISOInicio)
      AND auxLG.IdUsuario = U.Id
    ) AS TablaLogSesion
  WHERE
      P.Llave = 'CLT'
  AND U.Estado = 1   

  ------------------------------------------
  -- obtiene informacion de App Movil
  INSERT INTO @TablaAccesos(Aplicacion, NombreUsuario, Username, NombrePerfil, Fecha, TieneAcceso, NombreLineaTransporte, Hora)
  SELECT
    Aplicacion = CASE 
                   WHEN P.Llave = 'CON' THEN 'APP MOVIL CONDUCTOR'
                   WHEN P.Llave = 'CLT' THEN 'APP MOVIL CARRIER'
                   WHEN P.Llave = 'LRT' THEN 'APP MOVIL TIENDA'
                   ELSE 'APP MOVIL'
                 END,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Username = U.Username,
    NombrePerfil = P.Nombre,
    Fecha = ISNULL(TablaLogSesion.Fecha,''),
    TieneAcceso = CASE WHEN TablaLogSesion.IdLogSesion IS NULL THEN 0 ELSE 1 END,
    NombreLineaTransporte = ISNULL(UTransportista.Nombre,'') + ISNULL(' ' + UTransportista.Paterno,'') + ISNULL(' ' + UTransportista.Materno,''),
    Hora = ISNULL(TablaLogSesion.Hora,'')
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON U.IdPerfil = P.Id
    INNER JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
    LEFT JOIN Usuario AS UTransportista ON (UDE.IdUsuarioTransportista = UTransportista.Id)
    OUTER APPLY (
      SELECT
        IdLogSesion = auxLG.Id,
        Fecha = REPLACE(CONVERT(VARCHAR, auxLG.Fecha, 111),'/','-'),
        Hora = LEFT(CONVERT(VARCHAR, auxLG.Fecha, 108),2)
      FROM
        LogSesion AS auxLG
      WHERE
          auxLG.Accion LIKE 'Mobile%'
      AND auxLG.Fecha >= CONVERT(DATETIME, @FechaISOInicio)
      AND auxLG.IdUsuario = U.Id
    ) AS TablaLogSesion
  WHERE
      (
      	 P.Llave IN ('CLT')
      	 OR U.Username IN (
          'jperez',
          'jpluma',
          'rquintana',
          'jvargas',
          'igomez',
          'fahernandez',
          'isantana',
          'jzabala',
          'vflores',
          'tienda2345',
          'tienda3025',
          'tienda3877',
          'tienda3753',
          'tienda1713',
          'tienda3750',
          'tienda8124',
          'tienda6534',
          'tienda6497',
          'tienda6397',
          'tienda6393',
          'tienda6205',
          'tienda4954',
          'tienda6224',
          'tienda4827',
          'trcdamarin',
          'trcdareyes',
          'trcdaalmanza',
          'trcdahernandez',
          'trcdaferrusca',
          'trcdaservantes',
          'trcdaguevara',
          'trcdapeña',
          'trcdcmartinez',
          'trcdcgomez',
          'trcdcfernando',
          'trcdcjuarez',
          'trcdcmadrid',
          'trcddcoayault',
          'trcddgarcia',
          'trcddhernandez',
          'trcdeestrella',
          'trcdeaguayo',
          'trcderuiz',
          'trcdfmorales',
          'trcdfcorrea',
          'trcdfhernandez',
          'trcdgronquillo',
          'trcdgvargas',
          'trcdhvazquez',
          'trcdhcarpio',
          'trcdizoyet',
          'trcdiacosta',
          'trcdiguevara',
          'trcdigutierrez',
          'trcdjlopez',
          'trcdjmoreno',
          'trcdjpérezg',
          'trcdjgonzalez',
          'trcdjrodriguez',
          'trcdjmartinez',
          'trcdjgomez',
          'trcdjaldana',
          'trcdjtellez',
          'trcdjaguilar',
          'trcdjsanchez',
          'trcdjherrada',
          'trcdjpérezm',
          'trcdjgutierrez',
          'trcdjmorales',
          'trcdjcanales',
          'trcdllopez',
          'trcdlmacias',
          'trcdlcastro',
          'trcdlgutierrez',
          'trcdlalcala',
          'trcdlmartinez',
          'trcdlrojas',
          'trcdlcarrillo',
          'trcdlhernandez',
          'trcdmmerin',
          'trcdmperez',
          'trcdmsuarez',
          'trcdmluna',
          'trcdmortiz',
          'trcdmalvarez',
          'trcdmferrusca',
          'trcdmbernal',
          'trcdmelizalde',
          'trcdncarbajal',
          'trcdocruz',
          'trcdoramirez',
          'trcdochavaro',
          'trcdpmartinez',
          'trcdphurtado',
          'trcdrvega',
          'trcdrpeña',
          'trcdrgonzalez',
          'trcdrruiz',
          'trcdsvazquez',
          'trcdsmujica',
          'trcdvfabela',
          'trcdvregalado',
          'trcdjtelles',
          'trcdmflores',
          'trcdapineda',
          'trcdghernandez ',
          'trcdjchamorro',
          'trcdsfernando',
          'trcdrramirez',
          'trcdegonzalez',
          'trcdjbautista',
          'trcdmcid  ',
          'trcdemaqueda',
          'trcdagonzález',
          'trcdjhernández',
          'trcdjmaldonado',
          'trcderamírez ',
          'trcdjserrano ',
          'trcdecamacho'
      	 )
      )
  AND U.Estado = 1     	

  -------------------------------------
  -- TABLA 0: CUADRO RESUMEN
  -------------------------------------
  SELECT
    APLICACION = TablaResumen.Aplicacion,
    FECHA = TablaResumen.Fecha,
    [TOTAL USUARIOS CREADOS] = TablaResumen.TotalUsuariosCreados,
    [TOTAL USUARIOS CON ACCCESOS] = TablaResumen.TotalUsuariosConAccesos,
    [PORCENTAJE USABILIDAD] = CASE WHEN TablaResumen.TotalUsuariosCreados = 0 THEN 0 ELSE ROUND((CONVERT(FLOAT, TablaResumen.TotalUsuariosConAccesos) / TablaResumen.TotalUsuariosCreados) * 100, 2) END
  FROM (
        SELECT DISTINCT
          T.Aplicacion,
          T.Fecha,
          TotalUsuariosCreados = (SELECT COUNT(DISTINCT aux.Username) FROM @TablaAccesos AS aux WHERE aux.Aplicacion = T.Aplicacion),
          TotalUsuariosConAccesos = (SELECT COUNT(DISTINCT aux.Username) FROM @TablaAccesos AS aux WHERE aux.Aplicacion = T.Aplicacion AND aux.Fecha = T.Fecha)
        FROM
          @TablaAccesos AS T
        WHERE
          T.Fecha <> ''
  ) AS TablaResumen
  ORDER BY
    TablaResumen.Aplicacion, TablaResumen.Fecha

  -------------------------------------
  -- TABLA 1: INFORME GPSTRACK
  -------------------------------------
  SELECT
    [NOMBRE USUARIO] = T.NombreUsuario,
    [USERNAME] = T.Username,
    [PERFIL] = T.NombrePerfil,
    [FECHA ACCESO] = T.Fecha,
    [TOTAL ACCESOS] = SUM(T.TieneAcceso)
  FROM
    @TablaAccesos AS T
  WHERE
    T.Aplicacion = 'GPSTRACK'
  GROUP BY
    T.NombreUsuario, T.Username, T.NombrePerfil, T.Fecha
  ORDER BY
    T.Username, T.Fecha

  -------------------------------------
  -- TABLA 2: CALLCENTER
  -------------------------------------
  SELECT
    [NOMBRE USUARIO] = T.NombreUsuario,
    [USERNAME] = T.Username,
    [PERFIL] = T.NombrePerfil,
    [FECHA ACCESO] = T.Fecha,
    [TOTAL ACCESOS] = SUM(T.TieneAcceso)
  FROM
    @TablaAccesos AS T
  WHERE
    T.Aplicacion = 'CALLCENTER'
  GROUP BY
    T.NombreUsuario, T.Username, T.NombrePerfil, T.Fecha
  ORDER BY
    T.Username, T.Fecha

  -------------------------------------
  -- TABLA 3: COORDINADOR LINEA TRANSPORTE (CARRIER)
  -------------------------------------
  SELECT
    [LINEA DE TRANSPORTE] = T.NombreLineaTransporte,
    [NOMBRE USUARIO] = T.NombreUsuario,
    [USERNAME] = T.Username,
    [PERFIL] = T.NombrePerfil,
    [FECHA ACCESO] = T.Fecha,
    [TOTAL ACCESOS] = SUM(T.TieneAcceso)
  FROM
    @TablaAccesos AS T
  WHERE
    T.Aplicacion = 'CARRIER'
  GROUP BY
    T.NombreUsuario, T.Username, T.NombrePerfil, T.NombreLineaTransporte, T.Fecha
  ORDER BY
    T.NombreLineaTransporte, T.Username, T.Fecha  

  -------------------------------------
  -- TABLA 4: APP MOVIL
  -------------------------------------
  SELECT
    [PERFIL] = T.NombrePerfil,
    [NOMBRE USUARIO] = T.NombreUsuario,
    [USERNAME] = T.Username,
    [LINEA DE TRANSPORTE] = T.NombreLineaTransporte,
    [FECHA ACCESO] = T.Fecha,
    [TOTAL ACCESOS] = SUM(T.TieneAcceso)
  FROM
    @TablaAccesos AS T
  WHERE
    T.Aplicacion LIKE 'APP MOVIL%'
  GROUP BY
    T.NombreUsuario, T.Username, T.NombrePerfil, T.NombreLineaTransporte, T.Fecha
  ORDER BY
    T.NombrePerfil, T.NombreUsuario, T.Username, T.Fecha   

  -------------------------------------
  -- TABLA 5: SABANA DATOS
  -------------------------------------
  SELECT
    [APLICACION] = T.Aplicacion,
    [PERFIL] = T.NombrePerfil,
    [NOMBRE USUARIO] = T.NombreUsuario,
    [USERNAME] = T.Username,
    [LINEA DE TRANSPORTE] = ISNULL(T.NombreLineaTransporte,''),
    [FECHA ACCESO] = T.Fecha,
    [HORA ACCESO] = T.Hora,
    [TOTAL ACCESOS] = SUM(T.TieneAcceso)
  FROM
    @TablaAccesos AS T
  GROUP BY
    T.Aplicacion, T.NombreUsuario, T.Username, T.NombrePerfil, T.Fecha, T.Hora, T.NombreLineaTransporte
  ORDER BY
    T.Aplicacion, T.NombrePerfil, T.Username, T.Fecha, T.Hora  

END

 
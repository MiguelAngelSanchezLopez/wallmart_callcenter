﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_AsignarAlertaEnColaAtencion')
BEGIN
  PRINT  'Dropping Procedure spu_Job_AsignarAlertaEnColaAtencion'
  DROP  Procedure  dbo.spu_Job_AsignarAlertaEnColaAtencion
END

GO

PRINT  'Creating Procedure spu_Job_AsignarAlertaEnColaAtencion'
GO
CREATE Procedure dbo.spu_Job_AsignarAlertaEnColaAtencion
/******************************************************************************
**    Descripcion  : obtiene listado de alertas que estan en cola de atencion por un tiempo X de minutos
**    Por          : VSR, 27/08/2015
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  -------------------------------------------------------------------
  -- TABLA 0: obtiene todo el listado
  -------------------------------------------------------------------
  SELECT
    IdAlerta = APG.IdAlertaPadre,
    IdAlertaHija = APG.IdAlertaHija,
    NroTransporte = APG.NroTransporte,
    IdEmbarque = APG.IdEmbarque,
    NombreAlerta = APG.NombreAlerta,
    LocalDestino = APG.LocalDestino,
    IdFormato = APG.IdFormato,
    Prioridad = AC.Prioridad,
    OrdenPrioridad = CONVERT(INT,TG.Extra1),
    FechaHoraCreacion = APG.FechaHoraCreacion,
    FechaHoraCreacionDMA = dbo.fnu_ConvertirDatetimeToDDMMYYYY(APG.FechaHoraCreacion,'FECHA_COMPLETA')
  FROM
    vwu_AlertaPorGestionar AS APG
    INNER JOIN Local AS L ON (APG.LocalDestino = L.CodigoInterno)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND APG.NombreAlerta = AD.Nombre)
    INNER JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
    INNER JOIN TipoGeneral AS TG ON (AC.Prioridad = TG.Valor AND TG.Tipo = 'Prioridad')
  WHERE
    AC.Activo = 1
  ORDER BY
    OrdenPrioridad, FechaHoraCreacion


END
/**
**  Autor		:	Mauricio Espinoza A.
**	Fecha		:	10-02-2017
**	Descripción	:	Inserta los registros nuevos de tracto
**/
CREATE PROCEDURE [dbo].[spu_jobTracoTemporalToOriginal]
AS
BEGIN
	Begin try
		Insert into InformacionTracto (
		   [Id_Master]
		  ,[Tipo_Viaje]
		  ,[CT_Tipo_Flota]
		  ,[Determinante_WMS]
		  ,[Id_Unidad_Viaje]
		  ,[Clave_MTS]
		  ,[Tipo_Unidad]
		  ,[Operador]
		  ,[Carta_Porte]
		  ,[Id_GTS]
		  ,[Id_Embarque]
		  ,[Embarque_GLS]
		  ,[Tipo_Servicio]
		  ,[Tipo_Entrega]
		  ,[Numero_Entregas]
		  ,[CT_Tipo_Flota_1_Tipo_Flota]
		  ,[Id_Unidad_Embarque]
		  ,[CT_Proveedores_1_Clave_MTS]
		  ,[CT_Tipo_Unidad_1_Tipo_Unidad]
		  ,[1]
		  ,[2]
		  ,[3]
		  ,[4]
		  ,[5]
		  ,[6]
		  ,[7]
		  ,[8]
		  ,[9]
		  ,[10]
		  ,[11]
		  ,[12]
		  ,[13]
		  ,[14]
		  ,[15]
		  ,[Fecha_Solicitud]
		  ,[Fecha_Enrampe]
		  ,[Fecha_Apertura_Embarque]
		  ,[Fecha_Cierre_Embarque]
		  ,[Fecha_Retiro]
		  ,[Fecha_Cita_Asignada]
		  ,[Fecha_Generacion]
		  ,[Fecha_Despacho]
		  ,[CT_Status_Embarques]
		  ,[CT_Status_Viajes]
		  ,[Factura_GLS]
		)
		Select 
		   t.[Id_Master]
		  ,t.[Tipo_Viaje]
		  ,t.[CT_Tipo_Flota]
		  ,t.[Determinante_WMS]
		  ,t.[Id_Unidad_Viaje]
		  ,t.[Clave_MTS]
		  ,t.[Tipo_Unidad]
		  ,t.[Operador]
		  ,t.[Carta_Porte]
		  ,t.[Id_GTS]
		  ,t.[Id_Embarque]
		  ,t.[Embarque_GLS]
		  ,t.[Tipo_Servicio]
		  ,t.[Tipo_Entrega]
		  ,t.[Numero_Entregas]
		  ,t.[CT_Tipo_Flota_1_Tipo_Flota]
		  ,t.[Id_Unidad_Embarque]
		  ,t.[CT_Proveedores_1_Clave_MTS]
		  ,t.[CT_Tipo_Unidad_1_Tipo_Unidad]
		  ,t.[1]
		  ,t.[2]
		  ,t.[3]
		  ,t.[4]
		  ,t.[5]
		  ,t.[6]
		  ,t.[7]
		  ,t.[8]
		  ,t.[9]
		  ,t.[10]
		  ,t.[11]
		  ,t.[12]
		  ,t.[13]
		  ,t.[14]
		  ,t.[15]
		  ,t.[Fecha_Solicitud]
		  ,t.[Fecha_Enrampe]
		  ,t.[Fecha_Apertura_Embarque]
		  ,t.[Fecha_Cierre_Embarque]
		  ,t.[Fecha_Retiro]
		  ,t.[Fecha_Cita_Asignada]
		  ,t.[Fecha_Generacion]
		  ,t.[Fecha_Despacho]
		  ,t.[CT_Status_Embarques]
		  ,t.[CT_Status_Viajes]
		  ,t.[Factura_GLS]
		from 
			InformacionTractoTemporal t Left Join
			InformacionTracto r ON
				t.Id_Master = r.Id_Master And
				t.Id_Embarque = r.Id_Embarque And
				t.CT_Status_Embarques = r.CT_Status_Embarques And
				t.CT_Status_Viajes = r.CT_Status_Viajes 
		Where
			(r.Id_Master is null) And
			(t.Id_Master is not null)


		Declare @fecha as varchar(30) =  CONVERT(varchar(10),dbo.fnu_GETDATE(), 112) + REPLACE(CONVERT(varchar(10),dbo.fnu_GETDATE(), 108),':','')
	
		INSERT INTO usuario (
			Username,
			Password,
			Rut,
			Nombre,
			FechaHoraCreacion,
			FechaHoraModificacion,
			LoginDias,
			Super,
			Estado,
			IdPerfil
		)
		SELECT   distinct	
			LOWER( (SELECT Substring(Data, 1,1) FROM [dbo].Track_Split (t.NombreSplit, ' ') WHERE Data <> '' and Id = 1) + 
					ISNULL((SELECT Data  FROM [dbo].Track_Split (t.NombreSplit, ' ') WHERE Data <> '' and Id = 2), '') ),
			'844F0AC6BFC2D39EF0E399B30CFAE16776ABB533',
			t.NombreSplit ,
			t.NombreSplit ,
			@fecha,
			@fecha,
			1111111,
			0,
			1,
			2
		FROM dbo.InformacionTracto AS x Cross Apply (
			select distinct NombreSplit = STUFF((SELECT Data + ' '   FROM [dbo].Track_Split (operador, ' ')  WHERE Data <> ''  FOR XML PATH('')), 1, 0, '')
		) as t
		where NOT exists (select rut from Usuario where rut = t.NombreSplit )
		Order by t.NombreSplit 

		Insert into usuariodatoextra (
			IdUsuario,
			NotificarPorEmailHorarioNocturno
		)
		select 
			Id,
			0
		from 
			usuario
		where 
			Idperfil = 2 AND not exists (select top 1 * from usuariodatoextra where IdUsuario = Id)

	End try
	begin catch
		insert into Track_Error ( 
			Fecha,
			NombreObjeto,
			MensajeError
		)
		Values (
			dbo.fnu_GETDATE(),
			'spu_jobTracoTemporalToOriginal',
			'Error: ' + error_message()
		)
	end catch

	truncate table InformacionTractoTemporal
End




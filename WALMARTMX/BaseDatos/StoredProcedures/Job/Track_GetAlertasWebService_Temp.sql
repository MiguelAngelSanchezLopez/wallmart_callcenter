﻿
/****** Object:  StoredProcedure [dbo].[Track_GetAlertasWebService_Temp]    Script Date: 07/03/2015 16:14:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Track_GetAlertasWebService_Temp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Track_GetAlertasWebService_Temp]
GO

CREATE Procedure [dbo].[Track_GetAlertasWebService_Temp]
	
AS

Begin Try

	
	Select  convert(varchar(255),Id) Id, 
			isnull(convert(varchar(255),NroTransporte), '') NroTransporte, 
			isnull(convert(varchar(255),OrigenCodigo), '') OrigenCodigo, 
			isnull(convert(varchar(255),PatenteTracto), '') PatenteTracto,
			isnull(convert(varchar(255),PatenteTrailer), '') PatenteTrailer,
			isnull(convert(varchar(255),RutTransportista), '') RutTransportista,
			isnull(convert(varchar(255),NombreTransportista), '') NombreTransportista,
			isnull(convert(varchar(255),RutConductor), '') RutConductor,
			isnull(convert(varchar(255),NombreConductor), '') NombreConductor,
			isnull(convert(varchar(255),FechaUltimaTransmision), '') FechaUltimaTransmision,
			isnull(convert(varchar(255),DescripcionAlerta), '') DescripcionAlerta,
			isnull(convert(varchar(255),FechaInicioAlerta), '') FechaInicioAlerta,
			isnull(convert(varchar(255),LatTrailer), '') LatTrailer,
			isnull(convert(varchar(255),LonTrailer), '') LonTrailer,
			isnull(convert(varchar(255),LatTracto), '') LatTracto,
			isnull(convert(varchar(255),LonTracto), '') LonTracto,
			isnull(convert(varchar(255),TipoAlerta), '') TipoAlerta,
			isnull(convert(varchar(255),Permiso), '') Permiso,
			isnull(convert(varchar(255),Criticidad), '') Criticidad,
			isnull(convert(varchar(255),TipoAlertaDescripcion), '') TipoAlertaDescripcion,
			isnull(convert(varchar(255),AlertaMapa), '') AlertaMapa,
			isnull(convert(varchar(255),NombreZona), '') NombreZona,
			isnull(convert(varchar(255),Velocidad), '') Velocidad,
			isnull(convert(varchar(255),EstadoGPSTracto), '') EstadoGPSTracto,
			isnull(convert(varchar(255),EstadoGPSRampla), '') EstadoGPSRampla,
			isnull(convert(varchar(255),EstadoGPS), '') EstadoGPS,
			isnull(convert(varchar(255),LocalDestino), '') LocalDestino,
			isnull(convert(varchar(255),TipoPunto), '') TipoPunto,
			isnull(convert(varchar(255),Temp1), '') Temp1,
			isnull(convert(varchar(255),Temp2), '') Temp2,
			isnull(convert(varchar(255),DistanciaTT), '') DistanciaTT,
			isnull(convert(varchar(255),TransportistaTrailer), '') TransportistaTrailer,
			isnull(convert(varchar(255),CantidadSatelites), '') CantidadSatelites

	From AlertaWebService_Temp
	

End	Try

Begin Catch
	
	Insert into Track_Error(Fecha, NombreObjeto, MensajeError)   
	Values (getdate(), 
			'SP: Track_GetAlertasWebService_Temp',
			ERROR_MESSAGE())

End Catch


GO



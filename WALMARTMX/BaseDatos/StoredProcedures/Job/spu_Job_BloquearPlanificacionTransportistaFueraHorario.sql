﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_BloquearPlanificacionTransportistaFueraHorario')
BEGIN
  PRINT  'Dropping Procedure spu_Job_BloquearPlanificacionTransportistaFueraHorario'
  DROP  Procedure  dbo.spu_Job_BloquearPlanificacionTransportistaFueraHorario
END
GO

PRINT  'Creating Procedure spu_Job_BloquearPlanificacionTransportistaFueraHorario'
GO
CREATE Procedure dbo.spu_Job_BloquearPlanificacionTransportistaFueraHorario
/******************************************************************************
**    Descripcion  : obtiene listado de planificacion que seran bloqueados por fuera de horario (2 hrs. antes de la fecha planificacion)
**    Por          : VSR, 27/07/2015
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaISOInicio = '-1') SET @FechaISOInicio = '20150101'
  IF (@FechaISOTermino = '-1') SET @FechaISOTermino = '21001231'

  DECLARE @TablaPlanificacion AS TABLE(IdPlanificacionTransportista INT, IdUsuarioTransportista INT, NombreTransportista VARCHAR(255), RutTransportista VARCHAR(255),
                                       NroTransporte INT, IdLocal INT, NombreLocal VARCHAR(255), CodigoLocal INT, FechaPresentacion DATETIME, Carga VARCHAR(255),
                                       OrdenEntrega INT, TipoCamion VARCHAR(255), EmailTransportista VARCHAR(255), EmailTransportistaConCopia TEXT)

  INSERT INTO @TablaPlanificacion
  (
	  IdPlanificacionTransportista,
	  IdUsuarioTransportista,
	  NombreTransportista,
	  RutTransportista,
	  NroTransporte,
	  IdLocal,
	  NombreLocal,
	  CodigoLocal,
	  FechaPresentacion,
	  Carga,
	  OrdenEntrega,
	  TipoCamion,
	  EmailTransportista,
	  EmailTransportistaConCopia
  )
  SELECT 
    IdPlanificacionTransportista = PT.Id,
    IdUsuarioTransportista = PT.IdUsuarioTransportista,
    NombreTransportista = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Materno,'') + ISNULL(' ' + U.Paterno,''),
    RutTransportista = ISNULL(U.Rut,'') + CASE WHEN ISNULL(U.DV,'') = '' THEN '' ELSE '-' + ISNULL(U.DV,'') END,
    NroTransporte = PT.NroTransporte,
    IdLocal = PT.IdLocal,
    NombreLocal = ISNULL(L.Nombre,''),
    CodigoLocal = ISNULL(L.CodigoInterno,''),
    FechaPresentacion = PT.FechaPresentacion,
    Carga = ISNULL(PT.Carga,''),
    OrdenEntrega = ISNULL(PT.OrdenEntrega,0),
    TipoCamion = ISNULL(PT.TipoCamion,''),
    EmailTransportista = ISNULL(U.Email,''),
    EmailTransportistaConCopia = ISNULL(UDE.EmailConCopia,'')
  FROM
    PlanificacionTransportista AS PT
    INNER JOIN Local AS L ON (PT.IdLocal = L.Id)
    INNER JOIN Usuario AS U ON (PT.IdUsuarioTransportista = U.Id)
    LEFT JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
  WHERE
      PT.CerradoPorTransportista = 0
  AND PT.Finalizada = 0
  AND PT.BloqueadoPorFueraHorario = 0
  AND ISNULL(PT.Anden, '') = ''
  -- verifica que la fecha presentacion se encuentre hasta 2 hrs. antes de la hora actual
  AND DATEDIFF(mi, dbo.fnu_GETDATE(), PT.FechaPresentacion) <= 120
  -- obtiene registros hasta un dia atras, para no obtener el resto de la historia que no ha sido procesado
  AND DATEDIFF(dd, PT.FechaPresentacion, dbo.fnu_GETDATE()) <= 1

  --------------------------------------------------------------
  -- TABLA 0: Listado Transportistas
  --------------------------------------------------------------
  SELECT DISTINCT
    P.IdUsuarioTransportista,
    P.NombreTransportista,
    P.RutTransportista,
    P.EmailTransportista,
    EmailTransportistaConCopia = CONVERT(VARCHAR(8000), P.EmailTransportistaConCopia)
  FROM
    @TablaPlanificacion AS P
  ORDER BY
    P.NombreTransportista

  --------------------------------------------------------------
  -- TABLA 1: Listado planificaciones
  --------------------------------------------------------------
  SELECT
    P.*
  FROM
    @TablaPlanificacion AS P
  ORDER BY
    P.NombreTransportista, P.FechaPresentacion

  --------------------------------------------------------------
  -- TABLA 2: Listado planificaciones para Excel
  --------------------------------------------------------------
  SELECT 
    [RUT LINEA DE TRANSPORTE] = '''' + P.RutTransportista,
    [LINEA DE TRANSPORTE] = P.NombreTransportista,
    [FECHA HORA PRESENTACION] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(P.FechaPresentacion, 'FECHA_COMPLETA'),
    [IDMASTER] = P.NroTransporte,
    [TIENDA] = P.NombreLocal,
    [DETERMINANTE] = P.CodigoLocal,
    [ORDEN ENTREGA] = P.OrdenEntrega,
    [CARGA] = P.Carga,
    [TIPO CAMION] = P.TipoCamion,
    IDUSUARIOTRANSPORTISTA = P.IdUsuarioTransportista
  FROM
    @TablaPlanificacion AS P
  ORDER BY
    P.NombreTransportista, P.FechaPresentacion

END
CREATE PROCEDURE spu_job_Carrier_CargaPDF (
	@SiglaTransportista varchar(10),
	@Fecha			    varchar(10),
	@TipoInforme		varchar(50)
)
AS
BEGIN
	
	Declare @IdRegistro As Int
	DECLARE @IdTrans	As Int
	
	Select 
		@IdTrans =  IdUsuario 
	From 
		Track_Homoclave 
	Where 
		Nomenclatura = @SiglaTransportista 

	BEGIN TRY

		Insert Into Carrier_Informe (
			IdUsuarioTrans,
			Fecha,
			Tipo_Informe 
		)
		Values (
			@IdTrans,
			CONVERT(datetime, @Fecha),
			@TipoInforme 
		)

		SET @IdRegistro = @@IDENTITY 

	END TRY
	BEGIN CATCH

		Insert Into Track_Error (
			Fecha,
			NombreObjeto,
			MensajeError
		)
		Values (
			dbo.fnu_GETDATE(),
			'spu_job_Carrier_CargaPDF',
			'Trans' + @SiglaTransportista + ', ERROR: ' + ERROR_MESSAGE()
		)
		
		SET @IdRegistro = -1
	END CATCH

	SELECT @IdRegistro AS 'IdRegistro'
END
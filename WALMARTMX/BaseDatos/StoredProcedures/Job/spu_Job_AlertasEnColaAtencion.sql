﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_AlertasEnColaAtencion')
BEGIN
  PRINT  'Dropping Procedure spu_Job_AlertasEnColaAtencion'
  DROP  Procedure  dbo.spu_Job_AlertasEnColaAtencion
END

GO

PRINT  'Creating Procedure spu_Job_AlertasEnColaAtencion'
GO
CREATE Procedure dbo.spu_Job_AlertasEnColaAtencion
/******************************************************************************
**    Descripcion  : obtiene listado de alertas que estan en cola de atencion por un tiempo X de minutos
**    Por          : VSR, 27/08/2015
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaISOInicio = '-1') SET @FechaISOInicio = CONVERT(VARCHAR,dbo.fnu_GETDATE(),112)
  IF (@FechaISOTermino = '-1') SET @FechaISOTermino = CONVERT(VARCHAR,dbo.fnu_GETDATE(),112)

  DECLARE @minutosEsperaParaAtencion INT, @minutosEsperaRegionMetropolitana INT, @minutosEsperaOtrasRegiones INT
  SET @minutosEsperaParaAtencion = 20
  SET @minutosEsperaRegionMetropolitana = 60
  SET @minutosEsperaOtrasRegiones = 60

  ----------------------------------------------------------
  -- PASO 2: OBTIENE TODAS LAS ALERTAS QUE ESTAN EN COLA Y EL TIEMPO DE ESPERA
  ----------------------------------------------------------
  DECLARE @TablaAlertaEnCola AS TABLE(
	  IdAlerta INT, NombreAlerta VARCHAR(255), NroTransporte BIGINT, IdEmbarque BIGINT, Fecha VARCHAR(255), AtendidoPor VARCHAR(255), Prioridad VARCHAR(255), OrdenPrioridad INT,
	  Clasificacion VARCHAR(255), OrdenAparicion INT, NombreFormato VARCHAR(255), LocalDestino INT, NombreLocalDestino VARCHAR(255), DiferenciaMinutos INT,
	  NumeroRegionLocal INT
  )
  INSERT INTO @TablaAlertaEnCola
  (
	  IdAlerta,
	  NombreAlerta,
	  NroTransporte,
    IdEmbarque,
	  Fecha,
	  AtendidoPor,
	  Prioridad,
	  OrdenPrioridad,
	  Clasificacion,
	  OrdenAparicion,
	  NombreFormato,
	  LocalDestino,
	  NombreLocalDestino,
	  DiferenciaMinutos,
	  NumeroRegionLocal
  )
  SELECT
    IdAlerta = T.IdAlerta,
    NombreAlerta = T.NombreAlerta,
    NroTransporte = T.NroTransporte,
    IdEmbarque = T.IdEmbarque,
    Fecha = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.Fecha, 'FECHA_COMPLETA'),
    AtendidoPor = T.AtendidoPor,
    Prioridad = T.Prioridad,
    OrdenPrioridad = T.OrdenPrioridad,
    Clasificacion = T.Clasificacion,
    OrdenAparicion = T.OrdenAparicion,
    NombreFormato = T.NombreFormato,
    LocalDestino = T.LocalDestino,
    NombreLocalDestino = T.NombreLocalDestino,
    DiferenciaMinutos = CASE WHEN T.Asignada = 1 THEN
                          DATEDIFF(mi, T.FechaAsignacion, dbo.fnu_GETDATE())
                        ELSE
      	                  DATEDIFF(mi, T.Fecha, dbo.fnu_GETDATE())
                        END,
    NumeroRegionLocal                              
  FROM (
        ----------------------------------------------------------
        -- PASO 1: OBTIENE INFORMACION DE LAS ALERTAS QUE ESTAN EN COLA DE ATENCION
        ----------------------------------------------------------
        SELECT
          IdAlerta = AECA.IdAlerta,
          NombreFormato = F.Nombre,
          NombreAlerta = AECA.NombreAlerta,
          NroTransporte = AECA.NroTransporte,
          IdEmbarque = AECA.IdEmbarque,
          Fecha = AECA.FechaHoraCreacion,
          AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
          Prioridad = AECA.Prioridad,
          Clasificacion = CASE WHEN AECA.Asignada = 1 THEN 'SIENDO ATENDIDA' ELSE 'EN COLA DE ATENCION' END,
          OrdenAparicion = CASE WHEN AECA.Asignada = 1 THEN 1 ELSE 2 END,
          Asignada = ISNULL(AECA.Asignada,0),
          FechaAsignacion = AECA.FechaAsignacion,
          OrdenPrioridad = AECA.OrdenPrioridad,
          LocalDestino = AECA.LocalDestino,
          NombreLocalDestino = ISNULL(L.Nombre,''),
          NumeroRegionLocal = ISNULL(L.NumeroRegionLocal,'')
        FROM
          AlertaEnColaAtencion AS AECA
          INNER JOIN Alerta AS A ON (AECA.IdAlerta = A.Id)
          INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
          INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
          LEFT JOIN Usuario AS U ON (AECA.IdUsuarioAsignada = U.Id)        	
  ) AS T

  ----------------------------------------------------------
  -- TABLA 0: DEVUELVE LAS ALERTAS QUE ESTAN EN COLA POR MAS DE 20 MIN.
  ----------------------------------------------------------
  SELECT
    CLASIFICACION = T.Clasificacion,
	  IDALERTA = T.IdAlerta,
    FORMATO = T.NombreFormato,
    [NOMBRE ALERTA] = T.NombreAlerta,
    [ID MASTER] = T.NroTransporte,
    [ID EMBARQUE] = T.IdEmbarque,
    [TIENDA] = T.NombreLocalDestino,
    [DETERMINANTE] = T.LocalDestino,
    PRIORIDAD = T.Prioridad,
    [FECHA ALERTA] = T.Fecha,
    [MINUTOS SIN ATENCION] = T.DiferenciaMinutos
  FROM
    @TablaAlertaEnCola AS T
  WHERE
    T.DiferenciaMinutos > @minutosEsperaParaAtencion
  ORDER BY
    T.OrdenAparicion, T.OrdenPrioridad, T.DiferenciaMinutos DESC

  ----------------------------------------------------------
  -- TABLA 1: DEVUELVE LAS ALERTAS QUE SERAN DESACTIVADAS
  ----------------------------------------------------------
  SELECT
    CLASIFICACION = T.Clasificacion,
	  IDALERTA = T.IdAlerta,
    FORMATO = T.NombreFormato,
    [NOMBRE ALERTA] = T.NombreAlerta,
    [ID MASTER] = T.NroTransporte,
    [ID EMBARQUE] = T.IdEmbarque,
    [TIENDA] = T.NombreLocalDestino,
    [DETERMINANTE] = T.LocalDestino,
    PRIORIDAD = T.Prioridad,
    [FECHA ALERTA] = T.Fecha,
    [MINUTOS SIN ATENCION] = T.DiferenciaMinutos
  FROM
    @TablaAlertaEnCola AS T
  WHERE
      T.Clasificacion <> 'SIENDO ATENDIDA'
  AND (
        -- locales region metropolitana
        ( T.NumeroRegionLocal = 13 AND T.DiferenciaMinutos > @minutosEsperaRegionMetropolitana)
        -- locales otras regiones
    OR ( T.NumeroRegionLocal <> 13 AND T.DiferenciaMinutos > @minutosEsperaOtrasRegiones)
  )
  ORDER BY
    T.LocalDestino, T.OrdenPrioridad, T.DiferenciaMinutos DESC
    
  -------------------------------------------------------------------------
  -- TABLA 2: CONTACTOS PARA EL ENVIO DE SMS Y EMAIL
  -------------------------------------------------------------------------
  DECLARE @TablaLocalesPorUsuario AS TABLE(
	  IdLocal INT, IdUsuario INT, NombrePerfil VARCHAR(255), NombreUsuario VARCHAR(255), Telefono VARCHAR(255), Email VARCHAR(255)
  )

  INSERT INTO @TablaLocalesPorUsuario
  (
	  IdLocal,
	  IdUsuario,
	  NombrePerfil,
	  NombreUsuario,
	  Telefono,
	  Email
  )
  SELECT
    LPS.IdLocal,
    LPS.IdUsuario,
    P.Nombre,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL(U.Telefono,''),
    Email = ISNULL(U.Email,'')
  FROM
    LocalesPorUsuario AS LPS
    INNER JOIN Usuario AS U ON (LPS.IdUsuario = U.Id)
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      P.Llave IN ('GL', 'JR')
  
  -- devuelve los registros    
  SELECT
    LocalDestino = L.CodigoInterno,
    NombreLocal = L.Nombre,
    -- cargo: GERENTE LOCAL
    CargoEncargado1 = ISNULL(GL.NombrePerfil,''),
    NombreEncargado1 = isnull(GL.NombreUsuario,''),
    TelefonoEncargado1 = isnull(GL.Telefono,''),
    EmailEncargado1 = isnull(GL.Email,''),
    -- cargo: JEFE RECEPCION
    CargoEncargado2 = ISNULL(JR.NombrePerfil,''),
    NombreEncargado2 = isnull(JR.NombreUsuario,''),
    TelefonoEncargado2 = isnull(JR.Telefono,''),
    EmailEncargado2 = isnull(JR.Email,'')
  FROM
    Local AS L
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    LEFT merge JOIN @TablaLocalesPorUsuario AS GL ON (GL.IdLocal = L.Id AND GL.NombrePerfil = 'Gerente Local')
    LEFT merge JOIN @TablaLocalesPorUsuario AS JR ON (JR.IdLocal = L.Id AND JR.NombrePerfil = 'Jefe Recepción')
  WHERE
    L.CodigoInterno IN (
      SELECT LocalDestino FROM @TablaAlertaEnCola
    )
END
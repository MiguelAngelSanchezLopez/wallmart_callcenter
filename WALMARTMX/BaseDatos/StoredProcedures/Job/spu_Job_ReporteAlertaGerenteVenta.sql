﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_ReporteAlertaGerenteVenta')
BEGIN
  PRINT  'Dropping Procedure spu_Job_ReporteAlertaGerenteVenta'
  DROP  Procedure  dbo.spu_Job_ReporteAlertaGerenteVenta
END

GO

PRINT  'Creating Procedure spu_Job_ReporteAlertaGerenteVenta'
GO
CREATE Procedure dbo.spu_Job_ReporteAlertaGerenteVenta
/******************************************************************************
**    Descripcion  : obtiene listado de reclamos
**    Por          : VSR, 30/01/2015
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaISOInicio = '-1') SET @FechaISOInicio = '20150101'
  IF (@FechaISOTermino = '-1') SET @FechaISOTermino = '21001231'

  DECLARE @TablaEncargadoTienda AS TABLE (IdLocal INT, Formato VARCHAR(255), NombreUsuario VARCHAR(255), Cargo VARCHAR(255), Email VARCHAR(255))

  ------------------------------------------------------------------------------------
  -- obtiene GERENTE VENTA y sus homologos
  ------------------------------------------------------------------------------------
  INSERT INTO @TablaEncargadoTienda(IdLocal,Formato,NombreUsuario,Cargo,Email)
  SELECT
    IdLocal = L.Id,
    Formato = F.Nombre,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Cargo = P.Nombre,
    Email = ISNULL(U.Email,'')
  FROM
    Local AS L
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    INNER JOIN LocalesPorUsuario AS LPU ON (L.Id = LPU.IdLocal)
    INNER JOIN Usuario AS U ON (LPU.IdUsuario = U.Id)
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
    P.Llave IN ('GTV','JFT','ADML')
    
  -------------------------------------------------------------
  -- obtiene las alertas gestionadas
  -------------------------------------------------------------
  DECLARE @TablaHistorialEscalamiento AS TABLE(Id INT, IdAlerta INT, IdUsuarioCreacion INT, IdEscalamientoPorAlerta INT, Fecha DATETIME)
 
  INSERT INTO @TablaHistorialEscalamiento(Id,IdAlerta,IdUsuarioCreacion,IdEscalamientoPorAlerta,Fecha)
  SELECT
    HE.Id,
    HE.IdAlerta,
    HE.IdUsuarioCreacion,
    HE.IdEscalamientoPorAlerta,
    HE.Fecha
  FROM
    CallCenterHistorialEscalamiento AS HE
    INNER JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
    INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
  WHERE
      CONVERT(VARCHAR, HE.Fecha, 112) BETWEEN @FechaISOInicio AND @FechaISOTermino 
  AND HE.IdEscalamientoPorAlerta IS NOT NULL
  AND A.NroTransporte NOT IN (SELECT NroTransporte FROM HistorialReporteGerenteVenta)
  AND CCA.Desactivada = 0

  -------------------------------------------------------------
  -- obtiene detalle de las alertas gestionadas
  -------------------------------------------------------------
  DECLARE @TablaAlerta AS TABLE(IdAlerta INT, NombreAlerta VARCHAR(255), NroTransporte INT, NombreTransportista VARCHAR(255), LocalDestino VARCHAR(255), LocalDestinoCodigo VARCHAR(255),
                                Zona VARCHAR(255), TipoViaje VARCHAR(255), AtendidoPor VARCHAR(255), FechaCreacion DATETIME, NroEscalamiento VARCHAR(255),
                                FechaInicioGestion DATETIME, FechaTerminoGestion DATETIME, MinutosGestion INT, IdHistorialeEcalamientoContacto INT,
                                GrupoContacto VARCHAR(255), NombreContacto VARCHAR(255), Explicacion VARCHAR(255), Observacion VARCHAR(4000), CargoLocal VARCHAR(255),
                                NombreEncargadoLocal VARCHAR(255), FormatoLocal VARCHAR(255), EmailEncargado VARCHAR(255), AlertaMapa VARCHAR(1024)
                               )

  INSERT INTO @TablaAlerta(IdAlerta, NombreAlerta, NroTransporte, NombreTransportista, LocalDestino, LocalDestinoCodigo, Zona, TipoViaje, AtendidoPor,
                           FechaCreacion, NroEscalamiento, FechaInicioGestion, FechaTerminoGestion, MinutosGestion, IdHistorialeEcalamientoContacto,
                           GrupoContacto, NombreContacto, Explicacion, Observacion, CargoLocal, NombreEncargadoLocal, FormatoLocal, EmailEncargado, AlertaMapa
                          )
  SELECT DISTINCT
    IdAlerta = A.Id,
    NombreAlerta = A.DescripcionAlerta,
    NroTransporte = A.NroTransporte,
    NombreTransportista = ISNULL(A.NombreTransportista,''),
    LocalDestino = ISNULL(L.Nombre,''),
    LocalDestinoCodigo = ISNULL(CONVERT(VARCHAR,L.CodigoInterno),''),
    Zona = ISNULL(A.Permiso,''),
    TipoViaje = ISNULL(TV.TipoViaje,''),
    AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    FechaCreacion = A.FechaHoraCreacion,
    NroEscalamiento = CONVERT(VARCHAR,EPA.Orden),
    FechaInicioGestion = AGA.FechaAsignacion,
    FechaTerminoGestion = AGA.FechaTerminoGestion,
    MinutosGestion = DATEDIFF(mi,AGA.FechaAsignacion,AGA.FechaTerminoGestion),
    IdHistorialeEcalamientoContacto = HEC.Id,
    GrupoContacto = CASE WHEN HEC.IdEscalamientoPorAlertaContacto IS NULL THEN UPPER(P.Nombre) ELSE UPPER(EPAGC.Nombre) END,
    NombreContacto = CASE WHEN HEC.IdEscalamientoPorAlertaContacto IS NULL THEN
                       ISNULL(USinGrupo.Nombre,'') + ISNULL(' ' + USinGrupo.Paterno,'') + ISNULL(' ' + USinGrupo.Materno,'')
                     ELSE
 	                     ISNULL(UConGrupo.Nombre,'') + ISNULL(' ' + UConGrupo.Paterno,'') + ISNULL(' ' + UConGrupo.Materno,'')
                     END,
    Explicacion = ISNULL(HEC.Explicacion,''),
    Observacion = ISNULL(HEC.Observacion,''),
    CargoLocal = TEL.Cargo,
    NombreEncargadoLocal = TEL.NombreUsuario,
    FormatoLocal = TEL.Formato,
    EmailEncargado = TEL.Email,
    AlertaMapa = ISNULL(A.AlertaMapa,'')
  FROM
    @TablaHistorialEscalamiento AS HE
    INNER JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
    INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    INNER JOIN Usuario AS U ON (HE.IdUsuarioCreacion = U.Id)
    INNER JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id) -- 3seg
    INNER JOIN CallCenterHistorialEscalamientoContacto AS HEC ON (HE.Id = HEC.IdHistorialEscalamiento)
    LEFT JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
    LEFT JOIN CallCenterAlertaGestionAcumulada AS AGA ON (HE.Id = AGA.IdHistorialEscalamiento)
    LEFT JOIN EscalamientoPorAlertaContacto AS EPAC ON (HEC.IdEscalamientoPorAlertaContacto = EPAC.Id)
    LEFT JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (EPAC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
    LEFT JOIN Usuario AS UConGrupo ON (EPAC.IdUsuario = UConGrupo.Id)
    LEFT JOIN Usuario AS USinGrupo ON (HEC.IdUsuarioSinGrupoContacto = USinGrupo.Id)
    LEFT JOIN Perfil AS P ON (USinGrupo.IdPerfil = P.Id)
    LEFT JOIN @TablaEncargadoTienda AS TEL ON (L.Id = TEL.IdLocal)
  WHERE
      TV.EstadoViaje LIKE 'EN%LOCAL%'
  AND CCA.Desactivada = 0


  -------------------------------------------------------------
  -- TABLA 0: obtiene los encargados a los que se les enviara la informacion
  -------------------------------------------------------------
  SELECT DISTINCT
    T.NombreEncargadoLocal,
    T.CargoLocal,
    T.EmailEncargado
  FROM
    @TablaAlerta AS T
  WHERE
    T.NombreEncargadoLocal IS NOT NULL
  ORDER BY
    T.NombreEncargadoLocal

  -------------------------------------------------------------
  -- TABLA 1: obtiene totales de alertas por encargado
  -------------------------------------------------------------
  SELECT
    [ENCARGADO TIENDA] = T.NombreEncargadoLocal,
    [CARGO ENCARGADO] = T.CargoLocal,
    [TIENDA] = T.LocalDestino,
    [DETERMINANTE] = T.LocalDestinoCodigo,
    [ALERTA] = T.NombreAlerta,
    [TOTAL GESTIONADAS] = COUNT(T.NombreAlerta)
  FROM (
        SELECT DISTINCT
          T.NombreEncargadoLocal,
          T.CargoLocal,
          T.LocalDestino,
          T.LocalDestinoCodigo,
          T.IdAlerta,
          T.NombreAlerta
        FROM
          @TablaAlerta AS T
        WHERE
          T.NombreEncargadoLocal IS NOT NULL
  ) AS T
  GROUP BY
    T.NombreEncargadoLocal, T.CargoLocal, T.LocalDestino, T.LocalDestinoCodigo, T.NombreAlerta
  ORDER BY
    T.NombreEncargadoLocal, T.LocalDestino


  -------------------------------------------------------------
  -- TABLA 2: lista el detalle de las alertas gestionadas
  -------------------------------------------------------------
  SELECT
    [ENCARGADO TIENDA] = T.NombreEncargadoLocal,
    [CARGO ENCARGADO] = T.CargoLocal,
    [FORMATO TIENDA] = T.FormatoLocal,
    [TIENDA] = T.LocalDestino,
    [DETERMINANTE] = T.LocalDestinoCodigo,
    [ID ALERTA] = T.IdAlerta,
    [FECHA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaCreacion, 'FECHA_COMPLETA'),
    ALERTA = T.NombreAlerta,
    [IDMASTER] = T.NroTransporte, 
    [LINEA DE TRANSPORTE] = T.NombreTransportista,
    ZONA = T.Zona, 
    [TIPO VIAJE] = T.TipoViaje, 
    [ESCALAMIENTO NRO] = CASE WHEN T.NroEscalamiento IS NULL THEN 'Supervisor' ELSE T.NroEscalamiento END,
    [ESCALAMIENTO ATENDIDO POR] = T.AtendidoPor, 
    [ESCALAMIENTO FECHA INICIO GESTION] = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaInicioGestion,'FECHA_COMPLETA'),''),
    [ESCALAMIENTO FECHA TERMINO GESTION] = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaTerminoGestion,'FECHA_COMPLETA'),''),
    [ESCALAMIENTO MINUTOS GESTION] = ISNULL(T.MinutosGestion,-1),
    [CARGO CONTACTO] = T.GrupoContacto,
    [NOMBRE CONTACTO] = T.NombreContacto,
    EXPLICACION = T.Explicacion,
    OBSERVACION = T.Observacion,
    [LINK MAPA] = T.AlertaMapa
  FROM
    @TablaAlerta AS T
  WHERE
    T.NombreEncargadoLocal IS NOT NULL
  ORDER BY
    T.NombreEncargadoLocal, T.IdHistorialeEcalamientoContacto, T.FechaCreacion, T.IdAlerta

  -------------------------------------------------------------
  -- TABLA 3: lista los nro transporte por encargado
  -------------------------------------------------------------
  SELECT DISTINCT
    NombreEncargadoLocal = T.NombreEncargadoLocal,
    CargoLocal = T.CargoLocal,
    FormatoLocal = T.FormatoLocal,
    NroTransporte = T.NroTransporte,
    NombreLocal = T.LocalDestino,
    CodigoLocal = T.LocalDestinoCodigo
  FROM
    @TablaAlerta AS T
  WHERE
    T.NombreEncargadoLocal IS NOT NULL
  ORDER BY
    T.NombreEncargadoLocal

END

 
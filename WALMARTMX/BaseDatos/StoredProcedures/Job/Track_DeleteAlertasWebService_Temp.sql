﻿
/****** Object:  StoredProcedure [dbo].[Track_DeleteAlertasWebService_Temp]    Script Date: 07/03/2015 16:14:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Track_DeleteAlertasWebService_Temp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Track_DeleteAlertasWebService_Temp]
GO

CREATE Procedure [dbo].[Track_DeleteAlertasWebService_Temp]@Response varchar(255) output, @Id int
	
AS

Begin Try

	Delete From AlertaWebService_Temp Where Id = @Id

	Set @Response = 'Ok'
	Select @Response

End	Try

Begin Catch
	
	Insert into Track_Error(Fecha, NombreObjeto, MensajeError)   
	Values (getdate(), 
			'SP: Track_DeleteAlertasWebService_Temp',
			ERROR_MESSAGE())
			
	Set @Response = 'Error'		
	Select @Response	
End Catch





GO



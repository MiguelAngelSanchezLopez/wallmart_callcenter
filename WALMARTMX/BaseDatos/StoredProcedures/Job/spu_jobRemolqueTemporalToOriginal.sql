/**
**  Autor		:	Mauricio Espinoza A.
**	Fecha		:	10-02-2017
**	Descripción	:	Inserta los registros nuevos de tracto
**/
CREATE PROCEDURE [dbo].[spu_jobRemolqueTemporalToOriginal]
AS
Begin
	Begin try
		INSERT INTO InformacionRemolque (
			   Id_Embarque
			  ,Determinante_WMS
			  ,Cortina
			  ,Tipo_Servicio
			  ,Tipo_Entrega
			  ,Numero_Entregas
			  ,Tipo_Flota
			  ,Id_Unidad_Embarque
			  ,Clave_MTS
			  ,Tipo_Unidad
			  ,Status
			  ,[1]
			  ,[2]
			  ,[3]
			  ,[4]
			  ,[5]
			  ,[6]
			  ,[7]
			  ,[8]
			  ,[9]
			  ,[10]
			  ,[11]
			  ,[12]
			  ,[13]
			  ,[14]
			  ,[15]
			  ,Fecha_Solicitud
			  ,Marchamo1
			  ,Marchamo2 
		)
		SELECT  distinct
			   t.Id_Embarque
			  ,t.Determinante_WMS
			  ,t.Cortina
			  ,t.Tipo_Servicio
			  ,t.Tipo_Entrega
			  ,t.Numero_Entregas
			  ,t.Tipo_Flota
			  ,t.Id_Unidad_Embarque
			  ,t.Clave_MTS
			  ,t.Tipo_Unidad
			  ,t.Status
			  ,t.[1]
			  ,t.[2]
			  ,t.[3]
			  ,t.[4]
			  ,t.[5]
			  ,t.[6]
			  ,t.[7]
			  ,t.[8]
			  ,t.[9]
			  ,t.[10]
			  ,t.[11]
			  ,t.[12]
			  ,t.[13]
			  ,t.[14]
			  ,t.[15]
			  ,t.Fecha_Solicitud
			  ,t.Marchamo1
			  ,t.Marchamo2 
		FROM 
			InformacionRemolqueTemporal t LEFT JOIN 
			InformacionRemolque r On
				t.Id_Embarque   = r.Id_Embarque AND
				IsNuLL(t.Status,'')		= isnull(r.Status,'') AND
				IsNull(t.Marchamo1, -1)		= IsNull(r.Marchamo1,-1) AND
				Isnull(t.Marchamo2, -1)		= Isnull(r.Marchamo2, -1) 
		Where
			r.Id_Embarque Is Null
	End try
	begin catch
		insert into Track_Error ( 
			Fecha,
			NombreObjeto,
			MensajeError
		)
		Values (
			dbo.fnu_GETDATE(),
			'spu_jobRemolqueTemporalToOriginal',
			'Error: ' + error_message()
		)
	end catch	
		
	truncate table InformacionRemolqueTemporal
	


End
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Job_Reclamos')
BEGIN
  PRINT  'Dropping Procedure spu_Job_Reclamos'
  DROP  Procedure  dbo.spu_Job_Reclamos
END

GO

PRINT  'Creating Procedure spu_Job_Reclamos'
GO
CREATE Procedure dbo.spu_Job_Reclamos
/******************************************************************************
**    Descripcion  : obtiene listado de reclamos
**    Por          : VSR, 30/01/2015
*******************************************************************************/
@FechaISOInicio VARCHAR(8),
@FechaISOTermino VARCHAR(8)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaISOInicio = '-1') SET @FechaISOInicio = '20140101'
  IF (@FechaISOTermino = '-1') SET @FechaISOTermino = '21001231'

  SELECT TOP 10
  *
  FROM
    Usuario

END

 
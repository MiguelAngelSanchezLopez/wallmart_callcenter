IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Configuracion_Leer')
BEGIN
  PRINT  'Dropping Procedure spu_Configuracion_Leer'
  DROP  Procedure  dbo.spu_Configuracion_Leer
END

GO

PRINT  'Creating Procedure spu_Configuracion_Leer'
GO
CREATE Procedure [dbo].[spu_Configuracion_Leer] (  
@Clave varchar(50)   
) AS  
BEGIN  
   
 SELECT Valor FROM Configuracion WHERE Clave = @Clave  
   
END  
GO  
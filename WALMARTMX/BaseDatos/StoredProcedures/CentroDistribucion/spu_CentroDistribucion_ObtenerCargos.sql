﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_CentroDistribucion_ObtenerCargos')
BEGIN
  PRINT  'Dropping Procedure spu_CentroDistribucion_ObtenerCargos'
  DROP  Procedure  dbo.spu_CentroDistribucion_ObtenerCargos
END

GO

PRINT  'Creating Procedure spu_CentroDistribucion_ObtenerCargos'
GO
CREATE Procedure dbo.spu_CentroDistribucion_ObtenerCargos
/******************************************************************************
**    Descripcion  : obtiene los cargos asociados al centro distribucion
**    Por          : VSR, 10/04/2015
*******************************************************************************/
@IdCentroDistribucion INT,
@Categoria VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  ---------------------------------------------------------------
  -- obtiene todos los usuarios segun los perfiles asociados al centro distribucion
  ---------------------------------------------------------------
  DECLARE @TablaUsuarios AS TABLE(IdUsuario INT, NombreUsuario VARCHAR(255), IdPerfil INT, NombrePerfil VARCHAR(255), LlavePerfil VARCHAR(255), Seleccionado INT)
  INSERT INTO @TablaUsuarios
  (
	  IdUsuario,
	  NombreUsuario,
	  IdPerfil,
	  NombrePerfil,
	  LlavePerfil,
	  Seleccionado
  )
  SELECT
    IdUsuario = U.Id,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    IdPerfil = P.Id,
    NombrePerfil = ISNULL(P.Nombre,''),
    LlavePerfil = ISNULL(P.Llave,''),
    Seleccionado = CASE WHEN CDPU.IdUsuario IS NOT NULL THEN 1 ELSE 0 END
  FROM
    PerfilesPorCentroDistribucion AS PPCD
    INNER JOIN Usuario AS U ON (PPCD.IdPerfil = U.IdPerfil)
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    LEFT JOIN CentroDistribucionPorUsuario AS CDPU ON (U.Id = CDPU.IdUsuario AND CDPU.IdCentroDistribucion = @IdCentroDistribucion)
  ORDER BY
    LlavePerfil, NombreUsuario


  ---------------------------------------------------------------
  -- TABLA 0: Perfiles  
  ---------------------------------------------------------------
  SELECT DISTINCT
	  T.IdPerfil,
	  T.NombrePerfil,
	  T.LlavePerfil
  FROM
    @TablaUsuarios AS T
  ORDER BY
    T.NombrePerfil

  ---------------------------------------------------------------
  -- TABLA 1: Listado Usuarios  
  ---------------------------------------------------------------
  SELECT
    T.*
  FROM
    @TablaUsuarios AS T
  ORDER BY
    T.LlavePerfil, T.NombreUsuario


END
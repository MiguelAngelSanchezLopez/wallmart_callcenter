﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_CentroDistribucion_GrabarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_CentroDistribucion_GrabarDatos'
  DROP  Procedure  dbo.spu_CentroDistribucion_GrabarDatos
END

GO

PRINT  'Creating Procedure spu_CentroDistribucion_GrabarDatos'
GO
CREATE Procedure dbo.spu_CentroDistribucion_GrabarDatos
/******************************************************************************
**  Descripcion  : graba los datos del centro distribucion
**  Fecha        : 04/09/2014
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdCentroDistribucionOutput AS INT OUTPUT,
@IdCentroDistribucion AS INT,
@Nombre AS VARCHAR(255),
@CodigoInterno AS INT,
@Activo AS INT,
@ListadoUsuariosCargos VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    -- ingresa nuevo registro
    IF (@IdCentroDistribucion = -1) BEGIN
      INSERT INTO CentroDistribucion
      (
	      Nombre,
	      CodigoInterno,
	      FechaCreacion,
	      Activo
      )
      VALUES
      (
	      @Nombre,
	      @CodigoInterno,
	      dbo.fnu_GETDATE(),
	      @Activo
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdCentroDistribucionOutput = @IdCentroDistribucion
        RETURN
      END

      SET @Status = 'ingresado'
      SET @IdCentroDistribucionOutput = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE CentroDistribucion
      SET
	      Nombre = @Nombre,
	      CodigoInterno = @CodigoInterno,
	      Activo = @Activo
      WHERE
        Id = @IdCentroDistribucion

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdCentroDistribucionOutput = @IdCentroDistribucion
        RETURN
      END

      SET @Status = 'modificado'
      SET @IdCentroDistribucionOutput = @IdCentroDistribucion

    END

    ----------------------------------------------------------------
    -- elimina todas las referencias de usuarios y carga los nuevos
    ----------------------------------------------------------------
    DELETE FROM CentroDistribucionPorUsuario WHERE IdCentroDistribucion = @IdCentroDistribucionOutput 

    IF (@ListadoUsuariosCargos <> '') BEGIN
      INSERT INTO CentroDistribucionPorUsuario(IdCentroDistribucion,IdUsuario)
      SELECT @IdCentroDistribucionOutput, valor FROM dbo.fnu_InsertaListaTabla(@ListadoUsuariosCargos)
    END

  COMMIT
END
GO        
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_CentroDistribucion_EliminarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_CentroDistribucion_EliminarDatos'
  DROP  Procedure  dbo.spu_CentroDistribucion_EliminarDatos
END

GO

PRINT  'Creating Procedure spu_CentroDistribucion_EliminarDatos'
GO
CREATE Procedure dbo.spu_CentroDistribucion_EliminarDatos
/******************************************************************************
**  Descripcion  : elimina un centro distribucion
**  Fecha        : 04/09/2014
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdCentroDistribucion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM CentroDistribucion WHERE Id = @IdCentroDistribucion

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO           
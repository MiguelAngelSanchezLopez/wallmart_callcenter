﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_CentroDistribucion_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_CentroDistribucion_ObtenerDetalle'
  DROP  Procedure  dbo.spu_CentroDistribucion_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_CentroDistribucion_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_CentroDistribucion_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle del centro distribucion consultado
**    Por          : VSR, 03/09/2014
*******************************************************************************/
@IdCentroDistribucion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  ----------------------------------------------------------------
  -- TABLA 0: DETALLE
  ----------------------------------------------------------------
  SELECT
    IdCentroDistribucion = CD.Id,
    NombreCentroDistribucion = ISNULL(CD.Nombre,''),
    CodigoInterno = ISNULL(CD.CodigoInterno,''),
    Activo = CONVERT(INT, CD.Activo)
  FROM
    CentroDistribucion AS CD
  WHERE
    CD.Id = @IdCentroDistribucion
  ORDER BY
    IdCentroDistribucion DESC

END



   
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_CentroDistribucion_VerificarDuplicidadCodigo')
BEGIN
  PRINT  'Dropping Procedure spu_CentroDistribucion_VerificarDuplicidadCodigo'
  DROP  Procedure  dbo.spu_CentroDistribucion_VerificarDuplicidadCodigo
END

GO

PRINT  'Creating Procedure spu_CentroDistribucion_VerificarDuplicidadCodigo'
GO
CREATE Procedure dbo.spu_CentroDistribucion_VerificarDuplicidadCodigo
/******************************************************************************
**  Descripcion  : valida que el codigo del centro distribucion sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 04/09/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdCentroDistribucion AS INT,
@Valor AS VARCHAR(1028)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdCentroDistribucion IS NULL OR @IdCentroDistribucion = 0) SET @IdCentroDistribucion = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdCentroDistribucion = -1) BEGIN
    IF( EXISTS(
                SELECT
                  CD.CodigoInterno
                FROM
                  CentroDistribucion AS CD
                WHERE
                  ISNULL(CD.CodigoInterno,'-200') = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  CD.CodigoInterno
                FROM
                  CentroDistribucion AS CD
                WHERE
                    ISNULL(CD.CodigoInterno,'-200') = @Valor
                AND CD.Id <> @IdCentroDistribucion
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO       
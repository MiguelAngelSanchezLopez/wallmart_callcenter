﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Archivo_ObtenerDetallePorId')
BEGIN
  PRINT  'Dropping Procedure spu_Archivo_ObtenerDetallePorId'
  DROP  Procedure  dbo.spu_Archivo_ObtenerDetallePorId
END

GO

PRINT  'Creating Procedure spu_Archivo_ObtenerDetallePorId'
GO
CREATE Procedure dbo.spu_Archivo_ObtenerDetallePorId
/******************************************************************************
**    Descripcion  : Obtiene informacion del archivo
**    Autor        : VSR
**    Fecha        : 16/12/2014
*******************************************************************************/
@IdArchivo AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    IdArchivo = A.Id,
    Nombre = A.Nombre,
    Ruta = A.Ruta,
    Tamano = A.Tamano,
    Extension = A.Extension,
    ContentType = A.ContentType,
    Contenido = A.Contenido,
    Orden = A.Orden,
    FechaCreacion = A.FechaCreacion,
    HoraCreacion = A.HoraCreacion,
    IdUsuario = A.IdUsuario
  FROM
    Archivo AS A
  WHERE
    A.Id = @IdArchivo

END




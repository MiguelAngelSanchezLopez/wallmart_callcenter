﻿  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_VerificarDuplicidadNroTransporte')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_VerificarDuplicidadNroTransporte'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_VerificarDuplicidadNroTransporte
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_VerificarDuplicidadNroTransporte'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_VerificarDuplicidadNroTransporte
/******************************************************************************
**  Descripcion  : valida que el NroTransporte sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : VSR, 23/07/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdPlanificacion AS INT,
@Valor AS VARCHAR(20),
@NroTransporteValorActual AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdPlanificacion IS NULL OR @IdPlanificacion=0) SET @IdPlanificacion = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdPlanificacion = -1) BEGIN
    IF( EXISTS(
                SELECT
                  PT.NroTransporte
                FROM
                  PlanificacionTransportista AS PT
                WHERE
                  PT.NroTransporte = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                -----------------------------------------------------------------------
                --  obtiene todos los registros que no pertenezcan al idPlanificacion
                -----------------------------------------------------------------------
                SELECT
                  PT.NroTransporte
                FROM
                  PlanificacionTransportista AS PT
                WHERE
                    PT.NroTransporte = @Valor
                AND PT.IdPlanificacion <> @IdPlanificacion
      
                -----------------------------------------------------------------------
                --  obtiene todos los registros que pertenecen a la planificacion del nroTransporteActual menos el
                -----------------------------------------------------------------------
                UNION ALL
                SELECT
                  T.NroTransporte
                FROM (
                  SELECT
                    PT.NroTransporte
                  FROM
                    PlanificacionTransportista AS PT
                  WHERE
                      PT.IdPlanificacion = @IdPlanificacion
                  AND PT.NroTransporte <> @NroTransporteValorActual
                ) AS T
                WHERE 
                  T.NroTransporte = @Valor
              )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO      
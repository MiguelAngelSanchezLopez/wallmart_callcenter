﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_GuardarNuevo')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_GuardarNuevo'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_GuardarNuevo
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_GuardarNuevo'
GO

CREATE PROCEDURE [dbo].[spu_PlanificacionTransportista_GuardarNuevo]
/******************************************************************************
**    Descripcion  : Agrega un nuevo registro a la tabla Planificacion
**    Autor        : VSR
**    Fecha        : 03/06/2015
*******************************************************************************/
@Id AS INT OUTPUT,
@IdPlanificacion AS INT,
@IdLocal AS INT,
@IdUsuarioTransportista AS INT,
@NroTransporte AS INT,
@FechaPresentacion AS DATETIME,
@Carga AS VARCHAR(255),
@OrdenEntrega AS INT,
@TipoCamion AS VARCHAR(255),
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255),
@NombreConductor AS VARCHAR(255),
@RutConductor AS VARCHAR(255),
@Celular AS VARCHAR(255),
@CerradoPorTransportista AS BIT,
@Anden AS VARCHAR(255),
@FechaAsignacionAnden AS DATETIME,
@IdUsuarioAsignacionAnden AS INT,
@ConfirmacionAnden AS BIT,
@FechaConfirmacionAnden AS DATETIME,
@IdUsuarioConfirmacionAnden AS INT,
@Sello AS BIGINT,
@FechaLecturaSello AS DATETIME,
@IdUsuarioLecturaSello AS INT,
@Finalizada AS BIT,
@Criterio AS VARCHAR(255),
@Observacion AS VARCHAR(500),
@BloqueadoPorFueraHorario AS BIT,
@FechaBloqueoFueraHorario AS DATETIME
AS
BEGIN

  IF (@IdPlanificacion = -1) SET @IdPlanificacion = NULL
  IF (@IdLocal = -1) SET @IdLocal = NULL
  IF (@IdUsuarioTransportista = -1) SET @IdUsuarioTransportista = NULL
  IF (@NroTransporte = -1) SET @NroTransporte = NULL
  IF (@OrdenEntrega = -1) SET @OrdenEntrega = NULL
  IF (CONVERT(VARCHAR, @FechaAsignacionAnden, 112) = '19000101') SET @FechaAsignacionAnden = NULL
  IF (@IdUsuarioAsignacionAnden = -1) SET @IdUsuarioAsignacionAnden = NULL
  IF (CONVERT(VARCHAR, @FechaConfirmacionAnden, 112) = '19000101') SET @FechaConfirmacionAnden = NULL
  IF (@IdUsuarioConfirmacionAnden = -1) SET @IdUsuarioConfirmacionAnden = NULL
  IF (@Sello = -1) SET @Sello = NULL
  IF (CONVERT(VARCHAR, @FechaLecturaSello, 112) = '19000101') SET @FechaLecturaSello = NULL
  IF (@IdUsuarioLecturaSello = -1) SET @IdUsuarioLecturaSello = NULL
  IF (@Criterio = 'Por Planificar' AND @PatenteTracto != '' ) SET @Criterio = 'Confirmo llegada'
  IF (CONVERT(VARCHAR, @FechaBloqueoFueraHorario, 112) = '19000101') SET @FechaBloqueoFueraHorario = NULL

  INSERT INTO PlanificacionTransportista
  (
	  IdPlanificacion,
	  IdLocal,
	  IdUsuarioTransportista,
	  NroTransporte,
	  FechaPresentacion,
	  Carga,
	  OrdenEntrega,
	  TipoCamion,
	  PatenteTracto,
    PatenteTrailer,
	  NombreConductor,
    RutConductor,
    Celular,
    CerradoPorTransportista,
    Anden,
    FechaAsignacionAnden,
    IdUsuarioAsignacionAnden,
    ConfirmacionAnden,
    FechaConfirmacionAnden,
    IdUsuarioConfirmacionAnden,
    Sello,
    FechaLecturaSello,
    IdUsuarioLecturaSello,
    Finalizada,
    Criterio,
    Observacion,
    BloqueadoPorFueraHorario,
    FechaBloqueoFueraHorario
  )
  VALUES
  (
	  @IdPlanificacion,
	  @IdLocal,
	  @IdUsuarioTransportista,
	  @NroTransporte,
	  @FechaPresentacion,
	  @Carga,
	  @OrdenEntrega,
	  @TipoCamion,
	  @PatenteTracto,
    @PatenteTrailer,
	  @NombreConductor,
    @RutConductor,
    @Celular,
    @CerradoPorTransportista,
    @Anden,
    @FechaAsignacionAnden,
    @IdUsuarioAsignacionAnden,
    @ConfirmacionAnden,
    @FechaConfirmacionAnden,
    @IdUsuarioConfirmacionAnden,
    @Sello,
    @FechaLecturaSello,
    @IdUsuarioLecturaSello,
    @Finalizada,
    @Criterio,
    @Observacion,
    @BloqueadoPorFueraHorario,
    @FechaBloqueoFueraHorario
  )

  SET @Id = SCOPE_IDENTITY()

END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_ObtenerAsignacionCamionesTransportista')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_ObtenerAsignacionCamionesTransportista'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_ObtenerAsignacionCamionesTransportista
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_ObtenerAsignacionCamionesTransportista'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_ObtenerAsignacionCamionesTransportista
/******************************************************************************
**    Descripcion  : obtiene la asignacion de camiones realizada por el transportista
**    Por          : VSR, 10/06/2015
*******************************************************************************/
@listIdUsuarioTransportista AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @FechaDesde VARCHAR(255), @FechaHasta VARCHAR(255)

  SELECT
    [LINEA DE TRANSPORTE] = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Materno,'') + ISNULL(' ' + U.Paterno,''),
    [RUT LINEA DE TRANSPORTE] = ISNULL(U.Rut,'') + CASE WHEN ISNULL(U.DV,'') = '' THEN '' ELSE '-' + ISNULL(U.DV,'') END,
	  [FECHA PRESENTACION] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(PT.FechaPresentacion, 'FECHA_COMPLETA'),
	  [IDMASTER] = PT.NroTransporte,
	  [ORDEN ENTREGA] = ISNULL(PT.OrdenEntrega,0),
    [TIENDA] = ISNULL(L.Nombre,''),
    [DETERMINANTE] = ISNULL(L.CodigoInterno,''),
	  [CARGA] = ISNULL(PT.Carga,''),
	  [TIPO CAMION] = ISNULL(PT.TipoCamion,''),
	  [PLACA TRACTO] = ISNULL(PT.PatenteTracto,''),
	  [PLACA REMOLQUE] = ISNULL(PT.PatenteTrailer,''),
	  [OPERADOR] = ISNULL(PT.NombreConductor,''),
	  [RUT OPERADOR] = '''' + ISNULL(PT.RutConductor,''),
	  [CELULAR] = ISNULL(PT.Celular,'')
  FROM
	  PlanificacionTransportista AS PT
    INNER JOIN Local AS L ON (PT.IdLocal = L.Id)
    INNER JOIN Usuario AS U ON (PT.IdUsuarioTransportista = U.Id)
  WHERE
      (PT.Id IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@listIdUsuarioTransportista)))
  ORDER BY
    PT.FechaPresentacion, NroTransporte, OrdenEntrega


END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_AutocompletarLocales')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_AutocompletarLocales'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_AutocompletarLocales
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_AutocompletarLocales'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_AutocompletarLocales
/******************************************************************************
**    Descripcion  : obtiene listado de locales para poder autocompletar la busqueda
**    Autor        : VSR
**    Fecha        : 22/07/2015
*******************************************************************************/
@Texto AS VARCHAR(1024)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT TOP 10
    IdLocal = L.Id,
    NombreLocal = L.Nombre + ISNULL(' - ' + CONVERT(VARCHAR,L.CodigoInterno),'')
  FROM
    Local AS L
  WHERE
     L.Nombre LIKE '%'+ @Texto +'%'
  OR L.CodigoInterno LIKE '%'+ @Texto +'%'
  ORDER BY
    NombreLocal

END
GO   
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_EliminarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_EliminarDatos'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_EliminarDatos
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_EliminarDatos'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_EliminarDatos
/******************************************************************************
**  Descripcion  : elimina planificacion de un transportista
**  Fecha        : 10/06/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdPlanificacion AS INT,
@NroTransporte AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM PlanificacionTransportista
    WHERE
        IdPlanificacion = @IdPlanificacion
    AND NroTransporte = @NroTransporte

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO           
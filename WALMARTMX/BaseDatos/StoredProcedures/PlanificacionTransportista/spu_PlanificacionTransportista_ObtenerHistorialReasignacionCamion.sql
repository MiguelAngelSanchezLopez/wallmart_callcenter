﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_ObtenerHistorialReasignacionCamion')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_ObtenerHistorialReasignacionCamion'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_ObtenerHistorialReasignacionCamion
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_ObtenerHistorialReasignacionCamion'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_ObtenerHistorialReasignacionCamion
/******************************************************************************
**    Descripcion  : obtiene el historial de reasignacion del camion
**    Por          : VSR, 10/06/2015
*******************************************************************************/
@IdPlanificacion AS INT,
@NroTransporte AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    NroTransporte = PHRC.NroTransporte,
    FechaModificacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(PHRC.FechaModificacion, 'FECHA_COMPLETA'),
	  CampoModificado = PHRC.CampoModificado,
	  Observacion = ISNULL(PHRC.Observacion,''),
	  ModificadoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Materno,'') + ISNULL(' ' + U.Paterno,'')
  FROM
	  PlanificacionHistorialReasignacionCamion AS PHRC
    INNER JOIN Usuario AS U ON (PHRC.IdUsuarioModificacion = U.Id)
  WHERE
      PHRC.IdPlanificacion = @IdPlanificacion
	AND PHRC.NroTransporte = @NroTransporte
  ORDER BY
    PHRC.FechaModificacion DESC

END



  
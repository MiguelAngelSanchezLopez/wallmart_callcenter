﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_AsociarHistorialAlNuevoNroTransporte')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_AsociarHistorialAlNuevoNroTransporte'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_AsociarHistorialAlNuevoNroTransporte
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_AsociarHistorialAlNuevoNroTransporte'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_AsociarHistorialAlNuevoNroTransporte
/******************************************************************************
**  Descripcion  : asocia el historial al nuevo NroTransporte
**  Fecha        : VSR, 23/07/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@NroTransporteActual AS INT,
@NuevoNroTransporte AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    INSERT INTO PlanificacionHistorialReasignacionCamion
    (
	    IdPlanificacion,
	    NroTransporte,
	    CampoModificado,
	    Observacion,
	    IdUsuarioModificacion,
	    FechaModificacion
    )
    SELECT
	    IdPlanificacion,
	    @NuevoNroTransporte,
	    CampoModificado,
	    Observacion,
	    IdUsuarioModificacion,
	    FechaModificacion
    FROM
	    PlanificacionHistorialReasignacionCamion
    WHERE
      NroTransporte = @NroTransporteActual

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    -- retorna que todo estuvo bien
    SET @Status = 1

  COMMIT
END
GO
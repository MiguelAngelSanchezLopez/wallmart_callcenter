﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_Actualizar')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_Actualizar'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_Actualizar
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_Actualizar'
GO

CREATE PROCEDURE [dbo].[spu_PlanificacionTransportista_Actualizar]
/******************************************************************************
**    Descripcion  : Actualiza registro a la tabla Planificacion
**    Autor        : VSR
**    Fecha        : 03/06/2015
*******************************************************************************/
@Id AS INT,
@IdPlanificacion AS INT,
@IdLocal AS INT,
@IdUsuarioTransportista AS INT,
@NroTransporte AS INT,
@FechaPresentacion AS DATETIME,
@Carga AS VARCHAR(255),
@OrdenEntrega AS INT,
@TipoCamion AS VARCHAR(255),
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255),
@NombreConductor AS VARCHAR(255),
@RutConductor AS VARCHAR(255),
@Celular AS VARCHAR(255),
@CerradoPorTransportista AS BIT,
@Anden AS VARCHAR(255),
@FechaAsignacionAnden AS DATETIME,
@IdUsuarioAsignacionAnden AS INT,
@ConfirmacionAnden AS BIT,
@FechaConfirmacionAnden AS DATETIME,
@IdUsuarioConfirmacionAnden AS INT,
@Sello AS BIGINT,
@FechaLecturaSello AS DATETIME,
@IdUsuarioLecturaSello AS INT,
@Finalizada AS BIT,
@Criterio AS VARCHAR(255),
@Observacion AS VARCHAR(500),
@BloqueadoPorFueraHorario AS BIT,
@FechaBloqueoFueraHorario AS DATETIME
AS
BEGIN

  IF (@IdPlanificacion = -1) SET @IdPlanificacion = NULL
  IF (@IdLocal = -1) SET @IdLocal = NULL
  IF (@IdUsuarioTransportista = -1) SET @IdUsuarioTransportista = NULL
  IF (@NroTransporte = -1) SET @NroTransporte = NULL
  IF (@OrdenEntrega = -1) SET @OrdenEntrega = NULL
  IF (CONVERT(VARCHAR, @FechaAsignacionAnden, 112) = '19000101') SET @FechaAsignacionAnden = NULL
  IF (@IdUsuarioAsignacionAnden = -1) SET @IdUsuarioAsignacionAnden = NULL
  IF (CONVERT(VARCHAR, @FechaConfirmacionAnden, 112) = '19000101') SET @FechaConfirmacionAnden = NULL
  IF (@IdUsuarioConfirmacionAnden = -1) SET @IdUsuarioConfirmacionAnden = NULL
  IF (@Sello = -1) SET @Sello = NULL
  IF (CONVERT(VARCHAR, @FechaLecturaSello, 112) = '19000101') SET @FechaLecturaSello = NULL
  IF (@IdUsuarioLecturaSello = -1) SET @IdUsuarioLecturaSello = NULL
  IF (@Criterio = 'Por Planificar' AND @PatenteTracto != '' ) SET @Criterio = 'Confirmo llegada'
  IF (CONVERT(VARCHAR, @FechaBloqueoFueraHorario, 112) = '19000101') SET @FechaBloqueoFueraHorario = NULL

  UPDATE PlanificacionTransportista
  SET
	IdPlanificacion = @IdPlanificacion,
	IdLocal = @IdLocal,
	IdUsuarioTransportista = @IdUsuarioTransportista,
	NroTransporte = @NroTransporte,
	FechaPresentacion = @FechaPresentacion,
	Carga = @Carga,
	OrdenEntrega = @OrdenEntrega,
	TipoCamion = @TipoCamion,
	PatenteTracto = @PatenteTracto,
	PatenteTrailer = @PatenteTrailer,
	NombreConductor = @NombreConductor,
	RutConductor = @RutConductor,
	Celular = @Celular,
	CerradoPorTransportista = @CerradoPorTransportista,
	Anden = @Anden,
	FechaAsignacionAnden = @FechaAsignacionAnden,
	IdUsuarioAsignacionAnden = @IdUsuarioAsignacionAnden,
	ConfirmacionAnden = @ConfirmacionAnden,
	FechaConfirmacionAnden = @FechaConfirmacionAnden,
	IdUsuarioConfirmacionAnden = @IdUsuarioConfirmacionAnden,
	Sello = @Sello,
	FechaLecturaSello = @FechaLecturaSello,
	IdUsuarioLecturaSello = @IdUsuarioLecturaSello,
	Finalizada = @Finalizada,
	Criterio = @Criterio,
	Observacion = @Observacion,
  BloqueadoPorFueraHorario = @BloqueadoPorFueraHorario,
  FechaBloqueoFueraHorario = @FechaBloqueoFueraHorario
  WHERE
    Id = @Id

END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_EliminarPlanificacionesNoReferenciadas')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_EliminarPlanificacionesNoReferenciadas'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_EliminarPlanificacionesNoReferenciadas
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_EliminarPlanificacionesNoReferenciadas'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_EliminarPlanificacionesNoReferenciadas
/******************************************************************************
**  Descripcion  : elimina las planificaciones que no estan referenciadas
**  Fecha        : 23/07/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdPlanificacion AS INT,
@NroTransporte AS INT,
@Listado AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM PlanificacionTransportista
    WHERE 
        IdPlanificacion = @IdPlanificacion
    AND NroTransporte = @NroTransporte
    AND Id NOT IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    -- retorna que todo estuvo bien
    SET @Status = 1

  COMMIT
END
GO
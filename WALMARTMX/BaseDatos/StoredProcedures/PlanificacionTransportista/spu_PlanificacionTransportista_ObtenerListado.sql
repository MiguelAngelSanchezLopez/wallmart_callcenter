﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_ObtenerListado')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_ObtenerListado'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_ObtenerListado
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_ObtenerListado'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_ObtenerListado
/******************************************************************************
**    Descripcion  : obtiene planificacion asociada al transportista
**    Por          : VSR, 03/06/2015
*******************************************************************************/
@IdUsuario  AS INT,
@NroTransporte AS INT,
@FechaPresentacion AS VARCHAR(255),
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255),
@Carga AS VARCHAR(255),
@CodLocal AS INT,
@HoraPresentacion AS VARCHAR(5),
@MinutoPresentacion AS VARCHAR(5)


AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @FechaDesde VARCHAR(255), @FechaHasta VARCHAR(255)

  IF (@FechaPresentacion = '-1') 
  BEGIN
    SET @FechaDesde = '01/01/2015 00:00'
    SET @FechaHasta = '31/12/2100 23:59'
  END 
  ELSE 
  BEGIN
  	SET @FechaDesde = @FechaPresentacion 
  	SET @FechaHasta = @FechaPresentacion
  		
  	IF(@HoraPresentacion = '-1' AND @MinutoPresentacion = '-1')
  	BEGIN
  		SET @FechaDesde = @FechaDesde + ' 00:00'
  		SET @FechaHasta = @FechaHasta + ' 23:59'
  	END
  	ELSE
  	BEGIN
  		SET @FechaDesde = @FechaDesde + ' ' + @HoraPresentacion + ':' + @MinutoPresentacion
  		SET @FechaHasta = @FechaHasta + ' ' + @HoraPresentacion + ':' + @MinutoPresentacion
  	END
  END 
  	
  DECLARE @TablaPlanificacion AS TABLE(IdPlanificacionTransportista INT, IdPlanificacion INT, IdLocal INT, NombreLocal VARCHAR(255), CodigoLocal INT, IdUsuarioTransportista INT, NombreTransportista VARCHAR(255),
                                       RutTransportista VARCHAR(255), NroTransporte INT, FechaPresentacion DATETIME, Carga VARCHAR(255), OrdenEntrega INT, TipoCamion VARCHAR(255), PatenteTracto VARCHAR(255), 
                                       PatenteTrailer VARCHAR(255), NombreConductor VARCHAR(255), RutConductor VARCHAR(255), Celular VARCHAR(255), CerradoPorTransportista INT, Anden VARCHAR(255), Criterio VARCHAR(255),
                                       Observacion VARCHAR(500), Finalizada BIT, ConfirmacionAnden BIT, BloqueadoPorFueraHorario BIT, FechaBloqueoFueraHorario DATETIME)

  INSERT INTO @TablaPlanificacion(IdPlanificacionTransportista, IdPlanificacion, IdLocal, NombreLocal, CodigoLocal, IdUsuarioTransportista, NombreTransportista, RutTransportista, NroTransporte, FechaPresentacion,
                                  Carga, OrdenEntrega, TipoCamion, PatenteTracto, PatenteTrailer, NombreConductor, RutConductor, Celular, CerradoPorTransportista, Anden, Criterio, Observacion, Finalizada, ConfirmacionAnden,
                                  BloqueadoPorFueraHorario, FechaBloqueoFueraHorario)
  SELECT
	IdPlanificacionTransportista = PT.Id,
	IdPlanificacion = PT.IdPlanificacion,
	IdLocal = PT.IdLocal,
	NombreLocal = ISNULL(L.Nombre,''),
	CodigoLocal = ISNULL(L.CodigoInterno,''),
	IdUsuarioTransportista = PT.IdUsuarioTransportista,
	NombreTransportista = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Materno,'') + ISNULL(' ' + U.Paterno,''),
	RutTransportista = ISNULL(U.Rut,'') + CASE WHEN ISNULL(U.DV,'') = '' THEN '' ELSE '-' + ISNULL(U.DV,'') END,
	NroTransporte = PT.NroTransporte,
	FechaPresentacion = PT.FechaPresentacion,
	Carga = ISNULL(PT.Carga,''),
	OrdenEntrega = ISNULL(PT.OrdenEntrega,0),
	TipoCamion = ISNULL(PT.TipoCamion,''),
	PatenteTracto = ISNULL(PT.PatenteTracto,''),
	PatenteTrailer = ISNULL(PT.PatenteTrailer,''),
	NombreConductor = ISNULL(PT.NombreConductor,''),
	RutConductor = ISNULL(PT.RutConductor,''),
	Celular = ISNULL(PT.Celular,''),
	CerradoPorTransportista = CASE WHEN (ISNULL(PT.CerradoPorTransportista,0) = 1 OR ISNULL(PT.BloqueadoPorFueraHorario,0) = 1) THEN 1 ELSE
							-- bloquea el registro 2 horas antes de la fecha presentacion
							CASE WHEN DATEDIFF(mi, dbo.fnu_GETDATE(), PT.FechaPresentacion) <= 120 THEN 1 ELSE 0 END
						  END,
	Anden = ISNULL(PT.Anden,''),
	Criterio = ISNULL(PT.criterio,''),
	Observacion = ISNULL(PT.Observacion,''),
	Finalizada = PT.Finalizada,
	ConfirmacionAnden = PT.ConfirmacionAnden,
	BloqueadoPorFueraHorario = ISNULL(PT.BloqueadoPorFueraHorario,0),
	FechaBloqueoFueraHorario = PT.FechaBloqueoFueraHorario
  FROM
	PlanificacionTransportista AS PT
    INNER JOIN Local AS L ON (PT.IdLocal = L.Id)
    INNER JOIN Usuario AS U ON (PT.IdUsuarioTransportista = U.Id)
    
  WHERE
	(@IdUsuario = -1 OR U.Id = @IdUsuario)
	AND (@NroTransporte = -1 OR PT.NroTransporte = @NroTransporte)
	AND (PT.FechaPresentacion BETWEEN @FechaDesde AND @FechaHasta)
	AND (@PatenteTracto = '-1' OR PT.PatenteTracto = @PatenteTracto)
	AND (@PatenteTrailer = '-1' OR PT.PatenteTrailer = @PatenteTrailer)
	AND (@Carga = '-1' OR PT.Carga = @Carga)
	AND (@CodLocal = -1 OR PT.NroTransporte IN (SELECT nroTransporte 
	                                            FROM PlanificacionTransportista PT1
	                                            INNER JOIN local L1 ON l1.Id = pt1.IdLocal
	                                            WHERE L1.CodigoInterno = @CodLocal))

  ---------------------------------------------------------------
  -- TABLA 0: obtiene planificacion por NroTransporte
  ---------------------------------------------------------------
  SELECT DISTINCT
    P.IdPlanificacion,
	  P.NroTransporte,
	  FechaPresentacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(P.FechaPresentacion, 'FECHA_COMPLETA'),
	  P.IdUsuarioTransportista,
    P.NombreTransportista,
    P.RutTransportista,
	  P.Carga,
	  P.TipoCamion,
	  P.PatenteTracto,
    P.PatenteTrailer,
	  P.NombreConductor,
	  P.RutConductor,
    P.Celular,
    P.CerradoPorTransportista,
    P.Anden,
    P.Criterio,
    P.Observacion,
    P.Finalizada,
    P.ConfirmacionAnden,
    P.BloqueadoPorFueraHorario,
    FechaBloqueoFueraHorario = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(P.FechaBloqueoFueraHorario, 'FECHA_COMPLETA'),''),
    FechaOrden = P.FechaPresentacion
  FROM
	  @TablaPlanificacion AS P
  ORDER BY
    P.IdPlanificacion, FechaOrden, P.RutTransportista, P.NroTransporte

  ---------------------------------------------------------------
  -- TABLA 1: listado de locales por NroTransporte
  ---------------------------------------------------------------
  SELECT
    P.IdPlanificacion,
	  P.NroTransporte,
	  P.IdPlanificacionTransportista,
	  P.IdLocal,
    P.NombreLocal,
    P.CodigoLocal,
	  P.OrdenEntrega
  FROM
	  @TablaPlanificacion AS P
  ORDER BY
    NroTransporte, OrdenEntrega

END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_BloquearPlanificacionTransportistaFueraHorario')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_BloquearPlanificacionTransportistaFueraHorario'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_BloquearPlanificacionTransportistaFueraHorario
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_BloquearPlanificacionTransportistaFueraHorario'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_BloquearPlanificacionTransportistaFueraHorario
/******************************************************************************
**  Descripcion  : bloquea la planificacion que esta fuera de horario
**  Fecha        : VSR, 28/07/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@Listado AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    UPDATE PlanificacionTransportista
    SET
	    BloqueadoPorFueraHorario = 1,
	    FechaBloqueoFueraHorario = dbo.fnu_GETDATE()
    WHERE
      Id IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@Listado))

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    -- retorna que todo estuvo bien
    SET @Status = 1

  COMMIT
END
GO
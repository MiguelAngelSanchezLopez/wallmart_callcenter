﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_PlanificacionTransportista_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_PlanificacionTransportista_ObtenerDetalle'
  DROP  Procedure  dbo.spu_PlanificacionTransportista_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_PlanificacionTransportista_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_PlanificacionTransportista_ObtenerDetalle
/******************************************************************************
**    Descripcion  : obtiene detalle de la planificacion
**    Por          : VSR, 22/07/2015
*******************************************************************************/
@IdPlanificacion AS INT,
@NroTransporte AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @TablaPlanificacion AS TABLE(
    IdPlanificacionTransportista INT,
    IdLocal INT,
    FechaPresentacion DATETIME,
    NombreLocal VARCHAR(255),
    CodigoLocal INT,
    IdUsuarioTransportista INT,
    Carga VARCHAR(255),
    OrdenEntrega INT,
    TipoCamion VARCHAR(255),
    PatenteTracto VARCHAR(255),
    PatenteTrailer VARCHAR(255),
    NombreConductor VARCHAR(255),
    RutConductor VARCHAR(255),
    Celular VARCHAR(255),
    NroTransporte INT,
    ExisteTrazaVIaje INT
  )

  INSERT INTO @TablaPlanificacion
  (
	  IdPlanificacionTransportista,
	  IdLocal,
    FechaPresentacion,
	  NombreLocal,
	  CodigoLocal,
	  IdUsuarioTransportista,
	  Carga,
	  OrdenEntrega,
	  TipoCamion,
	  PatenteTracto,
	  PatenteTrailer,
	  NombreConductor,
	  RutConductor,
	  Celular,
    NroTransporte,
    ExisteTrazaVIaje
  )
  SELECT
	  IdPlanificacionTransportista = PT.Id,
	  IdLocal = PT.IdLocal,
    FechaPresentacion = PT.FechaPresentacion,
    NombreLocal = ISNULL(L.Nombre,''),
    CodigoLocal = ISNULL(L.CodigoInterno,-1),
	  IdUsuarioTransportista = ISNULL(PT.IdUsuarioTransportista,-1),
	  Carga = ISNULL(PT.Carga,''),
	  OrdenEntrega = ISNULL(PT.OrdenEntrega,-1),
	  TipoCamion = ISNULL(PT.TipoCamion,''),
	  PatenteTracto = ISNULL(PT.PatenteTracto,''),
	  PatenteTrailer = ISNULL(PT.PatenteTrailer,''),
	  NombreConductor = ISNULL(PT.NombreConductor,''),
	  RutConductor = ISNULL(PT.RutConductor,''),
	  Celular = ISNULL(PT.Celular,''),
    NroTransporte = PT.NroTransporte,
    ExisteTraza = CASE WHEN ISNULL(TV.IdTrazaViaje,0) = 0 THEN 0 ELSE 1 END
  FROM
	  PlanificacionTransportista AS PT
    INNER JOIN Local AS L ON (PT.IdLocal = L.Id)
    OUTER APPLY (
      SELECT TOP 1
        IdTrazaViaje = TV.Id
      FROM
        TrazaViaje AS TV
      WHERE
        TV.NroTransporte = @NroTransporte
    ) AS TV
  WHERE
      PT.IdPlanificacion = @IdPlanificacion
  AND PT.NroTransporte = @NroTransporte

  -------------------------------------------------------------------
  -- TABLA 0: Detalle Planificacion
  -------------------------------------------------------------------
  SELECT TOP 1
    PT.IdPlanificacionTransportista,
	  PT.IdUsuarioTransportista,
    FechaPresentacion = CONVERT(VARCHAR, PT.FechaPresentacion, 103),
    HoraPresentacion = LEFT(CONVERT(VARCHAR, PT.FechaPresentacion, 108), 5),
	  PT.Carga,
	  PT.TipoCamion,
	  PT.PatenteTracto,
	  PT.PatenteTrailer,
	  PT.NombreConductor,
	  PT.RutConductor,
	  PT.Celular,
    PT.ExisteTrazaVIaje
  FROM
	  @TablaPlanificacion AS PT

  -------------------------------------------------------------------
  -- TABLA 1: Listado locales
  -------------------------------------------------------------------
  SELECT
	  PT.IdPlanificacionTransportista,
	  PT.IdLocal,
    NombreLocal = PT.NombreLocal + ISNULL(' - ' + CONVERT(VARCHAR,PT.CodigoLocal),''),
	  PT.OrdenEntrega
  FROM
	  @TablaPlanificacion AS PT
  ORDER BY
    PT.OrdenEntrega

END



  
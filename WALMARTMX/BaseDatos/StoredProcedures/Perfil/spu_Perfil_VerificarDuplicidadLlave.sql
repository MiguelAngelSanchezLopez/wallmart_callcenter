﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_VerificarDuplicidadLlave')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_VerificarDuplicidadLlave'
  DROP  Procedure  dbo.spu_Perfil_VerificarDuplicidadLlave
END

GO

PRINT  'Creating Procedure spu_Perfil_VerificarDuplicidadLlave'
GO
CREATE Procedure dbo.spu_Perfil_VerificarDuplicidadLlave
/******************************************************************************
**  Descripcion  : valida que la llave del perfil sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 07/04/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdPerfil AS INT,
@Valor AS VARCHAR(1028)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdPerfil IS NULL OR @IdPerfil = 0) SET @IdPerfil = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdPerfil = -1) BEGIN
    IF( EXISTS(
                SELECT
                  P.Llave
                FROM
                  Perfil AS P
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(P.Llave,'')))) = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  P.Llave
                FROM
                  Perfil AS P
                WHERE
                    UPPER(RTRIM(LTRIM(ISNULL(P.Llave,'')))) = @Valor
                AND P.Id <> @IdPerfil
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO       
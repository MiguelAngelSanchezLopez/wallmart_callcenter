﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_ObtenerListadoConFiltro')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_ObtenerListadoConFiltro'
  DROP  Procedure  dbo.spu_Perfil_ObtenerListadoConFiltro
END

GO

PRINT  'Creating Procedure spu_Perfil_ObtenerListadoConFiltro'
GO
CREATE Procedure dbo.spu_Perfil_ObtenerListadoConFiltro
/******************************************************************************
**    Descripcion  : Obtiene listado de perfiles usando filtros de busqueda
**    Por          : VSR, 06/04/2015
*******************************************************************************/
@IdUsuario AS INT,
@Nombre AS VARCHAR(255),
@Llave AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    IdPerfil = P.Id,
    NombrePerfil = ISNULL(P.Nombre,''),
    Llave = ISNULL(P.Llave,''),
	  PaginaInicio = ISNULL(P.PaginaInicio,'')
  FROM
    Perfil AS P
  WHERE
      (@Nombre = '-1' OR P.Nombre LIKE '%'+ @Nombre +'%')
  AND (@Llave = '-1' OR P.Llave LIKE '%'+ @Llave +'%')
  ORDER BY
    IdPerfil DESC

END



  
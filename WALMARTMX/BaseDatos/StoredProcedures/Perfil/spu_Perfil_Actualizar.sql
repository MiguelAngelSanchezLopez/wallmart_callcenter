  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_Actualizar')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_Actualizar'
  DROP  Procedure  dbo.spu_Perfil_Actualizar
END

GO

PRINT  'Creating Procedure spu_Perfil_Actualizar'
GO
CREATE Procedure dbo.spu_Perfil_Actualizar
/******************************************************************************
**  Descripcion  : actualiza registro
**  Fecha        : 17/03/2009
*******************************************************************************/
@Id int,  
@Nombre varchar(50),  
@Llave varchar(50),
@PaginaInicio varchar(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  UPDATE Perfil
  SET
	  Nombre = @Nombre,
	  Llave = @Llave,
    PaginaInicio = @PaginaInicio
  WHERE
    Id = @Id
END
GO  
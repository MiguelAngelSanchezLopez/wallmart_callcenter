﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_GrabarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_GrabarDatos'
  DROP  Procedure  dbo.spu_Perfil_GrabarDatos
END

GO

PRINT  'Creating Procedure spu_Perfil_GrabarDatos'
GO
CREATE Procedure dbo.spu_Perfil_GrabarDatos
/******************************************************************************
**  Descripcion  : graba los datos del perfil
**  Fecha        : 06/04/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdPerfilOutput AS INT OUTPUT,
@IdPerfil AS INT,
@Nombre AS VARCHAR(50),
@Llave AS VARCHAR(50),
@PaginaInicio AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @NuevoIdPerfil AS INT
  SET @NuevoIdPerfil = (SELECT MAX(Id) FROM Perfil)
  IF (@NuevoIdPerfil IS NULL) SET @NuevoIdPerfil = 0

  -- incrementa en 1 el indice
  SET @NuevoIdPerfil = @NuevoIdPerfil + 1

  BEGIN TRANSACTION
    -- ingresa nuevo registro
    IF (@IdPerfil = -1) BEGIN
      INSERT INTO Perfil
      (
	      Id,
	      Nombre,
	      Llave,
	      PaginaInicio
      )
      VALUES
      (
	      @NuevoIdPerfil,
	      @Nombre,
	      @Llave,
	      @PaginaInicio
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdPerfilOutput = @IdPerfil
        RETURN
      END

      SET @Status = 'ingresado'
      SET @IdPerfilOutput = @NuevoIdPerfil
    END ELSE BEGIN
      UPDATE Perfil
      SET
	      Nombre = @Nombre,
	      Llave = @Llave,
	      PaginaInicio = @PaginaInicio
      WHERE
        Id = @IdPerfil

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdPerfilOutput = @IdPerfil
        RETURN
      END

      SET @Status = 'modificado'
      SET @IdPerfilOutput = @IdPerfil

    END

  COMMIT
END
GO        
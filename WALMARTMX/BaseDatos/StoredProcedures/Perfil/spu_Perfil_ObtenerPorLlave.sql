﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_ObtenerPorLlave')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_ObtenerPorLlave'
  DROP  Procedure  dbo.spu_Perfil_ObtenerPorLlave
END

GO

PRINT  'Creating Procedure spu_Perfil_ObtenerPorLlave'
GO
CREATE Procedure dbo.spu_Perfil_ObtenerPorLlave
/******************************************************************************
**  Descripcion  : obtiene informacion por su llave
**  Fecha        : 11/08/2014
*******************************************************************************/
@Llave AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    *
  FROM
    Perfil
  WHERE
    Llave = @Llave

END
GO    
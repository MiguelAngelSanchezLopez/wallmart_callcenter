﻿  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_VerificarDuplicidadNombre')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_VerificarDuplicidadNombre'
  DROP  Procedure  dbo.spu_Perfil_VerificarDuplicidadNombre
END

GO

PRINT  'Creating Procedure spu_Perfil_VerificarDuplicidadNombre'
GO
CREATE Procedure dbo.spu_Perfil_VerificarDuplicidadNombre
/******************************************************************************
**  Descripcion  : valida que el nombre del perfil sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 07/04/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdPerfil AS INT,
@Valor AS VARCHAR(1028)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdPerfil IS NULL OR @IdPerfil = 0) SET @IdPerfil = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdPerfil = -1) BEGIN
    IF( EXISTS(
                SELECT
                  P.Nombre
                FROM
                  Perfil AS P
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(P.Nombre,'')))) = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  P.Nombre
                FROM
                  Perfil AS P
                WHERE
                    UPPER(RTRIM(LTRIM(ISNULL(P.Nombre,'')))) = @Valor
                AND P.Id <> @IdPerfil
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO       
IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_Eliminar')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_Eliminar'
  DROP  Procedure  dbo.spu_Perfil_Eliminar
END

GO

PRINT  'Creating Procedure spu_Perfil_Eliminar'
GO
CREATE Procedure dbo.spu_Perfil_Eliminar
/******************************************************************************
**  Descripcion  : elimina registro
**  Fecha        : 17/03/2009
*******************************************************************************/
@Id AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DELETE FROM Perfil WHERE Id = @Id

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT
END
GO    
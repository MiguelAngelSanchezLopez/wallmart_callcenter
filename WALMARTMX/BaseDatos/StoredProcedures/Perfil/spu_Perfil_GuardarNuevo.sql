 IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Perfil_GuardarNuevo')
BEGIN
  PRINT  'Dropping Procedure spu_Perfil_GuardarNuevo'
  DROP  Procedure  dbo.spu_Perfil_GuardarNuevo
END

GO

PRINT  'Creating Procedure spu_Perfil_GuardarNuevo'
GO
CREATE Procedure dbo.spu_Perfil_GuardarNuevo
/******************************************************************************
**  Descripcion  : guarda un nuevo registro
**  Fecha        : 23/07/2009
*******************************************************************************/
@Id int output,
@Nombre varchar(50),
@Llave varchar(50),
@PaginaInicio varchar(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  INSERT INTO Perfil
  (
	  Nombre,
	  Llave,
    PaginaInicio
  )
  VALUES
  (
	  @Nombre,
	  @Llave,
    @PaginaInicio
  )
  
  set @Id = SCOPE_IDENTITY() /*set @Id = @@IDENTITY*/
END
GO  
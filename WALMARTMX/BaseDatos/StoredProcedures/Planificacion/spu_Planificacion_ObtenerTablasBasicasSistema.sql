﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Planificacion_ObtenerTablasBasicasSistema')
BEGIN
  PRINT  'Dropping Procedure spu_Planificacion_ObtenerTablasBasicasSistema'
  DROP  Procedure  dbo.spu_Planificacion_ObtenerTablasBasicasSistema
END

GO

PRINT  'Creating Procedure spu_Planificacion_ObtenerTablasBasicasSistema'
GO
CREATE Procedure dbo.spu_Planificacion_ObtenerTablasBasicasSistema
/******************************************************************************
**    Descripcion  : obtiene datos de las tablas basicas para comparar si existen los datos a cargar
**    Autor        : VSR
**    Fecha        : 03/06/2015
*******************************************************************************/
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  --------------------------------------------------------------------------
  -- TABLA 0: Transportistas
  --------------------------------------------------------------------------
  SELECT
    IdUsuario = U.Id,
    Rut = ISNULL(U.Rut,''),
    DV =  ISNULL(U.DV,''),
    RutCompleto = ISNULL(U.Rut,'') + ISNULL(U.DV,''),
    NombreTransportista = ISNULL(U.Nombre, '') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL(U.Telefono, ''),
    Email = ISNULL(U.Email, '')
  FROM 
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
    P.Llave = 'TRA'

  --------------------------------------------------------------------------
  -- TABLA 1: Locales
  --------------------------------------------------------------------------
  SELECT
    IdLocal = L.Id,
    NombreLocal = ISNULL(L.Nombre,''),
    CodigoInterno = ISNULL(L.CodigoInterno, -1)
  FROM 
    Local AS L

  --------------------------------------------------------------------------
  -- TABLA 2: Planificaciones ya realizadas
  --------------------------------------------------------------------------
  SELECT
    IdPlanificacionTransportista = PT.Id,
    NroTransporte = PT.NroTransporte
  FROM 
    PlanificacionTransportista AS PT

END
  
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Planificacion_GuardarNuevo')
BEGIN
  PRINT  'Dropping Procedure spu_Planificacion_GuardarNuevo'
  DROP  Procedure  dbo.spu_Planificacion_GuardarNuevo
END

GO

PRINT  'Creating Procedure spu_Planificacion_GuardarNuevo'
GO

CREATE PROCEDURE [dbo].[spu_Planificacion_GuardarNuevo]
/******************************************************************************
**    Descripcion  : Agrega un nuevo registro a la tabla Planificacion
**    Autor        : VSR
**    Fecha        : 03/06/2015
*******************************************************************************/
@Id AS INT OUTPUT,
@IdUsuarioCreacion AS INT,
@FechaCreacion AS DATETIME
AS
BEGIN

  IF (@IdUsuarioCreacion = -1) SET @IdUsuarioCreacion = NULL

  INSERT INTO Planificacion
  (
	  IdUsuarioCreacion,
	  FechaCreacion
  )
  VALUES
  (
	  @IdUsuarioCreacion,
	  @FechaCreacion
  )

  SET @Id = SCOPE_IDENTITY()

END
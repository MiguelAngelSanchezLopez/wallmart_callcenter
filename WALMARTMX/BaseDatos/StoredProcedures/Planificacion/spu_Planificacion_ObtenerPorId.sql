﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Planificacion_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_Planificacion_ObtenerPorId'
  DROP  Procedure  dbo.spu_Planificacion_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_Planificacion_ObtenerPorId'
GO

CREATE Procedure [dbo].[spu_Planificacion_ObtenerPorId] (  
@Id as INT  
) AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    Planificacion
  WHERE   
    Id = @Id  
  
END  
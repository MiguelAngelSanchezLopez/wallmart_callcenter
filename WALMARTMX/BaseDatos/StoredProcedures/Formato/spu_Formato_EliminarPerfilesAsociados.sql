﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Formato_EliminarPerfilesAsociados')
BEGIN
  PRINT  'Dropping Procedure spu_Formato_EliminarPerfilesAsociados'
  DROP  Procedure  dbo.spu_Formato_EliminarPerfilesAsociados
END

GO

PRINT  'Creating Procedure spu_Formato_EliminarPerfilesAsociados'
GO
CREATE Procedure dbo.spu_Formato_EliminarPerfilesAsociados
/******************************************************************************
**  Descripcion  : elimina un Formato
**  Fecha        : VSR, 07/04/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
        
    DELETE FROM PerfilesPorFormato WHERE IdFormato = @IdFormato AND Categoria = 'Local'

    -- retorna que todo estuvo bien
    SET @Status = 1

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO           
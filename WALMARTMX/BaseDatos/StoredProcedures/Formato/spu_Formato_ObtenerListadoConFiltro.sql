﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Formato_ObtenerListadoConFiltro')
BEGIN
  PRINT  'Dropping Procedure spu_Formato_ObtenerListadoConFiltro'
  DROP  Procedure  dbo.spu_Formato_ObtenerListadoConFiltro
END

GO

PRINT  'Creating Procedure spu_Formato_ObtenerListadoConFiltro'
GO
CREATE Procedure dbo.spu_Formato_ObtenerListadoConFiltro
/******************************************************************************
**    Descripcion  : Obtiene listado de formatos usando filtros de busqueda
**    Por          : VSR, 07/04/2015
*******************************************************************************/
@IdUsuario AS INT,
@Nombre AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
	  IdFormato = F.Id,
	  NombreFormato = ISNULL(F.Nombre,''),
	  FechaCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(F.FechaCreacion,'FECHA_COMPLETA'),
    Activo = CASE WHEN ISNULL(F.Activo,0) = 1 THEN 'SI' ELSE 'NO' END,
    TieneRegistrosAsociados = CASE WHEN (
                                (SELECT COUNT(aux.IdFormato) FROM PerfilesPorFormato AS aux WHERE aux.IdFormato = F.Id) +
                                (SELECT COUNT(aux.IdFormato) FROM Local AS aux WHERE aux.IdFormato = F.Id)
                              ) = 0 THEN '0' ELSE '1' END
  FROM
    Formato AS F
  WHERE
      (@Nombre = '-1' OR F.Nombre LIKE '%'+ @Nombre +'%')
  ORDER BY
    IdFormato DESC

END



  
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Formato_EliminarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Formato_EliminarDatos'
  DROP  Procedure  dbo.spu_Formato_EliminarDatos
END

GO

PRINT  'Creating Procedure spu_Formato_EliminarDatos'
GO
CREATE Procedure dbo.spu_Formato_EliminarDatos
/******************************************************************************
**  Descripcion  : elimina un Formato
**  Fecha        : VSR, 07/04/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
        
    DELETE FROM PerfilesPorFormato WHERE IdFormato = @IdFormato
    DELETE FROM Local WHERE IdFormato = @IdFormato
    DELETE FROM Formato WHERE Id = @IdFormato

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO           
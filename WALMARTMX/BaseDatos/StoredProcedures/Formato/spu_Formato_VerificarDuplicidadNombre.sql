﻿  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Formato_VerificarDuplicidadNombre')
BEGIN
  PRINT  'Dropping Procedure spu_Formato_VerificarDuplicidadNombre'
  DROP  Procedure  dbo.spu_Formato_VerificarDuplicidadNombre
END

GO

PRINT  'Creating Procedure spu_Formato_VerificarDuplicidadNombre'
GO
CREATE Procedure dbo.spu_Formato_VerificarDuplicidadNombre
/******************************************************************************
**  Descripcion  : valida que el nombre del formato sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : VSR, 07/04/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdFormato AS INT,
@Valor AS VARCHAR(1028)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdFormato IS NULL OR @IdFormato = 0) SET @IdFormato = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdFormato = -1) BEGIN
    IF( EXISTS(
                SELECT
                  F.Nombre
                FROM
                  Formato AS F
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(F.Nombre,'')))) = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  F.Nombre
                FROM
                  Formato AS F
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(F.Nombre,'')))) = @Valor
                AND F.Id <> @IdFormato
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO
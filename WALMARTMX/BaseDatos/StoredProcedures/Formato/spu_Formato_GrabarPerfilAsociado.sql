﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Formato_GrabarPerfilAsociado')
BEGIN
  PRINT  'Dropping Procedure spu_Formato_GrabarPerfilAsociado'
  DROP  Procedure  dbo.spu_Formato_GrabarPerfilAsociado
END

GO

PRINT  'Creating Procedure spu_Formato_GrabarPerfilAsociado'
GO
CREATE Procedure dbo.spu_Formato_GrabarPerfilAsociado
/******************************************************************************
**  Descripcion  : graba perfil asociado al formato
**  Fecha        : VSR, 08/05/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdFormato AS INT,
@IdPerfil AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    INSERT INTO PerfilesPorFormato
    (
	    IdFormato,
	    IdPerfil,
	    Categoria
    )
    VALUES
    (
	    @IdFormato,
	    @IdPerfil,
	    'Local'
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    SET @Status = 1
  COMMIT
END
GO        
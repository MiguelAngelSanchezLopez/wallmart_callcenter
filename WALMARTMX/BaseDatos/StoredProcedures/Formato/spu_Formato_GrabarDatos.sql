﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Formato_GrabarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Formato_GrabarDatos'
  DROP  Procedure  dbo.spu_Formato_GrabarDatos
END

GO

PRINT  'Creating Procedure spu_Formato_GrabarDatos'
GO
CREATE Procedure dbo.spu_Formato_GrabarDatos
/******************************************************************************
**  Descripcion  : graba los datos del Formato
**  Fecha        : VSR, 07/04/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdFormatoOutput AS INT OUTPUT,
@IdFormato AS INT,
@Nombre AS VARCHAR(255),
@Activo AS INT,
@Llave AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    -- ingresa nuevo registro
    IF (@IdFormato = -1) BEGIN
      INSERT INTO Formato
      (
	      Nombre,
	      FechaCreacion,
	      Activo,
        Llave
      )
      VALUES
      (
	      @Nombre,
	      dbo.fnu_GETDATE(),
	      @Activo,
        @Llave
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdFormatoOutput = @IdFormato
        RETURN
      END

      SET @Status = 'ingresado'
      SET @IdFormatoOutput = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE Formato
      SET
	      Nombre = @Nombre,
	      Activo = @Activo,
        Llave = @Llave
      WHERE
        Id = @IdFormato

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdFormatoOutput = @IdFormato
        RETURN
      END

      SET @Status = 'modificado'
      SET @IdFormatoOutput = @IdFormato

    END

  COMMIT
END
GO        
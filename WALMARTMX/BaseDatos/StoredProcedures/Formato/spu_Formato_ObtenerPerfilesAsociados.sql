﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Formato_ObtenerPerfilesAsociados')
BEGIN
  PRINT  'Dropping Procedure spu_Formato_ObtenerPerfilesAsociados'
  DROP  Procedure  dbo.spu_Formato_ObtenerPerfilesAsociados
END

GO

PRINT  'Creating Procedure spu_Formato_ObtenerPerfilesAsociados'
GO
CREATE Procedure dbo.spu_Formato_ObtenerPerfilesAsociados
/******************************************************************************
**    Descripcion  : obtiene perfiles asociados al formato
**    Por          : VSR, 15/04/2015
*******************************************************************************/
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  -------------------------------------------------
  -- obtiene perfiles asociados al formato
  -------------------------------------------------
  SELECT
    IdPerfil = PPF.IdPerfil,
    NombrePerfil = ISNULL(PPF.NombrePerfil,''),
    LlavePerfil = ISNULL(PPF.LlavePerfil,''),
    Tipo =  ISNULL(PPF.Tipo,'')
  FROM
    vwu_PerfilesPorFormato AS PPF
  WHERE
    PPF.IdFormato = @IdFormato
  ORDER BY
    NombrePerfil

END



   
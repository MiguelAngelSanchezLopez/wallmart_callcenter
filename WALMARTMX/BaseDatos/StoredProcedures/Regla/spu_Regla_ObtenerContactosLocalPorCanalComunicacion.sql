﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Regla_ObtenerContactosLocalPorCanalComunicacion')
BEGIN
  PRINT  'Dropping Procedure spu_Regla_ObtenerContactosLocalPorCanalComunicacion'
  DROP  Procedure  dbo.spu_Regla_ObtenerContactosLocalPorCanalComunicacion
END

GO

PRINT  'Creating Procedure spu_Regla_ObtenerContactosLocalPorCanalComunicacion'
GO
CREATE Procedure dbo.spu_Regla_ObtenerContactosLocalPorCanalComunicacion
/******************************************************************************
**    Descripcion  : obtiene los contactos del local que esten marcados con el canal consultado
**    Por          : VSR, 20/05/2015
*******************************************************************************/
@CodigoLocal AS INT,
@CodigoCentroDistribucion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  --------------------------------------------------------
  -- obtiene contactos relacionados al local
  --------------------------------------------------------
  SELECT
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') +  + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL(U.Telefono,''),
    Email = ISNULL(U.Email,''),
    NombrePerfil = ISNULL(P.Nombre,''),
    LlavePerfil = ISNULL(P.Llave,'')
  FROM
    Local AS L
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    INNER JOIN PerfilesPorFormato AS PPF ON (F.Id = PPF.IdFormato)
    INNER JOIN Perfil AS P ON (PPF.IdPerfil = P.Id)
    INNER JOIN Usuario AS U ON (P.Id = U.IdPerfil)
    INNER JOIN LocalesPorUsuario AS LPU ON (U.Id = LPU.IdUsuario AND L.Id = LPU.IdLocal)
  WHERE
    L.CodigoInterno = @CodigoLocal

  --------------------------------------------------------
  -- obtiene contactos relacionados al centro distribucion
  --------------------------------------------------------
  UNION ALL
  SELECT
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') +  + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL(U.Telefono,''),
    Email = ISNULL(U.Email,''),
    NombrePerfil = ISNULL(P.Nombre,''),
    LlavePerfil = ISNULL(P.Llave,'')
  FROM
    CentroDistribucion AS CD
    INNER JOIN CentroDistribucionPorUsuario AS CDPU ON (CD.Id = CDPU.IdCentroDistribucion)
    INNER JOIN Usuario AS U ON (CDPU.IdUsuario = U.Id)
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      CD.CodigoInterno = @CodigoCentroDistribucion

END



  
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_viaje_ObtenerInfoPatenteEcommerce')
BEGIN
  PRINT  'Dropping Procedure spu_viaje_ObtenerInfoPatenteEcommerce'
  DROP  Procedure  dbo.spu_viaje_ObtenerInfoPatenteEcommerce
END
GO

PRINT  'Creating Procedure spu_viaje_ObtenerInfoPatenteEcommerce'
GO
create Procedure [dbo].spu_viaje_ObtenerInfoPatenteEcommerce
/******************************************************************************    
**    Descripcion  : Obtiene datos de la patente  
spu_viaje_ObtenerInfoPatenteEcommerce '001DV3'
*******************************************************************************/  
 @patente AS varchar(10)
AS    
BEGIN    

	--Señal GPS (0: No Integrada / 1: GPS Online / 2: GPS Offline)
	---------------------------------------------------------------
	IF(EXISTS(SELECT TOP 1 1 FROM Track_LastPositions where Patente = @patente))
		SELECT 
			SeñalGps = CASE WHEN DATEDIFF (hour,Fecha, [dbo].[fnu_GETDATE]()) < 12 THEN 1 ELSE 2 END
		from Track_LastPositions
		where Patente = @patente
	ELSE
		SELECT SeñalGps = 0

	--Esta en CEDIS
	---------------------------------------------------------------
	declare @lat decimal(9,6), @lon decimal(9,6)
	
	select @lat = latitud, @lon = longitud from track_lastpositions where patente = @patente
	select Cedis = [dbo].[Track_PuntoEnTipoZona](@lat,@lon,1)

	--Nombre Transportista
	---------------------------------------------------------------
	select top 1 NombreTransportista = UPPER(isnull(NombreTransportista,''))
	from trazaviaje
	where 
		(patenteTracto = @patente or patenteTrailer = @patente)
	order by id desc


END
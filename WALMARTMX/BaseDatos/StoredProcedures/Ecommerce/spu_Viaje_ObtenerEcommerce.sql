﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Viaje_ObtenerEcommerce')
BEGIN
  PRINT  'Dropping Procedure spu_Viaje_ObtenerEcommerce'
  DROP  Procedure  dbo.spu_Viaje_ObtenerEcommerce
END

GO

PRINT  'Creating Procedure spu_Viaje_ObtenerEcommerce'
GO
CREATE Procedure dbo.spu_Viaje_ObtenerEcommerce
/******************************************************************************
**    Descripcion  : Obtiene viajes eccomerce
*******************************************************************************/
@fechaDesde AS datetime = null,
@fechaHasta AS datetime = null,
@pkt AS  VARCHAR(200) = null,
@upc AS  VARCHAR(200) = null,
@ciudad AS VARCHAR(200) = null,
@zip AS int = null,
@zona AS VARCHAR(200) = null,
@numeroOrden AS int = null
AS
BEGIN
	
	if(@fechaDesde is null) SET @fechaDesde = '19000101'
	if(@fechaHasta is null) SET @fechaHasta = '21000312'

	select 
		Id = ev.id
		,Fecha = ev.Fecha
		,OrderNumber = ev.OrderNumber
		,Pkt = ev.Pkt
		,Upc = ev.Upc
		,Ciudad = ev.ShipToState
		,Zip = ev.Zip
		,Zona = ev.CustomRcdExpansionField
		,Placa = isnull(eda.Placa,'')
		,Conductor = isnull(eda.Conductor,'')
		,EstadoViaje = case when eda.id is null then 'Sin Asignar' else 'Asignado' end
		,Asignado = case when eda.id is null then 0 else 1 end
	from Ecommerce_Viajes ev
	left join Ecommerce_DatosAsignados eda on ev.Id = eda.idEcommerceViaje
	where 
		(@fechaDesde is null or convert(datetime, Fecha,112) >= @fechaDesde and convert(datetime, Fecha,112) <= @fechaHasta)
		and (@pkt is null or pkt = @pkt)
		and (@upc is null or upc = @upc)
		and (@ciudad is null or ShipToState = @ciudad)
		and (@zip is null or zip = @zip)
		and (@zona is null or CustomRcdExpansionField = @zona)
		and (@numeroOrden is null or OrderNumber = @numeroOrden) 
END   


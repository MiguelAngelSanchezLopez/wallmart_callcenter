﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Viaje_AsignarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Viaje_AsignarDatos'
  DROP  Procedure  dbo.spu_Viaje_AsignarDatos
END

GO

PRINT  'Creating Procedure spu_Viaje_AsignarDatos'
GO
CREATE Procedure dbo.spu_Viaje_AsignarDatos
/******************************************************************************
**    Descripcion  : graba viajes eccomerce
*******************************************************************************/
@id AS int
,@fechaProgramacion AS datetime
,@CantidadPkt AS  int
,@Zona AS  VARCHAR(200)
,@placa AS VARCHAR(200)
,@conductor AS VARCHAR(200)
,@correlativo as int
AS
BEGIN
	begin transaction

	insert into Ecommerce_DatosAsignados(
		idEcommerceViaje
		,fechaProgramacion
		,cantidadPkt
		,Zona
		,placa
		,conductor
		,correlativo
	)
	values(
		@id
		,@fechaProgramacion
		,@CantidadPkt
		,@Zona
		,@placa 
		,@conductor
		,@correlativo
	)

	IF @@ERROR<>0 BEGIN
        ROLLBACK
        RETURN
    END

	commit
END   


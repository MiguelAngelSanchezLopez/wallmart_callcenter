﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarNuevaAlertaFuturaMultipunto')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarNuevaAlertaFuturaMultipunto'
  DROP  Procedure  dbo.spu_Alerta_GrabarNuevaAlertaFuturaMultipunto
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarNuevaAlertaFuturaMultipunto'
GO
CREATE Procedure dbo.spu_Alerta_GrabarNuevaAlertaFuturaMultipunto
/******************************************************************************
**    Descripcion  : graba una alerta futura para MULTIPUNTO dependiendo de la explicacion registrada
**    Autor        : VSR
**    Fecha        : 22/08/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlerta AS INT,
@Explicacion AS VARCHAR(255),
@TipoExplicacion AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DECLARE @TiempoRepeticion AS VARCHAR(255), @GrupoAlertaFutura VARCHAR(255)
    SET @Status = 1

    -- obtiene el tiempo de reprogramacion de la alerta segun el tipo de explicacion
    IF (UPPER(@TipoExplicacion) = 'REPROGRAMAR') BEGIN
      SELECT @TiempoRepeticion = Extra2 FROM TipoGeneral WHERE Tipo = 'Explicacion' AND Extra1 = @TipoExplicacion AND Nombre = @Explicacion
    END ELSE BEGIN
      SELECT @TiempoRepeticion = Extra2 FROM TipoGeneral WHERE Tipo = 'ExplicacionMultipunto' AND Nombre = @Explicacion
    END

    IF (@TiempoRepeticion IS NULL OR @TiempoRepeticion = '') SET @TiempoRepeticion = '-1'

    IF (@TiempoRepeticion <> '-1') BEGIN
      DECLARE @FechaFutura AS DATETIME
      SET @FechaFutura = DATEADD(mi, CONVERT(INT, @TiempoRepeticion), dbo.fnu_GETDATE())

      -- graba una copia de la alerta para dejarla como futura
      INSERT INTO AlertaWebservice
      (
	      NroTransporte,
		    IdEmbarque,
	      DescripcionAlerta,
	      LocalDestino,
	      OrigenCodigo,
	      PatenteTracto,
	      PatenteTrailer,
	      RutTransportista,
	      NombreTransportista,
	      RutConductor,
	      NombreConductor,
	      FechaUltimaTransmision,
	      FechaInicioAlerta,
	      LatTracto,
	      LonTracto,
	      LatTrailer,
	      LonTrailer,
	      TipoAlerta,
	      Permiso,
	      Criticidad,
	      TipoAlertaDescripcion,
	      AlertaMapa,
	      NombreZona,
	      Velocidad,
	      EstadoGPSTracto,
	      EstadoGPSRampla,
	      EstadoGPS,
	      TipoPunto,
	      Temp1,
	      Temp2,
	      DistanciaTT,
	      TransportistaTrailer,
	      CantidadSatelites,
	      FechaHoraCreacion,
	      FechaHoraActualizacion,
	      EnvioCorreo,
	      FueraDeHorario,
	      Activo,
	      ConLog,
	      Ocurrencia,
	      GrupoAlerta
      )
      SELECT
	      NroTransporte,
		    IdEmbarque,
	      DescripcionAlerta,
	      LocalDestino,
	      OrigenCodigo,
	      PatenteTracto,
	      PatenteTrailer,
	      RutTransportista,
	      NombreTransportista,
	      RutConductor,
	      NombreConductor,
	      FechaUltimaTransmision,
	      FechaInicioAlerta,
	      LatTracto,
	      LonTracto,
	      LatTrailer,
	      LonTrailer,
	      TipoAlerta,
	      Permiso,
	      Criticidad,
	      TipoAlertaDescripcion,
	      AlertaMapa,
	      NombreZona,
	      Velocidad,
	      EstadoGPSTracto,
	      EstadoGPSRampla,
	      EstadoGPS,
	      TipoPunto,
	      Temp1,
	      Temp2,
	      DistanciaTT,
	      TransportistaTrailer,
	      CantidadSatelites,
	      FechaHoraCreacion,
	      FechaHoraActualizacion,
	      EnvioCorreo,
	      FueraDeHorario,
	      Activo,
	      ConLog,
	      Ocurrencia,
	      GrupoAlerta
      FROM
	      Alerta
      WHERE
        Id = @IdAlerta

      SET @Status = SCOPE_IDENTITY()

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      -- actualiza campos para dejar la alerta como futura
      SET @GrupoAlertaFutura = 'GrupoAlertaFutura - ' + CONVERT(VARCHAR, @Status)

	    IF (UPPER(@TipoExplicacion) = 'REPROGRAMAR') BEGIN
		    UPDATE AlertaWebservice
		    SET
			    Permiso = 'REPROGRAMADA',
			    FechaHoraCreacion = @FechaFutura,
			    FechaHoraActualizacion = @FechaFutura,
			    FueraDeHorario = 0,
			    Activo = 1,
			    Ocurrencia = 1,
			    GrupoAlerta = @GrupoAlertaFutura
		    WHERE
			    Id = @Status
	    END ELSE BEGIN
		    UPDATE AlertaWebservice
		    SET
			    DescripcionAlerta = @Explicacion,
			    TipoAlertaDescripcion = 'ALERTA FUTURA MULTIPUNTO',
			    FechaHoraCreacion = @FechaFutura,
			    FechaHoraActualizacion = @FechaFutura,
			    FueraDeHorario = 0,
			    Activo = 1,
			    Ocurrencia = 1,
			    GrupoAlerta = @GrupoAlertaFutura
		    WHERE
			    Id = @Status
	    END

      -----------------------------------------------
      -- inserta la nueva en la tabla de gestion del callcenter
      -----------------------------------------------
      IF (@Status <> -200) BEGIN
        -- graba los datos en la tabla Alerta
        INSERT INTO Alerta SELECT * FROM AlertaWebservice WHERE Id = @Status

        IF @@ERROR<>0 BEGIN
          ROLLBACK
          SET @Status = -200
          RETURN
        END

        -------------------------------------------------------------
        -- graba la asociacion de la alerta segun el canal de envio
        -------------------------------------------------------------
        INSERT INTO CallCenterAlerta
        (
	        IdAlerta,
	        IdAlertaPadre,
	        Cerrado,
	        CerradoSatisfactorio,
          Desactivada
        )
        VALUES
        (
	        @Status,
	        @Status,
	        0,
	        0,
          0
        )

        IF @@ERROR<>0 BEGIN
          ROLLBACK
          SET @Status = -200
          RETURN
        END

      END

    END

  COMMIT
END
GO 
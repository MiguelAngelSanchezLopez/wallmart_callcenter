﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarDatosDefinicionAlerta')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarDatosDefinicionAlerta'
  DROP  Procedure  dbo.spu_Alerta_GrabarDatosDefinicionAlerta
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarDatosDefinicionAlerta'
GO
CREATE Procedure dbo.spu_Alerta_GrabarDatosDefinicionAlerta
/******************************************************************************
**  Descripcion  : graba los datos de la definicion de la alerta
**  Fecha        : VSR, 10/04/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdAlertaDefinicionOutput AS INT OUTPUT,
@IdAlertaDefinicion AS INT,
@Nombre AS VARCHAR(255),
@Formatos AS VARCHAR(8000),
@GestionarPorCemtra AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    -- ingresa nuevo registro
    IF (@IdAlertaDefinicion = -1) BEGIN
      INSERT INTO AlertaDefinicion
      (
	      Nombre,
        GestionarPorCemtra
      )
      VALUES
      (
	      @Nombre,
        @GestionarPorCemtra
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdAlertaDefinicionOutput = @IdAlertaDefinicion
        RETURN
      END

      SET @Status = 'ingresado'
      SET @IdAlertaDefinicionOutput = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE AlertaDefinicion
      SET
	      Nombre = @Nombre,
        GestionarPorCemtra = @GestionarPorCemtra
      WHERE
        Id = @IdAlertaDefinicion

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdAlertaDefinicionOutput = @IdAlertaDefinicion
        RETURN
      END

      SET @Status = 'modificado'
      SET @IdAlertaDefinicionOutput = @IdAlertaDefinicion

    END

    ------------------------------------------------------------
    -- inserta los formatos que no te esten asociados previamente
    ------------------------------------------------------------
    INSERT INTO AlertaDefinicionPorFormato
    (
	    IdAlertaDefinicion,
	    IdFormato
    )
    SELECT
      @IdAlertaDefinicionOutput,
      T.valor
    FROM (
          ---------------------------------------------------------------------
          -- busca que formatos estan asociados con anterioridad
          ---------------------------------------------------------------------
          SELECT
            FNU.valor,
            EstaAsociado = CASE WHEN ADPF.IdFormato IS NULL THEN 0 ELSE 1 END
          FROM
            dbo.fnu_InsertaListaTabla(@Formatos) AS FNU
            LEFT JOIN AlertaDefinicionPorFormato AS ADPF ON (CONVERT(NUMERIC,FNU.valor) = ADPF.IdFormato AND ADPF.IdAlertaDefinicion = @IdAlertaDefinicionOutput)
    ) AS T
    WHERE
      T.EstaAsociado = 0

  COMMIT
END
GO        
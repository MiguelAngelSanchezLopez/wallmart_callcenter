﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerConductores')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerConductores'
  DROP  Procedure  dbo.spu_Alerta_ObtenerConductores
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerConductores'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerConductores
/******************************************************************************
**    Descripcion  : obtiene listado de conductores para dibujar el combo en pantalla de gestion del teleoperador
**    Autor        : VSR
**    Fecha        : 28/07/2014
spu_Alerta_ObtenerConductores ''
*******************************************************************************/
@RutConductor AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @IdUsuario INT
  SET @RutConductor = UPPER(RTRIM(LTRIM(@RutConductor)))
  IF (@RutConductor = '') SET @RutConductor = '-1'
  
  -----------------------------------------------------------------------------
  -- busca el usuario por el nombre consultado
  -----------------------------------------------------------------------------
  SELECT TOP 1
    @IdUsuario = U.Id
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      P.Llave = 'CON'
  AND U.Rut = @RutConductor

  -- si no encuentra el usuario entonces setea variable para obtener todos los usuarios
  IF (@IdUsuario IS NULL) SET @IdUsuario = -1
  begin

  	DECLARE @usuaurio TABLE (IdEscalamientoPorAlertaContacto int, NombreUsuario varchar(5000), telefono varchar(200), email varchar(500), cargo varchar(5000), tipogrupo varchar(5000))
	
	INSERT @usuaurio
	EXEC spu_Alerta_ObtenerUsuariosPorPerfil @IdUsuario, 'CON'

	--Solo se listan los primeros 6000 ya que con mas conductores el string json se hace mas largo y se desborda
	select top 6000 * from @usuaurio

  end 
END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_EliminarConfiguracionEscalamientosNoReferenciados')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_EliminarConfiguracionEscalamientosNoReferenciados'
  DROP  Procedure  dbo.spu_Alerta_EliminarConfiguracionEscalamientosNoReferenciados
END

GO

PRINT  'Creating Procedure spu_Alerta_EliminarConfiguracionEscalamientosNoReferenciados'
GO
CREATE Procedure dbo.spu_Alerta_EliminarConfiguracionEscalamientosNoReferenciados
/******************************************************************************
**  Descripcion  : elimina los escalamientos que no estan referenciados
**  Fecha        : 06/08/2014 
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlertaConfiguracion AS INT,
@Listado AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    /*
    DELETE FROM CallCenterHistorialEscalamientoContacto WHERE IdEscalamientoPorAlertaContacto IN (
      SELECT Id FROM EscalamientoPorAlertaContacto WHERE IdEscalamientoPorAlertaGrupoContacto IN (
        SELECT Id FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
	        SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion = @IdAlertaConfiguracion AND Id NOT IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
        )
      )
    )

    DELETE FROM CallCenterHistorialEscalamientoContacto WHERE IdHistorialEscalamiento IN (
      SELECT Id FROM CallCenterHistorialEscalamiento WHERE IdEscalamientoPorAlerta IN (
	      SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion = @IdAlertaConfiguracion AND Id NOT IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
      )
    )

    DELETE FROM CallCenterHistorialEscalamiento WHERE IdEscalamientoPorAlerta IN (
	    SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion = @IdAlertaConfiguracion AND Id NOT IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
    )

    DELETE FROM EscalamientoPorAlertaContacto WHERE IdEscalamientoPorAlertaGrupoContacto IN (
      SELECT Id FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
	      SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion = @IdAlertaConfiguracion AND NOT Id IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
      )
    )

    DELETE FROM Script WHERE IdEscalamientoPorAlertaGrupoContacto IN (
      SELECT Id FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
	      SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion = @IdAlertaConfiguracion AND NOT Id IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
      )
    )

    DELETE FROM CallCenterHistorialLlamadaEjecutivo WHERE IdEscalamientoPorAlertaGrupoContacto IN (
      SELECT Id FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
	      SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion = @IdAlertaConfiguracion AND NOT Id IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
      )
    )

    DELETE FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
	    SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion = @IdAlertaConfiguracion AND Id NOT IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
    )

    DELETE FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion = @IdAlertaConfiguracion AND NOT Id IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))
    */

    UPDATE EscalamientoPorAlerta
    SET Eliminado = 1
    WHERE
        IdAlertaConfiguracion = @IdAlertaConfiguracion 
    AND NOT Id IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    -- retorna que todo estuvo bien
    SET @Status = 1

  COMMIT
END
GO
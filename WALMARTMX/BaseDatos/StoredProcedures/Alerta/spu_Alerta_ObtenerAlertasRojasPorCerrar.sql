﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerAlertasRojasPorCerrar')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerAlertasRojasPorCerrar'
  DROP  Procedure  dbo.spu_Alerta_ObtenerAlertasRojasPorCerrar
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerAlertasRojasPorCerrar'
GO
CREATE Procedure spu_Alerta_ObtenerAlertasRojasPorCerrar  
/***********************************************************************************  
**    Descripcion  : obtiene listado alertas rojas por cerrar 
**    Por          : DG, 25/03/2015
exec spu_Alerta_ObtenerAlertasRojasPorCerrar '-1','-1','-1'
*************************************************************************************/  
@FechaDesde AS VARCHAR(255),  
@FechaHasta AS VARCHAR(255),
@NroTransporte AS VARCHAR(255) 
AS  
BEGIN  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
 SET NOCOUNT ON  
  
 IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'  
 IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'  
 
 SET @FechaDesde = @FechaDesde + ' 00:00'  
 SET @FechaHasta = @FechaHasta + ' 23:59'  
    
SELECT
	 NroTransporte = a.NroTransporte
	,NombreTransportista = a.NombreTransportista
	,TipoAlerta = a.DescripcionAlerta
	,Cantidad = COUNT(a.Id)
	,MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte AND D.LocalDestino = a.LocalDestino),0)
	,Link = (SELECT TOP 1 alertaMapa FROM CallCenterAlerta AS CCA1 INNER JOIN Alerta AS A1 ON (CCA1.IdAlerta = A1.Id) WHERE a1.NroTransporte = a.NroTransporte AND LEFT(a1.LatTracto,6) = LEFT(a.LatTracto,6) AND LEFT(a1.LonTracto,6) = LEFT(a.LonTracto,6) AND a1.LocalDestino = a.LocalDestino)
	,Fecha = (SELECT CONVERT(VARCHAR,MIN(FECHAHORACREACION),103) FROM CallCenterAlerta AS CCA1 INNER JOIN Alerta AS A1 ON (CCA1.IdAlerta = A1.Id) WHERE a1.NroTransporte = a.NroTransporte AND LEFT(a1.LatTracto,6) = LEFT(a.LatTracto,6) AND LEFT(a1.LonTracto,6) = LEFT(a.LonTracto,6) AND a1.LocalDestino = a.LocalDestino)
	,Local = a.LocalDestino
  ,NombreFormato = F.Nombre
FROM 
  CallCenterAlerta AS CCA
  INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
  INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
  INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
WHERE	
	a.Activo = 1        
	AND a.FueraDeHorario = 0 
	--Filtros de busqueda
	AND (@NroTransporte = '-1' OR A.NroTransporte = @NroTransporte)
	AND A.FechaHoraCreacion BETWEEN @FechaDesde AND @FechaHasta       
GROUP BY 
	 a.NroTransporte
	,a.NombreTransportista
	,a.DescripcionAlerta
	,a.LocalDestino
  ,F.Nombre
	,LEFT(a.LonTracto,6)
	,LEFT(a.LatTracto,6)
  
END

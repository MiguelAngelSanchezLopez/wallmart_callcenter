﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_DesactivarAlertasEnCola')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_DesactivarAlertasEnCola'
  DROP  Procedure  dbo.spu_Alerta_DesactivarAlertasEnCola
END

GO

PRINT  'Creating Procedure spu_Alerta_DesactivarAlertasEnCola'
GO
CREATE Procedure dbo.spu_Alerta_DesactivarAlertasEnCola
/******************************************************************************
**    Descripcion  : desactiva las alertas que estan en cola de atencion
**    Autor        : VSR
**    Fecha        : 21/07/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuarioDesactivacion AS INT,
@NroTransporte AS INT,
@LocalDestino AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    ------------------------------------------------------------------
    -- obtiene las alertas y verifica las que han sido gestionada por callcenter
    DECLARE @TablaAlertaGestionada AS TABLE(IdAlerta INT, Gestionada INT)
    INSERT INTO @TablaAlertaGestionada(IdAlerta, Gestionada)
    SELECT
      IdAlerta = CCA.IdAlerta,
      Gestionada = CASE WHEN HAG.Accion IS NULL THEN 0 ELSE 1 END
    FROM
      CallCenterAlerta AS CCA
      INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
      LEFT JOIN CallCenterHistorialAlertaGestion AS HAG ON (A.Id = HAG.IdAlertaHija AND HAG.Accion='TELEOPERADOR_ALERTA_GRABADA')
    WHERE
        A.NroTransporte = @NroTransporte
    AND A.LocalDestino = @LocalDestino

    ------------------------------------------------------------------
    -- desactiva todas las alertas que no estan gestionadas
    UPDATE CallCenterAlerta
    SET
      Desactivada = 1,
      IdUsuarioDesactivacion = @IdUsuarioDesactivacion,
      FechaDesactivacion = dbo.fnu_GETDATE()
    FROM
      CallCenterAlerta AS CCA
      INNER JOIN @TablaAlertaGestionada AS AG ON (CCA.IdAlerta = AG.IdAlerta)
    WHERE
      AG.Gestionada = 0

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    SET @Status = 1
  COMMIT
END
GO 
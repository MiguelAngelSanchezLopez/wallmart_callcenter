﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_EliminarDatosConfiguracion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_EliminarDatosConfiguracion'
  DROP  Procedure  dbo.spu_Alerta_EliminarDatosConfiguracion
END

GO

PRINT  'Creating Procedure spu_Alerta_EliminarDatosConfiguracion'
GO
CREATE Procedure dbo.spu_Alerta_EliminarDatosConfiguracion
/******************************************************************************
**  Descripcion  : elimina la configuracion de la alerta
**  Fecha        : 31/07/2014 
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdAlertaConfiguracion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM CallCenterHistorialEscalamientoContacto WHERE IdHistorialEscalamiento IN (
      SELECT Id FROM CallCenterHistorialEscalamiento WHERE IdAlerta IN (
        SELECT Id FROM Alerta WHERE DescripcionAlerta IN (
          SELECT
            Nombre = AD.Nombre
          FROM
            AlertaConfiguracion AS AC
            INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
            INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
          WHERE
            AC.Id = @IdAlertaConfiguracion        
        )
      )
    )

    DELETE FROM CallCenterHistorialEscalamientoContacto WHERE IdEscalamientoPorAlertaContacto IN (
      SELECT Id FROM EscalamientoPorAlertaContacto WHERE IdEscalamientoPorAlertaGrupoContacto IN (
        SELECT Id FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
          SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion IN (
            SELECT Id FROM AlertaConfiguracion WHERE Id = @IdAlertaConfiguracion
          )
        )
      )
    )

    DELETE FROM EscalamientoPorAlertaContacto WHERE IdEscalamientoPorAlertaGrupoContacto IN (
      SELECT Id FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
        SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion IN (
          SELECT Id FROM AlertaConfiguracion WHERE Id = @IdAlertaConfiguracion
        )
      )
    )

    DELETE FROM Script WHERE IdEscalamientoPorAlertaGrupoContacto IN (
      SELECT Id FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
        SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion IN (
          SELECT Id FROM AlertaConfiguracion WHERE Id = @IdAlertaConfiguracion
        )
      )
    )

    DELETE FROM CallCenterHistorialLlamadaEjecutivo WHERE IdEscalamientoPorAlertaGrupoContacto IN (
      SELECT Id FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
        SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion IN (
          SELECT Id FROM AlertaConfiguracion WHERE Id = @IdAlertaConfiguracion
        )
      )
    )

    DELETE FROM EscalamientoPorAlertaGrupoContacto WHERE IdEscalamientoPorAlerta IN (
      SELECT Id FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion IN (
        SELECT Id FROM AlertaConfiguracion WHERE Id = @IdAlertaConfiguracion
      )
    )

    DELETE FROM CallCenterHistorialEscalamiento WHERE IdAlerta IN (
      SELECT Id FROM Alerta WHERE DescripcionAlerta IN (
          SELECT
            Nombre = AD.Nombre
          FROM
            AlertaConfiguracion AS AC
            INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
            INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
          WHERE
            AC.Id = @IdAlertaConfiguracion        
      )
    )
    
    DELETE FROM EscalamientoPorAlerta WHERE IdAlertaConfiguracion IN (
      SELECT Id FROM AlertaConfiguracion WHERE Id = @IdAlertaConfiguracion
    )
    
    DELETE FROM AlertaConfiguracion WHERE Id = @IdAlertaConfiguracion

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO
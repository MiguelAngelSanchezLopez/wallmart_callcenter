﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerUsuariosPorPerfil')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerUsuariosPorPerfil'
  DROP  Procedure  dbo.spu_Alerta_ObtenerUsuariosPorPerfil
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerUsuariosPorPerfil'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerUsuariosPorPerfil
/******************************************************************************
**    Descripcion  : obtiene listado de usuario segun el perfil consultado
**    Autor        : VSR
**    Fecha        : 30/07/2014
*******************************************************************************/
@IdUsuario INT,
@LlavePerfil AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  -----------------------------------------------------------------------------
  -- se mantiene la misma estructura de campos que tiene el select de la TABLA 3: Contactos Proximo Escalamiento
  -- del procedimiento spu_Alerta_ObtenerDetalle
  -----------------------------------------------------------------------------
  SELECT DISTINCT
    IdEscalamientoPorAlertaContacto = U.Id,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL((SELECT aux.Telefono FROM TelefonosPorUsuario AS aux WHERE aux.IdUsuario = U.Id AND REPLACE(LEFT(CONVERT(VARCHAR,dbo.fnu_GETDATE(),108),5),':','') BETWEEN aux.HoraInicio AND aux.HoraTermino), ISNULL(U.Telefono,'')),
    Email = ISNULL(U.Email,''),
    Cargo = ISNULL(P.Nombre,''),
    TipoGrupo = CASE WHEN PPF.NombrePerfil IS NULL THEN 'Personalizado' ELSE PPF.Tipo END
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    LEFT JOIN vwu_PerfilesPorFormato AS PPF ON (P.Nombre = PPF.NombrePerfil AND P.Llave = PPF.LlavePerfil)
  WHERE
      U.Estado = 1
  AND P.Llave = @LlavePerfil
  AND (@IdUsuario = -1 OR U.Id = @IdUsuario)
  ORDER BY
    NombreUsuario

END




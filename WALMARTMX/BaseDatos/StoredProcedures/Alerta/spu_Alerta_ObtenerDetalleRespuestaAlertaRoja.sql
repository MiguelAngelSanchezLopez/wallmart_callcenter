﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerDetalleRespuestaAlertaRoja')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerDetalleRespuestaAlertaRoja'
  DROP  Procedure  dbo.spu_Alerta_ObtenerDetalleRespuestaAlertaRoja
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerDetalleRespuestaAlertaRoja'
GO
CREATE Procedure [dbo].[spu_Alerta_ObtenerDetalleRespuestaAlertaRoja]
/******************************************************************************    
**    Descripcion  : obtiene listado respuestas alertas rojas  
**    Por          : VSR, 15/12/2014    
*******************************************************************************/    
@NroTransporte AS INT,
@IdAlertaRojaEstadoActual AS INT,
@Entidad VARCHAR(255)
AS    
BEGIN    
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
  SET NOCOUNT ON    
    
  ---------------------------------------------------------------------------------------------
  -- obtiene detalle de la alerta
  DECLARE @TablaAlertaRoja AS TABLE(IdAlerta INT, NroTransporte INT, NombreAlerta VARCHAR(255), NombreConductor VARCHAR(255), NombreTransportista VARCHAR(255), RutTransportista VARCHAR(255),
                                    PatenteTracto VARCHAR(255), PatenteTrailer VARCHAR(255), AlertaMapa VARCHAR(255), LocalOrigenCodigo INT, LocalOrigenDescripcion VARCHAR(255), LocalDestinoCodigo INT,
                                    LocalDestinoDescripcion VARCHAR(255), TipoViaje VARCHAR(255), FechaHoraCreacion DATETIME, TipoAlerta VARCHAR(255), FechaGestion DATETIME, CategoriaAlerta VARCHAR(255),
                                    TipoObservacion VARCHAR(255), Observacion VARCHAR(4000), IdAlertaRojaRespuesta INT, AlertaRojaRespuestaObservacion VARCHAR(4000), AlertaRojaRespuestaIdUsuarioCreacion INT,
                                    AlertaRojaRespuestaFechaCreacion DATETIME, AlertaRojaEstado VARCHAR(255), AlertaRojaObservacionEstado VARCHAR(4000), MontoDiscrepancia NUMERIC, MontoUnoTransportista NUMERIC,
                                    MontoDosTransportista NUMERIC
                                   )
  INSERT INTO @TablaAlertaRoja
  (
	  IdAlerta,
	  NroTransporte,
	  NombreAlerta,
	  NombreConductor,
	  NombreTransportista,
	  RutTransportista,
	  PatenteTracto,
	  PatenteTrailer,
	  AlertaMapa,
	  LocalOrigenCodigo,
	  LocalOrigenDescripcion,
	  LocalDestinoCodigo,
	  LocalDestinoDescripcion,
	  TipoViaje,
	  FechaHoraCreacion,
	  TipoAlerta,
	  FechaGestion,
	  CategoriaAlerta,
	  TipoObservacion,
	  Observacion,
	  IdAlertaRojaRespuesta,
	  AlertaRojaRespuestaObservacion,
	  AlertaRojaRespuestaIdUsuarioCreacion,
	  AlertaRojaRespuestaFechaCreacion,
	  AlertaRojaEstado,
	  AlertaRojaObservacionEstado,
	  MontoDiscrepancia,
	  MontoUnoTransportista,
	  MontoDosTransportista
  )
  SELECT DISTINCT
    A.Id,
    A.NroTransporte,
    A.DescripcionAlerta,
    A.NombreConductor,
    A.NombreTransportista,
    A.RutTransportista,
    A.PatenteTracto,
    A.PatenteTrailer,
    A.AlertaMapa,
    LocalOrigenCodigo = ISNULL(A.OrigenCodigo,-1),
    LocalOrigenDescripcion = ISNULL(CD.Nombre,''),
    LocalDestinoCodigo = ISNULL(L.CodigoInterno,-1),
    LocalDestinoDescripcion = ISNULL(L.Nombre,''),
    TV.TipoViaje,
    FechaHoraCreacion = A.FechaHoraCreacion,
    A.TipoAlerta,
    FechaGestion = HE.Fecha,
    HE.CategoriaAlerta,
    HE.TipoObservacion,
    HE.Observacion,
    IdAlertaRojaRespuesta = ARR.Id,
    ARR.Observacion,
    AlertaRojaRespuestaIdUsuarioCreacion = ARR.IdUsuarioCreacion,
    AlertaRojaRespuestaFechaCreacion = ARR.FechaCreacion,
    EA.Estado,
    EA.ObservacionEstado,
    MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte AND D.LocalDestino = a.LocalDestino),0),
	  EA.MontoUnoTransportista,    
	  EA.MontoDosTransportista
  FROM
    CallCenterHistorialEscalamiento AS HE
    INNER JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
    INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    INNER JOIN AlertaRojaRespuesta AS ARR ON (A.NroTransporte = ARR.NroTransporte AND A.Id = ARR.IdAlerta)
    INNER JOIN AlertaRojaEstadoActual AS EA ON (A.NroTransporte = EA.NroTransporte)
    LEFT JOIN CentroDistribucion AS CD ON (A.OrigenCodigo = CD.CodigoInterno)
  WHERE
      ( A.NroTransporte = @NroTransporte )
  AND ( HE.ClasificacionAlerta = 'Roja')
  ORDER BY
    A.DescripcionAlerta, FechaHoraCreacion
  
  ---------------------------------------------------------------------------------
  -- obtiene los ultimos estado cuando se aplican los planes de accion
  ---------------------------------------------------------------------------------
  DECLARE @TablaPlanAccion AS TABLE(Estado VARCHAR(255), FechaCreacion VARCHAR(255))
  INSERT INTO @TablaPlanAccion(Estado, FechaCreacion)
  SELECT
    ARHE.Estado,
    FechaCreacion = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(MAX(ARHE.FechaCreacion), 'FECHA_COMPLETA'),'')
  FROM
    AlertaRojaHistorialEstado AS ARHE
  WHERE
    ARHE.NroTransporte = @NroTransporte
  AND ARHE.Estado IN ('POR APROBAR PLAN ACCION','FINALIZADO')  
  GROUP BY
    ARHE.Estado


  ---------------------------------------------------------------------------------------------
  -- TABLA 0: LISTADO ALERTAS
  ---------------------------------------------------------------------------------------------
  SELECT DISTINCT
  	IdAlerta,
  	NroTransporte,
  	NombreAlerta,
  	NombreConductor = ISNULL(NombreConductor,''),
  	NombreTransportista = ISNULL(NombreTransportista,''),
  	PatenteTracto = ISNULL(PatenteTracto,''),
  	PatenteTrailer = ISNULL(PatenteTrailer,''),
  	AlertaMapa = ISNULL(AlertaMapa,''),
  	LocalOrigenCodigo = ISNULL(LocalOrigenCodigo,-1),
  	LocalOrigenDescripcion = ISNULL(LocalOrigenDescripcion,''),
    LocalDestinoCodigo = ISNULL(LocalDestinoCodigo,-1),
    LocalDestinoDescripcion = ISNULL(LocalDestinoDescripcion,''),
    TipoViaje = ISNULL(TipoViaje,''),
	  FechaHoraCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(FechaHoraCreacion,'FECHA_COMPLETA'),
  	TipoAlerta = ISNULL(TipoAlerta,''),
    MontoDiscrepancia = ISNULL(MontoDiscrepancia,0)
  FROM
  	@TablaAlertaRoja

  ---------------------------------------------------------------------------------------------
  -- TABLA 1: CATEGORIA DE LAS ALERTAS
  ---------------------------------------------------------------------------------------------
  SELECT DISTINCT
  	IdAlerta,
    FechaGestion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(FechaGestion,'FECHA_COMPLETA'),
  	CategoriaAlerta = ISNULL(CategoriaAlerta,''),
    TipoObservacion = ISNULL(TipoObservacion,''),
    Observacion = ISNULL(Observacion,'')
  FROM
  	@TablaAlertaRoja

  ---------------------------------------------------------------------------------------------
  -- TABLA 2: LISTADO ARCHIVOS ADJUNTOS RESPUESTA ALERTAS ROJAS
  ---------------------------------------------------------------------------------------------
  SELECT
    IdAlertaRojaRespuesta = T.IdAlertaRojaRespuesta,
    IdArchivo = A.Id,
    Nombre = A.Nombre,
    Ruta = A.Ruta,
    Tamano = A.Tamano,
    Extension = A.Extension,
    ContentType = A.ContentType,
    Contenido = A.Contenido
  FROM
    Archivo AS A
    INNER JOIN ArchivosXRegistro AS AxR ON (A.Id = AxR.IdArchivo)
    INNER JOIN (
    	SELECT DISTINCT IdAlertaRojaRespuesta FROM @TablaAlertaRoja AS AR 
    ) AS T ON (AxR.IdRegistro = T.IdAlertaRojaRespuesta AND AxR.Entidad = @Entidad)
  ORDER BY
    IdAlertaRojaRespuesta  

  ---------------------------------------------------------------------------------------------
  -- TABLA 3: ESTADO ALERTA ROJA
  ---------------------------------------------------------------------------------------------
  SELECT DISTINCT
    NombreTransportista,
    AlertaRojaEstado = ISNULL(AlertaRojaEstado,''),
    AlertaRojaObservacionEstado = ISNULL(AlertaRojaObservacionEstado,''),    
    MontoDiscrepancia = ISNULL(MontoDiscrepancia,0),
    MontoUnoTransportista = ISNULL(MontoUnoTransportista,-1), 
    MontoDosTransportista = ISNULL(MontoDosTransportista,-1)
  FROM
  	@TablaAlertaRoja
  	
  ---------------------------------------------------------------------------------------------
  -- TABLA 4: ESTADO TRANSPORTISTA
  ---------------------------------------------------------------------------------------------
  SELECT
    Id = TG.id,
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Sugerida = ISNULL(ETAREA.Sugerida,0),
    FechaCreacionSugerida = CASE WHEN ISNULL(ETAREA.Sugerida,0) = 1 THEN ISNULL((SELECT TOP 1 aux.FechaCreacion FROM @TablaPlanAccion AS aux WHERE aux.Estado = 'POR APROBAR PLAN ACCION'),'') ELSE '' END,
    Aprobada = ISNULL(ETAREA.Aprobada,0),
    FechaCreacionAprobada = CASE WHEN ISNULL(ETAREA.Aprobada,0) = 1 THEN ISNULL((SELECT TOP 1 aux.FechaCreacion FROM @TablaPlanAccion AS aux WHERE aux.Estado = 'FINALIZADO'),'') ELSE '' END
  FROM
    TipoGeneral AS TG
    LEFT JOIN EstadoTransportistaPorAlertaRojaEstadoActual AS ETAREA ON (TG.Id = ETAREA.IdTipoGeneral AND ETAREA.IdAlertaRojaEstadoActual = @IdAlertaRojaEstadoActual)
  WHERE
      TG.Tipo = 'EstadoTransportista'
  AND TG.Activo = 1
  ORDER BY
    TG.Nombre       

  ---------------------------------------------------------------------------------------------
  -- TABLA 5: RESPUESTA TRANSPORTISTA
  ---------------------------------------------------------------------------------------------   
  SELECT 
	  Id,
	  IdAlerta,
	  Observacion = ISNULL(Observacion,''),
	  FechaCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(FechaCreacion,'FECHA_COMPLETA'),
    Rechazo = ISNULL(Rechazo, 0)
  FROM
    AlertaRojaRespuesta
  WHERE 
    NroTransporte = @NroTransporte   
  ORDER BY 
    AlertaRojaRespuesta.FechaCreacion DESC
END
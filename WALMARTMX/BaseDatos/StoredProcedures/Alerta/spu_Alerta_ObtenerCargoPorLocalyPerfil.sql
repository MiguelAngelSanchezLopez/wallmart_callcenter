﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerCargoPorLocalyPerfil')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerCargoPorLocalyPerfil'
  DROP  Procedure  dbo.spu_Alerta_ObtenerCargoPorLocalyPerfil
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerCargoPorLocalyPerfil'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerCargoPorLocalyPerfil
/******************************************************************************
**    Descripcion  : obtiene listado de cargos segun el local y perfil seleccionado
**    Autor        : VSR
**    Fecha        : 16/04/2015
*******************************************************************************/
@LocalDestinoCodigo AS VARCHAR(255),
@LlavePerfil AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @IdUsuario INT
  SET @LocalDestinoCodigo = UPPER(RTRIM(LTRIM(@LocalDestinoCodigo)))

  -----------------------------------------------------------------------------
  -- se mantiene la misma estructura de campos que tiene el select de la TABLA 3: Contactos Proximo Escalamiento
  -- del procedimiento spu_Alerta_ObtenerDetalle
  -----------------------------------------------------------------------------
  DECLARE @Tabla AS TABLE(IdEscalamientoPorAlertaContacto INT, NombreUsuario VARCHAR(1000), Telefono VARCHAR(255), Email VARCHAR(255), Cargo VARCHAR(255),
                          TipoGrupo VARCHAR(255))

  INSERT INTO @Tabla(IdEscalamientoPorAlertaContacto, NombreUsuario, Telefono, Email, Cargo, TipoGrupo)
  SELECT
    IdEscalamientoPorAlertaContacto = U.Id,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL((SELECT aux.Telefono FROM TelefonosPorUsuario AS aux WHERE aux.IdUsuario = U.Id AND REPLACE(LEFT(CONVERT(VARCHAR,dbo.fnu_GETDATE(),108),5),':','') BETWEEN aux.HoraInicio AND aux.HoraTermino), ISNULL(U.Telefono,'')),
    Email = ISNULL(U.Email,''),
    Cargo = ISNULL(P.Nombre,''),
    TipoGrupo = CASE WHEN PPF.NombrePerfil IS NULL THEN 'Personalizado' ELSE PPF.Tipo END
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    INNER JOIN LocalesPorUsuario AS LPU ON (U.Id = LPU.IdUsuario)
    INNER JOIN Local AS L ON (LPU.IdLocal = L.Id)
    LEFT JOIN vwu_PerfilesPorFormato AS PPF ON (P.Nombre = PPF.NombrePerfil AND PPF.IdFormato = L.IdFormato)
  WHERE
      U.Estado = 1
  AND P.Llave = @LlavePerfil
  AND L.CodigoInterno = @LocalDestinoCodigo
  ORDER BY
    NombreUsuario

  -- cuenta cuantos registos encontro, si no encontro ninguna devuelve todos los Gerentes de Mercado
  DECLARE @TotalRegistros AS INT
  SET @TotalRegistros = (SELECT COUNT(IdEscalamientoPorAlertaContacto) FROM @Tabla)
  
  IF (@TotalRegistros > 0) BEGIN
    SELECT * FROM @Tabla
  END ELSE BEGIN
    EXEC spu_Alerta_ObtenerUsuariosPorPerfil -1, @LlavePerfil
  END

END




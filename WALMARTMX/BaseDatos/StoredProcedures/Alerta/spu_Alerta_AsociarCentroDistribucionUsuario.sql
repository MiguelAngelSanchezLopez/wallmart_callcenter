﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_AsociarCentroDistribucionUsuario')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_AsociarCentroDistribucionUsuario'
  DROP  Procedure  dbo.spu_Alerta_AsociarCentroDistribucionUsuario
END

GO

PRINT  'Creating Procedure spu_Alerta_AsociarCentroDistribucionUsuario'
GO
CREATE Procedure dbo.spu_Alerta_AsociarCentroDistribucionUsuario
/******************************************************************************
**    Descripcion  : asocia el usuario al centro distribucion
**    Autor        : VSR
**    Fecha        : 12/08/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT,
@LocalOrigenCodigo AS VARCHAR(255),
@LocalOrigenNombre AS VARCHAR(255),
@LlavePerfil AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @IdCentroDistribucion INT
    SET @IdCentroDistribucion = (SELECT Id FROM CentroDistribucion WHERE CodigoInterno = @LocalOrigenCodigo)
    IF (@IdCentroDistribucion IS NULL) SET @IdCentroDistribucion = -1

    -------------------------------------------------
    -- si el centro distribucion no existe entonces se crea
    -------------------------------------------------
    IF (@IdCentroDistribucion = -1) BEGIN
      INSERT INTO CentroDistribucion
      (
	      Nombre,
	      CodigoInterno,
	      FechaCreacion,
	      Activo
      )
      VALUES
      (
        @LocalOrigenNombre,
	      @LocalOrigenCodigo,
	      dbo.fnu_GETDATE(),
	      1
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END
      
      SET @IdCentroDistribucion = SCOPE_IDENTITY()
    END

    -------------------------------------------------
    -- elimina alguna asociacion que tenga el centro distribucion con el perfil consultado
    -------------------------------------------------
    DELETE FROM CentroDistribucionPorUsuario
    WHERE
        IdCentroDistribucion = @IdCentroDistribucion
    AND IdUsuario IN (SELECT U.Id FROM Usuario AS U INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id) WHERE P.Llave = @LlavePerfil )

    -------------------------------------------------
    -- asocia el centro distribucion con el usuario
    -------------------------------------------------
    INSERT INTO CentroDistribucionPorUsuario
    (
	    IdCentroDistribucion,
	    IdUsuario
    )
    VALUES
    (
	    @IdCentroDistribucion,
	    @IdUsuario
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    SET @Status = 1

  COMMIT
END
GO 
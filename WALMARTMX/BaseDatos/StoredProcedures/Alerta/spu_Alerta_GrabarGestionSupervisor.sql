﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarGestionSupervisor')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarGestionSupervisor'
  DROP  Procedure  dbo.spu_Alerta_GrabarGestionSupervisor
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarGestionSupervisor'
GO
CREATE Procedure [dbo].[spu_Alerta_GrabarGestionSupervisor]
/******************************************************************************
**    Descripcion  : graba la gestion de la alerta realizada por el supervisor
**    Autor        : HML
**    Fecha        : 17/07/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlerta AS INT,
@IdUsuario AS INT,
@Explicacion AS VARCHAR(255),
@ExplicacionOtro AS VARCHAR(255),
@Observacion AS VARCHAR(4000),
@categoriaAlerta AS VARCHAR(500),
@tipoObservacion AS VARCHAR(500),
@clasificacionAlerta AS VARCHAR(100),
@ClasificacionAlertaSist AS VARCHAR(100)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    SET @ExplicacionOtro = ISNULL(@ExplicacionOtro,'')
    IF (@ExplicacionOtro <> '') SET @Explicacion = @ExplicacionOtro

    ----------------------------------------------------------
    -- graba el registro
    ----------------------------------------------------------
    INSERT INTO CallCenterHistorialEscalamiento
    (
	    IdAlerta,
	    Fecha,
	    Explicacion,
	    Observacion,
		IdUsuarioCreacion,
		CategoriaAlerta,
		TipoObservacion,
		ClasificacionAlerta,
		ClasificacionAlertaSist
    )
    VALUES
    (
	    @IdAlerta,
		dbo.fnu_GETDATE(),
	    @Explicacion,
	    @Observacion,
	  	@IdUsuario,
		  @categoriaAlerta,
		  @tipoObservacion,
		  @clasificacionAlerta,
		  @ClasificacionAlertaSist
    )

    SET @Status = SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

	DECLARE @StatusTipoGeneral VARCHAR(255)
    IF (@ExplicacionOtro <> '') BEGIN
      EXEC spu_Sistema_GrabarTipoGeneral @StatusTipoGeneral OUTPUT, @ExplicacionOtro, 'Explicacion', 'Normal', '-1'
    END

  COMMIT
END

GO 
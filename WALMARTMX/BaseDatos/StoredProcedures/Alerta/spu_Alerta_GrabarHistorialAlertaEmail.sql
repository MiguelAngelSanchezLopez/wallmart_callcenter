﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarHistorialAlertaEmail')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarHistorialAlertaEmail'
  DROP  Procedure  dbo.spu_Alerta_GrabarHistorialAlertaEmail
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarHistorialAlertaEmail'
GO
CREATE Procedure dbo.spu_Alerta_GrabarHistorialAlertaEmail
/******************************************************************************
**    Descripcion  : graba los datos de la alerta que se envia por Email
**    Autor        : VSR
**    Fecha        : 08/07/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@DescripcionAlerta AS VARCHAR(255),
@NroTransporte AS INT,
@LocalDestino AS INT,
@Email AS VARCHAR(255),
@EnviadoA AS VARCHAR(255),
@Cargo AS VARCHAR(255),
@GrupoAlerta AS VARCHAR(255),
@Ocurrencia AS INT,
@EmailMensaje AS TEXT,
@EnviadoDesde AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    INSERT INTO HistorialAlertaEmail
    (
      NroTransporte,
	    DescripcionAlerta,
	    LocalDestino,
	    FechaEnvio,
	    Email,
	    EnviadoA,
	    Cargo,
      GrupoAlerta,
      Ocurrencia,
	    EmailMensaje,
      EnviadoDesde
    )
    VALUES
    (
	    @NroTransporte,
	    @DescripcionAlerta,
	    @LocalDestino,
	    dbo.fnu_GETDATE(),
	    @Email,
	    @EnviadoA,
	    @Cargo,
      @GrupoAlerta,
      @Ocurrencia,
	    @EmailMensaje,
      @EnviadoDesde
    )

    SET @Status = SCOPE_IDENTITY()
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_AsociarLocalUsuario')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_AsociarLocalUsuario'
  DROP  Procedure  dbo.spu_Alerta_AsociarLocalUsuario
END

GO

PRINT  'Creating Procedure spu_Alerta_AsociarLocalUsuario'
GO
CREATE Procedure dbo.spu_Alerta_AsociarLocalUsuario
/******************************************************************************
**    Descripcion  : asocia el usuario al local
**    Autor        : VSR
**    Fecha        : 12/08/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT,
@LocalDestinoCodigo AS VARCHAR(255),
@LocalDestinoNombre AS VARCHAR(255),
@LlavePerfil AS VARCHAR(255),
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @IdLocal INT
    SET @IdLocal = (SELECT Id FROM Local WHERE CodigoInterno = @LocalDestinoCodigo)
    IF (@IdLocal IS NULL) SET @IdLocal = -1

    -------------------------------------------------
    -- si el local no existe entonces se crea
    -------------------------------------------------
    IF (@IdLocal = -1) BEGIN
      INSERT INTO Local
      (
	      Nombre,
	      CodigoInterno,
	      IdFormato,
	      FechaCreacion,
	      Activo
      )
      VALUES
      (
        @LocalDestinoNombre,
	      @LocalDestinoCodigo,
	      @IdFormato,
	      dbo.fnu_GETDATE(),
	      1
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END
      
      SET @IdLocal = SCOPE_IDENTITY()
    END

    -------------------------------------------------
    -- elimina alguna asociacion que tenga el local con el perfil consultado
    -------------------------------------------------
    DELETE FROM LocalesPorUsuario
    WHERE
        IdLocal = @IdLocal
    AND IdUsuario IN (SELECT U.Id FROM Usuario AS U INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id) WHERE P.Llave = @LlavePerfil )

    -------------------------------------------------
    -- asocia el local con el usuario
    -------------------------------------------------
    INSERT INTO LocalesPorUsuario
    (
	    IdLocal,
	    IdUsuario
    )
    VALUES
    (
	    @IdLocal,
	    @IdUsuario
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    SET @Status = 1

  COMMIT
END
GO 
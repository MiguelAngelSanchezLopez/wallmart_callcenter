﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerDetalleDefincionAlerta')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerDetalleDefincionAlerta'
  DROP  Procedure  dbo.spu_Alerta_ObtenerDetalleDefincionAlerta
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerDetalleDefincionAlerta'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerDetalleDefincionAlerta
/******************************************************************************
**    Descripcion  : Obtiene detalle de la definicion de alerta
**    Por          : VSR, 10/04/2015
*******************************************************************************/
@IdAlertaDefinicion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  ----------------------------------------------------------------
  -- TABLA 0: DETALLE
  ----------------------------------------------------------------
  SELECT
	  IdAlertaDefinicion = AD.Id,
	  NombreAlerta = ISNULL(AD.Nombre,''),
    TieneRegistrosAsociados = CASE WHEN (
                                (SELECT COUNT(aux.IdAlertaDefinicion) FROM AlertaDefinicionPorFormato AS aux WHERE aux.IdAlertaDefinicion = AD.Id)
                              ) = 0 THEN '0' ELSE '1' END,
    GestionarPorCemtra = ISNULL(AD.GestionarPorCemtra, 0)
  FROM
    AlertaDefinicion AS AD
  WHERE
    AD.Id = @IdAlertaDefinicion

  ----------------------------------------------------------------
  -- TABLA 1: Formatos
  ----------------------------------------------------------------
  SELECT
    Texto = ISNULl(F.Nombre,''),
    Valor = F.Id,
    Seleccionado = CASE WHEN ADPF.IdFormato IS NOT NULL THEN 1 ELSE 0 END
  FROM
    Formato AS F
    LEFT JOIN AlertaDefinicionPorFormato AS ADPF ON (F.Id = ADPF.IdFormato AND ADPF.IdAlertaDefinicion = @IdAlertaDefinicion)
  WHERE
    F.Activo = 1
  ORDER BY
    Texto


END



   
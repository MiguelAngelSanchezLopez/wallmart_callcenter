﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerPorNroTransporte')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerPorNroTransporte'
  DROP  Procedure  dbo.spu_Alerta_ObtenerPorNroTransporte
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerPorNroTransporte'
GO

CREATE Procedure [dbo].[spu_Alerta_ObtenerPorNroTransporte] (  
@NroTransporte as INT  
) AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    Alerta
  WHERE   
    NroTransporte = @NroTransporte
  
END  
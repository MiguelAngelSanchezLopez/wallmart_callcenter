﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_TieneCertificacion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_TieneCertificacion'
  DROP  Procedure  dbo.spu_Alerta_TieneCertificacion
END

GO

PRINT  'Creating Procedure spu_Alerta_TieneCertificacion'
GO
CREATE Procedure dbo.spu_Alerta_TieneCertificacion
/******************************************************************************
**    Descripcion  : verifica si el viaje tiene certificaciones
**    Autor        : VSR
**    Fecha        : 08/09/2015
*******************************************************************************/
@NroTransporte AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    T.*
  FROM (
        --------------------------------------------------------------------
        -- obtiene Auditoria CD
        --------------------------------------------------------------------
        SELECT DISTINCT
          Fecha = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(FCP.FECHACREACION,'FECHA_COMPLETA'),''),
          RealizadoPor = ISNULL(FCP.CLD,''),
          IdFormulario = ISNULL(FCP.IDENTIFIER,'-1')
        FROM 
          SMUFormCargaPagina2 AS FCP
        WHERE
        (
             FCP.NT = @NroTransporte
          OR FCP.OPCIO = @NroTransporte
        )

        --------------------------------------------------------------------
        -- obtiene Recepcion local
        --------------------------------------------------------------------
        UNION ALL
        SELECT DISTINCT
          Fecha = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(FRP.FECHACREACION,'FECHA_COMPLETA'),''),
          RealizadoPor = ISNULL(FRP.USERC,''),
          IdFormulario = ISNULL(FRP.IDENTIFIER,'-1')
        FROM 
          SMUFormRecepcionPagina2 AS FRP
        WHERE
        (
              FRP.NT = @NroTransporte
          OR FRP.NHR = @NroTransporte
          OR FRP.NT2 = @NroTransporte
          OR FRP.NTOPC = @NroTransporte
        )
  
  ) AS T
  WHERE
    T.IdFormulario <> '-1'

END




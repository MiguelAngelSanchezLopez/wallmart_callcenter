﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarEstadoTransportista')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarEstadoTransportista'
  DROP  Procedure  dbo.spu_Alerta_GrabarEstadoTransportista
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarEstadoTransportista'
GO
CREATE Procedure dbo.spu_Alerta_GrabarEstadoTransportista
/******************************************************************************
**    Descripcion  : graba el estado del transportista
**    Autor        : VSR
**    Fecha        : 16/12/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlertaRojaEstadoActual AS INT,
@RutTransportista AS VARCHAR(255),
@ListadoIdEstados AS VARCHAR(8000),
@Observacion AS VARCHAR(4000),
@MontoUnoTransportista AS INT,
@MontoDosTransportista AS INT,
@EstadoAlertaRoja AS VARCHAR(255)
AS
BEGIN

  DECLARE @ESTADO_ALERTA_ROJA_FINALIZADO VARCHAR(255), @ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION VARCHAR(255)
  SET @ESTADO_ALERTA_ROJA_FINALIZADO = 'FINALIZADO'
  SET @ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION = 'POR APROBAR PLAN ACCION'

  IF (@MontoUnoTransportista = -1) SET @MontoUnoTransportista = NULL
  IF (@MontoDosTransportista = -1) SET @MontoDosTransportista = NULL

  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @IdUsuarioTransportista AS INT
    SELECT
      @IdUsuarioTransportista = U.Id
    FROM
      Usuario AS U
    WHERE
      ISNULL(U.Rut,'') + ISNULL('-' + U.Dv,'') = @RutTransportista

    --------------------------------------------------
    -- actualiza la tabla AlertaRojaEstadoActual
    --------------------------------------------------
    UPDATE AlertaRojaEstadoActual
    SET
	    ObservacionEstado = @Observacion
	    ,MontoUnoTransportista = @MontoUnoTransportista
	    ,MontoDosTransportista = @MontoDosTransportista
    WHERE
      Id = @IdAlertaRojaEstadoActual

    --------------------------------------------------
    -- actualiza la tabla UsuarioDatoExtra
    --------------------------------------------------
    UPDATE UsuarioDatoExtra
    SET
	    ObservacionEstadoActualTransportista = @Observacion
    WHERE
      IdUsuario = @IdUsuarioTransportista

	  ------------------------------------------------------------------------------------
    -- inserta los estados
    ------------------------------------------------------------------------------------
    IF (@EstadoAlertaRoja = @ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION) BEGIN
      -- inserta todos los planes de accion seleccionados
	    IF(@ListadoIdEstados <> '') BEGIN
		    INSERT INTO EstadoTransportistaPorAlertaRojaEstadoActual
		    (
			    idTipoGeneral,
			    idAlertaRojaEstadoActual,
          Sugerida,
          Aprobada
		    )
		    SELECT
			    valor,
			    @IdAlertaRojaEstadoActual,
          1,
          0
		    FROM
		      dbo.fnu_InsertaListaTabla(@ListadoIdEstados)
	    END
    END ELSE BEGIN
	    IF(@ListadoIdEstados <> '') BEGIN
        ------------------------------------------------------------------------------------
        -- inserta los nuevos planes de accion que no esten en el listado original
		    INSERT INTO EstadoTransportistaPorAlertaRojaEstadoActual
		    (
			    idTipoGeneral,
			    idAlertaRojaEstadoActual,
          Sugerida,
          Aprobada
		    )
		    SELECT
			    valor,
			    @IdAlertaRojaEstadoActual,
          0,
          1
		    FROM
		      dbo.fnu_InsertaListaTabla(@ListadoIdEstados)
        WHERE
          valor NOT IN (SELECT idTipoGeneral FROM EstadoTransportistaPorAlertaRojaEstadoActual WHERE idAlertaRojaEstadoActual = @IdAlertaRojaEstadoActual)

        ------------------------------------------------------------------------------------
        -- del listado final seleccionado se marcan las que fueron aprobadas
        UPDATE EstadoTransportistaPorAlertaRojaEstadoActual
        SET Aprobada = 1
        WHERE
          idAlertaRojaEstadoActual = @IdAlertaRojaEstadoActual
        AND idTipoGeneral IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@ListadoIdEstados))

	    END ELSE BEGIN
        ------------------------------------------------------------------------------------
        -- si no se selecciono nada entonces queda todo como desaprobado
        UPDATE EstadoTransportistaPorAlertaRojaEstadoActual
        SET Aprobada = 0
        WHERE
          idAlertaRojaEstadoActual = @IdAlertaRojaEstadoActual

      END

    END
      
    SET @Status = @IdAlertaRojaEstadoActual
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 
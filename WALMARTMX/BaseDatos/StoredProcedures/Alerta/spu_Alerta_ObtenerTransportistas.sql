﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerTransportistas')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerTransportistas'
  DROP  Procedure  dbo.spu_Alerta_ObtenerTransportistas
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerTransportistas'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerTransportistas
/******************************************************************************
**    Descripcion  : obtiene listado de transportistas para dibujar el combo en pantalla de gestion del teleoperador
**    Autor        : VSR
**    Fecha        : 13/11/2014
*******************************************************************************/
@RutTransportista AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @IdUsuario INT
  SET @RutTransportista = UPPER(RTRIM(LTRIM(@RutTransportista)))
  IF (@RutTransportista = '') SET @RutTransportista = '-1'
  
  -----------------------------------------------------------------------------
  -- busca el usuario por el nombre consultado
  -----------------------------------------------------------------------------
  SELECT TOP 1
    @IdUsuario = U.Id
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      P.Llave = 'TRA'
  AND U.Rut = @RutTransportista
  

  -- si no encuentra el usuario entonces setea variable para obtener todos los usuarios
  IF (@IdUsuario IS NULL) SET @IdUsuario = -1


  -- obtiene el perfil TRANSPORTISTA
  EXEC spu_Alerta_ObtenerUsuariosPorPerfil @IdUsuario, 'TRA'

END




﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerListadoRespuestaAlertaRoja')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerListadoRespuestaAlertaRoja'
  DROP  Procedure  dbo.spu_Alerta_ObtenerListadoRespuestaAlertaRoja
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerListadoRespuestaAlertaRoja'
GO
CREATE Procedure [dbo].[spu_Alerta_ObtenerListadoRespuestaAlertaRoja]    
/******************************************************************************    
**    Descripcion  : obtiene listado respuestas alertas rojas  
**    Por          : VSR, 15/12/2014    
*******************************************************************************/    
@IdUsuario AS INT,  
@NroTransporte AS INT,    
@Estado AS VARCHAR(255)    
AS    
BEGIN    
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
  SET NOCOUNT ON    
    
  SELECT DISTINCT    
    IdAlertaRojaEstadoActual = EA.Id,    
    NroTransporte = EA.NroTransporte,  
    NombreTransportista = ISNULL(A.NombreTransportista,''),
    RutTransportista = RTRIM(LTRIM(REPLACE(REPLACE(A.RutTransportista,' ',''),'.',''))),
    Estado = EA.Estado,    
    Fecha = dbo.fnu_ConvertirDatetimeToDDMMYYYY(EA.FechaModificacion, 'FECHA_COMPLETA'),    
    ModificadoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),    
    FechaModificacion = EA.FechaModificacion    
  FROM    
    AlertaRojaEstadoActual AS EA    
    INNER JOIN Usuario AS U ON (EA.IdUsuarioModificacion = U.Id)    
    INNER JOIN Alerta AS A ON (EA.NroTransporte = A.NroTransporte)
    INNER JOIN CallCenterAlerta AS CCA ON (A.Id = CCA.IdAlerta)
  WHERE    
      ( @NroTransporte = -1 OR EA.NroTransporte = @NroTransporte )    
  AND (   
           (@Estado = '-1' AND EA.Estado <> 'ENVIADO TRANSPORTISTA')  
        OR (EA.Estado = @Estado)  
      )    
  ORDER BY    
    FechaModificacion    
        
END  

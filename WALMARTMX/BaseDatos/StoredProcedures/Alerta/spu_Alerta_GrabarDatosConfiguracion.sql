﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarDatosConfiguracion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarDatosConfiguracion'
  DROP  Procedure  dbo.spu_Alerta_GrabarDatosConfiguracion
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarDatosConfiguracion'
GO
CREATE Procedure dbo.spu_Alerta_GrabarDatosConfiguracion
/******************************************************************************
**  Descripcion  : graba los datos de la configuracion de la alerta
**  Fecha        : 31/07/2014
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdAlertaConfiguracionOutput AS INT OUTPUT,
@IdAlertaConfiguracion AS INT,
@IdAlertaDefinicionPorFormato AS INT,
@Prioridad AS VARCHAR(255),
@FrecuenciaRepeticionMinutos AS INT,
@Activo AS INT,
@TipoAlerta AS VARCHAR(255),
@FechaInicio AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @dateFechaInicio AS DATETIME
  IF (@FechaInicio = '-1') BEGIN
    SET @dateFechaInicio = NULL
  END ELSE BEGIN
    SET @dateFechaInicio = CONVERT(DATETIME, @FechaInicio)
  END

  BEGIN TRANSACTION

    -- ingresa nuevo registro
    IF (@IdAlertaConfiguracion = -1) BEGIN
      INSERT INTO AlertaConfiguracion
      (
	      IdAlertaDefinicionPorFormato,
	      Prioridad,
	      FrecuenciaRepeticionMinutos,
        Activo,
        TipoAlerta,
        FechaInicio
      )
      VALUES
      (
	      @IdAlertaDefinicionPorFormato,
	      @Prioridad,
	      @FrecuenciaRepeticionMinutos,
        @Activo,
        @TipoAlerta,
        @dateFechaInicio
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdAlertaConfiguracionOutput = @IdAlertaConfiguracion
        RETURN
      END

      SET @Status = 'ingresado'
      SET @IdAlertaConfiguracionOutput = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE AlertaConfiguracion
      SET
	      Prioridad = @Prioridad,
	      FrecuenciaRepeticionMinutos = @FrecuenciaRepeticionMinutos,
        Activo = @Activo,
        TipoAlerta = @TipoAlerta,
        FechaInicio = @dateFechaInicio
      WHERE
        Id = @IdAlertaConfiguracion

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        SET @IdAlertaConfiguracionOutput = @IdAlertaConfiguracion
        RETURN
      END

      SET @Status = 'modificado'
      SET @IdAlertaConfiguracionOutput = @IdAlertaConfiguracion

    END

  COMMIT
END
GO        
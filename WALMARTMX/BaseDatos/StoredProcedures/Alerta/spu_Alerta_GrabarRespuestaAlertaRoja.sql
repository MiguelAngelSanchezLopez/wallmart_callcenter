﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarRespuestaAlertaRoja')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarRespuestaAlertaRoja'
  DROP  Procedure  dbo.spu_Alerta_GrabarRespuestaAlertaRoja
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarRespuestaAlertaRoja'
GO
CREATE Procedure dbo.spu_Alerta_GrabarRespuestaAlertaRoja
/******************************************************************************
**    Descripcion  : graba la respuesta de la alerta roja
**    Autor        : VSR
**    Fecha        : 04/08/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT,
@NroTransporte AS INT,
@IdAlerta AS INT,
@Observacion AS VARCHAR(4000),
@Rechazo AS BIT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    -- graba el registro
    INSERT AlertaRojaRespuesta
    (
	    NroTransporte,
	    IdAlerta,
	    Observacion,
	    IdUsuarioCreacion,
	    FechaCreacion,
		  Rechazo
    )
    VALUES
    (
	    @NroTransporte,
	    @IdAlerta,
	    @Observacion,
	    @IdUsuario,
	    dbo.fnu_GETDATE(),
      @Rechazo
    )

    SET @Status = SCOPE_IDENTITY()

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 
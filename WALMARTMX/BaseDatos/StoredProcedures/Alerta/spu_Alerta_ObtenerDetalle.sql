﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Alerta_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerDetalle
/******************************************************************************
**    Descripcion  : obtiene detalle de la alerta
**    Por          : VSR, 07/07/2014
*******************************************************************************/
@IdAlerta AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @NroEscalamientoActual INT, @TotalEscalamientoAlerta INT, @ProximoEscalamiento INT, @NombreAlerta VARCHAR(255), @IdAlertaGestionAcumuladaPadre INT
  DECLARE @LocalDestino VARCHAR(10), @TotalEscalamientosRealizados INT, @IdFormato INT, @HoraActual VARCHAR(255), @NroTransporte BIGINT, @IdEmbarque BIGINT

  SET @HoraActual = LEFT(REPLACE(CONVERT(VARCHAR, dbo.fnu_GETDATE(), 108),':',''),4)

  /**
   * obtiene nombre de la alerta */
  SELECT
    @NombreAlerta = A.DescripcionAlerta,
    @LocalDestino = A.LocalDestino,
    @IdFormato = L.IdFormato,
    @NroTransporte = A.NroTransporte,
    @IdEmbarque = A.IdEmbarque
  FROM
    CallCenterAlerta AS CCA
    INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
  WHERE
    A.Id = @IdAlerta

  IF (@LocalDestino IS NULL OR @LocalDestino = '0') SET @LocalDestino = '-1'
  IF (@IdFormato IS NULL OR @IdFormato = '0') SET @IdFormato = -1
  

  /**
   * obtiene el total de escalamientos configurado para la alerta */
  SELECT
    @TotalEscalamientoAlerta = COUNT(EPA.Id)
  FROM
    AlertaConfiguracion AS AC
    INNER JOIN EscalamientoPorAlerta AS EPA ON (AC.Id = EPA.IdAlertaConfiguracion)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
  WHERE
      AD.Nombre = @NombreAlerta
  AND ADPF.IdFormato = @IdFormato
  AND AC.Activo = 1
  AND EPA.Eliminado = 0


  /**
   * obtiene el escalamiento actual de la alerta */
  SELECT TOP 1
    @NroEscalamientoActual = EPA.Orden
  FROM
    CallCenterHistorialEscalamiento AS HE
    INNER JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
  WHERE
    HE.IdAlerta = @IdAlerta
  ORDER BY
    EPA.Orden DESC
  IF (@NroEscalamientoActual IS NULL) SET @NroEscalamientoActual = 0

  /**
   * obtiene el proximo escalamiento a registrar */
  SET @ProximoEscalamiento = @NroEscalamientoActual + 1

  /**
   * obtiene idAlertaGestionAcumulada de la alerta padre */
  SELECT TOP 1
    @IdAlertaGestionAcumuladaPadre = AGA.Id
  FROM
    CallCenterAlertaGestionAcumulada AS AGA
  WHERE
      AGA.IdAlerta = @IdAlerta
  AND AGA.IdAlertaHija = @IdAlerta
  IF (@IdAlertaGestionAcumuladaPadre IS NULL) SET @IdAlertaGestionAcumuladaPadre = -1

  /**
   * obtiene el total de escalamientos realizados en la gestion de la alerta */
  SELECT
    @TotalEscalamientosRealizados = COUNT(HE.Id)
  FROM
    CallCenterHistorialEscalamiento AS HE
  WHERE
      HE.IdAlerta = @IdAlerta
  AND HE.IdEscalamientoPorAlerta IS NOT NULL


  ----------------------------------------------------------------
  -- TABLA 0: Detalle
  ----------------------------------------------------------------
  SELECT
    IdAlerta = A.Id,
    NroTransporte = A.NroTransporte,
    IdEmbarque = A.IdEmbarque,
    NombreAlerta = A.DescripcionAlerta,
    Prioridad = AC.Prioridad,
    NombreConductor = ISNULL(A.NombreConductor,''),
    RutConductor = ISNULL(A.RutConductor,''),
    NombreTransportista = ISNULL(A.RutTransportista,''),
    RutTransportista = ISNULL(A.RutTransportista,''),
    PatenteTracto = ISNULL(A.PatenteTracto,''),
    PatenteTrailer = ISNULL(A.PatenteTrailer,''),
    NroEscalamientoActual = @NroEscalamientoActual,
    TotalEscalamientosAlerta = @TotalEscalamientoAlerta,
    ProximoEscalamiento = @ProximoEscalamiento,
    LatTracto = ISNULL(A.LatTracto,''),
    LonTracto = ISNULL(A.LonTracto,''),
    AlertaMapa = ISNULL(A.AlertaMapa,''),
    Cerrado = CONVERT(INT,ISNULL((CCA.Cerrado),0)),
    CerradoSatisfactorio = CONVERT(INT,ISNULL((CCA.CerradoSatisfactorio),0)),
    IdAlertaGestionAcumuladaPadre = @IdAlertaGestionAcumuladaPadre,
    LocalOrigenCodigo = ISNULL(A.OrigenCodigo,-1),
    LocalOrigenDescripcion = ISNULL(CD.Nombre, ISNULL(CDRetorno.Nombre,'')),
    LocalDestinoCodigo = ISNULL(L.CodigoInterno,-1),
    LocalDestinoDescripcion = ISNULL(L.Nombre,''),
    IdFormato = F.Id,
    NombreFormato = ISNULL(F.Nombre,''),
    FrecuenciaRepeticionMinutos = ISNULL(AC.FrecuenciaRepeticionMinutos,-1),
    Permiso = ISNULL(A.Permiso,''),
    Velocidad = ISNULL(A.Velocidad,'0'),
    TipoViaje = ISNULL(TV.TipoViaje,''),
    FechaHoraCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(A.FechaHoraCreacion, 'FECHA_COMPLETA'),
    TipoAlerta = ISNULL(A.TipoAlerta,''),
    TipoAlertaDescripcion = ISNULL(A.TipoAlertaDescripcion,''),
    TotalEscalamientosRealizados = @TotalEscalamientosRealizados,
    RespuestaCategoria = ISNULL(rc.Nombre,'Sin Información'),
    RespuestaObservacion = ISNULL(ra.Observacion,'Sin Información'),
    ClasificacionAlertaSist = CASE WHEN arb.idAlerta IS NULL THEN '' ELSE 'Roja' END,  
    Ocurrencias = ISNULL(arb.ocurrencias,0),
    TipoFlota = TV.TipoFlota,
    NumeroEconomicoTracto = ISNULL(tmTracto.Economico,''),
    NumeroEconomicoTrailer = ISNULL(tmTrailer.Economico,''),
    TelefonoLocal = ISNULL(L.Telefono, ''),
    ReportabilidadPatenteTracto = ISNULL(TLPTracto.Estado,'NoIntegrada'),
    ReportabilidadPatenteTractoFecha = ISNULL(TLPTracto.Fecha,''),
    ReportabilidadPatenteTrailer = ISNULL(TLPTrailer.Estado,'NoIntegrada'),
    ReportabilidadPatenteTrailerFecha = ISNULL(TLPTrailer.Fecha,''),
    HoraCita = ISNULL(TV.HoraCita, ''),
    VentanaHoraria = dbo.fnu_ObtenerVentanaHorarioLocal(A.LocalDestino)
  FROM
    CallCenterAlerta AS CCA
    INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
    INNER JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    LEFT JOIN CentroDistribucion AS CD ON (A.OrigenCodigo = CD.CodigoInterno)
    LEFT JOIN Local AS CDRetorno ON (A.OrigenCodigo = CDRetorno.CodigoInterno) -- se utiliza para backhault, cuando el origen es un local
    LEFT JOIN (
                SELECT TOP 1
                  NroTransporte = TV.NroTransporte,
                  IdEmbarque = TV.IdEmbarque,
                  TipoViaje = ISNULL(TV.TipoViaje,''),
                  TipoFlota = ISNULL(IT.CT_Tipo_Flota,''),
                  HoraCita = ISNULL(LEFT(CONVERT(VARCHAR, IT.Fecha_Cita_Asignada, 108), 5), '')
                FROM
                  CallCenterAlerta AS CCA
                  INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
                  INNER JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.IdEmbarque = TV.IdEmbarque)
                  INNER JOIN InformacionTracto AS IT ON (TV.NroTransporte = IT.Id_Master AND TV.IdEmbarque = IT.Id_Embarque )
                WHERE
                    A.Id = @IdAlerta
                AND (@LocalDestino = '-1' OR TV.LocalDestino = @LocalDestino)
    ) AS TV ON (A.NroTransporte = TV.NroTransporte AND A.IdEmbarque = TV.IdEmbarque)
    LEFT JOIN RespuestaAlerta ra ON ra.idAlerta = a.Id
    LEFT JOIN RespuestaCategoria rc ON rc.id = ra.idRespuestaCategoria
    LEFT JOIN AlertaRojaBatch arb ON arb.IdAlerta = A.Id
    LEFT JOIN Track_Movil tmTracto ON tmTracto.Patente = A.PatenteTracto
    LEFT JOIN Track_Movil tmTrailer ON tmTrailer.Patente = A.PatenteTrailer
    LEFT JOIN vwu_ReportabilidadPatente AS TLPTracto ON (REPLACE(A.PatenteTracto,'-','') = TLPTracto.Patente)
    LEFT JOIN vwu_ReportabilidadPatente AS TLPTrailer ON (REPLACE(A.PatenteTrailer,'-','') = TLPTrailer.Patente)
  WHERE
    A.Id = @IdAlerta

  ----------------------------------------------------------------
  -- TABLA 1: Historial Escalamiento
  ----------------------------------------------------------------
  SELECT
    IdAlerta = HE.IdAlerta,
    IdHistorialEscalamiento = HE.Id,
    NroEscalamiento = CASE WHEN (EPA.Orden IS NULL) THEN 'Supervisor' ELSE CONVERT(VARCHAR,EPA.Orden) END,
    Fecha = dbo.fnu_ConvertirDatetimeToDDMMYYYY(HE.Fecha,'FECHA_COMPLETA'),
    Explicacion = ISNULL(HE.Explicacion,''),
    TipoExplicacion = ISNULL(TablaExplicacion.Extra1,'Normal'),
    Observacion = ISNULL(HE.Observacion,''),
    AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    EmailCopia = ISNULL(EPA.EmailCopia,''),
    IdEscalamientoPorAlerta = ISNULL(HE.IdEscalamientoPorAlerta,-1),
    Eliminado = ISNULL(CONVERT(INT, EPA.Eliminado),0),
    ClaveNroEscalamiento = CASE WHEN (EPA.Orden IS NULL) THEN 'Supervisor' ELSE 
                             CONVERT(VARCHAR,EPA.Orden) + CASE WHEN (EPA.Eliminado = 1) THEN '-eliminado' ELSE '' END
                           END    
  FROM
    CallCenterHistorialEscalamiento AS HE
    LEFT JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
    LEFT JOIN Usuario AS U ON (HE.IdUsuarioCreacion = U.Id)
    LEFT JOIN (
      SELECT DISTINCT Valor, Extra1 FROM TipoGeneral AS TG WHERE TG.Tipo IN ('Explicacion','ExplicacionMultipunto')
    ) AS TablaExplicacion ON (HE.Explicacion = TablaExplicacion.Valor)
  WHERE
    HE.IdAlerta = @IdAlerta
  ORDER BY
    HE.Fecha DESC

  ----------------------------------------------------------------
  -- TABLA 2: Historial Escalamiento Contacto
  ----------------------------------------------------------------
  SELECT
    IdHistorialEscalamientoContacto = HEC.Id,
    IdHistorialEscalamiento = HEC.IdHistorialEscalamiento,
    IdEscalamientoPorAlertaContacto = ISNULL(HEC.IdEscalamientoPorAlertaContacto,-1),
    IdUsuarioSinGrupoContacto = ISNULL(HEC.IdUsuarioSinGrupoContacto,-1),
    NombreUsuario = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
    	                ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,'')
    	              ELSE
    	              	ISNULL(USGC.Nombre,'') + ISNULL(' ' + USGC.Paterno,'') + ISNULL(' ' + USGC.Materno,'')
    	              END,
    Cargo = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
              ISNULL(EPAC.Cargo,'')
            ELSE
            	ISNULL(P.Nombre,'')
            END,
    Telefono = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
                  ISNULL(U.Telefono,'')
                ELSE
            	    ISNULL(USGC.Telefono,'')
                END,
    Email = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
              ISNULL(U.Email,'')
            ELSE
        	    ISNULL(USGC.Email,'')
            END,
    Explicacion = ISNULL(HEC.Explicacion,''),
    TipoExplicacion = ISNULL(TablaExplicacion.Extra1,'Normal'),
    Observacion = ISNULL(HEC.Observacion,''),
    AgrupadoEn = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
    	              UPPER(ISNULL(EPAGC.Nombre,''))
    	            ELSE
    	              UPPER(ISNULL(P.Nombre,''))
    	            END,
    TipoGrupo = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
    	            ISNULL((SELECT TOP 1 aux.Tipo FROM vwu_PerfilesPorFormato AS aux WHERE EPAGC.Nombre = aux.NombrePerfil AND aux.IdFormato = @IdFormato),'Personalizado')
    	          ELSE
                  ISNULL((SELECT TOP 1 aux.Tipo FROM vwu_PerfilesPorFormato AS aux WHERE P.Nombre = aux.NombrePerfil AND aux.IdFormato = @IdFormato),'Personalizado')
    	          END,
    OrdenEscalamiento = CASE WHEN (EPA.Orden IS NULL) THEN 'Supervisor' ELSE CONVERT(VARCHAR,EPA.Orden) END,
    IdEscalamientoPorAlerta = (SELECT TOP 1 aux.IdEscalamientoPorAlerta FROM EscalamientoPorAlertaGrupoContacto AS aux WHERE aux.Id = HEC.IdEscalamientoPorAlertaGrupoContacto),
    IdEscalamientoPorAlertaGrupoContacto = HEC.IdEscalamientoPorAlertaGrupoContacto,
    ClaveOrdenEscalamiento = CASE WHEN (EPA.Orden IS NULL) THEN 'Supervisor' ELSE 
                               CONVERT(VARCHAR,EPA.Orden) + CASE WHEN (EPA.Eliminado = 1) THEN '-eliminado' ELSE '' END
                             END,
    NotificarPorEmail = CONVERT(INT,ISNULL(EPAGC.NotificarPorEmail,1)),
    NotificarPorEmailHorarioNocturno = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
                                         CONVERT(INT,ISNULL(UDE.NotificarPorEmailHorarioNocturno,0))
                                       ELSE
        	                               CONVERT(INT,ISNULL(USGCDE.NotificarPorEmailHorarioNocturno,0))
                                       END
  FROM
    CallCenterHistorialEscalamientoContacto AS HEC
    INNER JOIN CallCenterHistorialEscalamiento AS HE ON (HEC.IdHistorialEscalamiento = HE.Id)
    LEFT JOIN EscalamientoPorAlertaContacto AS EPAC ON (HEC.IdEscalamientoPorAlertaContacto = EPAC.Id)
    LEFT JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (HEC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
    LEFT JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
    LEFT JOIN Usuario AS U ON (EPAC.IdUsuario = U.Id)
    LEFT JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
    LEFT JOIN Usuario AS USGC ON (HEC.IdUsuarioSinGrupoContacto = USGC.Id)
    LEFT JOIN UsuarioDatoExtra AS USGCDE ON (USGC.Id = USGCDE.IdUsuario)
    LEFT JOIN Perfil AS P ON (USGC.IdPerfil = P.Id)
    LEFT JOIN (
      SELECT DISTINCT Valor, Extra1 FROM TipoGeneral AS TG WHERE TG.Tipo IN ('Explicacion','ExplicacionMultipunto')
    ) AS TablaExplicacion ON (HEC.Explicacion = TablaExplicacion.Valor)
  WHERE
    HE.IdAlerta = @IdAlerta
  ORDER BY
    IdHistorialEscalamiento

  ----------------------------------------------------------------
  -- TABLA 3: Contactos Proximo Escalamiento
  ----------------------------------------------------------------
  SELECT
    IdEscalamientoPorAlertaContacto = ISNULL(EPAC.Id,-1),
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL((SELECT aux.Telefono FROM TelefonosPorUsuario AS aux WHERE aux.IdUsuario = U.Id AND REPLACE(LEFT(CONVERT(VARCHAR,dbo.fnu_GETDATE(),108),5),':','') BETWEEN aux.HoraInicio AND aux.HoraTermino), ISNULL(U.Telefono,'')),
    Email = ISNULL(U.Email,''),
    Cargo = ISNULL(EPAC.Cargo,''),
    IdEscalamientoPorAlertaGrupoContacto = ISNULL(EPAGC.Id,-1),
    AgrupadoEn = UPPER(ISNULL(EPAGC.Nombre,'')),
    LlavePerfil = CASE WHEN PPF.NombrePerfil IS NULL THEN '' ELSE ISNULL(PPF.LlavePerfil,'') END,
    TipoGrupo = CASE WHEN PPF.NombrePerfil IS NULL THEN 'Personalizado' ELSE PPF.Tipo END,
    OrdenGrupoContacto = ISNULL(EPAGC.Orden,1),
    OrdenContacto = ISNULL(EPAC.Orden,1),
    MostrarCuandoEscalamientoAnteriorNoContesta = CONVERT(INT,ISNULL(EPAGC.MostrarCuandoEscalamientoAnteriorNoContesta,0)),
    TieneDependenciaGrupoEscalamientoAnterior = CONVERT(INT,ISNULL(EPAGC.TieneDependenciaGrupoEscalamientoAnterior,0)),
    IdGrupoContactoEscalamientoAnterior = ISNULL(EPAGC.IdGrupoContactoEscalamientoAnterior,-1),
    MostrarSiempre = CONVERT(INT,ISNULL(EPAGC.MostrarSiempre,1)),
    IdGrupoContactoPadre = ISNULL(EPAGC.IdGrupoContactoPadre,-1),
    NotificarPorEmail = CONVERT(INT,ISNULL(EPAGC.NotificarPorEmail,1))
  FROM
    AlertaConfiguracion AS AC
    INNER JOIN EscalamientoPorAlerta AS EPA ON (AC.Id = EPA.IdAlertaConfiguracion)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
    LEFT JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (EPA.Id = EPAGC.IdEscalamientoPorAlerta)
    LEFT JOIN EscalamientoPorAlertaContacto AS EPAC ON (EPAGC.Id = EPAC.IdEscalamientoPorAlertaGrupoContacto)
    LEFT JOIN Usuario AS U ON (EPAC.IdUsuario = U.Id)
    LEFT JOIN vwu_PerfilesPorFormato AS PPF ON (EPAGC.Nombre = PPF.NombrePerfil AND PPF.IdFormato = @IdFormato)
  WHERE
      AD.Nombre = @NombreAlerta
  AND ADPF.IdFormato = @IdFormato
  AND AC.Activo = 1
  AND EPA.Orden = @ProximoEscalamiento
  AND ISNULL(EPAC.Eliminado,0) = 0
  AND EPAGC.Eliminado = 0
  AND EPA.Eliminado = 0
  AND (
        ( -- si hora de inicio es menor o igual que la de termino, indica que esta dentro de un horario que no sobrepasa las 00:00
          (EPAGC.HoraAtencionInicio <= EPAGC.HoraAtencionTermino) AND
          (@HoraActual BETWEEN EPAGC.HoraAtencionInicio AND EPAGC.HoraAtencionTermino)
        )
        OR
        ( -- si hora de inicio es mayor que la hora de termino, indica que pasa las 00:00, por lo que hay que validar en dos tramos
          -- desde [horainicio - 23:59] a [00:00 - horatermino]
          (EPAGC.HoraAtencionInicio > EPAGC.HoraAtencionTermino) AND
          ( (@HoraActual BETWEEN EPAGC.HoraAtencionInicio AND '2359') OR (@HoraActual BETWEEN '0000' AND EPAGC.HoraAtencionTermino) )
        )
      )
  ORDER BY
    OrdenGrupoContacto, OrdenContacto

  ----------------------------------------------------------------
  -- TABLA 4: Script por grupo de contacto
  ----------------------------------------------------------------
  SELECT
    IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id,
    AgrupadoEn = UPPER(ISNULL(EPAGC.Nombre,'')),
    IdScript = S.Id,
    NombreScript = ISNULL(S.Nombre,''),
    Descripcion = ISNULL(S.Descripcion,'')
  FROM
    AlertaConfiguracion AS AC
    INNER JOIN EscalamientoPorAlerta AS EPA ON (AC.Id = EPA.IdAlertaConfiguracion)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
    INNER JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (EPA.Id = EPAGC.IdEscalamientoPorAlerta)
    INNER JOIN Script AS S ON (EPAGC.Id = S.IdEscalamientoPorAlertaGrupoContacto)
  WHERE
      AC.Activo = 1
  AND S.Activo = 1
  AND AD.Nombre = @NombreAlerta
  AND ADPF.IdFormato = @IdFormato
  AND EPA.Orden = @ProximoEscalamiento
  ORDER BY
    IdEscalamientoPorAlertaGrupoContacto

  ----------------------------------------------------------------
  -- TABLA 5: Historial de llamadas
  ----------------------------------------------------------------
  SELECT
    NroEscalamiento = EPA.Orden,
    AgrupadoEn = EPAGC.Nombre,
    NombreContacto = HLLCC.NombreContacto,
    Telefono = HLLCC.Telefono,
    DestinoLocal = HLLCC.DestinoLocal,
    DestinoLocalCodigo = HLLCC.DestinoLocalCodigo,
    Accion = HLLCC.Accion,
    AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    FechaCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(HLLCC.FechaCreacion, 'FECHA_COMPLETA'),
    IdEscalamientoPorAlerta = ISNULL(EPAGC.IdEscalamientoPorAlerta,-1),
    Eliminado = ISNULL(CONVERT(INT, EPA.Eliminado),0),
    ClaveNroEscalamiento = CASE WHEN (EPA.Orden IS NULL) THEN 'Supervisor' ELSE 
                             CONVERT(VARCHAR,EPA.Orden) + CASE WHEN (EPA.Eliminado = 1) THEN '-eliminado' ELSE '' END
                           END
  FROM
    CallCenterHistorialLlamadaEjecutivo AS HLLCC
    INNER JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (HLLCC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
    INNER JOIN EscalamientoPorAlerta AS EPA ON (EPAGC.IdEscalamientoPorAlerta = EPA.Id)
    INNER JOIN Usuario AS U ON (HLLCC.IdUsuarioCreacion = U.Id)
  WHERE
    HLLCC.IdAlertaPadre = @IdAlerta
  ORDER BY
    NroEscalamiento

  ----------------------------------------------------------------
  -- TABLA 6: Historial gestion de alertas para el mismo viaje
  ----------------------------------------------------------------
  SELECT
	  IdAlerta = A.Id,
	  NombreAlerta = A.DescripcionAlerta,
	  FechaHoraCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(A.FechaHoraCreacion, 'FECHA_COMPLETA'),
	  FechaGestion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(HE.Fecha, 'FECHA_COMPLETA'),
	  AtendidoPor = ISNULL(UAtendido.Nombre, '') + ISNULL(' ' + UAtendido.Paterno, '') + ISNULL(' ' + UAtendido.Materno, ''),
    NombreContacto = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
    	                 ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,'')
    	               ELSE
    	                 ISNULL(USGC.Nombre,'') + ISNULL(' ' + USGC.Paterno,'') + ISNULL(' ' + USGC.Materno,'')
    	               END,
    Cargo = CASE WHEN (HEC.IdUsuarioSinGrupoContacto IS NULL) THEN
    	        UPPER(ISNULL(EPAGC.Nombre,''))
    	      ELSE
    	        UPPER(ISNULL(P.Nombre,''))
    	      END,
    Explicacion = ISNULL(HEC.Explicacion,''),
    Observacion = ISNULL(HEC.Observacion,'')
  FROM
	  Alerta AS A
	  INNER JOIN CallCenterAlerta AS CA ON (A.Id = CA.IdAlerta)
	  INNER JOIN CallCenterHistorialEscalamiento AS HE ON (A.Id = HE.IdAlerta)
	  INNER JOIN CallCenterHistorialEscalamientoContacto AS HEC ON (HE.Id = HEC.IdHistorialEscalamiento)
	  INNER JOIN Usuario AS UAtendido ON (HE.IdUsuarioCreacion = UAtendido.Id)
    LEFT JOIN EscalamientoPorAlertaContacto AS EPAC ON (HEC.IdEscalamientoPorAlertaContacto = EPAC.Id)
    LEFT JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (HEC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
    LEFT JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
    LEFT JOIN Usuario AS U ON (EPAC.IdUsuario = U.Id)
    LEFT JOIN Usuario AS USGC ON (HEC.IdUsuarioSinGrupoContacto = USGC.Id)
    LEFT JOIN Perfil AS P ON (USGC.IdPerfil = P.Id)
	WHERE
		  A.NroTransporte = @NroTransporte
	AND A.IdEmbarque = @IdEmbarque
  ORDER BY
    IdAlerta DESC


END

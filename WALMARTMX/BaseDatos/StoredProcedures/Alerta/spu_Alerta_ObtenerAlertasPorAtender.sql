﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerAlertasPorAtender')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerAlertasPorAtender'
  DROP  Procedure  dbo.spu_Alerta_ObtenerAlertasPorAtender
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerAlertasPorAtender'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerAlertasPorAtender
/******************************************************************************
**    Descripcion  : obtiene listado alertas por atender
**    Por          : VSR, 04/07/2014
*******************************************************************************/
@IdUsuario AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @transportistasAsociados AS VARCHAR(4000), @gestionarSoloCemtra AS VARCHAR(255), @totalCDAsociados INT

  -- obtiene datos especiales del usuario
	SELECT 
		@gestionarSoloCemtra = GestionarSoloCemtra, 
		@transportistasAsociados = TransportistasAsociados 
	FROM
    UsuarioDatoExtra 
	WHERE 
		IdUsuario = @IdUsuario

  -- obtiene total de CD asociados
  SET @totalCDAsociados = (SELECT COUNT(IdCentroDistribucion) FROM CentroDistribucionPorUsuario WHERE IdUsuario = @IdUsuario)

  -- formatea valores si estan nulos
  IF (@gestionarSoloCemtra IS NULL) SET @gestionarSoloCemtra = 'Todas'
  IF (@transportistasAsociados IS NULL) SET @transportistasAsociados = ''

  ----------------------------------------------------------------
  -- TABLA 0: Obtiene las alertas que puede gestionar
  ----------------------------------------------------------------
	SELECT TOP 3000
		IdAlerta = AECA.IdAlerta,
		IdAlertaHija = AECA.IdAlertaHija,
		NroTransporte = AECA.NroTransporte,
		NombreAlerta = AECA.NombreAlerta,
		LocalDestino = AECA.LocalDestino,
		IdFormato = AECA.IdFormato,
		Prioridad = AECA.Prioridad,
		OrdenPrioridad = AECA.OrdenPrioridad,
		FechaHoraCreacion = AECA.FechaHoraCreacion,
		FechaHoraCreacionDMA = AECA.FechaHoraCreacionDMA
	FROM
		AlertaEnColaAtencion AS AECA
		LEFT JOIN Alerta AS A ON A.Id = AECA.IdAlerta
		LEFT JOIN Track_Homoclave AS THC ON THC.Nomenclatura = A.RutTransportista
	WHERE
		  ( AECA.Asignada = 0 )
  AND (
  	    -- gestiona todas las alertas
          (	@gestionarSoloCemtra = 'Todas' )
        -- solo las de cemtra
        OR( 
    	          @gestionarSoloCemtra = 'SoloCemtra'
    	      AND AECA.NombreAlerta IN (SELECT aux.Nombre FROM AlertaDefinicion AS aux WHERE aux.GestionarPorCemtra = 1)
    	    )
        -- excluir las de cemtra
        OR( 
    	          @gestionarSoloCemtra = 'ExcluirCemtra'
    	      AND AECA.NombreAlerta IN (SELECT aux.Nombre FROM AlertaDefinicion AS aux WHERE aux.GestionarPorCemtra = 0)
    	    )
  )
  AND (
        -- gestiona todos los transportistas
        ( @transportistasAsociados = '' )
        -- solo los transportistas asociados
        OR( 
    	          @transportistasAsociados <> ''
    	      AND THC.IdUsuario IN (SELECT LT.valor FROM dbo.fnu_InsertaListaTabla(@transportistasAsociados) AS LT)
    	    )
        
  )
  AND (
        -- gestiona todos los CD
        ( @totalCDAsociados = 0)
        -- gestiona solo los CD que tiene asociados
        OR (
                  @totalCDAsociados > 0
              AND A.OrigenCodigo IN (
                                      SELECT
                                        auxCD.CodigoInterno
                                      FROM 
                                        CentroDistribucionPorUsuario AS auxCDPU
                                        INNER JOIN CentroDistribucion AS auxCD ON (auxCDPU.IdCentroDistribucion = auxCD.Id)
                                      WHERE
                                        IdUsuario = @IdUsuario
                                    )
           )
  )
	ORDER BY
		OrdenPrioridad, FechaHoraCreacion

END



  
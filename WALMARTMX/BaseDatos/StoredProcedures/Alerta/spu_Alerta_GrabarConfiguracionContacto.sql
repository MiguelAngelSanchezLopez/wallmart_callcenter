﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarConfiguracionContacto')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarConfiguracionContacto'
  DROP  Procedure  dbo.spu_Alerta_GrabarConfiguracionContacto
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarConfiguracionContacto'
GO
CREATE Procedure dbo.spu_Alerta_GrabarConfiguracionContacto
/******************************************************************************
**  Descripcion  : graba los datos del contacto
**  Fecha        : 06/08/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdEscalamientoPorAlertaContacto AS INT,
@IdEscalamientoPorAlertaGrupoContacto AS INT,
@IdUsuario AS INT,
@Cargo AS VARCHAR(255),
@Orden AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    -- ingresa nuevo registro
    IF (@IdEscalamientoPorAlertaContacto = -1) BEGIN
      INSERT INTO EscalamientoPorAlertaContacto
      (
	      IdEscalamientoPorAlertaGrupoContacto,
	      IdUsuario,
	      Cargo,
	      Orden,
        Eliminado
      )
      VALUES
      (
	      @IdEscalamientoPorAlertaGrupoContacto,
	      @IdUsuario,
	      @Cargo,
	      @Orden,
        0
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE EscalamientoPorAlertaContacto
      SET
	      IdUsuario = @IdUsuario,
	      Cargo = @Cargo,
	      Orden = @Orden
      WHERE
        Id = @IdEscalamientoPorAlertaContacto

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = @IdEscalamientoPorAlertaContacto

    END

  COMMIT
END
GO        
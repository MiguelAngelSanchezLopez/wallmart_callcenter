﻿  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_VerificarDuplicidadDefinicionAlerta')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_VerificarDuplicidadDefinicionAlerta'
  DROP  Procedure  dbo.spu_Alerta_VerificarDuplicidadDefinicionAlerta
END

GO

PRINT  'Creating Procedure spu_Alerta_VerificarDuplicidadDefinicionAlerta'
GO
CREATE Procedure dbo.spu_Alerta_VerificarDuplicidadDefinicionAlerta
/******************************************************************************
**  Descripcion  : valida que la definicion de alerta sea unica (retorno-> 1: No existe; -1: existe)
**  Fecha        : 13/04/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlerta AS INT,
@Valor AS VARCHAR(1028),
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdAlerta IS NULL OR @IdAlerta = 0) SET @IdAlerta = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdAlerta = -1) BEGIN
    IF( EXISTS(
                SELECT
                  AD.Nombre
                FROM
                  AlertaDefinicion AS AD
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(AD.Nombre,'')))) = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  AD.Nombre
                FROM
                  AlertaDefinicion AS AD
                WHERE
                  UPPER(RTRIM(LTRIM(ISNULL(AD.Nombre,'')))) = @Valor
                AND AD.Id <> @IdAlerta
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO       
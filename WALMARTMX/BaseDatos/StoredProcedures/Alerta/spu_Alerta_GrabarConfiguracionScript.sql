﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarConfiguracionScript')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarConfiguracionScript'
  DROP  Procedure  dbo.spu_Alerta_GrabarConfiguracionScript
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarConfiguracionScript'
GO
CREATE Procedure dbo.spu_Alerta_GrabarConfiguracionScript
/******************************************************************************
**  Descripcion  : graba los datos del script
**  Fecha        : 06/08/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdScript AS INT,
@IdEscalamientoPorAlertaGrupoContacto AS INT,
@Nombre AS VARCHAR(255),
@Descripcion AS VARCHAR(4000),
@Activo AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    -- ingresa nuevo registro
    IF (@IdScript = -1) BEGIN
      INSERT INTO Script
      (
	      Nombre,
	      Descripcion,
	      IdEscalamientoPorAlertaGrupoContacto,
	      Activo
      )
      VALUES
      (
	      @Nombre,
	      @Descripcion,
	      @IdEscalamientoPorAlertaGrupoContacto,
	      @Activo
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE Script
      SET
	      Nombre = @Nombre,
	      Descripcion = @Descripcion,
	      Activo = @Activo
      WHERE
        Id = @IdScript

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = @IdScript

    END

  COMMIT
END
GO        
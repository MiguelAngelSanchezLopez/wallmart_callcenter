﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ResumenViaje')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ResumenViaje'
  DROP  Procedure  dbo.spu_Alerta_ResumenViaje
END

GO

PRINT  'Creating Procedure spu_Alerta_ResumenViaje'
GO
CREATE Procedure dbo.spu_Alerta_ResumenViaje
/******************************************************************************
**    Descripcion  : obtiene resumen del viaje consultado
**    Autor        : VSR
**    Fecha        : 30/07/2015
*******************************************************************************/
@NroTransporte AS INT,
@LocalDestino AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  --------------------------------------------------------------------
  -- obtiene total de alertas del viaje
  --------------------------------------------------------------------
  DECLARE @TablaTotalAlertas AS TABLE(LocalDestino INT, TipoAlerta VARCHAR(255), Ocurrencias NUMERIC, TiempoPermanencia NUMERIC)

  INSERT INTO @TablaTotalAlertas(LocalDestino, TipoAlerta, Ocurrencias, TiempoPermanencia)
  SELECT
    T.LocalDestino,
    T.TipoAlerta,
    T.Ocurrencias,
    TiempoPermanencia = DATEDIFF(mi, T.FechaHoraCreacionMinimo, T.FechaHoraCreacionMaximo)
  FROM (
        SELECT
          LocalDestino = A.LocalDestino,
          TipoAlerta = A.TipoAlerta,
          Ocurrencias = COUNT(A.TipoAlerta),
          FechaHoraCreacionMinimo = MIN(A.FechaHoraCreacion),
          FechaHoraCreacionMaximo = MAX(A.FechaHoraCreacion)
        FROM 
          Alerta AS A
        WHERE
          A.NroTransporte = @NroTransporte
        GROUP BY
          A.LocalDestino, A.TipoAlerta
  ) AS T


  --------------------------------------------------------------------------------
  -- TABLA 0: obtiene resumen de las alertas del viaje
  --------------------------------------------------------------------------------
  SELECT DISTINCT
    [SECUENCIA VIAJE] = ISNULL(TV.SecuenciaDestino,''),
    [TIENDA] = ISNULL(L.Nombre,''),
    [DETERMINANTE] = L.CodigoInterno,
    [IDMASTER] = TV.NroTransporte,
    [CEDIS ORIGEN] = ISNULL(TV.OrigenDescripcion,''),
    [FECHA SALIDA CEDIS] = '''' + dbo.fnu_ConvertirDatetimeToDDMMYYYY(TV.FHSalidaOrigen, 'FECHA_COMPLETA'),
    [RUT LINEA DE TRANSPORTE] = ISNULL(TV.RutTransportista,''),
    [RAZON SOCIAL LINEA DE TRANSPORTE] = ISNULL(TV.NombreTransportista,''),
    [PLACA REMOLQUE] = CASE WHEN ISNULL(TV.PatenteTrailer,'') = '' THEN ISNULL(TV.PatenteTracto,'') ELSE ISNULL(TV.PatenteTrailer,'') END,
    [TIPO CARGA] = ISNULL(TV.TipoViaje,''),
    [TIPO ALERTA] = ISNULL(TTA.TipoAlerta,''),
    [TOTAL OCURRENCIAS] = ISNULL(TTA.Ocurrencias,0),
    [TIEMPO PERMANENCIA EN MINUTOS] = ISNULL(TTA.TiempoPermanencia,0)
  FROM 
    TrazaViaje AS TV
    INNER MERGE JOIN Local AS L ON (TV.LocalDestino = L.CodigoInterno)
    LEFT JOIN @TablaTotalAlertas AS TTA ON (L.CodigoInterno = TTA.LocalDestino)
  WHERE
      TV.EstadoViaje = 'RUTA'
  AND TV.NroTransporte = @NroTransporte
  ORDER BY
    [SECUENCIA VIAJE], [TIPO ALERTA]

  -------------------------------------------------------------------------
  -- TABLA 1: obtiene datos del encargado del local
  -------------------------------------------------------------------------
  SELECT
    NombreLocal = L.Nombre,
    Cargo = ISNULL(P.Nombre,''),
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Telefono = ISNULL(U.Telefono,''),
    Email = ISNULL(U.Email,'')
  FROM
    Local AS L
    INNER JOIN LocalesPorUsuario AS LPU ON (L.Id = LPU.IdLocal)
    INNER JOIN Usuario AS U ON (LPU.IdUsuario = U.Id)
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      L.CodigoInterno = @LocalDestino
  AND P.Llave IN ('GL','JR')


END




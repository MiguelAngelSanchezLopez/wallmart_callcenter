﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerAlertasSinAtender')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerAlertasSinAtender'
  DROP  Procedure  dbo.spu_Alerta_ObtenerAlertasSinAtender
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerAlertasSinAtender'
GO
CREATE Procedure [dbo].[spu_Alerta_ObtenerAlertasSinAtender]
/******************************************************************************
**    Descripcion  : obtiene listado alertas sin atender
**    Por          : HML, 09/07/2014
*******************************************************************************/
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @SIN_ASIGNAR AS VARCHAR(255), @minutosEsperaParaAtencion INT
  SET @SIN_ASIGNAR = 'Sin Asignar'
  SET @minutosEsperaParaAtencion = 0

  ----------------------------------------------------------
  -- PASO 3: DEVUELVE TODOS LOS REGISTROS FORMATEADOS
  ----------------------------------------------------------
  SELECT
    IdAlerta = T.IdAlerta,
    NombreFormato = T.NombreFormato,
    NombreAlerta = T.NombreAlerta,
    NroTransporte = T.NroTransporte,
    Fecha = T.Fecha,
    AtendidoPor = T.AtendidoPor,
    Prioridad = T.Prioridad,
    OrdenPrioridad = T.OrdenPrioridad,
    Clasificacion = T.Clasificacion,
    OrdenAparicion = T.OrdenAparicion,
    DiferenciaMinutos = T.DiferenciaMinutos
  FROM (
        ----------------------------------------------------------
        -- PASO 2: FORMATEA LA FECHA DE LA ALERTA Y CALCULA LA DIFERENCIA EN MINUTOS DE ATENCION
        ----------------------------------------------------------
        SELECT
          IdAlerta = T.IdAlerta,
          NombreFormato = T.NombreFormato,
          NombreAlerta = T.NombreAlerta,
          NroTransporte = T.NroTransporte,
          Fecha = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.Fecha, 'FECHA_COMPLETA'),
          AtendidoPor = T.AtendidoPor,
          Prioridad = T.Prioridad,
          OrdenPrioridad = T.OrdenPrioridad,
          Clasificacion = T.Clasificacion,
          OrdenAparicion = T.OrdenAparicion,
          DiferenciaMinutos = CASE WHEN T.Asignada = 1 THEN
                                DATEDIFF(mi, T.FechaAsignacion, dbo.fnu_GETDATE())
                              ELSE
              	                DATEDIFF(mi, T.Fecha, dbo.fnu_GETDATE())
                              END
        FROM (
              ----------------------------------------------------------
              -- PASO 1: OBTIENE INFORMACION DE LAS ALERTAS QUE ESTAN EN COLA DE ATENCION
              ----------------------------------------------------------
              SELECT
                IdAlerta = AECA.IdAlerta,
                NombreFormato = F.Nombre,
                NombreAlerta = AECA.NombreAlerta,
                NroTransporte = AECA.NroTransporte,
                Fecha = AECA.FechaHoraCreacion,
                AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
                Prioridad = AECA.Prioridad,
                Clasificacion = CASE WHEN AECA.Asignada = 1 THEN 'SIENDO ATENDIDA' ELSE 'EN COLA DE ATENCION' END,
                OrdenAparicion = CASE WHEN AECA.Asignada = 1 THEN 1 ELSE 2 END,
                Asignada = ISNULL(AECA.Asignada,0),
                FechaAsignacion = AECA.FechaAsignacion,
                OrdenPrioridad = AECA.OrdenPrioridad
              FROM
                AlertaEnColaAtencion AS AECA
                INNER JOIN Alerta AS A ON (AECA.IdAlerta = A.Id)
                INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
                INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
                LEFT JOIN Usuario AS U ON (AECA.IdUsuarioAsignada = U.Id)        	
        ) AS T  	
  ) AS T
  WHERE
    T.DiferenciaMinutos >= @minutosEsperaParaAtencion
  ORDER BY
    OrdenAparicion, OrdenPrioridad, DiferenciaMinutos DESC

END

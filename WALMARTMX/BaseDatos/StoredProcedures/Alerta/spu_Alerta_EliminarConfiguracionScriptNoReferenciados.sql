﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_EliminarConfiguracionScriptNoReferenciados')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_EliminarConfiguracionScriptNoReferenciados'
  DROP  Procedure  dbo.spu_Alerta_EliminarConfiguracionScriptNoReferenciados
END

GO

PRINT  'Creating Procedure spu_Alerta_EliminarConfiguracionScriptNoReferenciados'
GO
CREATE Procedure dbo.spu_Alerta_EliminarConfiguracionScriptNoReferenciados
/******************************************************************************
**  Descripcion  : elimina los script que no estan referenciados
**  Fecha        : 06/08/2014 
*******************************************************************************/
@Status AS INT OUTPUT,
@IdEscalamientoPorAlertaGrupoContacto AS INT,
@Listado AS VARCHAR(8000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM Script WHERE IdEscalamientoPorAlertaGrupoContacto = @IdEscalamientoPorAlertaGrupoContacto AND Id NOT IN (SELECT CONVERT(NUMERIC,valor) FROM dbo.fnu_InsertaListaTabla(@Listado))

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    -- retorna que todo estuvo bien
    SET @Status = 1

  COMMIT
END
GO
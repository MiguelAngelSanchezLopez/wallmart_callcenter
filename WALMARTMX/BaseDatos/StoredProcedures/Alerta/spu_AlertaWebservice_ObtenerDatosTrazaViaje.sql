﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_ObtenerDatosTrazaViaje')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_ObtenerDatosTrazaViaje'
  DROP  Procedure  dbo.spu_AlertaWebservice_ObtenerDatosTrazaViaje
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_ObtenerDatosTrazaViaje'
GO

CREATE PROCEDURE [dbo].[spu_AlertaWebservice_ObtenerDatosTrazaViaje]
/******************************************************************************
**    Descripcion  : obtiene datos especiales de la trazaviaje
**    Autor        : DG
**    Fecha        : 02/02/2018
*******************************************************************************/
@NroTransporte AS BIGINT,
@IdEmbarque AS BIGINT
AS
BEGIN

	select 
		TipoViaje = ISNULL(TipoViaje, '') 
		,Cedis = isnull(OrigenDescripcion,'')
		,NombreDeterminante = case when CHARINDEX('Det.',isnull(DestinoDescripcion,'')) > 0 then DestinoDescripcion else 'Det. ' + cast(LocalDestino as varchar(100)) + ', ' + isnull(DestinoDescripcion,'') end 
	from TrazaViaje
	where 
		NroTransporte = @NroTransporte
		and IdEmbarque = @IdEmbarque
END


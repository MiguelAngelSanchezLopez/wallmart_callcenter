﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarAlertaEnColaAtencion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarAlertaEnColaAtencion'
  DROP  Procedure  dbo.spu_Alerta_GrabarAlertaEnColaAtencion
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarAlertaEnColaAtencion'
GO
CREATE Procedure dbo.spu_Alerta_GrabarAlertaEnColaAtencion
/******************************************************************************
**    Descripcion  : graba la alerta en la cola de atencion
**    Autor        : VSR
**    Fecha        : 06/10/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlerta AS INT,
@IdAlertaHija AS INT,
@NroTransporte AS BIGINT,
@IdEmbarque AS BIGINT,
@NombreAlerta AS VARCHAR(255),
@LocalDestino AS INT,
@IdFormato AS INT,
@Prioridad AS VARCHAR(255),
@OrdenPrioridad AS INT,
@FechaHoraCreacion AS DATETIME,
@FechaHoraCreacionDMA AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    DECLARE @totalRegistros INT
    SET @totalRegistros = (SELECT COUNT(IdAlerta) FROM AlertaEnColaAtencion WHERE IdAlerta = @IdAlerta AND IdAlertaHija = @IdAlertaHija)

    IF (@totalRegistros = 0 ) BEGIN
      INSERT INTO AlertaEnColaAtencion
      (
	      IdAlerta,
	      IdAlertaHija,
	      NroTransporte,
        IdEmbarque,
	      NombreAlerta,
        LocalDestino,
        IdFormato,
	      Prioridad,
	      OrdenPrioridad,
	      FechaHoraCreacion,
	      FechaHoraCreacionDMA,
	      Asignada
      )
      VALUES
      (
	      @IdAlerta,
	      @IdAlertaHija,
	      @NroTransporte,
        @IdEmbarque,
	      @NombreAlerta,
        @LocalDestino,
        @IdFormato,
	      @Prioridad,
	      @OrdenPrioridad,
	      dbo.fnu_GETDATE(),
	      dbo.fnu_ConvertirDatetimeToDDMMYYYY(dbo.fnu_GETDATE(),'FECHA_COMPLETA'),
	      0
      )

    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

    SET @Status = 1

  COMMIT
END
GO 
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarHistorialAlertaSMS')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarHistorialAlertaSMS'
  DROP  Procedure  dbo.spu_Alerta_GrabarHistorialAlertaSMS
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarHistorialAlertaSMS'
GO
CREATE Procedure dbo.spu_Alerta_GrabarHistorialAlertaSMS
/******************************************************************************
**    Descripcion  : graba los datos de la alerta que se envia por SMS
**    Autor        : VSR
**    Fecha        : 29/12/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@DescripcionAlerta AS VARCHAR(255),
@NroTransporte AS INT,
@LocalDestino AS INT,
@Telefono AS VARCHAR(255),
@EnviadoA AS VARCHAR(255),
@Cargo AS VARCHAR(255),
@GrupoAlerta AS VARCHAR(255),
@Ocurrencia AS INT,
@SMSFolio AS VARCHAR(255),
@SMSCodigo AS VARCHAR(255),
@SMSMensaje AS VARCHAR(255),
@EnviadoDesde AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    INSERT INTO HistorialAlertaSMS
    (
      NroTransporte,
	    DescripcionAlerta,
	    LocalDestino,
	    FechaEnvio,
	    Telefono,
	    EnviadoA,
	    Cargo,
      GrupoAlerta,
      Ocurrencia,
	    SMSFolio,
	    SMSCodigo,
	    SMSMensaje,
      EnviadoDesde,
      EnvioSMSFinalizado
    )
    VALUES
    (
	    @NroTransporte,
	    @DescripcionAlerta,
	    @LocalDestino,
	    dbo.fnu_GETDATE(),
	    @Telefono,
	    @EnviadoA,
	    @Cargo,
      @GrupoAlerta,
      @Ocurrencia,
	    @SMSFolio,
	    @SMSCodigo,
	    @SMSMensaje,
      @EnviadoDesde,
      0
    )

    SET @Status = SCOPE_IDENTITY()
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 
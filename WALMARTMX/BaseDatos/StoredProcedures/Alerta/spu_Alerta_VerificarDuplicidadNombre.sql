﻿  IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_VerificarDuplicidadNombre')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_VerificarDuplicidadNombre'
  DROP  Procedure  dbo.spu_Alerta_VerificarDuplicidadNombre
END

GO

PRINT  'Creating Procedure spu_Alerta_VerificarDuplicidadNombre'
GO
CREATE Procedure dbo.spu_Alerta_VerificarDuplicidadNombre
/******************************************************************************
**  Descripcion  : valida que el nombre de la alerta sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 30/07/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlerta AS INT,
@Valor AS VARCHAR(1028),
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdAlerta IS NULL OR @IdAlerta = 0) SET @IdAlerta = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdAlerta = -1) BEGIN
    IF( EXISTS(
                SELECT
                  AC.IdAlertaDefinicionPorFormato
                FROM
                  AlertaConfiguracion AS AC
                  INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
                WHERE
                    AC.IdAlertaDefinicionPorFormato = @Valor
                AND ADPF.IdFormato = @IdFormato
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  AC.IdAlertaDefinicionPorFormato
                FROM
                  AlertaConfiguracion AS AC
                  INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
                WHERE
                    AC.IdAlertaDefinicionPorFormato = @Valor
                AND ADPF.IdFormato = @IdFormato
                AND AC.Id <> @IdAlerta
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO       
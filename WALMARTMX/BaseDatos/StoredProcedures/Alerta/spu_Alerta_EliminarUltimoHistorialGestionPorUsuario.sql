﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_EliminarUltimoHistorialGestionPorUsuario')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_EliminarUltimoHistorialGestionPorUsuario'
  DROP  Procedure  dbo.spu_Alerta_EliminarUltimoHistorialGestionPorUsuario
END

GO

PRINT  'Creating Procedure spu_Alerta_EliminarUltimoHistorialGestionPorUsuario'
GO
CREATE Procedure dbo.spu_Alerta_EliminarUltimoHistorialGestionPorUsuario
/******************************************************************************
**  Descripcion  : elimina la ultima gestion del usuario cuando esta en estado auxiliar
**  Fecha        : VSR, 31/07/2015
*******************************************************************************/
@Status AS INT OUTPUT,
@IdUsuario AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM CallCenterHistorialAlertaGestion
    WHERE 
        IdUsuarioCreacion = @IdUsuario
    AND Id IN (
      SELECT TOP 2 Id FROM CallCenterHistorialAlertaGestion WHERE IdUsuarioCreacion = @IdUsuario ORDER BY Id DESC
    )

    -- retorna que todo estuvo bien
    SET @Status = 1

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarEstadoAlertaRoja')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarEstadoAlertaRoja'
  DROP  Procedure  dbo.spu_Alerta_GrabarEstadoAlertaRoja
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarEstadoAlertaRoja'
GO
CREATE Procedure dbo.spu_Alerta_GrabarEstadoAlertaRoja
/******************************************************************************
**    Descripcion  : graba el estado de la alerta roja
**    Autor        : VSR
**    Fecha        : 15/12/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdAlertaRojaEstadoActual AS INT,
@IdUsuario AS INT,
@NroTransporte AS INT,
@Estado AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    IF (@IdAlertaRojaEstadoActual = -1) BEGIN
      INSERT INTO AlertaRojaEstadoActual
      (
	      NroTransporte,
	      Estado,
	      IdUsuarioModificacion,
	      FechaModificacion
      )
      VALUES
      (
	      @NroTransporte,
	      @Estado,
	      @IdUsuario,
	      dbo.fnu_GETDATE()
      )

      SET @Status = SCOPE_IDENTITY()
      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END
    END ELSE BEGIN
      UPDATE AlertaRojaEstadoActual
      SET
	      NroTransporte = @NroTransporte,
	      Estado = @Estado,
	      IdUsuarioModificacion = @IdUsuario,
	      FechaModificacion = dbo.fnu_GETDATE()
      WHERE
        Id = @IdAlertaRojaEstadoActual

      SET @Status = @IdAlertaRojaEstadoActual
      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

    END

    ------------------------------------------------------------
    -- graba el historial
    ------------------------------------------------------------
    INSERT INTO AlertaRojaHistorialEstado
    (
	    NroTransporte,
	    Estado,
	    IdUsuarioCreacion,
	    FechaCreacion
    )
    VALUES
    (
	    @NroTransporte,
	    @Estado,
	    @IdUsuario,
	    dbo.fnu_GETDATE()
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = -200
      RETURN
    END

  COMMIT
END
GO 
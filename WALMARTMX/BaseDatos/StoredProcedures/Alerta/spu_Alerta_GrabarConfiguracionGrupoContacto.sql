﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_GrabarConfiguracionGrupoContacto')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_GrabarConfiguracionGrupoContacto'
  DROP  Procedure  dbo.spu_Alerta_GrabarConfiguracionGrupoContacto
END

GO

PRINT  'Creating Procedure spu_Alerta_GrabarConfiguracionGrupoContacto'
GO
CREATE Procedure dbo.spu_Alerta_GrabarConfiguracionGrupoContacto
/******************************************************************************
**  Descripcion  : graba los datos del grupo contacto
**  Fecha        : 06/08/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdEscalamientoPorAlertaGrupoContacto AS INT,
@IdEscalamientoPorAlerta AS INT,
@Nombre AS VARCHAR(255),
@Orden AS INT,
@MostrarCuandoEscalamientoAnteriorNoContesta AS INT,
@TieneDependenciaGrupoEscalamientoAnterior AS INT,
@IdGrupoContactoEscalamientoAnterior AS INT,
@MostrarSiempre AS INT,
@IdGrupoContactoPadre AS INT,
@NotificarPorEmail AS INT,
@HoraAtencionInicio AS VARCHAR(255),
@HoraAtencionTermino AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    IF (@IdGrupoContactoEscalamientoAnterior = -1) SET @IdGrupoContactoEscalamientoAnterior = NULL
    IF (@IdGrupoContactoPadre = -1) SET @IdGrupoContactoPadre = NULL

    -- ingresa nuevo registro
    IF (@IdEscalamientoPorAlertaGrupoContacto = -1) BEGIN
      INSERT INTO EscalamientoPorAlertaGrupoContacto
      (
	      IdEscalamientoPorAlerta,
	      Nombre,
	      Orden,
        MostrarCuandoEscalamientoAnteriorNoContesta,
        TieneDependenciaGrupoEscalamientoAnterior,
        IdGrupoContactoEscalamientoAnterior,
        MostrarSiempre,
        IdGrupoContactoPadre,
        Eliminado,
        NotificarPorEmail,
        HoraAtencionInicio,
        HoraAtencionTermino
      )
      VALUES
      (
	      @IdEscalamientoPorAlerta,
	      @Nombre,
	      @Orden,
        @MostrarCuandoEscalamientoAnteriorNoContesta,
        @TieneDependenciaGrupoEscalamientoAnterior,
        @IdGrupoContactoEscalamientoAnterior,
        @MostrarSiempre,
        @IdGrupoContactoPadre,
        0,
        @NotificarPorEmail,
        @HoraAtencionInicio,
        @HoraAtencionTermino
      )

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = SCOPE_IDENTITY()
    END ELSE BEGIN
      UPDATE EscalamientoPorAlertaGrupoContacto
      SET
	      Nombre = @Nombre,
	      Orden = @Orden,
        MostrarCuandoEscalamientoAnteriorNoContesta = @MostrarCuandoEscalamientoAnteriorNoContesta,
        TieneDependenciaGrupoEscalamientoAnterior = @TieneDependenciaGrupoEscalamientoAnterior,
        IdGrupoContactoEscalamientoAnterior = @IdGrupoContactoEscalamientoAnterior,
        MostrarSiempre = @MostrarSiempre,
        IdGrupoContactoPadre = @IdGrupoContactoPadre,
        NotificarPorEmail = @NotificarPorEmail,
        HoraAtencionInicio = @HoraAtencionInicio,
        HoraAtencionTermino = @HoraAtencionTermino
      WHERE
        Id = @IdEscalamientoPorAlertaGrupoContacto

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = -200
        RETURN
      END

      SET @Status = @IdEscalamientoPorAlertaGrupoContacto

    END

  COMMIT
END
GO        
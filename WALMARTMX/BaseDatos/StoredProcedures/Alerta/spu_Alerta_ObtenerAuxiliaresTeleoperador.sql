﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerAuxiliaresTeleoperador')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerAuxiliaresTeleoperador'
  DROP  Procedure  dbo.spu_Alerta_ObtenerAuxiliaresTeleoperador
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerAuxiliaresTeleoperador'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerAuxiliaresTeleoperador
/******************************************************************************
**    Descripcion  : obtiene auxiliares utilizados por el Teleoperador
**    Autor        : VSR
**    Fecha        : 03/11/2014
*******************************************************************************/
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Nombre = ISNULL(TG.Nombre,''),
    Valor =  ISNULL(TG.Valor,''),
    Accion =  ISNULL(TG.Extra1,'')
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'AuxiliarTeleoperador'
  AND TG.Activo = 1
  ORDER BY
    Nombre

END




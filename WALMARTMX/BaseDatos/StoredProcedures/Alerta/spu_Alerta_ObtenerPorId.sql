﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerPorId'
  DROP  Procedure  dbo.spu_Alerta_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerPorId'
GO

CREATE Procedure [dbo].[spu_Alerta_ObtenerPorId] (  
@Id as INT  
) AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    Alerta
  WHERE   
    Id = @Id  
  
END  
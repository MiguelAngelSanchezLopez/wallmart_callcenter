﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerListadoConfiguracion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerListadoConfiguracion'
  DROP  Procedure  dbo.spu_Alerta_ObtenerListadoConfiguracion
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerListadoConfiguracion'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerListadoConfiguracion
/******************************************************************************
**    Descripcion  : obtiene el listado de alertas que se deben configurar
**    Por          : VSR, 30/07/2014
*******************************************************************************/
@IdUsuario  AS INT,
@Nombre AS VARCHAR(255),
@Prioridad AS VARCHAR(255),
@SoloActivos AS INT,
@TipoAlerta AS VARCHAR(255),
@IdFormato AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
	  IdAlertaConfiguracion = AC.Id,
	  NombreAlerta = ISNULL(AD.Nombre,''),
	  Prioridad = ISNULL(AC.Prioridad,''),
    Activo = CASE WHEN ISNULL(AC.Activo,0) = 1 THEN 'SI' ELSE 'NO' END,
    TipoAlerta = ISNULL(AC.TipoAlerta,''),
    NombreFormato = ISNULL(F.Nombre,''),
    TieneAlertasAsociadas = 0
    /*
    TieneAlertasAsociadas = CASE WHEN (
                                        SELECT
                                          COUNT(auxA.Id)
                                        FROM
                                          CallCenterAlerta AS auxCCA
                                          INNER JOIN Alerta AS auxA ON (auxCCA.IdAlerta = auxA.Id)
                                          INNER JOIN Local AS auxL ON (auxA.LocalDestino = auxL.CodigoInterno)
                                          INNER JOIN AlertaDefinicionPorFormato AS auxADPF ON (auxADPF.IdFormato = auxL.IdFormato)
                                          INNER JOIN AlertaDefinicion AS auxAD ON (auxADPF.IdAlertaDefinicion = auxAD.Id AND auxAD.Nombre = auxA.DescripcionAlerta)
                                        WHERE
                                            auxAD.Nombre = AD.Nombre
                                        AND auxADPF.IdFormato = F.Id
                            ) > 0 THEN '1' ELSE '0' END
    */
  FROM
    AlertaConfiguracion AS AC
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
    INNER JOIN Formato AS F ON (ADPF.IdFormato = F.Id)
  WHERE
      (@Nombre = '-1' OR AD.Nombre LIKE '%'+ @Nombre +'%')
  AND (@Prioridad = '-1' OR AC.Prioridad LIKE '%'+ @Prioridad +'%')
  AND (@SoloActivos = '-1' OR AC.Activo = @SoloActivos)
  AND (@TipoAlerta = '-1' OR AC.TipoAlerta = @TipoAlerta)
  AND (@IdFormato = -1 OR F.Id = @IdFormato)
  ORDER BY
    IdAlertaConfiguracion DESC

END



  
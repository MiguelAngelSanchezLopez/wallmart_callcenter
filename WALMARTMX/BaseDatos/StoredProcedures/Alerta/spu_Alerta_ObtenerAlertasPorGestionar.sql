﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerAlertasPorGestionar')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerAlertasPorGestionar'
  DROP  Procedure  dbo.spu_Alerta_ObtenerAlertasPorGestionar
END
GO

PRINT  'Creating Procedure spu_Alerta_ObtenerAlertasPorGestionar'
GO
CREATE Procedure [dbo].[spu_Alerta_ObtenerAlertasPorGestionar]  
/******************************************************************************  
**    Descripcion  : obtiene listado alertas por gestionar  
**    Por          : HML, 17/07/2014  
*******************************************************************************/  
AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
      
  ----------------------------------------------------------------  
  -- obtiene informacion de las alertas por cerrar  
  ----------------------------------------------------------------  
  DECLARE @TablaAlerta AS TABLE(IdAlerta INT, NombreAlerta VARCHAR(255), NroTransporte VARCHAR(255), Prioridad VARCHAR(255), OrdenPrioridad INT, AtendidoPor VARCHAR(255), NroEscalamientoActual INT,   
                                TotalEscalamientos INT, FrecuenciaRepeticionMinutos INT, DiferenciaMinutos INT, UltimaGestionFecha VARCHAR(255), UltimaGestionAccion VARCHAR(255),  
                                NoContesta INT, LocalDestino  VARCHAR(255), LatTracto VARCHAR(255), LonTracto VARCHAR(255), TipoAlerta  VARCHAR(255),ClasificacionAlertaSist VARCHAR(10),MontoDiscrepancia NUMERIC)  
                                
  INSERT INTO @TablaAlerta(IdAlerta,NombreAlerta,NroTransporte,Prioridad, OrdenPrioridad, AtendidoPor,NroEscalamientoActual,TotalEscalamientos,  
                           FrecuenciaRepeticionMinutos,DiferenciaMinutos,UltimaGestionFecha,UltimaGestionAccion, NoContesta,  
                    LocalDestino,LatTracto,LonTracto,TipoAlerta,ClasificacionAlertaSist,MontoDiscrepancia)  
  SELECT  
   IdAlerta = A.Id,  
    NombreAlerta = A.DescripcionAlerta,  
    NroTransporte = A.NroTransporte,  
    Prioridad = AC.Prioridad,  
    OrdenPrioridad = CONVERT(INT,TG.Extra1),  
    AtendidoPor = AA.UltimaGestionAtendidoPor,  
    NroEscalamientoActual = ISNULL((  
                              SELECT TOP 1  
                                EPA.Orden  
                              FROM  
                                CallCenterHistorialEscalamiento AS HE  
                                INNER JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)  
                              WHERE  
                                HE.IdAlerta = A.Id  
                              ORDER BY  
                                EPA.Orden DESC  
                            ),0),  
    TotalEscalamientos = (  
                            SELECT  
                              COUNT(EPA.Id)  
                            FROM  
                              AlertaConfiguracion AS AC  
                              INNER JOIN EscalamientoPorAlerta AS EPA ON (AC.Id = EPA.IdAlertaConfiguracion)  
                              INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)  
                              INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)  
                            WHERE  
                                AD.Nombre = A.DescripcionAlerta  
                            AND ADPF.IdFormato = AA.IdFormato  
                            AND AC.Activo = 1  
                            AND EPA.Eliminado = 0  
                         ),  
    FrecuenciaRepeticionMinutos = AC.FrecuenciaRepeticionMinutos,  
    DiferenciaMinutos = CASE WHEN ISNULL(DATEDIFF(mi, AA.UltimaGestionFechaCreacion, dbo.fnu_GETDATE()),-1) > 99999 THEN 99999 ELSE ISNULL(DATEDIFF(mi, AA.UltimaGestionFechaCreacion, dbo.fnu_GETDATE()),-1) END, -- se coloca como tope -99999 solo por si en el futuro ocasiona problema de desbordamiento  
    UltimaGestionFecha = dbo.fnu_ConvertirDatetimeToDDMMYYYY(A.FechaHoraCreacion, 'FECHA_COMPLETA'),  
    UltimaGestionAccion = ISNULL(AA.UltimaGestionAccion,''),  
    NoContesta = NC.NoContesta,  
   LocalDestino = A.LocalDestino,  
   LatTracto = A.LatTracto,  
   LonTracto = A.LonTracto,  
   TipoAlerta = A.TipoAlerta,  
   ClasificacionAlertaSist = CASE WHEN arb.idAlerta IS NULL THEN '' ELSE 'Roja' END,  
   MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte AND D.LocalDestino = a.LocalDestino),0)  
  FROM  
    CallCenterAlerta AS CCA  
    INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)  
    INNER MERGE JOIN vwu_AlertaAcumulada AS AA ON (CCA.IdAlertaPadre = AA.IdAlertaPadre AND CCA.IdAlerta = AA.IdAlertaHija)  
    INNER MERGE JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)  
    INNER MERGE JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)  
    INNER MERGE JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)  
    INNER MERGE JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)  
    INNER MERGE JOIN TipoGeneral AS TG ON (AC.Prioridad = TG.Valor AND TG.Tipo = 'Prioridad')  
   LEFT MERGE JOIN AlertaRojaBatch arb ON (arb.idAlerta = A.id AND arb.NroTransporte = A.NroTransporte AND arb.LocalDestino = A.LocalDestino AND arb.LatTracto = A.LatTracto AND arb.LonTracto = A.LonTracto AND arb.TipoAlerta = A.TipoAlerta)  
    LEFT MERGE JOIN vwu_NoContesta AS NC ON (A.Id = NC.IdAlerta)  
  WHERE  
     AA.Cerrado = 0  
 AND AC.Activo = 1  
  AND CCA.Desactivada = 0  
  AND A.FechaHoraCreacion>='20151001'  
    
  
     
   ----------------------------------------------------------------------  
   -- obtiene todas las alertas que llegaron a su ultimo escalamiento sin contactabilidad  
   ----------------------------------------------------------------------  
    SELECT TOP 2000  
      T.*  
    FROM (  
          SELECT   
           ta.*  
           ,Clasificacion = 'SIN CONTACTABILIDAD'  
           ,OrdenClasificacion = 1   
          FROM @TablaAlerta ta  
          WHERE   
           NoContesta = 1  
            AND (MontoDiscrepancia > 0)  
     
           ----------------------------------------------------------------------  
           -- UNION: obtiene todas las alertas que llegaron a su ultimo escalamiento  
           ----------------------------------------------------------------------  
           UNION  
          SELECT   
           ta.*  
           ,Clasificacion = 'POR CERRAR'  
           ,OrdenClasificacion = 2   
          FROM @TablaAlerta ta  
          WHERE   
           NroEscalamientoActual = TotalEscalamientos   
           AND NoContesta = 0  
            AND (MontoDiscrepancia > 0)  
  
           ----------------------------------------------------------------------  
           -- UNION: obtiene todas las alertas que estan pendientes por cerrar del supervisor  
           ----------------------------------------------------------------------  
           UNION  
          SELECT   
           ta.*  
           ,Clasificacion = 'POR CERRAR'  
           ,OrdenClasificacion = 3  
          FROM @TablaAlerta ta  
          WHERE   
           UltimaGestionAccion = 'SUPERVISOR_ALERTA_CERRADO_PENDIENTE'   
           AND NroEscalamientoActual <> TotalEscalamientos   
           AND NoContesta = 0  
            AND (MontoDiscrepancia > 0)  
  
           ----------------------------------------------------------------------  
           -- UNION: obtiene todas las alertas que han sobrepasado el tiempo de frecuencia de repeticion y no se han vuelto a generar  
           ----------------------------------------------------------------------  
          UNION  
          SELECT   
           ta.*  
           ,Clasificacion = 'POR CERRAR'  
           ,OrdenClasificacion = 4   
          FROM @TablaAlerta ta  
          WHERE  
           NroEscalamientoActual > 0   
           AND UltimaGestionAccion <> 'SUPERVISOR_ALERTA_CERRADO_PENDIENTE' AND NroEscalamientoActual <> TotalEscalamientos AND NoContesta = 0  
            AND (MontoDiscrepancia > 0)  
    ) AS T   
  ORDER BY  
   T.OrdenClasificacion,  
   T.OrdenPrioridad  
END  

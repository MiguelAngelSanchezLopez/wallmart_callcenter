﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerDetalleConfiguracion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerDetalleConfiguracion'
  DROP  Procedure  dbo.spu_Alerta_ObtenerDetalleConfiguracion
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerDetalleConfiguracion'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerDetalleConfiguracion
/******************************************************************************
**    Descripcion  : obtiene los datos del detalle de la configuración de la alerta
**    Por          : VSR, 30/07/2014
*******************************************************************************/
@IdAlertaConfiguracion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  -------------------------------------------------
  -- obtiene formato al que pertenece la alerta
  -------------------------------------------------
  DECLARE @IdFormato INT
  SELECT
    @IdFormato = ADPF.IdFormato
  FROM
    AlertaConfiguracion AS AC
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
  WHERE
    AC.Id = @IdAlertaConfiguracion

  IF (@IdFormato IS NULL) SET @IdFormato = -1

  -------------------------------------------------
  -- obtiene todos los perfiles relacionados al formato y los graba en forma temporal
  -------------------------------------------------
  DECLARE @TablaPerfiles AS TABLE(IdPerfil INT, NombrePerfil VARCHAR(255), LlavePerfil VARCHAR(255), Tipo VARCHAR(255))

  INSERT INTO @TablaPerfiles(IdPerfil, NombrePerfil, LlavePerfil, Tipo)
  SELECT
    IdPerfil = PPF.IdPerfil,
    NombrePerfil = ISNULL(PPF.NombrePerfil,''),
    LlavePerfil = ISNULL(PPF.LlavePerfil,''),
    Tipo = ISNULL(PPF.Tipo,'')
  FROM
    vwu_PerfilesPorFormato AS PPF
  WHERE
    PPF.IdFormato = @IdFormato


  -----------------------------------------------------------------------------
  -- TABLA 0: Detalle
  -----------------------------------------------------------------------------
  SELECT
	  IdAlertaConfiguracion = AC.Id,
    IdFormato = ADPF.IdFormato,
    IdAlertaDefinicionPorFormato = AC.IdAlertaDefinicionPorFormato,
	  Prioridad = ISNULL(AC.Prioridad,''),
	  FrecuenciaRepeticionMinutos = ISNULL(AC.FrecuenciaRepeticionMinutos,-1),
	  TipoAlerta = ISNULL(AC.TipoAlerta,''),
	  Activo = CONVERT(INT,ISNULL(AC.Activo,0)),
    FechaInicio = CASE WHEN AC.FechaInicio IS NULL THEN '' ELSE dbo.fnu_ConvertirDatetimeToDDMMYYYY(AC.FechaInicio,'SOLO_FECHA') END,
    HoraInicio = CASE WHEN AC.FechaInicio IS NULL THEN '' ELSE dbo.fnu_ConvertirDatetimeToDDMMYYYY(AC.FechaInicio,'SOLO_HORA') END,
    TieneAlertasAsociadas = CASE WHEN (
                                        SELECT
                                          COUNT(auxA.Id)
                                        FROM
                                          CallCenterAlerta AS auxCCA
                                          INNER JOIN Alerta AS auxA ON (auxCCA.IdAlerta = auxA.Id)
                                          INNER JOIN Local AS auxL ON (auxA.LocalDestino = auxL.CodigoInterno)
                                          INNER JOIN AlertaDefinicionPorFormato AS auxADPF ON (auxADPF.IdFormato = auxL.IdFormato)
                                          INNER JOIN AlertaDefinicion AS auxAD ON (auxADPF.IdAlertaDefinicion = auxAD.Id AND auxAD.Nombre = auxA.DescripcionAlerta)
                                        WHERE
                                            auxAD.Nombre = AD.Nombre
                                        AND auxADPF.IdFormato = ADPF.IdFormato
                            ) > 0 THEN '1' ELSE '0' END

  FROM
    AlertaConfiguracion AS AC
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
  WHERE
    AC.Id = @IdAlertaConfiguracion
    
  -----------------------------------------------------------------------------
  -- TABLA 1: Escalamientos
  -----------------------------------------------------------------------------
  SELECT
    IdEscalamientoPorAlerta = EPA.Id,
    IdAlertaConfiguracion = EPA.IdAlertaConfiguracion,
    Orden = ISNULL(EPA.Orden,1),
    EmailCopia = ISNULL(EPA.EmailCopia,'')
  FROM
    EscalamientoPorAlerta AS EPA
  WHERE
      EPA.IdAlertaConfiguracion = @IdAlertaConfiguracion
  AND EPA.Eliminado = 0
  ORDER BY
    Orden
    
  -----------------------------------------------------------------------------
  -- TABLA 2: Grupo Contactos
  -----------------------------------------------------------------------------
  SELECT
  	IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id,
  	IdEscalamientoPorAlerta = EPAGC.IdEscalamientoPorAlerta,
   	NombreGrupo = ISNULL(EPAGC.Nombre,''),
  	OrdenGrupo = ISNULL(EPAGC.Orden,1),
  	MostrarCuandoEscalamientoAnteriorNoContesta = CONVERT(INT,ISNULL(EPAGC.MostrarCuandoEscalamientoAnteriorNoContesta,0)),
  	TieneDependenciaGrupoEscalamientoAnterior = CONVERT(INT,ISNULL(EPAGC.TieneDependenciaGrupoEscalamientoAnterior,0)),
  	IdGrupoContactoEscalamientoAnterior = ISNULL(EPAGC.IdGrupoContactoEscalamientoAnterior,-1),
    MostrarSiempre = CONVERT(INT,ISNULL(EPAGC.MostrarSiempre,0)),
  	IdGrupoContactoPadre = ISNULL(EPAGC.IdGrupoContactoPadre,-1),
    NotificarPorEmail = CONVERT(INT,ISNULL(EPAGC.NotificarPorEmail,0)),
    TipoGrupo = CASE WHEN TP.NombrePerfil IS NULL THEN 'Personalizado' ELSE TP.Tipo END,
    HoraAtencionInicio = ISNULL(EPAGC.HoraAtencionInicio,'0000'),
    HoraAtencionTermino = ISNULL(EPAGC.HoraAtencionTermino,'2359')
  FROM
  	EscalamientoPorAlertaGrupoContacto AS EPAGC
  	INNER JOIN EscalamientoPorAlerta AS EPA ON (EPAGC.IdEscalamientoPorAlerta = EPA.Id)
    LEFT JOIN @TablaPerfiles AS TP ON (EPAGC.Nombre = TP.NombrePerfil)
  WHERE
      EPA.IdAlertaConfiguracion = @IdAlertaConfiguracion
  AND EPAGC.Eliminado = 0
  ORDER BY
    ISNULL(EPA.Orden,1), OrdenGrupo
    
  -----------------------------------------------------------------------------
  -- TABLA 3: Usuarios Grupo Contactos
  -----------------------------------------------------------------------------
  SELECT
  	IdEscalamientoPorAlertaContacto = EPAC.Id,
  	IdEscalamientoPorAlertaGrupoContacto = EPAC.IdEscalamientoPorAlertaGrupoContacto,
    IdUsuario = EPAC.IdUsuario,
    NombreContacto = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Cargo = ISNULL(EPAC.Cargo, P.Nombre),
    OrdenContacto = ISNULL(EPAC.Orden,1)
  FROM
    EscalamientoPorAlertaContacto AS EPAC
  	INNER JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (EPAC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
  	INNER JOIN EscalamientoPorAlerta AS EPA ON (EPAGC.IdEscalamientoPorAlerta = EPA.Id)
  	INNER JOIN Usuario AS U ON (EPAC.IdUsuario = U.Id)
  	INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      EPA.IdAlertaConfiguracion = @IdAlertaConfiguracion
  AND EPAC.Eliminado = 0
  ORDER BY
    IdEscalamientoPorAlertaGrupoContacto, OrdenContacto

  -----------------------------------------------------------------------------
  -- TABLA 4: Script
  -----------------------------------------------------------------------------
  SELECT
  	IdScript = S.Id,
  	IdEscalamientoPorAlertaGrupoContacto = S.IdEscalamientoPorAlertaGrupoContacto,
  	Nombre = ISNULL(S.Nombre,''),
  	Descripcion = ISNULL(S.Descripcion,''),
  	Activo = ISNULL(CONVERT(INT, S.Activo),0)
  FROM
  	Script AS S
  	INNER JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (S.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
  	INNER JOIN EscalamientoPorAlerta AS EPA ON (EPAGC.IdEscalamientoPorAlerta = EPA.Id)
  WHERE
    EPA.IdAlertaConfiguracion = @IdAlertaConfiguracion
  ORDER BY
    IdEscalamientoPorAlertaGrupoContacto

END



   
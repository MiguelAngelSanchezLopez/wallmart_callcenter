﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerUltimaGestion')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerUltimaGestion'
  DROP  Procedure  dbo.spu_Alerta_ObtenerUltimaGestion
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerUltimaGestion'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerUltimaGestion
/******************************************************************************
**    Descripcion  : obtiene informacion de la ultima gestion de la alerta
**    Por          : VSR, 07/07/2014
*******************************************************************************/
@IdAlerta AS INT,
@IdUsuario AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  ------------------------------------------------
  -- busca la ultima gestion por el IdAlerta
  ------------------------------------------------
  IF (@IdAlerta <> -1) BEGIN
    SELECT TOP 1
      AUG.Accion
    FROM
      vwu_AlertaUltimaGestion AS AUG
    WHERE
      AUG.IdAlertaPadre = @IdAlerta
    ORDER BY
      AUG.FechaCreacion

  END ELSE BEGIN
  ------------------------------------------------
  -- busca la ultima gestion por el IdUsuario
  ------------------------------------------------
    SELECT TOP 1
      HAG.Accion
    FROM
      CallCenterHistorialAlertaGestion AS HAG
    WHERE
      HAG.IdUsuarioCreacion = @IdUsuario
    ORDER BY
      HAG.FechaCreacion DESC

  END

END



  
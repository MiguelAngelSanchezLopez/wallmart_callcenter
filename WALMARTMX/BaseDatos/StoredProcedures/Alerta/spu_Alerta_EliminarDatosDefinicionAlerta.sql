﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_EliminarDatosDefinicionAlerta')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_EliminarDatosDefinicionAlerta'
  DROP  Procedure  dbo.spu_Alerta_EliminarDatosDefinicionAlerta
END

GO

PRINT  'Creating Procedure spu_Alerta_EliminarDatosDefinicionAlerta'
GO
CREATE Procedure dbo.spu_Alerta_EliminarDatosDefinicionAlerta
/******************************************************************************
**  Descripcion  : elimina la definicion de alerta
**  Fecha        : VSR, 13/04/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdAlertaDefinicion AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM AlertaDefinicionPorFormato WHERE IdAlertaDefinicion = @IdAlertaDefinicion
    DELETE FROM AlertaDefinicion WHERE Id = @IdAlertaDefinicion

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO
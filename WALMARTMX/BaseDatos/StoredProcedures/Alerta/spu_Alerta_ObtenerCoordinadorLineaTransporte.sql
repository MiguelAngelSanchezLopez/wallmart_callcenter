﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Alerta_ObtenerCoordinadorLineaTransporte')
BEGIN
  PRINT  'Dropping Procedure spu_Alerta_ObtenerCoordinadorLineaTransporte'
  DROP  Procedure  dbo.spu_Alerta_ObtenerCoordinadorLineaTransporte
END

GO

PRINT  'Creating Procedure spu_Alerta_ObtenerCoordinadorLineaTransporte'
GO
CREATE Procedure dbo.spu_Alerta_ObtenerCoordinadorLineaTransporte
/******************************************************************************
**    Descripcion  : obtiene listado de coordinador linea transporte
**    Autor        : VSR
**    Fecha        : 09/02/2017
*******************************************************************************/
@NombreTransportista AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @IdUsuario INT, @IdUsuarioTransportista INT
  IF (@NombreTransportista = '') SET @NombreTransportista = '-1'

  -----------------------------------------------------------------------------
  -- obtiene idUsuarioTransportista
  -----------------------------------------------------------------------------
  SELECT
    @IdUsuarioTransportista = TH.IdUsuario
  FROM
    Track_Homoclave AS TH
  WHERE
    TH.Nomenclatura = @NombreTransportista

  -- si no encuentra el usuario entonces setea variable para obtener todos los usuarios
  IF (@IdUsuarioTransportista IS NULL) SET @IdUsuarioTransportista = -1

  -----------------------------------------------------------------------------
  -- busca el usuario por el nombre consultado
  -----------------------------------------------------------------------------
  IF (SELECT COUNT(IdUsuario) FROM UsuarioDatoExtra WHERE IdUsuarioTransportista = @IdUsuarioTransportista) > 0 BEGIN
    -----------------------------------------------------------------------------
    -- se mantiene la misma estructura de campos que tiene el select de la TABLA 3: Contactos Proximo Escalamiento
    -- del procedimiento spu_Alerta_ObtenerDetalle
    -----------------------------------------------------------------------------
    SELECT DISTINCT
      IdEscalamientoPorAlertaContacto = U.Id,
      NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
      Telefono = ISNULL((SELECT aux.Telefono FROM TelefonosPorUsuario AS aux WHERE aux.IdUsuario = U.Id AND REPLACE(LEFT(CONVERT(VARCHAR,dbo.fnu_GETDATE(),108),5),':','') BETWEEN aux.HoraInicio AND aux.HoraTermino), ISNULL(U.Telefono,'')),
      Email = ISNULL(U.Email,''),
      Cargo = ISNULL(P.Nombre,''),
      TipoGrupo = CASE WHEN PPF.NombrePerfil IS NULL THEN 'Personalizado' ELSE PPF.Tipo END
    FROM
      Usuario AS U
      INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
      INNER JOIN UsuarioDatoExtra AS UDE ON (U.Id = UDE.IdUsuario)
      LEFT JOIN vwu_PerfilesPorFormato AS PPF ON (P.Nombre = PPF.NombrePerfil AND P.Llave = PPF.LlavePerfil)
    WHERE
        U.Estado = 1
    AND P.Llave = 'CLT'
    AND UDE.IdUsuarioTransportista = @IdUsuarioTransportista
    ORDER BY
      NombreUsuario  
  END ELSE BEGIN
    -- obtiene el perfil COORDINADOR LINEA TRANSPORTE
    SET @IdUsuario = -1
    EXEC spu_Alerta_ObtenerUsuariosPorPerfil @IdUsuario, 'CLT'
  END

END




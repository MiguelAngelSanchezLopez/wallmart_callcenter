﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_PertenecePatente')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_PertenecePatente'
  DROP  Procedure  dbo.spu_Transportista_PertenecePatente
END

GO

PRINT  'Creating Procedure spu_Transportista_PertenecePatente'
GO
CREATE Procedure dbo.spu_Transportista_PertenecePatente
/******************************************************************************
**    Descripcion  : valida que la patente pertenezca al transportista
**    Por          : VSR, 25/08/2015
*******************************************************************************/
@Status AS INT OUTPUT, -- [Retorno: "1" -> Valido | "-1" -> No Valido]
@IdTransportista AS INT,
@Patente AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @TotalRegistros INT
  SET @TotalRegistros = (SELECT COUNT(Id) FROM Patente WHERE PlacaPatente = @Patente)

  ---------------------------------------------------
  -- 1) Valida si la patente existe en la tabla
  ---------------------------------------------------
  IF (@TotalRegistros = 0) BEGIN
    SET @Status = 1
  END ELSE BEGIN
    ---------------------------------------------------
    -- 2) Valida que la patente no pertenezca a otra empresa
    ---------------------------------------------------
    SET @TotalRegistros = (SELECT COUNT(Id) FROM Patente WHERE PlacaPatente = @Patente AND IdTransportista <> @IdTransportista)
  
    IF (@TotalRegistros > 0) BEGIN  
      SET @Status = -1 -- si encuentra registros en otra empresa entonces retorna que NO es valido
    END ELSE BEGIN
      SET @Status = 1
    END

  END
  
END
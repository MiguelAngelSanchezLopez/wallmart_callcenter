﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_ObtenerPatenteBloqueada')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_ObtenerPatenteBloqueada'
  DROP  Procedure  dbo.spu_Transportista_ObtenerPatenteBloqueada
END

GO

PRINT  'Creating Procedure spu_Transportista_ObtenerPatenteBloqueada'
GO
CREATE Procedure dbo.spu_Transportista_ObtenerPatenteBloqueada
/******************************************************************************
**    Descripcion  : Obtiene listado de patentes que han sido bloqueadas
**    Por          : DG, 23/06/2015
*******************************************************************************/
 @patente AS VARCHAR(6)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT MotivoBloqueo
  FROM Patente
  WHERE PlacaPatente = @patente
  AND (Bloqueado = 1)
  
END
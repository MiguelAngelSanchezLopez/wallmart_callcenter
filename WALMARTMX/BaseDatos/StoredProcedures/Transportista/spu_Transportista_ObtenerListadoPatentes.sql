﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_ObtenerListadoPatentes')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_ObtenerListadoPatentes'
  DROP  Procedure  dbo.spu_Transportista_ObtenerListadoPatentes
END

GO

PRINT  'Creating Procedure spu_Transportista_ObtenerListadoPatentes'
GO
CREATE Procedure dbo.spu_Transportista_ObtenerListadoPatentes
/******************************************************************************
**    Descripcion  : Obtiene listado de patentes asociadas al transportista
**    Por          : DG, 17/06/2015
*******************************************************************************/
 @IdTransportista AS INT
,@PlacaPatente AS VARCHAR(10) = '-1'
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT 
	Id
	,PlacaPatente
	,Marca
	,Tipo
  ,IdTransportista
  FROM Patente
  WHERE (idTransportista = @IdTransportista)
  AND (@PlacaPatente = '-1' OR  PlacaPatente LIKE '%' + @PlacaPatente + '%')
END
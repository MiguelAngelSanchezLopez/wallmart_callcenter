﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_ObtenerDetalleAlertaRojaTransportista')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_ObtenerDetalleAlertaRojaTransportista'
  DROP  Procedure  dbo.spu_Transportista_ObtenerDetalleAlertaRojaTransportista
END

GO

PRINT  'Creating Procedure spu_Transportista_ObtenerDetalleAlertaRojaTransportista'
GO
CREATE Procedure dbo.spu_Transportista_ObtenerDetalleAlertaRojaTransportista
/******************************************************************************    
**    Descripcion  : obtiene detalle de las alertas rojas del numero de viaje  
**    Por          : VSR, 12/12/2014    
*******************************************************************************/    
@Rut AS VARCHAR(255),    
@NroTransporte AS INT  
AS    
BEGIN    
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
  SET NOCOUNT ON    
    
  ---------------------------------------------------------------------------------------------  
  -- obtiene detalle de la alerta  
  DECLARE @TablaAlertaRoja AS TABLE(IdAlerta INT, NroTransporte INT, NombreAlerta VARCHAR(255), NombreConductor VARCHAR(255), NombreTransportista VARCHAR(255),  
                                    PatenteTracto VARCHAR(255), PatenteTrailer VARCHAR(255), AlertaMapa VARCHAR(255), LocalOrigenCodigo INT, LocalOrigenDescripcion VARCHAR(255),
                                    LocalDestinoCodigo INT, LocalDestinoDescripcion VARCHAR(255), TipoViaje VARCHAR(255), FechaHoraCreacion DATETIME, TipoAlerta VARCHAR(255),
                                    FechaGestion DATETIME, CategoriaAlerta VARCHAR(255), TipoObservacion VARCHAR(255), Observacion VARCHAR(4000), MontoDiscrepancia NUMERIC
                                   )  
  INSERT INTO @TablaAlertaRoja  
  (  
    IdAlerta,  
    NroTransporte,  
    NombreAlerta,  
    NombreConductor,  
    NombreTransportista,  
    PatenteTracto,  
    PatenteTrailer,  
    AlertaMapa,  
    LocalOrigenCodigo,  
    LocalOrigenDescripcion,  
    LocalDestinoCodigo,  
    LocalDestinoDescripcion,  
    TipoViaje,  
    FechaHoraCreacion,  
    TipoAlerta,  
    FechaGestion,  
    CategoriaAlerta,  
    TipoObservacion,  
    Observacion,
    MontoDiscrepancia
  )  
  SELECT DISTINCT  
    IdAlerta = A.Id,  
    NroTransporte = A.NroTransporte,  
    NombreAlerta = A.DescripcionAlerta,  
    NombreConductor = ISNULL(A.NombreConductor,''),  
    NombreTransportista = ISNULL(A.NombreTransportista,''),  
    PatenteTracto = ISNULL(A.PatenteTracto,''),  
    PatenteTrailer = ISNULL(A.PatenteTrailer,''),  
    AlertaMapa = ISNULL(A.AlertaMapa,''),  
    LocalOrigenCodigo = ISNULL(A.OrigenCodigo,-1),
    LocalOrigenDescripcion = ISNULL(CD.Nombre,''),
    LocalDestinoCodigo = ISNULL(L.CodigoInterno,-1),
    LocalDestinoDescripcion = ISNULL(L.Nombre,''),
    TipoViaje = ISNULL(TV.TipoViaje,''),  
    FechaHoraCreacion = A.FechaHoraCreacion, 
    TipoAlerta = ISNULL(A.TipoAlerta,''),  
    FechaGestion = HE.Fecha,
    CategoriaAlerta = ISNULL(HE.CategoriaAlerta,''),  
    TipoObservacion = ISNULL(HE.TipoObservacion,''),  
    Observacion = ISNULL(HE.Observacion,''),
    MontoDiscrepancia = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte AND D.LocalDestino = a.LocalDestino),0)
  FROM  
    CallCenterHistorialEscalamiento AS HE
    INNER JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
    INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    LEFT JOIN CentroDistribucion AS CD ON (A.OrigenCodigo = CD.CodigoInterno)
  WHERE  
      ( A.NroTransporte = @NroTransporte )  
  AND ( RTRIM(LTRIM(REPLACE(REPLACE(TV.RutTransportista,' ',''),'.',''))) = @Rut )    
  AND ( HE.ClasificacionAlerta = 'Roja')  
  ORDER BY  
    NombreAlerta, FechaHoraCreacion
    
  ---------------------------------------------------------------------------------------------  
  -- TABLA 0: LISTADO ALERTAS  
  ---------------------------------------------------------------------------------------------  
  SELECT DISTINCT  
   IdAlerta,  
   NroTransporte,  
   NombreAlerta,  
   NombreConductor,  
   NombreTransportista,  
   PatenteTracto,  
   PatenteTrailer,  
   AlertaMapa,  
   LocalOrigenCodigo,  
   LocalOrigenDescripcion,  
   LocalDestinoCodigo,  
   LocalDestinoDescripcion,  
   TipoViaje,  
   FechaHoraCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(FechaHoraCreacion,'FECHA_COMPLETA'),
   TipoAlerta  
  FROM  
   @TablaAlertaRoja  
  
  ---------------------------------------------------------------------------------------------  
  -- TABLA 1: CATEGORIA DE LAS ALERTAS  
  ---------------------------------------------------------------------------------------------  
  SELECT  
    IdAlerta,  
    FechaGestion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(FechaGestion,'FECHA_COMPLETA'),
    CategoriaAlerta,  
    TipoObservacion,  
    Observacion  
  FROM  
   @TablaAlertaRoja  
     
  ---------------------------------------------------------------------------------------------  
  -- TABLA 2: RESPUESTA TRANSPORTISTA    
  ---------------------------------------------------------------------------------------------  
  SELECT     
    Id,
    IdAlerta,
    Observacion = ISNULL(Observacion,''),
    FechaCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(FechaCreacion,'FECHA_COMPLETA'),
    Rechazo = ISNULL(Rechazo,0)
  FROM
    AlertaRojaRespuesta    
  WHERE
    NroTransporte = @NroTransporte       
  ORDER BY
    FechaCreacion DESC

  ---------------------------------------------------------------------------------------------
  -- TABLA 3: DISCREPANCIA
  ---------------------------------------------------------------------------------------------
  SELECT DISTINCT
    MontoDiscrepancia = ISNULL(MontoDiscrepancia,0)
  FROM
  	@TablaAlertaRoja
    
END  
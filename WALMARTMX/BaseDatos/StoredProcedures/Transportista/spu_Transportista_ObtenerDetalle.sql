﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Transportista_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Transportista_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Transportista_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle de transportista
**    Por          : DG, 09/06/2015
*******************************************************************************/
@Id AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  -----------------------------------------------------------------------------------
  -- TABLA 0: detalle
  -----------------------------------------------------------------------------------
  SELECT Rut = U.Rut + '-' + U.DV
		,Nombre = u.Nombre
  FROM Usuario u
  WHERE u.Id = @Id
  
  -----------------------------------------------------------------------------------
  -- TABLA 1: Ficha transportista
  -----------------------------------------------------------------------------------
  SELECT ft.Id
		  ,VolumenFlota
		  ,NombreDueño
		  ,Telefono1Dueño
		  ,Telefono2Dueño
		  ,EmailDueño
		  ,NombreGerenteOperacion
		  ,TelefonoGerenteOperacion
		  ,EmailGerenteOperacion
		  ,Telefono1Escalamiento
		  ,Telefono2Escalamiento
		  ,Telefono3Escalamiento
		  ,Telefono4Escalamiento
  FROM FichaTransportista ft
  WHERE ft.IdUsuario = @Id
END
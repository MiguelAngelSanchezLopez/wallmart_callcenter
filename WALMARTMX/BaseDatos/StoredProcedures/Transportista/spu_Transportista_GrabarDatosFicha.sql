﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_GrabarDatosFicha')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_GrabarDatosFicha'
  DROP  Procedure  dbo.spu_Transportista_GrabarDatosFicha
END

GO

PRINT  'Creating Procedure spu_Transportista_GrabarDatosFicha'
GO
CREATE Procedure spu_Transportista_GrabarDatosFicha
/******************************************************************************
**  Descripcion  : graba ficha transportista
**  Fecha        : 05/06/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdFicha AS INT,
@VolumenFlota AS INT,
@NombreDueño AS NCHAR(50),
@Telefono1Dueño AS NCHAR(10),
@Telefono2Dueño AS NCHAR(10),
@EmailDueño AS NCHAR(50),
@NombreGerenteOperacion AS NCHAR(50),
@TelefonoGerenteOperacion AS NCHAR(10),
@EmailGerenteOperacion AS NCHAR(50),
@Telefono1Escalamiento AS NCHAR(10),
@Telefono2Escalamiento AS NCHAR(10),
@Telefono3Escalamiento AS NCHAR(10),
@Telefono4Escalamiento AS NCHAR(10),
@IdTransportista AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
  
  IF (@IdFicha = -1)
  BEGIN
	  INSERT INTO FichaTransportista
	  (
  		VolumenFlota,
  		NombreDueño,
  		Telefono1Dueño,
  		Telefono2Dueño,
  		EmailDueño,
  		NombreGerenteOperacion,
  		TelefonoGerenteOperacion,
  		EmailGerenteOperacion,
  		Telefono1Escalamiento,
  		Telefono2Escalamiento,
  		Telefono3Escalamiento,
  		Telefono4Escalamiento,
  		IdUsuario
	  )
	  VALUES
	  (
  		@VolumenFlota,
  		@NombreDueño,
  		@Telefono1Dueño,
  		@Telefono2Dueño,
  		@EmailDueño,
  		@NombreGerenteOperacion,
  		@TelefonoGerenteOperacion,
  		@EmailGerenteOperacion,
  		@Telefono1Escalamiento,
  		@Telefono2Escalamiento,
  		@Telefono3Escalamiento,
  		@Telefono4Escalamiento,
  		@IdTransportista
	  )
	  
	  SET @Status	= SCOPE_IDENTITY()
  END  
  ELSE
  BEGIN
  	UPDATE FichaTransportista
  	SET
  		VolumenFlota = @VolumenFlota,
  		NombreDueño = @NombreDueño,
  		Telefono1Dueño = @Telefono1Dueño,
  		Telefono2Dueño = @Telefono2Dueño,
  		EmailDueño = @EmailDueño,
  		NombreGerenteOperacion = @NombreGerenteOperacion,
  		TelefonoGerenteOperacion = @TelefonoGerenteOperacion,
  		EmailGerenteOperacion = @EmailGerenteOperacion,
  		Telefono1Escalamiento = @Telefono1Escalamiento,
  		Telefono2Escalamiento = @Telefono2Escalamiento,
  		Telefono3Escalamiento = @Telefono3Escalamiento,
  		Telefono4Escalamiento = @Telefono4Escalamiento
  	WHERE ID = @IdFicha 
  	
	SET @Status	= @IdFicha
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = 'error'
    RETURN
  END

  COMMIT
END
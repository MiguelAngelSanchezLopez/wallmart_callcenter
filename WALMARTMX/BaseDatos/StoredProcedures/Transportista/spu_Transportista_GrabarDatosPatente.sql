﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Transportista_GrabarDatosPatente')
BEGIN
  PRINT  'Dropping Procedure spu_Transportista_GrabarDatosPatente'
  DROP  Procedure  dbo.spu_Transportista_GrabarDatosPatente
END

GO

PRINT  'Creating Procedure spu_Transportista_GrabarDatosPatente'
GO
CREATE Procedure spu_Transportista_GrabarDatosPatente
/******************************************************************************
**  Descripcion  : graba patente
**  Fecha        : 18/06/2015
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@Id AS INT,
@PlacaPatente AS VARCHAR(10),
@Marca AS VARCHAR(50),
@Tipo AS VARCHAR(50),
@IdTransportista AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
  
  IF (@Id = -1)
  BEGIN
	  INSERT INTO Patente
	  (
  		 PlacaPatente
  		,Marca
  		,Tipo
  		,idTransportista
	  )
	  VALUES
	  (
		 UPPER(@PlacaPatente)
		,@Marca
		,@Tipo
		,@IdTransportista
	  )
	  
	  SET @Status	= SCOPE_IDENTITY()
  END  
  ELSE
  BEGIN
  	UPDATE Patente
  	SET
  		PlacaPatente = UPPER(@PlacaPatente)
  		,Marca = @Marca
  		,Tipo = @Tipo
  		,idTransportista = @IdTransportista
  	WHERE Id = @Id
  	
	SET @Status	= @Id
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = 'error'
    RETURN
  END

  COMMIT
END
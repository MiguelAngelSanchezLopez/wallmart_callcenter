﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Carrier_getEmbarqueEnTienda')
BEGIN
  PRINT  'Dropping Procedure spu_Carrier_getEmbarqueEnTienda'
  DROP  Procedure  dbo.spu_Carrier_getEmbarqueEnTienda
END

GO

PRINT  'Creating Procedure spu_Carrier_getEmbarqueEnTienda'
GO
CREATE PROCEDURE [dbo].[spu_Carrier_getEmbarqueEnTienda] 
/******************************************************************************    
**    Descripcion  : obtiene remolques en patio  
**    Por          : DG, 07/02/2017  
*******************************************************************************/    
@IdUsuarioTransportista AS INT  
AS    
BEGIN    
  
	-- Se obtienen los datos desde el SP de Mobile, almacenandolos   
	-- en una tabla temporal para utilizarlos localmente  
	DECLARE @TablaRemolquesEnTienda AS TABLE(
		IdEmbarque BIGINT
		,IdMaster BIGINT
		,CodigoCedis INT
		,NombreCedis VARCHAR(255)
		,PlacaRemolque VARCHAR(255)
		,PlacaTracto VARCHAR(255)
		,Determinante VARCHAR(8000)
		,FechaLlegada VARCHAR(255)
		,FechaCita VARCHAR(255)
		,TipoTienda VARCHAR(255)
		,TiempoSobreestadia VARCHAR(255)
		,EstadoEnTienda VARCHAR(255)
		,Semaforo VARCHAR(255)
		,TiempoExcedidoSegundos BIGINT)

  INSERT INTO @TablaRemolquesEnTienda
  EXEC spu_Mobile_MX_AltotrackWalmart_CarrierObtenerEmbarquesEnTienda @IdUsuarioTransportista
  
  -----------------------------------------------------------------------
  -- TABLA 0: RESUMEN
  -----------------------------------------------------------------------
	SELECT
    Estado = T.EstadoEnTienda,
    Semaforo = T.Semaforo,
    TiempoPromedio = dbo.fnu_ConvertirSegundosToDHMS(AVG(T.TiempoExcedidoSegundos)),
    Total = COUNT(T.Semaforo)
  FROM
    @TablaRemolquesEnTienda AS T
  GROUP BY
    T.EstadoEnTienda, T.Semaforo

  -----------------------------------------------------------------------
  -- TABLA 1: DETALLE
  -----------------------------------------------------------------------
	SELECT   
		IdEmbarque
		,IdMaster
		,CodigoCedis
		,NombreCedis
		,PlacaRemolque
		,PlacaTracto
		,Determinante
		,FechaLlegada
		,FechaCita
		,TipoTienda
		,TiempoSobreestadia
		,EstadoEnTienda
		,Semaforo
		,TiempoExcedidoSegundos
	FROM @TablaRemolquesEnTienda     	

END 
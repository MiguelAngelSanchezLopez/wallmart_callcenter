﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Carrier_getRemolquesEnPatio')
BEGIN
  PRINT  'Dropping Procedure spu_Carrier_getRemolquesEnPatio'
  DROP  Procedure  dbo.spu_Carrier_getRemolquesEnPatio
END

GO

PRINT  'Creating Procedure spu_Carrier_getRemolquesEnPatio'
GO
CREATE PROCEDURE [dbo].[spu_Carrier_getRemolquesEnPatio] 
/******************************************************************************    
**    Descripcion  : obtiene remolques en patio  
**    Por          : DG, 07/02/2017  
*******************************************************************************/    
@IdUsuarioTransportista AS INT  
AS    
BEGIN    
  
 -- Se obtienen los datos desde el SP de Mobile, almacenandolos   
 -- en una tabla temporal para utilizarlos localmente  
 DECLARE @TablaRemolquesEnPatio AS TABLE(  
   IdEmbarque BIGINT  
  ,CodigoCedis INT  
  ,NombreCedis VARCHAR(255)  
  ,PlacaRemolque VARCHAR(50)  
  ,Determinante VARCHAR(8000)  
  ,TiempoEspera VARCHAR(255)  
  ,Semaforo VARCHAR(50)  
  ,TiempoEsperaSegundos INT  
 )  
    
 INSERT INTO @TablaRemolquesEnPatio  
 EXEC spu_Mobile_MX_AltotrackWalmart_CarrierObtenerRemolquesEnPatio @IdUsuarioTransportista  
  
  -----------------------------------------------------------------------
  -- TABLA 0: RESUMEN
  -----------------------------------------------------------------------
    SELECT
    Semaforo = T.Semaforo,
    TiempoPromedio = dbo.fnu_ConvertirSegundosToDHMS(AVG(T.TiempoEsperaSegundos)),
    Total = COUNT(T.Semaforo)
  FROM
    @TablaRemolquesEnPatio AS T
  GROUP BY
    T.Semaforo

  -----------------------------------------------------------------------
  -- TABLA 1: DETALLE
  -----------------------------------------------------------------------
	SELECT   
		IdEmbarque    
		,CodigoCedis    
		,NombreCedis    
		,PlacaRemolque    
		,Determinante  
		,TiempoEspera    
		,Semaforo    
		,TiempoEsperaSegundos    
	FROM @TablaRemolquesEnPatio     	

END 
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Carrier_getEmbarqueEnRuta')
BEGIN
  PRINT  'Dropping Procedure spu_Carrier_getEmbarqueEnRuta'
  DROP  Procedure  dbo.spu_Carrier_getEmbarqueEnRuta
END

GO

PRINT  'Creating Procedure spu_Carrier_getEmbarqueEnRuta'
GO
CREATE PROCEDURE [dbo].[spu_Carrier_getEmbarqueEnRuta] 
/******************************************************************************    
**    Descripcion  : obtiene remolques en patio  
**    Por          : DG, 07/02/2017  
*******************************************************************************/    
@IdUsuarioTransportista AS INT  
AS    
BEGIN    
  
	-- Se obtienen los datos desde el SP de Mobile, almacenandolos   
	-- en una tabla temporal para utilizarlos localmente  
	DECLARE @TablaRemolquesEnRuta AS TABLE(
		IdEmbarque BIGINT
		,IdMaster BIGINT
		,CodigoCedis INT
		,NombreCedis VARCHAR(255)
		,PlacaRemolque VARCHAR(255)
		,PlacaTracto VARCHAR(255)
		,Determinante VARCHAR(8000)
		,TiempoViaje VARCHAR(255)
		,TiempoEstimadoArribo VARCHAR(255)
		,KilometrosRecorridos VARCHAR(255)
		,TotalAlertas INT
		,Semaforo VARCHAR(255)
		,TiempoAtrasoSegundos BIGINT
	)

  INSERT INTO @TablaRemolquesEnRuta
  EXEC spu_Mobile_MX_AltotrackWalmart_CarrierObtenerEmbarquesEnRuta @IdUsuarioTransportista
  
  -----------------------------------------------------------------------
  -- TABLA 0: RESUMEN
  -----------------------------------------------------------------------
	SELECT
		Semaforo = T.Semaforo,
		TiempoPromedio = dbo.fnu_ConvertirSegundosToDHMS(AVG(T.TiempoAtrasoSegundos)),
		Total = COUNT(T.Semaforo)
	FROM
		@TablaRemolquesEnRuta AS T
	GROUP BY
		T.Semaforo

  -----------------------------------------------------------------------
  -- TABLA 1: DETALLE
  -----------------------------------------------------------------------
	SELECT   
		IdEmbarque
		,IdMaster 
		,CodigoCedis
		,NombreCedis
		,PlacaRemolque
		,PlacaTracto 
		,Determinante
		,TiempoViaje 
		,TiempoEstimadoArribo 
		,KilometrosRecorridos 
		,TotalAlertas 
		,Semaforo 
		,TiempoAtrasoSegundos
	FROM @TablaRemolquesEnRuta     	

END 
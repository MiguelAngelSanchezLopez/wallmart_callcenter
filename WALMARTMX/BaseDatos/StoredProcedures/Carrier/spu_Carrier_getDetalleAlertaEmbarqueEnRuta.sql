﻿ IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Carrier_getDetalleAlertaEmbarqueEnRuta')
BEGIN
  PRINT  'Dropping Procedure spu_Carrier_getDetalleAlertaEmbarqueEnRuta'
  DROP  Procedure  dbo.spu_Carrier_getDetalleAlertaEmbarqueEnRuta
END

GO

PRINT  'Creating Procedure spu_Carrier_getDetalleAlertaEmbarqueEnRuta'
GO
CREATE PROCEDURE [dbo].[spu_Carrier_getDetalleAlertaEmbarqueEnRuta] 
/******************************************************************************    
**    Descripcion  : obtiene remolques en patio  
**    Por          : DG, 07/02/2017  
*******************************************************************************/    
 @IdMaster AS BIGINT  
,@IdEmbarque AS BIGINT
AS    
BEGIN    

	 SELECT
	   DescripcionAlerta = A.DescripcionAlerta
	   ,FechaAlerta = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(A.FechaHoraCreacion, 'FECHA_COMPLETA'),'')
	   ,Gestionada = CASE WHEN ISNULL(HE.IdAlerta, -1) = -1 THEN 'NO' ELSE 'SI' END 
	   ,Lat = a.LatTracto
	   ,Lon = a.LonTracto
	 FROM
	   CallCenterAlerta AS CCA
	   INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
	   LEFT JOIN CallCenterHistorialEscalamiento AS HE ON (A.Id = HE.IdAlerta)
	 WHERE
		 A.NroTransporte = @idMaster
	 AND A.IdEmbarque = @IdEmbarque
	 --AND A.LocalDestino = @LocalDestino
	 ORDER BY
	   A.FechaHoraCreacion DESC

END
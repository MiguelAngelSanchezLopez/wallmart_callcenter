ALTER PROCEDURE spu_Carrier_ListarInforme (
	@IdUsuario   int,
	@Fech_Ini    varchar(10),
	@Fech_Fin    varchar(10),
	@TipoInforme varchar(120)
)
AS
BEGIN
	Select 
		c.Id,
		c.IdUsuarioTrans,
		c.Fecha,
		t.Nombre 
	From 
		carrier_informe c Inner Join
		TipoGeneral t ON c.Tipo_Informe = t.Valor And t.Tipo = 'CarrierInforme'
	Where 
		(IdUsuarioTrans =  @IdUsuario ) And
		((@Fech_Ini    = ''  OR @Fech_Fin = '')  OR Fecha between Convert(Date, @Fech_Ini) And Convert(Date,@Fech_Fin)) And
		(@TipoInforme = '-1' OR Tipo_Informe = @TipoInforme)
		
END
		
	
	
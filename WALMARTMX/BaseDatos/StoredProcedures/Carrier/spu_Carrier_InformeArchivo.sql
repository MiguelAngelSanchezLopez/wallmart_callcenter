/**
 ** Autor		: Mauricio Espinoza.
 ** Fecha		: 08/02/2017
 ** Descripción	: Retorna el archivo de un Informe 
 **/
 CREATE PROCEDURE spu_Carrier_InformeArchivo (
	@IdInforme int	
 )
 AS
 BEGIN	
	SELECT TOP 1    
		IdArchivo		= ISNULL(A.Id,-1),
		NombreArchivo	= ISNULL(A.Nombre,''),
		Extension		= ISNULL(A.Extension,''),
		ContentType		= ISNULL(A.ContentType,''),
		Tamano			= ISNULL(A.Tamano,0),
		Contenido		= Contenido 
	FROM    
		ArchivosXRegistro AxR 
		LEFT JOIN Archivo a ON (a.id = axr.IdArchivo)
	WHERE
		AxR.IdRegistro = @IdInforme And
		AxR.Entidad = 'CarrierInforme_PDF'


END
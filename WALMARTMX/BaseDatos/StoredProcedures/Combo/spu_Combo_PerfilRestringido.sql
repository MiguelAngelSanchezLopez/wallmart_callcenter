﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_PerfilRestringido')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_PerfilRestringido'
  DROP  Procedure  dbo.spu_Combo_PerfilRestringido
END

GO

PRINT  'Creating Procedure spu_Combo_PerfilRestringido'
GO
CREATE Procedure dbo.spu_Combo_PerfilRestringido
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Perfil", pero dependiendo de los que puede mirar
**    Autor        : VSR
**    Fecha        : 07/10/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @llavePerfilUsuarioConectado VARCHAR(255)
  SET @llavePerfilUsuarioConectado = (SELECT P.Llave FROM Usuario AS U INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id) WHERE U.Id = @Filtro)

  EXEC spu_Perfil_ObtenerListadoPorTipoPerfil @llavePerfilUsuarioConectado

END




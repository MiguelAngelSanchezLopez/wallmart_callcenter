﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ImportacionNombrePuertoDescarga')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ImportacionNombrePuertoDescarga'
  DROP  Procedure  dbo.spu_Combo_ImportacionNombrePuertoDescarga
END

GO

PRINT  'Creating Procedure spu_Combo_ImportacionNombrePuertoDescarga'
GO
CREATE Procedure dbo.spu_Combo_ImportacionNombrePuertoDescarga
/******************************************************************************
**    Descripcion  : obtiene NombrePuertoDescarga de tabla OS_Importacion
**    Autor        : VSR
**    Fecha        : 11/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = I.NombrePuertoDescarga,
    Valor = I.NombrePuertoDescarga
  FROM
    OS_Importacion AS I
  WHERE
    ISNULL(I.NombrePuertoDescarga,'') <> ''
  ORDER BY
    Texto

END




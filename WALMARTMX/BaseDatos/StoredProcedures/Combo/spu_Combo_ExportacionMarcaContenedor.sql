﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ExportacionMarcaContenedor')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ExportacionMarcaContenedor'
  DROP  Procedure  dbo.spu_Combo_ExportacionMarcaContenedor
END

GO

PRINT  'Creating Procedure spu_Combo_ExportacionMarcaContenedor'
GO
CREATE Procedure dbo.spu_Combo_ExportacionMarcaContenedor
/******************************************************************************
**    Descripcion  : obtiene MarcaContenedor de tabla OS_Exportacion
**    Autor        : VSR
**    Fecha        : 26/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = E.MarcaContenedor,
    Valor = E.MarcaContenedor
  FROM
    OS_Exportacion AS E
  WHERE
    ISNULL(E.MarcaContenedor,'') <> ''
  ORDER BY
    Texto

END




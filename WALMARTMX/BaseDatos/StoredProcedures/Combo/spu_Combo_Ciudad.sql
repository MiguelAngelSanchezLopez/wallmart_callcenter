﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Ciudad')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Ciudad'
  DROP  Procedure  dbo.spu_Combo_Ciudad
END

GO

PRINT  'Creating Procedure spu_Combo_Ciudad'
GO
CREATE Procedure spu_Combo_Ciudad
/*********************************************************************************************
**    Descripcion  : Obtiene el listado de regiones
**    Fecha        : 02/06/2015
*********************************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    --Texto = RTRIM(LTRIM(TG.Nombre)),
    --Valor = RTRIM(LTRIM(TG.id))
	Texto = 'CIUDAD DE MEXICO'
	,Valor = 'CIUDAD DE MEXICO'
  --FROM
    --region AS TG
  
  
END

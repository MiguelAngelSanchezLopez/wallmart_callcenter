﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_EstadoRespuestaAlertaRoja')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_EstadoRespuestaAlertaRoja'
  DROP  Procedure  dbo.spu_Combo_EstadoRespuestaAlertaRoja
END

GO

PRINT  'Creating Procedure spu_Combo_EstadoRespuestaAlertaRoja'
GO
CREATE Procedure dbo.spu_Combo_EstadoRespuestaAlertaRoja
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral -> EstadoAlertaRoja"
**    Autor        : VSR
**    Fecha        : 15/12/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.Valor))
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'EstadoAlertaRoja'
  AND TG.Activo = 1
  AND Nombre <> 'ENVIADO TRANSPORTISTA'
  ORDER BY
    CONVERT(INT,TG.Extra1) 

END




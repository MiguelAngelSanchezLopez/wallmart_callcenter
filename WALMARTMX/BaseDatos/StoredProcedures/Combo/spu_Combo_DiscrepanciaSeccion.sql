﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_DiscrepanciaSeccion')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_DiscrepanciaSeccion'
  DROP  Procedure  dbo.spu_Combo_DiscrepanciaSeccion
END

GO

PRINT  'Creating Procedure spu_Combo_DiscrepanciaSeccion'
GO
CREATE Procedure dbo.spu_Combo_DiscrepanciaSeccion
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Discrepancia"
**    Autor        : VSR
**    Fecha        : 18/08/2015
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = D.Seccion,
    Valor = D.Seccion
  FROM
    Discrepancia AS D
  WHERE
    ISNULL(D.Seccion,'') <> ''
  ORDER BY
    Texto

END




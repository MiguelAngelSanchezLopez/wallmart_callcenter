﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_DiscrepanciaResolucionFinal')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_DiscrepanciaResolucionFinal'
  DROP  Procedure  dbo.spu_Combo_DiscrepanciaResolucionFinal
END

GO

PRINT  'Creating Procedure spu_Combo_DiscrepanciaResolucionFinal'
GO
CREATE Procedure dbo.spu_Combo_DiscrepanciaResolucionFinal
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Discrepancia"
**    Autor        : VSR
**    Fecha        : 18/08/2015
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = D.ResolucionFinal,
    Valor = D.ResolucionFinal
  FROM
    Discrepancia AS D
  WHERE
    ISNULL(D.ResolucionFinal,'') <> ''
  ORDER BY
    Texto

END




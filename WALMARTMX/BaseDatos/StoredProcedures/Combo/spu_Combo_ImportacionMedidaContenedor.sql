﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ImportacionMedidaContenedor')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ImportacionMedidaContenedor'
  DROP  Procedure  dbo.spu_Combo_ImportacionMedidaContenedor
END

GO

PRINT  'Creating Procedure spu_Combo_ImportacionMedidaContenedor'
GO
CREATE Procedure dbo.spu_Combo_ImportacionMedidaContenedor
/******************************************************************************
**    Descripcion  : obtiene MedidaContenedor de tabla OS_Importacion
**    Autor        : VSR
**    Fecha        : 11/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = I.MedidaContenedor,
    Valor = I.MedidaContenedor
  FROM
    OS_Importacion AS I
  WHERE
    ISNULL(I.MedidaContenedor,'') <> ''
  ORDER BY
    Texto

END




﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_AlertaDefinicionPorFormato')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_AlertaDefinicionPorFormato'
  DROP  Procedure  dbo.spu_Combo_AlertaDefinicionPorFormato
END

GO

PRINT  'Creating Procedure spu_Combo_AlertaDefinicionPorFormato'
GO
CREATE Procedure dbo.spu_Combo_AlertaDefinicionPorFormato
/*********************************************************************************************
**    Descripcion  : Obtiene la definicion de alertas filtrado por formato
**    Autor        : VSR
**    Fecha        : 14/04/2015
**********************************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(Nombre)),
    Valor = ADPF.Id
  FROM
    AlertaDefinicionPorFormato AS ADPF
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)
  WHERE
		ADPF.IdFormato = @Filtro
  ORDER BY
    Texto

END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_CriterioPlanificacion')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_CriterioPlanificacion'
  DROP  Procedure  dbo.spu_Combo_CriterioPlanificacion
END

GO

PRINT  'Creating Procedure spu_Combo_CriterioPlanificacion'
GO
CREATE Procedure dbo.spu_Combo_CriterioPlanificacion
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral"
**    Autor        : VSR
**    Fecha        : 09/04/2015
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON
  
  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.Valor)),
    Seleccionar = ISNULL(TG.Extra1,0)
  FROM
    TipoGeneral AS TG
  WHERE
	TG.Tipo = 'CriterioPlanificacion'
	AND TG.Activo = 1

END
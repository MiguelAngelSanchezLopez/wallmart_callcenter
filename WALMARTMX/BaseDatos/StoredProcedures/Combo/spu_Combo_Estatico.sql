﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Estatico')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Estatico'
  DROP  Procedure  dbo.spu_Combo_Estatico
END

GO

PRINT  'Creating Procedure spu_Combo_Estatico'
GO
CREATE Procedure dbo.spu_Combo_Estatico
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral"
**    Autor        : VSR
**    Fecha        : 09/07/2014
*******************************************************************************/
@TipoCombo AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.Valor))
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = @TipoCombo
  AND TG.Activo = 1
  ORDER BY
    TG.Nombre
END




﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Conductor')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Conductor'
  DROP  Procedure  dbo.spu_Combo_Conductor
END

GO

PRINT  'Creating Procedure spu_Combo_Conductor'
GO
CREATE Procedure dbo.spu_Combo_Conductor
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Usuario -> Perfil Conductor"
**    Autor        : VSR
**    Fecha        : 28/07/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = UPPER(ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,'')),
    Valor = U.Id
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      U.Estado = 1
  AND P.Llave = 'CON' -- obtiene el perfil CONDUCTOR
  ORDER BY
    Texto

END




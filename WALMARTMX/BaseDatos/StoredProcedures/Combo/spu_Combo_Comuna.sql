﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Comuna')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Comuna'
  DROP  Procedure  dbo.spu_Combo_Comuna
END

GO

PRINT  'Creating Procedure spu_Combo_Comuna'
GO
CREATE Procedure spu_Combo_Comuna
/*********************************************************************************************
**    Descripcion  : Obtiene el listado de comunas por region
**    Autor        : DG
**    Fecha        : 02/06/2015
*********************************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(Nombre)),
    Valor = RTRIM(LTRIM(id))
  FROM
    comuna
  WHERE idRegion = @Filtro
END
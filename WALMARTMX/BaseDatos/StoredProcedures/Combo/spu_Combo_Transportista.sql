﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Transportista')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Transportista'
  DROP  Procedure  dbo.spu_Combo_Transportista
END

GO

PRINT  'Creating Procedure spu_Combo_Transportista'
GO
CREATE Procedure dbo.spu_Combo_Transportista
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Usuario -> Perfil Transportista"
**    Autor        : VSR
**    Fecha        : 08/06/2015
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''))),
    Valor = U.Id
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    OUTER APPLY (
      SELECT DISTINCT
        TH.IdUsuario
      FROM
        Track_Homoclave AS TH
      WHERE
        TH.Activo = 1
    ) AS TablaHomoclave
  WHERE
      U.Estado = 1
  AND P.Llave = 'TRA' -- obtiene el perfil Transportista
  AND TablaHomoclave.IdUsuario = U.Id
  ORDER BY
    Texto

END




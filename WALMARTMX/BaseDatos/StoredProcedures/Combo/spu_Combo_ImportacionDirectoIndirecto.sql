﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ImportacionDirectoIndirecto')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ImportacionDirectoIndirecto'
  DROP  Procedure  dbo.spu_Combo_ImportacionDirectoIndirecto
END

GO

PRINT  'Creating Procedure spu_Combo_ImportacionDirectoIndirecto'
GO
CREATE Procedure dbo.spu_Combo_ImportacionDirectoIndirecto
/******************************************************************************
**    Descripcion  : obtiene DirectoIndirecto de tabla OS_Importacion
**    Autor        : VSR
**    Fecha        : 11/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = I.DirectoIndirecto,
    Valor = I.DirectoIndirecto
  FROM
    OS_Importacion AS I
  WHERE
    ISNULL(I.DirectoIndirecto,'') <> ''
  ORDER BY
    Texto

END




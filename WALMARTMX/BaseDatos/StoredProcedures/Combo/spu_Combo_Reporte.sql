﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Reporte')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Reporte'
  DROP  Procedure  dbo.spu_Combo_Reporte
END

GO

PRINT  'Creating Procedure spu_Combo_Reporte'
GO
CREATE Procedure dbo.spu_Combo_Reporte
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Reporte"
**    Autor        : VSR
**    Fecha        : 22/09/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = R.Nombre,
    Valor = R.Id
  FROM
    Reporte AS R
  WHERE
    R.Activo = 1
  ORDER BY
    Texto

END




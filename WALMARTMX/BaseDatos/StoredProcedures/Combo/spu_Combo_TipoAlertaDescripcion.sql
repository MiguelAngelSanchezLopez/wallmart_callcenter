﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_TipoAlertaDescripcion')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_TipoAlertaDescripcion'
  DROP  Procedure  dbo.spu_Combo_TipoAlertaDescripcion
END

GO

PRINT  'Creating Procedure spu_Combo_TipoAlertaDescripcion'
GO
CREATE Procedure dbo.spu_Combo_TipoAlertaDescripcion
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral -> TipoAlertaDescripcion"
**    Autor        : VSR
**    Fecha        : 05/08/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.Valor)),
    Orden = 1
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'TipoAlertaDescripcion'
  AND TG.Activo = 1
  AND TG.Extra1 = @Filtro

  -- agrega opcion OTRO al final del combo
  UNION ALL
  SELECT
    Texto = 'Otro ...',
    Valor = 'MostrarCampoOtro',
    Orden = 2
  ORDER BY
    Orden, Texto

END




﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Carga')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Carga'
  DROP  Procedure  dbo.spu_Combo_Carga
END

GO

PRINT  'Creating Procedure spu_Combo_Carga'
GO
CREATE Procedure dbo.spu_Combo_Carga
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "PlanificacionTransportista.Carga"
**    Autor        : VSR
**    Fecha        : 24/06/2015
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = ISNULL(PT.Carga,''),
    Valor = ISNULL(PT.Carga,'')
  FROM
    PlanificacionTransportista AS PT
  WHERE
    ISNULL(PT.Carga,'') <> ''
  GROUP BY
    ISNULL(PT.Carga,'')
  ORDER BY
    Texto

END




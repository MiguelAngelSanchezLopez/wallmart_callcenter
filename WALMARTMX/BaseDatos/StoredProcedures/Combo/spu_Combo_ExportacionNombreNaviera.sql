﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ExportacionNombreNaviera')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ExportacionNombreNaviera'
  DROP  Procedure  dbo.spu_Combo_ExportacionNombreNaviera
END

GO

PRINT  'Creating Procedure spu_Combo_ExportacionNombreNaviera'
GO
CREATE Procedure dbo.spu_Combo_ExportacionNombreNaviera
/******************************************************************************
**    Descripcion  : obtiene NombreNaviera de tabla OS_Exportacion
**    Autor        : VSR
**    Fecha        : 26/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = E.NombreNaviera,
    Valor = E.NombreNaviera
  FROM
    OS_Exportacion AS E
  WHERE
    ISNULL(E.NombreNaviera,'') <> ''
  ORDER BY
    Texto

END




﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_TipoDocumento')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_TipoDocumento'
  DROP  Procedure  dbo.spu_Combo_TipoDocumento
END

GO

PRINT  'Creating Procedure spu_Combo_TipoDocumento'
GO
CREATE Procedure [dbo].[spu_Combo_TipoDocumento]
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoDocumento"
**    Autor        : VSR
**    Fecha        : 14/04/2015
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = ISNULL(TD.NombreDocumento,''),
    Valor = ISNULL(TD.Id,'')
  FROM
    TipoDocumento AS TD
  ORDER BY
    Orden

END
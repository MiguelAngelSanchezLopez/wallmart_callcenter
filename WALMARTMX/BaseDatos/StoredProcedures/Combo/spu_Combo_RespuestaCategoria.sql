﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_RespuestaCategoria')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_RespuestaCategoria'
  DROP  Procedure  dbo.spu_Combo_RespuestaCategoria
END

GO

PRINT  'Creating Procedure spu_Combo_RespuestaCategoria'
GO

CREATE Procedure spu_Combo_RespuestaCategoria
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "RespuestaCategoria"
**    Autor        : DG
**    Fecha        : 03/10/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RC.Nombre,
    Valor = RC.Id
  FROM
    RespuestaCategoria AS RC
  ORDER BY
    ordenar

END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_CategoriaAlerta')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_CategoriaAlerta'
  DROP  Procedure  dbo.spu_Combo_CategoriaAlerta
END

GO

PRINT  'Creating Procedure spu_Combo_CategoriaAlerta'
GO
CREATE Procedure spu_Combo_CategoriaAlerta
/*********************************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "TipoGeneral -> CategoriaAlerta"
**    Autor        : VSR
**    Fecha        : 30/07/2014
*********************************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = RTRIM(LTRIM(TG.Nombre)),
    Valor = RTRIM(LTRIM(TG.Valor))
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'CategoriaAlerta'
  AND TG.Activo = 1
END

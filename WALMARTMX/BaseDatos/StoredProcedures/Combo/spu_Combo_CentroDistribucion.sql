﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_CentroDistribucion')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_CentroDistribucion'
  DROP  Procedure  dbo.spu_Combo_CentroDistribucion
END

GO

PRINT  'Creating Procedure spu_Combo_CentroDistribucion'
GO
CREATE Procedure dbo.spu_Combo_CentroDistribucion
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "CentroDistribucion"
**    Autor        : VSR
**    Fecha        : 15/10/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = CD.Nombre + ISNULL(' - ' + CONVERT(VARCHAR,CD.CodigoInterno),''),
    Valor = CD.CodigoInterno
  FROM
    CentroDistribucion AS CD
  WHERE
    CD.Activo = 1
  ORDER BY
    Texto

END




﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Formato')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Formato'
  DROP  Procedure  dbo.spu_Combo_Formato
END

GO

PRINT  'Creating Procedure spu_Combo_Formato'
GO
CREATE Procedure dbo.spu_Combo_Formato
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Formato"
**    Autor        : VSR
**    Fecha        : 09/04/2015
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = F.Nombre,
    Valor = F.Id
  FROM
    Formato AS F
  ORDER BY
    Texto

END




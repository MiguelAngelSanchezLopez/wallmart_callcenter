﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ImportacionNombreCliente')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ImportacionNombreCliente'
  DROP  Procedure  dbo.spu_Combo_ImportacionNombreCliente
END

GO

PRINT  'Creating Procedure spu_Combo_ImportacionNombreCliente'
GO
CREATE Procedure dbo.spu_Combo_ImportacionNombreCliente
/******************************************************************************
**    Descripcion  : obtiene NombreCliente de tabla OS_Importacion
**    Autor        : VSR
**    Fecha        : 11/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = I.NombreCliente,
    Valor = I.NombreCliente
  FROM
    OS_Importacion AS I
  WHERE
    ISNULL(I.NombreCliente,'') <> ''
  ORDER BY
    Texto

END




﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ContactoEscalamiento')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ContactoEscalamiento'
  DROP  Procedure  dbo.spu_Combo_ContactoEscalamiento
END

GO

PRINT  'Creating Procedure spu_Combo_ContactoEscalamiento'
GO
CREATE Procedure dbo.spu_Combo_ContactoEscalamiento
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Usuario"
**    Autor        : VSR
**    Fecha        : 05/08/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,'') + ISNULL(' [' + P.Nombre + ']',''),
    Valor = U.Id
  FROM
    Usuario AS U
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
  WHERE
      U.Estado = 1
  AND P.Llave NOT IN (
    'ADM',
    'CON',
    'TO'
  )
  ORDER BY
    Texto

END

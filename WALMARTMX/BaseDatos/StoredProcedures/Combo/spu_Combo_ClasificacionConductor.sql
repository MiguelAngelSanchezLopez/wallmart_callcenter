﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ClasificacionConductor')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ClasificacionConductor'
  DROP  Procedure  dbo.spu_Combo_ClasificacionConductor
END

GO

PRINT  'Creating Procedure spu_Combo_ClasificacionConductor'
GO
CREATE Procedure dbo.spu_Combo_ClasificacionConductor
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "ClasificacionConductor"
**    Autor        : VSR
**    Fecha        : 26/04/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Valor = CC.Nombre,
    Texto = CC.Nombre
  FROM
    ClasificacionConductor AS CC
  ORDER BY
    Texto

END




﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_ImportacionDiasDemurrage')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_ImportacionDiasDemurrage'
  DROP  Procedure  dbo.spu_Combo_ImportacionDiasDemurrage
END

GO

PRINT  'Creating Procedure spu_Combo_ImportacionDiasDemurrage'
GO
CREATE Procedure dbo.spu_Combo_ImportacionDiasDemurrage
/******************************************************************************
**    Descripcion  : obtiene DiasDemurrage de tabla OS_Importacion
**    Autor        : VSR
**    Fecha        : 11/05/2016
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT DISTINCT
    Texto = I.DiasDemurrage,
    Valor = I.DiasDemurrage
  FROM
    OS_Importacion AS I
  WHERE
    ISNULL(I.DiasDemurrage,'') <> ''
  ORDER BY
    Texto

END




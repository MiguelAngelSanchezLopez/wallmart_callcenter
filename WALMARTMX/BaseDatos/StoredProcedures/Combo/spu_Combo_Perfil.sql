﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Combo_Perfil')
BEGIN
  PRINT  'Dropping Procedure spu_Combo_Perfil'
  DROP  Procedure  dbo.spu_Combo_Perfil
END

GO

PRINT  'Creating Procedure spu_Combo_Perfil'
GO
CREATE Procedure dbo.spu_Combo_Perfil
/******************************************************************************
**    Descripcion  : Obtiene datos para llenar combo de tabla "Perfil"
**    Autor        : VSR
**    Fecha        : 28/07/2014
*******************************************************************************/
@Filtro AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Texto = P.Nombre,
    Valor = P.Id
  FROM
    Perfil AS P
  ORDER BY
    Texto

END




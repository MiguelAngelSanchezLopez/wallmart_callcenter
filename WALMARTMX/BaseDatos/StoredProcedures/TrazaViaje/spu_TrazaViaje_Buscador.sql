﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_TrazaViaje_Buscador')
BEGIN
  PRINT  'Dropping Procedure spu_TrazaViaje_Buscador'
  DROP  Procedure  dbo.spu_TrazaViaje_Buscador
END

GO

PRINT  'Creating Procedure spu_TrazaViaje_Buscador'
GO
CREATE Procedure dbo.spu_TrazaViaje_Buscador
/******************************************************************************
**    Descripcion  : obtiene listado de traza de viajes
**    Por          : VSR, 15/10/2014
*******************************************************************************/
@IdUsuario AS INT,
@NroTransporte AS INT,
@CDOrigen AS INT,
@LocalDestino AS INT,
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255),
@EstadoViaje AS VARCHAR(255),
@Patente AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  DECLARE @mostrarTodosLocales INT
  
  -- obtiene la llave del perfil del usuario conectado
  DECLARE @llavePerfilUsuarioConectado VARCHAR(255)
  SET @llavePerfilUsuarioConectado = (SELECT P.Llave FROM Usuario AS U INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id) WHERE U.Id = @IdUsuario)
  IF (@llavePerfilUsuarioConectado IS NULL) SET @llavePerfilUsuarioConectado = '-1'

  SET @mostrarTodosLocales = CASE WHEN EXISTS(
  	                                           SELECT
                                                 PPF.LlavePerfil
                                               FROM
                                                 vwu_PerfilesPorFormato AS PPF
                                               WHERE
                                                   PPF.Tipo = 'Local' 
                                               AND PPF.LlavePerfil = @llavePerfilUsuarioConectado
                                       ) THEN 0 ELSE 1
                             END

  SELECT
    IdTrazaViaje = TV.Id,
    NroTransporte = TV.NroTransporte,
    NombreConductor = ISNULL(TV.NombreConductor,''),
    PatenteTracto = ISNULL(TV.PatenteTracto,''),
    PatenteTrailer = ISNULL(TV.PatenteTrailer,''),
    CDOrigen = ISNULL(TV.OrigenDescripcion,'') + ISNULL(' - ' + CONVERT(VARCHAR, TV.OrigenCodigo),''),
    LocalDestino = ISNULL(TV.DestinoDescripcion,'') + ISNULL(' - ' + CONVERT(VARCHAR, TV.LocalDestino),''),
    FHSalidaOrigen = ISNULL(TV.FHSalidaOrigen,''),
    TipoViaje = ISNULL(TV.TipoViaje,''),
    EstadoViaje = ISNULL(TV.EstadoViaje,'')
  FROM
    TrazaViaje AS TV
    INNER JOIN (
                --------------------------------------------------------------------------------
                -- obtiene locales filtrados segun el perfil del usuario conectado
                --------------------------------------------------------------------------------
                SELECT
                  CodigoInterno = L.CodigoInterno
                FROM
                  Local AS L
                WHERE
                    L.Activo = 1
                AND (
                         @mostrarTodosLocales = 1
                      OR ( L.Id IN (SELECT LPU.IdLocal FROM LocalesPorUsuario AS LPU WHERE LPU.IdUsuario = @IdUsuario ) )
                    )
    ) AS TablaLocalesUsuario ON (TV.LocalDestino = TablaLocalesUsuario.CodigoInterno)
  WHERE
      (@NroTransporte = -1 OR TV.NroTransporte = @NroTransporte)
  AND (@CDOrigen = -1 OR TV.OrigenCodigo = @CDOrigen)
  AND (@LocalDestino = -1 OR TV.LocalDestino = @LocalDestino)
  AND CONVERT(DATETIME,CONVERT(VARCHAR, ISNULL(TV.FHSalidaOrigen, '01/01/2014 00:00'), 112) + ' 00:00') BETWEEN @FechaDesde AND @FechaHasta
  AND (@EstadoViaje = '-1' OR TV.EstadoViaje LIKE '%'+ @EstadoViaje +'%')
  AND (@Patente = '-1' OR TV.PatenteTracto = @Patente OR TV.PatenteTrailer = @Patente)
  ORDER BY
    NroTransporte, IdTrazaViaje

END

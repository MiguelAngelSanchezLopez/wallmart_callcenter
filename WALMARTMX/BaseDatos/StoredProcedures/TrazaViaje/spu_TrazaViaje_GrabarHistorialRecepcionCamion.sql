﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_TrazaViaje_GrabarHistorialRecepcionCamion')
BEGIN
  PRINT  'Dropping Procedure spu_TrazaViaje_GrabarHistorialRecepcionCamion'
  DROP  Procedure  dbo.spu_TrazaViaje_GrabarHistorialRecepcionCamion
END

GO

PRINT  'Creating Procedure spu_TrazaViaje_GrabarHistorialRecepcionCamion'
GO
CREATE Procedure dbo.spu_TrazaViaje_GrabarHistorialRecepcionCamion
/******************************************************************************
**  Descripcion  : graba la recepcion del camion
**  Fecha        : 17/10/2014
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdUsuario AS INT,
@NroTransporte AS INT,
@PatenteTracto AS VARCHAR(255),
@Estado AS VARCHAR(255),
@Observacion AS VARCHAR(4000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    INSERT INTO HistorialRecepcionCamion
    (
	    NroTransporte,
	    PatenteTracto,
	    Estado,
	    Observacion,
	    IdUsuarioCreacion,
	    FechaCreacion
    )
    VALUES
    (
	    @NroTransporte,
	    @PatenteTracto,
	    @Estado,
	    @Observacion,
	    @IdUsuario,
	    dbo.fnu_GETDATE()
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

    SET @Status = 'ingresado'

  COMMIT
END
GO        
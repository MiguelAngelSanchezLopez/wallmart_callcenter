﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_TrazaViaje_Actualizar')
BEGIN
  PRINT  'Dropping Procedure spu_TrazaViaje_Actualizar'
  DROP  Procedure  dbo.spu_TrazaViaje_Actualizar
END

GO

PRINT  'Creating Procedure spu_TrazaViaje_Actualizar'
GO

CREATE PROCEDURE [dbo].[spu_TrazaViaje_Actualizar]
/******************************************************************************
**    Descripcion  : Actualiza registro en la tabla TrazaViaje
**    Autor        : VSR
**    Fecha        : 23/05/2014
*******************************************************************************/
@Id AS INT,
@NroTransporte AS INT,
@Operacion AS INT,
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255),
@RutTransportista AS VARCHAR(255),
@NombreTransportista AS VARCHAR(255),
@RutConductor AS VARCHAR(255),
@NombreConductor AS VARCHAR(255),
@OrigenCodigo AS INT,
@OrigenDescripcion AS VARCHAR(255),
@FHSalidaOrigen AS DATETIME,
@LocalDestino AS INT,
@DestinoDescripcion AS VARCHAR(255),
@FHLlegadaDestino AS DATETIME,
@FHAperturaPuerta AS DATETIME,
@FHSalidaDestino AS DATETIME,
@TipoViaje AS VARCHAR(255),
@ProveedorGPS AS VARCHAR(255),
@DestinoDomicilio AS VARCHAR(255),
@DestinoRegion AS VARCHAR(255),
@DestinoDescripcionRegion AS VARCHAR(255),
@DestinoComuna AS VARCHAR(255),
@DestinoLat AS VARCHAR(255),
@DestinoLon AS VARCHAR(255),
@EstadoViaje AS VARCHAR(255),
@SecuenciaDestino AS INT,
@RamplaProveedorGPS AS VARCHAR(255),
@EstadoPuerta AS VARCHAR(255),
@EstadoLat AS VARCHAR(255),
@EstadoLon AS VARCHAR(255),
@TempSalidaCD AS VARCHAR(255),
@FechaHoraCreacion AS DATETIME,
@FechaHoraActualizacion AS DATETIME,
@IngresoSCAT AS BIT
AS
BEGIN

  IF (@NroTransporte = -1) SET @NroTransporte = NULL
  IF (@Operacion = -1) SET @Operacion = NULL
  IF (@OrigenCodigo = -1) SET @OrigenCodigo = NULL
  IF (@LocalDestino = -1) SET @LocalDestino = NULL
  IF (@SecuenciaDestino = -1) SET @SecuenciaDestino = NULL

  UPDATE SMU_TrazaViaje
  SET
	  NroTransporte = @NroTransporte,
	  Operacion = @Operacion,
	  PatenteTracto = @PatenteTracto,
	  PatenteTrailer = @PatenteTrailer,
	  RutTransportista = @RutTransportista,
	  NombreTransportista = @NombreTransportista,
	  RutConductor = @RutConductor,
	  NombreConductor = @NombreConductor,
	  OrigenCodigo = @OrigenCodigo,
	  OrigenDescripcion = @OrigenDescripcion,
	  FHSalidaOrigen = @FHSalidaOrigen,
	  LocalDestino = @LocalDestino,
	  DestinoDescripcion = @DestinoDescripcion,
	  FHLlegadaDestino = @FHLlegadaDestino,
	  FHAperturaPuerta = @FHAperturaPuerta,
	  FHSalidaDestino = @FHSalidaDestino,
	  TipoViaje = @TipoViaje,
	  ProveedorGPS = @ProveedorGPS,
	  DestinoDomicilio = @DestinoDomicilio,
	  DestinoRegion = @DestinoRegion,
	  DestinoDescripcionRegion = @DestinoDescripcionRegion,
	  DestinoComuna = @DestinoComuna,
	  DestinoLat = @DestinoLat,
	  DestinoLon = @DestinoLon,
	  EstadoViaje = @EstadoViaje,
	  SecuenciaDestino = @SecuenciaDestino,
	  RamplaProveedorGPS = @RamplaProveedorGPS,
	  EstadoPuerta = @EstadoPuerta,
	  EstadoLat = @EstadoLat,
	  EstadoLon = @EstadoLon,
	  TempSalidaCD = @TempSalidaCD,
	  FechaHoraActualizacion = @FechaHoraActualizacion,
	  IngresoSCAT = @IngresoSCAT
  WHERE
    Id = @Id

END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_TrazaViaje_GuardarNuevo')
BEGIN
  PRINT  'Dropping Procedure spu_TrazaViaje_GuardarNuevo'
  DROP  Procedure  dbo.spu_TrazaViaje_GuardarNuevo
END

GO

PRINT  'Creating Procedure spu_TrazaViaje_GuardarNuevo'
GO

CREATE PROCEDURE [dbo].[spu_TrazaViaje_GuardarNuevo]
/******************************************************************************
**    Descripcion  : Agrega un nuevo registro a la tabla TrazaViaje
**    Autor        : VSR
**    Fecha        : 23/05/2014
*******************************************************************************/
@Id AS INT OUTPUT,
@NroTransporte AS INT,
@Operacion AS INT,
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255),
@RutTransportista AS VARCHAR(255),
@NombreTransportista AS VARCHAR(255),
@RutConductor AS VARCHAR(255),
@NombreConductor AS VARCHAR(255),
@OrigenCodigo AS INT,
@OrigenDescripcion AS VARCHAR(255),
@FHSalidaOrigen AS DATETIME,
@LocalDestino AS INT,
@DestinoDescripcion AS VARCHAR(255),
@FHLlegadaDestino AS DATETIME,
@FHAperturaPuerta AS DATETIME,
@FHSalidaDestino AS DATETIME,
@TipoViaje AS VARCHAR(255),
@ProveedorGPS AS VARCHAR(255),
@DestinoDomicilio AS VARCHAR(255),
@DestinoRegion AS VARCHAR(255),
@DestinoDescripcionRegion AS VARCHAR(255),
@DestinoComuna AS VARCHAR(255),
@DestinoLat AS VARCHAR(255),
@DestinoLon AS VARCHAR(255),
@EstadoViaje AS VARCHAR(255),
@SecuenciaDestino AS INT,
@RamplaProveedorGPS AS VARCHAR(255),
@EstadoPuerta AS VARCHAR(255),
@EstadoLat AS VARCHAR(255),
@EstadoLon AS VARCHAR(255),
@TempSalidaCD AS VARCHAR(255),
@FechaHoraCreacion AS DATETIME,
@FechaHoraActualizacion AS DATETIME,
@IngresoSCAT AS BIT
AS
BEGIN

  IF (@NroTransporte = -1) SET @NroTransporte = NULL
  IF (@Operacion = -1) SET @Operacion = NULL
  IF (@OrigenCodigo = -1) SET @OrigenCodigo = NULL
  IF (@LocalDestino = -1) SET @LocalDestino = NULL
  IF (@SecuenciaDestino = -1) SET @SecuenciaDestino = NULL

  INSERT INTO SMU_TrazaViaje
  (
	  NroTransporte,
	  Operacion,
	  PatenteTracto,
	  PatenteTrailer,
	  RutTransportista,
	  NombreTransportista,
	  RutConductor,
	  NombreConductor,
	  OrigenCodigo,
	  OrigenDescripcion,
	  FHSalidaOrigen,
	  LocalDestino,
	  DestinoDescripcion,
	  FHLlegadaDestino,
	  FHAperturaPuerta,
	  FHSalidaDestino,
	  TipoViaje,
	  ProveedorGPS,
	  DestinoDomicilio,
	  DestinoRegion,
	  DestinoDescripcionRegion,
	  DestinoComuna,
	  DestinoLat,
	  DestinoLon,
	  EstadoViaje,
	  SecuenciaDestino,
	  RamplaProveedorGPS,
	  EstadoPuerta,
	  EstadoLat,
	  EstadoLon,
	  TempSalidaCD,
	  FechaHoraCreacion,
	  FechaHoraActualizacion,
	  IngresoSCAT
  )
  VALUES
  (
	  @NroTransporte,
	  @Operacion,
	  @PatenteTracto,
	  @PatenteTrailer,
	  @RutTransportista,
	  @NombreTransportista,
	  @RutConductor,
	  @NombreConductor,
	  @OrigenCodigo,
	  @OrigenDescripcion,
	  @FHSalidaOrigen,
	  @LocalDestino,
	  @DestinoDescripcion,
	  @FHLlegadaDestino,
	  @FHAperturaPuerta,
	  @FHSalidaDestino,
	  @TipoViaje,
	  @ProveedorGPS,
	  @DestinoDomicilio,
	  @DestinoRegion,
	  @DestinoDescripcionRegion,
	  @DestinoComuna,
	  @DestinoLat,
	  @DestinoLon,
	  @EstadoViaje,
	  @SecuenciaDestino,
	  @RamplaProveedorGPS,
	  @EstadoPuerta,
	  @EstadoLat,
	  @EstadoLon,
	  @TempSalidaCD,
	  @FechaHoraCreacion,
	  @FechaHoraActualizacion,
	  @IngresoSCAT
  )

  SET @Id = SCOPE_IDENTITY()

END
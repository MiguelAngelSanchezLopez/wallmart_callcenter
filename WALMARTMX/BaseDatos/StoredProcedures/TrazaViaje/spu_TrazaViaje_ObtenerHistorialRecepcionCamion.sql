﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_TrazaViaje_ObtenerHistorialRecepcionCamion')
BEGIN
  PRINT  'Dropping Procedure spu_TrazaViaje_ObtenerHistorialRecepcionCamion'
  DROP  Procedure  dbo.spu_TrazaViaje_ObtenerHistorialRecepcionCamion
END

GO

PRINT  'Creating Procedure spu_TrazaViaje_ObtenerHistorialRecepcionCamion'
GO
CREATE Procedure dbo.spu_TrazaViaje_ObtenerHistorialRecepcionCamion
/******************************************************************************
**    Descripcion  : obtiene historial de recepcion del camion
**    Por          : VSR, 17/10/2014
*******************************************************************************/
@NroTransporte AS INT,
@PatenteTracto AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
	  IdHistorialRecepcionCamion = HRC.Id,
	  NroTransporte = HRC.NroTransporte,
	  PatenteTracto = HRC.PatenteTracto,
	  Estado = ISNULL(HRC.Estado,''),
	  Observacion = ISNULL(HRC.Observacion,''),
	  CreadoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
	  FechaCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(HRC.FechaCreacion, 'FECHA_COMPLETA')
  FROM
	  HistorialRecepcionCamion AS HRC
    INNER JOIN Usuario AS U ON (HRC.IdUsuarioCreacion = U.Id)
  WHERE
      HRC.NroTransporte = @NroTransporte
  AND HRC.PatenteTracto = @PatenteTracto
  ORDER BY
    HRC.FechaCreacion DESC

END

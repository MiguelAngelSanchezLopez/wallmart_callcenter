﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_TiempoConexionPorEjecutivo')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_TiempoConexionPorEjecutivo'
  DROP  Procedure  dbo.spu_Reporte_TiempoConexionPorEjecutivo
END

GO

PRINT  'Creating Procedure spu_Reporte_TiempoConexionPorEjecutivo'
GO
CREATE Procedure dbo.spu_Reporte_TiempoConexionPorEjecutivo
/******************************************************************************
**    Descripcion  : obtiene total de alertas por ejecutivo
**    Por          : VSR, 25/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  DECLARE @IdPerfilTeleoperador INT
  SET @IdPerfilTeleoperador = 3

  ------------------------------------------------------------------------------------------
  -- obtiene los log relacionados con la sesion
  ------------------------------------------------------------------------------------------
  DECLARE @TablaSesion AS TABLE(IdLogSesion INT, IdUsuario INT, NombreUsuario VARCHAR(255), Accion VARCHAR(255), Fecha DATETIME)
  INSERT @TablaSesion
  (
	  IdLogSesion,
	  IdUsuario,
	  NombreUsuario,
	  Accion,
	  Fecha
  )
  SELECT
    IdLogSesion = LS.Id,
    IdUsuario = U.Id,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Paterno,''),
    Accion = LS.Accion,
    Fecha = LS.Fecha
  FROM
    LogSesion AS LS
    INNER MERGE JOIN Usuario AS U ON (LS.IdUsuario = U.Id)
  WHERE
      U.IdPerfil = @IdPerfilTeleoperador
  AND (Accion = 'Inicia sesión' OR Accion = 'Cierra sesión')
  AND LS.Fecha BETWEEN @FechaDesde AND @FechaHasta
  ORDER BY
    IdLogSesion

  ------------------------------------------------------------------------------------------
  -- obtiene los log relacionados con los auxiliares
  ------------------------------------------------------------------------------------------
  DECLARE @TablaAuxiliares AS TABLE(IdLogSesion INT, IdUsuario INT, NombreUsuario VARCHAR(255), Accion VARCHAR(255), Fecha DATETIME)
  INSERT @TablaAuxiliares
  (
	  IdLogSesion,
	  IdUsuario,
	  NombreUsuario,
	  Accion,
	  Fecha
  )
  SELECT
    IdLogSesion = LS.Id,
    IdUsuario = U.Id,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Paterno,''),
    Accion = LS.Accion,
    Fecha = LS.Fecha
  FROM
    LogSesion AS LS
    INNER JOIN Usuario AS U ON (LS.IdUsuario = U.Id)
  WHERE
      U.IdPerfil = @IdPerfilTeleoperador
  AND Accion LIKE '%auxiliar%'
  AND LS.Fecha BETWEEN @FechaDesde AND @FechaHasta
  ORDER BY
    IdLogSesion

  ------------------------------------------------------------------------------------------
  -- TABLA 0: devuelve los registros ordernados por usuario y fecha de inicio de accion
  ------------------------------------------------------------------------------------------
  SELECT
    T.*
  FROM (
          ------------------------------------------------------------------------------------------
          -- obtiene los tiempos de finalizacion para los inicio de sesion
          ------------------------------------------------------------------------------------------
          SELECT
            [EJECUTIVO] = T.NombreUsuario,
            [ACCION] = 'SESION' + CASE 
                                    WHEN T.AccionTermino = 'Inicia sesión' THEN ' (No finaliza sesión correctamente)' 
                                    WHEN T.AccionTermino IS NULL THEN ' (Sesión activa)'
                                    ELSE ''
                                  END,
            [FECHA INICIO] = REPLACE(CONVERT(VARCHAR, T.Fecha, 111),'/','-'),
            [HORA INICIO] = LEFT(CONVERT(VARCHAR, T.Fecha, 108),5) + ' hrs.',
            [FECHA TERMINO] = CASE WHEN (T.AccionTermino = 'Inicia sesión' OR T.AccionTermino IS NULL) THEN '' ELSE REPLACE(CONVERT(VARCHAR, T.FechaTermino, 111),'/','-') END,
            [HORA TERMINO] = CASE WHEN (T.AccionTermino = 'Inicia sesión' OR T.AccionTermino IS NULL) THEN '' ELSE LEFT(CONVERT(VARCHAR, T.FechaTermino, 108),5)  + ' hrs.' END,
            [MINUTOS CONEXION] = CASE WHEN (T.AccionTermino = 'Inicia sesión' OR T.AccionTermino IS NULL) THEN '' ELSE CONVERT(VARCHAR,DATEDIFF(mi, T.Fecha, T.FechaTermino)) END
          FROM (
                SELECT
                  TS.*,
                  AccionTermino = ( SELECT TOP 1 aux.Accion FROM @TablaSesion AS aux WHERE aux.IdUsuario = TS.IdUsuario AND aux.IdLogSesion > TS.IdLogSesion ORDER BY aux.IdLogSesion ),
                  FechaTermino = ( SELECT TOP 1 aux.Fecha FROM @TablaSesion AS aux WHERE aux.IdUsuario = TS.IdUsuario AND aux.IdLogSesion > TS.IdLogSesion ORDER BY aux.IdLogSesion )
                FROM 
                  @TablaSesion AS TS
                WHERE
                  TS.Accion = 'Inicia sesión'
          ) AS T

          ------------------------------------------------------------------------------------------
          -- obtiene los tiempos de finalizacion para los auxiliares
          ------------------------------------------------------------------------------------------
          UNION ALL
          SELECT
            [EJECUTIVO] = T.NombreUsuario,
            [ACCION] = REPLACE(REPLACE(T.Accion,'Estado auxiliar: TELEOPERADOR_',''),'_',' ') + 
                       CASE 
                         WHEN (T.AccionTermino LIKE 'Estado auxiliar: TELEOPERADOR_AUXILIAR%' OR T.AccionTermino IS NULL) THEN ' (No finaliza auxiliar correctamente)' 
                         ELSE ''
                       END,
            [FECHA INICIO] = REPLACE(CONVERT(VARCHAR, T.Fecha, 111),'/','-'),
            [HORA INICIO] = LEFT(CONVERT(VARCHAR, T.Fecha, 108),5) + ' hrs.',
            [FECHA TERMINO] = CASE WHEN (T.AccionTermino LIKE 'Estado auxiliar: TELEOPERADOR_AUXILIAR%' OR T.AccionTermino IS NULL) THEN '' ELSE REPLACE(CONVERT(VARCHAR, T.FechaTermino, 111),'/','-') END,
            [HORA TERMINO] = CASE WHEN (T.AccionTermino LIKE 'Estado auxiliar: TELEOPERADOR_AUXILIAR%' OR T.AccionTermino IS NULL) THEN '' ELSE LEFT(CONVERT(VARCHAR, T.FechaTermino, 108),5) + ' hrs.' END,
            [MINUTOS CONEXION] = CASE WHEN (T.AccionTermino LIKE 'Estado auxiliar: TELEOPERADOR_AUXILIAR%' OR T.AccionTermino IS NULL) THEN '' ELSE CONVERT(VARCHAR,DATEDIFF(mi, T.Fecha, T.FechaTermino)) END
          FROM (
                SELECT
                  TS.*,
                  AccionTermino = ( SELECT TOP 1 aux.Accion FROM @TablaAuxiliares AS aux WHERE aux.IdUsuario = TS.IdUsuario AND aux.IdLogSesion > TS.IdLogSesion ORDER BY aux.IdLogSesion ),
                  FechaTermino = ( SELECT TOP 1 aux.Fecha FROM @TablaAuxiliares AS aux WHERE aux.IdUsuario = TS.IdUsuario AND aux.IdLogSesion > TS.IdLogSesion ORDER BY aux.IdLogSesion )
                FROM 
                  @TablaAuxiliares AS TS
                WHERE
                  TS.Accion LIKE 'Estado auxiliar: TELEOPERADOR_AUXILIAR%'
          ) AS T
  ) AS T
  ORDER BY
    T.[EJECUTIVO], T.[FECHA INICIO] + T.[HORA INICIO]

END



   
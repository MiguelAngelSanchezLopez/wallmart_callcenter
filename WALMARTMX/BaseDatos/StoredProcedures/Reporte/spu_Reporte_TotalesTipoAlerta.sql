﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_TotalesTipoAlerta')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_TotalesTipoAlerta'
  DROP  Procedure  dbo.spu_Reporte_TotalesTipoAlerta
END

GO

PRINT  'Creating Procedure spu_Reporte_TotalesTipoAlerta'
GO
CREATE Procedure dbo.spu_Reporte_TotalesTipoAlerta
/******************************************************************************
**    Descripcion  : obtiene totales por tipo de alerta
**    Por          : VSR, 26/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  DECLARE @TablaTotales TABLE (NombreFormato VARCHAR(255), NombreAlerta VARCHAR(255), Total NUMERIC)

  --------------------------------------------------------------
  -- Alertas atendidas por el callcenter
  --------------------------------------------------------------
  INSERT INTO @TablaTotales(NombreFormato, NombreAlerta, Total)
  SELECT
    NombreFormato = AA.NombreFormato,
    NombreAlerta = AA.NombreAlerta,
    Total = COUNT(AA.IdAlerta)
  FROM
    vwu_AlertaAtendida AS AA
  WHERE
      ( AA.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( AA.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  GROUP BY
    AA.NombreFormato, AA.NombreAlerta

  --------------------------------------------------------------
  -- Alertas que estan en cola de atencion
  --------------------------------------------------------------
  INSERT INTO @TablaTotales(NombreFormato, NombreAlerta, Total)
  SELECT
    NombreFormato = AECA.NombreFormato,
    NombreAlerta = AECA.NombreAlerta,
    Total = COUNT(AECA.IdAlerta)
  FROM
    vwu_AlertaEnColaAtencion AECA
  WHERE
      ( AECA.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
  AND ( AECA.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  GROUP BY
    AECA.NombreFormato, AECA.NombreAlerta


  --------------------------------------------------------------
  -- TABLA 0: obtiene valores totales
  --------------------------------------------------------------
  SELECT
    [FORMATO] = T.NombreFormato,
    [TIPO ALERTA] = T.NombreAlerta,
    [TOTAL] = SUM(T.Total)
  FROM
    @TablaTotales AS T
  GROUP BY
    T.NombreFormato, T.NombreAlerta
  ORDER BY
    [FORMATO], [TIPO ALERTA]

END



   
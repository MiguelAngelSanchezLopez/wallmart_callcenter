﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_Programacion')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_Programacion'
  DROP  Procedure  dbo.spu_Reporte_Programacion
END

GO

PRINT  'Creating Procedure spu_Reporte_Programacion'
GO
CREATE Procedure dbo.spu_Reporte_Programacion
/******************************************************************************
**    Descripcion  : obtiene planificacion asociada al transportista
**    Por          : VSR, 03/06/2015
*******************************************************************************/
@IdUsuario  AS INT,
@NroTransporte AS INT,
@FechaPresentacion AS VARCHAR(255),
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255),
@Carga AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @FechaDesde VARCHAR(255), @FechaHasta VARCHAR(255)

  IF (@FechaPresentacion = '-1') BEGIN
    SET @FechaDesde = '01/01/2015'
    SET @FechaHasta = '31/12/2100'
  END ELSE BEGIN
    SET @FechaDesde = @FechaPresentacion
    SET @FechaHasta = @FechaPresentacion
  END

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  ---------------------------------------------------------------
  -- TABLA 0: obtiene planificacion por NroTransporte
  ---------------------------------------------------------------
  SELECT DISTINCT
	  [FECHA PRESENTACION] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(PT.FechaPresentacion, 'FECHA_COMPLETA'),
	  [LINEA DE TRANSPORTE] = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Materno,'') + ISNULL(' ' + U.Paterno,''),
	  [IDMASTER] = '''' + CONVERT(VARCHAR,PT.NroTransporte),
	  [TIENDA] = dbo.fnu_ObtenerListadoLocalesProgramacionTextoPlano(PT.IdPlanificacion, PT.NroTransporte),
	  [CARGA] = ISNULL(PT.Carga,''),
	  [TIPO CAMION] = ISNULL(PT.TipoCamion,''),
	  [TRACTO] = ISNULL(PT.PatenteTracto,''),
	  [REMOLQUE] = ISNULL(PT.PatenteTrailer,''),
	  [OPERADOR] = ISNULL(PT.NombreConductor,''),
    [RUT OPERADOR] = '''' + ISNULL(PT.RutConductor,''),
	  [CELULAR] = '''' + ISNULL(PT.Celular,''),
    [CERRADO POR LINEA DE TRANSPORTE] = CASE WHEN ISNULL(PT.CerradoPorTransportista, 0) = 1 THEN 'SI' ELSE 'NO' END,
    [ANDEN] = ISNULL(PT.Anden,''),
	  [CONFIRMACION ANDEN] = CASE WHEN ISNULL(PT.ConfirmacionAnden, 0) = 1 THEN 'SI' ELSE 'NO' END,
    [FECHA CONFIRMACION ANDEN] = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(PT.FechaConfirmacionAnden, 'FECHA_COMPLETA'),''),
    [USUARIO CONFIRMACION ANDEN] = ISNULL(UCA.Nombre,'') + ISNULL(' ' + UCA.Materno,'') + ISNULL(' ' + UCA.Paterno,''),
    [SELLO] = ISNULL(PT.Sello,''),
    [FECHA LECTURA SELLO] = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(PT.FechaLecturaSello, 'FECHA_COMPLETA'),''),
    [USUARIO LECTURA SELLO] = ISNULL(ULS.Nombre,'') + ISNULL(' ' + ULS.Materno,'') + ISNULL(' ' + ULS.Paterno,''),
    [BLOQUEADO POR FUERA HORARIO] = CASE WHEN ISNULL(PT.BloqueadoPorFueraHorario, 0) = 1 THEN 'SI' ELSE 'NO' END,
    [FECHA BLOQUEO FUERA HORARIO] = ISNULL(dbo.fnu_ConvertirDatetimeToDDMMYYYY(PT.FechaBloqueoFueraHorario, 'FECHA_COMPLETA'),''),
    [GESTION FINALIZADA] = CASE WHEN ISNULL(PT.Finalizada, 0) = 1 THEN 'SI' ELSE 'NO' END,
    [CRITERIO] = ISNULL(PT.criterio,''),
    [OBSERVACION] = ltrim(replace(replace(REPLACE(ISNULL(PT.Observacion,''), char(9), ' '),'''','"'),'´','')) --Limpiar caracteres invalidos
  FROM
	  PlanificacionTransportista AS PT
    INNER JOIN Local AS L ON (PT.IdLocal = L.Id)
    INNER JOIN Usuario AS U ON (PT.IdUsuarioTransportista = U.Id)
    LEFT JOIN Usuario AS UCA ON (PT.IdUsuarioConfirmacionAnden = UCA.Id)
    LEFT JOIN Usuario AS ULS ON (PT.IdUsuarioLecturaSello = ULS.Id)
  WHERE
      (@IdUsuario = -1 OR U.Id = @IdUsuario)
  AND (@NroTransporte = -1 OR PT.NroTransporte = @NroTransporte)
  AND (PT.FechaPresentacion BETWEEN @FechaDesde AND @FechaHasta)
  AND (@PatenteTracto = '-1' OR PT.PatenteTracto = @PatenteTracto)
  AND (@PatenteTrailer = '-1' OR PT.PatenteTrailer = @PatenteTrailer)
  AND (@Carga = '-1' OR PT.Carga = @Carga)
  ORDER BY 
    [FECHA PRESENTACION], [IDMASTER]

END
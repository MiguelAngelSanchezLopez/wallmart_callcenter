﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_ProductividadPorEjecutivo')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_ProductividadPorEjecutivo'
  DROP  Procedure  dbo.spu_Reporte_ProductividadPorEjecutivo
END

GO

PRINT  'Creating Procedure spu_Reporte_ProductividadPorEjecutivo'
GO
CREATE Procedure dbo.spu_Reporte_ProductividadPorEjecutivo
/******************************************************************************
**    Descripcion  : obtiene total de alertas por ejecutivo
**    Por          : VSR, 25/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  SELECT
    [FECHA GESTION] = T.FechaCreacion,
    [EJECUTIVO] = T.AtendidoPor,
    [TOTAL CONTACTADAS] = SUM(T.Contactada),
    [TOTAL SIN CONTACTO] = SUM(T.SinContacto),
    [TOTAL GESTIONADAS] = SUM(T.Contactada) + SUM(T.SinContacto)
  FROM (
    SELECT DISTINCT
      AtendidoPor = APE.AtendidoPor, 
      IdAlerta = APE.IdAlerta,
      Contactada = CASE WHEN APE.NoContesta = 0 THEN 1 ELSE 0 END,
      SinContacto = APE.NoContesta,
      FechaCreacion = REPLACE(CONVERT(VARCHAR, APE.FechaCreacion, 111),'/','-')
    FROM
      vwu_AlertaPorEjecutivo AS APE
    WHERE
        ( APE.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )
    AND ( APE.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad
  ) AS T
  GROUP BY
    T.AtendidoPor, T.FechaCreacion
  ORDER BY
    T.FechaCreacion, [EJECUTIVO]

END



   
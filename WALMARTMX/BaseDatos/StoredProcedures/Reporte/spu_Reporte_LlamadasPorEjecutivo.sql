﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_LlamadasPorEjecutivo')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_LlamadasPorEjecutivo'
  DROP  Procedure  dbo.spu_Reporte_LlamadasPorEjecutivo
END

GO

PRINT  'Creating Procedure spu_Reporte_LlamadasPorEjecutivo'
GO
CREATE Procedure dbo.spu_Reporte_LlamadasPorEjecutivo
/******************************************************************************
**    Descripcion  : obtiene total de llamadas realizadas por ejecutivo
**    Por          : VSR, 25/09/2014
*******************************************************************************/
@FechaDesde AS VARCHAR(255),
@FechaHasta AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'

  SET @FechaDesde = @FechaDesde + ' 00:00'
  SET @FechaHasta = @FechaHasta + ' 23:59'

  SELECT
    [ID ALERTA] = T.IdAlerta,
    [FORMATO] = T.NombreFormato,
    ALERTA = T.NombreAlerta,
    [IDMASTER] = T.NroTransporte,
    PRIORIDAD = T.Prioridad,
    [LINEA DE TRANSPORTE] = T.NombreTransportista,
    [TIENDA DESTINO] = ltrim(replace(replace(REPLACE(T.LocalDestino, char(9), ' '),'''','"'),'´','')), --Limpiar caracteres invalidos
    [DETERMINANTE DESTINO] = T.LocalDestinoCodigo,
    [ZONA] = T.Zona,
    [TIPO VIAJE] = T.TipoViaje,
    [FECHA ALERTA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaCreacion, 'FECHA_COMPLETA'),
    [ATENDIDO POR] = T.AtendidoPor,
    ESCALAMIENTO = CONVERT(VARCHAR,T.NroEscalamiento) + CASE WHEN T.Eliminado = 1 THEN ' (eliminado)' ELSE '' END,
    [CARGO] = T.GrupoContacto,
    [NOMBRE CONTACTO] = T.NombreContacto,
    [TELEFONO] = T.Telefono,
    [ACCION] = T.Accion,
    [FECHA LLAMADA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(T.FechaLLamada, 'FECHA_COMPLETA'),
    [ID LLAMADA] = T.IdLlamada
  FROM (
        SELECT DISTINCT
          IdAlerta = A.Id,
          NombreAlerta = A.DescripcionAlerta,
          NroTransporte = A.NroTransporte,
          Prioridad = AC.Prioridad,
          NombreTransportista = ISNULL(A.NombreTransportista,''),
          LocalDestino = ISNULL(HLLCC.DestinoLocal,''),
          LocalDestinoCodigo = ISNULL(CONVERT(VARCHAR,HLLCC.DestinoLocalCodigo),''),
          IdFormato = L.IdFormato,
          NombreFormato = ISNULL(F.Nombre,''),
          Zona = ISNULL(A.Permiso,''),
          TipoViaje = ISNULL(TV.TipoViaje,''),
          FechaCreacion = A.FechaHoraCreacion,
          AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
          NroEscalamiento = CONVERT(VARCHAR,EPA.Orden),
          GrupoContacto = EPAGC.Nombre,
          NombreContacto = HLLCC.NombreContacto,
          Telefono = HLLCC.Telefono,
          Accion = HLLCC.Accion,
          FechaLlamada = HLLCC.FechaCreacion,
          IdLlamada = ISNULL(HLLCC.IdLlamada,''),
          Eliminado = ISNULL(CONVERT(INT, EPA.Eliminado),0)
        FROM
          CallCenterHistorialLlamadaEjecutivo AS HLLCC
          INNER MERGE JOIN CallCenterAlerta AS CCA ON (HLLCC.IdAlertaPadre = CCA.IdAlerta)
          INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
          INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
          INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
          INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
          INNER MERGE JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
          INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
          INNER JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (HLLCC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
          INNER JOIN EscalamientoPorAlerta AS EPA ON (EPAGC.IdEscalamientoPorAlerta = EPA.Id)
          INNER JOIN Usuario AS U ON (HLLCC.IdUsuarioCreacion = U.Id)
          LEFT JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
        WHERE
          CCA.Desactivada = 0
  ) AS T
  WHERE
    T.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta
  ORDER BY
    [ID ALERTA], [IDMASTER], [ESCALAMIENTO], T.FechaLlamada

END
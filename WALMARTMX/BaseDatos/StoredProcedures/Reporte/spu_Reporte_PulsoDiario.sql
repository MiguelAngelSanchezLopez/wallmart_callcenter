﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_PulsoDiario')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_PulsoDiario'
  DROP  Procedure  dbo.spu_Reporte_PulsoDiario
END

GO

PRINT  'Creating Procedure spu_Reporte_PulsoDiario'
GO
CREATE Procedure dbo.spu_Reporte_PulsoDiario
/******************************************************************************  
**    Descripcion  : obtiene total de alertas que ingresan al sistema para ser atendidas y las que ya se han gestionado  
**    Por          : VSR, 25/09/2014  
*******************************************************************************/  
@FechaDesde AS VARCHAR(255),  
@FechaHasta AS VARCHAR(255)  
AS  
BEGIN  
	  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	  SET NOCOUNT ON  
	  
	  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'  
	  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'  
	  
	  SET @FechaDesde = @FechaDesde + ' 00:00'  
	  SET @FechaHasta = @FechaHasta + ' 23:59'  
  
	SELECT 
		[IDMASTER] = a.NroTransporte
		,ESTADO = a.Estado
		,[FECHA CORTA] = CONVERT(CHAR(10),FechaModificacion,103)
		,HORA = CONVERT(CHAR(5), FechaModificacion, 108)
		,[ESTADO LINEA DE TRANSPORTE] = '' --a.estadotransportista
		,[OBSERVACION ESTADO] = a.observacionestado 
		,[LINEA DE TRANSPORTE] = (SELECT TOP 1 t.NombreTransportista FROM trazaviaje t WHERE t.NroTransporte= a.NroTransporte)
		,[MONTO DISCREPANCIA] = ISNULL((SELECT SUM(CONVERT(NUMERIC, D.ImporteML)) FROM Discrepancia AS D WHERE D.NroTransporte = a.NroTransporte),0)
		,[NOMBRE OPERADOR] =(SELECT DISTINCT t1.NombreConductor FROM trazaviaje t1 WHERE t1.NroTransporte= a.NroTransporte )
		,[RUT OPERADOR] =(SELECT DISTINCT t2.RutConductor FROM trazaviaje t2 WHERE t2.NroTransporte= a.NroTransporte )
	FROM alertarojaestadoactual a
	WHERE a.estado in('ENVIADO TRANSPORTISTA','ENVIADO OPERACION TRANSPORTE','FINALIZADO')
	AND a.FechaModificacion BETWEEN @FechaDesde AND @FechaHasta  
	ORDER BY [MONTO DISCREPANCIA] DESC

END  
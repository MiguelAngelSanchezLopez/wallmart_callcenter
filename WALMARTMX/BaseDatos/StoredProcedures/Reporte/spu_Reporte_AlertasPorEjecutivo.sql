﻿alter Procedure dbo.spu_Reporte_AlertasPorEjecutivo  
/******************************************************************************  
**    Descripcion  : obtiene total de alertas por ejecutivo  
**    Por          : VSR, 25/09/2014  
spu_Reporte_AlertasPorEjecutivo '31/01/2018','31/01/2018'
*******************************************************************************/  
@FechaDesde AS VARCHAR(255),  
@FechaHasta AS VARCHAR(255)  
AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  IF (@FechaDesde = '-1') SET @FechaDesde = '01/01/2014'  
  IF (@FechaHasta = '-1') SET @FechaHasta = '31/12/2100'  
  
  SET @FechaDesde = @FechaDesde + ' 00:00'  
  SET @FechaHasta = @FechaHasta + ' 23:59'  
  
  SELECT  
    [ID ALERTA] = APE.IdAlerta,  
    [FORMATO] = APE.NombreFormato,  
    ALERTA = APE.NombreAlerta,  
    IDMASTER = APE.NroTransporte,   
    IDEMBARQUE = APE.IdEmbarque,  
    PRIORIDAD = APE.Prioridad,   
    [LINEA DE TRANSPORTE] = APE.NombreTransportista,  
    [TIENDA DESTINO] = ltrim(replace(replace(REPLACE(APE.LocalDestino, char(9), ' '),'''','"'),'´','')), --Limpiar caracteres invalidos
    [DETERMINANTE DESTINO] = APE.LocalDestinoCodigo,  
    ZONA = APE.Zona,   
    [TIPO VIAJE] = APE.TipoViaje,   
    [ATENDIDO POR] = APE.AtendidoPor,   
    [FECHA] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(APE.FechaCreacion, 'FECHA_COMPLETA'),  
    ESCALAMIENTO = CONVERT(VARCHAR,APE.NroEscalamiento) + CASE WHEN APE.Eliminado = 1 THEN ' (eliminado)' ELSE '' END,  
    [FECHA INICIO GESTION] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(APE.FechaInicioGestion,'FECHA_COMPLETA'),  
    [FECHA TERMINO GESTION] = dbo.fnu_ConvertirDatetimeToDDMMYYYY(APE.FechaTerminoGestion,'FECHA_COMPLETA'),  
    [MINUTOS GESTION] = APE.MinutosGestion  
  FROM  
    vwu_AlertaPorEjecutivo AS APE  
  WHERE  
      ( APE.FechaCreacion BETWEEN @FechaDesde AND @FechaHasta )  
  AND ( APE.GestionarPorCemtra = 0 ) -- solo se muestra las de Visibilidad  
  ORDER BY  
    [ID ALERTA], [IDMASTER], [ESCALAMIENTO]  
  
END  
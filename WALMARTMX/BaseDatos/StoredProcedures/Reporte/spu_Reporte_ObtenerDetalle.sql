﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Reporte_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Reporte_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Reporte_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Reporte_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Reporte_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle del reporte consultado
**    Por          : VSR, 22/09/2014
*******************************************************************************/
@IdReporte AS INT = -1,
@LLave AS VARCHAR(255) = ''
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  /*****************************************************************************
  * TABLA 0: DETALLE
  *****************************************************************************/
  SELECT
	  IdReporte = R.Id,
	  NombreReporte = ISNULL(R.Nombre,''),
    Llave = ISNULL(R.Llave,''),
    FormatoDocumento = ISNULL(R.FormatoDocumento,''),
    ProcedimientoAlmacenado = ISNULL(R.ProcedimientoAlmacenado,'')
  FROM
    Reporte AS R
  WHERE
    (@IdReporte != -1 AND R.Id = @IdReporte)
	OR (@LLave != '' AND R.Llave = @LLave)
END



   
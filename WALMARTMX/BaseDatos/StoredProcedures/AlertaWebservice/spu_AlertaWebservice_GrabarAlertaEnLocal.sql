﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_GrabarAlertaEnLocal')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_GrabarAlertaEnLocal'
  DROP  Procedure  dbo.spu_AlertaWebservice_GrabarAlertaEnLocal
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_GrabarAlertaEnLocal'
GO

CREATE PROCEDURE [dbo].[spu_AlertaWebservice_GrabarAlertaEnLocal]
/******************************************************************************
**    Descripcion  : graba la alerta en tabla AlertaEnLocal para indicar que ya no se siga gestionando las del mismo tipo
**    Autor        : VSR
**    Fecha        : 22/05/2015
*******************************************************************************/
@NroTransporte AS INT,
@LocalDestino AS INT
AS
BEGIN

  BEGIN TRANSACTION

    INSERT INTO AlertaEnLocal
    (
	    NroTransporte,
	    DescripcionAlerta,
	    LocalDestino,
	    UltimaOcurrencia,
	    IdAlertaPadre,
      GrupoAlerta
    )
    SELECT
      NroTransporte,
      DescripcionAlerta,
      LocalDestino,
      MAX(Ocurrencia),
      MIN(Id),
      GrupoAlerta
    FROM
      AlertaWebservice
    WHERE
        NroTransporte = @NroTransporte
    AND LocalDestino = @LocalDestino
    GROUP BY
      NroTransporte, DescripcionAlerta, LocalDestino, GrupoAlerta

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT
END
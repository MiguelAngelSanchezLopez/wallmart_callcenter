﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_ObtenerDatosEspeciales')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_ObtenerDatosEspeciales'
  DROP  Procedure  dbo.spu_AlertaWebservice_ObtenerDatosEspeciales
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_ObtenerDatosEspeciales'
GO

CREATE PROCEDURE [dbo].[spu_AlertaWebservice_ObtenerDatosEspeciales]
/******************************************************************************
**    Descripcion  : obtiene datos especiales de la alerta
**    Autor        : VSR
**    Fecha        : 18/05/2015
*******************************************************************************/
@NroTransporte AS INT,
@DescripcionAlerta AS VARCHAR(255),
@LocalDestino AS INT,
@GrupoAlerta AS VARCHAR(255),
@Ocurrencia AS INT
AS
BEGIN

  DECLARE @IdAlertaEstaEnLocal INT, @AlertaEstaEnLocal AS BIT, @IdAlertaPadre AS INT, @FocoAlto AS BIT
  DECLARE @NombreFormato AS VARCHAR(255), @LlaveFormato AS VARCHAR(255), @ViajeEstaEnLocal AS BIT, @totalRegistrosEncontrados INT

  ---------------------------------------------------
  -- obtiene nombre y llave del formato
  SELECT
    @NombreFormato = F.Nombre,
    @LlaveFormato = F.Llave,
    @FocoAlto = L.FocoAlto
  FROM
    Local AS L
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
  WHERE
    L.CodigoInterno = @LocalDestino

  -- setea valores nulos
  IF (@NombreFormato IS NULL) SET @NombreFormato = ''
  IF (@LlaveFormato IS NULL) SET @LlaveFormato = ''
    
  ---------------------------------------------------
  -- consulta si la alerta esta en local
  SELECT
    @IdAlertaEstaEnLocal = Id,
    @IdAlertaPadre = IdAlertaPadre
  FROM
    AlertaEnLocal
  WHERE
      NroTransporte = @NroTransporte
  AND DescripcionAlerta = @DescripcionAlerta
  AND LocalDestino = @LocalDestino
  AND GrupoAlerta = @GrupoAlerta

  -- setea valores nulos
  IF (@IdAlertaEstaEnLocal IS NULL) SET @IdAlertaEstaEnLocal = -1


  ---------------------------------------------------
  -- consulta si el viaje esta en local
  SELECT
    @totalRegistrosEncontrados = COUNT(Id)
  FROM
    AlertaEnLocal
  WHERE
      NroTransporte = @NroTransporte
  AND LocalDestino = @LocalDestino

  IF (@DescripcionAlerta = 'LOGISTICA INVERSA' OR @DescripcionAlerta = 'RECEPCION CON PROBLEMAS') BEGIN
    SET @ViajeEstaEnLocal = 0
  END ELSE BEGIN
    IF (@totalRegistrosEncontrados = 0) BEGIN
      SET @ViajeEstaEnLocal = 0
    END ELSE BEGIN
      SET @ViajeEstaEnLocal = 1
    END
  END

  ----------------------------------------------------------------------------------
  -- si la alerta esta en local lo marca
  ----------------------------------------------------------------------------------
  IF (@IdAlertaEstaEnLocal <> -1) BEGIN
    SET @AlertaEstaEnLocal = 1

    -- actualiza en la tabla la ultima ocurrencia
    UPDATE AlertaEnLocal SET UltimaOcurrencia = @Ocurrencia WHERE Id = @IdAlertaEstaEnLocal
  END ELSE BEGIN
  ----------------------------------------------------------------------------------
  -- si no esta en local entonces calcula la ultima ocurrencia que esta en la tabla AlertaWebservice
  ----------------------------------------------------------------------------------
    SET @AlertaEstaEnLocal = 0

    SELECT
      @IdAlertaPadre = MIN(Id)
    FROM
      AlertaWebservice
    WHERE
        NroTransporte = @NroTransporte
    AND DescripcionAlerta = @DescripcionAlerta
    AND LocalDestino = @LocalDestino
    AND GrupoAlerta = @GrupoAlerta

    -- setea valores nulos
    IF (@IdAlertaPadre IS NULL) SET @IdAlertaPadre = -1
  END


  --------------------------------------------
  -- TABLA 0: devuelve los valores
  --------------------------------------------
  SELECT
    AlertaEstaEnLocal = @AlertaEstaEnLocal,
    ViajeEstaEnLocal = @ViajeEstaEnLocal,
    IdAlertaPadre = @IdAlertaPadre,
    NombreFormato = @NombreFormato,
    LlaveFormato = @LlaveFormato,
    FocoAlto = @FocoAlto

END
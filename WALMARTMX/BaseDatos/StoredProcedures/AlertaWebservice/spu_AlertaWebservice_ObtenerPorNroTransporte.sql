﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_ObtenerPorNroTransporte')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_ObtenerPorNroTransporte'
  DROP  Procedure  dbo.spu_AlertaWebservice_ObtenerPorNroTransporte
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_ObtenerPorNroTransporte'
GO

CREATE Procedure [dbo].[spu_AlertaWebservice_ObtenerPorNroTransporte] (  
@NroTransporte as INT  
) AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    AlertaWebservice  
  WHERE   
    NroTransporte = @NroTransporte
  
END  
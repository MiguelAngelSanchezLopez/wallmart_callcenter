﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_GuardarNuevo')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_GuardarNuevo'
  DROP  Procedure  dbo.spu_AlertaWebservice_GuardarNuevo
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_GuardarNuevo'
GO
  
CREATE PROCEDURE [dbo].[spu_AlertaWebservice_GuardarNuevo]  
/******************************************************************************  
**    Descripcion  : Agrega un nuevo registro a la tabla AlertaWebservice  
**    Autor        : VSR  
**    Fecha        : 12/05/2015  
*******************************************************************************/  
@Id AS INT OUTPUT,  
@NroTransporte AS BIGINT,  
@IdEmbarque AS BIGINT,  
@DescripcionAlerta AS VARCHAR(255),  
@LocalDestino AS INT,  
@OrigenCodigo AS INT,  
@PatenteTracto AS VARCHAR(255),  
@PatenteTrailer AS VARCHAR(255),  
@RutTransportista AS VARCHAR(255),  
@NombreTransportista AS VARCHAR(255),  
@RutConductor AS VARCHAR(255),  
@NombreConductor AS VARCHAR(255),  
@FechaUltimaTransmision AS DATETIME,  
@FechaInicioAlerta AS DATETIME,  
@LatTrailer AS VARCHAR(255),  
@LonTrailer AS VARCHAR(255),  
@LatTracto AS VARCHAR(255),  
@LonTracto AS VARCHAR(255),  
@TipoAlerta AS VARCHAR(255),  
@Permiso AS VARCHAR(255),  
@Criticidad AS VARCHAR(255),  
@TipoAlertaDescripcion AS VARCHAR(255),  
@AlertaMapa AS VARCHAR(255),  
@NombreZona AS VARCHAR(255),  
@Velocidad AS VARCHAR(255),  
@EstadoGPSTracto AS VARCHAR(255),  
@EstadoGPSRampla AS VARCHAR(255),  
@EstadoGPS AS VARCHAR(255),  
@TipoPunto AS VARCHAR(255),  
@Temp1 AS VARCHAR(255),  
@Temp2 AS VARCHAR(255),  
@DistanciaTT AS VARCHAR(255),  
@TransportistaTrailer AS VARCHAR(255),  
@CantidadSatelites AS INT,  
@FechaHoraCreacion AS DATETIME,  
@FechaHoraActualizacion AS DATETIME,  
@EnvioCorreo AS BIT,  
@FueraDeHorario AS BIT,  
@Activo AS BIT,  
@ConLog AS BIT,  
@Ocurrencia AS INT,  
@GrupoAlerta AS VARCHAR(255)  
AS  
BEGIN  
  
  IF (@NroTransporte = -1) SET @NroTransporte = NULL  
  IF (@IdEmbarque = -1) SET @IdEmbarque = NULL  
  IF (@OrigenCodigo = -1) SET @OrigenCodigo = NULL  
  IF (@LocalDestino = -1) SET @LocalDestino = NULL  
  IF (@CantidadSatelites = -1) SET @CantidadSatelites = NULL  
  IF (@Ocurrencia = -1) SET @Ocurrencia = NULL  
  
  INSERT INTO AlertaWebservice  
  (  
   NroTransporte,  
   IdEmbarque,  
   DescripcionAlerta,  
   LocalDestino,  
   OrigenCodigo,  
   PatenteTracto,  
   PatenteTrailer,  
   RutTransportista,  
   NombreTransportista,  
   RutConductor,  
   NombreConductor,  
   FechaUltimaTransmision,  
   FechaInicioAlerta,  
   LatTrailer,  
   LonTrailer,  
   LatTracto,  
   LonTracto,  
   TipoAlerta,  
   Permiso,  
   Criticidad,  
   TipoAlertaDescripcion,  
   AlertaMapa,  
   NombreZona,  
   Velocidad,  
   EstadoGPSTracto,  
   EstadoGPSRampla,  
   EstadoGPS,  
   TipoPunto,  
   Temp1,  
   Temp2,  
   DistanciaTT,  
   TransportistaTrailer,  
   CantidadSatelites,  
   FechaHoraCreacion,  
   FechaHoraActualizacion,  
   EnvioCorreo,  
   FueraDeHorario,  
   Activo,  
   ConLog,  
    Ocurrencia,  
    GrupoAlerta  
  )  
  VALUES  
  (  
   @NroTransporte,  
   @IdEmbarque,  
   @DescripcionAlerta,  
   @LocalDestino,  
   @OrigenCodigo,  
   @PatenteTracto,  
   @PatenteTrailer,  
   @RutTransportista,  
   @NombreTransportista,  
   @RutConductor,  
   @NombreConductor,  
   @FechaUltimaTransmision,  
   @FechaInicioAlerta,  
   @LatTrailer,  
   @LonTrailer,  
   @LatTracto,  
   @LonTracto,  
   @TipoAlerta,  
   @Permiso,  
   @Criticidad,  
   @TipoAlertaDescripcion,  
   @AlertaMapa,  
   @NombreZona,  
   @Velocidad,  
   @EstadoGPSTracto,  
   @EstadoGPSRampla,  
   @EstadoGPS,  
   @TipoPunto,  
   @Temp1,  
   @Temp2,  
   @DistanciaTT,  
   @TransportistaTrailer,  
   @CantidadSatelites,  
   @FechaHoraCreacion,  
   @FechaHoraActualizacion,  
   @EnvioCorreo,  
   @FueraDeHorario,  
   @Activo,  
   @ConLog,  
    @Ocurrencia,  
    @GrupoAlerta  
  )  
  
  SET @Id = SCOPE_IDENTITY()  
  
  UPDATE AlertaWebservice  
  SET  
    AlertaMapa = 'https://gpsmx.altotrack.com/ModuloMapa.aspx?ID=' + CONVERT(VARCHAR, @Id)  
  WHERE  
    Id = @Id  
  
END
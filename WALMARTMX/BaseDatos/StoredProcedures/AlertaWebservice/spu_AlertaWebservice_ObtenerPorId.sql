﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_ObtenerPorId'
  DROP  Procedure  dbo.spu_AlertaWebservice_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_ObtenerPorId'
GO

CREATE Procedure [dbo].[spu_AlertaWebservice_ObtenerPorId] (  
@Id as INT  
) AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    AlertaWebservice  
  WHERE   
    Id = @Id  
  
END  
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_GrabarAlertaCallCenter')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_GrabarAlertaCallCenter'
  DROP  Procedure  dbo.spu_AlertaWebservice_GrabarAlertaCallCenter
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_GrabarAlertaCallCenter'
GO

CREATE PROCEDURE [dbo].[spu_AlertaWebservice_GrabarAlertaCallCenter]
/******************************************************************************
**    Descripcion  : envia alerta para gestion via CallCenter
**    Autor        : VSR
**    Fecha        : 19/05/2015
*******************************************************************************/
@IdAlerta AS INT,
@NroTransporte AS BIGINT,
@DescripcionAlerta AS VARCHAR(255),
@LocalDestino AS INT,
@GrupoAlerta AS VARCHAR(255)
AS
BEGIN

  BEGIN TRANSACTION
    -------------------------------------------------------------
    -- graba los datos en la tabla Alerta
    -------------------------------------------------------------
    IF NOT EXISTS(SELECT Id FROM Alerta WHERE Id = @IdAlerta) BEGIN
      INSERT INTO Alerta SELECT * FROM AlertaWebservice WHERE Id = @IdAlerta
    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

    ---------------------------------------------------
    -- obtiene la alerta padre con los registros ingresados en la tabla CallCenterAlerta
    DECLARE @IdAlertaPadre INT
    SELECT
      @IdAlertaPadre = MIN(A.Id)
    FROM
      CallCenterAlerta AS CCA
      INNER JOIN AlertaWebservice AS A ON (CCA.IdAlerta = A.Id)
    WHERE
        A.NroTransporte = @NroTransporte
    AND A.DescripcionAlerta = @DescripcionAlerta
    AND A.LocalDestino = @LocalDestino
    AND A.GrupoAlerta = @GrupoAlerta

    IF (@IdAlertaPadre IS NULL) SET @IdAlertaPadre = @IdAlerta


    -------------------------------------------------------------
    -- graba la asociacion de la alerta segun el canal de envio
    -------------------------------------------------------------
    INSERT INTO CallCenterAlerta
    (
	    IdAlerta,
	    IdAlertaPadre,
	    Cerrado,
	    CerradoSatisfactorio,
      Desactivada
    )
    VALUES
    (
	    @IdAlerta,
	    @IdAlertaPadre,
	    0,
	    0,
      0
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT

END
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaWebservice_GrabarAlertaSMS')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaWebservice_GrabarAlertaSMS'
  DROP  Procedure  dbo.spu_AlertaWebservice_GrabarAlertaSMS
END

GO

PRINT  'Creating Procedure spu_AlertaWebservice_GrabarAlertaSMS'
GO

CREATE PROCEDURE [dbo].[spu_AlertaWebservice_GrabarAlertaSMS]
/******************************************************************************
**    Descripcion  : envia alerta para gestion via SMS
**    Autor        : VSR
**    Fecha        : 19/05/2015
*******************************************************************************/
@IdAlerta AS INT,
@IdAlertaPadre AS INT
AS
BEGIN

  BEGIN TRANSACTION
    -------------------------------------------------------------
    -- graba los datos en la tabla Alerta
    -------------------------------------------------------------
    IF NOT EXISTS(SELECT Id FROM Alerta WHERE Id = @IdAlerta) BEGIN
      INSERT INTO Alerta SELECT * FROM AlertaWebservice WHERE Id = @IdAlerta
    END

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

    -------------------------------------------------------------
    -- graba la asociacion de la alerta segun el canal de envio
    -------------------------------------------------------------
    INSERT INTO SMSAlerta
    (
	    IdAlerta,
	    IdAlertaPadre,
	    Cerrado,
	    CerradoSatisfactorio
    )
    VALUES
    (
	    @IdAlerta,
	    @IdAlertaPadre,
	    0,
	    0
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT

END
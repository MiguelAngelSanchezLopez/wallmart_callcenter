﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaHistoria_Actualizar')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaHistoria_Actualizar'
  DROP  Procedure  dbo.spu_AlertaHistoria_Actualizar
END

GO

PRINT  'Creating Procedure spu_AlertaHistoria_Actualizar'
GO

CREATE PROCEDURE [dbo].[spu_AlertaHistoria_Actualizar]
/******************************************************************************
**    Descripcion  : actualiza registro de la tabla AlertaHistoria
**    Autor        : VSR
**    Fecha        : 22/05/2014
*******************************************************************************/
@Id AS INT,
@NroTransporte AS INT,
@DescripcionAlerta AS VARCHAR(255),
@LocalDestino AS INT,
@OrigenCodigo AS INT,
@PatenteTracto AS VARCHAR(255),
@PatenteTrailer AS VARCHAR(255),
@RutTransportista AS VARCHAR(255),
@NombreTransportista AS VARCHAR(255),
@RutConductor AS VARCHAR(255),
@NombreConductor AS VARCHAR(255),
@FechaUltimaTransmision AS DATETIME,
@FechaInicioAlerta AS DATETIME,
@LatTrailer AS VARCHAR(255),
@LonTrailer AS VARCHAR(255),
@LatTracto AS VARCHAR(255),
@LonTracto AS VARCHAR(255),
@TipoAlerta AS VARCHAR(255),
@Permiso AS VARCHAR(255),
@Criticidad AS VARCHAR(255),
@TipoAlertaDescripcion AS VARCHAR(255),
@AlertaMapa AS VARCHAR(255),
@NombreZona AS VARCHAR(255),
@Velocidad AS VARCHAR(255),
@EstadoGPSTracto AS VARCHAR(255),
@EstadoGPSRampla AS VARCHAR(255),
@EstadoGPS AS VARCHAR(255),
@TipoPunto AS VARCHAR(255),
@Temp1 AS VARCHAR(255),
@Temp2 AS VARCHAR(255),
@DistanciaTT AS VARCHAR(255),
@TransportistaTrailer AS VARCHAR(255),
@CantidadSatelites AS INT,
@FechaHoraCreacion AS DATETIME,
@FechaHoraActualizacion AS DATETIME,
@EnvioCorreo AS BIT,
@FueraDeHorario AS BIT,
@Activo AS BIT,
@ConLog AS BIT,
@Ocurrencia AS INT,
@GrupoAlerta AS VARCHAR(255)
AS
BEGIN

  IF (@NroTransporte = -1) SET @NroTransporte = NULL
  IF (@OrigenCodigo = -1) SET @OrigenCodigo = NULL
  IF (@LocalDestino = -1) SET @LocalDestino = NULL
  IF (@CantidadSatelites = -1) SET @CantidadSatelites = NULL
  IF (@Ocurrencia = -1) SET @Ocurrencia = NULL

  UPDATE AlertaHistoria
  SET
	  NroTransporte = @NroTransporte,
	  DescripcionAlerta = @DescripcionAlerta,
	  LocalDestino = @LocalDestino,
	  OrigenCodigo = @OrigenCodigo,
	  PatenteTracto = @PatenteTracto,
	  PatenteTrailer = @PatenteTrailer,
	  RutTransportista = @RutTransportista,
	  NombreTransportista = @NombreTransportista,
	  RutConductor = @RutConductor,
	  NombreConductor = @NombreConductor,
	  FechaUltimaTransmision = @FechaUltimaTransmision,
	  FechaInicioAlerta = @FechaInicioAlerta,
	  LatTrailer = @LatTrailer,
	  LonTrailer = @LonTrailer,
	  LatTracto = @LatTracto,
	  LonTracto = @LonTracto,
	  TipoAlerta = @TipoAlerta,
	  Permiso = @Permiso,
	  Criticidad = @Criticidad,
	  TipoAlertaDescripcion = @TipoAlertaDescripcion,
	  AlertaMapa = @AlertaMapa,
	  NombreZona = @NombreZona,
	  Velocidad = @Velocidad,
	  EstadoGPSTracto = @EstadoGPSTracto,
	  EstadoGPSRampla = @EstadoGPSRampla,
	  EstadoGPS = @EstadoGPS,
	  TipoPunto = @TipoPunto,
	  Temp1 = @Temp1,
	  Temp2 = @Temp2,
	  DistanciaTT = @DistanciaTT,
	  TransportistaTrailer = @TransportistaTrailer,
	  CantidadSatelites = @CantidadSatelites,
	  FechaHoraActualizacion = @FechaHoraActualizacion,
	  EnvioCorreo = @EnvioCorreo,
	  FueraDeHorario = @FueraDeHorario,
	  Activo = @Activo,
	  ConLog = @ConLog,
    Ocurrencia = @Ocurrencia,
    GrupoAlerta = @GrupoAlerta
  WHERE
    Id = @Id

END
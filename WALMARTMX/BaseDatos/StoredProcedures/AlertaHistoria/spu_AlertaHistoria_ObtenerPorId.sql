﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaHistoria_ObtenerPorId')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaHistoria_ObtenerPorId'
  DROP  Procedure  dbo.spu_AlertaHistoria_ObtenerPorId
END

GO

PRINT  'Creating Procedure spu_AlertaHistoria_ObtenerPorId'
GO

CREATE Procedure [dbo].[spu_AlertaHistoria_ObtenerPorId] (  
@Id as INT  
) AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    AlertaHistoria  
  WHERE   
    Id = @Id  
  
END  
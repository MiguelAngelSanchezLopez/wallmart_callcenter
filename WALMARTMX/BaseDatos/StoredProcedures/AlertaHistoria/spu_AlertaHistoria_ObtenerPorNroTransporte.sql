﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_AlertaHistoria_ObtenerPorNroTransporte')
BEGIN
  PRINT  'Dropping Procedure spu_AlertaHistoria_ObtenerPorNroTransporte'
  DROP  Procedure  dbo.spu_AlertaHistoria_ObtenerPorNroTransporte
END

GO

PRINT  'Creating Procedure spu_AlertaHistoria_ObtenerPorNroTransporte'
GO

CREATE Procedure [dbo].[spu_AlertaHistoria_ObtenerPorNroTransporte] (  
@NroTransporte as INT  
) AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  SELECT  
    *
  FROM   
    AlertaHistoria  
  WHERE   
    NroTransporte = @NroTransporte
  
END  
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_ObtenerUltimoLogSesion')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_ObtenerUltimoLogSesion'
  DROP  Procedure  dbo.spu_Sistema_ObtenerUltimoLogSesion
END

GO

PRINT  'Creating Procedure spu_Sistema_ObtenerUltimoLogSesion'
GO
CREATE Procedure dbo.spu_Sistema_ObtenerUltimoLogSesion
/******************************************************************************
**    Descripcion  : obtiene informacion del ultimo log del usuario
**    Por          : VSR, 03/11/2014
*******************************************************************************/
@IdUsuario AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  DECLARE @UltimaFecha DATETIME

  -- obtiene del log la ultima fecha
  SELECT
    @UltimaFecha = MAX(LS.Fecha)
  FROM
    LogSesion AS LS
  WHERE
    LS.IdUsuario = @IdUsuario

  -- obtiene la ultima accion asociada a la ultima fecha
  SELECT
    LS.Accion
  FROM
    LogSesion AS LS
  WHERE
      LS.IdUsuario = @IdUsuario
  AND LS.Fecha = @UltimaFecha

END



  
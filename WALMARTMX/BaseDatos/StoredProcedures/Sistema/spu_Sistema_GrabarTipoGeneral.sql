﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_GrabarTipoGeneral')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_GrabarTipoGeneral'
  DROP  Procedure  dbo.spu_Sistema_GrabarTipoGeneral
END

GO

PRINT  'Creating Procedure spu_Sistema_GrabarTipoGeneral'
GO
CREATE Procedure dbo.spu_Sistema_GrabarTipoGeneral
/******************************************************************************
**  Descripcion  : graba registro tipo general
**  Fecha        : 12/08/2014
*******************************************************************************/
@Status AS VARCHAR(255) OUTPUT,
@Nombre AS VARCHAR(1000),
@Tipo AS VARCHAR(1000),
@Extra1 AS VARCHAR(1000),
@Extra2 AS VARCHAR(1000)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    IF (@Extra1 = '-1') SET @Extra1 = NULL
    IF (@Extra2 = '-1') SET @Extra2 = NULL

    DECLARE @IdTipoGeneral INT
    SET @IdTipoGeneral = (SELECT TOP 1 Id FROM TipoGeneral WHERE Nombre = @Nombre AND Tipo = @Tipo)
    IF (@IdTipoGeneral IS NULL) SET @IdTipoGeneral = -1

    IF (@IdTipoGeneral = -1) BEGIN
      INSERT INTO TipoGeneral
      (
	      Nombre,
	      Valor,
	      Tipo,
	      Activo,
        Extra1,
        Extra2
      )
      VALUES
      (
	      @Nombre,
	      @Nombre,
	      @Tipo,
	      1,
        @Extra1,
        @Extra2
      )      

      SET @Status = 'ingresado'

      IF @@ERROR<>0 BEGIN
        ROLLBACK
        SET @Status = 'error'
        RETURN
      END
    END ELSE BEGIN
      SET @Status = 'duplicado'
    END

  COMMIT
END
GO        
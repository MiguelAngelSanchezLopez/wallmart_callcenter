﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_ObtenerCanalComunicacion')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_ObtenerCanalComunicacion'
  DROP  Procedure  dbo.spu_Sistema_ObtenerCanalComunicacion
END

GO

PRINT  'Creating Procedure spu_Sistema_ObtenerCanalComunicacion'
GO
CREATE Procedure dbo.spu_Sistema_ObtenerCanalComunicacion
/******************************************************************************
**    Descripcion  : obtiene los canales de comunicacion
**    Por          : VSR, 08/05/2015
*******************************************************************************/
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    Nombre = ISNULL(TG.Nombre,''),
    Valor = ISNULL(TG.Valor,'')
  FROM
    TipoGeneral AS TG
  WHERE
      TG.Tipo = 'CanalComunicacion'
  AND TG.Activo = 1
  ORDER BY
    Nombre

END



  
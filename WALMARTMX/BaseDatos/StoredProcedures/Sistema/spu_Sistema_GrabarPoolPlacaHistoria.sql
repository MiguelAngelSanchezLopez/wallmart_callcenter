﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_GrabarPoolPlacaHistoria')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_GrabarPoolPlacaHistoria'
  DROP  Procedure  dbo.spu_Sistema_GrabarPoolPlacaHistoria
END

GO

PRINT  'Creating Procedure spu_Sistema_GrabarPoolPlacaHistoria'
GO
CREATE Procedure dbo.spu_Sistema_GrabarPoolPlacaHistoria
/******************************************************************************
**  Descripcion  : graba los datos del pool de placas historia
**  Fecha        : 23/08/2017
*******************************************************************************/
@ListadoDeterminantes AS VARCHAR(8000),
@FechaCarga AS DATETIME
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION
    INSERT INTO PoolPlacaHistoria (
        Id  
      , Placa
      , TipoVehiculo
      , Transporte
      , Cedis
      , Determinante
      , IdUsuarioTransportista
      , IdCentroDistribucion
      , IdUsuarioCreacion
      , FechaCarga
      , NumeroEconomico
    )
    SELECT
        Id  
      , Placa
      , TipoVehiculo
      , Transporte
      , Cedis
      , Determinante
      , IdUsuarioTransportista
      , IdCentroDistribucion
      , IdUsuarioCreacion
      , FechaCarga
      , NumeroEconomico
    FROM
      PoolPlaca
    WHERE
        Determinante IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@ListadoDeterminantes))
    AND FechaCarga <> @FechaCarga

    -----------------------------------------------------
    -- elimina los registros
    DELETE FROM
      PoolPlaca
    WHERE
        Determinante IN (SELECT valor FROM dbo.fnu_InsertaListaTabla(@ListadoDeterminantes))
    AND FechaCarga <> @FechaCarga

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT
END
GO        
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Sistema_GrabarLogSesion')
BEGIN
  PRINT  'Dropping Procedure spu_Sistema_GrabarLogSesion'
  DROP  Procedure  dbo.spu_Sistema_GrabarLogSesion
END

GO

PRINT  'Creating Procedure spu_Sistema_GrabarLogSesion'
GO
CREATE Procedure dbo.spu_Sistema_GrabarLogSesion
/******************************************************************************
**  Descripcion  : graba la accion realizada por el usuario
**  Fecha        : 09/10/2014
*******************************************************************************/
@IdUsuario AS INT,
@Accion AS VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DECLARE @IdHistorial INT, @AccionAlerta VARCHAR(255)

    /*
    Se realiza esta validacion cuando un usuario marca como ultima gestion de alerta un estado auxiliar, y luego cierra sesion entonces
    se elimina la ultima gestion de la alerta para que no le muestre la ventana de auxiliar
    */
    IF (@Accion = 'Cierra sesión') BEGIN
        SELECT TOP 1
            @IdHistorial = Id,
            @AccionAlerta = Accion
        FROM
            CallCenterHistorialAlertaGestion AS HAG
        WHERE
            HAG.IdUsuarioCreacion = @IdUsuario
        ORDER BY
            HAG.Id DESC

        IF (@AccionAlerta IS NOT NULL) BEGIN
            IF (PATINDEX('TELEOPERADOR_AUXILIAR_%', @AccionAlerta) > 0) BEGIN
                DECLARE @Status INT
                EXEC spu_Alerta_EliminarUltimoHistorialGestionPorUsuario @Status OUTPUT, @IdUsuario
            END
        END
    END

    INSERT LogSesion
    (
	    IdUsuario,
	    Accion,
	    Fecha
    )
    VALUES
    (
	    @IdUsuario,
	    @Accion,
	    dbo.fnu_GETDATE()
    )

    IF @@ERROR<>0 BEGIN
      ROLLBACK
      RETURN
    END

  COMMIT
END
GO        
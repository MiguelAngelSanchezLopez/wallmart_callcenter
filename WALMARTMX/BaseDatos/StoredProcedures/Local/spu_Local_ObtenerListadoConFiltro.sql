﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Local_ObtenerListadoConFiltro')
BEGIN
  PRINT  'Dropping Procedure spu_Local_ObtenerListadoConFiltro'
  DROP  Procedure  dbo.spu_Local_ObtenerListadoConFiltro
END

GO

PRINT  'Creating Procedure spu_Local_ObtenerListadoConFiltro'
GO
CREATE Procedure dbo.spu_Local_ObtenerListadoConFiltro
/******************************************************************************
**    Descripcion  : Obtiene listado de locales usando filtros de busqueda
**    Por          : VSR, 04/09/2014
*******************************************************************************/
@IdUsuario AS INT,
@Nombre AS VARCHAR(255),
@CodigoInterno AS INT,
@IdFormato AS INT,
@Etapa AS INT,
@FocoAlto AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  SELECT
    IdLocal = L.Id,
    NombreLocal = ISNULL(L.Nombre,''),
    CodigoInterno = ISNULL(L.CodigoInterno,''),
	  IdFormato = F.Id,
	  NombreFormato = ISNULL(F.Nombre,''),
	  FechaCreacion = dbo.fnu_ConvertirDatetimeToDDMMYYYY(L.FechaCreacion,'FECHA_COMPLETA'),
    Activo = CASE WHEN ISNULL(L.Activo,0) = 1 THEN 'SI' ELSE 'NO' END,
    FocoAlto = CASE WHEN ISNULL(L.FocoAlto,0) = 1 THEN 'SI' ELSE 'NO' END,
    FocoTablet = CASE WHEN ISNULL(L.FocoTablet,0) = 1 THEN 'SI' ELSE 'NO' END,
    Etapa = CASE WHEN ISNULL(L.Etapa,-1) = -1 THEN '' ELSE CONVERT(VARCHAR,L.Etapa) END
  FROM
    Local AS L
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
  WHERE
      (@Nombre = '-1' OR L.Nombre LIKE '%'+ @Nombre +'%')
  AND (@CodigoInterno = -1 OR L.CodigoInterno LIKE '%'+ CONVERT(VARCHAR,@CodigoInterno) +'%')
  AND (@IdFormato = -1 OR L.IdFormato = @IdFormato)
  AND (@Etapa = -1 OR L.Etapa = @Etapa)
  AND (@FocoAlto = 0 OR L.FocoAlto = @FocoAlto)
  ORDER BY
    IdLocal DESC

END



  
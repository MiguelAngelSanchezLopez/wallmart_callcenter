﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Local_EsFoco')
BEGIN
  PRINT  'Dropping Procedure spu_Local_EsFoco'
  DROP  Procedure  dbo.spu_Local_EsFoco
END

GO

PRINT  'Creating Procedure spu_Local_EsFoco'
GO

CREATE Procedure [dbo].[spu_Local_EsFoco] (  
@Status AS BIT OUTPUT,
@CodigoInterno AS INT
) AS  
BEGIN  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
 SET NOCOUNT ON  

  SELECT
    @Status = CONVERT(INT,FocoAlto)
  FROM
    Local
  WHERE
    CodigoInterno = @CodigoInterno

  IF (@Status IS NULL) SET @Status = 0
      
END
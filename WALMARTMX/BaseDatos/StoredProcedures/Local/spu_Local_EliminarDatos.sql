﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Local_EliminarDatos')
BEGIN
  PRINT  'Dropping Procedure spu_Local_EliminarDatos'
  DROP  Procedure  dbo.spu_Local_EliminarDatos
END

GO

PRINT  'Creating Procedure spu_Local_EliminarDatos'
GO
CREATE Procedure dbo.spu_Local_EliminarDatos
/******************************************************************************
**  Descripcion  : elimina un local
**  Fecha        : 04/09/2009
*******************************************************************************/
@Status AS VARCHAR(50) OUTPUT,
@IdLocal AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  BEGIN TRANSACTION

    DELETE FROM LocalesPorUsuario WHERE IdLocal = @IdLocal
    DELETE FROM Local WHERE Id = @IdLocal

    -- retorna que todo estuvo bien
    SET @Status = 'eliminado'

    -- si hay error devuelve codigo
    IF @@ERROR<>0 BEGIN
      ROLLBACK
      SET @Status = 'error'
      RETURN
    END

  COMMIT
END
GO           
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Local_ObtenerDetalle')
BEGIN
  PRINT  'Dropping Procedure spu_Local_ObtenerDetalle'
  DROP  Procedure  dbo.spu_Local_ObtenerDetalle
END

GO

PRINT  'Creating Procedure spu_Local_ObtenerDetalle'
GO
CREATE Procedure dbo.spu_Local_ObtenerDetalle
/******************************************************************************
**    Descripcion  : Obtiene detalle del local consultado
**    Por          : VSR, 03/09/2014
*******************************************************************************/
@IdLocal AS INT
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  /*****************************************************************************
  * TABLA 0: DETALLE
  *****************************************************************************/
  SELECT
    IdLocal = L.Id,
    NombreLocal = ISNULL(L.Nombre,''),
    CodigoInterno = ISNULL(L.CodigoInterno,''),
	  IdFormato = F.Id,
	  NombreFormato = ISNULL(F.Nombre,''),
    Activo = CONVERT(INT, L.Activo),
    MaximoHorasAlertaActiva = ISNULL(L.MaximoHorasAlertaActiva,-1),
    Etapa = ISNULL(L.Etapa,-1),
    FocoAlto = CONVERT(INT, ISNULL(L.FocoAlto,0)),
    FocoTablet = CONVERT(INT, ISNULL(L.FocoTablet,0)),
    Telefono = REPLACE(REPLACE(ISNULL(L.Telefono,''),' ',''), ',', ', ')
  FROM
    Local AS L
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
  WHERE
    L.Id = @IdLocal
  ORDER BY
    IdLocal DESC


END



   
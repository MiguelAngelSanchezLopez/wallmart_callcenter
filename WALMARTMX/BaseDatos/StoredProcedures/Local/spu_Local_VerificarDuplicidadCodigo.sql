﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Local_VerificarDuplicidadCodigo')
BEGIN
  PRINT  'Dropping Procedure spu_Local_VerificarDuplicidadCodigo'
  DROP  Procedure  dbo.spu_Local_VerificarDuplicidadCodigo
END

GO

PRINT  'Creating Procedure spu_Local_VerificarDuplicidadCodigo'
GO
CREATE Procedure dbo.spu_Local_VerificarDuplicidadCodigo
/******************************************************************************
**  Descripcion  : valida que el codigo del local sea unico (retorno-> 1: No existe; -1: existe)
**  Fecha        : 04/09/2014
*******************************************************************************/
@Status AS INT OUTPUT,
@IdLocal AS INT,
@Valor AS VARCHAR(1028)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  IF(@IdLocal IS NULL OR @IdLocal = 0) SET @IdLocal = -1

  -- asume que el registro no existe
  SET @Status = 1

  -- verifica un registro cuando es nuevo
  IF(@IdLocal = -1) BEGIN
    IF( EXISTS(
                SELECT
                  L.CodigoInterno
                FROM
                  Local AS L
                WHERE
                    ISNULL(L.CodigoInterno,'-200') = @Valor
                )
        ) BEGIN
      SET @Status = -1
    END
  -- verifica un registro cuando ya existe y lo compara con otro distinto a el
  END ELSE BEGIN
    IF( EXISTS(
                SELECT
                  L.CodigoInterno
                FROM
                  Local AS L
                WHERE
                    ISNULL(L.CodigoInterno,'-200') = @Valor
                AND L.Id <> @IdLocal
                )
        ) BEGIN
      SET @Status = -1
    END
  END

  IF @@ERROR<>0 BEGIN
    ROLLBACK
    SET @Status = -200
    RETURN
  END

END
GO       
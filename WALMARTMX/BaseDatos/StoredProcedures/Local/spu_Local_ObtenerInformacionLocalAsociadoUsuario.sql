﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Local_ObtenerInformacionLocalAsociadoUsuario')
BEGIN
  PRINT  'Dropping Procedure spu_Local_ObtenerInformacionLocalAsociadoUsuario'
  DROP  Procedure  dbo.spu_Local_ObtenerInformacionLocalAsociadoUsuario
END
GO

PRINT  'Creating Procedure spu_Local_ObtenerInformacionLocalAsociadoUsuario'
GO

CREATE PROCEDURE [dbo].[spu_Local_ObtenerInformacionLocalAsociadoUsuario]
/******************************************************************************
**  Descripcion  : obtiene informacion del local si tiene alguno asociado
**  Por          : VSR, 16/06/2017
*******************************************************************************/  
@IdUsuario AS INT
AS  
BEGIN  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  SET NOCOUNT ON  
  
  ----------------------------------------------------------------------------------
  -- TABLA 0: obtiene datos del local si tiene alguno asociado
  ----------------------------------------------------------------------------------
  SELECT TOP 1
    NombreFormato = F.Nombre,
    LlaveFormato = F.Llave,
    NombreLocal = F.Nombre + ' - ' + L.Nombre,
    CodigoLocal = L.CodigoInterno
  FROM
    LocalesPorUsuario AS LPU
    INNER JOIN Local AS L ON (LPU.IdLocal = L.Id)
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
  WHERE
    LPU.IdUsuario = @IdUsuario
          
END


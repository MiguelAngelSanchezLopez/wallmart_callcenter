﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'spu_Local_ObtenerCargosPorFormato')
BEGIN
  PRINT  'Dropping Procedure spu_Local_ObtenerCargosPorFormato'
  DROP  Procedure  dbo.spu_Local_ObtenerCargosPorFormato
END

GO

PRINT  'Creating Procedure spu_Local_ObtenerCargosPorFormato'
GO
CREATE Procedure dbo.spu_Local_ObtenerCargosPorFormato
/******************************************************************************
**    Descripcion  : obtiene los cargos asociados al formato y local
**    Por          : VSR, 09/04/2015
*******************************************************************************/
@IdFormato INT,
@IdLocal INT,
@Categoria VARCHAR(255)
AS
BEGIN
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
  SET NOCOUNT ON

  ---------------------------------------------------------------
  -- obtiene todos los usuarios segun los perfiles asociados al formato
  ---------------------------------------------------------------
  DECLARE @TablaUsuarios AS TABLE(IdUsuario INT, NombreUsuario VARCHAR(255), IdPerfil INT, NombrePerfil VARCHAR(255), LlavePerfil VARCHAR(255), Seleccionado INT)
  INSERT INTO @TablaUsuarios
  (
	  IdUsuario,
	  NombreUsuario,
	  IdPerfil,
	  NombrePerfil,
	  LlavePerfil,
	  Seleccionado
  )
  SELECT
    IdUsuario = U.Id,
    NombreUsuario = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    IdPerfil = P.Id,
    NombrePerfil = ISNULL(P.Nombre,''),
    LlavePerfil = ISNULL(P.Llave,''),
    Seleccionado = CASE WHEN LPU.IdUsuario IS NOT NULL THEN 1 ELSE 0 END
  FROM
    PerfilesPorFormato AS PPF
    INNER JOIN Usuario AS U ON (PPF.IdPerfil = U.IdPerfil)
    INNER JOIN Perfil AS P ON (U.IdPerfil = P.Id)
    LEFT JOIN LocalesPorUsuario AS LPU ON (U.Id = LPU.IdUsuario AND LPU.IdLocal = @IdLocal)
  WHERE
      PPF.IdFormato = @IdFormato
  AND PPF.Categoria = @Categoria
  ORDER BY
    LlavePerfil, NombreUsuario

  ---------------------------------------------------------------
  -- TABLA 0: Perfiles  
  ---------------------------------------------------------------
  SELECT DISTINCT
	  T.IdPerfil,
	  T.NombrePerfil,
	  T.LlavePerfil
  FROM
    @TablaUsuarios AS T
  ORDER BY
    T.NombrePerfil

  ---------------------------------------------------------------
  -- TABLA 1: Listado Usuarios  
  ---------------------------------------------------------------
  SELECT
    T.*
  FROM
    @TablaUsuarios AS T
  ORDER BY
    T.LlavePerfil, T.NombreUsuario


END



   
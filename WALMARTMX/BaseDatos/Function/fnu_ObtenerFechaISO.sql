﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_ObtenerFechaISO' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_ObtenerFechaISO'
  DROP FUNCTION dbo.fnu_ObtenerFechaISO
END
GO

PRINT 'Creating Function fnu_ObtenerFechaISO'
GO
CREATE FUNCTION dbo.fnu_ObtenerFechaISO (
/*-----------------------------------------------------------
Descripcion: obtiene fecha actual con formato AAAAMMDDHHMMSS
Por        : VSR, 15/11/2010
-------------------------------------------------------------*/
)
RETURNS VARCHAR(20)
AS
BEGIN

  DECLARE @fecha  VARCHAR(8)
  DECLARE @tiempo VARCHAR(6)
  DECLARE @fechaISO VARCHAR(20)

  SELECT @fecha = CONVERT(varchar,dbo.fnu_GETDATE(),112)
  SELECT @tiempo = REPLACE(CONVERT(VARCHAR,dbo.fnu_GETDATE(),108),':','')

  -- construye fecha
  SET @fechaISO  = @fecha + @tiempo

	-- retorna valor a la funcion
	RETURN @fechaISO
END
GO
     
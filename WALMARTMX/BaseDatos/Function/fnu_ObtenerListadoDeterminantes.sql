﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_ObtenerListadoDeterminantes' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_ObtenerListadoDeterminantes'
  DROP FUNCTION dbo.fnu_ObtenerListadoDeterminantes
END
GO

PRINT 'Creating Function fnu_ObtenerListadoDeterminantes'
GO
CREATE FUNCTION dbo.fnu_ObtenerListadoDeterminantes (
/******************************************************************************
**  Descripcion: obtiene listado de determinantes y nombres
**  Fecha      : 15/02/2017
*******************************************************************************/
@1 INT,
@2 INT,
@3 INT,
@4 INT,
@5 INT,
@6 INT,
@7 INT,
@8 INT,
@9 INT,
@10 INT,
@11 INT,
@12 INT,
@13 INT,
@14 INT,
@15 INT
)
RETURNS VARCHAR(8000)
AS
BEGIN
	-- se inicializa la variable
	DECLARE @listado AS VARCHAR(8000)
	SET @listado = ''

	SELECT
		@listado = @listado + L.Nombre + ' - ' + CONVERT(VARCHAR, L.CodigoInterno) + ' ; '
	FROM
    Local AS L
	WHERE 
    L.CodigoInterno IN (
        @1
      , @2
      , @3
      , @4
      , @5
      , @6
      , @7
      , @8
      , @9
      , @10
      , @11
      , @12
      , @13
      , @14
      , @15
    )	

  IF (@listado IS NULL OR @listado = '') BEGIN
    SET @listado =  ''
  END ELSE BEGIN
	  SET @listado = SUBSTRING(@listado, 1, Len(@listado) - 1 )
  END

  RETURN @listado
END
GO     
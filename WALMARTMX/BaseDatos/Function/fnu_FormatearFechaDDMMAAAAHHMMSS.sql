﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_FormatearFechaDDMMAAAAHHMMSS' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_FormatearFechaDDMMAAAAHHMMSS'
  DROP FUNCTION dbo.fnu_FormatearFechaDDMMAAAAHHMMSS
END
GO

PRINT 'Creating Function fnu_FormatearFechaDDMMAAAAHHMMSS'
GO
CREATE FUNCTION [dbo].[fnu_FormatearFechaDDMMAAAAHHMMSS] (  
/*-----------------------------------------------------------  
Funcion    : fnu_FormatearFechaDDMMAAAAHHMMSS  
Descripcion: formatea una fecha AAAAMMDD HHMMSS en DD/MM/AAAA HH:MM:SS  
Por        : VSR, 20/04/2015
-------------------------------------------------------------*/  
@Fecha VARCHAR(20),  
@MostrarSegundos AS INT  
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
  
  DECLARE @newFecha   VARCHAR(10)  
  DECLARE @newHora    VARCHAR(10)  
  DECLARE @parteFecha VARCHAR(10)  
  DECLARE @parteHora  VARCHAR(10)  
  
  DECLARE @ano      VARCHAR(4)  
  DECLARE @mesDia   VARCHAR(4)  
  DECLARE @mes      VARCHAR(2)  
  DECLARE @dia      VARCHAR(2)  
  
  DECLARE @hora             VARCHAR(2)  
  DECLARE @minutosSegundos  VARCHAR(4)  
  DECLARE @minutos          VARCHAR(2)  
  DECLARE @segundos         VARCHAR(2)  
  
  IF(@Fecha IS NULL OR @Fecha='') BEGIN  
    SET @newFecha = ''  
  END  
  ELSE BEGIN  
    SET @parteFecha = RTRIM(LTRIM(LEFT(@Fecha,8)))  
    SET @parteHora = RTRIM(LTRIM(RIGHT(@Fecha,6)))  
    
    -- construye fecha  
    SET @ano    = LEFT(@parteFecha,4)  
    SET @mesDia = RIGHT(@parteFecha,4)  
    SET @mes    = LEFT(@mesDia,2)  
    SET @dia    = RIGHT(@mesDia,2)  
    SET @newFecha = @dia + '/' + @mes + '/' + @ano  
  
    -- construye hora  
    SET @hora            = LEFT(@parteHora,2)  
    SET @minutosSegundos = RIGHT(@parteHora,4)  
    SET @minutos         = LEFT(@minutosSegundos,2)  
    SET @segundos        = RIGHT(@minutosSegundos,2)  
    SET @newHora         = @hora + ':' + @minutos  
    IF (@MostrarSegundos=1) SET @newHora = @newHora + ':' + @segundos  
  END  
   
 -- retorna valor a la funcion  
 RETURN @newFecha + ' ' + @newHora  
END
GO
     
﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_ObtenerListadoPerfilesPorPagina' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_ObtenerListadoPerfilesPorPagina'
  DROP FUNCTION dbo.fnu_ObtenerListadoPerfilesPorPagina
END
GO

PRINT 'Creating Function fnu_ObtenerListadoPerfilesPorPagina'
GO
CREATE FUNCTION dbo.fnu_ObtenerListadoPerfilesPorPagina (
/******************************************************************************
**  Descripcion: obtiene los perfiles asociados a la pagina
**  Fecha      : 15/09/2009
*******************************************************************************/
@IdPagina INT,
@esHTML INT
)
RETURNS VARCHAR(8000)
AS
BEGIN
  -- se inicializa la variable
  DECLARE @listado AS VARCHAR(8000)
  SET @listado = ''

  DECLARE @separador VARCHAR(10)
  DECLARE @vineta VARCHAR(10)
  IF(@esHTML = 1) BEGIN
    SET @separador = '<br/>'
    SET @vineta = '- '
  END
  ELSE BEGIN
    SET @separador = '; '
    SET @vineta = ''
  END

  SELECT
    @listado = @listado + @vineta + RTRIM(LTRIM(ISNULL(P.Nombre,''))) + @separador
  FROM
    PaginasXPerfil AS PxR
    INNER JOIN Perfil AS P ON (PxR.IdPerfil = P.Id)
  WHERE
    PxR.IdPagina = @IdPagina
  ORDER BY
    P.Nombre
    
  -- retorna listado
  RETURN @listado
END
GO
     
﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_ConvertirFechaISOToDatetime' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_ConvertirFechaISOToDatetime'
  DROP FUNCTION dbo.fnu_ConvertirFechaISOToDatetime
END
GO

PRINT 'Creating Function fnu_ConvertirFechaISOToDatetime'
GO
CREATE FUNCTION dbo.fnu_ConvertirFechaISOToDatetime (
/*-----------------------------------------------------------
Descripcion: convierte fecha aaaammddhhmmss en datetime
Por        : VSR, 14/07/2014
-------------------------------------------------------------*/
@Fecha AS VARCHAR(50)
)
RETURNS DATETIME
AS
BEGIN

  -- rellena con ceros a la derecha para obtener el largo exacto de la fecha en caso que falte algun dato
  SET @Fecha = @Fecha + REPLICATE('0',14)
  SET @Fecha = LEFT(@Fecha, 14)

  DECLARE @amd AS VARCHAR(8), @hms VARCHAR(6), @hh VARCHAR(2), @mm VARCHAR(2), @ss VARCHAR(2), @nuevaFecha VARCHAR(50)
  DECLARE @dateTime AS DATETIME

  SET @amd = LEFT(@Fecha, 8)
  SET @hms = RIGHT(@Fecha, 6)
  SET @hh = LEFT(@hms, 2)
  SET @mm = LEFT(RIGHT(@hms, 4),2)
  SET @ss = RIGHT(@hms, 2)

  SET @nuevaFecha = @amd + ' ' + @hh + ':' + @mm + ':' + @ss

  IF (ISDATE(@nuevaFecha) = 1) BEGIN
    SET @dateTime = CONVERT(DATETIME, @nuevaFecha)
  END ELSE BEGIN
    SET @dateTime = NULL
  END

	-- retorna valor a la funcion
	RETURN @dateTime
END
GO
     
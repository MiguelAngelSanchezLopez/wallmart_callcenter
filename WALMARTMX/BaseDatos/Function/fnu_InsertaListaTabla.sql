IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_InsertaListaTabla' AND xtype = 'TF') )
BEGIN
  PRINT 'Dropping Function fnu_InsertaListaTabla'
  DROP FUNCTION dbo.fnu_InsertaListaTabla
END
GO

PRINT 'Creating Function fnu_InsertaListaTabla'
GO
CREATE FUNCTION dbo.fnu_InsertaListaTabla (
/******************************************************************************
**  Descripcion: Toma una lista separada por comas y retorna una tabla con 1 columna con los valores
**  Fecha      : 23/07/2009
*******************************************************************************/
@lista varchar(8000)
)
RETURNS @Tabla TABLE ( valor varchar(50) NOT NULL)
AS

BEGIN DECLARE @valor varchar(50), @pos Int
SET @valor = ''
SET @pos = PATINDEX('%,%', @lista)
WHILE (@pos > 0)
BEGIN
  SET @valor = SUBSTRING(@lista,1,@pos-1)
  SET @lista = SUBSTRING(@lista, @pos+1, LEN(@lista)- @pos)
  INSERT INTO @Tabla (valor) VALUES (RTRIM(LTRIM(@valor)))
  SET @pos = PATINDEX('%,%', @lista)
END
SET @valor = @lista

If @valor <> ''
  INSERT INTO @Tabla (valor)
  VALUES (@valor)

RETURN


END
GO
  
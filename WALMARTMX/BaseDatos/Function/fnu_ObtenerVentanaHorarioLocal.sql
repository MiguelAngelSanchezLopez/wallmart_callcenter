﻿IF( EXISTS( SELECT * FROM sysobjects WHERE name = 'fnu_ObtenerVentanaHorarioLocal' AND xtype = 'FN') )
BEGIN
  PRINT 'Dropping Function fnu_ObtenerVentanaHorarioLocal'
  DROP FUNCTION dbo.fnu_ObtenerVentanaHorarioLocal
END
GO

PRINT 'Creating Function fnu_ObtenerVentanaHorarioLocal'
GO
CREATE FUNCTION dbo.fnu_ObtenerVentanaHorarioLocal (
/*-----------------------------------------------------------
Descripcion: obtiene listado ventana horaria
Por        : VSR, 21/08/2017
-------------------------------------------------------------*/
@IdZona INT
)
RETURNS VARCHAR(255)
AS
BEGIN

  DECLARE @Listado VARCHAR(1000), @Lunes_ViernesIni VARCHAR(255), @Lunes_ViernesFin VARCHAR(255), @SabadoIni VARCHAR(255), @SabadoFin VARCHAR(255),
          @DomingoIni VARCHAR(255), @DomingoFin VARCHAR(255)

  SET @Listado = ''

  SELECT
    @Lunes_ViernesIni = ISNULL(LEFT(CONVERT(VARCHAR, Semana_InicioVentana, 108), 5), ''),
    @Lunes_ViernesFin = ISNULL(LEFT(CONVERT(VARCHAR, Semana_FinVentana, 108), 5), ''),
    @SabadoIni = ISNULL(LEFT(CONVERT(VARCHAR, Sabado_InicioVentana, 108), 5), ''),
    @SabadoFin = ISNULL(LEFT(CONVERT(VARCHAR, Sabado_FinVentana, 108), 5), ''),
    @DomingoIni = ISNULL(LEFT(CONVERT(VARCHAR, Domingo_InicioVentana, 108), 5), ''),
    @DomingoFin = ISNULL(LEFT(CONVERT(VARCHAR, Domingo_FinVentana, 108), 5), '')
  FROM
    Track_VentanasHorarias
  WHERE
    IdZona = @IdZona

  IF (@Lunes_ViernesIni <> '' AND @Lunes_ViernesFin <> '') BEGIN
    SET @Listado = @Listado + 'Lu-Vi ' + @Lunes_ViernesIni + '-' + @Lunes_ViernesFin + ' / '
  END

  IF (@SabadoIni <> '' AND @SabadoFin <> '') BEGIN
    SET @Listado = @Listado + 'Sá ' + @SabadoIni + '-' + @SabadoFin + ' / '
  END

  IF (@DomingoIni <> '' AND @DomingoFin <> '') BEGIN
    SET @Listado = @Listado + 'Do ' + @DomingoIni + '-' + @DomingoFin + ' / '
  END

	-- retorna valor a la funcion
	RETURN @Listado
END
GO
     
﻿-- ESTADO: PRODUCCION DESDE 14/03/2017

/*
--------------------------------
SPU MODIFICADOS 
--------------------------------
spu_Usuario_GrabarDatosExtras
spu_Usuario_ObtenerDetalle
spu_Alerta_ObtenerAlertasPorAtender

*/

/*===================================================================================================
20/02/2017
===================================================================================================*/
ALTER TABLE UsuarioDatoExtra ALTER COLUMN GestionarSoloCemtra VARCHAR(255) NULL

UPDATE UsuarioDatoExtra SET GestionarSoloCemtra = 'SoloCemtra' WHERE GestionarSoloCemtra = 1
UPDATE UsuarioDatoExtra SET GestionarSoloCemtra = NULL WHERE GestionarSoloCemtra <> 'SoloCemtra'
UPDATE UsuarioDatoExtra SET GestionarSoloCemtra = 'Todas' WHERE GestionarSoloCemtra IS NULL AND IdUsuario IN (SELECT Id FROM Usuario WHERE IdPerfil = 3)




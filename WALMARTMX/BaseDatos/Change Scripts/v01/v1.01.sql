﻿-- ESTADO: EN PRODUCCION

/*
--------------------------------
SPU MODIFICADOS 
--------------------------------
spu_Alerta_GrabarAlertaEnColaAtencion
spu_Job_AsignarAlertaEnColaAtencion
spu_Job_AlertasEnColaAtencion
spu_Job_DesactivarAlertaEnColaAtencionLocalesRMyOtros
spu_Alerta_ActualizarAsignacionAlertaEnCola
spu_Alerta_EliminarAlertaEnColaAtencion
spu_Alerta_ObtenerAlertasPorAtender
spu_Alerta_ObtenerAlertasSinAtender
vwu_AlertaAcumulada
vwu_AlertaPorGestionar

*/

/*===================================================================================================
15/12/2016
===================================================================================================*/
CREATE TABLE [dbo].[AlertaEnColaAtencion](
	[IdAlerta] [int] NULL,
	[IdAlertaHija] [int] NULL,
	[NroTransporte] [int] NULL,
	[NombreAlerta] [varchar](255) NULL,
	[LocalDestino] [int] NULL,
	[IdFormato] [int] NULL,
	[Prioridad] [varchar](255) NULL,
	[OrdenPrioridad] [int] NULL,
	[FechaHoraCreacion] [datetime] NULL,
	[FechaHoraCreacionDMA] [varchar](255) NULL,
	[Asignada] [bit] NULL,
	[IdUsuarioAsignada] [int] NULL,
	[FechaAsignacion] [datetime] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HistorialAlertaEnColaAtencion](
	[IdAlerta] [int] NULL,
	[IdAlertaHija] [int] NULL,
	[NroTransporte] [int] NULL,
	[NombreAlerta] [varchar](255) NULL,
	[LocalDestino] [int] NULL,
	[IdFormato] [int] NULL,
	[Prioridad] [varchar](255) NULL,
	[OrdenPrioridad] [int] NULL,
	[FechaHoraCreacion] [datetime] NULL,
	[FechaHoraCreacionDMA] [varchar](255) NULL,
	[Asignada] [bit] NULL,
	[IdUsuarioAsignada] [int] NULL,
	[FechaAsignacion] [datetime] NULL
) ON [PRIMARY]
GO

/*===================================================================================================
03/01/2017
===================================================================================================*/
CREATE TABLE [dbo].[InformacionRemolque](
	[Id_Embarque] [bigint] NOT NULL,
	[Determinante_WMS] [int] NULL,
	[Embarque_GLS] [int] NULL,
	[Cortina] [int] NULL,
	[Tipo_Servicio] [varchar](50) NULL,
	[Tipo_Entrega] [varchar](50) NULL,
	[Numero_Entregas] [int] NULL,
	[Tipo_Flota] [varchar](50) NULL,
	[Id_Unidad_Embarque] [varchar](50) NULL,
	[Clave_MTS] [varchar](50) NULL,
	[Tipo_Unidad] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[Estatus_Completo] [varchar](50) NULL,
	[1] [varchar](50) NULL,
	[2] [varchar](50) NULL,
	[3] [varchar](50) NULL,
	[4] [varchar](50) NULL,
	[5] [varchar](50) NULL,
	[6] [varchar](50) NULL,
	[7] [varchar](50) NULL,
	[8] [varchar](50) NULL,
	[9] [varchar](50) NULL,
	[10] [varchar](50) NULL,
	[11] [varchar](50) NULL,
	[12] [varchar](50) NULL,
	[13] [varchar](50) NULL,
	[14] [varchar](50) NULL,
	[15] [varchar](50) NULL,
	[Fecha_Solicitud] [datetime] NULL,
	[Nro_Marchamo] [int] NULL,
	[FechaHoraCreacion] [datetime] NULL,
 CONSTRAINT [PK_InformacionRemolque] PRIMARY KEY CLUSTERED 
(
	[Id_Embarque] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[InformacionRemolque] ADD  CONSTRAINT [DF_InformacionRemolque_FechaHoraCreacion]  DEFAULT ([dbo].[fnu_GETDATE]()) FOR [FechaHoraCreacion]
GO


CREATE TABLE [dbo].[InformacionTracto](
	[Id_Master] [bigint] NOT NULL,
	[Tipo_Viaje] [varchar](50) NULL,
	[CT_Tipo_Flota] [varchar](50) NULL,
	[Determinante_WMS] [int] NULL,
	[Id_Unidad_Viaje] [bigint] NULL,
	[Clave_MTS] [varchar](50) NULL,
	[Tipo_Unidad] [varchar](50) NULL,
	[Operador] [varchar](50) NULL,
	[Carta_Porte] [int] NULL,
	[Id_GTS] [bigint] NULL,
	[Id_Embarque] [bigint] NULL,
	[Embarque_GLS] [int] NULL,
	[Tipo_Servicio] [varchar](50) NULL,
	[Tipo_Entrega] [varchar](50) NULL,
	[Numero_Entregas] [int] NULL,
	[Tipo_Flota] [varchar](50) NULL,
	[Id_Unidad_Embarque] [varchar](50) NULL,
	[Nro_Factura] [varchar](50) NULL,
	[1] [varchar](50) NULL,
	[2] [varchar](50) NULL,
	[3] [varchar](50) NULL,
	[4] [varchar](50) NULL,
	[5] [varchar](50) NULL,
	[6] [varchar](50) NULL,
	[7] [varchar](50) NULL,
	[8] [varchar](50) NULL,
	[9] [varchar](50) NULL,
	[10] [varchar](50) NULL,
	[11] [varchar](50) NULL,
	[12] [varchar](50) NULL,
	[13] [varchar](50) NULL,
	[14] [varchar](50) NULL,
  [15] [varchar](50) NULL,
	[Fecha_Solicitud] [datetime] NULL,
	[Fecha_Enrampe] [datetime] NULL,
	[Fecha_Apertura_Embarque] [datetime] NULL,
	[Fecha_Cierre_Embarque] [datetime] NULL,
	[Fecha_Retiro] [datetime] NULL,
	[Fecha_Cita_Asignada] [datetime] NULL,
	[Fecha_Generacion] [datetime] NULL,
	[Fecha_Despacho] [datetime] NULL,
	[CT_Status_Embarques] [varchar](50) NULL,
	[CT_Status_Viajes] [varchar](50) NULL,
	[FechaHoraCreacion] [datetime] NULL,
 CONSTRAINT [PK_InformacionTracto] PRIMARY KEY CLUSTERED 
(
	[Id_Master] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[InformacionTracto] ADD  CONSTRAINT [DF_InformacionTracto_FechaHoraCreacion]  DEFAULT ([dbo].[fnu_GETDATE]()) FOR [FechaHoraCreacion]
GO

/*===================================================================================================
04/01/2017
===================================================================================================*/
INSERT INTO TipoGeneral(Nombre,Valor,Tipo,Activo) VALUES('OPERACIONAL','OPERACIONAL','TipoAlerta',1)
INSERT INTO TipoGeneral(Nombre,Valor,Tipo,Activo) VALUES('INFORMATIVA','INFORMATIVA','TipoAlerta',1)

/*
TABLAS MODIFICADAS EN PRODUCCION PARA CONFIGURACION DE ALERTAS (CARGAR ESOS DATOS EN DESARROLLO)
AlertaDefinicion
TipoGeneral
[todas las tablas asociadas a la configuracion de las alertas]
*/






﻿-- Estado: EN PRODUCCION 22/09/2017

/*---------------------------------------------
NUEVOS PROCEDIMIENTOS ALMACENADOS / FUNCIONES / VISTAS
---------------------------------------------
vwu_AlertaAtendida
vwu_AlertaContactoEfectivo
vwu_AlertaEnColaAtencion
vwu_AlertaPorEjecutivo

spu_Reporte_AlertasAtendidas
spu_Reporte_AlertasContactadas
spu_Reporte_AlertasEntrantes
spu_Reporte_AlertasPorEjecutivo
spu_Reporte_HistorialAccionesPorAlerta
spu_Reporte_ProductividadPorEjecutivo
spu_Reporte_TotalesAlertas
spu_Reporte_TotalesTipoAlerta

spu_Sistema_GuardarPoolPlaca
spu_Sistema_GrabarPoolPlacaHistoria
spu_Sistema_ObtenerCargaPoolPlaca
spu_Job_RevisarCargaPoolPlacas

*/


/*===================================================================================================
21/09/2017
===================================================================================================*/
ALTER TABLE PoolPlaca ADD NumeroEconomico VARCHAR(255)
ALTER TABLE PoolPlacaHistoria ADD NumeroEconomico VARCHAR(255)

﻿-- Estado: EN PRODUCCION DESDE 27/07/2017

/*
--------------------------------
SPU MODIFICADOS 
--------------------------------
spu_Local_ObtenerInformacionLocalAsociadoUsuario


*/

/*===================================================================================================
15/05/2017
===================================================================================================*/
INSERT INTO CategoriaPagina(Nombre, Orden)
SELECT 'Soporte', Orden = MAX(Orden)+1 FROM CategoriaPagina

-- BEGIN [
DECLARE @IdCategoria INT, @Orden INT
SET @IdCategoria = (SELECT Id FROM CategoriaPagina WHERE Nombre='Soporte')
SET @Orden = ISNULL((SELECT MAX(Orden) FROM Pagina WHERE IdCategoria=@IdCategoria),0) + 1
INSERT INTO Pagina(Titulo,NombreArchivo,IdCategoria,Orden,Estado,MostrarEnMenu) VALUES('Enviar Ticket','Soporte.NuevoTicket.aspx',@IdCategoria,@Orden,1,1)

DECLARE @IdPagina INT,@IdPerfil INT
SET @IdPagina = (SELECT Id FROM Pagina WHERE NombreArchivo='Soporte.NuevoTicket.aspx')
INSERT INTO FuncionPagina(IdPagina,Nombre,LlaveIdentificador,Orden) VALUES(@IdPagina,'Ver','Ver',1)
-- ] end


-- BEGIN [
DECLARE @IdCategoria INT, @Orden INT
SET @IdCategoria = (SELECT Id FROM CategoriaPagina WHERE Nombre='Soporte')
SET @Orden = ISNULL((SELECT MAX(Orden) FROM Pagina WHERE IdCategoria=@IdCategoria),0) + 1
INSERT INTO Pagina(Titulo,NombreArchivo,IdCategoria,Orden,Estado,MostrarEnMenu) VALUES('Mis Ticket','Soporte.MisTicket.aspx',@IdCategoria,@Orden,1,1)

DECLARE @IdPagina INT,@IdPerfil INT
SET @IdPagina = (SELECT Id FROM Pagina WHERE NombreArchivo='Soporte.MisTicket.aspx')
INSERT INTO FuncionPagina(IdPagina,Nombre,LlaveIdentificador,Orden) VALUES(@IdPagina,'Ver','Ver',1)
INSERT INTO FuncionPagina(IdPagina,Nombre,LlaveIdentificador,Orden) VALUES(@IdPagina,'Crear','Crear',2)
-- ] end

-- BEGIN [
DECLARE @IdCategoria INT, @Orden INT
SET @IdCategoria = (SELECT Id FROM CategoriaPagina WHERE Nombre='Soporte')
SET @Orden = ISNULL((SELECT MAX(Orden) FROM Pagina WHERE IdCategoria=@IdCategoria),0) + 1
INSERT INTO Pagina(Titulo,NombreArchivo,IdCategoria,Orden,Estado,MostrarEnMenu) VALUES('Detalle Ticket','Soporte.DetalleTicket.aspx',@IdCategoria,@Orden,1,0)

DECLARE @IdPagina INT,@IdPerfil INT
SET @IdPagina = (SELECT Id FROM Pagina WHERE NombreArchivo='Soporte.DetalleTicket.aspx')
INSERT INTO FuncionPagina(IdPagina,Nombre,LlaveIdentificador,Orden) VALUES(@IdPagina,'Ver','Ver',1)
-- ] end

﻿
-- Estado: Desarrollo

/*---------------------------------------------
NUEVOS PROCEDIMIENTOS ALMACENADOS / FUNCIONES / VISTAS
---------------------------------------------

FormCargaCabeceraMX

*/

/*===================================================================================================
12/10/2017 Mauro Rojas. //Carga  Cabecera
===================================================================================================*/
/****** Object:  Table [dbo].[FormCargaCabeceraMX]    Script Date: 12/10/2017 17:11:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].FormCargaCabeceraMX(
	[IDENTIFIER] [int] NOT NULL,
	[SHIFTEDDEVICESUBMITDATE] [varchar](19) NULL,
	[USERNAME] [varchar](50) NULL,
	[FECHACREACION] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDENTIFIER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/*===================================================================================================
13/10/2017 Mauro Rojas. //Carga paginas

tablas de imagenes, iguales solo cambia en el numero de pagina donde vienen fotos
===================================================================================================*/

CREATE TABLE [dbo].[FormCargaImagenPagina2](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina3](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina4](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina5](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE TABLE [dbo].[FormCargaImagenPagina6](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE TABLE [dbo].[FormCargaImagenPagina7](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE TABLE [dbo].[FormCargaImagenPagina8](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina9](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina10](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina11](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina12](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina13](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina14](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina15](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina16](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaImagenPagina17](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
/*===================================================================================================
13/10/2017 Mauro Rojas //Carga

tablas de las paginas a crear para la Carga
===================================================================================================*/

CREATE TABLE [dbo].[FormCargaPagina1](
	[RUT] [varchar](255) NULL,
	[Embarque] [varchar](255) NULL,
	[Sldd] [varchar](255) NULL,
	[Correo 5] [varchar](255) NULL,
	[CDO] [varchar](255) NULL,
	[Usuario] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[FormCargaPagina2](
    [Ec1 1][varchar](255) NULL,
	[PP1 1] [varchar](255) NULL,
	[ETC1 1][varchar](255) NULL,
	[ETC3 1][varchar](255) NULL,
	[ETC4 1][varchar](255) NULL,
	[PP2 1] [varchar](255) NULL,
	[PP3 1] [varchar](255) NULL,
	[OBS1 1] [varchar](255) NULL,
	[FSR1 1] [varchar](255) NULL,
	[OTRACARGA1 1] [varchar](255) NULL,
	[E 11] [varchar](255) NULL,
	[E 12] [varchar](255) NULL,
	[E 13] [varchar](255) NULL,
	[E 14] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaPagina3](
	[Ec1 2] [varchar](255) NULL,
	[PP1 2] [varchar](255) NULL,
	[ETC1 2] [varchar](255) NULL,
	[PP2 2] [varchar](255) NULL,
	[PP3 2] [varchar](255) NULL,
	[ETC3 2] [varchar](255) NULL,
	[ETC4 2][varchar](255) NULL,
	[OBS1 2] [varchar](255) NULL,
	[FSR1 2] [varchar](255) NULL,
	[OTRACARGA1 2] [varchar](255) NULL,
	[E 21] [varchar](255) NULL,
	[E 22] [varchar](255) NULL,
	[E 23] [varchar](255) NULL,
	[E 24] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaPagina4](
	[Ec1 3] [varchar](255) NULL,
	[PP1 3] [varchar](255) NULL,
	[PP2 3] [varchar](255) NULL,
	[PP3 3] [varchar](255) NULL,
	[ETC1 3] [varchar](255) NULL,
	[ETC3 3] [varchar](255) NULL,
	[ETC4 3] [varchar](255) NULL,
	[OBS1 3] [varchar](255) NULL,
	[FSR1 3] [varchar](255) NULL,
	[OTRACARGA1 3] [varchar](255) NULL,
	[E 31] [varchar](255) NULL,
	[E 32] [varchar](255) NULL,
	[E 33] [varchar](255) NULL,
	[E 34] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaPagina5](
	[Ec1 4] [varchar](255) NULL,
	[PP1 4] [varchar](255) NULL,
	[ETC1 4] [varchar](255) NULL,
	[PP2 4] [varchar](255) NULL,
	[PP3 4] [varchar](255) NULL,
	[ETC3 4] [varchar](255) NULL,
	[OBS1 4] [varchar](255) NULL,
	[FSR1 4] [varchar](255) NULL,
	[ETC4 4] [varchar](255) NULL,
	[OTRACARGA1 4] [varchar](255) NULL,
	[E 41] [varchar](255) NULL,
	[E 42] [varchar](255) NULL,
	[E 43] [varchar](255) NULL,
	[E 44] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[FormCargaPagina6](
	[Ec1 5] [varchar](255) NULL,
	[PP1 5] [varchar](255) NULL,
	[PP2 5] [varchar](255) NULL,
	[PP3 5] [varchar](255) NULL,
	[ETC1 5] [varchar](255) NULL,
	[ETC3 5] [varchar](255) NULL,
	[ETC4 5] [varchar](255) NULL,
	[OBS1 5] [varchar](255) NULL,
	[FSR1 5] [varchar](255) NULL,
	[OTRACARGA1 5] [varchar](255) NULL,
	[E 51] [varchar](255) NULL,
	[E 52] [varchar](255) NULL,
	[E 53] [varchar](255) NULL,
	[E 54] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaPagina7](
	[Ec1 6] [varchar](255) NULL,
	[PP1 6] [varchar](255) NULL,
	[PP2 6] [varchar](255) NULL,
	[PP3 6] [varchar](255) NULL,
	[ETC1 6] [varchar](255) NULL,
	[ETC3 6] [varchar](255) NULL,
	[ETC4 6] [varchar](255) NULL,
	[OBS1 6] [varchar](255) NULL,
	[FSR1 6] [varchar](255) NULL,
	[OTRACARGA1 6] [varchar](255) NULL,
	[E 61] [varchar](255) NULL,
	[E 62] [varchar](255) NULL,
	[E 63] [varchar](255) NULL,
	[E 64] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[FormCargaPagina8](
	[Ec1 7] [varchar](255) NULL,
	[PP1 7] [varchar](255) NULL,
	[PP2 7] [varchar](255) NULL,
	[PP3 7] [varchar](255) NULL,
	[ETC1 7] [varchar](255) NULL,
	[ETC3 7] [varchar](255) NULL,
	[ETC4 7] [varchar](255) NULL,
	[OBS1 7] [varchar](255) NULL,
	[FSR1 7] [varchar](255) NULL,
	[OTRACARGA1 7] [varchar](255) NULL,
	[E 71] [varchar](255) NULL,
	[E 72] [varchar](255) NULL,
	[E 73] [varchar](255) NULL,
	[E 74] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[FormCargaPagina9](
	[Ec1 8] [varchar](255) NULL,
	[PP1 8] [varchar](255) NULL,
	[PP2 8] [varchar](255) NULL,
	[PP3 8] [varchar](255) NULL,
	[ETC1 8] [varchar](255) NULL,
	[ETC3 8] [varchar](255) NULL,
	[ETC4 8] [varchar](255) NULL,
	[OBS1 8] [varchar](255) NULL,
	[FSR1 8] [varchar](255) NULL,
	[OTRACARGA1 8] [varchar](255) NULL,
	[E 81] [varchar](255) NULL,
	[E 82] [varchar](255) NULL,
	[E 83] [varchar](255) NULL,
	[E 84] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[FormCargaPagina10](
	[Ec1 9] [varchar](255) NULL,
	[PP1 9] [varchar](255) NULL,
	[PP2 9] [varchar](255) NULL,
	[PP3 9] [varchar](255) NULL,
	[ETC1 9] [varchar](255) NULL,
	[ETC3 9] [varchar](255) NULL,
	[ETC4 9] [varchar](255) NULL,
	[OBS1 9] [varchar](255) NULL,
	[FSR1 9] [varchar](255) NULL,
	[OTRACARGA1 9] [varchar](255) NULL,
	[E 91] [varchar](255) NULL,
	[E 92] [varchar](255) NULL,
	[E 93] [varchar](255) NULL,
	[E 94] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]



CREATE TABLE [dbo].[FormCargaPagina11](
	[Ec1 10] [varchar](255) NULL,
	[PP1 10] [varchar](255) NULL,
	[PP2 10] [varchar](255) NULL,
	[PP3 10] [varchar](255) NULL,
	[ETC1 10] [varchar](255) NULL,
	[ETC3 10] [varchar](255) NULL,
	[ETC4 10] [varchar](255) NULL,
	[OBS1 10] [varchar](255) NULL,
	[FSR1 10] [varchar](255) NULL,
	[OTRACARGA1 10] [varchar](255) NULL,
	[E 101] [varchar](255) NULL,
	[E 102] [varchar](255) NULL,
	[E 103] [varchar](255) NULL,
	[E 104] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[FormCargaPagina12](
	[Ec1 11] [varchar](255) NULL,
	[PP1 11] [varchar](255) NULL,
	[PP2 11] [varchar](255) NULL,
	[PP3 11] [varchar](255) NULL,
	[ETC1 11] [varchar](255) NULL,
	[ETC3 11] [varchar](255) NULL,
	[ETC4 11] [varchar](255) NULL,
	[OBS1 11] [varchar](255) NULL,
	[FSR1 11] [varchar](255) NULL,
	[OTRACARGA1 11] [varchar](255) NULL,
	[E 111] [varchar](255) NULL,
	[E 112] [varchar](255) NULL,
	[E 113] [varchar](255) NULL,
	[E 114] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]



CREATE TABLE [dbo].[FormCargaPagina13](
	[Ec1 12] [varchar](255) NULL,
	[PP1 12] [varchar](255) NULL,
	[PP2 12] [varchar](255) NULL,
	[PP3 12] [varchar](255) NULL,
	[ETC1 12] [varchar](255) NULL,
	[ETC3 12] [varchar](255) NULL,
	[ETC4 12] [varchar](255) NULL,
	[OBS1 12] [varchar](255) NULL,
	[FSR1 12] [varchar](255) NULL,
	[OTRACARGA1 12] [varchar](255) NULL,
	[E 121] [varchar](255) NULL,
	[E 122] [varchar](255) NULL,
	[E 123] [varchar](255) NULL,
	[E 124] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaPagina14](
	[Ec1 13] [varchar](255) NULL,
	[PP1 13] [varchar](255) NULL,
	[PP2 13] [varchar](255) NULL,
	[PP3 13] [varchar](255) NULL,
	[ETC1 13] [varchar](255) NULL,
	[ETC3 13] [varchar](255) NULL,
	[ETC4 13] [varchar](255) NULL,
	[OBS1 13] [varchar](255) NULL,
	[FSR1 13] [varchar](255) NULL,
	[OTRACARGA1 13] [varchar](255) NULL,
	[E 131] [varchar](255) NULL,
	[E 132] [varchar](255) NULL,
	[E 133] [varchar](255) NULL,
	[E 134] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[FormCargaPagina15](
	[Ec1 14] [varchar](255) NULL,
	[PP1 14] [varchar](255) NULL,
	[PP2 14] [varchar](255) NULL,
	[PP3 14] [varchar](255) NULL,
	[ETC1 14] [varchar](255) NULL,
	[ETC3 14] [varchar](255) NULL,
	[ETC4 14] [varchar](255) NULL,
	[OBS1 14] [varchar](255) NULL,
	[FSR1 14] [varchar](255) NULL,
	[OTRACARGA1 14] [varchar](255) NULL,
	[E 141] [varchar](255) NULL,
	[E 142] [varchar](255) NULL,
	[E 143] [varchar](255) NULL,
	[E 144] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[FormCargaPagina16](
	[Ec1 15] [varchar](255) NULL,
	[PP1 15] [varchar](255) NULL,
	[PP2 15] [varchar](255) NULL,
	[PP3 15] [varchar](255) NULL,
	[ETC1 15] [varchar](255) NULL,
	[ETC3 15] [varchar](255) NULL,
	[ETC4 15] [varchar](255) NULL,
	[OBS1 15] [varchar](255) NULL,
	[FSR1 15] [varchar](255) NULL,
	[OTRACARGA1 15] [varchar](255) NULL,
	[E 151] [varchar](255) NULL,
	[E 152] [varchar](255) NULL,
	[E 153] [varchar](255) NULL,
	[E 154] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[FormCargaPagina17](
	[IPondlC] [varchar](255) NULL,
	[FíadlCeS] [varchar](255) NULL,
	[CSF] [varchar](255) NULL,
	[FíadlCeS 3] [varchar](255) NULL,
	[Patente] [varchar](255) NULL,
	[FPatente] [varchar](255) NULL,
	[CDP] [varchar](255) NULL,
	[Firma] [varchar](255) NULL,
	[identifier] [varchar](255) NULL
) ON [PRIMARY]
/*===================================================================================================
13/10/2017 Mauro Rojas //Recepcion

tablas de las paginas a crear para la Recepcion
===================================================================================================*/


CREATE TABLE [dbo].[FormRecepcionImagenPagina1](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina2](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina3](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina4](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina5](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina6](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina7](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina8](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina9](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina10](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina11](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina12](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina13](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina14](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina15](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina16](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina17](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/*===================================================================================================
13/10/2017 Mauro Rojas //Recepcion Cabecera

tablas de cabecera a crear para la Recepcion
===================================================================================================*/

CREATE TABLE [dbo].[FormRecepcionCabeceraMX](
	[IDENTIFIER] [int] NOT NULL,
	[SHIFTEDDEVICESUBMITDATE] [varchar](19) NULL,
	[USERNAME] [varchar](50) NULL,
	[FECHACREACION] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDENTIFIER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


/*===================================================================================================
13/10/2017 Mauro Rojas //Recepcion paginas

tablas de las paginas a crear para la Recepcion
===================================================================================================*/
CREATE TABLE [dbo].[FormRecepcionImagenPagina2](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina3](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina4](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina5](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina6](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[FormRecepcionImagenPagina7](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina8](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina9](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[FormRecepcionImagenPagina10](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina11](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[FormRecepcionImagenPagina12](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[FormRecepcionImagenPagina13](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[FormRecepcionImagenPagina14](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[FormRecepcionImagenPagina15](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina16](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FormRecepcionImagenPagina17](
	[identifier] [varchar](255) NULL,
	[label] [varchar](255) NULL,
	[identifier_foto] [varchar](255) NULL,
	[filename] [varchar](255) NULL,
	[contentType] [varchar](255) NULL,
	[bytes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
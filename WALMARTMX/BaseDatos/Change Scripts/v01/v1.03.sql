﻿-- ESTADO: PRODUCCION DESDE 16/02/2017

/*
-------------------------------
SPU MODIFICADOS 
--------------------------------
spu_Usuario_GrabarDatos
spu_Usuario_ModificarDatos
spu_Usuario_ObtenerPorId
spu_Carrier_getTractoDisponibles
spu_Carrier_getRemolquesEnPatio
spu_Carrier_getRemolquesEnCortina
spu_Carrier_getEmbarqueEnRuta
spu_Carrier_getEmbarqueEnTienda
spu_Carrier_getTractoRegresando
spu_Carrier_getDetalleAlertaEmbarqueEnRuta
spu_Carrier_InformeArchivo
spu_Carrier_ListarInforme

*/

ALTER TABLE usuario ALTER COLUMN Telefono VARCHAR(4000)
/*===================================================================================================
19/01/2017
===================================================================================================*/
-- BEGIN [

	INSERT INTO TipoGeneral (
           Nombre, Valor, Tipo, Extra1, Extra2, Activo)
    VALUES (
		'Cumplimiento ventana horaria', 'CumplimientoVentanaHoraria', 'CarrierInforme', NULL, NULL, 1)
GO
	INSERT INTO TipoGeneral (
           Nombre, Valor, Tipo, Extra1, Extra2, Activo)
    VALUES (
		'Enganche de carga', 'EngancheDeCarga', 'CarrierInforme', NULL, NULL, 1)
GO
	INSERT INTO TipoGeneral (
           Nombre, Valor, Tipo, Extra1, Extra2, Activo)
    VALUES (
		'Tiempo de tránsito', 'TiempodeTransito', 'CarrierInforme',  NULL, NULL, 1)
GO
	INSERT INTO TipoGeneral (
           Nombre, Valor, Tipo, Extra1, Extra2, Activo)
    VALUES (
		'Tractos regreso', 'TractosRegreso', 'CarrierInforme',  NULL, NULL, 1)

--] END

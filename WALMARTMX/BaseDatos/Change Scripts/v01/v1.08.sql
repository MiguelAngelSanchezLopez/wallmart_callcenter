﻿-- ESTADO: EN PRODUCCION 05/05/2017

/*
--------------------------------
SPU MODIFICADOS 
--------------------------------
spu_Local_GrabarDatos
spu_Local_ObtenerDetalle
spu_Alerta_ObtenerDetalle
spu_Alerta_GrabarNuevaAlertaFuturaMultipunto

*/

/*===================================================================================================
26/04/2017
===================================================================================================*/
ALTER TABLE Local ADD Telefono VARCHAR(4000) NULL

-- [begin: actualiza telefonos de locales
UPDATE
Local
SET Telefono = T.Telefono
FROM
Local AS L
INNER JOIN (
	select
		lpu.IdLocal,
		u.Telefono
	from
	usuario as u
	inner join LocalesPorUsuario as LPU on u.id = lpu.IdUsuario
	where
	Telefono <> ''
	and idPerfil in (52)
) AS T ON (L.Id = T.IdLocal)
-- :end]


INSERT INTO TipoGeneral(Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Volver a llamar en 10 min','Volver a llamar en 10 min','Explicacion','Reprogramar','10',1)
INSERT INTO TipoGeneral(Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Volver a llamar en 20 min','Volver a llamar en 20 min','Explicacion','Reprogramar','20',1)
INSERT INTO TipoGeneral(Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Volver a llamar en 30 min','Volver a llamar en 30 min','Explicacion','Reprogramar','30',1)

INSERT INTO TipoGeneral(Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Volver a llamar en 10 min','Volver a llamar en 10 min','ExplicacionMultipunto','Reprogramar','10',1)
INSERT INTO TipoGeneral(Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Volver a llamar en 20 min','Volver a llamar en 20 min','ExplicacionMultipunto','Reprogramar','20',1)
INSERT INTO TipoGeneral(Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Volver a llamar en 30 min','Volver a llamar en 30 min','ExplicacionMultipunto','Reprogramar','30',1)



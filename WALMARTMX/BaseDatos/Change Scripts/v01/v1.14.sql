﻿-- Estado: EN PRODUCCION 29/08/2017

/*---------------------------------------------
NUEVOS PROCEDIMIENTOS ALMACENADOS / FUNCIONES / VISTAS
---------------------------------------------
fnu_ObtenerVentanaHorarioLocal
spu_Alerta_ObtenerAlertasPorAtender
spu_Alerta_ObtenerDetalle
spu_Job_RevisarCargaPoolPlacas
spu_Sistema_GrabarPoolPlacaHistoria
spu_Sistema_GuardarPoolPlaca
spu_Sistema_ObtenerCargaPoolPlaca
spu_Sistema_ObtenerTablasBasicasSistema
spu_Usuario_GrabarDatosExtras
spu_Usuario_ObtenerDetalle
spu_Usuario_ObtenerEstadoConexionTeleoperador
spu_Usuario_ObtenerEstadoConexionTeleoperadorCemtra
vwu_AlertaEnColaAtencion
vwu_ReportabilidadPatente


*/

/*===================================================================================================
22/08/2017
===================================================================================================*/
-- [begin:
DECLARE @IdCategoria INT, @Orden INT
SET @IdCategoria = (SELECT Id FROM CategoriaPagina WHERE Nombre='Transporte')
SET @Orden = ISNULL((SELECT MAX(Orden) FROM Pagina WHERE IdCategoria = @IdCategoria),0) + 1
INSERT INTO Pagina (Titulo,NombreArchivo,IdCategoria,Orden,Estado,MostrarEnMenu) VALUES ('Cargar Pool Placas','Mantenedor.CargadorPoolPlaca.aspx',@IdCategoria,@Orden,1,1)

DECLARE @IdPagina INT,@IdPerfil INT
SET @IdPagina = (SELECT Id FROM Pagina WHERE NombreArchivo='Mantenedor.CargadorPoolPlaca.aspx')
INSERT INTO FuncionPagina (IdPagina,Nombre,LlaveIdentificador,Orden) VALUES (@IdPagina,'Ver','Ver',1)
-- :end]

/*===================================================================================================
23/08/2017
===================================================================================================*/
CREATE TABLE [dbo].[PoolPlaca](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Placa] [varchar](255) NULL,
	[TipoVehiculo] [varchar](255) NULL,
	[Transporte] [varchar](255) NULL,
	[Cedis] [varchar](255) NULL,
	[Determinante] [int] NULL,
	[IdUsuarioTransportista] [int] NULL,
	[IdCentroDistribucion] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCarga] [datetime] NULL,
 CONSTRAINT [PK_PoolPlaca] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PoolPlaca]  WITH CHECK ADD  CONSTRAINT [FK_PoolPlaca_CentroDistribucion] FOREIGN KEY([IdCentroDistribucion])
REFERENCES [dbo].[CentroDistribucion] ([Id])
GO

ALTER TABLE [dbo].[PoolPlaca]  WITH CHECK ADD  CONSTRAINT [FK_PoolPlaca_Usuario] FOREIGN KEY([IdUsuarioCreacion])
REFERENCES [dbo].[Usuario] ([Id])
GO

ALTER TABLE [dbo].[PoolPlaca]  WITH CHECK ADD  CONSTRAINT [FK_PoolPlaca_UsuarioTransportista] FOREIGN KEY([IdUsuarioTransportista])
REFERENCES [dbo].[Usuario] ([Id])
GO


CREATE TABLE [dbo].[PoolPlacaHistoria](
	[Id] [int] NULL,
	[Placa] [varchar](255) NULL,
	[TipoVehiculo] [varchar](255) NULL,
	[Transporte] [varchar](255) NULL,
	[Cedis] [varchar](255) NULL,
	[Determinante] [int] NULL,
	[IdUsuarioTransportista] [int] NULL,
	[IdCentroDistribucion] [int] NULL,
	[IdUsuarioCreacion] [int] NULL,
	[FechaCarga] [datetime] NULL
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_PoolPlacaHistoria] ON [dbo].[PoolPlacaHistoria]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



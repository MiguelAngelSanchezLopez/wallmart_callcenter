﻿-- ESTADO: PRODUCCION DESDE 23/02/2017

/*
--------------------------------
SPU MODIFICADOS 
--------------------------------
spu_Alerta_GrabarDatosDefinicionAlerta
spu_Alerta_ObtenerDetalleDefincionAlerta
spu_Alerta_ObtenerListadoDefinicionAlertas
spu_Usuario_GrabarDatosExtras
spu_Usuario_ObtenerDetalle
spu_Job_AlertasEnColaAtencion
spu_Job_DesactivarAlertaEnColaAtencionLocalesRMyOtros
spu_Alerta_ObtenerAlertasPorAtender


*/

/*===================================================================================================
20/02/2017
===================================================================================================*/
ALTER TABLE AlertaDefinicion ADD GestionarPorCemtra BIT NULL
UPDATE AlertaDefinicion SET GestionarPorCemtra = 0

ALTER TABLE UsuarioDatoExtra ADD GestionarSoloCemtra BIT NULL
UPDATE UsuarioDatoExtra SET GestionarSoloCemtra = 0

ALTER TABLE UsuarioDatoExtra ADD TransportistasAsociados VARCHAR(4000) NULL



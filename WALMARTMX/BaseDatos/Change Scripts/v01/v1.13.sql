﻿-- Estado: EN PRODUCCION 14/08/2017

/*---------------------------------------------
NUEVOS PROCEDIMIENTOS ALMACENADOS / FUNCIONES / VISTAS
---------------------------------------------
spu_Combo_TeleOperadores
spu_Usuario_ReportePulsoDiario
spu_Usuario_GrabarDatos
spu_Usuario_ModificarDatos
spu_Usuario_ObtenerPorId
spu_Usuario_Actualizar
spu_Alerta_ObtenerListadoConfiguracion

*/

/*===================================================================================================
28/07/2017
===================================================================================================*/
-- begin [
DECLARE @IdCategoria INT, @Orden INT
SET @IdCategoria = (SELECT Id FROM CategoriaPagina WHERE Nombre='CallCenter')
SET @Orden = ISNULL((SELECT MAX(Orden) FROM Pagina WHERE IdCategoria = @IdCategoria),0) + 1
INSERT INTO Pagina (Titulo,NombreArchivo,IdCategoria,Orden,Estado,MostrarEnMenu) VALUES ('Pulso Diario Teleoperador','Supervisor.ReportePulsoDiario.aspx',@IdCategoria,@Orden,1,1)

DECLARE @IdPagina INT,@IdPerfil INT
SET @IdPagina = (SELECT Id FROM Pagina WHERE NombreArchivo='Supervisor.ReportePulsoDiario.aspx')
INSERT INTO FuncionPagina (IdPagina,Nombre,LlaveIdentificador,Orden) VALUES (@IdPagina,'Ver','Ver',1)
-- ] end

INSERT INTO TipoGeneral (Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Matutino (6:00 a 14:00)','Matutino (6:00 a 14:00)','TurnoTeleOperador','06:00','14:00',1)
INSERT INTO TipoGeneral (Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Vespertino (14:00 a 21:30)','Vespertino (14:00 a 21:30)','TurnoTeleOperador','14:00','21:30',1)
INSERT INTO TipoGeneral (Nombre,Valor,Tipo,Extra1,Extra2,Activo) VALUES('Nocturno (22:00 a 5:00)','Nocturno (22:00 a 5:00)','TurnoTeleOperador','22:00','05:00',1)

/*===================================================================================================
02/08/2017
===================================================================================================*/
ALTER TABLE Usuario ADD FechaCaducidadPassword DATETIME NULL
INSERT INTO Configuracion (Clave, Valor) VALUES ('webMesesCaducaClave','2')

UPDATE Usuario SET FechaCaducidadPassword = '20171031 23:59:59.997'

﻿/*===================================================================================================
                                              SCRIPT DE CAMBIOS
===================================================================================================*/

/*---------------------------------------------
PROCEDIMIENTOS ALMACENADOS / FUNCIONES / VISTAS
---------------------------------------------*/
spu_viaje_ObtenerInfoPatenteEcommerce
spu_Viaje_ObtenerEcommerce
spu_Viaje_AsignarDatos
spu_Combo_Ciudad

/*---------------------------------------------
                    SCRIPT
---------------------------------------------*/

--[INICIO]--
-- Se agrega nueva pagina 
DECLARE @idCategoria INT, @orden INT
SET @idCategoria = (SELECT Id FROM CategoriaPagina WHERE Nombre='Planificación')
SET @orden = (SELECT MAX(Orden) FROM Pagina WHERE IdCategoria=@idCategoria) + 1
INSERT INTO Pagina(Titulo,NombreArchivo,IdCategoria,Orden,Estado,MostrarEnMenu) VALUES('Cargar Viajes Ecommerce','Mantenedor.CargaViajeEcommerce.aspx',@idCategoria,@orden,1,1)

-- Se agrega nueva FuncionPagina
DECLARE @idPagina INT,@idPerfil INT
SET @idPagina = (SELECT Id FROM Pagina WHERE NombreArchivo='Mantenedor.CargaViajeEcommerce.aspx')
INSERT INTO FuncionPagina(IdPagina,Nombre,LlaveIdentificador,Orden) VALUES(@idPagina,'Ver','Ver',1)

--[FIN]--

--[INICIO]--
-- Se agrega nueva pagina 
DECLARE @idCategoria INT, @orden INT
SET @idCategoria = (SELECT Id FROM CategoriaPagina WHERE Nombre='Planificación')
SET @orden = (SELECT MAX(Orden) FROM Pagina WHERE IdCategoria=@idCategoria) + 1
INSERT INTO Pagina(Titulo,NombreArchivo,IdCategoria,Orden,Estado,MostrarEnMenu) VALUES('Listado Viajes Ecommerce','Ecommerce.ListadoViaje.aspx',@idCategoria,@orden,1,1)

-- Se agrega nueva FuncionPagina
DECLARE @idPagina INT,@idPerfil INT
SET @idPagina = (SELECT Id FROM Pagina WHERE NombreArchivo='Ecommerce.ListadoViaje.aspx')
INSERT INTO FuncionPagina(IdPagina,Nombre,LlaveIdentificador,Orden) VALUES(@idPagina,'Ver','Ver',1)
INSERT INTO FuncionPagina(IdPagina,Nombre,LlaveIdentificador,Orden) VALUES(@idPagina,'Crear','Crear',2)

--[FIN]--
﻿-- ESTADO: EN PRODUCCION 30/05/2017

/*
--------------------------------
SPU MODIFICADOS 
--------------------------------
SPU_USUARIO_OBTENERESTADOCONEXIONTELEOPERADOR
SPU_USUARIO_OBTENERESTADOCONEXIONTELEOPERADORCEMTRA

*/

/*===================================================================================================
17/05/2017
===================================================================================================*/


-- BEGIN [
DECLARE @IDCATEGORIA INT, @ORDEN INT
SET @IDCATEGORIA = (SELECT ID FROM CATEGORIAPAGINA WHERE NOMBRE='CALLCENTER')
SET @ORDEN = (SELECT MAX(ORDEN) FROM PAGINA WHERE IDCATEGORIA=@IDCATEGORIA) + 1
INSERT INTO PAGINA(TITULO,NOMBREARCHIVO,IDCATEGORIA,ORDEN,ESTADO,MOSTRARENMENU) VALUES('Estado Conexión Teleoperador CEMTRA','Supervisor.EstadoConexionTeleoperadorCemtra.aspx',@IDCATEGORIA,@ORDEN,1,1)

DECLARE @IDPAGINA INT,@IDPERFIL INT
SET @IDPAGINA = (SELECT ID FROM PAGINA WHERE NOMBREARCHIVO='Supervisor.EstadoConexionTeleoperadorCemtra.aspx')
INSERT INTO FUNCIONPAGINA(IDPAGINA,NOMBRE,LLAVEIDENTIFICADOR,ORDEN) VALUES(@IDPAGINA,'Ver','Ver',1)

SET @IDPERFIL = (SELECT ID FROM PERFIL WHERE NOMBRE='SUPERVISOR')
INSERT INTO PAGINASXPERFIL(IDPERFIL,IDPAGINA) VALUES(@IDPERFIL,@IDPAGINA)
GO

UPDATE PAGINA SET
TITULO = 'Estado Conexión Teleoperador Visibilidad'
WHERE NombreArchivo = 'Supervisor.EstadoConexionTeleoperador.aspx'
GO
-- ] end

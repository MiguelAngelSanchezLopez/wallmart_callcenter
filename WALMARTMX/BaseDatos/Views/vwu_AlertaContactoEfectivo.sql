﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_AlertaContactoEfectivo')
	BEGIN
	  PRINT  'Dropping Procedure vwu_AlertaContactoEfectivo'
		DROP  View dbo.vwu_AlertaContactoEfectivo
	END
GO

PRINT  'Creating Procedure vwu_AlertaContactoEfectivo'
GO
CREATE VIEW dbo.vwu_AlertaContactoEfectivo
/******************************************************************************
**    Descripcion  : obtiene listado de alertas gestionadas marca cuales fueron contactadas (NoContesta=0) y cuales no (NoContesta=1)
**    Por          : VSR, 26/09/2014
*******************************************************************************/
AS
  SELECT DISTINCT
    IdAlerta = A.Id,
    NombreAlerta = A.DescripcionAlerta,
    NroTransporte = A.NroTransporte,
    IdEmbarque = A.IdEmbarque,
    Prioridad = AC.Prioridad,
    NombreTransportista = ISNULL(A.NombreTransportista,''),
    LocalDestino = ISNULL(L.Nombre,''),
    LocalDestinoCodigo = ISNULL(CONVERT(VARCHAR,L.CodigoInterno),''),
    TipoAlerta = ISNULL(A.TipoAlerta,''),
    IdFormato = L.IdFormato,
    NombreFormato = ISNULL(F.Nombre,''),
    Zona = ISNULL(A.Permiso,''),
    TipoViaje = ISNULL(TV.TipoViaje,''),
    FechaCreacion = A.FechaHoraCreacion,
    NroEscalamiento = CONVERT(VARCHAR,EPA.Orden),
    GrupoContacto = CASE WHEN HEC.IdEscalamientoPorAlertaContacto IS NULL THEN UPPER(P.Nombre) ELSE UPPER(EPAGC.Nombre) END,
    NombreContacto = CASE WHEN HEC.IdEscalamientoPorAlertaContacto IS NULL THEN
                       ISNULL(USinGrupo.Nombre,'') + ISNULL(' ' + USinGrupo.Paterno,'') + ISNULL(' ' + USinGrupo.Materno,'')
                     ELSE
       	               ISNULL(UConGrupo.Nombre,'') + ISNULL(' ' + UConGrupo.Paterno,'') + ISNULL(' ' + UConGrupo.Materno,'')
                     END,
    IdHistorialeEcalamientoContacto = HEC.Id,
    Explicacion = ISNULL(HEC.Explicacion,''),
    Observacion = ISNULL(HEC.Observacion,''),
    AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    NoContesta = NC.NoContesta,
    Eliminado = ISNULL(CONVERT(INT, EPA.Eliminado),0),
    GestionarPorCemtra = ISNULL(AD.GestionarPorCemtra, 0)
  FROM
    CallCenterHistorialEscalamiento AS HE
    INNER JOIN Alerta AS A ON (HE.IdAlerta = A.Id)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
    INNER MERGE JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    INNER JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
    INNER JOIN Usuario AS U ON (HE.IdUsuarioCreacion = U.Id)
    INNER JOIN CallCenterHistorialEscalamientoContacto AS HEC ON (HE.Id = HEC.IdHistorialEscalamiento)
    LEFT MERGE JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
    LEFT JOIN EscalamientoPorAlertaContacto AS EPAC ON (HEC.IdEscalamientoPorAlertaContacto = EPAC.Id)
    LEFT JOIN EscalamientoPorAlertaGrupoContacto AS EPAGC ON (EPAC.IdEscalamientoPorAlertaGrupoContacto = EPAGC.Id)
    LEFT JOIN Usuario AS UConGrupo ON (EPAC.IdUsuario = UConGrupo.Id)
    LEFT JOIN Usuario AS USinGrupo ON (HEC.IdUsuarioSinGrupoContacto = USinGrupo.Id)
    LEFT JOIN Perfil AS P ON (USinGrupo.IdPerfil = P.Id)
    LEFT JOIN vwu_NoContesta AS NC ON (A.Id = NC.IdAlerta)

﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_AlertaEnColaAtencion')
	BEGIN
	  PRINT  'Dropping Procedure vwu_AlertaEnColaAtencion'
		DROP  View dbo.vwu_AlertaEnColaAtencion
	END
GO

PRINT  'Creating Procedure vwu_AlertaEnColaAtencion'
GO
CREATE VIEW dbo.vwu_AlertaEnColaAtencion
/******************************************************************************
**    Descripcion  : obtiene listado de alertas que estan en la cola de atencion
**    Por          : VSR, 26/09/2014
*******************************************************************************/
AS
  SELECT DISTINCT
    Clasificacion = 'EN COLA DE ATENCION',
    IdAlerta = A.Id,
    NombreAlerta = APG.NombreAlerta,
    NroTransporte = APG.NroTransporte,
    IdEmbarque = APG.IdEmbarque,
    Prioridad = AC.Prioridad,
    NombreTransportista = ISNULL(A.NombreTransportista,''),
    LocalDestino = ISNULL(L.Nombre,''),
    LocalDestinoCodigo = ISNULL(CONVERT(VARCHAR,L.CodigoInterno),''),
    IdFormato = L.IdFormato,
    NombreFormato = ISNULL(F.Nombre,''),
    Zona = ISNULL(A.Permiso,''),
    TipoViaje = ISNULL(TV.TipoViaje,''),
    FechaCreacion = A.FechaHoraCreacion,
    AlertaMapa = a.AlertaMapa,
    Cerrado = CONVERT(INT,ISNULL(CCA.Cerrado,0)),
    CerradoSatisfactorio = CONVERT(INT,ISNULL(CCA.CerradoSatisfactorio,0)),
    GestionarPorCemtra = ISNULL(AD.GestionarPorCemtra, 0)
  FROM
    AlertaEnColaAtencion AS APG
    INNER JOIN CallCenterAlerta AS CCA ON (APG.IdAlerta = CCA.IdAlerta)
    INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN Local AS L ON (APG.LocalDestino = L.CodigoInterno)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = APG.NombreAlerta)
    INNER JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    LEFT JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
  WHERE
    CCA.Desactivada = 0
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_AlertaPorEjecutivo')
	BEGIN
	  PRINT  'Dropping Procedure vwu_AlertaPorEjecutivo'
		DROP  View dbo.vwu_AlertaPorEjecutivo
	END
GO

PRINT  'Creating Procedure vwu_AlertaPorEjecutivo'
GO
CREATE VIEW dbo.vwu_AlertaPorEjecutivo
/******************************************************************************
**    Descripcion  : obtiene listado de alertas atendidas por ejecutivo
**    Por          : VSR, 26/09/2014
*******************************************************************************/
AS
  SELECT DISTINCT
    IdAlerta = A.Id,
    NombreAlerta = A.DescripcionAlerta,
    NroTransporte = A.NroTransporte,
    IdEmbarque = A.IdEmbarque,
    Prioridad = AC.Prioridad,
    NombreTransportista = ISNULL(A.NombreTransportista,''),
    LocalDestino = ISNULL(L.Nombre,''),
    LocalDestinoCodigo = ISNULL(CONVERT(VARCHAR,L.CodigoInterno),''),
    IdFormato = L.IdFormato,
    NombreFormato = ISNULL(F.Nombre,''),
    Zona = ISNULL(A.Permiso,''),
    TipoViaje = ISNULL(TV.TipoViaje,''),
    IdUsuarioCreacion = ISNULL(HE.IdUsuarioCreacion,-1),
    AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    UsuarioActivo = ISNULL(U.Estado,0),
    FechaCreacion = A.FechaHoraCreacion,
    NroEscalamiento = CONVERT(VARCHAR,EPA.Orden),
    FechaInicioGestion = AGA.FechaAsignacion,
    FechaTerminoGestion = AGA.FechaTerminoGestion,
    MinutosGestion = DATEDIFF(mi,AGA.FechaAsignacion,AGA.FechaTerminoGestion),
    Eliminado = ISNULL(CONVERT(INT, EPA.Eliminado),0),
    NoContesta = VNC.NoContesta,
    GestionarPorCemtra = ISNULL(AD.GestionarPorCemtra, 0)
  FROM
    CallCenterHistorialEscalamiento AS HE
    INNER MERGE JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
    INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
    INNER MERGE JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    INNER JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
    INNER JOIN Usuario AS U ON (HE.IdUsuarioCreacion = U.Id)
    LEFT JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
    LEFT JOIN CallCenterAlertaGestionAcumulada AS AGA ON (HE.Id = AGA.IdHistorialEscalamiento)
    LEFT MERGE JOIN vwu_NoContesta AS VNC ON (A.Id = VNC.IdAlerta)
  WHERE
    CCA.Desactivada = 0

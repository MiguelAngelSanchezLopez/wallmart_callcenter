﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_ReportabilidadPatente')
	BEGIN
	  PRINT  'Dropping Procedure vwu_ReportabilidadPatente'
		DROP  View dbo.vwu_ReportabilidadPatente
	END
GO

PRINT  'Creating Procedure vwu_ReportabilidadPatente'
GO
CREATE VIEW dbo.vwu_ReportabilidadPatente
/******************************************************************************
**    Descripcion  : Obtiene la reportabilidad de las patentes
**    Por          : VSR, 20/07/2016
*******************************************************************************/
AS
  SELECT
    TLP.Patente,
    Fecha = dbo.fnu_ConvertirDatetimeToDDMMYYYY(TLP.Fecha,'FECHA_COMPLETA'),
    Estado = CASE
               WHEN TLP.Fecha IS NULL THEN 'NoIntegrada'
               WHEN TLP.Fecha < DATEADD(HOUR, -12, dbo.fnu_GETDATE()) THEN 'Offline'
               ELSE 'Online'   
             End                
  FROM
    Track_LastPositions AS TLP



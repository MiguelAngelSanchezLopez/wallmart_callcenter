﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_AlertaAtendida')
	BEGIN
	  PRINT  'Dropping View vwu_AlertaAtendida'
		DROP  View dbo.vwu_AlertaAtendida
	END
GO

PRINT  'Creating View vwu_AlertaAtendida'
GO
CREATE VIEW dbo.vwu_AlertaAtendida
/******************************************************************************
**    Descripcion  : obtiene listado de alertas atendidas por callcenter
**    Por          : VSR, 26/09/2014
*******************************************************************************/
AS

  SELECT DISTINCT
    Clasificacion = 'ATENDIDA',
    IdAlerta = A.Id,
    NombreAlerta = A.DescripcionAlerta,
    NroTransporte = A.NroTransporte,
    IdEmbarque = A.IdEmbarque,
    Prioridad = AC.Prioridad,
    NombreTransportista = ISNULL(A.NombreTransportista,''),
    LocalDestino = ISNULL(L.Nombre,''),
    LocalDestinoCodigo = ISNULL(CONVERT(VARCHAR,L.CodigoInterno),''),
    IdFormato = L.IdFormato,
    NombreFormato = ISNULL(F.Nombre,''),
    Zona = ISNULL(A.Permiso,''),
    TipoViaje = ISNULL(TV.TipoViaje,''),
    FechaCreacion = A.FechaHoraCreacion,
    NroEscalamiento = CONVERT(VARCHAR,EPA.Orden),
    AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
    Eliminado = ISNULL(CONVERT(INT, EPA.Eliminado),0),
    CategoriaAlerta = ISNULL(hec.CategoriaAlerta,''),
    TipoObservacion = ISNULL(hec.TipoObservacion,''),
    AlertaMapa = ISNULL(a.AlertaMapa,''),
    RespuestaCategoria = ISNULL(rc.Nombre,''),
    RespuestaObservacion = ISNULL(ra.Observacion,''),
    Observacion = ISNULL(he.Observacion,''),
    ClasificacionAlerta = ISNULL(he.clasificacionAlerta,''),
    PatenteTracto = ISNULL(A.PatenteTracto,''),
    NombreConductor = ISNULL(A.NombreConductor,''),
    RutConductor = ISNULL(c.Rut,''),
    dvConductor = ISNULL(c.dv,''),
    TipoAlerta = ISNULL(a.TipoAlerta,''),
    Permiso =  ISNULL(a.Permiso,''),
	  LatTracto =  ISNULL(a.LatTracto,''),
	  LonTracto =  ISNULL(a.LonTracto,''),
    Cerrado = CONVERT(INT,ISNULL(CCA.Cerrado,0)),
    CerradoSatisfactorio = CONVERT(INT,ISNULL(CCA.CerradoSatisfactorio,0)),
    GestionarPorCemtra = ISNULL(AD.GestionarPorCemtra, 0)
  FROM
    CallCenterHistorialEscalamiento AS HE
    INNER MERGE JOIN CallCenterAlerta AS CCA ON (HE.IdAlerta = CCA.IdAlerta)
    INNER MERGE JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)
    INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)
    INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)
    INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)
    INNER MERGE JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)
    INNER JOIN Formato AS F ON (L.IdFormato = F.Id)
    INNER JOIN Usuario AS U ON (HE.IdUsuarioCreacion = U.Id)
    LEFT JOIN TrazaViaje AS TV ON (A.NroTransporte = TV.NroTransporte AND A.LocalDestino = TV.LocalDestino)
    LEFT JOIN EscalamientoPorAlerta AS EPA ON (HE.IdEscalamientoPorAlerta = EPA.Id)
    LEFT JOIN CallCenterHistorialEscalamientoContacto AS hec ON he.Id = hec.idhistorialescalamiento
    LEFT JOIN RespuestaAlerta ra ON RA.idAlerta = A.Id
    LEFT MERGE JOIN RespuestaCategoria rc ON (RC.id = RA.idRespuestaCategoria)
    LEFT JOIN Conductores c ON (c.Nombre = a.NombreConductor) 
  WHERE
      A.Activo = 1
  AND CCA.Desactivada = 0

﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_PerfilesPorFormato')
	BEGIN
	  PRINT  'Dropping View vwu_PerfilesPorFormato'
		DROP  View dbo.vwu_PerfilesPorFormato
	END
GO

PRINT  'Creating View vwu_PerfilesPorFormato'
GO
CREATE VIEW dbo.vwu_PerfilesPorFormato
/******************************************************************************
**    Descripcion  : obtiene los perfiles especiales asociados por formato
**    Por          : VSR, 16/04/2015
*******************************************************************************/ 
AS         
  -------------------------------------------------
  -- obtiene perfiles que se deben mostrar siempre
  -------------------------------------------------
  SELECT
    IdFormato = F.Id,
    IdPerfil = P.Id,
    NombrePerfil = ISNULL(P.Nombre,''),
    LlavePerfil = ISNULL(P.Llave,''),
    Tipo = 'MostrarSiempre'
  FROM
    Perfil AS P,
    Formato AS F
  WHERE
    P.Llave IN ('TRA','CON','CLT')

  -------------------------------------------------
  -- obtiene perfiles asociados al local
  -------------------------------------------------
  UNION ALL
  SELECT
    IdFormato = PPF.IdFormato,
    IdPerfil = P.Id,
    NombrePerfil = ISNULL(P.Nombre,''),
    LlavePerfil = ISNULL(P.Llave,''),
    Tipo = 'Local'
  FROM
    PerfilesPorFormato AS PPF
    INNER JOIN Perfil AS P ON (PPF.IdPerfil = P.Id)

  -------------------------------------------------
  -- obtiene perfiles asociados al centro distribucion
  -------------------------------------------------
  UNION ALL
  SELECT
    IdFormato = F.Id,
    IdPerfil = P.Id,
    NombrePerfil = ISNULL(P.Nombre,''),
    LlavePerfil = ISNULL(P.Llave,''),
    Tipo = 'CentroDistribucion'
  FROM
    PerfilesPorCentroDistribucion AS PPCD
    INNER JOIN Perfil AS P ON (PPCD.IdPerfil = P.Id),
    Formato AS F

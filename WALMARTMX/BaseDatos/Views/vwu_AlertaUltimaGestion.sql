﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_AlertaUltimaGestion')
	BEGIN
	  PRINT  'Dropping Procedure vwu_AlertaUltimaGestion'
		DROP  View dbo.vwu_AlertaUltimaGestion
	END
GO

PRINT  'Creating Procedure vwu_AlertaUltimaGestion'
GO
CREATE VIEW dbo.vwu_AlertaUltimaGestion
/******************************************************************************
**    Descripcion  : obtiene la ultima gestion de la alerta
**    Por          : VSR, 11/07/2014
*******************************************************************************/
AS
    SELECT
      IdAlertaPadre = HAG.IdAlertaPadre,
      Accion = HAG.Accion,
      IdUsuarioCreacion = HAG.IdUsuarioCreacion,
      AtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),
      FechaCreacion = HAG.FechaCreacion
    FROM
      CallCenterHistorialAlertaGestion AS HAG
      INNER JOIN (
                  SELECT
                    IdHistorialAlertaGestion = MAX(HAG.Id),
                    IdAlertaPadre = HAG.IdAlertaPadre
                  FROM
                    CallCenterHistorialAlertaGestion AS HAG
                  GROUP BY
                    HAG.IdAlertaPadre
      ) AS T ON (HAG.IdAlertaPadre = T.IdAlertaPadre AND HAG.Id = T.IdHistorialAlertaGestion)
      INNER JOIN Usuario AS U ON (HAG.IdUsuarioCreacion = U.Id)
﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_AlertaAcumulada')
	BEGIN
	  PRINT  'Dropping Procedure vwu_AlertaAcumulada'
		DROP  View dbo.vwu_AlertaAcumulada
	END
GO

PRINT  'Creating Procedure vwu_AlertaAcumulada'
GO
CREATE VIEW dbo.vwu_AlertaAcumulada    
/******************************************************************************    
**    Descripcion  : obtiene listado de alertas agrupadas    
**    Por          : VSR, 14/07/2014    
*******************************************************************************/    
AS    
  ---------------------------------------------------------------------------------------        
  -- ULTIMO PASO:    
  -- calcula para las alertas si se deben volver a gestionar despues de haber transcurrido X tiempo de frecuencia        
  ---------------------------------------------------------------------------------------        
  SELECT        
    T.IdAlertaPadre,        
    T.IdAlertaHija,        
    T.NroTransporte,   
    T.IdEmbarque,
    T.NombreAlerta,    
    T.LocalDestino,    
    T.IdFormato,    
    T.LatTracto,    
    T.LonTracto,    
    T.FechaHoraCreacion,        
    T.NroEscalamientoActual,        
    T.TotalEscalamientos,        
    T.Cerrado,        
    T.FrecuenciaRepeticionMinutos,        
    T.UltimaGestionAccion,        
    T.UltimaGestionIdUsuarioCreacion,        
    UltimaGestionAtendidoPor = ISNULL(U.Nombre,'') + ISNULL(' ' + U.Paterno,'') + ISNULL(' ' + U.Materno,''),        
    T.UltimaGestionFechaCreacion,        
    DiferenciaMinutos = CASE WHEN ISNULL(DATEDIFF(mi, T.UltimaGestionFechaCreacion, T.FechaHoraCreacion),-1) < -99999 THEN -99999 ELSE ISNULL(DATEDIFF(mi, T.UltimaGestionFechaCreacion, T.FechaHoraCreacion),-1) END, -- se coloca como tope -99999 solo por si en el futuro ocasiona problema de desbordamiento        
    VolverGestionarPorFrecuenciaRepeticion = CASE WHEN        
                                                ( T.FechaHoraCreacion <= dbo.fnu_GETDATE() )        
                                                AND    
                                                ( T.GestionadaPorCallCenter = 0 )    
                                                AND        
                                                (         
                                                     (T.UltimaGestionFechaCreacion IS NULL)        
                                                  OR (T.UltimaGestionAccion = 'TELEOPERADOR_OCUPADO')    
                                                  OR (T.UltimaGestionAccion = 'TELEOPERADOR_CERRAR_SESION')    
                                                  OR (T.UltimaGestionAccion LIKE 'TELEOPERADOR_AUXILIAR_%')    
                                                )        
                                             THEN 1 ELSE 0 END,    
     T.MaximoHorasAlertaActiva,    
     T.GrupoAlerta    
  FROM (        
        ---------------------------------------------------------------------------------------        
        -- PASO 1:        
        -- obtiene informacion de las alerta padre e hijas    
        ---------------------------------------------------------------------------------------        
        SELECT    
          IdAlertaPadre = CCA.IdAlertaPadre,    
          IdAlertaHija = CCA.IdAlerta,    
          NroTransporte = A.NroTransporte,    
          IdEmbarque = A.IdEmbarque,
          NombreAlerta = A.DescripcionAlerta,    
          LocalDestino = A.LocalDestino,    
          IdFormato = L.IdFormato,    
          LatTracto = A.LatTracto,    
          LonTracto = A.LonTracto,    
          FechaHoraCreacion = A.FechaHoraCreacion,    
          NroEscalamientoActual = ISNULL((    
                                            SELECT TOP 1    
                                              auxEPA.Orden    
                                            FROM    
                                              CallCenterHistorialEscalamiento AS auxHE    
                                              INNER JOIN EscalamientoPorAlerta AS auxEPA ON (auxHE.IdEscalamientoPorAlerta = auxEPA.Id)    
                                            WHERE    
                                                auxHE.IdAlerta = CCA.IdAlerta    
                                            AND auxEPA.Eliminado = 0    
                                            ORDER BY    
                                              auxEPA.Orden DESC    
                        ),0),    
          TotalEscalamientos = ISNULL((    
                                        SELECT    
                                          COUNT(EPA.Id)    
                                        FROM    
                                          AlertaConfiguracion AS AC    
                                          INNER JOIN EscalamientoPorAlerta AS EPA ON (AC.Id = EPA.IdAlertaConfiguracion)    
                                          INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (AC.IdAlertaDefinicionPorFormato = ADPF.Id)    
                                          INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id)    
                                        WHERE    
                                            AD.Nombre = A.DescripcionAlerta    
                                        AND ADPF.IdFormato = L.IdFormato    
                                        AND AC.Activo = 1    
                                        AND EPA.Eliminado = 0    
                               ),0),    
          Cerrado = CONVERT(INT,ISNULL(CCA.Cerrado,0)),    
          FrecuenciaRepeticionMinutos = AC.FrecuenciaRepeticionMinutos,    
          UltimaGestionAccion = ISNULL(AUG.Accion,''),    
          UltimaGestionIdUsuarioCreacion = ISNULL(AUG.IdUsuarioCreacion,-1),    
          UltimaGestionFechaCreacion = AUG.FechaCreacion,    
          MaximoHorasAlertaActiva = ISNULL(L.MaximoHorasAlertaActiva,-1),    
          GrupoAlerta = A.GrupoAlerta,    
          GestionadaPorCallCenter = CASE WHEN TablaAlertaGestionada.IdHistorialAlertaGestion IS NULL THEN 0 ELSE 1 END    
        FROM    
          CallCenterAlerta AS CCA    
          INNER JOIN Alerta AS A ON (CCA.IdAlerta = A.Id)    
          INNER JOIN Local AS L ON (A.LocalDestino = L.CodigoInterno)    
          INNER JOIN AlertaDefinicionPorFormato AS ADPF ON (ADPF.IdFormato = L.IdFormato)    
          INNER JOIN AlertaDefinicion AS AD ON (ADPF.IdAlertaDefinicion = AD.Id AND AD.Nombre = A.DescripcionAlerta)    
          INNER JOIN AlertaConfiguracion AS AC ON (ADPF.Id = AC.IdAlertaDefinicionPorFormato)    
          LEFT JOIN vwu_AlertaUltimaGestion AS AUG ON (CCA.IdAlerta = AUG.IdAlertaPadre)    
          LEFT JOIN (    
                     SELECT    
                       IdHistorialAlertaGestion = aux.Id,    
                       aux.IdAlertaPadre,     
                       aux.IdAlertaHija    
                     FROM    
                       CallCenterHistorialAlertaGestion AS aux    
                     WHERE    
                       aux.Accion = 'TELEOPERADOR_ALERTA_GRABADA'    
          ) AS TablaAlertaGestionada ON (CCA.IdAlertaPadre = TablaAlertaGestionada.IdAlertaPadre AND CCA.IdAlerta = TablaAlertaGestionada.IdAlertaHija)    
        WHERE    
            A.Activo = 1    
        AND AC.Activo = 1    
        AND CCA.Desactivada = 0    
        AND A.FechaHoraCreacion >= AC.FechaInicio    
        ---------------------------------------------------------------------------------------    
        -- FIN PASO 1    
        ---------------------------------------------------------------------------------------    
    
  ) AS T        
  LEFT JOIN Usuario AS U ON (T.UltimaGestionIdUsuarioCreacion = U.Id)    
  
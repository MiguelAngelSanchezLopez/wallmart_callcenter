﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'V' AND name = 'vwu_AlertaPorGestionar')
	BEGIN
	  PRINT  'Dropping Procedure vwu_AlertaPorGestionar'
		DROP  View dbo.vwu_AlertaPorGestionar
	END
GO

PRINT  'Creating Procedure vwu_AlertaPorGestionar'
GO
CREATE VIEW dbo.vwu_AlertaPorGestionar      
/******************************************************************************      
**    Descripcion  : Obtiene listado de alertas pendientes por gestionar      
**    Por          : VSR, 11/07/2014      
*******************************************************************************/      
AS      
  ---------------------------------------------------------------------------------------      
  -- PASO 3:            
  -- de todas las alertas hijas que se encuentran sin gestion obtiene el id más pequeño      
  ---------------------------------------------------------------------------------------    
  SELECT      
    IdAlertaPadre = T.IdAlertaPadre,      
    IdAlertaHija = MIN(T.IdAlertaHija),      
    Gestionada = T.Gestionada,      
    NroTransporte = T.NroTransporte,      
    IdEmbarque = T.IdEmbarque,   
    NombreAlerta = T.NombreAlerta,    
    LocalDestino = T.LocalDestino,    
    IdFormato = T.IdFormato,    
    GrupoAlerta = T.GrupoAlerta,      
    FechaHoraCreacion = MIN(T.FechaHoraCreacion)      
  FROM      
  (    
      ---------------------------------------------------------------------------------------      
      -- PASO 2:            
      -- De las alertas que estan acumuladas obtiene las alertas que estan dentro de las horas activas por local            
      ---------------------------------------------------------------------------------------            
      SELECT      
      T.*      
      FROM (      
            ---------------------------------------------------------------------------------------            
            -- PASO 1:      
            -- De las alertas que estan acumuladas indica cual se ha gestionado con anterioridad. Las alertas se agrupan en Padre e Hija      
            ---------------------------------------------------------------------------------------      
            SELECT      
              IdAlertaPadre = AA.IdAlertaPadre,      
              IdAlertaHija = AA.IdAlertaHija,    
              Gestionada = CASE WHEN (    
                                        -- revisa que si una alerta del grupo esta siendo atendida, entonces lo marca como gestionada    
                                        SELECT TOP 1    
                                          auxHAG.Accion    
                                        FROM    
                                          CallCenterHistorialAlertaGestion AS auxHAG    
                                          INNER JOIN Alerta AS auxA ON (auxHAG.IdAlertaPadre = auxA.Id)    
                                        WHERE    
                                            auxHAG.IdAlertaPadre = AA.IdAlertaPadre     
                                        AND auxA.GrupoAlerta = AA.GrupoAlerta    
                                        ORDER BY    
                                        auxHAG.FechaCreacion DESC                                       
                                      ) = 'TELEOPERADOR_ALERTA_ASIGNADA' THEN 1    
                            ELSE    
                                CASE WHEN (      
                                    ISNULL(AGA.IdAlertaHija,0) = 0       
                                    OR AA.UltimaGestionAccion = 'TELEOPERADOR_OCUPADO'       
                                    OR AA.UltimaGestionAccion = 'TELEOPERADOR_CERRAR_SESION'      
                                    OR AA.UltimaGestionAccion LIKE 'TELEOPERADOR_AUXILIAR%'    
                                    )      
                                    THEN 0 ELSE 1       
                                END                      
                            END,                    
              NroTransporte = AA.NroTransporte,    
              IdEmbarque = AA.IdEmbarque,
              NombreAlerta = AA.NombreAlerta,    
              LocalDestino = AA.LocalDestino,    
              IdFormato = AA.IdFormato,      
              FechaHoraCreacion = AA.FechaHoraCreacion,      
              NroEscalamientoActual = AA.NroEscalamientoActual,      
              TotalEscalamientos = AA.TotalEscalamientos,      
              UltimaGestionAccion = AA.UltimaGestionAccion,      
              Cerrado = AA.Cerrado,      
              MaximoHorasAlertaActiva = AA.MaximoHorasAlertaActiva,      
              DiferenciaHoras = CASE WHEN AA.MaximoHorasAlertaActiva = -1 THEN -1 ELSE DATEDIFF(hh, AA.FechaHoraCreacion, dbo.fnu_GETDATE()) END,    
              GrupoAlerta = AA.GrupoAlerta    
            FROM      
              vwu_AlertaAcumulada AS AA      
              LEFT JOIN CallCenterAlertaGestionAcumulada AS AGA ON (AA.IdAlertaHija = AGA.IdAlertaHija)      
            WHERE      
            -- obtiene las alertas que aun no han llegado al ultimo escalamiento      
            AA.NroEscalamientoActual < AA.TotalEscalamientos      
            --obtiene las alertas que no estan asignadas actualmente a un teleoperador      
            AND AA.UltimaGestionAccion <> 'TELEOPERADOR_ALERTA_ASIGNADA'      
            --obtiene las alertas que aun no se han cerrado      
            AND AA.Cerrado = 0      
            --obtiene las alertas se deben volver a gestionar despues de haber transcurrido un tiempo de repeticion      
            AND AA.VolverGestionarPorFrecuenciaRepeticion = 1    
            ---------------------------------------------------------------------------------------            
            -- FIN PASO 1            
            ---------------------------------------------------------------------------------------            
      ) AS T      
      WHERE      
        --obtiene las alertas que estan dentro de las horas activas por local      
        (T.DiferenciaHoras = -1 OR T.DiferenciaHoras <= T.MaximoHorasAlertaActiva)            
      ---------------------------------------------------------------------------------------      
      -- FIN PASO 2      
      ---------------------------------------------------------------------------------------      
  ) AS T      
  WHERE      
    T.Gestionada = 0      
  GROUP BY      
    T.IdAlertaPadre, T.Gestionada, T.NroTransporte, T.IdEmbarque, T.NombreAlerta, T.LocalDestino, T.IdFormato, T.GrupoAlerta 


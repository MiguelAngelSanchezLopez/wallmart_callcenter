﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class wucMultiselectCombinados

  '''<summary>
  '''Control pnlBloque.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlBloque As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control lblPadre.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblPadre As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lstPadre.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lstPadre As Global.System.Web.UI.WebControls.ListBox

  '''<summary>
  '''Control holderCheckTodosPadre.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents holderCheckTodosPadre As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control chkTodosPadre.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents chkTodosPadre As Global.System.Web.UI.WebControls.CheckBox

  '''<summary>
  '''Control lbtnAgregarItem.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lbtnAgregarItem As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control lbtnQuitarItem.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lbtnQuitarItem As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control txtIdsHija.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtIdsHija As Global.System.Web.UI.WebControls.HiddenField

  '''<summary>
  '''Control lblHija.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblHija As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lstHija.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lstHija As Global.System.Web.UI.WebControls.ListBox

  '''<summary>
  '''Control holderCheckTodosHija.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents holderCheckTodosHija As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control chkTodosHija.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents chkTodosHija As Global.System.Web.UI.WebControls.CheckBox

  '''<summary>
  '''Control txtFuncionesEspeciales.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtFuncionesEspeciales As Global.System.Web.UI.HtmlControls.HtmlInputHidden

  '''<summary>
  '''Control lblAyuda.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblAyuda As Global.System.Web.UI.WebControls.Label
End Class

﻿Imports CapaNegocio

Public Class wucHora
  Inherits System.Web.UI.UserControl

#Region "Propiedades del control"
  'variables privadas
  Private _selectedValueHora As String
  Private _selectedValueMinutos As String
  Private _inicializarHoraSistema As Boolean
  Private _inicializarVacio As Boolean

  'SelectedValueHora: Retorna o asigna el value del item seleccionado de la hora
  Public Property SelectedValueHora() As String
    Get
      'Si el combo esta vacio, lo llena
      If ddlHora.Items.Count = 0 Then
        CargarHoras()
      End If
      Return ddlHora.SelectedValue
    End Get
    Set(ByVal Value As String)
      _selectedValueHora = Value
      'Si el combo esta vacio, lo llena
      If ddlHora.Items.Count = 0 Then
        CargarHoras()
      End If
      'si ya esta lleno entonces busca el valor para seleccionarlo
      If ddlHora.Items.Count > 0 Then
        'Solo si existe el valor en la lista, lo deja seleccionado
        If Not ddlHora.Items.FindByValue(_selectedValueHora) Is Nothing Then
          ddlHora.SelectedValue = _selectedValueHora
        End If
      End If
    End Set
  End Property

  'SelectedValueMinutos: Retorna o asigna el value del item seleccionado de los minutos
  Public Property SelectedValueMinutos() As String
    Get
      'Si el combo esta vacio, lo llena
      If ddlMinutos.Items.Count = 0 Then
        CargarMinutos()
      End If
      Return ddlMinutos.SelectedValue
    End Get
    Set(ByVal Value As String)
      _selectedValueMinutos = Value
      'Si el combo esta vacio, lo llena
      If ddlMinutos.Items.Count = 0 Then
        CargarMinutos()
      End If
      'si ya esta lleno entonces busca el valor para seleccionarlo
      If ddlMinutos.Items.Count > 0 Then
        'Solo si existe el valor en la lista, lo deja seleccionado
        If Not ddlMinutos.Items.FindByValue(_selectedValueMinutos) Is Nothing Then
          ddlMinutos.SelectedValue = _selectedValueMinutos
        End If
      End If
    End Set
  End Property

  'InicializarHoraSistema: Determina si el control se va a cargar con la hora del sistema
  Public Property InicializarHoraSistema() As Boolean
    Get
      Return _inicializarHoraSistema
    End Get
    Set(ByVal Value As Boolean)
      _inicializarHoraSistema = Value
    End Set
  End Property

  'Enabled: Permite indicar si el control esta habilitado o no
  Public Property Enabled() As Boolean
    Get
      Return ddlHora.Enabled
      Return ddlMinutos.Enabled
    End Get
    Set(ByVal Value As Boolean)
      ddlHora.Enabled = Value
      ddlMinutos.Enabled = Value
    End Set
  End Property

  Public Property InicializarVacio() As Boolean
    Get
      Return _inicializarVacio
    End Get
    Set(ByVal Value As Boolean)
      _inicializarVacio = Value
    End Set
  End Property

#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' carga el combo de horas
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CargarHoras()
    Dim totalHoras As Integer = 23
    Dim i As Integer
    Dim item As ListItem
    Dim newHora As String

    For i = 0 To totalHoras
      newHora = IIf(i <= 9, "0" & CStr(i), CStr(i))
      item = New ListItem(newHora, newHora)
      ddlHora.Items.Insert(i, item)
    Next

    item = New ListItem("--", "")
    ddlHora.Items.Insert(0, item)
  End Sub

  ''' <summary>
  ''' carga el combo de minutos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CargarMinutos()
    Dim totalMinutos As Integer = 59
    Dim i As Integer
    Dim item As ListItem
    Dim newMinutos As String

    For i = 0 To totalMinutos
      newMinutos = IIf(i <= 9, "0" & CStr(i), CStr(i))
      item = New ListItem(newMinutos, newMinutos)
      ddlMinutos.Items.Insert(i, item)
    Next

    item = New ListItem("--", "")
    ddlMinutos.Items.Insert(0, item)
  End Sub

  ''' <summary>
  ''' inicializa los controles
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InicializarHora()
    Dim fecha As Date = Herramientas.MyNow()
    Dim horaActual As String = Hour(fecha).ToString
    Dim minutoActual As String = Minute(fecha).ToString

    horaActual = IIf(horaActual <= 9, "0" & horaActual, horaActual)
    minutoActual = IIf(minutoActual <= 9, "0" & minutoActual, minutoActual)

    If InicializarHoraSistema Then
      ddlHora.SelectedValue = horaActual
      ddlMinutos.SelectedValue = minutoActual
    End If
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Page.IsPostBack Then
      'If Me.SelectedValueHora = "" Then CargarHoras()
      'If Me.SelectedValueMinutos = "" Then CargarMinutos()
      InicializarHora()

      If InicializarVacio() Then
        If Not InicializarHoraSistema Then ddlHora.SelectedIndex = 0
        If Not InicializarHoraSistema Then ddlMinutos.SelectedIndex = 0
      End If

    End If
  End Sub

End Class
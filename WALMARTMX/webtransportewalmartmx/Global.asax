<%@ Import namespace="webtransportewalmartmx" %>
<%@ Import namespace="System.IO" %>
<%@ Import namespace="System.Diagnostics" %>
<%@ Import namespace="webtransportewalmartmx.Msdn.ErrorIO" %>
<%@ Import namespace="CapaNegocio" %>

<script Language="VB" RunAt="server">
  
  Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
    ' Code that runs on application startup
  End Sub

  Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
    ' Code that runs on application shutdown
  End Sub

' capturamos cualquier excepcion no controlada y mostramos un mensaje de error amigable
  Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
    Dim oUtilidades As New Utilidades
    If System.Configuration.ConfigurationManager.AppSettings("paginaErrorHabilitado").ToLower = "false" Then Exit Sub
   
    ' anotamos en el trace
    HttpContext.Current.Trace.Write("global.asax: Application_Error")

    ' obtenemos la ultima excepcion no controlada
    Dim objError As Exception = Server.GetLastError.GetBaseException
    Dim nombreTipoError As String = objError.GetType.FullName

    ' registramos el error en el log del servidor (visor de sucesos)
    EventLogHandler.EscribirEvento(objError, EventLogEntryType.Error, EventLogHandler.EVENTLOG_ALTO)

    ' si la opcion esta, intentamos enviar un correo a ALTO para notificar del error
    Dim paginaErrorEnviarCorreo As String = System.Configuration.ConfigurationManager.AppSettings("paginaErrorEnviarCorreo").ToLower

    If paginaErrorEnviarCorreo = "true" Then

      ' obtenemos el (o los) correo para las notificaciones
      Dim ErrorEmailNotificaciones As String() = Split(System.Configuration.ConfigurationManager.AppSettings("paginaErrorEmailNotificaciones"), ";")
      
      Dim ColeccionEmails As New System.Net.Mail.MailAddressCollection
      For Each direccion As String In ErrorEmailNotificaciones
        
        If Utilidades.EsEmail(direccion) Then ColeccionEmails.Add(direccion)
        
      Next
      

      If Not Utilidades.EmailError(objError, ColeccionEmails) Then

        Dim nuevoError As String = "Ocurrio un error al enviar el correo de notificacion a ALTO"
        EventLogHandler.EscribirEvento(nuevoError, EventLogEntryType.Warning, EventLogHandler.EVENTLOG_ALTO)

      End If

    End If

    ' En este punto, ya tenemos el error logueado y posiblemente notificado

    ' verificamos la ip del cliente para mostrar los detalles del error en caso de que este un rango permitido
    Dim strRedirect As String = System.Configuration.ConfigurationManager.AppSettings("paginaErrorURL")
    Dim strQueryString As String = String.Empty
    Dim strFilePath As String = String.Empty
    Dim strErrorMethod As String = System.Configuration.ConfigurationManager.AppSettings("paginaErrorMetodo").ToLower
    Dim objErrorIOFactory As New Msdn.ErrorIO.ErrorIOFactory
    Dim objErrorBasket As Msdn.ErrorIO.IErrorIOHandler

    objErrorBasket = objErrorIOFactory.Create(strErrorMethod)

    ' guardamos el error en el objeto y construimos la url a la pagina amistosa
    strQueryString = objErrorBasket.Store(objError)
      
    ' verificar que la pagina de error exista primero, si no existe, logueamos el error
    Dim fileInfo As New FileInfo(Server.MapPath(strRedirect))

    If Not fileInfo.Exists Then

      Dim nuevoError As String = "La pagina de error [" & strRedirect & "] no existe!"
      EventLogHandler.EscribirEvento(nuevoError, EventLogEntryType.Warning, EventLogHandler.EVENTLOG_ALTO)

      strRedirect = String.Empty

    End If
    
    '-- si la session del usuario caduco, se produjo alguna excepecion de referencia nula o un error con la criptografia, entonces lo redirecciona al login
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    If (oUsuario Is Nothing) _
       OrElse (nombreTipoError = "System.NullReferenceException") _
       OrElse (nombreTipoError = "System.Security.Cryptography.CryptographicException") _
       Then
      'OrElse (nombreTipoError = "System.Web.HttpException") _
      Response.Redirect("~/login.aspx")
      Exit Sub
    End If
    '--------------------------------

    strFilePath = strRedirect & strQueryString
    If strRedirect <> String.Empty Then
      Select Case System.Configuration.ConfigurationManager.AppSettings("paginaErrorBranchMethod").ToLower
        Case "transfer"
          ' usar con application, context or cookie. Resultados en la URL requerida originalmente
          Server.Transfer(strFilePath, False)
        Case "redirect"
          ' usar con application, cookie o querystring. Resultados en la URL de la pagina amistosa
          Response.Redirect(strFilePath, True)
      End Select
    End If

  End Sub

  Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
    ' Code that runs when a new session is started
  End Sub

  Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
    ' Code that runs when a session ends. 
    ' Note: The Session_End event is raised only when the sessionstate mode
    ' is set to InProc in the Web.config file. If session mode is set to StateServer 
    ' or SQLServer, the event is not raised.
  End Sub
</script>


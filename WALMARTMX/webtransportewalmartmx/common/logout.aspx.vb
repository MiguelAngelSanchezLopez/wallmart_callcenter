﻿Imports CapaNegocio

Partial Public Class logout
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario

    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      Sistema.GrabarLogSesion(oUsuario.Id, "Cierra sesión")
    Catch ex As Exception
      'no hace nada
    End Try

    'cierra sesion
    oUtilidades.CerrarSesion()
  End Sub
End Class
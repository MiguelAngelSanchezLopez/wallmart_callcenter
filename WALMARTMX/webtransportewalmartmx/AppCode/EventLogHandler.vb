Imports Microsoft.VisualBasic
Imports System.Diagnostics
Imports System.Web

Public Class EventLogHandler

#Region "Constantes y Enums"

  Public Const EVENTLOG_ALTO As String = "AltoSistema"

  Public Enum EstadosEventLog

    EscritoOk = 0
    EventSourceNoExiste = 1
    ErrorEscribiendo = 2

  End Enum

#End Region

#Region "Shared"

  ' insertar un evento al eventsource (mensaje tipo String)
  Public Shared Function EscribirEvento(ByVal mensaje As String, ByVal tipo As EventLogEntryType, ByVal eventSource As String) As EstadosEventLog

    Try

      If Not EventLog.SourceExists("AltoSistema") Then
        Return EstadosEventLog.EventSourceNoExiste
        Exit Function
      End If

      Dim ipError As String = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")

      mensaje = mensaje & vbCrLf & vbCrLf & "Ip Origen " & ipError

      ' inicializar y escribir el evento
      Dim objEventLog As New System.Diagnostics.EventLog

      With objEventLog

        .Source = eventSource
        .WriteEntry(mensaje, tipo)

      End With

      Return EstadosEventLog.EscritoOk

    Catch ex As Exception

      Return EstadosEventLog.ErrorEscribiendo

    End Try

  End Function

  ' insertar un evento al eventsource (mensaje tipo Exception)
  Public Shared Function EscribirEvento(ByVal mensaje As System.Exception, ByVal tipo As EventLogEntryType, ByVal eventSource As String) As EstadosEventLog
    Try

      If Not EventLog.SourceExists(eventSource) Then
        Return EstadosEventLog.EventSourceNoExiste
        Exit Function
      End If

      Dim ipError As String = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")

      Dim excepcion As String = String.Empty

      excepcion = mensaje.ToString & vbCrLf & vbCrLf
      excepcion &= "Origen " & mensaje.Source & vbCrLf & vbCrLf
      excepcion &= "Servidor " & System.Environment.MachineName & vbCrLf & vbCrLf
      excepcion &= "Ip Origen " & ipError

      ' inicializar y escribir el evento
      Dim objEventLog As New System.Diagnostics.EventLog

      With objEventLog

        .Source = eventSource
        .WriteEntry(excepcion, tipo)

      End With

      Return EstadosEventLog.EscritoOk

    Catch ex As Exception

      Return EstadosEventLog.ErrorEscribiendo

    End Try

  End Function

#End Region

End Class


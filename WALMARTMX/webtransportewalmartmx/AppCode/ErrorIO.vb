Imports CapaNegocio
Imports System.IO
Imports System.Text
Imports System.Web
Imports System.Globalization

Namespace Msdn.ErrorIO
' Exceptional Events, Exceptional Measures
' Published: http://msdn.microsoft.com/asp.net/ 
' Author: Eli Robillard 
' Website: http://www.erobillard.com/

	Public Interface IErrorIOHandler
		Function Store(ByVal objError As System.Exception) As String
		Sub Retrieve(ByRef strMessage As String, ByRef strSource As String, ByRef strStackTrace As String, ByRef strDate As String, ByRef strErrorPath As String)
		Sub Clear()
	End Interface

	Public Class ErrorIOFactory
		Public Function Create(ByVal strMethod As String) As IErrorIOHandler
			' Dim strMethod As String
			' The method to use is defined in the web.config, appSettings section like so:
			' 	<appSettings>
			' 		<add key="customErrorMethod" value="cookie" />
			' 	</appSettings>
			' strMethod = System.Configuration.ConfigurationSettings.AppSettings("customErrorMethod")
			Select Case strMethod.ToLower(CultureInfo.InstalledUICulture)
				Case "application"
					HttpContext.Current.Trace.Write("ErrorIOFactory: Create('Application')")
					Return New ErrorApplication
				Case "context"
					HttpContext.Current.Trace.Write("ErrorIOFactory: Create('Context')")
					Return New ErrorContext
				Case "cookie"
					HttpContext.Current.Trace.Write("ErrorIOFactory: Create('Cookie')")
					Return New ErrorCookie
				Case "querystring"
					HttpContext.Current.Trace.Write("ErrorIOFactory: Create('QueryString')")
          Return New ErrorQueryString
				Case Else
					' Cookie works with contextErrors mode="On", global.asax:Response.Redirect, global.asax:Server.Transfer, 
					' and is not prone to displaying someone else's error (as Application may be). So, it wins default.
					HttpContext.Current.Trace.Write("ErrorIOFactory: Create('[Cookie by default]')")
					Return New ErrorCookie
			End Select
		End Function
	End Class

	Public MustInherit Class ErrorStorage
		' An abstract class with shared fields and methods 
		Protected errorPath As String = ""
		Protected message As String = ""
		Protected source As String = ""
		Protected stackTrace As String = ""
		Protected errorDate As String = ""
	End Class


	Public Class ErrorApplication
		Inherits ErrorStorage
		Implements IErrorIOHandler

		Public Function Store(ByVal objError As System.Exception) As String Implements IErrorIOHandler.Store
			HttpContext.Current.Trace.Write("ErrorIOFactory: Application Store()")
			errorPath = HttpContext.Current.Request.FilePath
			' Build IP address into each identifier to ensure clients don't get each other's errors.
			Dim _errorip As String = HttpContext.Current.Request.UserHostAddress

			HttpContext.Current.Application.Lock()
			HttpContext.Current.Application.Add("LastError" & _errorip, objError)
      HttpContext.Current.Application.Add("LastErrorDate" & _errorip, Herramientas.MyNow().ToString(CultureInfo.InstalledUICulture))
			HttpContext.Current.Application.Add("LastErrorPath" & _errorip, errorPath)
			HttpContext.Current.Application.UnLock()

			Return "?aspxerrorpath=" & errorPath
		End Function

		Public Sub Retrieve(ByRef strMessage As String, ByRef strSource As String, ByRef strStackTrace As String, ByRef strDate As String, ByRef strErrorPath As String) Implements IErrorIOHandler.Retrieve
			HttpContext.Current.Trace.Write("ErrorIOFactory: Application Retrieve()")
			Try
				' IP addresses are built into each identifier to ensure clients don't get each other's errors.
				Dim errorip As String = HttpContext.Current.Request.UserHostAddress
				Dim objException As System.Exception = HttpContext.Current.Application.Get("LastError" & errorip)

				strMessage = objException.Message
				strSource = objException.Source
				strStackTrace = objException.StackTrace
				strDate = HttpContext.Current.Application.Get("LastErrorDate" & errorip)
				strErrorPath = HttpContext.Current.Application.Get("LastErrorPath" & errorip)
			Catch
				strMessage = "There was a problem retrieving Error data from the Application."
				strSource = "Assembly ErrorIO; Class ErrorApplication; Method Retrieve()."
				strStackTrace = "Not available."
        strDate = Herramientas.MyNow().ToString(CultureInfo.InstalledUICulture)
        strErrorPath = "Not available."
      End Try
    End Sub

    Public Sub Clear() Implements IErrorIOHandler.Clear
      ' IP address is built into each identifier to ensure clients don't get each other's errors.
      Dim _errorip As String = HttpContext.Current.Request.UserHostAddress

      HttpContext.Current.Trace.Write("ErrorIOFactory: Application Clear()")
      HttpContext.Current.Application.Remove("LastError" & _errorip)
      HttpContext.Current.Application.Remove("LastErrorDate" & _errorip)
      HttpContext.Current.Application.Remove("LastErrorPath" & _errorip)
    End Sub

  End Class


  Public Class ErrorCookie
    Inherits ErrorStorage
    Implements IErrorIOHandler

    Function Store(ByVal objError As System.Exception) As String Implements IErrorIOHandler.Store
      HttpContext.Current.Trace.Write("ErrorIOFactory: Cookie Store()")
      errorPath = HttpContext.Current.Request.FilePath

      Dim cookieError As HttpCookie = New HttpCookie("LastError")
      cookieError.Values("Message") = objError.Message
      cookieError.Values("Source") = objError.Source
      cookieError.Values("StackTrace") = objError.StackTrace
      cookieError.Values("DateTime") = Herramientas.MyNow().ToString(CultureInfo.InstalledUICulture)
      cookieError.Values("FilePath") = errorPath
      cookieError.Expires = Herramientas.MyNow().AddMinutes(30)
      HttpContext.Current.Response.Cookies.Add(cookieError)

      Return "?aspxerrorpath=" & HttpUtility.UrlEncode(errorPath)
    End Function

    Public Sub Retrieve(ByRef strMessage As String, ByRef strSource As String, ByRef strStackTrace As String, ByRef strDate As String, ByRef strErrorPath As String) Implements IErrorIOHandler.Retrieve
      HttpContext.Current.Trace.Write("ErrorIOFactory: Cookie Retrieve()")

      Dim cookieError As HttpCookie = HttpContext.Current.Request.Cookies("LastError")
      Try
        strMessage = cookieError.Values("Message")
        strSource = cookieError.Values("Source")
        strStackTrace = cookieError.Values("StackTrace")
        strDate = cookieError.Values("DateTime")
        strErrorPath = cookieError.Values("FilePath")
      Catch
        strMessage = "There was a problem retrieving Error data from the Cookie."
        strSource = "Assembly ErrorIO; Class ErrorCookie; Method Retrieve()."
        strStackTrace = "Not available."
        strDate = Herramientas.MyNow().ToString(CultureInfo.InstalledUICulture)
        strErrorPath = "Not available."
      End Try
    End Sub

    Public Sub Clear() Implements IErrorIOHandler.Clear
      HttpContext.Current.Trace.Write("ErrorIOFactory: Cookie Clear()")
      Dim cookieError As HttpCookie = New HttpCookie("Error")
      cookieError.Expires = Herramientas.MyNow().AddYears(-30)
      HttpContext.Current.Response.Cookies.Add(cookieError)
    End Sub
  End Class


  Public Class ErrorContext
    Inherits ErrorStorage
    Implements IErrorIOHandler

    Public Function Store(ByVal objError As System.Exception) As String Implements IErrorIOHandler.Store
      HttpContext.Current.Trace.Write("ErrorIOFactory: Context Store()")
      errorPath = HttpContext.Current.Request.FilePath
      HttpContext.Current.Items.Add("LastError", objError)
      HttpContext.Current.Items.Add("LastErrorDate", Herramientas.MyNow().ToString(CultureInfo.InstalledUICulture))
      HttpContext.Current.Items.Add("LastErrorPath", errorPath)
      Return "?aspxerrorpath=" & HttpUtility.UrlEncode(errorPath)
    End Function

    Public Sub Retrieve(ByRef strMessage As String, ByRef strSource As String, ByRef strStackTrace As String, ByRef strDate As String, ByRef strErrorPath As String) Implements IErrorIOHandler.Retrieve
      HttpContext.Current.Trace.Write("ErrorIOFactory: Context Retrieve()")
      Try
        Dim objException As System.Exception = HttpContext.Current.Items.Item("LastError")
        strMessage = objException.Message
        strSource = objException.Source
        strStackTrace = objException.StackTrace
        strDate = HttpContext.Current.Items.Item("LastErrorDate")
        strErrorPath = HttpContext.Current.Items.Item("LastErrorPath")
      Catch
        ' Any part of the above could fail, so only replace empty items with N/A or a better description.
      Finally
        If (strMessage = String.Empty) Or (strSource = String.Empty) Then
          strMessage = "There was a problem retrieving Error data from the Context."
          strSource = "Assembly ErrorIO; Class ErrorQueryString; Method Retrieve()."
        End If
        If strStackTrace = String.Empty Then strStackTrace = "Not available."
        If strDate = String.Empty Then strDate = Herramientas.MyNow().ToString(CultureInfo.InstalledUICulture)
        If strErrorPath = String.Empty Then strErrorPath = "Not available."
      End Try
    End Sub

    Public Sub Clear() Implements IErrorIOHandler.Clear
      HttpContext.Current.Trace.Write("ErrorIOFactory: Context Clear()")
      HttpContext.Current.Items.Remove("LastError")
      HttpContext.Current.Items.Remove("LastErrorDate")
      HttpContext.Current.Items.Remove("LastErrorPath")
    End Sub
  End Class


  Public Class ErrorQueryString
    Inherits ErrorStorage
    Implements IErrorIOHandler

    Public Function Store(ByVal objError As System.Exception) As String Implements IErrorIOHandler.Store
      ' Note: in this implementation the values aren't actually stored, but put into a value to be retrieved with QueryString()
      HttpContext.Current.Trace.Write("ErrorIOFactory: QueryString Store()")
      Dim sbQueryString As StringBuilder = New StringBuilder(512)
      errorPath = HttpContext.Current.Request.FilePath
      sbQueryString.Append("?aspxerrorpath=")
      sbQueryString.Append(HttpUtility.UrlEncode(errorPath))
      sbQueryString.Append("&message=")
      sbQueryString.Append(HttpUtility.UrlEncode(objError.Message))
      sbQueryString.Append("&source=")
      sbQueryString.Append(HttpUtility.UrlEncode(objError.Source))
      sbQueryString.Append("&stacktrace=")
      sbQueryString.Append(System.Web.HttpUtility.UrlEncode(objError.StackTrace))
      sbQueryString.Append("&errordate=")
      sbQueryString.Append(HttpUtility.UrlEncode(Herramientas.MyNow().ToString(CultureInfo.InstalledUICulture)))
      Return (sbQueryString.ToString)
    End Function

    Public Sub Retrieve(ByRef strMessage As String, ByRef strSource As String, ByRef strStackTrace As String, ByRef strDate As String, ByRef strErrorPath As String) Implements IErrorIOHandler.Retrieve
      HttpContext.Current.Trace.Write("ErrorIOFactory: QueryString Retrieve()")
      strMessage = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString("message"))
      strSource = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString("source"))
      strStackTrace = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString("stacktrace"))
      strDate = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString("errordate"))
      strErrorPath = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString("aspxerrorpath"))

      If (strMessage = String.Empty) Or (strSource = String.Empty) Then
        strMessage = "There was a problem retrieving Error data from the QueryString."
        strSource = "Assembly ErrorIO; Class ErrorQueryString; Method Retrieve()."
      End If
      If strStackTrace = String.Empty Then strStackTrace = "Not available."
      If strDate = String.Empty Then strDate = Herramientas.MyNow().ToString(CultureInfo.InstalledUICulture)
      If strErrorPath = String.Empty Then strErrorPath = "Not available."
    End Sub

    Public Sub Clear() Implements IErrorIOHandler.Clear
      HttpContext.Current.Trace.Write("ErrorIOFactory: QueryString Clear()")
      ' Nothing to do
    End Sub
  End Class


End Namespace

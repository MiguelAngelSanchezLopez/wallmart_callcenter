var Combo = {
  /*******************************************************************************************
  * METODOS USADOS PARA HTML
  *******************************************************************************************/
  dibujarHTML: function (opciones) {
    var totalRegistros = 0;
    var queryString = "";

    try {
      if (opciones.data) { totalRegistros = opciones.data.length; }

      if (totalRegistros == 0) {

        //construye queryString
        queryString += "op=DibujarCombo";
        queryString += "&tipoCombo=" + opciones.tipoCombo;
        queryString += "&filtroId=" + opciones.filtro;
        queryString += "&fuenteDatos=" + opciones.fuenteDatos;

        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo obtener los datos");
          } else {
            //Respuesta exitosa...
            var json = jQuery.parseJSON(respuesta);
            opciones.data = json;
            Combo.cargarDatos(opciones);
          }
        }

        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo obtener los datos");
        }

        jQuery.ajax({
          url: "../webAjax/waCombobox.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });

      } else {
        Combo.cargarDatos(opciones);
      }


    } catch (e) {
      alert("Exception Combo.dibujarHTML:\n" + e);
    }
  },

  cargarDatos: function (opciones) {
    var objCombo = jQuery("#" + opciones.elem_contenedor);
    var template = "<option value=\"{VALOR}\" {SELECTED} {DATA}>{TEXTO}</option>";
    var listado = "";
    var selected = "";
    var mostrarBuscador = 0;
    var permitirLimpiarCombo = false;
    var disabled = false;
    var valor, texto;

    if (opciones.placeholder) { objCombo.attr("placeholder", opciones.placeholder); }
    if (opciones.onChange) { if (opciones.onChange != "") { objCombo.attr("onchange", opciones.onChange); } }
    if (opciones.permitirLimpiarCombo) { permitirLimpiarCombo = opciones.permitirLimpiarCombo; }
    if (opciones.combo_simple) { opciones.combo_simple = true; } else { opciones.combo_simple = false; }
    if (typeof (opciones.enabled) != "undefined") { disabled = !opciones.enabled; }

    //setea propiedades
    objCombo.attr("multiple", opciones.multiple);
    objCombo.attr("disabled", disabled);

    //Obtener los data-field
    var dataFields = "";

    if (!opciones.multiple) {
      var templateAux = template;
      switch (opciones.item_cabecera) {
        case "Seleccione":
          valor = "";
          texto = "--Seleccione--";
          break;
        case "Todos":
          valor = "-1";
          texto = "--Todos--";
          break;
        default:
          valor = "";
          texto = "";
          break;
      }

      for (var key in opciones.data[0]) {
        var name = key.toLowerCase();
        //sistema.js starswith
        if (name.substring(0, 4) == "data") {
          dataFields = dataFields + name.replace("_", "-") + "=\"\" ";
        }
      }

      templateAux = templateAux.replace(/{VALOR}/ig, valor);
      templateAux = templateAux.replace(/{SELECTED}/ig, selected);
      templateAux = templateAux.replace(/{TEXTO}/ig, texto);
      templateAux = templateAux.replace(/{DATA}/ig, dataFields);

      listado += templateAux;
    }

    jQuery.each(opciones.data, function (indice, obj) {

      //Obtener los data-field
      dataFields = "";
      for (var key in obj) {
        var name = key.toLowerCase();
        //sistema.js starswith
        if (name.substring(0, 4) == "data") {
          dataFields = dataFields + name.replace("_", "-") + "=\"" + obj[key] + "\" ";
        }
      }

      valor = obj.Valor;
      texto = obj.Texto;

      selected = (jQuery.inArray(valor.toString(), opciones.valores_seleccionados) != -1) ? "selected" : "";
      templateAux = template;

      templateAux = templateAux.replace(/{VALOR}/ig, valor);
      templateAux = templateAux.replace(/{SELECTED}/ig, selected);
      templateAux = templateAux.replace(/{TEXTO}/ig, texto);
      templateAux = templateAux.replace(/{DATA}/ig, dataFields);
      listado += templateAux;

    });

    objCombo.html(listado);

    if (!opciones.combo_simple) {
      jQuery(objCombo).select2({
        allowClear: permitirLimpiarCombo,
        minimumResultsForSearch: mostrarBuscador,
        dropdownCssClass: "sist-font16"
      });
    }

    if (opciones.combos_relacionados) {
      if (jQuery.isArray(opciones.combos_relacionados)) {
        jQuery.each(opciones.combos_relacionados, function (indice, obj) {
          Combo.dibujarHTML(obj);
        });
      }
    }

  },

  cambiarComboTipoObservacion: function (opciones) {
    var id = jQuery("#" + opciones.elem_contenedor).select2("val");
    var combosRelacionados = [
                                {
                                  elem_contenedor: "ddlTipoObservacion",
                                  tipoCombo: 'TipoObservacion',
                                  multiple: false,
                                  placeholder: "",
                                  item_cabecera: "Seleccione",
                                  valores_seleccionados: ['-1'],
                                  onChange: "Alerta.mostrarClasificacionTipoObservacion();",
                                  fuenteDatos: "Tabla",
                                  data: [],
                                  filtro: id
                                }
                             ];

    jQuery.each(combosRelacionados, function (indice, obj) {
      Combo.dibujarHTML(obj);
    });

    //Limpiar la descripcion
    Alerta.mostrarClasificacionTipoObservacion();
  }

};
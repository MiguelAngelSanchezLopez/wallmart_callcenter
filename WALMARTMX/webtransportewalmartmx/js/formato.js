﻿var Formato = {

  validarFormularioBusqueda: function () {
    try {
      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Formato.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  abrirFormulario: function (opciones) {
    try {
      var queryString = "";
      var idFormato = opciones.idFormato;

      queryString += "?id=" + idFormato;

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Mantenedor.FormatoDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Formato.abrirFormulario:\n" + e);
    }
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.Formato.cargarDesdePopUp(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Formato.cerrarPopUp:\n" + e);
    }

  },

  cargarDesdePopUp: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception Formato.cargarDesdePopUp:\n" + e);
    }
  },

  validarFormularioFormato: function () {
    var item, arrListBox, arrPerfiles, arrPerfilesPorRevisar, tieneValorRepetido;

    try {
      var existeNombreFormato = Formato.existeNombreFormato();
      var existeLlaveFormato = Formato.existeLlaveFormato();

      var jsonValidarCampos = [
          { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtLlave", requerido: true, tipo_validacion: "alfanumerico" }
      ];

      //valida que el nombre de la Formato sea unico
      if (jQuery("#txtNombre").val() != "") {
        item = {
          valor_a_validar: existeNombreFormato,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorNombre",
          mensaje_validacion: "El nombre ya existe"
        };
        jsonValidarCampos.push(item);
      }

      //valida que la llave del Formato sea unica
      if (jQuery("#txtLlave").val() != "") {
        item = {
          valor_a_validar: existeLlaveFormato,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorLlave",
          mensaje_validacion: "La llave ya existe"
        };
        jsonValidarCampos.push(item);
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        Formato.obtenerJSONPerfilesSeleccionados();
        return true;
      }

    } catch (e) {
      alert("Exception: Formato.validarFormularioFormato\n" + e);
      return false;
    }

  },

  existeNombreFormato: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var nombreFormato = jQuery("#txtNombre").val();
      var idFormato = jQuery("#txtIdFormato").val();
      queryString += "op=Formato.Nombre";
      queryString += "&id=" + idFormato;
      queryString += "&valor=" + Sistema.urlEncode(nombreFormato);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el nombre único del Formato");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el nombre único del Formato");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Formato.existeNombreFormato:\n" + e);
    }

    return existe;
  },

  existeLlaveFormato: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var llaveFormato = jQuery("#txtLlave").val();
      var idFormato = jQuery("#txtIdFormato").val();
      queryString += "op=Formato.Llave";
      queryString += "&id=" + idFormato;
      queryString += "&valor=" + Sistema.urlEncode(llaveFormato);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar la llave única del Formato");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar la llave única del Formato");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Formato.existeLlaveFormato:\n" + e);
    }

    return existe;
  },

  eliminarRegistro: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarFormato";
      queryString += "&idFormato=" + Sistema.urlEncode(opciones.idFormato);

      if (confirm("Si elimina el Formato se eliminarán todas las referencias asociadas a él y no se podrá volver a recuperar.\n¿Desea eliminar el Formato?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waSistema.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Formato.eliminarRegistro\n" + e);
    }
  },

  validarEliminar: function () {
    try {
      if (confirm("Si elimina el Formato se eliminarán todas las referencias asociadas a él y no se podrá volver a recuperar.\n¿Desea eliminar el Formato?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: Formato.validarEliminar\n" + e);
      return false;
    }
  },

  agregarFilaPerfilAsociado: function (opciones) {
    var objFila, templateFila, idPerfil, nombrePerfil, jsonValidarCampos, esValido, jsonPerfiles, elemTabla;

    try {
      elemTabla = opciones.elem_tabla;
      idPerfil = jQuery("#ddlPerfil").val();
      nombrePerfil = jQuery("#ddlPerfil option:selected").text();
      objFila = jQuery("#" + elemTabla + " tbody tr:eq(0)").clone();
      objFila.removeClass("sist-display-none");
      templateFila = objFila.wrap("<div />").parent().html(); // obtiene el html del clone

      jsonValidarCampos = [
		    { elemento_a_validar: "ddlPerfil", requerido: true }
      ];
      esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        alert("Debe seleccionar un perfil para agregarlo");
      } else {
        templateFila = templateFila.replace(/{PREFIJO_CONTROL}/ig, idPerfil);
        templateFila = templateFila.replace(/{VALOR}/ig, idPerfil);
        templateFila = templateFila.replace(/{TEXTO}/ig, nombrePerfil);
        templateFila = templateFila.replace(/{CLASS_TR}/ig, "");
        jQuery("#" + elemTabla).append(templateFila);

        //marca en el json el perfil seleccionado
        jsonPerfiles = jQuery("#txtJSONPerfiles").val();
        jsonPerfiles = jQuery.parseJSON(jsonPerfiles);
        jQuery.each(jsonPerfiles, function (indice, obj) {
          if (obj.Valor == idPerfil) {
            obj.Seleccionado = 1;
            return false;
          }
        });
        //asigna actualizacion del json en campo oculto
        jQuery("#txtJSONPerfiles").val(Sistema.convertirJSONtoString(jsonPerfiles));

        //dibuja nuevamente el combo de perfiles con los que no estan seleccionados
        Formato.dibujarComboPerfiles();
      }

    } catch (e) {
      alert("Exception: Formato.agregarFilaPerfilAsociado\n" + e);
    }
  },

  dibujarComboPerfiles: function () {
    var template = "<option value=\"{VALOR}\">{TEXTO}</option>";
    var listado = "";
    var jsonPerfiles, templateAux, valor, texto;

    try {
      jsonPerfiles = jQuery("#txtJSONPerfiles").val();
      jsonPerfiles = jQuery.parseJSON(jsonPerfiles);

      //filtra los perfiles que no estan seleccionados
      jsonPerfiles = jQuery.grep(jsonPerfiles, function (item, indice) {
        return (item.Seleccionado == 0);
      });

      //asigna --Seleccione-- por defecto
      templateAux = template;
      templateAux = templateAux.replace(/{VALOR}/ig, "");
      templateAux = templateAux.replace(/{TEXTO}/ig, "--Seleccione--");
      listado += templateAux;

      //recorre los perfiles y los agrega al combo
      jQuery.each(jsonPerfiles, function (indice, obj) {
        templateAux = template;
        valor = obj.Valor;
        texto = obj.Texto;

        templateAux = templateAux.replace(/{VALOR}/ig, valor);
        templateAux = templateAux.replace(/{TEXTO}/ig, texto);
        listado += templateAux;
      });

      jQuery("#ddlPerfil").empty();
      jQuery("#ddlPerfil").html(listado);
      jQuery("#ddlPerfil").select2("val", "");
    } catch (e) {
      alert("Exception: Formato.dibujarComboPerfiles\n" + e);
    }

  },

  obtenerJSONPerfilesSeleccionados: function () {
    var arrIdPerfil, idPerfil, item, arrCheckbox;
    var json = [];

    try {
      arrIdPerfil = jQuery("input[data-tipo=id-perfil]");

      //recorre los perfiles seleccionados
      jQuery.each(arrIdPerfil, function (indice, obj) {
        idPerfil = jQuery(obj).val();

        if (idPerfil != "{VALOR}") {
          item = {
            IdPerfil: idPerfil
          };
          json.push(item);

        }
      });

      //asigna actualizacion del json en campo oculto
      jQuery("#txtJSONPerfilesSeleccionados").val(Sistema.convertirJSONtoString(json));

    } catch (e) {
      alert("Exception: Formato.obtenerJSONPerfilesSeleccionados\n" + e);
    }

  },

  eliminarPerfilAsociado: function (opciones) {
    var idPerfil, jsonPerfiles;

    try {
      idPerfil = opciones.idPerfil;

      if (confirm("¿Está seguro de eliminar el perfil asociado?")) {
        //elimina la fila de la tabla
        jQuery("#" + idPerfil + "_tr").remove();

        //marca en el json el perfil seleccionado
        jsonPerfiles = jQuery("#txtJSONPerfiles").val();
        jsonPerfiles = jQuery.parseJSON(jsonPerfiles);
        jQuery.each(jsonPerfiles, function (indice, obj) {
          if (obj.Valor == idPerfil) {
            obj.Seleccionado = 0;
            return false;
          }
        });
        //asigna actualizacion del json en campo oculto
        jQuery("#txtJSONPerfiles").val(Sistema.convertirJSONtoString(jsonPerfiles));

        //dibuja nuevamente el combo de perfiles con los que no estan seleccionados
        Formato.dibujarComboPerfiles();
      }

    } catch (e) {
      alert("Exception: Formato.eliminarPerfilAsociado\n" + e);
    }

  }

};
﻿var Soporte = {
  LLAVE_FORMATO: "WALMARTMX",
  ENVIADO_DESDE: "WEB WALMART MX",
  ORIGEN_TICKET: "Sitio Web",
  COMODIN_OTRO: "OTRO",

  inicializarControles: function () {
    var oUsuario;

    try {
      oUsuario = Sistema.obtenerUsuarioConectadoJSON();

      Soporte.obtenerMotivosTicket(oUsuario);
      Soporte.obtenerAplicaciones(oUsuario);
      Soporte.obtenerModulos(oUsuario);
    } catch (e) {
      alert("Exception: Soporte.inicializarControles\n" + e);
    }
  },

  inicializarControlesMisTicket: function () {
    var oUsuario;

    try {
      oUsuario = Sistema.obtenerUsuarioConectadoJSON();

      Soporte.obtenerEstados(oUsuario);
    } catch (e) {
      alert("Exception: Soporte.inicializarControles\n" + e);
    }
  },

  cargarCombo: function (opciones) {
    var elemContenedor, tipoCombo, data, itemCabecera;

    try {
      elemContenedor = opciones.elemContenedor;
      tipoCombo = opciones.tipoCombo;
      data = opciones.data;
      itemCabecera = opciones.itemCabecera;

      if (data) {
        Combo.dibujarHTML({
          elem_contenedor: Sistema.PREFIJO_CONTROL + elemContenedor,
          tipoCombo: tipoCombo,
          multiple: false,
          placeholder: "",
          item_cabecera: itemCabecera,
          valores_seleccionados: ["-1"],
          onChange: "",
          fuenteDatos: "",
          data: data,
          filtro: "",
          enabled: true
        });
      }
    } catch (e) {
      alert("Exception: Soporte.cargarComboLocal\n" + e);
    }
  },

  obtenerMotivosTicket: function (oUsuario) {
    var dataJSON;

    try {
      dataJSON = {
        oParametros: {
          LlaveFormato: Soporte.LLAVE_FORMATO,
          LlavePerfil: oUsuario.PerfilLlave,
          MostrarOtro: true
        }
      };
      dataJSON = Sistema.convertirJSONtoString(dataJSON);

      var okFunc = function (t) {
        var oRetorno = t.d;

        if (oRetorno.Respuesta.Codigo == Sistema.CONST_CODIGO_SQL_ERROR) {
          Sistema.modalMensajeError({ mensaje: oRetorno.Respuesta.Descripcion });
        } else {
          Soporte.cargarCombo({
            elemContenedor: "ddlMotivoTicket",
            tipoCombo: "MotivoTicket",
            data: oRetorno.ListadoMotivoTicket,
            itemCabecera: "Seleccione"
          });
        }
      }
      var errFunc = function (t, status) {
        Sistema.modalMensajeError({ mensaje: "Ha ocurrido un error interno y no se pudo obtener los motivo ticket asociados al formato.<br />" + t.statusText });
      }

      jQuery.ajax({
        url: oUsuario.UrlApiTicketSoporte + "/ObtenerMotivosTicket",
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: dataJSON,
        success: okFunc,
        error: errFunc,
        timeout: 60000 //60000 = 60 segundos de espera
      });
    } catch (e) {
      alert("Exception: Soporte.obtenerMotivosTicket\n" + e);
    }
  },

  obtenerAplicaciones: function (oUsuario) {
    var dataJSON;

    try {
      dataJSON = {
        oParametros: {
          LlaveFormato: Soporte.LLAVE_FORMATO,
          LlavePerfil: oUsuario.PerfilLlave,
          MostrarOtro: true
        }
      };
      dataJSON = Sistema.convertirJSONtoString(dataJSON);

      var okFunc = function (t) {
        var oRetorno = t.d;

        if (oRetorno.Respuesta.Codigo == Sistema.CONST_CODIGO_SQL_ERROR) {
          Sistema.modalMensajeError({ mensaje: oRetorno.Respuesta.Descripcion });
        } else {
          Soporte.cargarCombo({
            elemContenedor: "ddlAplicacion",
            tipoCombo: "Aplicacion",
            data: oRetorno.ListadoAplicaciones,
            itemCabecera: "Seleccione"
          });
        }
      }
      var errFunc = function (t, status) {
        Sistema.modalMensajeError({ mensaje: "Ha ocurrido un error interno y no se pudo obtener las aplicaciones asociadas al formato.<br />" + t.statusText });
      }

      jQuery.ajax({
        url: oUsuario.UrlApiTicketSoporte + "/ObtenerAplicaciones",
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: dataJSON,
        success: okFunc,
        error: errFunc,
        timeout: 60000 //60000 = 60 segundos de espera
      });
    } catch (e) {
      alert("Exception: Soporte.obtenerAplicaciones\n" + e);
    }
  },

  obtenerModulos: function (oUsuario) {
    var dataJSON;

    try {
      dataJSON = {
        oParametros: {
          LlaveFormato: Soporte.LLAVE_FORMATO,
          LlavePerfil: oUsuario.PerfilLlave,
          MostrarOtro: true
        }
      };
      dataJSON = Sistema.convertirJSONtoString(dataJSON);

      var okFunc = function (t) {
        var oRetorno = t.d;

        if (oRetorno.Respuesta.Codigo == Sistema.CONST_CODIGO_SQL_ERROR) {
          Sistema.modalMensajeError({ mensaje: oRetorno.Respuesta.Descripcion });
        } else {
          Soporte.cargarCombo({
            elemContenedor: "ddlModulo",
            tipoCombo: "Modulo",
            data: oRetorno.ListadoModulos,
            itemCabecera: "Seleccione"
          });
        }
      }
      var errFunc = function (t, status) {
        Sistema.modalMensajeError({ mensaje: "Ha ocurrido un error interno y no se pudo obtener los módulos asociadas al formato.<br />" + t.statusText });
      }

      jQuery.ajax({
        url: oUsuario.UrlApiTicketSoporte + "/ObtenerModulos",
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: dataJSON,
        success: okFunc,
        error: errFunc,
        timeout: 60000 //60000 = 60 segundos de espera
      });
    } catch (e) {
      alert("Exception: Soporte.obtenerModulos\n" + e);
    }
  },

  obtenerEstados: function (oUsuario) {
    var dataJSON;

    try {
      dataJSON = {
        oParametros: {
          Filtro: "-1"
        }
      };
      dataJSON = Sistema.convertirJSONtoString(dataJSON);

      var okFunc = function (t) {
        var oRetorno = t.d;

        if (oRetorno.Respuesta.Codigo == Sistema.CONST_CODIGO_SQL_ERROR) {
          Sistema.modalMensajeError({ mensaje: oRetorno.Respuesta.Descripcion });
        } else {
          Soporte.cargarCombo({
            elemContenedor: "ddlEstado",
            tipoCombo: "Estado",
            data: oRetorno.ListadoEstados,
            itemCabecera: "Todos"
          });
        }
      }
      var errFunc = function (t, status) {
        Sistema.modalMensajeError({ mensaje: "Ha ocurrido un error interno y no se pudo obtener los estados del ticket.<br />" + t.statusText });
      }

      jQuery.ajax({
        url: oUsuario.UrlApiTicketSoporte + "/ObtenerEstados",
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: dataJSON,
        success: okFunc,
        error: errFunc,
        timeout: 60000 //60000 = 60 segundos de espera
      });
    } catch (e) {
      alert("Exception: Soporte.obtenerEstados\n" + e);
    }
  },

  mostrarCampoOtro: function (opciones) {
    var invocadoDesde, valorCombo;

    try {
      invocadoDesde = opciones.invocadoDesde;
      valorCombo = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddl" + invocadoDesde).select2("val");

      if (Sistema.toUpper(valorCombo) == Soporte.COMODIN_OTRO) {
        jQuery("#" + Sistema.PREFIJO_CONTROL + "h" + invocadoDesde + "Otro").show();
      } else {
        jQuery("#" + Sistema.PREFIJO_CONTROL + "h" + invocadoDesde + "Otro").hide();
      }
      //limpia el control
      jQuery("#" + Sistema.PREFIJO_CONTROL + "txt" + invocadoDesde + "Otro").val("");
    } catch (e) {
      alert("Exception: Soporte.mostrarCampoOtro\n" + e);
    }
  },

  validarFormularioNuevoTicket: function () {
    var esValido = false;
    var jsonValidarCampos, esValido, motivoTicket, aplicacion, modulo;
    var requeridoMotivoTicket, requeridoAplicacion, requeridoModulo;

    try {
      motivoTicket = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlMotivoTicket").select2("val");
      aplicacion = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlAplicacion").select2("val");
      modulo = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlModulo").select2("val");
      requeridoMotivoTicket = (Sistema.toUpper(motivoTicket) == Soporte.COMODIN_OTRO) ? true : false;
      requeridoAplicacion = (Sistema.toUpper(aplicacion) == Soporte.COMODIN_OTRO) ? true : false;
      requeridoModulo = (Sistema.toUpper(modulo) == Soporte.COMODIN_OTRO) ? true : false;

      jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNombrePersona", requerido: true, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTelefono", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtEmail", requerido: false, tipo_validacion: "email" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "ddlMotivoTicket", requerido: true }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "ddlAplicacion", requerido: true }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "ddlModulo", requerido: true }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtObservacion", requerido: true, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtMotivoTicketOtro", requerido: requeridoMotivoTicket, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtAplicacionOtro", requerido: requeridoAplicacion, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtModuloOtro", requerido: requeridoModulo, tipo_validacion: "caracteres-prohibidos" }
      ];

      esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.modalMensajeError();
      } else {
        bootbox.dialog({
          closeButton: false,
          title: "<strong class=\"sist-font-size-24\">Grabar Ticket</strong>",
          message: "<span class=\"sist-font-size-16\">¿Seguro que desea enviar el ticket?</span>",
          buttons: {
            cancel: {
              label: "Cancelar",
              className: "btn-default btn-lg",
              callback: function () {
                //cierra la ventana modal
              }
            },
            success: {
              label: "Aceptar",
              className: "btn-success btn-lg",
              callback: function () {
                jQuery("#" + Sistema.PREFIJO_CONTROL + "hBotones").hide();
                jQuery("#" + Sistema.PREFIJO_CONTROL + "hMensajeGrabando").show();

                setTimeout(function () {
                  Soporte.grabarNuevoTicket();
                }, 100);
              }
            }

          }
        });
      }

    } catch (e) {
      alert("Exception: Soporte.validarFormularioNuevoTicket\n" + e);
      return false;
    }
  },

  grabarNuevoTicket: function () {
    var oUsuario;
    var mensaje = "";

    try {
      oUsuario = Sistema.obtenerUsuarioConectadoJSON();

      dataJSON = {
        oNuevoTicket: {
          LlaveFormato: Soporte.LLAVE_FORMATO,
          EnviadoDesde: Soporte.ENVIADO_DESDE,
          Fecha: "",
          Hora: "",
          NombreLocal: "",
          CodigoLocal: "-1",
          NombrePersona: jQuery("#" + Sistema.PREFIJO_CONTROL + "txtNombrePersona").val(),
          Cargo: oUsuario.PerfilNombre,
          Telefono: jQuery("#" + Sistema.PREFIJO_CONTROL + "txtTelefono").val(),
          Email: jQuery("#" + Sistema.PREFIJO_CONTROL + "txtEmail").val(),
          OrigenTicket: Soporte.ORIGEN_TICKET,
          MotivoTicket: jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlMotivoTicket").select2("val"),
          Aplicacion: jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlAplicacion").select2("val"),
          Modulo: jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlModulo").select2("val"),
          Observacion: jQuery("#" + Sistema.PREFIJO_CONTROL + "txtObservacion").val(),
          MotivoTicketOtro: jQuery("#" + Sistema.PREFIJO_CONTROL + "txtMotivoTicketOtro").val(),
          AplicacionOtro: jQuery("#" + Sistema.PREFIJO_CONTROL + "txtAplicacionOtro").val(),
          ModuloOtro: jQuery("#" + Sistema.PREFIJO_CONTROL + "txtModuloOtro").val(),
          IdUsuarioGestionTicket: -1,
          IdUsuarioTicket: oUsuario.IdUsuario
        }
      };
      dataJSON = Sistema.convertirJSONtoString(dataJSON);

      var okFunc = function (t) {
        var oRetorno = t.d;

        if (oRetorno.Respuesta.Codigo == Sistema.CONST_CODIGO_SQL_ERROR) {
          jQuery("#" + Sistema.PREFIJO_CONTROL + "hBotones").show();
          jQuery("#" + Sistema.PREFIJO_CONTROL + "hMensajeGrabando").hide();
          Sistema.modalMensajeError({ mensaje: oRetorno.Respuesta.Descripcion });
        } else {
          mensaje += "<div>El ticket <strong>Nro. "+ oRetorno.IdTicket +"</strong> fue enviado satisfactoriamente</div>" +
                     "<div>[Ir a <a href=\"./Soporte.MisTicket.aspx\" class=\"alert-link\">Mis Ticket</a>]</div>";

          mensaje = Sistema.mostrarMensajeUsuarioHTML({
                      mensaje: mensaje,
                      tipoMensaje: "success",
                      mostrarBotonCerrar: true,
                      alineacionTexto: "text-center"
                    });
          jQuery("#" + Sistema.PREFIJO_CONTROL + "hMensajeEnvioTicket").html(mensaje);
          jQuery("#" + Sistema.PREFIJO_CONTROL + "hMensajeEnvioTicket").show();
          Soporte.limpiarControles();
        }
      }
      var errFunc = function (t, status) {
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hBotones").show();
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hMensajeGrabando").hide();
        Sistema.modalMensajeError({ mensaje: "Ha ocurrido un error interno y no se pudo enviar el ticket.<br />" + t.statusText });
      }

      jQuery.ajax({
        url: oUsuario.UrlApiTicketSoporte + "/GrabarNuevoTicket",
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: dataJSON,
        success: okFunc,
        error: errFunc,
        timeout: 60000 //60000 = 60 segundos de espera
      });
    } catch (e) {
      alert("Exception: Soporte.grabarNuevoTicket\n" + e);
    }
  },

  limpiarControles: function () {
    try {
      jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlMotivoTicket").select2("val", ""),
      jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlAplicacion").select2("val", ""),
      jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlModulo").select2("val", ""),
      jQuery("#" + Sistema.PREFIJO_CONTROL + "txtObservacion").val(""),

      Soporte.mostrarCampoOtro({ invocadoDesde: "MotivoTicket" });
      Soporte.mostrarCampoOtro({ invocadoDesde: "Aplicacion" });
      Soporte.mostrarCampoOtro({ invocadoDesde: "Modulo" });

      jQuery("#" + Sistema.PREFIJO_CONTROL + "hBotones").show();
      jQuery("#" + Sistema.PREFIJO_CONTROL + "hMensajeGrabando").hide();
    } catch (e) {
      alert("Exception: Soporte.limpiarControles\n" + e);
    }
  },

  validarFormularioBusqueda: function () {
    var htmlCargando;

    try {
      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtIdTicket", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTextoBusqueda", requerido: false, tipo_validacion: "caracteres-prohibidos" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.modalMensajeError();
      } else {
        htmlCargando = Sistema.mensajeCargandoDatos();
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hTablaDatos").html(htmlCargando);
        setTimeout(function () { Soporte.obtenerMisTicket(); }, 1000);
      }
    } catch (e) {
      alert("Exception: Soporte.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  obtenerMisTicket: function () {
    var dataJSON, idTicket, texto;

    try {
      oUsuario = Sistema.obtenerUsuarioConectadoJSON();
      idTicket = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtIdTicket").val();
      texto = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtTextoBusqueda").val();

      //formatea valores
      idTicket = (idTicket == "") ? "-1" : idTicket;
      texto = (texto == "") ? "-1" : texto;

      dataJSON = {
        oParametros: {
          LlaveFormato: Soporte.LLAVE_FORMATO,
          IdUsuario: oUsuario.IdUsuario,
          IdTicket: idTicket,
          Texto: texto,
          Estado: jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlEstado").select2("val")
        }
      };
      dataJSON = Sistema.convertirJSONtoString(dataJSON);

      var okFunc = function (t) {
        var oRetorno = t.d;

        if (oRetorno.Respuesta.Codigo == Sistema.CONST_CODIGO_SQL_ERROR) {
          Sistema.modalMensajeError({ mensaje: oRetorno.Respuesta.Descripcion });
        } else {
          Soporte.dibujarMisTicket(oRetorno.ListadoMisTicket);
        }
      }
      var errFunc = function (t, status) {
        Sistema.modalMensajeError({ mensaje: "Ha ocurrido un error interno y no se pudo obtener mis ticket.<br />" + t.statusText });
      }

      jQuery.ajax({
        url: oUsuario.UrlApiTicketSoporte + "/ObtenerMisTicket",
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: dataJSON,
        success: okFunc,
        error: errFunc,
        timeout: 60000 //60000 = 60 segundos de espera
      });
    } catch (e) {
      alert("Exception: Soporte.obtenerMisTicket\n" + e);
    }
  },

  dibujarMisTicket: function (json) {
    var template, templateAux, totalRegistros, idTicket, fechaHoraTicket, fechaHoraTicketOrden, origenTicket, motivoTicket, aplicacion, modulo, estado;
    var sinRegistros;
    var html = "";

    try {
      template = "<tr>" +
                 "  <td>{INDICE}</td>" +
                 "  <td>{ID_TICKET}</td>" +
                 "  <td>" +
                 "    <span class=\"sist-display-none\" data-about=\"etiqueta solo para ordenar\">{FECHA_HORA_TICKET_ORDEN}</span>" +
                 "    {FECHA_HORA_TICKET}" +
                 "  </td>" +
                 "  <td>{ORIGEN_TICKET}</td>" +
                 "  <td>{APLICACION}</td>" +
                 "  <td>{MODULO}</td>" +
                 "  <td>{MOTIVO_TICKET}</td>" +
                 "  <td>{ESTADO}</td>" +
                 "  <td>" +
                 "    <span class=\"sist-display-none\" data-about=\"etiqueta solo para ordenar\">{FECHA_HORA_TICKET_ORDEN}</span>" +
                 "    <button type=\"button\" class=\"btn btn-default\" onclick=\"Soporte.verDetalleTicket({ idTicket:'{ID_TICKET}' })\">" +
                 "      <span class=\"glyphicon glyphicon-pencil\"></span>" +
                 "    </button>" +
                 "  </td>" +
                 "</tr>";

      sinRegistros = Sistema.mostrarMensajeUsuarioHTML({
        mensaje: "No se encontraron registros asociados",
        tipoMensaje: "warning",
        mostrarBotonCerrar: false,
        alineacionTexto: "text-center"
      });

      if (!json) {
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hTablaDatos").html(sinRegistros);
      } else {
        totalRegistros = json.length;

        if (totalRegistros == 0) {
          jQuery("#" + Sistema.PREFIJO_CONTROL + "hTablaDatos").html(sinRegistros);
        } else {
          html += "<table id=\"tblDatos\" class=\"table table-bordered table-striped\">";
          html += "  <thead>";
          html += "    <tr>";
          html += "      <th>#</th>";
          html += "      <th>Nro Ticket</th>";
          html += "      <th>Fecha Ticket</th>";
          html += "      <th>Origen Ticket</th>";
          html += "      <th>Aplicaci&oacute;n</th>";
          html += "      <th>M&oacute;dulo</th>";
          html += "      <th>Motivo</th>";
          html += "      <th>Estado</th>";
          html += "      <th>Opciones</th>";
          html += "    </tr>";
          html += "  </thead>";
          html += "  <tbody>";

          jQuery.each(json, function (indice, obj) {
            templateAux = template;
            idTicket = obj.IdTicket;
            fechaHoraTicket = obj.FechaHoraTicket;
            fechaHoraTicketOrden = obj.FechaHoraTicketOrden;
            origenTicket = obj.OrigenTicket;
            motivoTicket = obj.MotivoTicket;
            aplicacion = obj.Aplicacion;
            modulo = obj.Modulo;
            estado = obj.Estado;

            templateAux = templateAux.replace(/{INDICE}/ig, indice + 1);
            templateAux = templateAux.replace(/{ID_TICKET}/ig, idTicket);
            templateAux = templateAux.replace(/{FECHA_HORA_TICKET_ORDEN}/ig, fechaHoraTicketOrden);
            templateAux = templateAux.replace(/{FECHA_HORA_TICKET}/ig, fechaHoraTicket);
            templateAux = templateAux.replace(/{ORIGEN_TICKET}/ig, Sistema.htmlEncode(origenTicket));
            templateAux = templateAux.replace(/{MOTIVO_TICKET}/ig, Sistema.htmlEncode(motivoTicket));
            templateAux = templateAux.replace(/{APLICACION}/ig, Sistema.htmlEncode(aplicacion));
            templateAux = templateAux.replace(/{MODULO}/ig, Sistema.htmlEncode(modulo));
            templateAux = templateAux.replace(/{ESTADO}/ig, Sistema.htmlEncode(estado));
            templateAux = templateAux.replace(/{OPCIONES}/ig, "opciones");
            html += templateAux;
          });

          html += "  </tbody>";
          html += "</table>";
          jQuery("#" + Sistema.PREFIJO_CONTROL + "hTablaDatos").html(html);

          setTimeout(function () {
            jQuery("#tblDatos").dataTable({
              "sScrollY": "315px",
              "bScrollCollapse": true,
              "bFilter": false
            });
          }, 100);
        }
      }
    } catch (e) {
      alert("Exception: Soporte.dibujarMisTicket\n" + e);
    }
  },

  verDetalleTicket: function (opciones) {
    try {
      var queryString = "";
      var idTicket = opciones.idTicket;

      queryString += "?id=" + idTicket;

      jQuery.fancybox({
        width: "90%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Soporte.DetalleTicket.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Soporte.verDetalleTicket:\n" + e);
    }
  },

  obtenerDetalleTicket: function (opciones) {
    var oUsuario, dataJSON;

    try {
      oUsuario = Sistema.obtenerUsuarioConectadoJSON();

      dataJSON = {
        idTicket: opciones.idTicket
      };
      dataJSON = Sistema.convertirJSONtoString(dataJSON);

      var okFunc = function (t) {
        var oRetorno = t.d;

        if (oRetorno.Respuesta.Codigo == Sistema.CONST_CODIGO_SQL_ERROR) {
          Sistema.modalMensajeError({ mensaje: oRetorno.Respuesta.Descripcion });
        } else {
          Soporte.cargarDatosDetalleTicket(oRetorno.Ticket);
        }
      }
      var errFunc = function (t, status) {
        Sistema.modalMensajeError({ mensaje: "Ha ocurrido un error interno y no se pudo la información del ticket.<br />" + t.statusText });
      }

      jQuery.ajax({
        url: oUsuario.UrlApiTicketSoporte + "/ObtenerDetalleTicket",
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: dataJSON,
        success: okFunc,
        error: errFunc,
        timeout: 60000 //60000 = 60 segundos de espera
      });

    } catch (e) {
      alert("Exception: Soporte.obtenerDetalleTicket\n" + e);
    }
  },

  cargarDatosDetalleTicket: function (json) {
    var row, idTicket, fechaHoraTicket, origenTicket, motivoTicket, aplicacion, modulo, observacion, estado, resultadoGestion;
    var sinInformacion = "<em>Sin informaci&oacute;n</em>";

    try {
      row = json.Detalle;
      idTicket = row.IdTicket;
      fechaHoraTicket = row.FechaHoraTicket;
      origenTicket = row.OrigenTicket;
      motivoTicket = row.MotivoTicket;
      aplicacion = row.Aplicacion;
      modulo = row.Modulo;
      observacion = row.Observacion;
      estado = row.Estado;
      resultadoGestion = row.ResultadoGestion;

      //formatea valores
      fechaHoraTicket = (fechaHoraTicket == "") ? sinInformacion : fechaHoraTicket;
      origenTicket = (origenTicket == "") ? sinInformacion : origenTicket;
      motivoTicket = (motivoTicket == "") ? sinInformacion : motivoTicket;
      aplicacion = (aplicacion == "") ? sinInformacion : aplicacion;
      modulo = (modulo == "") ? sinInformacion : modulo;
      observacion = (observacion == "") ? sinInformacion : observacion;
      estado = (estado == "") ? sinInformacion : estado;
      resultadoGestion = (resultadoGestion == "") ? sinInformacion : resultadoGestion;

      jQuery("#lblIdRegistro").html("[Estado Actual: " + estado + "]");
      jQuery("#lblIdTicket").html(idTicket);
      jQuery("#lblFechaHoraTicket").html(fechaHoraTicket);
      jQuery("#lblOrigenTicket").html(origenTicket);
      jQuery("#lblMotivoTicket").html(motivoTicket);
      jQuery("#lblAplicacion").html(aplicacion);
      jQuery("#lblModulo").html(modulo);
      jQuery("#lblObservacion").html(observacion);
      jQuery("#lblEstado").html(estado);
      jQuery("#lblResultadoGestion").html(resultadoGestion);
    } catch (e) {
      alert("Exception: Soporte.cargarDatosDetalleTicket\n" + e);
    }
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.Soporte.cargarDesdePopUp(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Soporte.cerrarPopUp:\n" + e);
    }

  },

  cargarDesdePopUp: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception Soporte.cargarDesdePopUp:\n" + e);
    }
  }

};
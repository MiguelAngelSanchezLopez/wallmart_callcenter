﻿var TrazaViaje = {
  validarFormularioBusqueda: function () {
    try {
      var fechaDesde = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaDesde").val();
      var fechaHasta = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaHasta").val();

      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroTransporte", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaDesde", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaHasta", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { valor_a_validar: fechaDesde, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorFechaHasta", mensaje_validacion: "No puede ser menor que Fecha Desde", extras: { fechaInicio: fechaDesde, horaInicio: "00:00", fechaTermino: fechaHasta, horaTermino: "23:59"} }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }
    } catch (e) {
      alert("Exception TrazaViaje.validarFormularioBusqueda:\n" + e);
      return false;
    }
  },

  mostrarCamionMapa: function (opciones) {
    var queryString = "";

    try {
      queryString += "?nroTransporte=" + opciones.nroTransporte;
      queryString += "&patente=" + Sistema.urlEncode(opciones.patente);

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Common.MapaPosicionCamion.aspx" + queryString
      });

    } catch (e) {
      alert("Exception TrazaViaje.mostrarCamionMapa:\n" + e);
    }

  },

  cerrarPopUp: function () {
    try {
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception TrazaViaje.cerrarPopUp:\n" + e);
    }

  },

  validarFormularioMapaPosicionCamion: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: "ddlEstadoRecepcionCamion_ddlCombo", requerido: true }
        , { elemento_a_validar: "txtObservacion", requerido: false, tipo_validacion: "caracteres-prohibidos" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception TrazaViaje.validarFormularioMapaPosicionCamion:\n" + e);
      return false;
    }
  },

  cerrarPopUpMapaPosicionCamion: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.TrazaViaje.cargarDesdePopUpMapaPosicionCamion(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception TrazaViaje.cerrarPopUpMapaPosicionCamion:\n" + e);
    }
  },

  cargarDesdePopUpMapaPosicionCamion: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception TrazaViaje.cargarDesdePopUpMapaPosicionCamion:\n" + e);
    }
  }

};
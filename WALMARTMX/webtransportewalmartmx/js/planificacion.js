﻿var Planificacion = {
  FORMULARIO_ASIGNAR_CAMION: "Planificacion.AsignarCamion",
  FORMULARIO_REASIGNAR_CAMION: "Planificacion.ReasignarCamion",

  validarFormularioCarga: function (event) {
    try {

      if (event) event.preventDefault(); //Cancela el postback

      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "fileCarga", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hBotonValidar").hide();
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hMensajeBotonValidar").show();

        __doPostBack('ctl00$cphContent$btnValidar', '');
      }

    } catch (e) {
      alert("Exception: Planificacion.validarFormularioCarga\n" + e);
      return false;
    }
  },

  validarFormularioBusquedaAsignarCamion: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaPresentacion", requerido: true, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroTransporte", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
      ];

      validar = 1;
      horaPresentacion = jQuery("#" + Sistema.PREFIJO_CONTROL + "wucHoraInicio_ddlHora").val();
      minutosPresentacion = jQuery("#" + Sistema.PREFIJO_CONTROL + "wucHoraInicio_ddlMinutos").val();

      if ((horaPresentacion == "" && minutosPresentacion != "") || (horaPresentacion != "" && minutosPresentacion == "")) {
        validar = -1
      }

      //asigna objeto al validador
      item = {
        valor_a_validar: validar,
        requerido: false,
        tipo_validacion: "numero-entero-positivo",
        contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorHora",
        mensaje_validacion: "Debe ingresar hora y minutos"
      };
      jsonValidarCampos.push(item);

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Planificacion.validarFormularioAsignarCamion\n" + e);
      return false;
    }
  },

  grabarAsignarCamion: function () {
    var jsonValidarCampos = [];
    var item = {};
    var arrNroTransporte, prefijoControl, prefijoControlAux, count, valorValidar;
    var fRequerido = false;

    try {
      //agrega controles al validador
      arrNroTransporte = jQuery("span[data-target=nro-transporte]");
      jQuery.each(arrNroTransporte, function (indice, obj) {
        prefijoControl = jQuery(obj).attr("data-prefijoControl");

        fRequerido = false;
        if (jQuery("#" + prefijoControl + "_txtPatenteTracto").val() != "" ||
            jQuery("#" + prefijoControl + "_txtPatenteTrailer").val() != "" ||
            jQuery("#" + prefijoControl + "_txtNombreConductor").val() != "" ||
            jQuery("#" + prefijoControl + "_txtRutConductor").val() != "" ||
            jQuery("#" + prefijoControl + "_txtCelular").val() != "") {

          fRequerido = true;
        }

        //valida PatenteTracto
        item = { elemento_a_validar: prefijoControl + "_txtPatenteTracto", requerido: fRequerido, tipo_validacion: "patente" };
        jsonValidarCampos.push(item);

        //valida PatenteTrailer
        item = { elemento_a_validar: prefijoControl + "_txtPatenteTrailer", requerido: fRequerido, tipo_validacion: "patente" };
        jsonValidarCampos.push(item);

        //valida NombreConductor
        item = { elemento_a_validar: prefijoControl + "_txtNombreConductor", requerido: fRequerido, tipo_validacion: "caracteres-prohibidos" };
        jsonValidarCampos.push(item);

        //valida RutConductor
        item = { elemento_a_validar: prefijoControl + "_txtRutConductor", requerido: fRequerido, tipo_validacion: "rut" };
        jsonValidarCampos.push(item);

        //valida Celular
        item = { elemento_a_validar: prefijoControl + "_txtCelular", requerido: fRequerido, tipo_validacion: "numero-entero-positivo-mayor-cero" };
        jsonValidarCampos.push(item);

        var bloqueoPTracto = Planificacion.validarPatenteBloqueada({ patente: jQuery("#" + prefijoControl + "_txtPatenteTracto").val() }); // -1: Bloqueado | 1: No bloqueado
        var bloqueoPTrailer = Planificacion.validarPatenteBloqueada({ patente: jQuery("#" + prefijoControl + "_txtPatenteTrailer").val() }); // -1: Bloqueado | 1: No bloqueado

        //valida patente bloqueada
        if (jQuery("#" + prefijoControl + "_txtPatenteTracto").val() != "") {
          item = {
            valor_a_validar: bloqueoPTracto,
            requerido: false,
            tipo_validacion: "numero-entero-positivo",
            contenedor_mensaje_validacion: prefijoControl + "_hErrorPTracto",
            mensaje_validacion: "La placa se encuentra bloqueada"
          };
          jsonValidarCampos.push(item);
        }

        if (jQuery("#" + prefijoControl + "_txtPatenteTrailer").val() != "") {
          item = {
            valor_a_validar: bloqueoPTrailer,
            requerido: false,
            tipo_validacion: "numero-entero-positivo",
            contenedor_mensaje_validacion: prefijoControl + "_hErrorPTrailer",
            mensaje_validacion: "La placa se encuentra bloqueada"
          };
          jsonValidarCampos.push(item);
        }

      });

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        Planificacion.obtenerJSONAsignarCamion();
        return true;
      }

    } catch (e) {
      alert("Exception: Planificacion.grabarAsignarCamion\n" + e);
      return false;
    }
  },

  obtenerJSONAsignarCamion: function () {
    var arrNroTransporte, prefijoControl, jsonIdPlanificacionTransportista, patenteTracto, nombreConductor, idPlanificacionTransportista, idUsuarioTransportista, criterio, observacion;
    var patenteTrailer, rutConductor, celular;
    var jsonAsignarCamion = [];
    var item = {};

    try {
      arrNroTransporte = jQuery("span[data-target=nro-transporte]");
      jQuery.each(arrNroTransporte, function (indice, obj) {
        prefijoControl = jQuery(obj).attr("data-prefijoControl");
        jsonIdPlanificacionTransportista = jQuery("#" + prefijoControl + "_txtJSONIdPlanificacionTransportista").val();
        patenteTracto = jQuery("#" + prefijoControl + "_txtPatenteTracto").val();
        patenteTrailer = jQuery("#" + prefijoControl + "_txtPatenteTrailer").val();
        nombreConductor = jQuery("#" + prefijoControl + "_txtNombreConductor").val();
        rutConductor = jQuery("#" + prefijoControl + "_txtRutConductor").val();
        celular = jQuery("#" + prefijoControl + "_txtCelular").val();
        criterio = jQuery("#" + prefijoControl + "_ddlCriterio").val();
        observacion = jQuery("#" + prefijoControl + "_ddlObservacion_ddlCombo").val();

        jsonIdPlanificacionTransportista = jQuery.parseJSON(jsonIdPlanificacionTransportista);
        jQuery.each(jsonIdPlanificacionTransportista, function (indice, obj) {
          idPlanificacionTransportista = obj.IdPlanificacionTransportista;

          item = {
            IdPlanificacionTransportista: idPlanificacionTransportista,
            PatenteTracto: patenteTracto,
            PatenteTrailer: patenteTrailer,
            NombreConductor: nombreConductor,
            RutConductor: rutConductor,
            Celular: celular,
            Criterio: criterio,
            Observacion: observacion
          };
          jsonAsignarCamion.push(item);
        });

      });

    } catch (e) {
      alert("Exception: Planificacion.obtenerJSONAsignarCamion\n" + e);
      jsonAsignarCamion = [];
    }

    //asigna json en campo oculto
    jQuery("#" + Sistema.PREFIJO_CONTROL + "txtJSONAsignarCamion").val(Sistema.convertirJSONtoString(jsonAsignarCamion));
  },

  reasignarCamion: function (opciones) {
    var queryString = "";

    try {
      queryString += "?idPlanificacion=" + opciones.idPlanificacion;
      queryString += "&nroTransporte=" + opciones.nroTransporte;
      queryString += "&llaveRecargarPagina=" + opciones.llaveRecargarPagina;
      queryString += "&nombrePagina=" + opciones.nombrePagina;

      jQuery.fancybox({
        width: "80%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Planificacion.ReasignarCamion.aspx" + queryString
      });

    } catch (e) {
      alert("Exception Planificacion.reasignarCamion:\n" + e);
    }
  },

  validarFormularioReasignarCamion: function () {
    var idPlanificacion, fechaPresentacion, horaPresentacion, item;

    try {
      idPlanificacion = jQuery("#txtIdPlanificacion").val();
      fechaPresentacion = jQuery("#txtFechaPresentacion").val();
      horaPresentacion = jQuery("#txtHoraPresentacion").val();

      var jsonValidarCampos = [
          { elemento_a_validar: "ddlTransportista_ddlCombo", requerido: true }
        , { elemento_a_validar: "txtCarga", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtTipoCamion", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtPatenteTracto", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtPatenteTrailer", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtNombreConductor", requerido: true, tipo_validacion: "caracteres-prohibidos", contenedor_mensaje_validacion: "hErrorNombreConductor" }
        , { elemento_a_validar: "txtRutConductor", requerido: true, tipo_validacion: "caracteres-prohibidos", contenedor_mensaje_validacion: "hErrorRutConductor" }
        , { elemento_a_validar: "txtCelular", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "ddlObservacion_ddlCombo", requerido: true }
        , { elemento_a_validar: "txtNroTransporte", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtFechaPresentacion", requerido: true, tipo_validacion: "fecha" }
        , { elemento_a_validar: "txtHoraPresentacion", requerido: true, tipo_validacion: "hora" }
      ];

      //valida que la fecha ingresada no sea menor que la actual cuando se ingresa un registro nuevo
      if (idPlanificacion == "-1" && fechaPresentacion != "") {
        item = { valor_a_validar: fechaPresentacion, requerido: false, tipo_validacion: "fecha-menor-hoy", contenedor_mensaje_validacion: "lblMensajeErrorFechaPresentacion", mensaje_validacion: "Fecha y Hora no pueden ser menor a la actual", extras: { fecha: fechaPresentacion, hora: horaPresentacion} };
        jsonValidarCampos.push(item);
      }

      jsonValidarCampos = Planificacion.validarFormularioReasignarCamion_Patentes(jsonValidarCampos);
      jsonValidarCampos = Planificacion.validarFormularioReasignarCamion_OrdenEntrega(jsonValidarCampos);
      jsonValidarCampos = Planificacion.validarFormularioReasignarCamion_IngresoMinimoLocales(jsonValidarCampos);
      jsonValidarCampos = Planificacion.validarFormularioReasignarCamion_CambioLocales(jsonValidarCampos);

      //valida todo el formulario
      var esValido = Validacion.validarControles(jsonValidarCampos);
      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        Planificacion.actualizarJSONLocalesSeleccionados();
        return true;
      }

    } catch (e) {
      alert("Exception: Planificacion.validarFormularioReasignarCamion\n" + e);
      return false;
    }
  },

  validarFormularioReasignarCamion_Patentes: function (jsonValidarCampos) {
    var idTransportista, patenteTracto, patenteTrailer, bloqueoPTracto, bloqueoPTrailer, pertenecePatenteTracto, pertenecePatenteTrailer;

    try {
      idTransportista = jQuery("#ddlTransportista_ddlCombo").val();
      patenteTracto = jQuery("#txtPatenteTracto").val();
      patenteTrailer = jQuery("#txtPatenteTrailer").val();

      //--------------------------------------------
      //valida patente bloqueada
      bloqueoPTracto = Planificacion.validarPatenteBloqueada({ patente: patenteTracto });
      bloqueoPTrailer = Planificacion.validarPatenteBloqueada({ patente: patenteTrailer });

      //--------------------------------------------
      //valida que la patente pertenezca al transportista
      pertenecePatenteTracto = Planificacion.validarPertenecePatenteTransportista({ idTransportista: idTransportista, patente: patenteTracto });
      pertenecePatenteTrailer = Planificacion.validarPertenecePatenteTransportista({ idTransportista: idTransportista, patente: patenteTrailer });

      if (patenteTracto != "") {
        //asigna validacion de bloqueo
        item = { valor_a_validar: bloqueoPTracto, requerido: false, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: "hErrorPTracto", mensaje_validacion: "La placa se encuentra bloqueada" };
        jsonValidarCampos.push(item);

        //asigna validacion si pertenece patente
        item = { valor_a_validar: pertenecePatenteTracto, requerido: false, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: "hErrorPertenecePatenteTracto", mensaje_validacion: "La placa no le pertenece" };
        jsonValidarCampos.push(item);
      }

      if (patenteTrailer != "") {
        //asigna validacion de bloqueo
        item = { valor_a_validar: bloqueoPTrailer, requerido: false, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: "hErrorPTrailer", mensaje_validacion: "La placa se encuentra bloqueada" };
        jsonValidarCampos.push(item);

        //asigna validacion si pertenece patente
        item = { valor_a_validar: pertenecePatenteTrailer, requerido: false, tipo_validacion: "numero-entero-positivo", contenedor_mensaje_validacion: "hErrorPertenecePatenteTrailer", mensaje_validacion: "La placa no le pertenece" };
        jsonValidarCampos.push(item);
      }
    } catch (e) {
      alert("Exception: Planificacion.validarFormularioReasignarCamion_Patentes\n" + e);
    }

    return jsonValidarCampos;
  },

  validarFormularioReasignarCamion_OrdenEntrega: function (jsonValidarCampos) {
    var arrIdLocal, prefijoControl, item, prefijoControlAux, totalEncontrados, valorValidar, ordenEntrega, ordenEntregaComparar;

    try {
      arrIdLocal = jQuery("input[data-targetLocal=id-local]");

      //--------------------------------------------
      //recorre los locales
      jQuery.each(arrIdLocal, function (indice, obj) {
        prefijoControl = jQuery(obj).attr("data-prefijoControl");
        if (prefijoControl != "{PREFIJO_CONTROL}") {
          item = { elemento_a_validar: prefijoControl + "_txtOrdenEntrega", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" };
          jsonValidarCampos.push(item);

          //[begin: ---------------------------------------------------
          //verifica que el OrdenEntrega no se repita
          totalEncontrados = 0;
          valorValidar = 1; // se inicializa que el valor no esta repetido (1: No repetido | -1: repetido)
          ordenEntrega = jQuery("#" + prefijoControl + "_txtOrdenEntrega").val();

          jQuery(arrIdLocal).each(function (indice, obj) {
            prefijoControlAux = jQuery(obj).attr("data-prefijoControl");

            if (prefijoControlAux != "{PREFIJO_CONTROL}") {
              ordenEntregaComparar = jQuery("#" + prefijoControlAux + "_txtOrdenEntrega").val();

              if (ordenEntrega == ordenEntregaComparar) {
                totalEncontrados++;
                if (totalEncontrados > 1) {
                  valorValidar = -1;
                  return false;
                }
              }
            }
          });

          //asigna la validacion por control
          item = {
            valor_a_validar: valorValidar,
            requerido: false,
            tipo_validacion: "numero-entero-positivo",
            contenedor_mensaje_validacion: prefijoControl + "_hErrorOrdenEntrega",
            mensaje_validacion: "El valor est&aacute; repetido"
          };
          jsonValidarCampos.push(item);
          //end] ---------------------------------------------------
        }

      });
    } catch (e) {
      alert("Exception: Planificacion.validarFormularioReasignarCamion_OrdenEntrega\n" + e);
    }

    return jsonValidarCampos;
  },

  validarFormularioReasignarCamion_IngresoMinimoLocales: function (jsonValidarCampos) {
    var totalFilas, valorValidar, item;

    try {
      //--------------------------------------------
      //valida que se ingrese al menos 1 local
      totalFilas = jQuery("#tblLocalesAsociados >tbody >tr").length;
      valorValidar = (totalFilas == 1) ? -1 : 1; // (1: Tiene locales | -1: No tiene locales)
      item = {
        valor_a_validar: valorValidar,
        requerido: false,
        tipo_validacion: "numero-entero-positivo",
        contenedor_mensaje_validacion: "hErrorTablaLocales",
        mensaje_validacion: "Debe agregar al menos 1 tienda"
      };
      jsonValidarCampos.push(item);

    } catch (e) {
      alert("Exception: Planificacion.validarFormularioReasignarCamion_IngresoMinimoLocales\n" + e);
    }

    return jsonValidarCampos;
  },

  validarFormularioReasignarCamion_CambioLocales: function (jsonValidarCampos) {
    var idPlanificacion, jsonLocalesValorActual, jsonLocalesSeleccionados, hayCambioLocales, totalCoincidenciaLocal, idLocalValorActual;
    var idLocalSeleccionado, nroTransporteActual, nroTransporteIngresado, valorValidar, mensajeValidacionNroTransporte, ordenEntregaValorActual;
    var ordenEntregaSeleccionado, claveOrdenEntregaValorActual, claveOrdenEntregaSeleccionado, item;

    try {
      idPlanificacion = jQuery("#txtIdPlanificacion").val();

      //--------------------------------------------
      //valida si hay cambio de locales
      jsonLocalesValorActual = jQuery("#txtJSONLocalesSeleccionados_ValorActual").val();
      jsonLocalesSeleccionados = jQuery("#txtJSONLocalesSeleccionados").val();
      jsonLocalesValorActual = jQuery.parseJSON(jsonLocalesValorActual);
      jsonLocalesSeleccionados = jQuery.parseJSON(jsonLocalesSeleccionados);

      if (jsonLocalesValorActual.length != jsonLocalesSeleccionados.length) {
        hayCambioLocales = "1";
      } else {
        //compara los locales de los json si son iguales
        hayCambioLocales = "0"; // se inicializa valor que no hay cambio de locales

        //obtiene local para comparar con el resto
        jQuery.each(jsonLocalesValorActual, function (indice, obj) {
          totalCoincidenciaLocal = 0;
          idLocalValorActual = obj.IdLocal;
          ordenEntregaSeleccionado = obj.OrdenEntrega;
          claveOrdenEntregaValorActual = idLocalValorActual + " - " + ordenEntregaSeleccionado;

          //recorre el resto de los locales
          jQuery.each(jsonLocalesSeleccionados, function (indice, obj) {
            idLocalSeleccionado = obj.IdLocal;
            ordenEntregaSeleccionado = jQuery("#" + idLocalSeleccionado + "_txtOrdenEntrega").val();
            claveOrdenEntregaSeleccionado = idLocalSeleccionado + " - " + ordenEntregaSeleccionado;

            //compara que el local sea el mismo y que este en la misma secuencia
            if (idLocalValorActual == idLocalSeleccionado && claveOrdenEntregaValorActual == claveOrdenEntregaSeleccionado) {
              totalCoincidenciaLocal += 1;
            }
            if (totalCoincidenciaLocal > 0) { return false; }
          });

          //si no se encontro ninguna coincidencia entonces marca que hay diferencias en locales
          if (totalCoincidenciaLocal == 0) {
            hayCambioLocales = "1";
            return false;
          }
        });
      }
      jQuery("#txtHayCambioLocales").val(hayCambioLocales);

      //si hay cambio de locales entonces exige que se modifique el NroTransporte
      nroTransporteActual = jQuery("#txtNroTransporte_ValorActual").val();
      nroTransporteIngresado = jQuery("#txtNroTransporte").val();

      if (hayCambioLocales == "1") {
        jQuery("#txtNroTransporte").attr("disabled", false);

        if (nroTransporteActual == nroTransporteIngresado) {
          valorValidar = -1; //debe ingresar nuevo NroTransporte
          mensajeValidacionNroTransporte = "Debe ingresar un nuevo IdMaster";
        } else {
          valorValidar = Planificacion.validarDuplicidadNroTransporte(); // (-1: debe ingresar nuevo NroTransporte | 1: mantiene el NroTransporte actual)
          mensajeValidacionNroTransporte = "El IdMaster ya fue asignado en el sistema";
        }
      } else {
        //si se esta ingresando una nueva programacion, entonces se mantiene habilitado el campo NroTransporte
        if (idPlanificacion == "-1") {
          jQuery("#txtNroTransporte").attr("disabled", false);
        } else {
          jQuery("#txtNroTransporte").val(nroTransporteActual);
          jQuery("#txtNroTransporte").attr("disabled", true);
        }
        valorValidar = 1; // mantiene el NroTransporte actual
      }
      item = {
        valor_a_validar: valorValidar,
        requerido: false,
        tipo_validacion: "numero-entero-positivo-mayor-cero",
        contenedor_mensaje_validacion: "hErrorNroTransporte",
        mensaje_validacion: mensajeValidacionNroTransporte
      };
      jsonValidarCampos.push(item);

    } catch (e) {
      alert("Exception: Planificacion.validarFormularioReasignarCamion_CambioLocales\n" + e);
    }

    return jsonValidarCampos;
  },

  validarPatenteBloqueada: function (opciones) {
    var queryString = "";
    var existe = "-1";

    try {

      queryString += "op=ValidarPatenteBloqueada";
      queryString += "&patente=" + opciones.patente;

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar la placa: " + opciones.patente);
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar la placa: " + opciones.patente);
      }

      jQuery.ajax({
        url: "../webAjax/waSistema.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Planificacion.validarPatenteBloqueada:\n" + e);
    }

    return existe;
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Planificacion.cerrarPopUp:\n" + e);
    }

  },

  verHistorialReasignacionCamion: function (opciones) {
    var queryString = "";
    var idPlanificacion, nroTransporte;

    try {
      queryString += "?idPlanificacion=" + opciones.idPlanificacion;
      queryString += "&nroTransporte=" + opciones.nroTransporte;

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Planificacion.HistorialReasignacionCamion.aspx" + queryString
      });

    } catch (e) {
      alert("Exception Planificacion.verHistorialReasignacionCamion:\n" + e);
    }

  },

  eliminarRegistro: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarPlanificacionTransportista";
      queryString += "&idPlanificacion=" + opciones.idPlanificacion;
      queryString += "&nroTransporte=" + opciones.nroTransporte;
      queryString += "&llaveRecargarPagina=" + Sistema.urlEncode(opciones.llaveRecargarPagina);

      if (confirm("Si elimina la planificación no podrá volver a recuperarla.\n¿Desea eliminar la planificación?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar la planificación");
          } else {
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar la planificación");
        }
        jQuery.ajax({
          url: "../webAjax/waSistema.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Planificacion.eliminarRegistro\n" + e);
    }
  },

  validarFormularioBusquedaRevisarProgramacion: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaPresentacion", requerido: true, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroTransporte", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtPatenteTracto", requerido: false, tipo_validacion: "patente" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtPatenteTrailer", requerido: false, tipo_validacion: "patente" }
      ];

      validar = 1;
      horaPresentacion = jQuery("#" + Sistema.PREFIJO_CONTROL + "wucHoraInicio_ddlHora").val();
      minutosPresentacion = jQuery("#" + Sistema.PREFIJO_CONTROL + "wucHoraInicio_ddlMinutos").val();

      if ((horaPresentacion == "" && minutosPresentacion != "") || (horaPresentacion != "" && minutosPresentacion == "")) {
        validar = -1
      }

      //asigna objeto al validador
      item = {
        valor_a_validar: validar,
        requerido: false,
        tipo_validacion: "numero-entero-positivo",
        contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorHora",
        mensaje_validacion: "Debe ingresar hora y minutos"
      };
      jsonValidarCampos.push(item);

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Planificacion.validarFormularioBusquedaRevisarProgramacion\n" + e);
      return false;
    }
  },

  notificarAsignacionCamiones: function () {
    try {
      if (confirm("Si notifica la asignación se finalizará el proceso\n¿Está seguro de continuar con la notificación?")) {
        Planificacion.obtenerJSONAsignarCamion();
        return true;
      } else {
        return false;
      }

    } catch (e) {
      alert("Exception: Planificacion.notificarAsignacionCamiones\n" + e);
      return false;
    }
  },

  grabarRevisarProgramacion: function () {
    var jsonValidarCampos = [];
    var item = {};
    var arrNroTransporte, prefijoControl;

    try {
      //agrega controles al validador
      arrNroTransporte = jQuery("span[data-target=nro-transporte]");
      jQuery.each(arrNroTransporte, function (indice, obj) {
        prefijoControl = jQuery(obj).attr("data-prefijoControl");

        //valida Anden
        item = { elemento_a_validar: prefijoControl + "_txtAnden", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" };
        jsonValidarCampos.push(item);
      });

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        Planificacion.obtenerJSONRevisarProgramacion();
        return true;
      }

    } catch (e) {
      alert("Exception: Planificacion.grabarRevisarProgramacion\n" + e);
      return false;
    }
  },

  obtenerJSONRevisarProgramacion: function () {
    var arrNroTransporte, prefijoControl, jsonIdPlanificacionTransportista, anden, idPlanificacionTransportista;
    var jsonRevisarProgramacion = [];
    var item = {};

    try {
      arrNroTransporte = jQuery("span[data-target=nro-transporte]");
      jQuery.each(arrNroTransporte, function (indice, obj) {
        prefijoControl = jQuery(obj).attr("data-prefijoControl");
        jsonIdPlanificacionTransportista = jQuery("#" + prefijoControl + "_txtJSONIdPlanificacionTransportista").val();
        anden = jQuery("#" + prefijoControl + "_txtAnden").val();

        jsonIdPlanificacionTransportista = jQuery.parseJSON(jsonIdPlanificacionTransportista);
        jQuery.each(jsonIdPlanificacionTransportista, function (indice, obj) {
          idPlanificacionTransportista = obj.IdPlanificacionTransportista;

          item = {
            IdPlanificacionTransportista: idPlanificacionTransportista,
            Anden: anden
          };
          jsonRevisarProgramacion.push(item);
        });

      });

    } catch (e) {
      alert("Exception: Planificacion.obtenerJSONRevisarProgramacion\n" + e);
      jsonRevisarProgramacion = [];
    }

    //asigna json en campo oculto
    jQuery("#" + Sistema.PREFIJO_CONTROL + "txtJSONRevisarProgramacion").val(Sistema.convertirJSONtoString(jsonRevisarProgramacion));
  },

  verInformacionPatio: function (opciones) {
    var queryString = "";
    var idPlanificacion, nroTransporte;

    try {
      queryString += "?idPlanificacion=" + opciones.idPlanificacion;
      queryString += "&nroTransporte=" + opciones.nroTransporte;
      queryString += "&llaveRecargarPagina=" + Sistema.urlEncode(opciones.llaveRecargarPagina);

      jQuery.fancybox({
        width: "50%",
        height: "70%",
        modal: false,
        type: "iframe",
        href: "../page/Planificacion.InformacionPatio.aspx" + queryString
      });

    } catch (e) {
      alert("Exception Planificacion.verInformacionPatio:\n" + e);
    }

  },

  asociarLocal: function (json) {
    var item, idLocal, jsonLocales, localExiste;

    try {
      item = json.item;
      idLocal = item.value;
      localExiste = false;

      //valida que el local no haya sido asociado previamente
      jsonLocales = jQuery("#txtJSONLocalesSeleccionados").val();
      jsonLocales = jQuery.parseJSON(jsonLocales);
      jQuery.each(jsonLocales, function (indice, obj) {
        if (obj.IdLocal == idLocal) {
          localExiste = true;
          return false;
        }
      });

      if (localExiste) {
        alert("La tienda ya fue agregada al listado");
      } else {
        Planificacion.agregarFilaLocalAsociado({ elem_tabla: "tblLocalesAsociados", data: item });
      }

      //limpia el control de busqueda
      setTimeout(function () {
        jQuery("#txtLocalAutocomplete").val("");
      }, 100);

    } catch (e) {
      alert("Exception Planificacion.asociarLocal:\n" + e);
    }

  },

  agregarFilaLocalAsociado: function (opciones) {
    var objFila, templateFila, idLocal, nombreLocal, jsonValidarCampos, esValido, jsonLocales, elemTabla, item, totalFilas;

    try {
      elemTabla = opciones.elem_tabla;
      item = opciones.data;
      idLocal = item.value;
      nombreLocal = item.nombreLocal;
      objFila = jQuery("#" + elemTabla + " tbody tr:eq(0)").clone();
      objFila.removeClass("sist-display-none");
      templateFila = objFila.wrap("<div />").parent().html(); // obtiene el html del clone

      //calcula el ultimo orden de entrega
      totalFilas = jQuery("#" + elemTabla + " >tbody >tr").length;

      templateFila = templateFila.replace(/{PREFIJO_CONTROL}/ig, idLocal);
      templateFila = templateFila.replace(/{ID_LOCAL}/ig, idLocal);
      templateFila = templateFila.replace(/{NOMBRE_LOCAL}/ig, nombreLocal);
      templateFila = templateFila.replace(/{ORDEN_ENTREGA}/ig, totalFilas);
      templateFila = templateFila.replace(/{CLASS_TR}/ig, "");
      jQuery("#" + elemTabla).append(templateFila);

      //agrega local al json
      jsonLocales = jQuery("#txtJSONLocalesSeleccionados").val();
      jsonLocales = jQuery.parseJSON(jsonLocales);
      item = {
        IdPlanificacionTransportista: -1,
        IdLocal: idLocal,
        NombreLocal: nombreLocal,
        OrdenEntrega: -1
      };
      jsonLocales.push(item);
      jQuery("#txtJSONLocalesSeleccionados").val(Sistema.convertirJSONtoString(jsonLocales));

    } catch (e) {
      alert("Exception: Planificacion.agregarFilaLocalAsociado\n" + e);
    }
  },

  eliminarLocalAsociado: function (opciones) {
    var idLocal, jsonLocales;

    try {
      idLocal = opciones.idLocal;

      if (confirm("¿Está seguro de eliminar la tienda asociada?")) {
        //elimina la fila de la tabla
        jQuery("#" + idLocal + "_tr").remove();

        //elimina local del json
        jsonLocales = jQuery("#txtJSONLocalesSeleccionados").val();
        jsonLocales = jQuery.parseJSON(jsonLocales);
        jQuery.each(jsonLocales, function (indice, obj) {
          if (obj.IdLocal == idLocal) {
            jsonLocales.splice(indice, 1);
            return false;
          }
        });
        jQuery("#txtJSONLocalesSeleccionados").val(Sistema.convertirJSONtoString(jsonLocales));
      }

    } catch (e) {
      alert("Exception: Planificacion.eliminarLocalAsociado\n" + e);
    }

  },

  actualizarJSONLocalesSeleccionados: function () {
    var jsonLocales, idLocal, ordenEntrega;

    try {
      jsonLocales = jQuery("#txtJSONLocalesSeleccionados").val();
      jsonLocales = jQuery.parseJSON(jsonLocales);
      jQuery.each(jsonLocales, function (indice, obj) {
        idLocal = obj.IdLocal;
        ordenEntrega = jQuery("#" + idLocal + "_txtOrdenEntrega").val();
        obj.OrdenEntrega = ordenEntrega;
      });

      //ordena json
      jsonLocales = Sistema.ordenarJSON({ json: jsonLocales, atributoPorOrdenar: "OrdenEntrega", tipoOrden: "Asc" });
      jQuery("#txtJSONLocalesSeleccionados").val(Sistema.convertirJSONtoString(jsonLocales));
    } catch (e) {
      alert("Exception: Planificacion.actualizarJSONLocalesSeleccionados\n" + e);
    }
  },

  validarDuplicidadNroTransporte: function () {
    var queryString = "";
    var existe = "-1";
    var opcion, valor, idPlanificacion, nroTransporteValorActual;

    try {
      idPlanificacion = jQuery("#txtIdPlanificacion").val();
      nroTransporteValorActual = jQuery("#txtNroTransporte_ValorActual").val();
      valor = jQuery("#txtNroTransporte").val();

      queryString += "op=PlanificacionTransportista.NroTransporte";
      queryString += "&id=" + idPlanificacion;
      queryString += "&valor=" + Sistema.urlEncode(valor);
      queryString += "&nroTransporteValorActual=" + Sistema.urlEncode(nroTransporteValorActual);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el IdMaster único");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el IdMaster único");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Planificacion.validarDuplicidadNroTransporte:\n" + e);
    }

    return existe;
  },

  cargarDatosConductor: function (opciones) {
    var item, nombreConductor, rutConductor, telefono, prefijoControl;

    try {
      json = opciones.data;
      objInput = opciones.objInput;
      invocadoDesde = opciones.invocadoDesde;
      item = json.item;
      nombreConductor = item.nombreConductor;
      rutConductor = item.rutConductor;
      telefono = item.telefono;

      switch (invocadoDesde) {
        case Planificacion.FORMULARIO_ASIGNAR_CAMION:
          prefijoControl = objInput.attr("data-prefijoControl");
          //asigna valores
          setTimeout(function () {
            jQuery("#" + prefijoControl + "_txtNombreConductor").val(nombreConductor);
            jQuery("#" + prefijoControl + "_txtRutConductor").val(rutConductor);
            jQuery("#" + prefijoControl + "_txtCelular").val(telefono);
          }, 100);
          break;

        case Planificacion.FORMULARIO_REASIGNAR_CAMION:
          setTimeout(function () {
            jQuery("#txtNombreConductor").val(nombreConductor);
            jQuery("#txtRutConductor").val(rutConductor);
            jQuery("#txtCelular").val(telefono);
          }, 100);
          break;

        default:
          break;
      }

    } catch (e) {
      alert("Exception Planificacion.cargarDatosConductor:\n" + e);
    }

  },

  asignarAutocompletarConductor: function () {
    var idTransportista;

    try {
      idTransportista = jQuery("#ddlTransportista_ddlCombo").val();

      jQuery(".autocomplete-conductor").autocomplete({
        source: "../webAjax/waSistema.aspx?op=AutocompletarConductorTransportista&idUsuarioTransportista=" + idTransportista,
        select: function (event, json) { Planificacion.cargarDatosConductor({ data: json, objInput: jQuery(this), invocadoDesde: Planificacion.FORMULARIO_REASIGNAR_CAMION }); },
        minLength: 1,
        create: function () {
          jQuery(this).data('ui-autocomplete')._renderItem = function (ul, item) {
            var autocomplete_template = "";
            autocomplete_template += "<span class=\"sist-font-size-12\" style=\"font-family:Arial\">";
            autocomplete_template += "<strong>{NOMBRE_CONDUCTOR}</strong><br />";
            autocomplete_template += "<span class=\"small\"><em>Rut: {RUT_CONDUCTOR} / Celular: {TELEFONO}</em></span>";
            autocomplete_template += "</span>";

            //reemplaza marcas
            autocomplete_template = autocomplete_template.replace("{NOMBRE_CONDUCTOR}", item.nombreConductor);
            autocomplete_template = autocomplete_template.replace("{RUT_CONDUCTOR}", item.rutConductor);
            autocomplete_template = autocomplete_template.replace("{TELEFONO}", item.telefono);

            return jQuery("<li>")
                         .append("<a>" + autocomplete_template + "</a>")
                         .appendTo(ul);
          };
        }
      });

    } catch (e) {
      alert("Exception Planificacion.asignarAutocompletarConductor:\n" + e);
    }
  },

  validarPertenecePatenteTransportista: function (opciones) {
    var queryString = "";
    var existe = "-1";
    var idTransportista, patente;

    try {
      idTransportista = opciones.idTransportista;
      patente = opciones.patente;

      queryString += "op=ValidarPertenecePatenteTransportista";
      queryString += "&idTransportista=" + idTransportista;
      queryString += "&patente=" + patente;

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar si la placa " + patente + " pertenece a la Línea de Transporte");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar si la placa " + patente + " pertenece a la Línea de Transporte");
      }

      jQuery.ajax({
        url: "../webAjax/waSistema.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Planificacion.validarPertenecePatenteTransportista:\n" + e);
    }

    return existe;
  }

};
﻿//--------------------------------------------------------------
//Metodo      : wucmsAgregarItem
//Descripcion : agrega los item seleccionados del padre(multiselect de la izquierda) a la hija(multiselect de la derecha)
//Por         : VSR, 21/08/2008
function wucmsAgregarItem() {
  // usamos la funcion para trasparas los elementos seleccionado al lb de los seleccionados
  wucmsMoveOption(lstPadre, lstHija);
  //obtiene los item seleccionados
  wucmsObtenerIdSeleccionados();
  //verifica si muestra el check todos
  wucmsMostrarCheckTodos(lstPadre, chkTodosPadre, holderCheckTodosPadre);
  wucmsMostrarCheckTodos(lstHija, chkTodosHija, holderCheckTodosHija);
  return false;
}

//--------------------------------------------------------------
//Metodo      : wucmsQuitarItem
//Descripcion : quita los item seleccionados de la hija(multiselect de la derecha) y los agrega al padre(multiselect de la izquierda) 
//Por         : VSR, 21/08/2008
function wucmsQuitarItem() {
  // usamos la funcion para trasparas los elementos seleccionado al lb de los seleccionados
  wucmsMoveOption(lstHija, lstPadre);
  //obtiene los item seleccionados
  wucmsObtenerIdSeleccionados();
  //verifica si muestra el check todos
  wucmsMostrarCheckTodos(lstPadre, chkTodosPadre, holderCheckTodosPadre);
  wucmsMostrarCheckTodos(lstHija, chkTodosHija, holderCheckTodosHija);
  return false;
}

//--------------------------------------------------------------
//Metodo      : wucmsObtenerIdSeleccionados
//Descripcion : actualiza la lista de id de los item seleccionados
//Por         : VSR, 21/08/2008
function wucmsObtenerIdSeleccionados() {
  var lista = "";
  txtIdsHija.value = "";
  //construye la lista separados por coma
  for (var i = 0; i < lstHija.length; i++) {
    id = lstHija.options[i].value;
    lista += (lista == "") ? id : "," + id;
  }
  //asigna valor
  txtIdsHija.value = lista;

  //invoca a funciones especiales
  if (txtFuncionesEspeciales.value != "") {
    eval(txtFuncionesEspeciales.value);
  }
}

//--------------------------------------------------------------
//Metodo      : wucmsMostrarCheckTodos
//Descripcion : verifica si se debe o no mostrar el check todos
//Por         : VSR, 22/08/2008
function wucmsMostrarCheckTodos(objListBox, objCheckTodos, objHolderCheckTodos) {
  if (objCheckTodos) {
    objCheckTodos.checked = false;
    objHolderCheckTodos.style.display = (objListBox.length == 0) ? "none" : "block";
  }
}

//--------------------------------------------------------------
//Metodo      : wucmsMoveOption
//Descripcion : function para copiar elementos de un select a otro. 
//              con mod para que no copie cuando el value del elemento a copiar ya existe en el select destino
//Por         : EM
function wucmsMoveOption(objSourceElement, objTargetElement) {
  var aryTempSourceOptions = new Array();
  var aryTempTargetOptions = new Array();
  var x = 0;
  var existe = false;

  //looping through source element to find selected options
  for (var i = 0; i < objSourceElement.length; i++) {
    if (objSourceElement.options[i].selected) {
      // revisamos que el value de este elemento no este en el target
      existe = false;
      for (var t = 0; t < objTargetElement.length; t++) {
        if (objTargetElement.options[t].value == objSourceElement.options[i].value) {
          // esto es para que no elimine el elemento seleccionado (pero no movido) del select origen
          var objTempValues = new Object();
          objTempValues.text = objSourceElement.options[i].text;
          objTempValues.value = objSourceElement.options[i].value;
          aryTempSourceOptions[x] = objTempValues;
          x++;
          // decimos que el elemento ya existe
          existe = true;
          break;
        }
      }
      if (!existe) {
        //need to move this option to target element
        var intTargetLen = objTargetElement.length++;
        objTargetElement.options[intTargetLen].text = objSourceElement.options[i].text;
        objTargetElement.options[intTargetLen].value = objSourceElement.options[i].value;
      }
    }
    else {
      //storing options that stay to recreate select element
      var objTempValues = new Object();
      objTempValues.text = objSourceElement.options[i].text;
      objTempValues.value = objSourceElement.options[i].value;
      aryTempSourceOptions[x] = objTempValues;
      x++;
    }
  }

  //sorting and refilling target list
  for (var i = 0; i < objTargetElement.length; i++) {
    var objTempValues = new Object();
    objTempValues.text = objTargetElement.options[i].text;
    objTempValues.value = objTargetElement.options[i].value;
    aryTempTargetOptions[i] = objTempValues;
  }

  aryTempTargetOptions.sort(wucmsSortByText);

  for (var i = 0; i < objTargetElement.length; i++) {
    objTargetElement.options[i].text = aryTempTargetOptions[i].text;
    objTargetElement.options[i].value = aryTempTargetOptions[i].value;
    objTargetElement.options[i].selected = false;
  }

  //resetting length of source
  objSourceElement.length = aryTempSourceOptions.length;
  //looping through temp array to recreate source select element
  for (var i = 0; i < aryTempSourceOptions.length; i++) {
    objSourceElement.options[i].text = aryTempSourceOptions[i].text;
    objSourceElement.options[i].value = aryTempSourceOptions[i].value;
    objSourceElement.options[i].selected = false;
  }
}

//--------------------------------------------------------------
//Metodo      : wucmsSortByText
//Descripcion : orden los elementos
//Por         : EM
function wucmsSortByText(a, b) {
  if (a.text < b.text) { return -1 }
  if (a.text > b.text) { return 1 }
  return 0;
}

//------------------------------------------------------------------
//Metodo      : wucmsSelectAll()
//Descripcion : marca o desmarca todos los item
//Por         : VSR, 07/05/2008
function wucmsSelectAll(nombreListBox, nombreCheckBox) {
  var objList = document.getElementById(nombreListBox);
  var objCheck = document.getElementById(nombreCheckBox);
  if (objList && objCheck) {
    for (var i = 0; i < objList.length; i++) {
      objList.options[i].selected = objCheck.checked;
    }
  }
  return false;
}

//------------------------------------------------------------------
//Metodo      : wucmsSelectAllConValorBooleano()
//Descripcion : marca o desmarca todos los item
//Por         : VSR, 01/09/2008
function wucmsSelectAllConValorBooleano(nombreListBox, marcado) {
  var objList = document.getElementById(nombreListBox);
  if (objList) {
    for (var i = 0; i < objList.length; i++) {
      objList.options[i].selected = marcado;
    }
  }
  return false;
}

//------------------------------------------------------------------
//Descripcion : obtiene valores seleccionados
//Por         : VSR, 01/09/2008
function wucmsListadoIdSeleccionados() {
  return txtIdsHija.value;
}

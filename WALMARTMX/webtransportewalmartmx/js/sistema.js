﻿var Sistema = {
    SIMULAR_ACTIVEX: false,
    PREFIJO_CONTROL: "ctl00_cphContent_",
    CONST_CODIGO_SQL_VALOR_DUPLICADO: -100,
    CONST_CODIGO_SQL_ERROR: -200,
    CONST_CODIGO_SIN_CONEXION: -201,
    CONST_CODIGO_SIN_SESION: -500,
    CONST_CODIGO_SQL_EXITO: 200,
    CONST_CODIGO_SQL_SIN_DATOS: 404,
    CONST_CODIGO_TEXTO_NO_PERMITIDO: 550,
    PAGINA_LOGOUT: "../common/logout.aspx",
    SETINTERVAL_CALLCENTER_LLAMADO: 0,
    SETINTERVAL_MENSAJE_USUARIO: 0,
    SETINTERVAL_MILISEGUNDOS_MENSAJE_USUARIO: 60000, // 60000 (1 minuto)
    SETINTERVAL_MOSTRAR_FINALIZAR_LLAMADA: 0,
    CALLCENTER_CAMPAIGN: "walmart",
    CALLCENTER_ESTADO_LLAMADA_LOCKED: "LOCKED",
    CALLCENTER_ESTADO_LLAMADA_TAKED: "TAKED",
    CALLCENTER_ESTADO_LLAMADA_ENDED: "ENDED",
    CALLCENTER_ESTADO_LLAMADA_OCUPADO: "INCONCERT OCUPADO", //estado creado en forma manual para determinar si Inconcert no esta disponible
    FANCYBOX_OVERLAY_COLOR_DEFECTO: "rgba(119, 119, 119, 0.7)", //#777
    FANCYBOX_OVERLAY_COLOR_SUBNIVEL: "rgba(240, 240, 240, 0.7)", //#FFF
    CONST_LLAVE_PASSWORD: "!4lt0_$#",

    //llave perfiles
    KEY_PERFIL_ADMINISTRADOR: "ADM",
    KEY_PERFIL_CONDUCTOR: "CON",
    KEY_PERFIL_TELEOPERADOR: "TO",
    KEY_PERFIL_SUPERVISOR: "SUP",
    KEY_PERFIL_GENERICO: "GEN",
    KEY_PERFIL_TRANSPORTISTA: "TRA",
    KEY_PERFIL_SUPERVISOR_PROTECCION_ACTIVOS: "SPA",
    KEY_PERFIL_JEFE_AREA_PROTECCION_ACTIVOS: "JPA",
    KEY_PERFIL_SUBGERENTE_PROTECCION_ACTIVOS: "SBPA",
    KEY_PERFIL_JEFE_AREA_TRANSPORTES: "JAT",
    KEY_PERFIL_SUBGERENTE_TRANSPORTES: "SDT",
    KEY_PERFIL_SUPERVISOR_CONTROL_FLOTA_CEDIS: "SCFC",
    KEY_PERFIL_GERENCIA_TRANSPORTES: "GT",
    KEY_PERFIL_COMMANAGER: "CM",
    KEY_PERFIL_GERENTE_TIENDA: "GRT",
    KEY_PERFIL_GERENTE_DISTRITAL_TIENDA: "GDT",
    KEY_PERFIL_LIDER_RECIBO_TIENDA: "LRT",
    KEY_PERFIL_COORDINADOR_LINEA_TRANSPORTE: "CLT",
    KEY_PERFIL_SUPERVISOR_LINEA_TRANSPORTE: "LDT",
    KEY_PERFIL_AUXILIAR_VISIBILIDAD: "AXV",
    KEY_PERFIL_SUPERVISOR_VISIBILIDAD: "SPVIS",
    KEY_PERFIL_JEFE_AREA_VISIBILIDAD: "JFVIS",
    KEY_PERFIL_SUPERVISOR_TARIMAS: "SUPT",
    KEY_PERFIL_JEFE_AREA_TARIMAS: "JATA",
    KEY_PERFIL_SUPERVISOR_CALLCENTER_CEMTRA: "SCC",

    simularActiveX: function () {
        var json;

        try {
            jQuery("#txtSimulacionActiveX").attr("type", "text");
            jQuery("#txtSimulacionActiveX").attr("size", "150");
            json = jQuery("#txtSimulacionActiveX").val();
            if (json == "") {
                json = "[{ GetIdCall:'{F085B912-6D4F-49B2-AA12-AACAF7CF2539}', CurrentIntStateByID:'LOCKED' }]";
                jQuery("#txtSimulacionActiveX").val(json);
            }
            json = eval(json);
            return json[0];
        } catch (e) {
            alert("Exception Sistema.simularActiveX:\n" + e);
        }
    },

    cambiarTemaBootstrap: function () {
        try {
            var theme = jQuery("#ddlTemaBootstrap").val();
            jQuery("#ctl00_styleBootstrap").attr("href", "../css/bootstrap-" + theme + ".css");
        } catch (e) {
            alert("Exception: Sistema.deleteFila\n" + e);
        }

    },

    login: function () {
        try {
            var jsonValidarCampos = [
                { elemento_a_validar: "txtUsername", requerido: true, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtPassword", requerido: true, tipo_validacion: "caracteres-prohibidos" }
            ];
            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
                return false;
            } else {
                return true;
            }

        } catch (e) {
            alert("Exception: Sistema.login\n" + e);
            return false;
        }
    },

    alertaMensajeError: function (mensaje) {
        try {
            if (!mensaje) { mensaje = "Verifique los errores del formulario"; }
            alert(mensaje);
        } catch (e) {
            alert("Exception: Sistema.alertaMensajeError\n" + e);
        }
    },

    deleteFila: function (opciones) {
        try {
            var table = document.getElementById(opciones.elem_tabla);
            var txtTotalRegistros = document.getElementById(opciones.elem_total_registros);
            var totalRegistros = parseInt(txtTotalRegistros.value);

            if (confirm("Seguro que quiere eliminar el registro?")) {
                table.deleteRow(opciones.rowIndex);
                totalRegistros--;
                txtTotalRegistros.value = totalRegistros;
            }

        } catch (e) {
            alert("Exception: Sistema.deleteFila\n" + e);
        }
    },

    deleteFilaSinConfirmacion: function (id, indiceFila, objTotalRegistros) {
        try {
            var table = document.getElementById(opciones.elem_tabla);
            var txtTotalRegistros = document.getElementById(opciones.elem_total_registros);
            var totalRegistros = parseInt(txtTotalRegistros.value);

            table.deleteRow(opciones.rowIndex);
            totalRegistros--;
            txtTotalRegistros.value = totalRegistros;
        } catch (e) {
            alert("Exception: Sistema.deleteFilaSinConfirmacion\n" + e);
        }

    },

    urlEncode: function (string) {
        return escape(this._utf8_encode(string));
    },

    urlDecode: function (string) {
        return this._utf8_decode(unescape(string));
    },

    // private method for UTF-8 encoding
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    },

    toUpper: function (s) {
        return s.toUpperCase();
    },

    toLower: function (s) {
        return s.toLowerCase();
    },

    left: function (s, n) {
        if (n > s.length)
            n = s.length;

        return s.substring(0, n);
    },

    right: function (s, n) {
        var t = s.length;
        if (n > t)
            n = t;
        return s.substring(t - n, t);
    },

    inStr: function (n, s1, s2) {
        var numargs = Sistema.inStr.arguments.length;

        if (numargs < 3)
            return n.indexOf(s1) + 1;
        else
            return s1.indexOf(s2, n) + 1;
    },

    getISODateTime: function (d, conFormato) {
        var fecha = "";
        var s = function (p) {
            return ('' + p).length < 2 ? '0' + p : '' + p;
        };

        if (typeof d === 'undefined') {
            d = new Date();
        };

        fecha = d.getFullYear() + '-' +
            s(d.getMonth() + 1) + '-' +
            s(d.getDate()) + ' ' +
            s(d.getHours()) + ':' +
            s(d.getMinutes()) + ':' +
            s(d.getSeconds()) + ':' +
            s(d.getMilliseconds());

        if (!conFormato) {
            fecha = fecha.replace(/-/ig, "");
            fecha = fecha.replace(/ /ig, "");
            fecha = fecha.replace(/:/ig, "");
        }

        return fecha;
    },

    crearIdUnico: function (opciones) {
        var idUnico;

        try {
            var fechaActual = new Date();
            idUnico = Sistema.getISODateTime(fechaActual, false);

            if (opciones) {
                if (opciones.prefijo) {
                    idUnico = (opciones.prefijo != "") ? opciones.prefijo + "_" + idUnico : idUnico;
                }
            }

        } catch (e) {
            id = "";
        }

        return idUnico;
    },

    contains: function (textoContenido, textoBuscar) {
        var contiene = (textoContenido.indexOf(textoBuscar) == -1) ? false : true;
        return contiene
    },

    mostrarFechaActual: function (opciones) {
        try {
            var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            var diasSemana = new Array("Domingo", "Lunes", "Martes", "Mi&eacute;rcoles", "Jueves", "Viernes", "S&aacute;bado");
            var f = new Date();
            var nombreDiaSemana = diasSemana[f.getDay()];
            var dia = f.getDate();
            var mes = meses[f.getMonth()];
            var ano = f.getFullYear();
            var hora = Sistema.right("00" + f.getHours(), 2);
            var minutos = Sistema.right("00" + f.getMinutes(), 2);
            var fecha = nombreDiaSemana + ", " + dia + " " + mes + " " + ano;

            if (opciones.mostrarReloj) { fecha += " " + hora + ":" + minutos + " hrs."; }
            jQuery("#" + opciones.elem_contenedor).html(fecha);
        } catch (e) {
            alert("Exception: Sistema.mostrarFechaActual\n" + e);
        }
    },

    obtenerStyleAlertaPorPrioridad: function (prioridad) {
        var tipoAlerta;
        try {
            switch (Sistema.toLower(prioridad)) {
                case "alta":
                    tipoAlerta = "danger";
                    break;
                case "media":
                    tipoAlerta = "warning";
                    break;
                case "baja":
                    tipoAlerta = "success";
                    break;
                default:
                    tipoAlerta = "success";
                    break;
            }

            return tipoAlerta;
        } catch (e) {
            alert("Exception: Sistema.obtenerTipoAlertaPorPrioridad\n" + e);
        }
    },

    convertirJSONtoString: function (obj) {
        try {
            return JSON.stringify(obj);
        } catch (e) {
            alert("Exception: Sistema.convertirJSONtoString\n" + e);
        }
    },


    obtenerPaginaActual: function () {
        var pathname = jQuery(location).attr("pathname");
        var arrPathname = pathname.split("/");
        var pagina = arrPathname[arrPathname.length - 1];
        return pagina;
    },

    validarLogout: function (opciones) {
        var paginaActual;
        var redireccionarLogout = false;

        try {
            paginaActual = Sistema.obtenerPaginaActual();

            switch (opciones.llavePerfil) {
                case Sistema.KEY_PERFIL_TELEOPERADOR:
                    switch (paginaActual) {
                        case "TO.GestionAlerta.aspx":
                            Alerta.validarLogout_TeleOperador();
                            break;
                        default:
                            redireccionarLogout = true;
                            break;
                    }
                    break;
                default:
                    redireccionarLogout = true;
                    break;
            }

            if (redireccionarLogout) {
                Sistema.accederUrl({ location: "top", url: Sistema.PAGINA_LOGOUT });
            }

        } catch (e) {
            alert("Exception: Sistema.obtenerTipoAlertaPorPrioridad\n" + e);
        }
    },

    redireccionarLogin: function () {
        try {
            Sistema.accederUrl({ location: "top", url: Sistema.PAGINA_LOGOUT });
        } catch (e) {
            alert("Exception: Sistema.redireccionarLogin\n" + e);
        }
    },

    contieneTextoTerminoSesion: function (texto) {
        try {
            if (Sistema.contains(texto, "!DOCTYPE")) {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            alert("Exception: Sistema.contieneTextoTerminoSesion\n" + e);
            return false;
        }
    },

    dateDiff: function (datepart, fromdate, todate) {
        /**
        * datepart: año:'y' | mes:'m' | semana:'w' | dia 'd' | hora:'h' | minutos:'mi' | segundos:'s'
        */
        try {
            datepart = datepart.toLowerCase();
            var diff = todate - fromdate;
            var divideBy = {
                y: 31104000000,
                m: 2592000000,
                w: 604800000,
                d: 86400000,
                h: 3600000,
                mi: 60000,
                s: 1000
            };

            return Math.floor(diff / divideBy[datepart]);
        } catch (e) {
            alert("Exception: Sistema.dateDiff\n" + e);
            return 0;
        }
    },

    cronometro: function (opciones) {
        try {
            var y = opciones.year;
            var m = opciones.month - 1;
            var d = opciones.day;
            var h = opciones.hours;
            var mi = opciones.minutes;
            var s = opciones.seconds;
            var fechaInicio = new Date(y, m, d, h, mi, s, 0);
            var hoy = new Date();

            var segundos = Sistema.dateDiff("s", fechaInicio, hoy);
            jQuery("#" + opciones.elem_cronometro).html(Sistema.convertirSegundosEnHHMMSS(segundos));
        } catch (e) {
            alert("Exception: Sistema.cronometro\n" + e);
        }
    },

    convertirSegundosEnHHMMSS: function (segundos) {
        var minutos = 0;
        var horas = 0;
        var dias = 0;
        var fecha = "";

        try {
            //obtiene los segundos
            var division = parseFloat(segundos) / 60;
            minutos = Math.floor(division);
            segundos = segundos % 60;

            //obtiene minutos
            if (minutos >= 60) {
                horas = Math.floor(parseFloat(minutos) / 60);
                minutos = minutos % 60;
            }

            //obtiene dias
            if (horas >= 24) {
                dias = Math.floor(parseFloat(horas) / 24);
                horas = horas % 24;
            }

            dias = (dias <= 9) ? "0" + dias : dias;
            horas = (horas <= 9) ? "0" + horas : horas;
            minutos = (minutos <= 9) ? "0" + minutos : minutos;
            segundos = (segundos <= 9) ? "0" + segundos : segundos;

        } catch (e) {
            alert("Exception: Sistema.convertirSegundosEnHHMMSS\n" + e);
        }

        fecha = horas + "h : " +
            minutos + "m : " +
            segundos + "s";
        if (dias > 0) { fecha = dias + "d : " + fecha; }
        return fecha;
    },

    moverScrollHastaControl: function (elem_control) {
        var top;

        try {
            if (elem_control == "body") {
                top = jQuery(document).height();
            } else {
                if (jQuery("#" + elem_control).is(":visible")) {
                    top = jQuery("#" + elem_control).offset().top;
                } else {
                    top = 0;
                }
            }
            jQuery("html, body").animate({ scrollTop: top }, 500);

        } catch (e) {
            alert("Exception: Sistema.moverScrollHastaControl\n" + e);
        }
    },

    accederUrl: function (opciones) {
        var url;

        try {
            url = opciones.url;

            switch (opciones.location) {
                case "top":
                    top.location.href = url;
                    break;
                case "self":
                    self.location.href = url;
                    break;
                case "blank":
                    window.open(url, "_blank");
                    break;
                default:
                    top.location.href = url;
                    break;
            }

        } catch (e) {
            alert("Exception: Sistema.accederUrl\n" + e);
        }

    },

    obtenerLabelPrioridad: function (prioridad) {
        var html, template, tipoAlerta;
        try {
            template = "<span class=\"alert alert-{TIPO_ALERTA} sist-padding-label-prioridad sist-margin-cero\"><strong>{PRIORIDAD}</strong></span>";
            tipoAlerta = Sistema.obtenerStyleAlertaPorPrioridad(prioridad);

            template = template.replace("{TIPO_ALERTA}", tipoAlerta);
            template = template.replace("{PRIORIDAD}", prioridad);
            html = template;
        } catch (e) {
            html = prioridad;
        }

        return html;
    },

    destacarFilaTabla: function (opciones) {
        try {
            //quita el estilo destacado de las filas
            jQuery(".destacar-fila").removeClass("sist-fila-destacada");

            //asigna el estilo a la fila consultada
            if (opciones.indice != "-1") { jQuery("#tr" + opciones.indice).addClass("sist-fila-destacada"); }
        } catch (e) {
            alert("Exception: Sistema.destacarFilaTabla\n" + e);
        }

    },

    mensajeCargandoDatos: function (mensaje) {
        var html = "";

        try {
            if (!mensaje) { mensaje = "Cargando datos, espere un momento por favor ..."; }

            html += "<div>";
            html += "<span class=\"help-block\"><em><img src=\"../img/select2-spinner.gif\" alt=\"\" />&nbsp;" + mensaje + "</em></span>";
            html += "</div>";
        } catch (e) {
            alert("Exception: Sistema.mensajeCargandoDatos\n" + e);
        }
        return html;
    },

    obtenerLabelPermisoZona: function (permiso) {
        var html, template, tipoAlerta;
        try {
            if (Sistema.toUpper(permiso) == "REPROGRAMADA") {
                template = "<span class=\"alert alert-{TIPO_ALERTA} sist-padding-label-prioridad sist-margin-cero\"><strong>ALERTA {PERMISO}</strong></span>";
            } else {
                template = "<span class=\"alert alert-{TIPO_ALERTA} sist-padding-label-prioridad sist-margin-cero\"><strong>ZONA {PERMISO}</strong></span>";
            }
            tipoAlerta = (Sistema.toUpper(permiso) == "AUTORIZADA") ? "success" : "danger";

            template = template.replace("{TIPO_ALERTA}", tipoAlerta);
            template = template.replace("{PERMISO}", permiso);
            html = template;
        } catch (e) {
            html = permiso;
        }

        return html;
    },

    startsWith: function (texto, patron) {
        return texto.indexOf(patron) === 0;
    },

    endsWith: function (texto, patron) {
        var d = texto.length - patron.length;
        return d >= 0 && texto.lastIndexOf(patron) === d;
    },

    formatearTelefonoCallCenter: function (telefono) {
        var largo;

        try {
            largo = telefono.length;
            if (largo == 8 && !Sistema.startsWith(telefono, "2")) {
                telefono = "09" + telefono;
            } else {
                telefono = "9" + telefono;
            }
        } catch (e) {
            alert("Exception: Sistema.formatearTelefonoCallCenter\n" + e);
        }
        return telefono;
    },

    mensajeLlamadaPorEstado: function (opciones) {
        var mensaje, estado, telefono;
        try {
            estado = opciones.estado;
            telefono = opciones.telefono;

            switch (Sistema.toUpper(estado)) {
                case "-1":
                    mensaje = "<div class=\"alert alert-info sist-margin-cero sist-padding-5\"><span class=\"glyphicon glyphicon-hand-up\"></span>&nbsp;Iniciando llamada</div>";
                    break;
                case Sistema.CALLCENTER_ESTADO_LLAMADA_OCUPADO:
                    mensaje = "<div class=\"alert alert-danger sist-margin-cero sist-padding-5\"><span class=\"glyphicon glyphicon-time\"></span>&nbsp;INCONCERT est&aacute; ocupado, haga click sobre el n&uacute;mero nuevamente por favor ...</div>";
                    break;
                case Sistema.CALLCENTER_ESTADO_LLAMADA_LOCKED:
                    mensaje = "<div class=\"alert alert-info sist-margin-cero sist-padding-5\"><span class=\"glyphicon glyphicon-phone-alt\"></span>&nbsp;Llamando al " + telefono + " ...</div>";
                    break;
                case Sistema.CALLCENTER_ESTADO_LLAMADA_TAKED:
                    mensaje = "<div class=\"alert alert-success sist-margin-cero sist-padding-5\"><span class=\"glyphicon glyphicon-earphone\"></span>&nbsp;<span class=\"glyphicon glyphicon-user\"></span>&nbsp;Hablando con " + telefono + "</div>";
                    break;
                case Sistema.CALLCENTER_ESTADO_LLAMADA_ENDED:
                case "":
                    mensaje = "<div class=\"alert alert-danger sist-margin-cero sist-padding-5\"><span class=\"glyphicon glyphicon-remove\"></span>&nbsp;Llamada finalizada al " + telefono + "</div>";
                    break;
                default:
                    mensaje = "<div class=\"alert alert-danger sist-margin-cero sist-padding-5\"><span class=\"glyphicon glyphicon-question-sign\"></span>&nbsp;Estado desconocido</span></div>";
                    break;
            }

        } catch (e) {
            alert("Exception Sistema.mensajeLlamadaPorEstado:\n" + e);
            mensaje = "<span class=\"glyphicon glyphicon-question-sign\"></span>&nbsp;Estado desconocido</span>";
        }

        if (Sistema.SIMULAR_ACTIVEX) { mensaje = mensaje + "<div class=\"text-danger\">(Está en modo SimulacionActiveX)</div>"; }
        return "<div class=\"sist-font-size-14 text-center\"><strong><em>" + mensaje + "</em></strong></div>";
    },

    marcarTelefono: function (opciones) {
        var telefono;

        try {
            var jsonValidarCampos = [
                { elemento_a_validar: "txtTelefono", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero", contenedor_mensaje_validacion: "lblMensajeErrorTelefono" }
            ];

            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
            } else {
                telefono = jQuery("#txtTelefono").val();
                telefono = Sistema.formatearTelefonoCallCenter(telefono);

                jQuery("#btnLlamar").removeClass("btn-success");
                jQuery("#btnLlamar").addClass("btn-danger");
                jQuery("#btnLlamar").attr("onclick", "Sistema.finalizarLlamado()");
                jQuery("#btnLlamar").html("<span class=\"glyphicon glyphicon-remove\"></span>&nbsp;Colgar");
                jQuery("#txtTelefono").attr("disabled", true);

                if (!Sistema.SIMULAR_ACTIVEX) {
                    var barAgent = new ActiveXObject("BarAgentControl.BAControl");
                    barAgent.MakeCallDir(Sistema.CALLCENTER_CAMPAIGN, telefono);
                }

                Sistema.monitorearLlamada();
                Sistema.SETINTERVAL_CALLCENTER_LLAMADO = setInterval(function () { Sistema.monitorearLlamada(); }, 1000);
            }

        } catch (e) {
            alert("No es posible realizar la llamada desde un ambiente fuera del CallCentter\n(Error: " + e + ")");
            clearInterval(Sistema.SETINTERVAL_CALLCENTER_LLAMADO);
            Sistema.limpiarControlesLlamada();
        }
    },

    monitorearLlamada: function () {
        var idLlamada, estadoLlamada, barAgent, mensaje, telefono, finalizarLlamadaModoManual, prefijoControl;
        var currentIntStateByID = "-1";

        try {
            idLlamada = jQuery("#txtCallCenterIdLlamada").val();
            estadoLlamada = jQuery("#txtCallCenterEstadoLlamada").val();
            telefono = jQuery("#txtTelefono").val();
            finalizarLlamadaModoManual = jQuery("#txtCallCenterFinalizarLlamadaModoManual").val();

            if (Sistema.SIMULAR_ACTIVEX) {
                barAgent = Sistema.simularActiveX();
            } else {
                barAgent = new ActiveXObject("BarAgentControl.BAControl");
            }

            if (idLlamada == "") {
                if (Sistema.SIMULAR_ACTIVEX) {
                    idLlamada = barAgent.GetIdCall;
                } else {
                    idLlamada = barAgent.GetIdCall();
                }
                jQuery("#txtCallCenterIdLlamada").val(idLlamada);
            } else {
                if (Sistema.SIMULAR_ACTIVEX) {
                    currentIntStateByID = barAgent.CurrentIntStateByID;
                } else {
                    currentIntStateByID = barAgent.CurrentIntStateByID(idLlamada);
                }
            }

            //si se finaliza la llamada usando el boton, entonces asigna estado finalizado a la llamada
            if (finalizarLlamadaModoManual == "1") { currentIntStateByID = Sistema.CALLCENTER_ESTADO_LLAMADA_ENDED; }

            //si la llamada cambia de estado entonces se graba el nuevo estado
            if (idLlamada != "" && currentIntStateByID != estadoLlamada) {
                jQuery("#txtCallCenterEstadoLlamada").val(currentIntStateByID);
            }

            //muestra mensaje segun el estado de la llamada
            mensaje = Sistema.mensajeLlamadaPorEstado({ estado: currentIntStateByID, telefono: telefono });
            jQuery("#lblMensajeLlamada").html(mensaje);
            jQuery("#lblMensajeLlamada").show();

            //si la llamada finalizo cierra popUp y limpia controles
            if (currentIntStateByID == Sistema.CALLCENTER_ESTADO_LLAMADA_ENDED || currentIntStateByID == "") {
                if (!Sistema.SIMULAR_ACTIVEX) {
                    barAgent.WrapUpByID(idLlamada);
                }
                clearInterval(Sistema.SETINTERVAL_CALLCENTER_LLAMADO);
                setTimeout(function () { Sistema.limpiarControlesLlamada(); }, 1000);
            }

        } catch (e) {
            alert("Exception Sistema.monitorearLlamada:\n" + e);
            clearInterval(Sistema.SETINTERVAL_CALLCENTER_LLAMADO);
            Sistema.limpiarControlesLlamada();
        }

    },

    finalizarLlamado: function (opciones) {
        var telefono, idLlamada, currentIntStateByID;

        try {
            idLlamada = jQuery("#txtCallCenterIdLlamada").val();

            if (Sistema.SIMULAR_ACTIVEX) {
                barAgent = Sistema.simularActiveX();
                currentIntStateByID = barAgent.CurrentIntStateByID;
            } else {
                barAgent = new ActiveXObject("BarAgentControl.BAControl");
                currentIntStateByID = barAgent.CurrentIntStateByID(idLlamada);
            }

            if (idLlamada != "") {
                if (!Sistema.SIMULAR_ACTIVEX) {
                    barAgent.HangUp();
                }
                jQuery("#txtCallCenterFinalizarLlamadaModoManual").val("1");
            }

        } catch (e) {
            alert("Exception Sistema.finalizarLlamado:\n" + e);
            clearInterval(Sistema.SETINTERVAL_CALLCENTER_LLAMADO);
            Sistema.limpiarControlesLlamada();
        }
    },

    limpiarControlesLlamada: function () {
        var telefono;

        try {
            jQuery("#txtTelefono").attr("disabled", false);
            jQuery("#txtTelefono").val("");
            jQuery("#txtCallCenterIdLlamada").val("");
            jQuery("#txtCallCenterEstadoLlamada").val("-1");
            jQuery("#txtCallCenterFinalizarLlamadaModoManual").val("0");
            jQuery("#btnLlamar").removeClass("btn-danger");
            jQuery("#btnLlamar").addClass("btn-success");
            jQuery("#btnLlamar").attr("onclick", "Sistema.marcarTelefono()");
            jQuery("#btnLlamar").html("<span class=\"glyphicon glyphicon-phone-alt\"></span>&nbsp;Llamar");
            jQuery("#lblMensajeLlamada").html("");
            jQuery("#lblMensajeLlamada").hide("");
        } catch (e) {
            alert("Exception Sistema.limpiarControlesLlamada:\n" + e);
        }
    },

    mostrarMensajeUsuario: function () {
        var queryString = "";

        try {
            //construye queryString
            queryString += "op=ObtenerMensajeUsuario";

            var okFunc = function (t) {
                var respuesta = t;
                if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                    Sistema.redireccionarLogin();
                } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                    Sistema.dibujarMensajeUsuario("");
                } else {
                    Sistema.dibujarMensajeUsuario(respuesta);
                }
            }
            var errFunc = function (t) {
                Sistema.dibujarMensajeUsuario("");
            }
            jQuery.ajax({
                url: "../webAjax/waSistema.aspx",
                type: "post",
                async: false,
                data: queryString,
                success: okFunc,
                error: errFunc
            });

        } catch (e) {
            Sistema.dibujarMensajeUsuario("");
        }
    },

    dibujarMensajeUsuario: function (mensaje) {
        var html;
        var template = "";
        var templateReiniciar = "";

        try {
            template += "<div class=\"sist-height-mensaje-footer\">&nbsp;</div>";
            template += "<div class=\"navbar-fixed-bottom sist-height-mensaje-footer\">";
            template += "  <div class=\"alert alert-warning sist-padding-5\">";
            template += "    <div class=\"row\">";
            template += "      <div class=\"col-xs-2\">";
            template += "        <div class=\"sist-font-size-24\"><img src=\"../img/warning.png\" alt=\"\" /><strong>Atenci&oacute;n</strong></div>";
            template += "      </div>";
            template += "      <div class=\"col-xs-10\">";
            template += "        {MENSAJE}";
            template += "      </div>";
            template += "    </div>";
            template += "  </div>";
            template += "</div>";

            templateReiniciar += "<div class=\"row\">";
            templateReiniciar += "  <div class=\"col-xs-9\">";
            templateReiniciar += "    Estimado Usuario, el sistema se encuentra actualizado en su &uacute;ltima versi&oacute;n, por lo que solicitamos que <strong>cierre la sesi&oacute;n actual</strong> y vuelva a ingresar para completar los cambios. Si no realiza esto puede provocar alg&uacute;n <strong>error inesperado</strong>.";
            templateReiniciar += "  </div>";
            templateReiniciar += "  <div class=\"col-xs-3\">";
            templateReiniciar += "    <div class=\"btn btn-lg btn-danger\" onclick=\"Sistema.validarLogout({ llavePerfil:'' })\"><span class=\"glyphicon glyphicon-refresh\"></span>&nbsp;Reiniciar sistema</div>";
            templateReiniciar += "  </div>";
            templateReiniciar += "</div>";

            switch (mensaje) {
                case "":
                    html = "";
                    break;
                case "[REINICIAR]":
                    template = template.replace("{MENSAJE}", templateReiniciar);
                    html = template;
                    break;
                default:
                    template = template.replace("{MENSAJE}", mensaje);
                    html = template;
                    break;
            }

        } catch (e) {
            html = "";
        }

        jQuery("#lblFooterMensajeUsuario").html(html);
    },

    dondeEstaCamion: function () {
        var queryString = "";
        var patente, json, htmlSinRegistro;

        try {
            var jsonValidarCampos = [
                { elemento_a_validar: "txtPatente", requerido: true, tipo_validacion: "patente", contenedor_mensaje_validacion: "lblMensajeErrorPatente" }
            ];
            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
            } else {
                elemCanvas = "map-canvas";
                patente = jQuery("#txtPatente").val();

                //construye queryString
                queryString += "op=DondeEstaCamion";
                queryString += "&patente=" + Sistema.urlEncode(patente);

                var okFunc = function (t) {
                    var respuesta = t;
                    if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                        Sistema.redireccionarLogin();
                    } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                        alert("No se pudo obtener la posición del camión");
                    } else {
                        if (json == "") {
                            htmlSinRegistro = Sistema.obtenerHtmlSinMapa();
                            jQuery("#" + elemCanvas).html(htmlSinRegistro);
                        } else {
                            json = jQuery.parseJSON(respuesta);
                            Sistema.verGoogleMaps({ elem_contenedor_mapa: elemCanvas, latTracto: json.LatTracto, lonTracto: json.LonTracto });
                            Sistema.mostrarDatosPosicionCamion({ elem_tabla_datos: "hTablaDatos", json: json });
                        }
                    }
                }
                var errFunc = function (t) {
                    alert("No se pudo obtener la posición del camión");
                }
                jQuery.ajax({
                    url: "../webAjax/waSistema.aspx",
                    type: "post",
                    async: false,
                    data: queryString,
                    success: okFunc,
                    error: errFunc
                });

            }

        } catch (e) {
            alert("Exception Sistema.dondeEstaCamion:\n" + e);
        }
    },

    verGoogleMaps: function (opciones) {
        var htmlSinRegistro, elemContenedorMapa, latTracto, lonTracto;

        try {
            elemContenedorMapa = opciones.elem_contenedor_mapa;
            if (opciones.latTracto) { latTracto = opciones.latTracto; } else { latTracto = ""; }
            if (opciones.lonTracto) { lonTracto = opciones.lonTracto; } else { lonTracto = ""; }

            //muestra el mapa si vienen las coordenadas
            if (latTracto != "" && lonTracto != "") {
                var myLatlng = new google.maps.LatLng(latTracto, lonTracto);
                var mapOptions = {
                    zoom: 16,
                    center: myLatlng
                };
                map = new google.maps.Map(document.getElementById(elemContenedorMapa), mapOptions);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    icon: "../img/camion.png"
                });

            } else {
                htmlSinRegistro = Sistema.obtenerHtmlSinMapa();
                jQuery("#" + elemContenedorMapa).html(htmlSinRegistro);
            }
        } catch (e) {
            alert("Exception Sistema.verEnGoogleMaps:\n" + e);
        }
    },

    obtenerHtmlSinMapa: function () {
        var htmlSinRegistro = "";

        try {
            htmlSinRegistro += "<div class=\"text-center\">";
            htmlSinRegistro += "  <div class=\"sist-padding-top-20\"></div>";
            htmlSinRegistro += "  <div class=\"form-group\"><img src=\"../img/sin_mapa.png\" alt=\"\" /></div>";
            htmlSinRegistro += "</div>";
        } catch (e) {
            alert("Exception Sistema.obtenerHtmlSinMapa:\n" + e);
        }

        return htmlSinRegistro;
    },

    mostrarDatosPosicionCamion: function (opciones) {
        var elemTablaDatos, json, fechaConsultaGPS, fechaRespuestaGPS;
        var template = "";

        try {
            elemTablaDatos = opciones.elem_tabla_datos;
            json = opciones.json;

            if (json.Error) {
                html = "<div class=\"alert alert-warning text-center\"><strong>NO SE ENCONTRARON REGISTROS PARA LA PLACA CONSULTADA</strong></div>";
            } else {
                fechaConsultaGPS = json.Fh_dato.replace(/-/ig, "/");
                fechaRespuestaGPS = json.Fecha_Procesa.replace(/-/ig, "/");

                template += "<table class=\"table\">";
                template += "  <thead>";
                template += "    <tr>";
                template += "      <th>PLACA</th>";
                template += "      <th>LAT TRACTO</th>";
                template += "      <th>LON TRACTO</th>";
                template += "      <th>VELOCIDAD</th>";
                template += "      <th>FECHA ULTIMA CONSULTA GPS</th>";
                template += "      <th>FECHA ULTIMA RESPUESTA GPS</th>";
                template += "    </tr>";
                template += "  </thead>";
                template += "  <tbody>";
                template += "    <tr>";
                template += "      <td>{PATENTE}</td>";
                template += "      <td>{LAT_TRACTO}</td>";
                template += "      <td>{LON_TRACTO}</td>";
                template += "      <td>{VELOCIDAD} km/hr</td>";
                template += "      <td>{FECHA_CONSULTA_GPS} hrs.</td>";
                template += "      <td>{FECHA_RESPUESTA_GPS} hrs.</td>";
                template += "    </tr>";
                template += "  </tbody>";
                template += "</table>";

                template = template.replace("{PATENTE}", json.Patente);
                template = template.replace("{LAT_TRACTO}", json.LatTracto);
                template = template.replace("{LON_TRACTO}", json.LonTracto);
                template = template.replace("{VELOCIDAD}", json.Velocidad);
                template = template.replace("{FECHA_CONSULTA_GPS}", fechaConsultaGPS);
                template = template.replace("{FECHA_RESPUESTA_GPS}", fechaRespuestaGPS);
                html = template;
            }

            jQuery("#" + elemTablaDatos).html(html);
        } catch (e) {
            alert("Exception Sistema.mostrarDatosPosicionCamion:\n" + e);
        }
    },

    roundNumber: function (num, dec) {
        var result;

        try {
            result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
        } catch (e) {
            alert("Exception Sistema.roundNumber:\n" + e);
            result = num;
        }

        return result;
    },

    //----------------------------
    //fuente: http://stackoverflow.com/questions/881510/sorting-json-by-values
    ordenarJSON: function (opciones) {
        var json, atributo, ordenarAscendente;

        try {
            json = opciones.json;
            atributo = opciones.atributoPorOrdenar;
            ordenarAscendente = (Sistema.toUpper(opciones.tipoOrden) == "ASC") ? true : false;

            json = json.sort(function (a, b) {
                if (ordenarAscendente) return (a[atributo] > b[atributo]) ? 1 : ((a[atributo] < b[atributo]) ? -1 : 0);
                else return (b[atributo] > a[atributo]) ? 1 : ((b[atributo] < a[atributo]) ? -1 : 0);
            });

        } catch (e) {
            alert("Exception Sistema.ordenarJSON:\n" + e);
        }

        return json;
    },

    getDate: function (strDate, strTime) {
        strDate = strDate.replace(/[-]/g, "/");
        var day = strDate.split("/")[0];
        var month = strDate.split("/")[1] - 1;
        var year = strDate.split("/")[2];
        var hour = strTime.split(":")[0];
        var seg = strTime.split(":")[1];
        return new Date(year, month, day, hour, seg, 0, 0);
    },

    modalMensajeError: function (opciones) {
        var titulo, mensaje, textoAceptar, tituloDefecto, mensajeDefecto;

        try {
            tituloDefecto = "Error";
            mensajeDefecto = "Verifique los errores del formulario";

            if (opciones) {
                if (opciones.titulo) { titulo = opciones.titulo; } else { titulo = tituloDefecto; }
                if (opciones.mensaje) { mensaje = opciones.mensaje; } else { mensaje = mensajeDefecto; }
            } else {
                titulo = tituloDefecto;
                mensaje = mensajeDefecto;
            }

            textoAceptar = "Aceptar";
            bootbox.dialog({
                closeButton: false,
                title: "<span class=\"sist-font-size-24\"><strong>" + titulo + "</strong></span>",
                message: "<span class=\"sist-font-size-16\">" + mensaje + "</span>",
                buttons: {
                    success: {
                        label: textoAceptar,
                        className: "btn-success btn-lg",
                        callback: function () {
                            //no hace nada
                        }
                    }
                }
            });
        } catch (e) {
            alert("Exception: Sistema.alertaMensajeError\n" + e);
        }
    },

    htmlEncode: function (value) {
        return jQuery("<div/>").text(value).html();
    },

    htmlDecode: function (value) {
        return jQuery("<div/>").html(value).text();
    },

    mostrarMensajeUsuarioHTML: function (opciones) {
        var html = "";
        var template = "<div class=\"alert alert-{TIPO} {DISMISSABLE} {ALINEACION_TEXTO}\">{BOTON_CERRAR}{MENSAJE}</div>";
        var mensaje, tipoMensaje, mostrarBotonCerrar, alineacionTexto, dismissable, botonCerrar;

        try {
            mensaje = opciones.mensaje;
            tipoMensaje = Sistema.toLower(opciones.tipoMensaje);
            mostrarBotonCerrar = opciones.mostrarBotonCerrar;
            alineacionTexto = opciones.alineacionTexto;

            dismissable = (mostrarBotonCerrar) ? "alert-dismissable" : "";
            botonCerrar = (mostrarBotonCerrar) ? "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" : "";

            template = template.replace("{TIPO}", tipoMensaje);
            template = template.replace("{DISMISSABLE}", dismissable);
            template = template.replace("{BOTON_CERRAR}", botonCerrar);
            template = template.replace("{MENSAJE}", mensaje);
            template = template.replace("{ALINEACION_TEXTO}", alineacionTexto);
            html = template;
        } catch (e) {
            alert("Exception Sistema.mostrarMensajeUsuarioHTML:\n" + e);
            html = "";
        }

        return html;
    },

    obtenerUsuarioConectadoJSON: function () {
        var oUsuario, dataJSON;
        var queryString = "";

        try {
            //construye queryString
            queryString += "op=ObtenerUsuarioConectadoJSON";

            var okFunc = function (t) {
                var respuesta = t;
                if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                    Sistema.redireccionarLogin();
                } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                    oUsuario = {};
                } else {
                    oUsuario = jQuery.parseJSON(respuesta);
                }
            }
            var errFunc = function (t) {
                oUsuario = {};
            }
            jQuery.ajax({
                url: "../webAjax/waSistema.aspx",
                type: "post",
                async: false,
                data: queryString,
                success: okFunc,
                error: errFunc
            });

        } catch (e) {
            alert("Exception: Sistema.obtenerUsuarioConectadoJSON\n" + e);
            oUsuario = {};
        }

        return oUsuario;
    },

    cambiarClave: function (opciones) {
        var passwordCaduco, modal;
        var queryString = "";

        try {
            passwordCaduco = opciones.passwordCaduco;
            modal = (passwordCaduco == "1") ? true : false;

            queryString += "?passwordCaduco=" + passwordCaduco;
            jQuery.fancybox({
                width: 500,
                height: 580,
                modal: modal,
                type: "iframe",
                href: "../page/Mantenedor.CambiarClave.aspx" + queryString
            });
        } catch (e) {
            alert("Exception Sistema.cambiarClave:\n" + e);
        }
    },

    validarCambioClave: function () {
        var item, claveActual, nuevaClave, confirmarNuevaClave, valorConfirmarNuevaClave, valorClaveActual, claveEncriptada, claveActualEncriptada;

        try {
            claveActual = jQuery("#txtClaveActual").val();
            claveEncriptada = jQuery("#txtClaveEncriptada").val();
            nuevaClave = jQuery("#txtNuevaClave").val();
            confirmarNuevaClave = jQuery("#txtConfirmarNuevaClave").val();

            var jsonValidarCampos = [
                { elemento_a_validar: "txtClaveActual", requerido: true, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtNuevaClave", requerido: true, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtConfirmarNuevaClave", requerido: true, tipo_validacion: "caracteres-prohibidos" }
            ];

            //valida que ingrese su clave actual
            if (claveActual != "") {
                claveActualEncriptada = Sistema.toUpper(hex_sha1(claveActual + Sistema.CONST_LLAVE_PASSWORD));
                valorClaveActual = (claveActualEncriptada != claveEncriptada) ? "-1" : "1";
                item = {
                    valor_a_validar: valorClaveActual, // -1: No coinciden / 1: Coinciden
                    requerido: false,
                    tipo_validacion: "numero-entero-positivo",
                    contenedor_mensaje_validacion: "hErrorClaveActual",
                    mensaje_validacion: "La clave no coincide"
                };
                jsonValidarCampos.push(item);
            }

            //valida que las claves nuevas sean iguales
            if (nuevaClave != "" && confirmarNuevaClave != "") {
                valorConfirmarNuevaClave = (nuevaClave != confirmarNuevaClave) ? "-1" : "1";
                item = {
                    valor_a_validar: valorConfirmarNuevaClave, // -1: No coinciden / 1: Coinciden
                    requerido: false,
                    tipo_validacion: "numero-entero-positivo",
                    contenedor_mensaje_validacion: "hErrorConfirmarNuevaClave",
                    mensaje_validacion: "La confirmación no coincide con la clave nueva"
                };
                jsonValidarCampos.push(item);
            }

            //valida que la clave nueva sea distinta a la actual
            if (claveActual != "" && nuevaClave != "") {
                valorConfirmarNuevaClave = (claveActual == nuevaClave) ? "-1" : "1";
                item = {
                    valor_a_validar: valorConfirmarNuevaClave, // -1: Clave NO valida / 1: valida
                    requerido: false,
                    tipo_validacion: "numero-entero-positivo",
                    contenedor_mensaje_validacion: "hErrorNuevaClave",
                    mensaje_validacion: "La clave nueva debe ser distinta a la actual"
                };
                jsonValidarCampos.push(item);
            }

            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
                return false;
            } else {
                return true;
            }

        } catch (e) {
            alert("Exception: Sistema.validarCambioClave\n" + e);
            return false;
        }
    },

    cerrarPopUp: function () {
        try {
            parent.jQuery.fancybox.close();
        } catch (e) {
            alert("Exception Sistema.cerrarPopUp:\n" + e);
        }
    }

};
﻿var Ecommerce = {

  CorrelativoViaje: 1,

  close: function (event) {
    if (event) event.preventDefault(); //Cancela el postback
    parent.$.fancybox.close();
  },

  closeWithReload: function () {
    window.parent.location.reload();
    parent.$.fancybox.getInstance().close();
  },

  loadFileExcelViajesEcommerce: function (fileName) {

    $("#hText").html("Obteniendo datos, espere por favor...");

    $.ajax({
      type: "POST",
      url: "../webMethod/wmEcommerce.aspx/LoadFileExcel",
      data: "{fileName: '" + fileName + "' }",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      async: true,
      success: function (response) {

        if (response.d == "") {
          Ecommerce.saveDataTable();
        } else {
          Ecommerce.showMensaje("danger", response.d);
        }
      },
      failure: function () {
        Ecommerce.showMensaje("danger", "Ocurrio un error mientras se cargaban los datos, intente nuevamente...");
      }
    });
  },

  saveDataTable: function () {

    $("#hText").html("Grabando datos, espere por favor...");

    $.ajax({
      type: "POST",
      url: "../webMethod/wmEcommerce.aspx/SaveDataTable",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      async: true,
      success: function (response) {

        if (response.d == "") {

          var mensajeHtml = "La carga se ha realizado de manera satisfactoria.";
          Ecommerce.showMensaje("success", mensajeHtml);

          $("#dvFupload").hide();

        } else {
          Ecommerce.showMensaje("danger", response.d);
        }
      },
      failure: function () {
        Ecommerce.showMensaje("danger", "Ocurrio un error mientras se cargaban los datos, intente nuevamente...");
      }
    });
  },

  showMensaje: function (state, mensaje) {

    if (state === "success") {
      $("#dvMensaje").html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Carga exitosa.</strong>&nbsp;" + mensaje + "</div>");
    } else {
      $("#dvMensaje").html("<div class='alert alert-dismissible alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Ha ocurrido un error.</strong>&nbsp;" + mensaje + "</div>");
    }
  },

  buscarViaje: function (event) {

    if (event) event.preventDefault(); //Cancela el postback

    var fechaDesde = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaDesde").val();
    var fechaHasta = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaHasta").val();

    var jsonValidarCampos = [
           { elemento_a_validar: "ctl00_cphContent_txtFechaDesde", requerido: false, tipo_validacion: "fecha" }
          , { elemento_a_validar: "ctl00_cphContent_txtFechaHasta", requerido: false, tipo_validacion: "fecha" }
          , { valor_a_validar: fechaDesde, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorFechaHasta", mensaje_validacion: "No puede ser menor que Fecha Desde", extras: { fechaInicio: fechaDesde, horaInicio: "00:00", fechaTermino: fechaHasta, horaTermino: "23:59"} }
          , { elemento_a_validar: "ctl00_cphContent_txtPkt", requerido: false, tipo_validacion: "caracteres-prohibidos" }
          , { elemento_a_validar: "ctl00_cphContent_txtUpc", requerido: false, tipo_validacion: "caracteres-prohibidos" }
          , { elemento_a_validar: "ctl00_cphContent_txtZip", requerido: false, tipo_validacion: "caracteres-prohibidos" }
          , { elemento_a_validar: "ctl00_cphContent_txtZona", requerido: false, tipo_validacion: "caracteres-prohibidos" }
          , { elemento_a_validar: "ctl00_cphContent_txtFiltroRegistrosPorPagina", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
      ];

    var esValido = Validacion.validarControles(jsonValidarCampos);
    if (!esValido) {
      //code
    } else {

      Loading.show("Cargando datos, espere por favor...");

      //Ejecuta el postback
      __doPostBack("ctl00$cphContent$btnBuscar", "");
    }

  },

  grabarDetalle: function (event) {
    if (event) event.preventDefault(); //Cancela el postback

    if (confirm("Esta seguro que desea continuar?")) {
      var jsonValidarCampos = [
                         { elemento_a_validar: "ctl00_cphContent_txtFechaProgramacion", requerido: false, tipo_validacion: "fecha" }
                        , { elemento_a_validar: "ctl00_cphContent_txtCantidadPKT", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                        , { elemento_a_validar: "ctl00_cphContent_txtZonaAsignada", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                        , { elemento_a_validar: "ctl00_cphContent_txtPlaca", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                        , { elemento_a_validar: "ctl00_cphContent_txtConductor", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                    ];


      var esValido = Validacion.validarControles(jsonValidarCampos);
      if (!esValido) {
        //Error campos
      } else {
        //Ejecuta el postback
        __doPostBack("ctl00$cphContent$btnGrabar", "");
      }
    }

  },

  habilitarGrabar: function (event) {

    //Obtiene el objeto para asignar correlativo
    var nameCtrl = event.target.id.split("_");
    $("#ctl00_cphContent_gvPrincipal_" + nameCtrl[3] + "_dvSeleccionar").click(false); //bloquea el click
    ///$("#ctl00_cphContent_gvPrincipal_" + nameCtrl[3] + "_lblCorrelativo").html(Ecommerce.CorrelativoViaje);
    $("#ctl00_cphContent_gvPrincipal_" + nameCtrl[3] + "_lblCorrelativo").text(Ecommerce.CorrelativoViaje);
    $("#ctl00_cphContent_gvPrincipal_" + nameCtrl[3] + "_hCorrelativo").val(Ecommerce.CorrelativoViaje);

    Ecommerce.CorrelativoViaje += 1;

    //Habilita el boton asignar
    var selected = false;
    $("#ctl00_cphContent_gvPrincipal :checkbox").each(function () {
      if (this.checked)
        selected = true;
    });

    if (selected) {
      $("#ctl00_cphContent_btnAsignar").removeAttr("disabled");
      $("#ctl00_cphContent_btnLimpiar").removeAttr("disabled");
    }
    else {
      $("#ctl00_cphContent_btnAsignar").attr("disabled", true);
      $("#ctl00_cphContent_btnLimpiar").attr("disabled", true);
    }
  },

  showModal: function (event) {

    if (event) event.preventDefault(); //Cancela el postback

    //limpiar controles
    $("#ctl00_cphContent_txtFechaProgramacion").val("");
    $("#ctl00_cphContent_txtCantidadPKT").val("");
    $("#ctl00_cphContent_txtZonaAsignada").val("");
    $("#ctl00_cphContent_txtPlaca").val("");
    $("#ctl00_cphContent_txtConductor").val("");

    //levanta modal
    $("#mAsignarDatos").modal("show");
  },

//  quitarSeleccion: function (event) {

//    if (event) event.preventDefault(); //Cancela el postback
//    $(".badge").html("0");
//    $("#ctl00_cphContent_gvPrincipal input[type=checkbox]").prop('checked', false);
//  },

  obtenerInfoPatente: function () {

    var patente = $("#ctl00_cphContent_txtPlaca").val();
    if (patente.length >= 6) {

      //Llama al método
      var okFunc = function (response) {

        var oJson = jQuery.parseJSON(response.d);

        //$("#dvInfo").removeClass("alert alert-info");
        $("#dvInfo").show();

        //Señal Gps
        switch (oJson.SeñalGps) {
          case "0":
            $("#ctl00_cphContent_lblSeñalGps").html("<span class='glyphicon glyphicon-ban-circle'></span> NO INTEGRADA</span>");
            break;
          case "1":
            $("#ctl00_cphContent_lblSeñalGps").html("<span class='glyphicon glyphicon-signal'></span> SEÑAL GPS ONLINE</span>");
            break;
          case "2":
            $("#ctl00_cphContent_lblSeñalGps").html("<span class='glyphicon glyphicon-off'></span> NO REPORTANDO SEÑAL GPS</span>");
            break;
        }

        //En Cedis
        switch (oJson.Cedis) {
          case "-1":
            $("#ctl00_cphContent_lblEnCD").html("<span class='glyphicon glyphicon-map-marker'></span> PLACA NO SE ENCUENTRA EN CEDIS</span>");
            break;
          default:
            $("#ctl00_cphContent_lblEnCD").html("<span class='glyphicon glyphicon-map-marker'></span> PLACA SE ENCUENTRA EN CEDIS</span>");
            break;
        }

        //Transportista
        if (oJson.NombreTransportista)
          $("#ctl00_cphContent_lblTransportista").text(oJson.NombreTransportista);

      }

      var errFunc = function (response) {
        alertify.alert("Error", "Ha ocurrido un error al obtener la Señal Gps<br/>Error: " + response.d);
      }


      $.ajax({
        type: "POST",
        url: "../webMethod/wmEcommerce.aspx/ObtenerInfoPatente",
        data: "{patente: '" + patente + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: okFunc,
        failure: errFunc,
        async: false
      });

    } else {
      $("#dvInfo").hide();
    }

  }

}
﻿var Transportista = {
  SETINTERVAL_ESTADO_CONEXION_TELEOPERADOR: 0,
  SETINTERVAL_MILISEGUNDOS_ESTADO_CONEXION_TELEOPERADOR: 5000, // 5000 (5 segundos)
  FORMULARIO_MANTENEDOR_DOCUMENTO: "Mantenedor.Documento",
  FORMULARIO_MANTENEDOR_ASIGNAR_DOCUMENTO: "Mantenedor.Asignar.Documento",
  FORMULARIO_TRANSPORTISTA_RESPUESTA_ALERTAROJA: "Transportista.Respuesta.AlertaRoja",
  FORMULARIO_MANTENEDOR_CONDUCTOR: "Mantenedor.Conductor",
  FORMULARIO_MANTENEDOR_PATENTE: "Transportista.Patente",

  validarFormularioBusqueda: function () {
    try {

      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Transportista.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  validarFormularioBusquedaRespuesta: function () {
    try {

      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNumeroViaje", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Transportista.validarFormularioBusquedaRespuesta\n" + e);
      return false;
    }
  },

  validarFormularioBusquedaDocumento: function () {
    try {

      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Transportista.validarFormularioBusquedaDocumento\n" + e);
      return false;
    }
  },

  validarFormularioAsignarDocumento: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: "msTransportista_txtIdsHija", requerido: true, contenedor_mensaje_validacion: "lblMensajeErrormsTransportista", mensaje_validacion: "Debe seleccionar al menos una Línea de Transporte" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

      return true;
    } catch (e) {
      alert("Exception: Transportista.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  abrirFormulario: function (opciones) {
    try {
      var queryString = "";
      var url = "";
      var id, numeroViaje, rut, idAlertaRojaEstadoActual;

      switch (opciones.formulario) {

        case Transportista.FORMULARIO_MANTENEDOR_DOCUMENTO:
          id = opciones.idDocumento;

          queryString += "?idDocumento=" + id;

          url = "../page/Mantenedor.DocTransportistaDetalle.aspx" + queryString;
          break;

        case Transportista.FORMULARIO_MANTENEDOR_ASIGNAR_DOCUMENTO:
          id = opciones.idDocumento;

          queryString += "?idDocumento=" + id;

          url = "../page/Mantenedor.DocTransportistaAsignar.aspx" + queryString;
          break;

        case Transportista.FORMULARIO_TRANSPORTISTA_RESPUESTA_ALERTAROJA:

          queryString += "?nroTransporte=" + opciones.nroTransporte;
          queryString += "&rut=" + opciones.rut;
          queryString += "&idAlertaRojaEstadoActual=" + opciones.idAlertaRojaEstadoActual;

          url = "../page/Transportista.RespuestaAlertaRojaDetalle.aspx" + queryString;
          break;

        case Transportista.FORMULARIO_MANTENEDOR_CONDUCTOR:
          id = opciones.idConductor;

          queryString += "?id=" + id;

          url = "../page/Transportista.ConductorDetalle.aspx" + queryString
          break;
        case Transportista.FORMULARIO_MANTENEDOR_PATENTE:
          id = opciones.id;

          queryString += "?id=" + id;

          url = "../page/Transportista.PatenteDetalle.aspx" + queryString
          break;
      }

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: url
      });
    } catch (e) {
      alert("Exception Transportista.abrirFormulario:\n" + e);
    }
  },

  validarFormularioDocumento: function () {
    var item, idUsuario, requeridoPassword, llavePerfil;

    try {

      var jsonValidarCampos = [
         { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
         , { elemento_a_validar: "txtDescripcion", requerido: false, tipo_validacion: "caracteres-prohibidos" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception Transportista.validarFormularioDocumento:\n" + e);
      return false;
    }
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.Transportista.cargarDesdePopUpUsuario(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Transportista.cerrarPopUp:\n" + e);
    }
  },

  cargarDesdePopUpUsuario: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception Transportista.cargarDesdePopUpUsuario:\n" + e);
    }
  },

  eliminarDocumento: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarDocumento";
      queryString += "&idDocumento=" + Sistema.urlEncode(opciones.idDocumento);

      if (confirm("Si elimina el documento se eliminarán todas las referencias asociadas a el y no se podrá volver a recuperar.\n¿Desea eliminar el documento?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waTransportista.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Pagina.eliminarDocumento\n" + e);
    }
  },

  expandirContraer: function (opciones) {
    var elem_holderComentario, visible;

    try {
      elem_holderComentario = opciones.elem_holderComentario;
      idAlerta = opciones.idAlerta;

      jQuery("#" + elem_holderComentario).slideToggle(function () {
        visible = jQuery("#" + elem_holderComentario).is(":visible");
        if (visible) {
          jQuery("#" + idAlerta + "_btnComentar").attr("class", "btn btn-danger btn-sm sist-width-100-porciento");
          jQuery("#" + idAlerta + "_btnComentar").html("<span class=\"glyphicon glyphicon-remove\"></span>&nbsp; Cerrar");
        } else {
          jQuery("#" + idAlerta + "_btnComentar").attr("class", "btn btn-success btn-sm sist-width-100-porciento");
          jQuery("#" + idAlerta + "_btnComentar").html("<span class=\"glyphicon glyphicon-eye-open\"></span>&nbsp; Comentar");
        }

      });

    } catch (e) {
      alert("Exception Transportista.expandirContraer:\n" + e);
    }
  },

  validarFormularioAlertaRoja: function () {
    var msgError = "";
    var idAlerta;
    var esValido;

    try {
      jQuery(".id-alerta").each(function (indice, obj) {
        idAlerta = jQuery(this).val();

        var jsonValidarCampos = [
	        { elemento_a_validar: idAlerta + "_txtObservacion", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        ];

        var esValidoAux = Validacion.validarControles(jsonValidarCampos);

        if (!esValidoAux) {
          esValido = false;
        } else {
          esValido = true;
        }

      });

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        Transportista.obtenerJSONAlertasRojas();
        return true;
      }

    } catch (e) {
      alert("Exception Transportista.validarFormularioAlertaRoja:\n" + e);
      return false;
    }

  },

  obtenerJSONAlertasRojas: function () {
    var json = [];
    var item = {};

    try {
      jQuery(".id-alerta").each(function (indice, obj) {
        idAlerta = jQuery(this).val();
        observacion = jQuery("#" + idAlerta + "_txtObservacion").val();

        item = {
          IdAlerta: idAlerta,
          Observacion: observacion
        };
        json.push(item);
      });
    } catch (e) {
      alert("Exception Transportista.obtenerJSONAlertasRojas:\n" + e);
      json = [];
    }

    jQuery("#txtJSON").val(Sistema.convertirJSONtoString(json));
  },

  eliminarRegistro: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarPagina";
      queryString += "&idConductor=" + Sistema.urlEncode(opciones.idConductor);

      if (confirm("¿Está seguro de eliminar el registro?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waTransportista.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Transportista.eliminarRegistro\n" + e);
    }
  },

  validarEliminar: function () {
    try {
      if (confirm("Si elimina la página se eliminarán todas las referencias asociadas a ellas y no se podrá recuperar\n¿Desea eliminar la página?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: Transportista.validarEliminar\n" + e);
      return false;
    }
  },

  validarFormularioFichaConductor: function () {
    var item, idUsuario, requeridoPassword, llavePerfil, reqNacionalidadOtra;

    try {
      idUsuario = jQuery("txtIdConductor").val();
      var existeRut = Transportista.existeRegistroDuplicado({ tipo: "Rut" });
      var nacionalidad = jQuery("#ddlFiltroNacionalidad_ddlCombo option:selected").val();

      if (nacionalidad == "Otra") {
        reqNacionalidadOtra = true;
      }
      else {
        reqNacionalidadOtra = false;
      }


      var jsonValidarCampos = [
          { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtPaterno", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtMaterno", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtRUT", requerido: true, tipo_validacion: "rut" }
        , { elemento_a_validar: "txtFechaNacimiento", requerido: true, tipo_validacion: "fecha" }
        , { elemento_a_validar: "txtMail", requerido: false, tipo_validacion: "email" }
        , { elemento_a_validar: "txtTelefono", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtTelefono2", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtDireccion", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "ddlFiltroEstadoCivil_ddlCombo", requerido: false }
        , { elemento_a_validar: "ddlFiltroSexo_ddlCombo", requerido: true }
        , { elemento_a_validar: "ddlFiltroNacionalidad_ddlCombo", requerido: true }
        , { elemento_a_validar: "ddlRegion", requerido: true }
        , { elemento_a_validar: "ddlComuna", requerido: true }
        , { elemento_a_validar: "txtFechaIngreso", requerido: true, tipo_validacion: "fecha" }
        , { elemento_a_validar: "ddlNivelEstudio_ddlCombo", requerido: true }
        , { elemento_a_validar: "txtNacionalidadOtra", requerido: reqNacionalidadOtra, tipo_validacion: "caracteres-prohibidos" }
      ];

      //valida que el rut sea unico
      if (jQuery("#txtRUT").val() != "") {
        item = {
          valor_a_validar: existeRut,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorRUT",
          mensaje_validacion: "El RUT ya existe"
        };
        jsonValidarCampos.push(item);
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        //Transportista.obtenerJSONDatosPerfil();
        return true;
      }

    } catch (e) {
      alert("Exception Transportista.validarFormularioFichaConductor:\n" + e);
      return false;
    }
  },

  cerrarPopUpFichaConductor: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.Transportista.cargarDesdePopUpFichaConductor(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Transportista.cerrarPopUpFichaConductor:\n" + e);
    }
  },

  cargarDesdePopUpFichaConductor: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception Transportista.cargarDesdePopUpFichaConductor:\n" + e);
    }
  },

  existeRegistroDuplicado: function (opciones) {
    var queryString = "";
    var existe = "-1";
    var opcion, valor;

    try {
      switch (opciones.tipo) {
        case "Rut":
          opcion = "Conductor.Rut";
          valor = jQuery("#txtRUT").val();
          break;
      }
      var idConductor = jQuery("#txtIdConductor").val();

      queryString += "op=" + opcion;
      queryString += "&id=" + idConductor;
      queryString += "&valor=" + Sistema.urlEncode(valor);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el RUT único: " + opciones.tipo);
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el RUT único: " + opciones.tipo);
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Transportista.existeRegistroDuplicado:\n" + e);
    }

    return existe;
  },

  mostrarNacionalidadOtro: function () {

    var nacionalidad = jQuery("#ddlFiltroNacionalidad_ddlCombo option:selected").val();

    try {

      if (nacionalidad == "Otra") {
        jQuery("#divOtra").show();
      }
      else {
        jQuery("#divOtra").hide();
      }

    } catch (e) {
      alert("Exception Transportista.mostrarNacionalidadOtro:\n" + e);
    }

  },

  cargarComunas: function () {
    var id = jQuery("#ddlRegion").select2("val");
    var combosRelacionados = [
                                {
                                  elem_contenedor: "ddlComuna",
                                  tipoCombo: 'Comuna',
                                  multiple: false,
                                  placeholder: "",
                                  item_cabecera: "Seleccione",
                                  valores_seleccionados: ['-1'],
                                  fuenteDatos: "Tabla",
                                  data: [],
                                  filtro: id
                                }
                             ];

    jQuery.each(combosRelacionados, function (indice, obj) {
      Combo.dibujarHTML(obj);
    });

    //Limpiar la descripcion
    Alerta.mostrarClasificacionTipoObservacion();
  },

  cargarComboRegionComuna: function (opciones) {
    var ddlRegion = opciones.ddlRegion.toString();
    var ddlComuna = opciones.ddlComuna.toString();
    var idRegion = opciones.idRegion.toString();
    var idComuna = opciones.idComuna.toString();

    try {
      var combosRelacionados = [
                                {
                                  elem_contenedor: ddlRegion,
                                  tipoCombo: "Region",
                                  multiple: false,
                                  placeholder: "",
                                  item_cabecera: "Seleccione",
                                  valores_seleccionados: [idRegion],
                                  onChange: "",
                                  fuenteDatos: "Tabla",
                                  data: [],
                                  filtro: "-1",
                                  combos_relacionados: [
                                                          {
                                                            elem_contenedor: ddlComuna,
                                                            tipoCombo: 'Comuna',
                                                            multiple: false,
                                                            placeholder: "",
                                                            item_cabecera: "Seleccione",
                                                            valores_seleccionados: [idComuna],
                                                            fuenteDatos: "Tabla",
                                                            data: [],
                                                            filtro: idRegion
                                                          }
                                                       ]
                                }
                             ];

      jQuery.each(combosRelacionados, function (indice, obj) {
        Combo.dibujarHTML(obj);
      });

    } catch (e) {
      alert("Exception: Transportista.cargarComboRegionComuna\n" + e);
    }
  },

  validarFormularioEncuesta: function () {
    var prefijoControl;
    var jsonPatente;
    var json = "";

    jsonPatente = [];

    try {

      if (confirm("¿Está seguro que desea Grabar?")) {

        //arma el json de patentes
        jQuery(".id-patente").each(function (indice, obj) {

          //dejo fuera la primera fila 
          if (jQuery(obj).val() != "{VALOR_PATENTE}") {

            prefijoControl = jQuery(obj).attr("id").replace("_txtPatente", "");

            item = {
              patente: jQuery(obj).val(),
              cantPuerta: jQuery("#" + prefijoControl + "_txtCantPuerta").val(),
              tipoPuerta: jQuery("#" + prefijoControl + "_txtTipoPuerta").val()
            };
            jsonPatente.push(item);

          }

        });

        json = Sistema.convertirJSONtoString(jsonPatente);
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hJson").val(json);

        return true;
      }

      //var jsonValidarCampos = [
      //      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtPatenteRampla", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      //, { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtCantPuertas", requerido: true, tipo_validacion: "numero-entero-positivo" }
      //];
      //var esValido = Validacion.validarControles(jsonValidarCampos);              

      var esValido = true;
      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Transportista.validarFormularioEncuesta\n" + e);
      return false;
    }
  },

  agregarPatente: function (opciones) {
    var objFila, templateFila, patente, nombrePerfil, jsonPerfiles, elemTabla, valor;
    var txtPatente1 = Sistema.PREFIJO_CONTROL + "txtPatente1"
    var txtPatente2 = Sistema.PREFIJO_CONTROL + "txtPatente2"
    var txtPatente3 = Sistema.PREFIJO_CONTROL + "txtPatente3"
    var hPatente = Sistema.PREFIJO_CONTROL + "hPatente"

    try {
      patente = jQuery("#" + txtPatente1).val() + "-" + jQuery("#" + txtPatente2).val() + "-" + jQuery("#" + txtPatente3).val();
      valor = patente.replace("-", "");
      valor = valor.replace("-", "");

      objFila = jQuery("#tbPatente tbody tr:eq(0)").clone();
      objFila.removeClass("sist-display-none");
      templateFila = objFila.wrap("<div />").parent().html(); // obtiene el html del clone

      jQuery("#" + hPatente).val(valor);

      var jsonValidarCampos = [
            { elemento_a_validar: hPatente, requerido: true, tipo_validacion: "patente", contenedor_mensaje_validacion: "divErrorPatente" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        templateFila = templateFila.replace(/{PREFIJO_CONTROL}/ig, valor.toUpperCase());
        templateFila = templateFila.replace(/{VALOR}/ig, valor.toUpperCase());
        templateFila = templateFila.replace(/{TEXTO}/ig, patente.toUpperCase());
        jQuery("#tbPatente").append(templateFila);

        jQuery("#" + txtPatente1).val("");
        jQuery("#" + txtPatente2).val("");
        jQuery("#" + txtPatente3).val("");
      }

    } catch (e) {
      alert("Exception: Formato.agregarPatente\n" + e);
    }
  },

  mostrarDiv: function (opciones) {
    var div = opciones.div;

    try {

      if (div == "patentes") {
        jQuery("#dvProveedores").hide();
        jQuery("#dvPatentes").show();
      }
      else {
        jQuery("#dvProveedores").show();
        jQuery("#dvPatentes").hide();
      }

    } catch (e) {
      alert("Exception Transportista.mostrarDiv:\n" + e);
    }
  },

  eliminarPatente: function (opciones) {
    var patente;

    try {
      patente = opciones.patente;

      if (confirm("¿Está seguro de eliminar la placa?")) {
        //elimina la fila de la tabla
        jQuery("#" + patente + "_tr").remove();
      }

    } catch (e) {
      alert("Exception: Transportista.eliminarPatente\n" + e);
    }

  },

  ValidarPatente: function () {
    var jsonPatente;
    var json = "";

    jsonPatente = [];

    try {

      if (confirm("¿Está seguro que desea Grabar?")) {

        //arma el json de patentes
        jQuery(".id-patente").each(function (indice, obj) {

          //dejo fuera la primera fila 
          if (jQuery(obj).val() != "{VALOR}") {
            item = {
              patente: jQuery(obj).val()
            };
            jsonPatente.push(item);
          }

        });

        json = Sistema.convertirJSONtoString(jsonPatente);
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hJson").val(json);

        return true;
      }

    } catch (e) {
      alert("Exception: Transportista.ValidarPatente\n" + e);

      return false;
    }
  },

  validarFormularioDetalle: function () {
    var item, idUsuario, requeridoPassword, llavePerfil, reqNacionalidadOtra;

    try {

      var jsonValidarCampos = [
            { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtRutEmpresa", requerido: true, tipo_validacion: "rut" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNombreEmpresa", requerido: true, tipo_validacion: "caracteres-prohibidos" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtVolumen", requerido: true, tipo_validacion: "caracteres-prohibidos" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtVolumen", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNombreDueño", requerido: true, tipo_validacion: "caracteres-prohibidos" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTelefono1Dueño", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTelefono2Dueño", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtEmailDueño", requerido: true, tipo_validacion: "email" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNombreGerente", requerido: true, tipo_validacion: "caracteres-prohibidos" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTelefonoGerente", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtEmailGerente", requerido: true, tipo_validacion: "email" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTelefono1Escalamiento", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTelefono2Escalamiento", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTelefono3Escalamiento", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
          , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtTelefono4Escalamiento", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }
    } catch (e) {
      alert("Exception Transportista.validarFormularioDetalle:\n" + e);
      return false;
    }
  },

  validarFormularioPatente: function () {
    var item, idUsuario, requeridoPassword, llavePerfil, patente;

    patente = jQuery("#txtPlacaPatente1").val() + "" + jQuery("#txtPlacaPatente2").val() + "" + jQuery("#txtPlacaPatente3").val();
    jQuery("#hPatente").val(patente);

    try {

      var jsonValidarCampos = [
         { elemento_a_validar: "hPatente", requerido: true, tipo_validacion: "patente", contenedor_mensaje_validacion: "divErrorPatente" }
         , { elemento_a_validar: "txtMarca", requerido: true, tipo_validacion: "caracteres-prohibidos" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception Transportista.validarFormularioPatente:\n" + e);
      return false;
    }
  },

  eliminarPatente: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarPatente";
      queryString += "&id=" + Sistema.urlEncode(opciones.id);

      if (confirm("¿Está seguro de eliminar el registro?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waTransportista.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Transportista.eliminarRegistro\n" + e);
    }
  }

};
﻿var idUsuarioGlobal = 0;
var Usuario = {
    SETINTERVAL_ESTADO_CONEXION_TELEOPERADOR: 0,
    SETINTERVAL_MILISEGUNDOS_ESTADO_CONEXION_TELEOPERADOR: 30000, // 5000 (30 segundos) le puse un cero mas
    //SETINTERVAL_MILISEGUNDOS_ESTADO_CONEXION_TELEOPERADOR: 60000, // 5000 (30 segundos) le puse un cero mas
    SETINTERVAL_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR: 0,
    SETINTERVAL_MILISEGUNDOS_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR: 31000, // 5000 (31 segundos) le puse un cero mas
    //SETINTERVAL_MILISEGUNDOS_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR: 61000, // 5000 (31 segundos) le puse un cero mas
    FORMULARIO_DETALLE_USUARIO: "Mantenedor.UsuarioDetalle",
    FORMULARIO_TELEFONO_POR_USUARIO: "Mantenedor.TelefonoPorUsuario",

    validarFormularioBusqueda: function (opciones) {
        try {

            var jsonValidarCampos = [
                { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroUsername", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
            ];
            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
                return false;
            } else {
                return true;
            }

        } catch (e) {
            alert("Exception: Usuario.validarFormularioBusqueda\n" + e);
            return false;
        }
    },

    abrirFormulario: function (opciones) {
        try {
            var queryString = "";
            var idUsuario = opciones.idUsuario;

            idUsuarioGlobal = idUsuario;

            queryString += "?id=" + idUsuario;

            jQuery.fancybox({
                width: "100%",
                height: "100%",
                modal: false,
                type: "iframe",
                href: "../page/Mantenedor.UsuarioDetalle.aspx" + queryString
            });
        } catch (e) {
            alert("Exception Usuario.abrirFormulario:\n" + e);
        }
    },

    validarPermisos: function () {
        var eliminarTodosPermisos = "0";
        var dibujarMatriz = true;
        var idPerfil, idUsuario, llavePerfil;

        try {
            if (jQuery("#txtEliminarAsignaciones").val() == "0" && Usuario.tienePermisosAsignados()) {
                if (confirm("Tiene permisos asignados y si cambia de perfil se desmarcarán todos.\n¿Desea continuar?")) {
                    eliminarTodosPermisos = "1";
                    dibujarMatriz = true;
                    jQuery("#txtEliminarAsignaciones").val("1");
                }
                else {
                    dibujarMatriz = false;
                    jQuery("#ddlPerfil").val(jQuery("#txtIdPerfilActual").val());
                }
            }
            else {
                eliminarTodosPermisos = jQuery("#txtEliminarAsignaciones").val();
                dibujarMatriz = true;
            }

            if (dibujarMatriz) {
                Usuario.obtenerLlavePerfil("ddlPerfil", "txtLlavePerfil");
                idPerfil = jQuery("#ddlPerfil").val();
                idUsuario = jQuery("#txtIdUsuario").val();
                llavePerfil = jQuery("#txtLlavePerfil").val();

                Usuario.dibujarMatrizPermiso("hMatrizPermiso", idPerfil, idUsuario, eliminarTodosPermisos, llavePerfil);
                Usuario.mostrarControlesPerfil({ limpiarControles: true });
            }

        } catch (e) {
            alert("Exception Usuario.validarPermisos:\n" + e);
        }

    },

    tienePermisosAsignados: function () {
        var txtTotalCategorias = document.getElementById("txtTotalCategorias");
        var txtTotalPaginas;
        var txtTotalFuncionesAsignadas;
        var totalAsignaciones = 0;
        var tienePermisos = false;

        try {
            //recorre las categorias
            if (txtTotalCategorias) {
                for (var i = 1; i <= txtTotalCategorias.value; i++) {
                    txtTotalPaginas = document.getElementById("Cat" + i + "_txtTotalPaginas");
                    //recorre las paginas de las categorias
                    if (txtTotalPaginas) {
                        for (var j = 1; j <= txtTotalPaginas.value; j++) {
                            txtTotalFuncionesAsignadas = document.getElementById("Cat" + i + "_Pag" + j + "_txtTotalFuncionesAsignadas");
                            //suma todas las funciones asignadas
                            if (txtTotalFuncionesAsignadas) totalAsignaciones += txtTotalFuncionesAsignadas.value;
                        }
                    }
                }
            }
            //si tiene funciones asignadas retorna verdadero
            if (totalAsignaciones > 0) tienePermisos = true;
        } catch (e) {
            alert("Exception Usuario.tienePermisosAsignados:\n" + e);
        }

        return tienePermisos;
    },

    obtenerLlavePerfil: function (elemOrigen, elemDestino) {
        var oOrigen = document.getElementById(elemOrigen);
        var oDestino = document.getElementById(elemDestino);
        var queryString = "";

        try {
            if (oOrigen && oDestino) {
                if (Validacion.tipoVacio(oOrigen.value)) {
                    oDestino.value = "";
                } else {
                    queryString += "op=ObtenerLlavePerfil";
                    queryString += "&idp=" + oOrigen.value;

                    var okFunc = function (t) {
                        var respuesta = t;
                        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                            Sistema.redireccionarLogin();
                        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                            alert("Ha ocurrido un error interno y no se pudo obtener la llave del perfil.");
                            oDestino.value = "";
                        } else {
                            oDestino.value = respuesta;
                        }
                    }
                    var errFunc = function (t) {
                        alert("Ha ocurrido un error interno y no se pudo obtener la llave del perfil.");
                        oDestino.value = "";
                    }
                    jQuery.ajax({
                        url: "../webAjax/waPerfil.aspx",
                        type: "post",
                        async: false,
                        data: queryString,
                        success: okFunc,
                        error: errFunc
                    });
                }
            }
        } catch (e) {
            alert("Exception Usuario.obtenerLlavePerfil:\n" + e);
        }
    },

    dibujarMatrizPermiso: function (elHolerMatriz, idPerfil, idUsuario, eliminarTodosPermisos, llavePerfil) {
        var hMatrizPermiso = document.getElementById(elHolerMatriz);
        var queryString = "";

        try {
            //setea algunos valores
            eliminarTodosPermisos = (eliminarTodosPermisos == "") ? "0" : eliminarTodosPermisos;

            //construye queryString
            queryString += "idp=" + Sistema.urlEncode(idPerfil);
            queryString += "&idu=" + Sistema.urlEncode(idUsuario);
            queryString += "&eli=" + Sistema.urlEncode(eliminarTodosPermisos);

            if (idPerfil != "" && llavePerfil != Sistema.KEY_PERFIL_ADMINISTRADOR) {
                hMatrizPermiso.innerHTML = "<span class=\"help-block\">obtiendo datos, espere un momento...</span>";

                var okFunc = function (t) {
                    var respuesta = t;
                    if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                        Sistema.redireccionarLogin();
                    } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                        hMatrizPermiso.innerHTML = "<span class=\"text-danger\">Ha ocurrido un error interno y no se pudo obtener la matriz de permisos</span>";
                    } else {
                        hMatrizPermiso.innerHTML = respuesta;
                    }
                }
                var errFunc = function (t) {
                    hMatrizPermiso.innerHTML = "<span class=\"text-danger\">Ha ocurrido un error interno y no se pudo obtener la matriz de permisos</span>";
                }
                jQuery.ajax({
                    url: "../webAjax/waObtenerMatrizPermiso.aspx",
                    type: "post",
                    async: false,
                    data: queryString,
                    success: okFunc,
                    error: errFunc
                });
            } else {
                hMatrizPermiso.innerHTML = "";
            }

        } catch (e) {
            alert("Exception Usuario.dibujarMatrizPermiso:\n" + e);
        }
    },

    validarFormularioUsuario: function () {
        var item, idUsuario, requeridoPassword, llavePerfil;

        try {
            idUsuario = jQuery("txtIdUsuario").val();

            idUsuarioGlobal = idUsuario;

            requeridoPassword = (idUsuario == "-1") ? true : false;
            var existeUsername = Usuario.existeRegistroDuplicado({ tipo: "Username" });
            var existeRut = Usuario.existeRegistroDuplicado({ tipo: "Rut" });

            var jsonValidarCampos = [
                { elemento_a_validar: "txtUsername", requerido: true, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtPassword", requerido: requeridoPassword, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtPaterno", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtMaterno", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtRUT", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "txtEmail", requerido: false, tipo_validacion: "email" }
                , { elemento_a_validar: "txtTelefono", requerido: false, tipo_validacion: "caracteres-prohibidos" }
                , { elemento_a_validar: "ddlPerfil", requerido: true }
            ];

            //valida que la clave y repertir clave sean iguales
            var validarRepetirPassword = (jQuery("#txtPassword").val() != jQuery("#txtRepetirPassword").val()) ? -1 : 1; // -1: No coincide | 1: coincide
            item = {
                valor_a_validar: validarRepetirPassword,
                requerido: false,
                tipo_validacion: "numero-entero-positivo",
                contenedor_mensaje_validacion: "hErrorRepetirPassword",
                mensaje_validacion: "La clave no coincide"
            };
            jsonValidarCampos.push(item);

            //valida que el username sea unico
            if (jQuery("#txtUsername").val() != "") {
                item = {
                    valor_a_validar: existeUsername,
                    requerido: false,
                    tipo_validacion: "numero-entero-positivo",
                    contenedor_mensaje_validacion: "hErrorUsername",
                    mensaje_validacion: "El usuario ya existe"
                };
                jsonValidarCampos.push(item);
            }

            //valida que el rut sea unico
            if (jQuery("#txtRUT").val() != "") {
                item = {
                    valor_a_validar: existeRut,
                    requerido: false,
                    tipo_validacion: "numero-entero-positivo",
                    contenedor_mensaje_validacion: "hErrorRUT",
                    mensaje_validacion: "El RUT ya existe"
                };
                jsonValidarCampos.push(item);
            }

            //-----------------------------------------
            //obtiene las validaciones segun el perfil seleccionado
            llavePerfil = jQuery("#txtLlavePerfil").val();

            switch (llavePerfil) {
                case Sistema.KEY_PERFIL_TRANSPORTISTA:
                    jsonValidarCampos = Usuario.validarControlesEmailConCopia(jsonValidarCampos);
                    break;
                case Sistema.KEY_PERFIL_COORDINADOR_LINEA_TRANSPORTE:
                    jsonValidarCampos = Usuario.validarControlesCoordinadorLineaTransporte(jsonValidarCampos);
                    break;
            }
            //-----------------------------------------

            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
                return false;
            } else {
                Usuario.obtenerJSONDatosPerfil();
                return true;
            }

        } catch (e) {
            alert("Exception Usuario.validarFormularioUsuario:\n" + e);
            return false;
        }
    },

    cerrarPopUpUsuario: function (valorCargarDesdePopUp) {
        try {
            if (valorCargarDesdePopUp == "1") {
                parent.Usuario.cargarDesdePopUpUsuario(valorCargarDesdePopUp);
                parent.document.forms[0].submit();
            }
            parent.jQuery.fancybox.close();

        } catch (e) {
            alert("Exception Usuario.cerrarPopUpUsuario:\n" + e);
        }
    },

    cargarDesdePopUpUsuario: function (valor) {
        try {
            var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
            txtCargaDesdePopUp.value = valor;
        } catch (e) {
            alert("Exception Usuario.cargarDesdePopUpUsuario:\n" + e);
        }
    },

    existeRegistroDuplicado: function (opciones) {
        var queryString = "";
        var existe = "-1";
        var opcion, valor;

        try {
            switch (opciones.tipo) {
                case "Username":
                    opcion = "Usuario.Username";
                    valor = jQuery("#txtUsername").val();
                    break;
                case "Rut":
                    opcion = "Usuario.Rut";
                    valor = jQuery("#txtRUT").val();
                    break;
            }
            var idUsuario = jQuery("#txtIdUsuario").val();

            queryString += "op=" + opcion;
            queryString += "&id=" + idUsuario;
            queryString += "&valor=" + Sistema.urlEncode(valor);

            var okFunc = function (t) {
                var respuesta = jQuery.trim(t);
                if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                    Sistema.redireccionarLogin();
                } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                    alert("Ha ocurrido un error interno y no se pudo validar el nombre único: " + opciones.tipo);
                } else {
                    existe = respuesta;
                }
            }

            var errFunc = function (t) {
                alert("Ha ocurrido un error interno y no se pudo validar el nombre único: " + opciones.tipo);
            }

            jQuery.ajax({
                url: "../webAjax/waComprobarDatoDuplicado.aspx",
                type: "post",
                async: false,
                data: queryString,
                success: okFunc,
                error: errFunc
            });
        } catch (e) {
            alert("Exception Usuario.existeRegistroDuplicado:\n" + e);
        }

        return existe;
    },

    mostrarControlesPerfil: function (opciones) {
        var llavePerfil, limpiarControles;

        try {
            Usuario.ocultarControlesPerfil(opciones);
            llavePerfil = jQuery("#txtLlavePerfil").val();
            limpiarControles = opciones.limpiarControles;

            //obtiene la key para saber que controles mostrar
            switch (llavePerfil) {
                case Sistema.KEY_PERFIL_TRANSPORTISTA:
                    Usuario.visibleControlesEmailConCopia({ limpiarControles: limpiarControles });
                    Usuario.visibleControlesNotificarPorEmailHorarioNocturno({ limpiarControles: limpiarControles });
                    break;
                case Sistema.KEY_PERFIL_COORDINADOR_LINEA_TRANSPORTE:
                    Usuario.visibleControlesCoordinadorLineaTransporte({ limpiarControles: limpiarControles });
                    Usuario.visibleControlesCentroDistribucion({ limpiarControles: limpiarControles, textoLabel: "Asociar" });
                    break;
                case Sistema.KEY_PERFIL_TELEOPERADOR:
                    Usuario.visibleControlesTeleoperador({ limpiarControles: limpiarControles });
                    Usuario.visibleControlesCentroDistribucion({ limpiarControles: limpiarControles, textoLabel: "Gestionar" });
                    break;
                case Sistema.KEY_PERFIL_SUPERVISOR:
                    Usuario.visibleControlesCentroDistribucion({ limpiarControles: limpiarControles, textoLabel: "Gestionar" });
                    break;
                case Sistema.KEY_PERFIL_COMMANAGER:
                case Sistema.KEY_PERFIL_JEFE_AREA_TRANSPORTES:
                case Sistema.KEY_PERFIL_GERENCIA_TRANSPORTES:
                case Sistema.KEY_PERFIL_JEFE_AREA_PROTECCION_ACTIVOS:
                case Sistema.KEY_PERFIL_SUBGERENTE_TRANSPORTES:
                case Sistema.KEY_PERFIL_SUBGERENTE_PROTECCION_ACTIVOS:
                case Sistema.KEY_PERFIL_SUPERVISOR_CONTROL_FLOTA_CEDIS:
                case Sistema.KEY_PERFIL_SUPERVISOR_LINEA_TRANSPORTE:
                case Sistema.KEY_PERFIL_SUPERVISOR_PROTECCION_ACTIVOS:
                case Sistema.KEY_PERFIL_AUXILIAR_VISIBILIDAD:
                case Sistema.KEY_PERFIL_SUPERVISOR_VISIBILIDAD:
                case Sistema.KEY_PERFIL_JEFE_AREA_VISIBILIDAD:
                case Sistema.KEY_PERFIL_SUPERVISOR_TARIMAS:
                case Sistema.KEY_PERFIL_JEFE_AREA_TARIMAS:
                case Sistema.KEY_PERFIL_SUPERVISOR_CALLCENTER_CEMTRA:
                case "AV":
                    Usuario.visibleControlesCentroDistribucion({ limpiarControles: limpiarControles, textoLabel: "Asociar" });
                    break;
                case Sistema.KEY_PERFIL_GERENTE_DISTRITAL_TIENDA:
                case Sistema.KEY_PERFIL_GERENTE_TIENDA:
                case Sistema.KEY_PERFIL_LIDER_RECIBO_TIENDA:
                    Usuario.visibleControlesLocales({ limpiarControles: limpiarControles });
                    break;
                default:
                    setTimeout(function () { Usuario.ocultarControlesPerfil({ limpiarControles: true }); }, 100);
                    break;
            }

        } catch (e) {
            alert("Exception Usuario.mostrarControlesPerfil:\n" + e);
        }
    },

    ocultarControlesPerfil: function (opciones) {
        var limpiarControles;

        try {
            limpiarControles = opciones.limpiarControles;
            if (limpiarControles) {
                jQuery("#chkNotificarPorEmailHorarioNocturno").attr("checked", false);
                jQuery("#txtEmailConCopia").val("");
                jQuery("#ddlTransportistaAsociado_ddlCombo").select2("val", "");
                jQuery("#rbtnTeleoperadorTodasLasAlertas").attr("checked", true);
                jQuery("#rbtnTeleoperadorSoloCemtra").attr("checked", false);
                jQuery("#rbtnTeleoperadorExcluirCemtra").attr("checked", false);
                jQuery("#ddlTeleoperadorTransportistasAsociados").select2("val", "");
                jQuery("#ddlCentroDistribucionAsociados").select2("val", "");
                jQuery("#ddlLocalesAsociados").select2("val", "");
            }

            jQuery("div[data-visible=control-perfil]").hide();
            jQuery("#txtJSONDatosPerfil").val("[]");
        } catch (e) {
            alert("Exception Usuario.ocultarControlesPerfil:\n" + e);
        }
    },

    obtenerJSONDatosPerfil: function () {
        var llavePerfil;
        var json = [];
        var item, emailConCopia, notificarPorEmailHorarioNocturno, idTransportistaAsociado, gestionarSoloCemtra, teleoperadorTransportistasAsociados;
        var centroDistribucionAsociados, localesAsociados;

        try {
            llavePerfil = jQuery("#txtLlavePerfil").val();
            emailConCopia = jQuery("#txtEmailConCopia").val();
            notificarPorEmailHorarioNocturno = (jQuery("#chkNotificarPorEmailHorarioNocturno").is(":checked")) ? "1" : "0";
            idTransportistaAsociado = jQuery("#ddlTransportistaAsociado_ddlCombo").select2("val");
            teleoperadorTransportistasAsociados = jQuery("#ddlTeleoperadorTransportistasAsociados").select2("val");
            centroDistribucionAsociados = jQuery("#ddlCentroDistribucionAsociados").select2("val");
            localesAsociados = jQuery("#ddlLocalesAsociados").select2("val");

            //formatea valores
            teleoperadorTransportistasAsociados = teleoperadorTransportistasAsociados.join();
            centroDistribucionAsociados = centroDistribucionAsociados.join();
            localesAsociados = localesAsociados.join();

            if (jQuery("#rbtnTeleoperadorSoloCemtra").is(":checked")) {
                gestionarSoloCemtra = "SoloCemtra";
            } else if (jQuery("#rbtnTeleoperadorExcluirCemtra").is(":checked")) {
                gestionarSoloCemtra = "ExcluirCemtra";
            } else {
                gestionarSoloCemtra = "Todas";
            }

            console.log("+++");
            console.log(centroDistribucionAsociados);
            console.log("+++");


            //construye json
            item = {
                emailConCopia: emailConCopia,
                notificarPorEmailHorarioNocturno: notificarPorEmailHorarioNocturno,
                idTransportistaAsociado: idTransportistaAsociado,
                gestionarSoloCemtra: gestionarSoloCemtra,
                teleoperadorTransportistasAsociados: teleoperadorTransportistasAsociados,
                centroDistribucionAsociados: centroDistribucionAsociados,
                localesAsociados: localesAsociados
            };
            json.push(item);

            json = Sistema.convertirJSONtoString(json);
            jQuery("#txtJSONDatosPerfil").val(json);
        } catch (e) {
            alert("Exception Usuario.obtenerJSONDatosPerfil:\n" + e);
        }
    },

    visibleControlesEmailConCopia: function (opciones) {
        try {
            //muestra controles
            jQuery("#hDatosEspecialPorPerfil").show();
            jQuery("#hEmailConCopia").show();

            //limpia controles
            if (opciones.limpiarControles) {
                jQuery("#txtEmailConCopia").val("");
            }
        } catch (e) {
            alert("Exception Usuario.visibleControlesEmailConCopia:\n" + e);
        }

    },

    validarControlesEmailConCopia: function (jsonValidarCampos) {
        var item;

        try {
            item = { elemento_a_validar: "txtEmailConCopia", requerido: false, tipo_validacion: "email-listado" };
            jsonValidarCampos.push(item);

        } catch (e) {
            alert("Exception Usuario.validarControlesEmailConCopia:\n" + e);
        }

        return jsonValidarCampos;
    },

    obtenerEstadoConexionTeleoperador: function () {
        var queryString = "";

        try {
            //construye queryString
            queryString += "op=ObtenerEstadoConexionTeleoperador";
            
            var okFunc = function (t) {
                var respuesta = t;
                if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                    Sistema.redireccionarLogin();
                } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                    alert("Ha ocurrido un error interno y no se pudo obtener el estado de conexión de los teleoperadores");
                } else {
                    jQuery("#txtJSONDatos").val(respuesta);

                    //console.log("tal ves es esto 1: " + respuesta);

                }
            }
            var errFunc = function (t) {
                alert("Ha ocurrido un error interno y no se pudo obtener el estado de conexión de los teleoperadores");
            }
            jQuery.ajax({
                url: "../webAjax/waSistema.aspx",
                type: "post",
                async: false,
                data: queryString,
                success: okFunc,
                error: errFunc
            });

        } catch (e) {
            alert("Exception Usuario.obtenerEstadoConexionTeleoperador:\n" + e);
        }
    },

    obtenerEstadoConexionTeleoperadorCemtra: function (idUsuarioGlobal) {

        console.log("obtenerEstadoConexionTeleoperadorCemtra " );

        var queryString = "";
        var codCentroDistribucion = $("#ctl00_cphContent_ddlCentroDistribucion_ddlCombo").val();

        try {
            
            //construye queryString
            queryString += "op=ObtenerEstadoConexionTeleoperadorCemtra";
            queryString += "&codCentroDistribucion=" + codCentroDistribucion;

            queryString += "&idUsuarioGlobal=" + idUsuarioGlobal;

            var okFunc = function (t) {
                var respuesta = t;

                //console.log("inicio espera");
                //setTimeout(function () { console.log("esperando"); }, 60000);
                //console.log("fin espera");

                if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                    Sistema.redireccionarLogin();
                } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                    alert("Ha ocurrido un error interno y no se pudo obtener el estado de conexión de los teleoperadores");
                } else {
                    

                    jQuery("#txtJSONDatos").val(respuesta);

                    //console.log("tal ves es esto 2: " + respuesta);
                }
            }
            var errFunc = function (t) {
                alert("Ha ocurrido un error interno y no se pudo obtener el estado de conexión de los teleoperadores");
            }

            jQuery.ajax({
                url: "../webAjax/waSistema.aspx",
                type: "post",
                async: false,
                data: queryString,
                success: okFunc,
                error: errFunc,
                timeout: 60000 // sets timeout to 3 seconds
            });

        } catch (e) {
            alert("Exception Usuario.obtenerEstadoConexionTeleoperador:\n" + e);
        }
    },

    obtenerReportepulsoDiario: function (FecI, fecF, Turno, TO) {
        var queryString = "";

        try {
            //construye queryString
            queryString += "op=obtenerReportepulsoDiario&FecI=" + FecI + "&FecF=" + fecF + "&Turno=" + Turno + "&To=" + TO;

            var okFunc = function (t) {
                var respuesta = t;
                if (Sistema.contieneTextoTerminoSesion(respuesta)) {
                    Sistema.redireccionarLogin();
                } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
                    alert("Ha ocurrido un error interno y no se pudo obtener el estado de conexión de los teleoperadores");
                } else {
                    if (respuesta != "") {
                        json = respuesta;
                        json = jQuery.parseJSON(json);
                        Grafico.bar({ data: json.datosGraficoTorta, elem_contenedor: "hGrafico" });
                    } else {
                        jQuery("#hGrafico").html("");
                    }
                }
            }
            var errFunc = function (t) {
                alert("Ha ocurrido un error interno y no se pudo obtener el estado de conexión de los teleoperadores");
            }
            jQuery.ajax({
                url: "../webAjax/waSistema.aspx",
                type: "post",
                async: false,
                data: queryString,
                success: okFunc,
                error: errFunc
            });

        } catch (e) {
            alert("Exception Usuario.obtenerEstadoConexionTeleoperador:\n" + e);
        }
    },

    dibujarReportepulsoDiarior: function () {
        try {
            var json = jQuery("#txtJSONDatos").val();
            json = jQuery.parseJSON(json);
            Grafico.torta({ data: json.datosGraficoTorta, elem_contenedor: "hGrafico" });
        } catch (e) {
            alert("Exception Usuario.dibujarReportepulsoDiarior:\n" + e);
        }
    },


    dibujarTablaEstadoConexionTeleoperador: function (opciones) {
        
        var totalGlobalAlertas = 0;
        var jsonListadoUsuarios, jsonTotalAlertas, jsonTotalAlertasAtendidas, totalRegistros, templateAux, nombreUsuario, username, estado, ultimaAccion, ultimaFechaAccion;
        var segundosUltimaAccionFecha, fechaUltimoInicioSesion, segundosFechaUltimoInicioSesion, classTR, icono, jsonContactabilidad, centroDistribucion;
        //var totalAlertasRecibidas, totalAlertasGestionadas, porcentajeContactabilidad;
        var template = "";
        var html = "";
        
        try {
            jQuery("#tableDatos tbody").empty();
            json = jQuery("#txtJSONDatos").val();

            //console.log("json: " + JSON.stringify(json));
            
            json = jQuery.parseJSON(json);

            //console.log("json: " + JSON.stringify(json));

            if (json) {
            
                jsonListadoUsuarios = json.listadoUsuarios;

                //console.log("jsonListadoUsuarios: " + JSON.stringify(jsonListadoUsuarios));

                jsonTotalAlertas = json.listadoTotalAlertas;
                jsonTotalAlertasAtendidas = json.listadoTotalAlertasAtendidas
                
                try {
                    totalRegistros = jsonListadoUsuarios.length;
                } catch (err) {
                    throw "error en jsonListadoUsuarios.length";
                }
                
            } else {
                totalRegistros = 0;
            }
            
            template += "<tr class=\"{CLASS_TR}\">";
            template += "  <td><img src=\"../img/{ICONO}\" alt=\"\">&nbsp;{NOMBRE_USUARIO}</td>";
            template += "  <td>{ESTADO}</td>";
            template += "  <td>{ULTIMA_ACCION}</td>";
            template += "  <td>{ULTIMA_ACCION_FECHA}</td>";
            template += "  <td>{TIEMPO_CONEXION_ULTIMA_ACCION}</td>";
            template += "  <td>{FECHA_ULTIMO_INICIO_SESION}</td>";
            template += "  <td>{TIEMPO_CONEXION_INICIO_SESION}</td>";

            if (opciones.cemtra) {
                template += "  <td>{CENTRO_DISTRIBUCION}</td>";
            }
            
            template += "</tr>";

            if (totalRegistros == 0) {
                
                mensaje = Sistema.mostrarMensajeUsuarioHTML({
                    mensaje: "No hay usuarios conectados",
                    tipoMensaje: "warning",
                    mostrarBotonCerrar: false,
                    alineacionTexto: "text-center"
                });
                jQuery("#hTablaSinDatos").html(mensaje);
                jQuery("#hTablaSinDatos").show();
                
            } else {
                
                jQuery("#hTablaSinDatos").html("");
                jQuery("#hTablaSinDatos").hide();

                jQuery.each(jsonListadoUsuarios, function (indice, obj) {
                    templateAux = template;
                    nombreUsuario = obj.NombreUsuario;
                    username = obj.Username;
                    estado = obj.Estado;
                    ultimaAccion = obj.UltimaAccion;
                    ultimaFechaAccion = obj.UltimaFechaAccion;
                    segundosUltimaAccionFecha = obj.SegundosUltimaAccionFecha;
                    fechaUltimoInicioSesion = obj.FechaUltimoInicioSesion;
                    segundosFechaUltimoInicioSesion = obj.SegundosFechaUltimoInicioSesion;

                    if (opciones.cemtra) {
                        centroDistribucion = obj.CD;

                        

                        if (centroDistribucion == undefined || centroDistribucion == '') {
                            centroDistribucion = 'Todos';
                        }
                        else {
                            $("#mCentroDistribucionTexto").html(centroDistribucion.replace(/\|/g, '</br>'));

                            var arrayCentroDistribucion = centroDistribucion.split('|');

                            try {
                                centroDistribucion = arrayCentroDistribucion[0] + '&nbsp;&nbsp;<a href="#"><span class="badge badge-default" data-toggle="modal" data-target="#mCentroDistribucion" title="Clic para ver detalle" >&nbsp;+ ' + arrayCentroDistribucion.length + '&nbsp;</span></a>';
                            } catch (err) {
                                throw "error en arrayCentroDistribucion.length";
                            }

                            

                        }
                    }
                    
                    //formatea valores
                    classTR = "";
                    segundosUltimaAccionFecha = (segundosUltimaAccionFecha == -1) ? "" : "<strong>" + Sistema.convertirSegundosEnHHMMSS(segundosUltimaAccionFecha) + "</strong>";
                    segundosFechaUltimoInicioSesion = (segundosFechaUltimoInicioSesion == -1) ? "" : Sistema.convertirSegundosEnHHMMSS(segundosFechaUltimoInicioSesion);

                    switch (estado) {
                        case "DESCONECTADO":
                            icono = "off.png";
                            segundosUltimaAccionFecha = "";
                            segundosFechaUltimoInicioSesion = "";
                            classTR = "sist-inactivo";
                            break;
                        case "SIN GESTION":
                            icono = "off.png";
                            classTR = "sist-inactivo";
                            break;
                        case "LLAMANDO":
                            icono = "llamando.png";
                            estado = "<div class=\"alert alert-info sist-margin-cero sist-padding-5\"><span class=\"glyphicon glyphicon-phone-alt\"></span>&nbsp;" + estado + "</div>";
                            break;
                        case "HABLANDO":
                            icono = "hablando.png";
                            estado = "<div class=\"alert alert-success sist-margin-cero sist-padding-5\"><span class=\"glyphicon glyphicon-earphone\"></span>&nbsp;<span class=\"glyphicon glyphicon-user\"></span>&nbsp;" + estado + "</div>";
                            break;
                        case "AUXILIAR":
                            icono = "clock.png";
                            estado = "<div class=\"sist-auxiliar\"><span class=\"glyphicon glyphicon-time\"></span>&nbsp;" + estado + "</div>";
                            ultimaAccion = "<div class=\"sist-auxiliar\">" + ultimaAccion + "</div>";
                            break;
                        case "DISPONIBLE":
                            icono = "on.png";
                            break;
                        default:
                            icono = "off.png";
                            break;
                    }
                    
                    //reemplaza marcas
                    templateAux = templateAux.replace("{CLASS_TR}", classTR);
                    templateAux = templateAux.replace("{ICONO}", icono);
                    templateAux = templateAux.replace("{NOMBRE_USUARIO}", nombreUsuario);
                    templateAux = templateAux.replace("{USERNAME}", username);
                    templateAux = templateAux.replace("{ESTADO}", estado);
                    templateAux = templateAux.replace("{ULTIMA_ACCION}", ultimaAccion);
                    templateAux = templateAux.replace("{ULTIMA_ACCION_FECHA}", ultimaFechaAccion);
                    templateAux = templateAux.replace("{TIEMPO_CONEXION_ULTIMA_ACCION}", segundosUltimaAccionFecha);
                    templateAux = templateAux.replace("{FECHA_ULTIMO_INICIO_SESION}", fechaUltimoInicioSesion);
                    templateAux = templateAux.replace("{TIEMPO_CONEXION_INICIO_SESION}", segundosFechaUltimoInicioSesion);

                    if (opciones.cemtra) {
                        templateAux = templateAux.replace("{CENTRO_DISTRIBUCION}", centroDistribucion);


                        //console.log("---");
                        //console.log("centroDistribucion --> " + centroDistribucion);
                        //console.log("---");
                    }

                    //html += templateAux;
                    jQuery("#tableDatos tbody").append(templateAux);
                });
            }

            
            //Dibujar Tabla Total Alertas
            //-----------------------------------------------------------------------------------------------

            try {
                var algo = jsonTotalAlertas.length;
            } catch (err) {
                throw "error en jsonTotalAlertas.length";
            }

            if (jsonTotalAlertas.length == 0) {
                
                mensaje = Sistema.mostrarMensajeUsuarioHTML({
                    mensaje: "No hay registro de alertas",
                    tipoMensaje: "warning",
                    mostrarBotonCerrar: false,
                    alineacionTexto: "text-center"
                });
                jQuery("#hTablaSinDatosTotalAlertas").html(mensaje);
                jQuery("#hTablaSinDatosTotalAlertas").show();
                
            } else {
                
                jQuery("#hTablaSinDatosTotalAlertas").html("");
                jQuery("#hTablaSinDatosTotalAlertas").hide();

                jQuery("#tableTotalAlertas tbody").empty();

                var totalGeneralAlertasAtendidas = 0;
                var totalGeneralAlertasPendientesAtender = 0;
                var total = 0;

                jQuery.each(jsonTotalAlertas, function (indice, obj) {

                    //reemplaza marcas
                    templateAux = "<tr>";
                    templateAux += "  <td>" + obj.NombreAlerta + "</td>";
                    templateAux += "  <td>" + obj.CantAlertasAtendidas + "</td>";
                    templateAux += "  <td>" + obj.CantPorAtender + "</td>";
                    templateAux += "  <td>" + obj.CantidadTotal + "</td>";
                    templateAux += "</tr>";

                    jQuery("#tableTotalAlertas tbody").append(templateAux);

                    totalGeneralAlertasAtendidas += obj.CantAlertasAtendidas;
                    totalGeneralAlertasPendientesAtender += obj.CantPorAtender;
                    total += obj.CantidadTotal;
                });

                templateAux = "<tr>";
                templateAux += "  <td><center><b>Total General</b><center></td>";
                templateAux += "  <td><b>" + totalGeneralAlertasAtendidas + "</b></td>";
                templateAux += "  <td><b>" + totalGeneralAlertasPendientesAtender + "</b></td>";
                templateAux += "  <td><b>" + total + "</b></td>";
                templateAux += "</tr>";

                jQuery("#tableTotalAlertas tbody").append(templateAux);
                
                totalGlobalAlertas = total;
                $("#lblTotalAlertasRecibidas").html(total);
            }

            //Dibujar Tabla Total Alertas Atendidas
            //-----------------------------------------------------------------------------------------------

            try {
                var algo = jsonTotalAlertasAtendidas.length;
            } catch (err) {
                throw "error en jsonTotalAlertasAtendidas.length";
            }

            if (jsonTotalAlertasAtendidas.length == 0) {
                
                mensaje = Sistema.mostrarMensajeUsuarioHTML({
                    mensaje: "No hay registro de alertas atendidas",
                    tipoMensaje: "warning",
                    mostrarBotonCerrar: false,
                    alineacionTexto: "text-center"
                });
                jQuery("#hTablaSinDatosTotalAlertasAtendidas").html(mensaje);
                jQuery("#hTablaSinDatosTotalAlertasAtendidas").show();
                
            } else {
                
                jQuery("#hTablaSinDatosTotalAlertasAtendidas").html("");
                jQuery("#hTablaSinDatosTotalAlertasAtendidas").hide();

                jQuery("#tableTotalAlertasAtendidas tbody").empty();

                var totalGeneral = 0;

                jQuery.each(jsonTotalAlertasAtendidas, function (indice, obj) {

                    //reemplaza marcas
                    templateAux = "<tr>";
                    templateAux += "  <td>" + obj.Usuario + "</td>";
                    templateAux += "  <td>" + obj.Cantidad + "</td>";
                    templateAux += "</tr>";

                    jQuery("#tableTotalAlertasAtendidas tbody").append(templateAux);

                    totalGeneral += obj.Cantidad;
                });

                templateAux = "<tr>";
                templateAux += "  <td><center><b>Total General</b><center></td>";
                templateAux += "  <td><b>" + totalGeneral + "</b></td>";
                templateAux += "</tr>";

                jQuery("#tableTotalAlertasAtendidas tbody").append(templateAux);
                

                $("#lblTotalAlertasAtendidas").html(totalGeneral);

                var resultadoNivelContactabilidad = 0;
                //try {
                    resultadoNivelContactabilidad = ((totalGeneral / totalGlobalAlertas) * 100).toFixed(2);

                    if (totalGeneral == 0 || totalGlobalAlertas == 0 )
                        resultadoNivelContactabilidad = 0;
                    
                //} catch (err) {
                //    resultadoNivelContactabilidad = 0;
                //}
                
                //$("#lblNivelContactabilidad").html(((totalGeneral / totalGlobalAlertas) * 100).toFixed(2));
                $("#lblNivelContactabilidad").html(resultadoNivelContactabilidad);
            }
            
            //dibuja grafico 'Alertas Pendientes por Gestionar'
            //-----------------------------------------------------------------------------------------------
            Grafico.torta({ data: json.datosGraficoTorta, elem_contenedor: "hGrafico" });

            //dibuja grafico 'Alertas Atendidas'
            //-----------------------------------------------------------------------------------------------
            Grafico.torta({ data: json.datosGraficoTorta2, elem_contenedor: "hGrafico2" });
            
        } catch (e) {
            
            alert("Exception Usuario.dibujarTablaEstadoConexionTeleoperador:\n" + e);
        }
    },

    visibleControlesNotificarPorEmailHorarioNocturno: function (opciones) {
        try {
            //muestra controles
            jQuery("#hDatosEspecialPorPerfil").show();
            jQuery("#hNotificarPorEmailHorarioNocturno").show();

            //limpia controles
            if (opciones.limpiarControles) {
                jQuery("#chkNotificarPorEmailHorarioNocturno").attr("checked", false);
            }
        } catch (e) {
            alert("Exception Usuario.visibleControlesNotificarPorEmailHorarioNocturno:\n" + e);
        }

    },

    abrirFormularioTelefonoPorUsuario: function (opciones) {
        try {
            var queryString = "";
            var idTelefonoPorUsuario = opciones.idTelefonoPorUsuario;
            var indiceJSON = opciones.indiceJSON;

            queryString += "?idTelefonoPorUsuario=" + idTelefonoPorUsuario;
            queryString += "&indiceJSON=" + indiceJSON;

            jQuery.fancybox({
                width: "60%",
                height: "70%",
                modal: false,
                type: "iframe",
                helpers: {
                    overlay: {
                        css: {
                            background: Sistema.FANCYBOX_OVERLAY_COLOR_SUBNIVEL
                        }
                    }
                },
                href: "../page/Mantenedor.TelefonoPorUsuario.aspx" + queryString
            });
        } catch (e) {
            alert("Exception Usuario.abrirFormularioTelefonoPorUsuario:\n" + e);
        }
    },

    cargarDatosTelefonoPorUsuario: function (opciones) {
        var json, idTelefonoPorUsuario, indiceJSON, telefono, horaInicio, minutosInicio, horaTermino, minutosTermino, item;

        try {
            idTelefonoPorUsuario = opciones.idTelefonoPorUsuario;
            indiceJSON = opciones.indiceJSON;

            if (indiceJSON == "-1") {
                telefono = "";
                horaInicio = "";
                minutosInicio = "";
                horaTermino = "";
                minutosTermino = "";
            } else {
                json = jQuery("#txtJSONTelefonosPorUsuario", window.parent.document).val();
                json = jQuery.parseJSON(json);

                item = json[indiceJSON];
                telefono = item.Telefono;
                horaInicio = item.HoraInicio;
                horaTermino = item.HoraTermino;

                if (Validacion.tipoVacio(horaInicio)) {
                    minutosInicio = "";
                } else {
                    minutosInicio = Sistema.right(horaInicio, 2);
                    horaInicio = Sistema.left(horaInicio, 2);
                }

                if (Validacion.tipoVacio(horaTermino)) {
                    minutosTermino = "";
                } else {
                    minutosTermino = Sistema.right(horaTermino, 2);
                    horaTermino = Sistema.left(horaTermino, 2);
                }
            }

            //asigna valores
            jQuery("#txtTelefono").val(telefono);
            jQuery("#wucHoraInicio_ddlHora").val(horaInicio);
            jQuery("#wucHoraInicio_ddlMinutos").val(minutosInicio);
            jQuery("#wucHoraTermino_ddlHora").val(horaTermino);
            jQuery("#wucHoraTermino_ddlMinutos").val(minutosTermino);

        } catch (e) {
            alert("Exception Usuario.cargarDatosTelefonoPorUsuario:\n" + e);
        }

    },

    cerrarPopUp: function () {
        try {
            parent.jQuery.fancybox.close();

        } catch (e) {
            alert("Exception Usuario.cerrarPopUp:\n" + e);
        }

    },

    validarFormularioTelefonoPorUsuario: function (opciones) {
        var item, fechaDefecto, horaInicio, minutosInicio, horaTermino, minutosTermino, horaInicioJSON, horaTerminoJSON, indiceJSON;
        var existeHorario, estaOcupado;

        try {
            fechaDefecto = "01/01/2015"; //esta fecha se usa solamente para validar los horarios
            indiceJSON = opciones.indiceJSON;
            horaInicio = jQuery("#wucHoraInicio_ddlHora").val();
            minutosInicio = jQuery("#wucHoraInicio_ddlMinutos").val();
            horaTermino = jQuery("#wucHoraTermino_ddlHora").val();
            minutosTermino = jQuery("#wucHoraTermino_ddlMinutos").val();

            horaInicio = horaInicio + ":" + minutosInicio;
            horaTermino = horaTermino + ":" + minutosTermino;

            var jsonValidarCampos = [
                { elemento_a_validar: "txtTelefono", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
                , { elemento_a_validar: "wucHoraInicio_ddlHora", requerido: true }
                , { elemento_a_validar: "wucHoraInicio_ddlMinutos", requerido: true }
                , { elemento_a_validar: "wucHoraTermino_ddlHora", requerido: true }
                , { elemento_a_validar: "wucHoraTermino_ddlMinutos", requerido: true }
                , { valor_a_validar: fechaDefecto, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: "lblMensajeErrorHoraTermino", mensaje_validacion: "No puede ser menor que Hora Inicio", extras: { fechaInicio: fechaDefecto, horaInicio: horaInicio, fechaTermino: fechaDefecto, horaTermino: horaTermino } }
            ];

            //valida que los horarios no se solapen
            json = jQuery("#txtJSONTelefonosPorUsuario", window.parent.document).val();
            json = jQuery.parseJSON(json);
            horaInicio = horaInicio.replace(":", "");
            horaTermino = horaTermino.replace(":", "");

            //recorre los horarios para hacer las comparaciones
            jQuery.each(json, function (indice, obj) {
                if (indiceJSON != indice) {
                    horaInicioJSON = obj.HoraInicio;
                    horaTerminoJSON = obj.HoraTermino;

                    //1ro. compara que el horario seleccionado no existe en el listado JSON
                    if ((horaInicio >= horaInicioJSON && horaInicio <= horaTerminoJSON) || (horaTermino >= horaInicioJSON && horaTermino <= horaTerminoJSON)) {
                        existeHorario = "-1";
                        estaOcupado = true;

                    } else {
                        //2do. La catolica XD !! (by VSR, 30/04/2015)
                        //3ro. compara que el horario existente en el json no este entre el horario seleccionado
                        if ((horaInicioJSON >= horaInicio && horaInicioJSON <= horaTermino) || (horaTerminoJSON >= horaInicio && horaTerminoJSON <= horaTermino)) {
                            existeHorario = "-1";
                            estaOcupado = true;

                        } else {
                            existeHorario = "1";
                            estaOcupado = false;
                        }

                    } // <-- if () 1ro. compara que el horario seleccionado no existe en el listado JSON

                    //asigna objeto al validador
                    item = {
                        valor_a_validar: existeHorario, // si es igual a [-1 => el horario existe | 1 = el horario no existe]
                        requerido: false,
                        tipo_validacion: "numero-entero-positivo",
                        contenedor_mensaje_validacion: "lblMensajeErrorHoraInicio",
                        mensaje_validacion: "El rango de hora est&aacute; ocupado"
                    };
                    jsonValidarCampos.push(item);

                    //si encuentra un horario ocupado termina el ciclo del each
                    if (estaOcupado) { return false; }

                }
            });

            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
                return false;
            } else {
                return true;
            }

        } catch (e) {
            alert("Exception Usuario.validarFormularioTelefonoPorUsuario:\n" + e);
            return false;
        }
    },

    grabarTelefonoPorPersona: function (opciones) {
        var jsonTelefonoPorUsuario, jsonTelefono, indiceJSON, item;

        try {
            indiceJSON = opciones.indiceJSON;
            jsonTelefono = opciones.jsonTelefono;

            //obtiene json de los telefonos del usuario
            jsonTelefonoPorUsuario = jQuery("#txtJSONTelefonosPorUsuario", window.parent.document).val();
            jsonTelefonoPorUsuario = jQuery.parseJSON(jsonTelefonoPorUsuario);

            //si es nuevo entonces lo agrega al json, sino lo actualiza
            if (indiceJSON == "-1") {
                jsonTelefonoPorUsuario.push(jsonTelefono);
            } else {
                item = jsonTelefonoPorUsuario[indiceJSON];
                item.Telefono = jsonTelefono.Telefono;
                item.HoraInicio = jsonTelefono.HoraInicio;
                item.HoraTermino = jsonTelefono.HoraTermino;
            }

            //ordena json segun HoraInicio
            jsonTelefonoPorUsuario = Sistema.ordenarJSON({ json: jsonTelefonoPorUsuario, atributoPorOrdenar: "HoraInicio", tipoOrden: "Asc" });

            //asigna json actualizado al control input
            jQuery("#txtJSONTelefonosPorUsuario", window.parent.document).val(Sistema.convertirJSONtoString(jsonTelefonoPorUsuario));

            //dibuja listado de telefonos
            parent.Usuario.dibujarTelefonosPorUsuario();

            //cierra la ventana
            Usuario.cerrarPopUp();
        } catch (e) {
            alert("Exception Usuario.grabarTelefonoPorPersona:\n" + e);
        }

    },

    dibujarTelefonosPorUsuario: function () {
        var html = "";
        var template = "";
        var templateBotonAgregar = "";
        var json, idTelefonoPorUsuario, telefono, horaInicio, minutosInicio, horaTermino, minutosTermino, templateAux, jsonPermisosTelefonoAdicional;
        var onclickAbrirFormulario = "";

        try {
            jsonPermisosTelefonoAdicional = jQuery("#txtJSONPermisosTelefonoAdicional").val();
            jsonPermisosTelefonoAdicional = jQuery.parseJSON(jsonPermisosTelefonoAdicional);

            template += "<li class=\"sist-font-size-12\">";
            if (jsonPermisosTelefonoAdicional.EliminarTelefonoAdicional) {
                template += "  <div class=\"col-sm-3\">";
                template += "		 <button type=\"button\" class=\"btn btn-default\" onclick=\"Usuario.eliminarTelefonoPorUsuario({ indiceJSON: '{INDICE_JSON}', invocadoDesde: Usuario.FORMULARIO_DETALLE_USUARIO })\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
                template += "	 </div>";
            }

            if (jsonPermisosTelefonoAdicional.ModificarTelefonoAdicional) {
                onclickAbrirFormulario = "onclick=\"Usuario.abrirFormularioTelefonoPorUsuario({ idTelefonoPorUsuario: '{ID_TELEFONO_POR_USUARIO}', indiceJSON: '{INDICE_JSON}' })\"";
            }

            template += "	 <div class=\"col-sm-9 sist-cursor-pointer\" " + onclickAbrirFormulario + ">";
            template += "		 <span>{TELEFONO}</span>";
            template += "		 <em class=\"help-block sist-margin-cero small\">entre las {HORA_INICIO} y {HORA_TERMINO}</em>";
            template += "	 </div>";
            template += "	 <div class=\"sist-clear-both\"></div>";
            template += "</li>";
            template += "<li class=\"divider\"></li>";

            if (jsonPermisosTelefonoAdicional.CrearTelefonoAdicional) {
                templateBotonAgregar += "<li>";
                templateBotonAgregar += "  <a href=\"javascript:;\" onclick=\"Usuario.abrirFormularioTelefonoPorUsuario({ idTelefonoPorUsuario: '-1', indiceJSON: '-1' })\"><span class=\"glyphicon glyphicon-plus\"></span>&nbsp;Agregar tel&eacute;fono</a>";
                templateBotonAgregar += "</li>";
            }

            //obtiene json de los telefonos del usuario
            json = jQuery("#txtJSONTelefonosPorUsuario").val();
            json = jQuery.parseJSON(json);

            //recorre json para dibujar los telefonos
            jQuery.each(json, function (indice, obj) {
                templateAux = template;
                idTelefonoPorUsuario = obj.IdTelefonoPorUsuario;
                telefono = obj.Telefono;
                horaInicio = obj.HoraInicio;
                horaTermino = obj.HoraTermino;

                if (Validacion.tipoVacio(horaInicio)) {
                    minutosInicio = "";
                } else {
                    minutosInicio = Sistema.right(horaInicio, 2);
                    horaInicio = Sistema.left(horaInicio, 2);
                    horaInicio = horaInicio + ":" + minutosInicio;
                }

                if (Validacion.tipoVacio(horaTermino)) {
                    minutosTermino = "";
                } else {
                    minutosTermino = Sistema.right(horaTermino, 2);
                    horaTermino = Sistema.left(horaTermino, 2);
                    horaTermino = horaTermino + ":" + minutosTermino;
                }

                //reemplaza las marcas
                templateAux = templateAux.replace("{ID_TELEFONO_POR_USUARIO}", idTelefonoPorUsuario);
                templateAux = templateAux.replace(/{INDICE_JSON}/ig, indice);
                templateAux = templateAux.replace("{TELEFONO}", telefono);
                templateAux = templateAux.replace("{HORA_INICIO}", horaInicio);
                templateAux = templateAux.replace("{HORA_TERMINO}", horaTermino);
                html += templateAux;

            });

            //agrega boton Agregar
            html += templateBotonAgregar;
            jQuery("#ulListadoTelefonos").html(html);
            jQuery("#lblTotalTelefonosPorUsuario").html(json.length);

        } catch (e) {
            alert("Exception Usuario.dibujarTelefonosPorUsuario:\n" + e);
        }
    },

    eliminarTelefonoPorUsuario: function (opciones) {
        var json, indiceJSON;

        try {
            indiceJSON = opciones.indiceJSON;

            if (confirm("¿Seguro que desea eliminar el teléfono?")) {
                //obtiene json de los telefonos del usuario
                json = jQuery("#txtJSONTelefonosPorUsuario").val();
                json = jQuery.parseJSON(json);

                //elimina el registro
                json.splice(indiceJSON, 1);

                //ordena json segun HoraInicio
                json = Sistema.ordenarJSON({ json: json, atributoPorOrdenar: "HoraInicio", tipoOrden: "Asc" });

                //asigna json actualizado al control input
                jQuery("#txtJSONTelefonosPorUsuario").val(Sistema.convertirJSONtoString(json));

                //dibuja listado de telefonos
                Usuario.dibujarTelefonosPorUsuario();

                if (opciones.invocadoDesde == Usuario.FORMULARIO_TELEFONO_POR_USUARIO) {
                    jQuery.fancybox.close();
                }

            }

        } catch (e) {
            alert("Exception Usuario.eliminarTelefonoPorUsuario:\n" + e);
        }

    },

    visibleControlesCoordinadorLineaTransporte: function (opciones) {
        try {
            //muestra controles
            jQuery("#hDatosEspecialPorPerfil").show();
            jQuery("#hLineaTransporteAsociada").show();

            //limpia controles
            if (opciones.limpiarControles) {
                jQuery("#ddlTransportistaAsociado_ddlCombo").select2("val", "");
            }
        } catch (e) {
            alert("Exception Usuario.visibleControlesCoordinadorLineaTransporte:\n" + e);
        }
    },

    validarControlesCoordinadorLineaTransporte: function (jsonValidarCampos) {
        var item;

        try {
            item = { elemento_a_validar: "ddlTransportistaAsociado_ddlCombo", requerido: true };
            jsonValidarCampos.push(item);

        } catch (e) {
            alert("Exception Usuario.validarControlesCoordinadorLineaTransporte:\n" + e);
        }

        return jsonValidarCampos;
    },

    visibleControlesTeleoperador: function (opciones) {
        try {
            //muestra controles
            jQuery("#hDatosEspecialPorPerfil").show();
            jQuery("#hTeleoperador").show();

            //limpia controles
            if (opciones.limpiarControles) {
                jQuery("#rbtnTeleoperadorTodasLasAlertas").attr("checked", true);
                jQuery("#rbtnTeleoperadorSoloCemtra").attr("checked", false);
                jQuery("#rbtnTeleoperadorExcluirCemtra").attr("checked", false);
            }
        } catch (e) {
            alert("Exception Usuario.visibleControlesTeleoperador:\n" + e);
        }
    },

    validarFormularioPulsoDiario: function () {
        var fechaInicio, fechaTermino;

        try {
            fechaInicio = jQuery("#" + Sistema.PREFIJO_CONTROL + "TxFechaInicio").val();
            fechaTermino = jQuery("#" + Sistema.PREFIJO_CONTROL + "TxFechaTermino").val();

            var jsonValidarCampos = [
                { elemento_a_validar: Sistema.PREFIJO_CONTROL + "TxFechaInicio", requerido: false, tipo_validacion: "fecha" }
                , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "TxFechaInicio", requerido: false, tipo_validacion: "fecha" }
                , { valor_a_validar: fechaTermino, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: "lblMensajeErrorFechaInicio", mensaje_validacion: "No puede ser mayor que Fecha Final", extras: { fechaInicio: fechaInicio, horaInicio: "00:00", fechaTermino: fechaTermino, horaTermino: "23:59" } }
            ];
            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
                return false;
            } else {
                return true;
            }
        } catch (e) {
            alert("Exception: Usuario.validarFormularioPulsoDiario\n" + e);
        }
    },

    dibujarPulsoDiario: function () {
        try {
            var fecIni = jQuery("#" + Sistema.PREFIJO_CONTROL + "TxFechaInicio").val();
            var fecFin = jQuery("#" + Sistema.PREFIJO_CONTROL + "TxFechaTermino").val();
            var turno = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlTurno_ddlCombo").select2("val");
            var operador = jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlOperador_ddlCombo").select2("val");

            //formatea valores
            fecIni = (fecIni == "") ? "-1" : fecIni;
            fecFin = (fecFin == "") ? "-1" : fecFin;

            var mensajeCargando = Sistema.mensajeCargandoDatos("dibujando gr&aacute;fico, espere un momento ...");
            jQuery("#hGrafico").html(mensajeCargando);

            setTimeout(function () {
                Usuario.obtenerReportepulsoDiario(fecIni, fecFin, turno, operador);
            }, 100);
        } catch (e) {
            alert("Exception: Usuario.dibujarPulsoDiario\n" + e);
        }
    },

    visibleControlesCentroDistribucion: function (opciones) {
        var label, helpBlock;

        try {
            switch (Sistema.toUpper(opciones.textoLabel)) {
                case "ASOCIAR":
                    label = "Seleccione Cedis para asociar";
                    helpBlock = "";
                    break;
                default:
                    label = "Seleccione Cedis a gestionar";
                    helpBlock = "(si no selecciona ninguno gestionar&aacute; todos los Cedis)";
                    break;
            }

            //muestra controles
            jQuery("#hDatosEspecialPorPerfil").show();
            jQuery("#hCentroDistribucion").show();
            jQuery("#lblCentroDistribucionLabel").html(label);
            jQuery("#lblCentroDistribucionHelpBlock").html(helpBlock);

            //limpia controles
            if (opciones.limpiarControles) {
                jQuery("#ddlCentroDistribucionAsociados").select2("val", "");
            }
        } catch (e) {
            alert("Exception Usuario.visibleControlesCentroDistribucion:\n" + e);
        }
    },

    visibleControlesLocales: function (opciones) {
        var label, helpBlock;

        try {
            //muestra controles
            jQuery("#hDatosEspecialPorPerfil").show();
            jQuery("#hLocales").show();

            //limpia controles
            if (opciones.limpiarControles) {
                jQuery("#ddlLocalesAsociados").select2("val", "");
            }
        } catch (e) {
            alert("Exception Usuario.visibleControlesLocales:\n" + e);
        }
    },

    cagarDatosCemtra: function () {

        clearInterval(Usuario.SETINTERVAL_ESTADO_CONEXION_TELEOPERADOR);
        clearInterval(Usuario.SETINTERVAL_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR);

        //Usuario.obtenerEstadoConexionTeleoperadorCemtra();
        //Usuario.SETINTERVAL_ESTADO_CONEXION_TELEOPERADOR = setInterval(function () { Usuario.obtenerEstadoConexionTeleoperadorCemtra(); }, Usuario.SETINTERVAL_MILISEGUNDOS_ESTADO_CONEXION_TELEOPERADOR);
        console.log("iniciando todo");
        Usuario.obtenerEstadoConexionTeleoperadorCemtra(idUsuarioGlobal);
        //setTimeout(function () { console.log("esperando"); }, 60000);
        
        Usuario.SETINTERVAL_ESTADO_CONEXION_TELEOPERADOR = setInterval(function () { Usuario.obtenerEstadoConexionTeleoperadorCemtra(idUsuarioGlobal); }, Usuario.SETINTERVAL_MILISEGUNDOS_ESTADO_CONEXION_TELEOPERADOR);
        

        //setTimeout(function () {
            Usuario.dibujarTablaEstadoConexionTeleoperador({ cemtra: true });
            Usuario.SETINTERVAL_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR = setInterval(function () { Usuario.dibujarTablaEstadoConexionTeleoperador({ cemtra: true }); }, Usuario.SETINTERVAL_MILISEGUNDOS_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR);    
        //}, 50000);

        

    }

};
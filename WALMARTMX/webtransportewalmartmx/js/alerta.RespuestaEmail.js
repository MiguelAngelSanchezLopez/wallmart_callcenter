﻿var AlertaRespuesta = {

    abrirFormulario: function (opciones) {
        try {
            jQuery.fancybox({
                width: "100%",
                height: "100%",
                modal: false,
                type: "iframe",
                href: opciones
            });
        } catch (e) {
            alert("Exception Usuario.abrirFormulario:\n" + e);
        }
    },

    validarFormulario: function () {
        var texto, esRrequerido;

        try {
            texto = jQuery("#ddlRespuestaCategoria_ddlCombo option:selected").text();
            esRrequerido = (Sistema.contains(texto, "Detención no autorizada") || Sistema.contains(texto, "Camión en Panne")) ? true : false;

            var jsonValidarCampos = [
              { elemento_a_validar: "ddlRespuestaCategoria_ddlCombo", requerido: true },
              { elemento_a_validar: "txtObservacion", requerido: esRrequerido, tipo_validacion: "caracteres-prohibidos" }
            ];

            var esValido = Validacion.validarControles(jsonValidarCampos);

            if (!esValido) {
                Sistema.alertaMensajeError();
                return false;
            } else {
                return true;
            }
        } catch (e) {
            alert("Exception Alerta.RespuestaEmail.validarFormulario:\n" + e);
        }
    }
};
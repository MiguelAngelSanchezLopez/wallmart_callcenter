﻿var GestionOperacion = {
  SIN_TELEFONO: "SIN TELEFONO",
  IMPORTACION: "Importacion",
  EXPORTACION: "Exportacion",
  BLOQUE_DATOS_LLEGADA: "DatosLlegada",
  BLOQUE_CARGA_SUELTA: "CargaSuelta",
  CAMPO_MOSTRAR_OBSERVACION: "Observacion",
  CAMPO_MOSTRAR_FECHA_LLEGADA: "FechaLlegada",
  CAMPO_MOSTRAR_EXPORTACION: "Exportacion",

  mostrarFiltrosBuscador: function (opciones) {
    var modo;

    try {
      if (Sistema.toUpper(opciones.modo) == "ABRIR") {
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hFiltroBuscador").slideDown("slow");
        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnMostrarFiltroBuscador").removeClass("btn-success").addClass("btn-danger");
        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnMostrarFiltroBuscador").html("<span class=\"glyphicon glyphicon-remove\"></span>&nbsp;Ocultar Filtros");
        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnMostrarFiltroBuscador").attr("onclick", "GestionOperacion.mostrarFiltrosBuscador({ modo: 'cerrar' })");
      } else {
        jQuery("#" + Sistema.PREFIJO_CONTROL + "hFiltroBuscador").slideUp("slow");
        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnMostrarFiltroBuscador").removeClass("btn-danger").addClass("btn-success");
        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnMostrarFiltroBuscador").html("<span class=\"glyphicon glyphicon-search\"></span>&nbsp;Mostrar Filtros");
        jQuery("#" + Sistema.PREFIJO_CONTROL + "btnMostrarFiltroBuscador").attr("onclick", "GestionOperacion.mostrarFiltrosBuscador({ modo: 'abrir' })");
      }
    } catch (e) {
      alert("Exception GestionOperacion.mostrarFiltrosBuscador:\n" + e);
    }
  },

  validarFormularioBusquedaAsignacionViajeImportacion: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaPresentacion", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtHoraPresentacion", requerido: false, tipo_validacion: "hora" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaProgramacion", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroOrdenServicio", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroGuiaDespacho", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: GestionOperacion.validarFormularioBusquedaAsignacionViajeImportacion\n" + e);
      return false;
    }
  },

  abrirFormularioAsignacionViajeImportacion: function (opciones) {
    var idImportacion;

    try {
      idImportacion = opciones.idImportacion;

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/GestionOperacion.AsignacionViajeImportacionDetalle.aspx?idImportacion=" + idImportacion
      });
    } catch (e) {
      alert("Exception GestionOperacion.abrirFormularioAsignacionViajeImportacion:\n" + e);
    }
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.GestionOperacion.cargarDesdePopUp(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception GestionOperacion.cerrarPopUp:\n" + e);
    }
  },

  cargarDesdePopUp: function (valor) {
    try {
      jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val(valor);
    } catch (e) {
      alert("Exception GestionOperacion.cargarDesdePopUp:\n" + e);
    }
  },

  seleccionarConductor: function (opciones) {
    var prefijoControl, jsonDatosConductor, indiceFila;

    try {
      prefijoControl = opciones.prefijoControl;
      jsonDatosConductor = jQuery("#" + prefijoControl + "_txtJSONDatosConductor").val();
      jsonDatosConductor = (typeof (jsonDatosConductor) === "undefined") ? "[]" : jsonDatosConductor;
      jQuery("#txtJSONDatosConductor").val(jsonDatosConductor);

      //marca el radiobutton
      jQuery("#" + prefijoControl + "_rbtnSeleccionar").attr("checked", true);

      //destaca la fila seleccionada
      jQuery("tr[data-type=fila-conductor]").each(function (indice, obj) {
        indiceFila = jQuery(obj).attr("data-indice");

        if (indiceFila == prefijoControl) {
          jQuery("#" + indiceFila + "_trFila").addClass("success");
        } else {
          jQuery("#" + indiceFila + "_trFila").removeClass("success");
        }
      });

    } catch (e) {
      alert("Exception: GestionOperacion.seleccionarConductor\n" + e);
    }
  },

  validarFormularioAsignacionConductor: function () {
    var jsonValidarCampos = [];
    var indice, valorValidarIndice;

    try {
      indice = jQuery("input:radio[name=groupConductores]:checked").val();
      if (typeof (indice) == "undefined") {
        valorValidarIndice = "-1"; // -1 -> es invalido
      } else {
        valorValidarIndice = "1"; // 1 -> valido
      }

      item = {
        valor_a_validar: valorValidarIndice,
        requerido: false,
        tipo_validacion: "numero-entero-positivo",
        contenedor_mensaje_validacion: "hMensajeErrorTablaDatos",
        mensaje_validacion: "Debe seleccionar un Operador"
      };
      jsonValidarCampos.push(item);

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: GestionOperacion.validarFormularioAsignacionConductor\n" + e);
      return false;
    }
  },

  abrirFormularioAsignarConductorManual: function (opciones) {
    var rutConductorAsignado;
    var queryString = "";

    try {
      rutConductorAsignado = jQuery("#txtRutConductorAsignado").val();
      queryString += "?rutConductorAsignado=" + Sistema.urlEncode(rutConductorAsignado);

      jQuery.fancybox({
        width: "90%",
        height: "90%",
        modal: false,
        type: "iframe",
        helpers: {
          overlay: {
            css: {
              background: Sistema.FANCYBOX_OVERLAY_COLOR_SUBNIVEL
            }
          }
        },
        href: "../page/GestionOperacion.AsignacionConductorManual.aspx" + queryString
      });

    } catch (e) {
      alert("Exception GestionOperacion.abrirFormularioAsignarConductorManual:\n" + e);
    }
  },

  validarFormularioAsignacionConductorManual: function () {
    var jsonValidarCampos = [];
    var indice, valorValidarIndice, item;

    try {
      indice = jQuery("input:radio[name=groupConductores]:checked").val();
      if (typeof (indice) == "undefined") {
        valorValidarIndice = "-1"; // -1 -> es invalido
      } else {
        valorValidarIndice = "1"; // 1 -> valido
      }

      item = {
        valor_a_validar: valorValidarIndice,
        requerido: false,
        tipo_validacion: "numero-entero-positivo",
        contenedor_mensaje_validacion: "hMensajeErrorTablaDatos",
        mensaje_validacion: "Debe seleccionar un Operador"
      };
      jsonValidarCampos.push(item);

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        jQuery("#txtJSONDatosConductor", window.parent.document).val(jQuery("#txtJSONDatosConductor").val());
        parent.jQuery.fancybox.close();

        //ejecuta boton Grabar de la ventana que lo invoco
        parent.__doPostBack("btnGrabar", "");
      }

    } catch (e) {
      alert("Exception: GestionOperacion.validarFormularioAsignacionConductorManual\n" + e);
    }
  },

  modificarDatoFila: function (opciones) {
    var prefijoControl, campo, telefono, patenteTracto, patenteTrailer;

    try {
      prefijoControl = opciones.prefijoControl;
      campo = opciones.campo;

      switch (campo) {
        case "Telefono":
          jQuery("#" + prefijoControl + "_lblTelefono").hide();
          jQuery("#" + prefijoControl + "_hTelefono").show();

          telefono = jQuery("#" + prefijoControl + "_lblTelefono").text();
          telefono = (telefono == GestionOperacion.SIN_TELEFONO) ? "" : telefono;
          jQuery("#" + prefijoControl + "_txtTelefono").val(telefono);
          jQuery("#" + prefijoControl + "_txtTelefono").focus();
          break;
        case "Patente":
          jQuery("#" + prefijoControl + "_lblPatente").hide();
          jQuery("#" + prefijoControl + "_hPatente").show();

          patenteTracto = jQuery("#" + prefijoControl + "_lblPatenteTracto").text();
          patenteTrailer = jQuery("#" + prefijoControl + "_lblPatenteTrailer").text();
          //telefono = (telefono == GestionOperacion.SIN_TELEFONO) ? "" : telefono;
          jQuery("#" + prefijoControl + "_txtPatenteTracto").val(patenteTracto);
          jQuery("#" + prefijoControl + "_txtPatenteTrailer").val(patenteTrailer);
          jQuery("#" + prefijoControl + "_txtPatenteTracto").focus();
          break;
      }

    } catch (e) {
      alert("Exception: GestionOperacion.modificarDatoFila\n" + e);
    }
  },

  cancelarModificarDatoFila: function (opciones) {
    var prefijoControl, campo;

    try {
      prefijoControl = opciones.prefijoControl;
      campo = opciones.campo;

      switch (campo) {
        case "Telefono":
          jQuery("#" + prefijoControl + "_lblTelefono").show();
          jQuery("#" + prefijoControl + "_hTelefono").hide();
          break;
        case "Patente":
          jQuery("#" + prefijoControl + "_lblPatente").show();
          jQuery("#" + prefijoControl + "_hPatente").hide();
          break;
      }
    } catch (e) {
      alert("Exception: GestionOperacion.cancelarModificarDatoFila\n" + e);
    }
  },

  grabarModificarDatoFila: function (opciones) {
    var prefijoControl, json, telefono, row, jsonAux, campo, patenteTracto, patenteTrailer;
    var jsonValidarCampos = [];

    try {
      prefijoControl = opciones.prefijoControl;
      campo = opciones.campo;
      json = jQuery("#" + prefijoControl + "_txtJSONDatosConductor").val();
      json = jQuery.parseJSON(json);
      row = json[0];
      telefono = jQuery("#" + prefijoControl + "_txtTelefono").val();
      patenteTracto = jQuery("#" + prefijoControl + "_txtPatenteTracto").val();
      patenteTrailer = jQuery("#" + prefijoControl + "_txtPatenteTrailer").val();

      switch (campo) {
        case "Telefono":
          jsonValidarCampos = [
            { elemento_a_validar: prefijoControl + "_txtTelefono", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
          ];
          break;
        case "Patente":
          jsonValidarCampos = [
              { elemento_a_validar: prefijoControl + "_txtPatenteTracto", requerido: false, tipo_validacion: "patente" }
            , { elemento_a_validar: prefijoControl + "_txtPatenteTrailer", requerido: false, tipo_validacion: "patente" }
          ];
          break;
      }
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
      } else {
        switch (campo) {
          case "Telefono":
            row.Telefono = telefono;
            telefono = (telefono == "") ? GestionOperacion.SIN_TELEFONO : telefono;
            jQuery("#" + prefijoControl + "_lblTelefono").html(telefono);
            break;
          case "Patente":
            row.PatenteTracto = patenteTracto;
            row.PatenteTrailer = patenteTrailer;
            jQuery("#" + prefijoControl + "_lblPatenteTracto").html(patenteTracto);
            jQuery("#" + prefijoControl + "_lblPatenteTrailer").html(patenteTrailer);
            break;
        }
        jQuery("#" + prefijoControl + "_txtJSONDatosConductor").val(Sistema.convertirJSONtoString(json));

        //si el conductor esta seleccionado, copia json modificado en el json principal
        if (jQuery("#" + prefijoControl + "_rbtnSeleccionar").is(":checked")) {
          jsonAux = jQuery("#" + prefijoControl + "_txtJSONDatosConductor").val();
          jQuery("#txtJSONDatosConductor").val(jsonAux);
        }

        //actualiza el registro en base datos
        opciones.json = json;
        GestionOperacion.actualizarRegistroEnBaseDatos(opciones);
      }

    } catch (e) {
      alert("Exception: GestionOperacion.grabarModificarDatoFila\n" + e);
    }
  },

  actualizarRegistroEnBaseDatos: function (opciones) {
    var row, telefono, origenDatoTabla, origenDatoIdRegistro, patenteTracto, patenteTrailer, campo;
    var queryString = "";

    try {
      row = opciones.json[0];
      origenDatoTabla = row.OrigenDatoTabla;
      origenDatoIdRegistro = row.OrigenDatoIdRegistro;
      telefono = row.Telefono;
      patenteTracto = row.PatenteTracto;
      patenteTrailer = row.PatenteTrailer;
      campo = opciones.campo;

      queryString += "op=ActualizarRegistroEnBaseDatos";
      queryString += "&origenDatoTabla=" + Sistema.urlEncode(origenDatoTabla);
      queryString += "&origenDatoIdRegistro=" + Sistema.urlEncode(origenDatoIdRegistro.toString());
      queryString += "&telefono=" + Sistema.urlEncode(telefono);
      queryString += "&patenteTracto=" + Sistema.urlEncode(patenteTracto);
      queryString += "&patenteTrailer=" + Sistema.urlEncode(patenteTrailer);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo actualizar el registro en base de datos.");
        } else {
          GestionOperacion.cancelarModificarDatoFila(opciones);
          // refresca la pantalla despues de hacer la actualizacion de las patentes
          switch (campo) {
            case "Patente":
              jQuery("#txtCargaDesdePopUp").val("1");
              __doPostBack("btnFiltrar", "");
              break;
          }
        }
      }
      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo actualizar el registro en base de datos.");
      }
      jQuery.ajax({
        url: "../webAjax/waSistema.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception: GestionOperacion.actualizarRegistroEnBaseDatos\n" + e);
    }
  },

  desasignarConductor: function () {
    try {
      if (confirm("¿Está seguro que desea desasignar el Operador?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: GestionOperacion.desasignarConductor\n" + e);
      return false;
    }
  },

  validarFormularioBusquedaAsignacionViajeExportacion: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaPresentacion", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtHoraPresentacion", requerido: false, tipo_validacion: "hora" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaProgramacion", requerido: false, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroOrdenServicio", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroGuiaDespacho", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: GestionOperacion.validarFormularioBusquedaAsignacionViajeExportacion\n" + e);
      return false;
    }
  },

  abrirFormularioAsignacionViajeExportacion: function (opciones) {
    var idExportacion;

    try {
      idExportacion = opciones.idExportacion;

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/GestionOperacion.AsignacionViajeExportacionDetalle.aspx?idExportacion=" + idExportacion
      });
    } catch (e) {
      alert("Exception GestionOperacion.abrirFormularioAsignacionViajeExportacion:\n" + e);
    }
  },

  verHistorialAsignacionViaje: function (opciones) {
    var numeroOrdenServicio, versionMO, url, entidad, numeroLineaMO;
    var queryString = "";

    try {
      numeroOrdenServicio = opciones.numeroOrdenServicio;
      numeroLineaMO = opciones.numeroLineaMO;
      versionMO = opciones.versionMO;
      entidad = opciones.entidad;

      queryString += "?numeroOrdenServicio=" + numeroOrdenServicio;
      queryString += "&numeroLineaMO=" + numeroLineaMO;
      queryString += "&versionMO=" + versionMO;

      switch (entidad) {
        case GestionOperacion.IMPORTACION:
          url = "GestionOperacion.AsignacionViajeImportacionHistorialMO.aspx";
          break;
        case GestionOperacion.EXPORTACION:
          url = "GestionOperacion.AsignacionViajeExportacionHistorialMO.aspx";
          break;
      }

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/" + url + queryString
      });
    } catch (e) {
      alert("Exception GestionOperacion.verHistorialAsignacionViaje:\n" + e);
    }
  },

  abrirFormularioDatoExtra: function (opciones) {
    var idRegistro, entidad, bloque, fechaHoraPresentacion, campoMostrar;
    var queryString = "";

    try {
      idRegistro = opciones.idRegistro;
      entidad = opciones.entidad;
      bloque = opciones.bloque;
      fechaHoraPresentacion = opciones.fechaHoraPresentacion;
      campoMostrar = opciones.campoMostrar;

      queryString += "?idRegistro=" + idRegistro;
      queryString += "&entidad=" + entidad;
      queryString += "&bloque=" + bloque;
      queryString += "&fechaHoraPresentacion=" + Sistema.urlEncode(fechaHoraPresentacion);
      queryString += "&campoMostrar=" + campoMostrar;

      jQuery.fancybox({
        width: "80%",
        height: "80%",
        modal: false,
        type: "iframe",
        href: "../page/GestionOperacion.DatoExtra.aspx" + queryString
      });
    } catch (e) {
      alert("Exception GestionOperacion.abrirFormularioDatoExtra:\n" + e);
    }
  },

  validarFormularioDatoExtra: function () {
    var item, fechaLlegada, horaLlegada, requeridoFechaLlegada, requeridoHoraLlegada, requeridoFechaSalida, requeridoHoraSalida;

    try {
      fechaLlegada = jQuery("#txtFechaLlegada").val();
      horaLlegada = jQuery("#txtHoraLlegada").val();
      fechaSalida = jQuery("#txtFechaSalida").val();
      horaSalida = jQuery("#txtHoraSalida").val();

      if (!Validacion.tipoVacio(fechaLlegada) || !Validacion.tipoVacio(horaLlegada)) {
        requeridoFechaLlegada = true;
        requeridoHoraLlegada = true;
      } else {
        requeridoFechaLlegada = false;
        requeridoHoraLlegada = false;
      }

      if (!Validacion.tipoVacio(fechaSalida) || !Validacion.tipoVacio(horaSalida)) {
        requeridoFechaSalida = true;
        requeridoHoraSalida = true;
      } else {
        requeridoFechaSalida = false;
        requeridoHoraSalida = false;
      }

      var jsonValidarCampos = [
          { elemento_a_validar: "txtFechaLlegada", requerido: requeridoFechaLlegada, tipo_validacion: "fecha" }
        , { elemento_a_validar: "txtHoraLlegada", requerido: requeridoHoraLlegada, tipo_validacion: "hora" }
        , { elemento_a_validar: "txtObservacionLlegada", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtCantidadPallet", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtPesoReal", requerido: false, tipo_validacion: "numero-decimal" }
        , { elemento_a_validar: "txtOrdenCompra", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtFechaSalida", requerido: requeridoFechaSalida, tipo_validacion: "fecha" }
        , { elemento_a_validar: "txtHoraSalida", requerido: requeridoHoraSalida, tipo_validacion: "hora" }
        , { elemento_a_validar: "txtNroSello", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtNroContenedor", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { valor_a_validar: fechaLlegada, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: "lblMensajeErrorFechaSalida", mensaje_validacion: "No puede ser menor que Fecha Llegada", extras: { fechaInicio: fechaLlegada, horaInicio: horaLlegada, fechaTermino: fechaSalida, horaTermino: horaSalida} }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: GestionOperacion.validarFormularioDatoExtra\n" + e);
      return false;
    }
  },

  actualizaInterfazDatoExtra: function (opciones) {
    var bloque, entidad, campoMostrar;

    try {
      bloque = opciones.bloque;
      entidad = opciones.entidad;
      campoMostrar = opciones.campoMostrar;

      //muestra el bloque seleccionado
      jQuery("div[data-bloqueVisible~='" + bloque + "']").show();

      switch (campoMostrar) {
        case GestionOperacion.CAMPO_MOSTRAR_FECHA_LLEGADA:
          jQuery("div[data-campoVisible~='FechaLlegada']").show();
          jQuery("div[data-campoVisible~='Observacion']").show();
          break;
        case GestionOperacion.CAMPO_MOSTRAR_OBSERVACION:
          jQuery("div[data-campoVisible~='Observacion']").show();
          break;
        case GestionOperacion.CAMPO_MOSTRAR_EXPORTACION:
          jQuery("div[data-campoVisible~='Exportacion']").show();
          break;
      }
    } catch (e) {
      alert("Exception: GestionOperacion.actualizaInterfazDatoExtra\n" + e);
    }
  },

  validarFormularioBusquedaAsignacionManual: function () {
    try {
      var jsonValidarCampos = [
          { elemento_a_validar: "txtNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtRut", requerido: false, tipo_validacion: "rut" }
        , { elemento_a_validar: "txtPatente", requerido: false, tipo_validacion: "patente" }
      ];

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: GestionOperacion.validarFormularioBusquedaAsignacionManual\n" + e);
      return false;
    }
  }

};


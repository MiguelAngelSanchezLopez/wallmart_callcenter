﻿var Discrepancia = {
  SETINTERVAL_VALIDAR_GENERACION: 0,
  DIFERENCIA_DIAS_RANGO_FECHA: 31,

  validarFormularioBusqueda: function () {
    var fechaContableDesde, fechaContableHasta, requeridoFechaContableDesde, requeridoFechaContableHasta, item, valorValidarDiferenciaDias;

    try {
      fechaContableDesde = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaContableDesde").val();
      fechaContableHasta = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaContableHasta").val();

      requeridoFechaContableDesde = (Validacion.tipoVacio(fechaContableHasta)) ? false : true;
      requeridoFechaContableHasta = (Validacion.tipoVacio(fechaContableDesde)) ? false : true;

      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroTransporte", requerido: false, tipo_validacion: "numero-entero-positivo" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtDM", requerido: false, tipo_validacion: "numero-entero-positivo" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaContableDesde", requerido: requeridoFechaContableDesde, tipo_validacion: "fecha" }
        , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaContableHasta", requerido: requeridoFechaContableHasta, tipo_validacion: "fecha" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
        , { valor_a_validar: fechaContableDesde, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorFechaContableHasta", mensaje_validacion: "No puede ser menor que Fecha Desde", extras: { fechaInicio: fechaContableDesde, horaInicio: "00:00", fechaTermino: fechaContableHasta, horaTermino: "23:59"} }
      ];

      //-----------------------------
      //si hay rango de fecha establecido, valida que sea a lo mas hasta 30 dias
      if (!Validacion.tipoVacio(fechaContableDesde) && !Validacion.tipoVacio(fechaContableHasta)) {
        var dateFechaContableDesde = Sistema.getDate(fechaContableDesde, "00:00");
        var dateFechaContableHasta = Sistema.getDate(fechaContableHasta, "00:00");
        var diferenciaDias = Sistema.dateDiff("d", dateFechaContableDesde, dateFechaContableHasta);

        if (diferenciaDias > Discrepancia.DIFERENCIA_DIAS_RANGO_FECHA) {
          valorValidarDiferenciaDias = -1; // el rango es INVALIDO
        } else {
          valorValidarDiferenciaDias = 1; // el rango es valido
        }
      } else {
        valorValidarDiferenciaDias = 1; // el rango es valido
      }
      item = {
        valor_a_validar: valorValidarDiferenciaDias,
        requerido: false,
        tipo_validacion: "numero-entero-positivo",
        contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorFechaDiferenciaDias",
        mensaje_validacion: "El rango de fecha debe ser menor o igual a " + Discrepancia.DIFERENCIA_DIAS_RANGO_FECHA + " d&iacute;as"
      };
      jsonValidarCampos.push(item);

      var esValido = Validacion.validarControles(jsonValidarCampos);
      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Discrepancia.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  abrirFormulario: function (opciones) {
    try {
      var queryString = "";
      var nroTransporte = opciones.nroTransporte;
      var codigoLocal = opciones.codigoLocal;
      var nombreLocal = opciones.nombreLocal;
      var dm = opciones.dm;
      var mensajeRetorno = opciones.mensajeRetorno;

      queryString += "?nroTransporte=" + nroTransporte;
      queryString += "&codigoLocal=" + codigoLocal;
      queryString += "&nombreLocal=" + Sistema.urlEncode(nombreLocal);
      queryString += "&dm=" + dm;
      queryString += "&mensajeRetorno=" + Sistema.urlEncode(mensajeRetorno);

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Discrepancia.Detalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Discrepancia.abrirFormulario:\n" + e);
    }
  },

  cerrarPopUp: function () {
    try {
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Discrepancia.cerrarPopUp:\n" + e);
    }

  },

  verInformePDF: function (opciones) {
    var queryString = "";
    var tipoInforme, idFormulario, urlInvocadoDesde, nroTransporte, codigoLocal, nombreLocal, dm;

    try {
      tipoInforme = opciones.tipoInforme;
      idFormulario = opciones.idFormulario;
      nroTransporte = opciones.nroTransporte;
      codigoLocal = opciones.codigoLocal;
      nombreLocal = opciones.nombreLocal;
      dm = opciones.dm;

      if (idFormulario == "-1") {
        alert("No existe archivo para descargar");
      } else {
        urlInvocadoDesde = "../page/Discrepancia.Buscador.aspx";
        queryString += "?idFormulario=" + idFormulario;
        queryString += "&tipoInforme=" + Sistema.urlEncode(tipoInforme);
        queryString += "&urlInvocadoDesde=" + Sistema.urlEncode(urlInvocadoDesde);
        queryString += "&nroTransporte=" + Sistema.urlEncode(nroTransporte);
        queryString += "&codigoLocal=" + Sistema.urlEncode(codigoLocal);
        queryString += "&nombreLocal=" + Sistema.urlEncode(nombreLocal);
        queryString += "&dm=" + Sistema.urlEncode(dm);

        jQuery("#btnGenerarPDF" + tipoInforme).addClass("sist-display-none");
        jQuery("#lblMensajeGenerarPDF" + tipoInforme).html(Sistema.mensajeCargandoDatos("Buscando PDF, espere un momento"));
        jQuery("#lblMensajeGenerarPDF" + tipoInforme).show();

        setTimeout(function () {
          Discrepancia.validarGeneracion(opciones);
          //envia valores para generar el excel
          Sistema.accederUrl({ location: "top", url: "../page/Common.GenerarPDFDiscrepancia.aspx" + queryString });
        }, 100);

      }

    } catch (e) {
      alert("Exception Discrepancia.verInformePDF:\n" + e);
    }

  },

  validarGeneracion: function (opciones) {
    var queryString = "";
    var idGenerarReporte, mensajeRetorno;
    var tipoInforme = opciones.tipoInforme;

    //construye queryString
    queryString += "op=ObtenerGeneracionInforme";

    var okFunc = function (t) {
      var respuesta = t;
      if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
        alert("Ha ocurrido un error interno y no se pudo obtener la generación del reporte");
      } else {
        //vuelve a monitorear la sesion despues de X segundos
        if (respuesta == "0") {
          Discrepancia.SETINTERVAL_VALIDAR_GENERACION = setTimeout("Discrepancia.validarGeneracion({ tipoInforme: '" + tipoInforme + "' })", 1000);
        } else {
          clearTimeout(Discrepancia.SETINTERVAL_VALIDAR_GENERACION);
          jQuery("#btnGenerarPDF" + tipoInforme).removeClass("sist-display-none");
          jQuery("#lblMensajeGenerarPDF" + tipoInforme).html("");
          jQuery("#lblMensajeGenerarPDF" + tipoInforme).hide();
        }
      }
    }
    var errFunc = function (t) {
      alert("Ha ocurrido un error interno y no se pudo obtener la generación del reporte");
    }
    jQuery.ajax({
      url: "../webAjax/waReporte.aspx",
      type: "post",
      async: false,
      data: queryString,
      success: okFunc,
      error: errFunc
    });
  },

  validarFormularioBusquedaSinAlertas: function () {
    var fechaContableDesde, fechaContableHasta, requeridoFechaContableDesde, requeridoFechaContableHasta, item, valorValidarDiferenciaDias;

    try {
      fechaContableDesde = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaContableDesde").val();
      fechaContableHasta = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaContableHasta").val();

      requeridoFechaContableDesde = (Validacion.tipoVacio(fechaContableHasta)) ? false : true;
      requeridoFechaContableHasta = (Validacion.tipoVacio(fechaContableDesde)) ? false : true;

      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroViaje", requerido: false, tipo_validacion: "numero-entero-positivo" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtNroReclamo", requerido: false, tipo_validacion: "numero-entero-positivo" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaContableDesde", requerido: requeridoFechaContableDesde, tipo_validacion: "fecha" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFechaContableHasta", requerido: requeridoFechaContableHasta, tipo_validacion: "fecha" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
		    , { valor_a_validar: fechaContableDesde, requerido: false, tipo_validacion: "fecha-mayor-entre-ambas", contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorFechaContableHasta", mensaje_validacion: "No puede ser menor que Fecha Desde", extras: { fechaInicio: fechaContableDesde, horaInicio: "00:00", fechaTermino: fechaContableHasta, horaTermino: "23:59"} }
      ];

      //-----------------------------
      //si hay rango de fecha establecido, valida que sea a lo mas hasta 30 dias
      if (!Validacion.tipoVacio(fechaContableDesde) && !Validacion.tipoVacio(fechaContableHasta)) {
        var dateFechaContableDesde = Sistema.getDate(fechaContableDesde, "00:00");
        var dateFechaContableHasta = Sistema.getDate(fechaContableHasta, "00:00");
        var diferenciaDias = Sistema.dateDiff("d", dateFechaContableDesde, dateFechaContableHasta);

        if (diferenciaDias > Discrepancia.DIFERENCIA_DIAS_RANGO_FECHA) {
          valorValidarDiferenciaDias = -1; // el rango es INVALIDO
        } else {
          valorValidarDiferenciaDias = 1; // el rango es valido
        }
      } else {
        valorValidarDiferenciaDias = 1; // el rango es valido
      }
      item = {
        valor_a_validar: valorValidarDiferenciaDias,
        requerido: false,
        tipo_validacion: "numero-entero-positivo",
        contenedor_mensaje_validacion: Sistema.PREFIJO_CONTROL + "lblMensajeErrorFechaDiferenciaDias",
        mensaje_validacion: "El rango de fecha debe ser menor o igual a " + Discrepancia.DIFERENCIA_DIAS_RANGO_FECHA + " d&iacute;as"
      };
      jsonValidarCampos.push(item);

      var esValido = Validacion.validarControles(jsonValidarCampos);
      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Discrepancia.validarFormularioBusquedaSinAlertas\n" + e);
      return false;
    }
  },

  dibujarComboLocales: function (opciones) {
    var checked, jsonLocales, texto, valor, selected;

    try {
      valorSeleccionado = opciones.valorSeleccionado;
      checked = jQuery("#" + Sistema.PREFIJO_CONTROL + "chkSoloFocoTablet").is(":checked");
      jsonLocales = jQuery("#" + Sistema.PREFIJO_CONTROL + "txtJSONLocales").val();
      jsonLocales = jQuery.parseJSON(jsonLocales);
      jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlLocal").empty();
      jQuery(".chosen-select-locales").select2("destroy");

      //agrega opcion TODOS
      jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlLocal").append("<option value=\"-1\">--Todos--</option>");

      if (checked) {
        jsonLocales = jQuery.grep(jsonLocales, function (item, indice) {
          return (item.FocoTablet == 1);
        });
      }

      jQuery.each(jsonLocales, function (indice, obj) {
        texto = obj.Texto;
        valor = obj.Valor;
        selected = (valorSeleccionado == valor) ? "selected" : "";
        jQuery("#" + Sistema.PREFIJO_CONTROL + "ddlLocal").append("<option value=\"" + valor + "\" " + selected + ">" + texto + "</option>");
      });
      jQuery(".chosen-select-locales").select2();

    } catch (e) {
      alert("Exception: Discrepancia.dibujarComboLocales\n" + e);
      return false;
    }
  }

};
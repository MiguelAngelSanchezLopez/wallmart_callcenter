﻿var Loading = {

    show: function (mensaje) {
        //Carga imagen al div
        $("#imgLoading").attr("src", "../img/cargando.gif");

        //Carga texto al div
        $("#hLoading").text(mensaje);

        //Muestra el div
        $("#div_load").show();
    },

    hide: function () {
        //Oculta el div
        $("#div_load").hide();
        
        //Limpia texto
        $("#hLoading").text("");
    }
}
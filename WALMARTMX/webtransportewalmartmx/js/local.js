﻿var Local = {

  validarFormularioBusqueda: function () {
    try {
      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroCodigo", requerido: false, tipo_validacion: "numero-entero-positivo" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtEtapa", requerido: false, tipo_validacion: "numero-entero-positivo" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Local.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  abrirFormulario: function (opciones) {
    try {
      var queryString = "";
      var idLocal = opciones.idLocal;

      queryString += "?id=" + idLocal;

      jQuery.fancybox({
        width: "100%",
        height: "100%",
        modal: false,
        type: "iframe",
        href: "../page/Mantenedor.LocalDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Local.abrirFormulario:\n" + e);
    }
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.Local.cargarDesdePopUp(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Local.cerrarPopUp:\n" + e);
    }

  },

  cargarDesdePopUp: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception Local.cargarDesdePopUp:\n" + e);
    }
  },

  validarFormularioLocal: function () {
    var item;

    try {
      var existeCodigoLocal = Local.existeCodigoLocal();

      var jsonValidarCampos = [
          { elemento_a_validar: "ddlFormato_ddlCombo", requerido: true }
        , { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtCodigo", requerido: true, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtMaximoHorasAlertaActiva", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtEtapa", requerido: false, tipo_validacion: "numero-entero-positivo-mayor-cero" }
        , { elemento_a_validar: "txtTelefono", requerido: false, tipo_validacion: "caracteres-prohibidos" }
      ];

      //valida que el codigo del local sea unico
      if (jQuery("#txtCodigo").val() != "") {
        item = {
          valor_a_validar: existeCodigoLocal,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorCodigo",
          mensaje_validacion: "El c&oacute;digo ya existe"
        };
        jsonValidarCampos.push(item);
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        Local.obtenerUsuariosPorCargo();
        return true;
      }

    } catch (e) {
      alert("Exception: Local.validarFormularioLocal\n" + e);
      return false;
    }

  },

  existeCodigoLocal: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var codigoLocal = jQuery("#txtCodigo").val();
      var idLocal = jQuery("#txtIdLocal").val();
      queryString += "op=Local.Codigo";
      queryString += "&id=" + idLocal;
      queryString += "&valor=" + Sistema.urlEncode(codigoLocal);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el determinante único de la tienda");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el determinante único de la tienda");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Local.existeCodigoLocal:\n" + e);
    }

    return existe;
  },

  eliminarRegistro: function (opciones) {
    var queryString = "";

    try {
      //construye queryString
      queryString += "op=EliminarLocal";
      queryString += "&idLocal=" + Sistema.urlEncode(opciones.idLocal);

      if (confirm("Si elimina la tienda se eliminarán todas las referencias asociadas a él y no se podrá volver a recuperar.\n¿Desea eliminar la tienda?")) {
        var okFunc = function (t) {
          var respuesta = t;
          if (Sistema.contieneTextoTerminoSesion(respuesta)) {
            Sistema.redireccionarLogin();
          } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
            alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
          } else {
            jQuery("#" + Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp").val("1");
            document.forms[0].submit();
          }
        }
        var errFunc = function (t) {
          alert("Ha ocurrido un error interno y no se pudo eliminar el registro");
        }
        jQuery.ajax({
          url: "../webAjax/waSistema.aspx",
          type: "post",
          async: false,
          data: queryString,
          success: okFunc,
          error: errFunc
        });
      }
    } catch (e) {
      alert("Exception: Local.eliminarRegistro\n" + e);
    }
  },

  validarEliminar: function () {
    try {
      if (confirm("Si elimina la tienda se eliminarán todas las referencias asociados a él y no se podrá volver a recuperar.\n¿Desea eliminar la tienda?")) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      alert("Exception: Local.validarEliminar\n" + e);
      return false;
    }
  },

  dibujarComboCargosPorFormato: function () {
    var queryString = "";
    var idFormato, idLocal, categoria;

    try {
      idFormato = jQuery("#ddlFormato_ddlCombo").val();
      idLocal = jQuery("#txtIdLocal").val();
      categoria = jQuery("#txtCategoria").val();
      idFormato = (idFormato == "") ? "-1" : idFormato;

      //construye queryString
      queryString += "op=DibujarComboCargosPorFormato";
      queryString += "&idFormato=" + Sistema.urlEncode(idFormato);
      queryString += "&idLocal=" + Sistema.urlEncode(idLocal);
      queryString += "&categoria=" + Sistema.urlEncode(categoria);

      var okFunc = function (t) {
        var respuesta = t;
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo obtener los cargos");
        } else {
          jQuery("#hListadoCargos").html(respuesta);
          jQuery("select[data-categoria=combo-cargo]").select2();
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo obtener los cargos");
      }

      jQuery.ajax({
        url: "../webAjax/waSistema.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });

    } catch (e) {
      alert("Exception: Local.dibujarComboCargosPorFormato\n" + e);
    }
  },

  obtenerUsuariosPorCargo: function () {
    var arrUsuarios = [];
    var listado = "";
    var idUsuario;

    try {
      jQuery("select[data-categoria=combo-cargo]").each(function (indice, obj) {
        idUsuario = jQuery(obj).select2("val");
        if (!Validacion.tipoVacio(idUsuario)) {
          arrUsuarios.push(idUsuario);
        }
      });

      listado = arrUsuarios.join(",");
    } catch (e) {
      alert("Exception: Local.obtenerUsuariosPorCargo\n" + e);
    }

    //asigna valor al contol
    jQuery("#txtListadoUsuariosCargos").val(listado);
  }

};
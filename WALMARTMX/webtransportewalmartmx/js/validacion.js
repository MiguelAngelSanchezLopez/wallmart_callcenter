/**
* ---------------
* VALIDACIONES
* ---------------
*
* var opciones = [
*   {
*     elemento_a_validar            : "txtEmail", 					      //requerido -> input|select|textarea
*     requerido						          : true, 						          //requerido -> true o false
*     tipo_validacion					      : "email", 						        //requerido -> email|caracteres-prohibido|numero-entero|... (si es un combo entonces se puede omitir)
*     valor_a_validar					      : "email@prueba.cl", 			    //opcional 	-> se debe setear elemento_a_validar: "", si no hay elemento por validar entonces se puede pasar el valor, sino se deja vacio
*     contenedor_mensaje_validacion	: "lblMensajeValidacion",	    //opcional 	-> se usa cuando se quiere colocar el mensaje de validacion en otro contenedor que no sea el elmento validado
*     mensaje_validacion				    : "Escribe bien el EmAiL!!!"  //opcional 	-> personaliza el mensaje de la validacion
*     class_validacion				      : "cssMensajeError", 			    //opcional 	-> personaliza el estilo del mensaje
*     extras                        : {}                          //opcional  -> se incluyen otras propiedas para validaciones especiales (ejm-> extras: { largo_minimo: 1, largo_maximo: 5})
*   },
*   {...},{...},...
* ];
*
*/
var Validacion = {
  inStr: function (n, s1, s2) {
    var numargs = Validacion.inStr.arguments.length;

    if (numargs < 3)
      return n.indexOf(s1) + 1;
    else
      return s1.indexOf(s2, n) + 1;
  },

  obtenerExtensionArchivo: function (nombreArchivo) {
    var extension = "";

    //valida si es Vacio
    if (jQuery.trim(nombreArchivo) != "") {
      var arrSplit = nombreArchivo.split(".");
      var largo = arrSplit.length;
      extension = arrSplit[largo - 1].toLowerCase();
    }

    return extension;
  },

  obtenerTipoArchivo: function (extension) {
    var tipoImage = "|bmp|cod|gif|ief|jpe|jpeg|jpg|jfif|svg|tif|tiff|ras|cmx|ico|png|pnm|pbm|pgm|ppm|rgb|xbm|xpm|xwd|";
    var tipoOffice = "|doc|docx|dot|xla|xlc|xlm|xls|xlsx|xlt|xlw|msg|sst|cat|stl|pdf|pot|pps|ppt|pptx|mpp|wcm|wdb|wks|wps|mdb|";
    var tipoApplication = "|evy|fif|spl|hta|acx|hqx|bin|class|dms|exe|lha|lzh|oda|axs|prf|p10|crl|ai|eps|ps|rtf|setpay|setreg|hlp|bcpio|cdf|z|tgz|cpio|csh|dcr|dir|dxr|dvi|gtar|gz|hdf|ins|isp|iii|js|latex|crd|clp|dll|m13|m14|mvb|wmf|mny|pub|scd|trm|wri|cdf|nc|pma|pmc|pml|pmr|pmw|p12|pfx|p7b|spc|p7r|p7c|p7m|p7s|sh|shar|swf|sit|sv4cpio|sv4crc|tar|tcl|tex|texi|texinfo|roff|t|tr|man|me|ms|ustar|src|cer|crt|der|pko|zip|";
    var tipoAudio = "|au|snd|mid|rmi|mp3|mp4|aif|aifc|aiff|m3u|ra|ram|wav|";
    var tipoMessage = "|mht|mhtml|nws|";
    var tipoText = "|css|323|htm|html|stm|uls|bas|c|h|txt|rtx|sct|tsv|htt|htc|etx|vcf|csv|";
    var tipoVideo = "|mp2|mpa|mpe|mpeg|mpg|mpv2|mov|qt|lsf|lsx|asf|asr|asx|avi|movie|";
    var tipoOtros = "|flr|vrml|wrl|wrz|xaf|xof|";
    var tipoArchivo = "";

    //se formatea la extension para buscarlo
    extension = "|" + extension + "|";

    //verfica que tipo de archivo es   
    if (Validacion.inStr(0, tipoImage, extension) > 0) {
      tipoArchivo = "image";
    }
    else if (Validacion.inStr(0, tipoOffice, extension) > 0) {
      tipoArchivo = "office";
    }
    else if (Validacion.inStr(0, tipoApplication, extension) > 0) {
      tipoArchivo = "application";
    }
    else if (Validacion.inStr(0, tipoAudio, extension) > 0) {
      tipoArchivo = "audio";
    }
    else if (Validacion.inStr(0, tipoMessage, extension) > 0) {
      tipoArchivo = "message";
    }
    else if (Validacion.inStr(0, tipoText, extension) > 0) {
      tipoArchivo = "text";
    }
    else if (Validacion.inStr(0, tipoVideo, extension) > 0) {
      tipoArchivo = "video";
    }
    else if (Validacion.inStr(0, tipoOtros, extension) > 0) {
      tipoArchivo = "otro";
    }
    else {
      tipoArchivo = "desconocido";
    }

    return tipoArchivo;
  },

  obtenerObjetoFecha: function (strDate, strTime) {
    strDate = strDate.replace(/[-]/g, "/");
    var day = strDate.split("/")[0];
    var month = strDate.split("/")[1] - 1;
    var year = strDate.split("/")[2];
    var hour = strTime.split(":")[0];
    var seg = strTime.split(":")[1];
    return new Date(year, month, day, hour, seg, 0, 0);
  },

  validarControles: function (opciones) {
    var esValido = true;
    var tieneErrores = false;
    var validarPorTipo = true;

    if (opciones) {
      jQuery.each(opciones, function (indice, obj) {
        var elementoValidar = obj.elemento_a_validar;
        var requerido = obj.requerido;
        var tipoValidacion = obj.tipo_validacion;
        var valorValidar = "";
        var contenedorMensajeValidacion = "";
        var mensajeValidacion = "";
        var classValidacion = "text-danger";

        if (obj.valor_a_validar) { valorValidar = obj.valor_a_validar; }
        if (obj.contenedor_mensaje_validacion) { contenedorMensajeValidacion = obj.contenedor_mensaje_validacion; }
        if (obj.mensaje_validacion) { mensajeValidacion = obj.mensaje_validacion; }
        if (obj.class_validacion) { classValidacion = obj.class_validacion; }

        var valor = (valorValidar == "") ? jQuery.trim(jQuery("#" + elementoValidar).val()) : jQuery.trim(valorValidar);

        //valida si es requerido
        if (requerido) {
          esValido = Validacion.tipoVacio(valor);
          if (esValido) {
            validarPorTipo = false;
            if (!tieneErrores) { tieneErrores = true; }

            //mensajeValidacion = "Este campo es requerido";
            mensajeValidacion = (mensajeValidacion == "") ? "Este campo es requerido" : mensajeValidacion;
            Validacion.mostrarMensaje({
              mostrar: true,
              elemento_a_validar: elementoValidar,
              class_validacion: classValidacion,
              mensaje_validacion: mensajeValidacion,
              contenedor_mensaje_validacion: contenedorMensajeValidacion
            });
          } else {
            validarPorTipo = true;
          }
        } else {
          validarPorTipo = true;
        }

        //verifica el tipo de validacion
        if (validarPorTipo) {
          if (Validacion.tipoVacio(valor)) {
            esValido = true;
          } else {
            switch (tipoValidacion) {
              case "email":
                esValido = Validacion.tipoEmail(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El email no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "email-listado":
                esValido = Validacion.tipoEmailListado(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El email no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "fecha":
                esValido = Validacion.tipoFecha(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "La fecha no es v&aacute;lida" : mensajeValidacion; }
                break;

              case "numero-entero":
                esValido = Validacion.tipoNumeroEntero(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El valor no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "numero-entero-positivo":
                esValido = Validacion.tipoNumeroEnteroPositivo(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El valor no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "numero-entero-positivo-mayor-cero":
                esValido = Validacion.tipoNumeroEnteroPositivoMayorCero(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El valor no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "numero-decimal":
                esValido = Validacion.tipoNumeroDecimal(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El valor no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "caracteres-prohibidos":
                esValido = Validacion.tipoCaracteresProhibidos(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El texto no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "alfanumerico":
                esValido = Validacion.tipoAlfanumerico(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El texto no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "rut":
                esValido = Validacion.tipoRut(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El RUT no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "largo":
                esValido = Validacion.tipoLargo(valor, obj);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El largo no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "patente":
                esValido = Validacion.tipoPatente(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "La placa no es v&aacute;lida" : mensajeValidacion; }
                break;

              case "archivo-imagen":
                esValido = Validacion.tipoArchivoImagen(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El archivo no es un formato de imagen" : mensajeValidacion; }
                break;

              case "archivo-extension":
                esValido = Validacion.tipoArchivoExtension(valor, obj);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El archivo no es un formato v&aacute;lido" : mensajeValidacion; }
                break;

              case "rango-numerico":
                esValido = Validacion.tipoRangoNumerico(valor, obj);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El valor no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "numero-formato-americano":
                esValido = Validacion.tipoNumeroFormatoAmericano(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "El valor no es v&aacute;lido" : mensajeValidacion; }
                break;

              case "fecha-mayor-hoy":
              case "fecha-menor-hoy":
              case "fecha-mayor-entre-ambas":
                esValido = Validacion.tipoFechaDentroRangoHoy(tipoValidacion, obj);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "La fecha no est&aacute; fuera de rango" : mensajeValidacion; }
                break;

              case "hora":
                esValido = Validacion.tipoHora(valor);
                if (!esValido) { mensajeValidacion = (mensajeValidacion == "") ? "La hora no es v&aacute;lida" : mensajeValidacion; }
                break;

              default:
                esValido = true;
                break;
            }
          }

          //muestra u oculta el mensaje segun corresponda
          if (!esValido) {
            if (!tieneErrores) { tieneErrores = true; }
            Validacion.mostrarMensaje({
              mostrar: true,
              elemento_a_validar: elementoValidar,
              class_validacion: classValidacion,
              mensaje_validacion: mensajeValidacion,
              contenedor_mensaje_validacion: contenedorMensajeValidacion
            });
          } else {
            Validacion.mostrarMensaje({
              mostrar: false,
              elemento_a_validar: elementoValidar,
              contenedor_mensaje_validacion: contenedorMensajeValidacion
            });
          }

        }

      });
    }

    return !tieneErrores;
  },

  mostrarMensaje: function (opciones) {
    var prefijoControlMensaje = "msjError_";
    var elementoContenedorMensaje = opciones.elemento_a_validar;
    var colocarMensajeDespuesControl = true;

    if (opciones.contenedor_mensaje_validacion) {
      elementoContenedorMensaje = (opciones.contenedor_mensaje_validacion != "") ? opciones.contenedor_mensaje_validacion : elementoContenedorMensaje;
      colocarMensajeDespuesControl = false;
    }
    var nombreElementoContenedorMensaje = prefijoControlMensaje + elementoContenedorMensaje;
    var totalElementos = jQuery("#" + nombreElementoContenedorMensaje).length;

    //si no existe el control entonces lo agrega
    if (opciones.mostrar) {
      if (totalElementos == 0) {
        var htmlMensaje = "<div id=\"" + nombreElementoContenedorMensaje + "\" class=\"" + opciones.class_validacion + "\">" + opciones.mensaje_validacion + "</div>";

        if (colocarMensajeDespuesControl) {
          jQuery("#" + elementoContenedorMensaje).after(htmlMensaje);
        } else {
          jQuery("#" + elementoContenedorMensaje).html(htmlMensaje);
        }

      } else {
        jQuery("#" + nombreElementoContenedorMensaje).show();
        jQuery("#" + nombreElementoContenedorMensaje).html(opciones.mensaje_validacion);
      }
    } else {
      jQuery("#" + nombreElementoContenedorMensaje).hide();
    }
  },

  testearExpresionRegular: function (valor, expresion) {
    var expresionRegular = new RegExp(expresion);
    return expresionRegular.test(valor);
  },

  /*************************************************
  TIPOS DE VALIDACIONES
  *************************************************/
  tipoVacio: function (valor) {
    var esValido = false;
    if (valor == "") {
      esValido = true;
    }
    return esValido;
  },

  tipoEmail: function (valor) {
    var esValido = false;
    var expresionRegular = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\\.[A-Za-z]{2,4}$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  },

  tipoEmailListado: function (valor) {
    var esValido = true;
    valor = valor.replace(/,/ig, ";");
    var arrEmail = valor.split(";");

    jQuery.each(arrEmail, function (indice, email) {
      if (!Validacion.tipoEmail(jQuery.trim(email))) {
        esValido = false;
        return false;
      }
    });

    return esValido;
  },

  tipoFecha: function (valor) {
    var esValido = false;
    var largoObjeto;

    //Strings de expresiones regulares
    var meses = "01|02|03|04|05|06|07|08|09|10|11|12";
    var dias = "01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31";
    var expresionRegular = "^(" + dias + ")\(\/)(" + meses + ")\(\/)[0-9][0-9][0-9][0-9]$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    if (esValido) {
      var largo = valor.length;
      var dia = valor.substr(0, 2);
      var mes = valor.substr(3, 2);
      var anno = valor.substr(largo - 4, 4);

      if (mes == "02") {
        //Si es divisible por 4 y no por 100 a no ser que sea div por 400 -> 29 2000 si 1900 no
        var bisiesto = (((anno % 4 == 0) & (anno % 100 != 0)) | (anno % 400 == 0)) ? true : false;
        esValido = (bisiesto) ? (eval(dia + "<30")) : (eval(dia + "<29"));
      }
      //Revisa los dias 31 del mes
      else if (dia == "31" && ((eval(mes + "<8")) && (mes % 2 == 0) || (eval(mes + ">7")) && (mes % 2 == 1))) {
        esValido = false;
      }

    }

    return esValido;
  },

  tipoNumeroEntero: function (valor) {
    var esValido = false;
    var expresionRegular = "^-?[0-9]+$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  },

  tipoNumeroEnteroPositivo: function (valor) {
    var esValido = false;
    var expresionRegular = "^[0-9]*$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  },

  tipoNumeroEnteroPositivoMayorCero: function (valor) {
    var esValido = false;
    var expresionRegular = "^([1-9])([0-9]*)$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  },

  tipoNumeroDecimal: function (valor) {
    var esValido = false;
    var expresionRegular = "^-?[0-9]+([.|,|/]?([0-9]+)?)$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  },

  tipoCaracteresProhibidos: function (valor) {
    var esValido = false;
    var expresionRegular = "(<|>)";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return !esValido;
  },

  tipoAlfanumerico: function (valor) {
    var esValido = false;
    var expresionRegular = "^[a-zA-Z0-9]+$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  },

  tipoRut: function (valor) {
    var esValido = false;

    var verificaDV = function (iRut) {
      var digit, ind, Lar, Suma, RutAux, DigCal, fac;

      DigCal = "";
      RutAux = iRut;
      Lar = RutAux.length;
      if (Lar < 9) {
        var Cad = "000000000";
        RutAux = Cad.substr(1, 9 - Lar) + RutAux;
      }
      fac = "432765432";
      Suma = 0;
      ind = 9;
      while (ind > 1) {
        Pa = RutAux.substr(ind - 1, 1);
        Pb = fac.substr(ind - 1, 1);
        Suma = parseFloat(Suma) + (parseFloat(Pa) * parseFloat(Pb));
        ind = ind - 1;
      }
      digit = 11 - (Suma - parseInt(Suma / 11) * 11);
      if (digit == 10) {
        DigCal = "K";
      }
      if (digit != 10) {
        if (digit == 11) {
          DigCal = 0;
        }
        if (digit != 11) {
          DigCal = (digit);
        }
      }
      return DigCal;
    };

    //se quita espacios, puntos y gui�n
    var er = new RegExp();
    er = / |-|\./ig;
    var rut = valor.replace(er, "");

    //valida que el largo del rut sea de 9 digitos, incluyendo el digito Verificador
    if (rut.length > 9) {
      esValido = false;
    } else {
      var dv = rut.substring(rut.length - 1, rut.length);
      if (isNaN(dv)) { dv = dv.toUpperCase(); }
      var parteNumerica = rut.substring(0, rut.length - 1);

      if (verificaDV(parteNumerica) == dv) {
        esValido = true;
      } else {
        esValido = false;
      };

    }

    return esValido;
  },

  tipoLargo: function (valor, opciones) {
    var esValido = false;
    var minimo = opciones.extras.largo_minimo;
    var maximo = opciones.extras.largo_maximo;
    var largo = valor.length;

    if (maximo == "") maximo = 0;
    if (minimo < 1) minimo = valor.length;
    if (maximo < 1) maximo = valor.length;

    if (largo >= minimo && largo <= maximo) {
      esValido = true;
    } else {
      esValido = false;
    }

    return esValido;
  },

  tipoPatente: function (valor) {
    var esValido = false;
    var expresionRegular = "^([a-zA-Z]{2}[0-9]{2}[0-9]{2}|[b-df-hj-lpr-tv-zB-DF-HJ-LPR-TV-Z]{2}[b-df-hj-lpr-tv-zB-DF-HJ-LPR-TV-Z]{2}[0-9]{2})$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  },

  tipoArchivoImagen: function (valor) {
    var esValido = false;
    var extension = Validacion.obtenerExtensionArchivo(valor);
    var tipoArchivo = Validacion.obtenerTipoArchivo(extension);

    //determina si es un tipo imagen o no
    if (tipoArchivo == "image") {
      esValido = true;
    }
    else {
      esValido = false;
    }

    return esValido;
  },

  tipoArchivoExtension: function (valor, opciones) {
    var esValido = false;
    var extensionArchivo = Validacion.obtenerExtensionArchivo(valor);
    var extension = opciones.extras.extension;

    //determina si es un tipo imagen o no
    if (extensionArchivo.toLowerCase() == extension.toLowerCase()) {
      esValido = true;
    }
    else {
      esValido = false;
    }

    return esValido;
  },

  tipoRangoNumerico: function (valor, opciones) {
    var esValido = false;
    var esNumerico = false;
    var minimo = opciones.extras.rango_minimo;
    var maximo = opciones.extras.rango_maximo;

    esNumerico = Validacion.tipoNumeroEntero(valor);

    if (!esNumerico) {
      esValido = false;
    } else {
      if (valor >= minimo && valor <= maximo) {
        esValido = true;
      } else {
        esValido = false;
      }
    }

    return esValido;
  },

  tipoNumeroFormatoAmericano: function (valor) {
    var esValido = false;
    var expresionRegular = "^(\\d|-)?(\\d|,)*\\.?\\d*$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  },

  tipoFechaDentroRangoHoy: function (tipoValidacion, opciones) {
    var esValido = true;
    var fechaHoy = new Date();

    if (tipoValidacion == "fecha-mayor-entre-ambas") {
      var fechaInicio = opciones.extras.fechaInicio;
      var horaInicio = opciones.extras.horaInicio;
      var objFechaInicio = Validacion.obtenerObjetoFecha(fechaInicio, horaInicio);
      var fechaTermino = opciones.extras.fechaTermino;
      var horaTermino = opciones.extras.horaTermino;
      var objFechaTermino = Validacion.obtenerObjetoFecha(fechaTermino, horaTermino);
    } else {
      var fecha = opciones.extras.fecha;
      var hora = opciones.extras.hora;
      var objFecha = Validacion.obtenerObjetoFecha(fecha, hora);
    }

    switch (tipoValidacion) {
      case "fecha-mayor-hoy":
        if (objFecha > fechaHoy) {
          esValido = false;
        }
        break;
      case "fecha-menor-hoy":
        if (objFecha < fechaHoy) {
          esValido = false;
        }
        break;
      case "fecha-mayor-entre-ambas":
        if (objFechaInicio > objFechaTermino) {
          esValido = false;
        }
        break;
      default:
        esValido = true;
        break;
    }

    return esValido;
  },

  tipoHora: function (valor) {
    var esValido = false;
    var expresionRegular = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
    esValido = Validacion.testearExpresionRegular(valor, expresionRegular);

    return esValido;
  }

};
﻿var Perfil = {

  validarFormularioBusqueda: function () {
    try {
      var jsonValidarCampos = [
		      { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroNombre", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroLlave", requerido: false, tipo_validacion: "caracteres-prohibidos" }
		    , { elemento_a_validar: Sistema.PREFIJO_CONTROL + "txtFiltroRegistrosPorPagina", requerido: false, tipo_validacion: "numero-entero-positivo" }
      ];
      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Perfil.validarFormularioBusqueda\n" + e);
      return false;
    }
  },

  abrirFormulario: function (opciones) {
    try {
      var queryString = "";
      var idPerfil = opciones.idPerfil;

      queryString += "?id=" + idPerfil;

      jQuery.fancybox({
        width: "100%",
        height: "80%",
        modal: false,
        type: "iframe",
        href: "../page/Mantenedor.PerfilDetalle.aspx" + queryString
      });
    } catch (e) {
      alert("Exception Perfil.abrirFormulario:\n" + e);
    }
  },

  cerrarPopUp: function (valorCargarDesdePopUp) {
    try {
      if (valorCargarDesdePopUp == "1") {
        parent.Perfil.cargarDesdePopUp(valorCargarDesdePopUp);
        parent.document.forms[0].submit();
      }
      parent.jQuery.fancybox.close();

    } catch (e) {
      alert("Exception Perfil.cerrarPopUp:\n" + e);
    }

  },

  cargarDesdePopUp: function (valor) {
    try {
      var txtCargaDesdePopUp = document.getElementById(Sistema.PREFIJO_CONTROL + "txtCargaDesdePopUp");
      txtCargaDesdePopUp.value = valor;
    } catch (e) {
      alert("Exception Perfil.cargarDesdePopUp:\n" + e);
    }
  },

  validarFormularioPerfil: function () {
    var item;

    try {
      var existeNombrePerfil = Perfil.existeNombrePerfil();
      var existeLlavePerfil = Perfil.existeLlavePerfil();

      var jsonValidarCampos = [
          { elemento_a_validar: "txtNombre", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtLlave", requerido: true, tipo_validacion: "caracteres-prohibidos" }
        , { elemento_a_validar: "txtPaginaInicio", requerido: false, tipo_validacion: "caracteres-prohibidos" }
      ];

      //valida que el nombre del perfil sea unico
      if (jQuery("#txtNombre").val() != "") {
        item = {
          valor_a_validar: existeNombrePerfil,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorNombre",
          mensaje_validacion: "El nombre ya existe"
        };
        jsonValidarCampos.push(item);
      }

      //valida que la llave del perfil sea unico
      if (jQuery("#txtLlave").val() != "") {
        item = {
          valor_a_validar: existeLlavePerfil,
          requerido: false,
          tipo_validacion: "numero-entero-positivo",
          contenedor_mensaje_validacion: "hErrorLlave",
          mensaje_validacion: "La llave ya existe"
        };
        jsonValidarCampos.push(item);
      }

      var esValido = Validacion.validarControles(jsonValidarCampos);

      if (!esValido) {
        Sistema.alertaMensajeError();
        return false;
      } else {
        return true;
      }

    } catch (e) {
      alert("Exception: Perfil.validarFormularioPerfil\n" + e);
      return false;
    }

  },

  existeNombrePerfil: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var nombrePerfil = jQuery("#txtNombre").val();
      var idPerfil = jQuery("#txtIdPerfil").val();
      queryString += "op=Perfil.Nombre";
      queryString += "&id=" + idPerfil;
      queryString += "&valor=" + Sistema.urlEncode(nombrePerfil);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar el nombre único del perfil");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar el nombre único del perfil");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Perfil.existeNombrePerfil:\n" + e);
    }

    return existe;
  },

  existeLlavePerfil: function () {
    var queryString = "";
    var existe = "-1";

    try {
      var llavePerfil = jQuery("#txtLlave").val();
      var idPerfil = jQuery("#txtIdPerfil").val();
      queryString += "op=Perfil.Llave";
      queryString += "&id=" + idPerfil;
      queryString += "&valor=" + Sistema.urlEncode(llavePerfil);

      var okFunc = function (t) {
        var respuesta = jQuery.trim(t);
        if (Sistema.contieneTextoTerminoSesion(respuesta)) {
          Sistema.redireccionarLogin();
        } else if (respuesta == Sistema.CONST_CODIGO_SQL_ERROR) {
          alert("Ha ocurrido un error interno y no se pudo validar la llave del perfil");
        } else {
          existe = respuesta;
        }
      }

      var errFunc = function (t) {
        alert("Ha ocurrido un error interno y no se pudo validar la llave del perfil");
      }

      jQuery.ajax({
        url: "../webAjax/waComprobarDatoDuplicado.aspx",
        type: "post",
        async: false,
        data: queryString,
        success: okFunc,
        error: errFunc
      });
    } catch (e) {
      alert("Exception Perfil.existeLlavePerfil:\n" + e);
    }

    return existe;
  }

};
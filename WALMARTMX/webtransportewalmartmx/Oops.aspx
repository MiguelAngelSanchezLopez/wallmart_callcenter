﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Oops.aspx.vb" Inherits="webtransportewalmartmx.Oops" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	  <meta http-equiv="Cache-Control" content="no-cache" />
	  <meta http-equiv="Pragma" content="no-cache" />
	  <meta http-equiv="Expires" content="-1" />
    <title>Oops!</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap-cerulean.css"  />
</head>
<body>
  <form id="form1" runat="server">
    <div class="container">
      <div class="form-group">&nbsp;</div>
      <div class="form-group">
        <div class="jumbotron">
          <h1><img src="img/oops.png" alt="logo" /> Disculpe!</h1>
          <p>
          ... pero ocurrió un problema al procesar su solicitud. El problema ha sido registrado y notificado para que sea solucionado a la brevedad.
          Lamentamos los inconvenientes que esto pueda haberle causado.<br /><br />
          Puede <a href="#" onclick="javascript:history.back(-1)">volver atrás</a> e intentarlo nuevamente.<br />
          Tambi&eacute;n puede comunicarse con nosotros a <a href="mailto:soporte@alto.cl">soporte@alto.cl</a><br /><br />
          </p>
        </div>
      </div>

      <div class="form-form-group">
        <div id="DivDebug" style="width:800px; text-align: left;" runat="server">
          <strong><asp:Label ID="lblErrorTitulo" runat="server"></asp:Label></strong>&nbsp;<asp:Label ID="lblErrorFecha" runat="server"></asp:Label><br />
          <br />
          <strong>Mensaje:</strong>&nbsp;<asp:Label ID="lblErrorMensajeError" runat="server"></asp:Label><br />
          <br />
          <strong>Origen:</strong>&nbsp;<asp:Label ID="lblErrorURLOrigen" runat="server"></asp:Label><br />
          <br />
          <strong>Stack:</strong>&nbsp;<asp:Label ID="lblErrorStack" runat="server"></asp:Label><br />
          <br />
          Trace:
          <asp:HyperLink ID="lnkTrace" runat="server">HyperLink</asp:HyperLink><br />
        </div>
      </div>
    </div>
  </form>
</body>
</html>


﻿Imports CapaNegocio
Imports System.Data

Public Class login
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim ambienteEjecucion As String
    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    ' vemos si viene algun mensaje de error
    Dim mensaje As String = Request.QueryString("m")

    If Not mensaje Is Nothing Then
      ' desencriptamos el mensaje (que es un codigo, no el mensaje mismo) y mostramos que significa
      Select Case Criptografia.DesencriptarTripleDES(mensaje)
        Case "EXP"
          Me.pnlMensajeError.Visible = True
          Me.lblMensaje.Text = "Su sesión ha expirado. Por favor ingrese nuevamente"
        Case Else
          Me.lblMensaje.Text = String.Empty
      End Select
    End If

    ' luego siempre matamos cualquier sesion
    If Not IsPostBack Then
      Session.Clear()
      Session.Abandon()
      FormsAuthentication.SignOut()
    End If

    'escribe donde se esta ejecutando el sitio
    ambienteEjecucion = Utilidades.ObtenerAmbienteEjecucion()
    If (InStr(ambienteEjecucion, Utilidades.eAmbienteEjecucion.Produccion.ToString.ToUpper)) = 0 Then
      Me.lblAmbienteEjecucion.Text = ".:: SITIO " & ambienteEjecucion & "&nbsp;(" & Utilidades.PaisSitio().ToString.ToUpper & ") ::."
    End If

    Me.txtUsername.Attributes.Add("placeholder", "Usuario")
    Me.txtPassword.Attributes.Add("placeholder", "Clave")
    Me.lblVersionSistema.Text = "v" & Utilidades.VersionSistema()

  End Sub

  Protected Sub btnIngresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIngresar.Click
    Dim oUsuario As New Usuario
    Dim resultado As Usuario.ResultadoLogin

    oUsuario.Username = Utilidades.LimpiarInput(txtUsername.Text.Trim)
    oUsuario.Password = Utilidades.LimpiarInput(txtPassword.Text.Trim)

    ' encriptar
    oUsuario.Password = Criptografia.obtenerSHA1(oUsuario.Password)
    Try
      resultado = oUsuario.Login()
      oUsuario.IpUltimoAcceso = Request.ServerVariables("REMOTE_ADDR")

      Select Case resultado
        Case Usuario.ResultadoLogin.UsernameNoExiste, Usuario.ResultadoLogin.ClaveIncorrecta
          Me.pnlMensajeError.Visible = True
          lblMensaje.Text = "Entrada no válida"
        Case Usuario.ResultadoLogin.Existe
          ' si el usuario no puede conectarse este dia lo hechamos pa' fuera
          If Not oUsuario.PuedeLoguearse(Date.Today.DayOfWeek) Then
            Me.pnlMensajeError.Visible = True
            lblMensaje.Text = "Entrada no permitida"
            Exit Sub
          End If

          Dim oUtilidades As New Utilidades
          'graba objetos en session
          oUtilidades.InicializarSesion(oUsuario)
          'obtiene la matriz de permisos asociadas al usuario
          Dim dsPermisos As DataSet = Usuario.ObtenerMatrizPermisos(oUsuario.Id, oUsuario.Super)
          oUtilidades.GrabarDataSetSession(Usuario.SESION_MATRIZ_PERMISO, dsPermisos)

          EventLogHandler.EscribirEvento("Usuario Logueado!" & vbCrLf & "Username " & oUsuario.Username & vbCrLf & "Ip " & oUsuario.IpUltimoAcceso, Diagnostics.EventLogEntryType.SuccessAudit, EventLogHandler.EVENTLOG_ALTO)
          Sistema.GrabarLogSesion(oUsuario.Id, "Inicia sesión")

          Response.Redirect("~/" & Session("PaginaDeInicio"), False)
          HttpContext.Current.ApplicationInstance.CompleteRequest()

          'graba log
          Sistema.EscribirLog4Net("Usuario Logueado!, Username: " & oUsuario.Username & ", IP: " & oUsuario.IpUltimoAcceso, Conexion.eOrigenDatos.SistemaBase, Sistema.eLog4NetTipoAplicacion.Web, Sistema.eLog4NetNivelVerbosidad.Info)
      End Select
    Catch ex As Exception
      Dim dummy As String = ""
      Dim result As EventLogHandler.EstadosEventLog = EventLogHandler.EscribirEvento(ex, Diagnostics.EventLogEntryType.Error, EventLogHandler.EVENTLOG_ALTO)
      Select Case result
        Case EventLogHandler.EstadosEventLog.ErrorEscribiendo
          dummy &= "ELEE#"
        Case EventLogHandler.EstadosEventLog.EventSourceNoExiste
          dummy &= "ELSNE#"
      End Select


      ' obtenemos el (o los) correo para las notificaciones
      Dim ErrorEmailNotificaciones As String() = Split(System.Configuration.ConfigurationManager.AppSettings("paginaErrorEmailNotificaciones"), ";")
      Dim ColeccionEmails As New System.Net.Mail.MailAddressCollection
      For Each direccion As String In ErrorEmailNotificaciones
        If Utilidades.EsEmail(direccion) Then ColeccionEmails.Add(direccion)
      Next

      If Not Utilidades.EmailError(ex, ColeccionEmails) Then dummy = "NM#"
      Me.pnlMensajeError.Visible = True
      lblMensaje.Text = "Ocurrió un error desconocido. Comuniquese con ALTO <br /><div style=""color: white"">" & dummy & "</div><br />"
    End Try
  End Sub

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Scat.ValidacionSalidaFinal.aspx.vb" Inherits="webtransportewalmartmx.Scat_ValidacionSalidaFinal" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
  <script src="../js/jquery.select2.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Validar Salida Final</p>
      </div>
    </div>
  </div>
  <asp:Panel ID="pnlMensaje" runat="server">
  </asp:Panel>
  <div>
    <asp:Panel ID="pnlMensajeAcceso" runat="server">
    </asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="form-group">
      <div class="row">
        <div class="col-xs-6">
          <div class="panel panel-primary">
            <div class="panel-heading">
          <h3 class="panel-title">CONTROL OPERADOR</h3>
        </div>
            <div class="panel-body">
              <div class="form-group">
              <div class="row">
              <div class="col-xs-6">
                <label class="control-label">Nombre Operador<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtNombreConductor" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
               <div class="col-xs-6">
                 <label class="control-label">Rut<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtRutConductor" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
                <span class="help-block">(sin puntos ej:12345678-K)</span>
              </div>
            </div>
            </div>
            </div>
          </div>     
        </div>
        <div class="col-xs-6">
          <div class="panel panel-primary">
          <div class="panel-heading">
          <h3 class="panel-title">Información Ruta</h3>
          </div>
          <div class="panel-body">
              <div class="form-group">
              <div class="row">
              <div class="col-xs-4">
                <label class="control-label">IdMaster<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
               <div class="col-xs-4">
                <label class="control-label">Nro Sello<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtNroSello" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
                <div class="col-xs-4">
                <label class="control-label">Nombre Despachador<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtNombreDespachador" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox> 
              </div>
            </div>
            </div>
            </div>
          </div>     
        </div>
      </div>
    </div>
      
      <div class="panel panel-primary">
          <div class="panel-heading">
          <h3 class="panel-title">Control Camión</h3>
        </div>
          <div class="panel-body">
            <div class="form-group">
              <div class="row">
              <div class="col-xs-3">
                <label class="control-label">L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox1" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
              <div class="col-xs-3">
                 <label class="control-label">Rut L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox2" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
                <span class="help-block">(sin puntos ej:12345678-K)</span>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Fecha Salida<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtFechaEntrada" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Hora Salida<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox4" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
                <span class="help-block">(ejm: 09:00)</span>
              </div>
              </div>
              <div class="row">
              <div class="col-xs-3">
                <label class="control-label">Placa Tracto<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox3" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
              <div class="col-xs-3">
                <label class="control-label">Señal Satelital Tracto<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox5" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
                 <div class="col-xs-3">
                <label class="control-label">Placa Remolque<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox8" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
              <div class="col-xs-3">
                <label class="control-label">Señal Satelital Remolque<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox9" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
            </div>
            </div>
          </div>
       </div> 

      <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Información de Carga</h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
             <div class="row">
              <div class="col-xs-3">
                  <label class="control-label">Tipo Envio<strong class="text-danger">&nbsp;*</strong></label>
                  <uc1:wucCombo ID="ddlTipoEnvio" runat="server" FuenteDatos="TipoGeneral" TipoCombo="Scat.TipoEnvio" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>                   
                </div>
              <div class="col-xs-3">
                  <label class="control-label">Ingreso Errores<strong class="text-danger">&nbsp;*</strong></label>
                  <uc1:wucCombo ID="ddlIngresoErrores" runat="server" FuenteDatos="TipoGeneral" TipoCombo="Scat.IngresoErrores" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>                   
                </div>
              <div class="col-xs-2">
                <label class="control-label">Temperatura Despacho<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox6" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
              <div class="col-xs-2">
                <label class="control-label">Temperatura Actual<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox7" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
              <div class="col-xs-2">
                <label class="control-label">Control Balanza<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox10" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
              </div>
            </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-8">
                  <label class="control-label">Comentario<strong class="text-danger">&nbsp;*</strong></label>
                  <asp:TextBox ID="TextBox11" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>      
                </div>
              </div>
            </div>
          </div>
       </div> 
    </asp:Panel>
  </div>

  <div class="form-group text-center">
    <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="#"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
    <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="#"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
   </div>
</asp:Content>

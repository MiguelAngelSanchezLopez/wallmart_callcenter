﻿Imports System.Web

Public Class FileUploadHandler
  Implements System.Web.IHttpHandler

  Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

    If (context.Request.Files.Count > 0) Then

      Try

        Dim files As HttpFileCollection = context.Request.Files
        'For i As Integer = 0 To files.Count - 1
        Dim file As HttpPostedFile = files(0)
        Dim filePath As String = context.Server.MapPath("~/fileTemp/" + file.FileName)
        file.SaveAs(filePath)
        'Next

        context.Response.ContentType = "text/plain"
        context.Response.Write("true")

      Catch ex As Exception
        context.Response.ContentType = "text/plain"
        context.Response.Write("false")
      End Try

    End If

  End Sub

  ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
    Get
      Return False
    End Get
  End Property
End Class
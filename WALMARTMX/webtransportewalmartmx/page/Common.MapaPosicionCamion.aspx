﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Common.MapaPosicionCamion.aspx.vb" Inherits="webtransportewalmartmx.Common_MapaPosicionCamion" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Agregar Contacto</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY"></script>-->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8"></script>
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/trazaviaje.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">
          <div>
            <div id="hMapaCanvas" runat="server" class="sist-map-canvas-donde-esta-camion">
              <div class="text-center">
                <div class="sist-padding-top-20"></div>
                <div class="form-group"><img src="../img/sin_mapa.png" alt="" /></div>
              </div>                  
            </div>
          </div>

          <div>
            <div class="form-group">
              <asp:Literal ID="ltlTablaDatos" runat="server"></asp:Literal>
            </div>
          </div> 

          <asp:Panel ID="pnlFormulario" runat="server">
            <div class="panel panel-primary">
              <div class="panel-heading"><h3 class="panel-title">DATOS RECEPCION CAMION</h3></div>
              <div class="panel-body">
                <div class="form-group row">
                  <div class="col-xs-4">
                    <label class="control-label">Estado recepci&oacute;n del cami&oacute;n</label>
                    <uc1:wucCombo id="ddlEstadoRecepcionCamion" runat="server" FuenteDatos="TipoGeneral" TipoCombo="EstadoRecepcionCamion" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
                  </div>
                  <div class="col-xs-8">
                    <label class="control-label">Observaci&oacute;n</label>
                    <asp:TextBox ID="txtObservacion" runat="server" MaxLength="4000" CssClass="form-control"></asp:TextBox>
                  </div>
                </div>
                
                <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
                  <div class="form-group h4"><strong>Historial de recepci&oacute;n del cami&oacute;n</strong></div>
                  <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>
                  <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="False" AllowSorting="False" CssClass="table table-striped table-bordered small">
                    <Columns>
                      <asp:TemplateField HeaderText="#">
                        <HeaderStyle Width="40px" />
                        <ItemTemplate>
                          <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="NroTransporte" HeaderText="IdMaster" HeaderStyle-Width="100px" />
                      <asp:BoundField DataField="PatenteTracto" HeaderText="Placa Tracto" HeaderStyle-Width="110px" />
                      <asp:BoundField DataField="FechaCreacion" HeaderText="Fecha Creaci&oacute;n" HeaderStyle-Width="150px" />
                      <asp:BoundField DataField="CreadoPor" HeaderText="Creado Por" />
                      <asp:BoundField DataField="Estado" HeaderText="Estado" HeaderStyle-Width="130px" />
                      <asp:BoundField DataField="Observacion" HeaderText="Observaci&oacute;n" />
                    </Columns>
                  </asp:GridView>
                </asp:Panel>      

              </div>
            </div>
          </asp:Panel>

        </asp:Panel>

        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="TrazaViaje.cerrarPopUp()"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(TrazaViaje.validarFormularioMapaPosicionCamion())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".chosen-select").select2();

      });
    </script>

  </form>
</body>
</html>

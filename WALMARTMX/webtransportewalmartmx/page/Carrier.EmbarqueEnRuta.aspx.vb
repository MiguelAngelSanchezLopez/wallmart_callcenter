﻿Imports System.Web.Script.Serialization
Imports CapaNegocio
Imports Syncfusion.Windows.Forms.Chart.SvgBase

Public Class Carrier_EmbarqueEnRuta
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim semaforo As String = Request("semaforo")
    Dim jsonString As String = Request("json")
    Dim jss As New JavaScriptSerializer
    Dim json As Object = jss.Deserialize(Of Object)(jsonString)

    'Cabecera
    ''''''''''''''''''''''''''''''''''''''
    If (semaforo = "ROJO") Then
      semaforo = "<div class=""text-danger""><img src=""../img/bullet_red.png"" alt="""" /> " + json.item("IdEmbarque") + "</div>"
    Else
      semaforo = "<div class=""text-success""><img src=""../img/bullet_green.png"" alt="""" /> " + json.item("IdEmbarque") + "</div>"
    End If

    lblSemaforo.Text = semaforo
    lblIdMaster.Text = json.item("IdMaster")
    lblCedis.Text = json.item("NombreCompletoCedis")
    lblPlacaRemolque.Text = json.item("PlacaRemolque")
    lblPlacaTracto.Text = json.item("PlacaTracto")
    lblDeterminante.Text = json.item("Determinante")
    lblTiempoViaje.Text = json.item("TiempoViaje")
    lblEta.Text = json.item("TiempoEstimadoArribo")
    lblKilometrosRecorridos.Text = json.item("KilometrosRecorridos")
    lblTotalAlertas.Text = json.item("TotalAlertas")

    'Detalle
    ''''''''''''''''''''''''''''''''''''''
    cargarGrillaAlertas(json.item("IdMaster"), json.item("IdEmbarque"))

  End Sub

  Private Sub cargarGrillaAlertas(ByVal idMaster As Long, ByVal idEmbarque As Long)

    Dim dt As DataTable = Carrier.GetDetalleAlertaEmbarqueEnRuta(idMaster, idEmbarque)

    If (Not dt Is Nothing AndAlso dt.Rows.Count > 0) Then
      gvPrincipal.DataSource = dt
      gvPrincipal.DataBind()

      Utilidades.MostrarTotalRegistrosGrilla(dt.DefaultView, lblRegistros)

    Else
      lblRegistros.Text = "No se encontraron registros."
    End If

  End Sub



  Protected Sub gvPrincipal_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPrincipal.RowDataBound

    If (e.Row.RowType = DataControlRowType.DataRow) Then
      Dim lat As String = gvPrincipal.DataKeys(e.Row.RowIndex).Values("Lat")
      Dim lon As String = gvPrincipal.DataKeys(e.Row.RowIndex).Values("Lon")

      Dim btn As HtmlGenericControl = e.Row.FindControl("btnVerMapa")
      btn.Attributes.Add("onclick", "return(Carrier.abrirAlertaMapa('" + lat + "','" + lon + "'));")
    End If

  End Sub
End Class
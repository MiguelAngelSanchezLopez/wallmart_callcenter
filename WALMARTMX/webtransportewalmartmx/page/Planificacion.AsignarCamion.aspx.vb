﻿Imports CapaNegocio
Imports System.Data

Public Class Planificacion_AsignarCamion
  Inherits System.Web.UI.Page

#Region "Enum"
  Private NOMBRE_PAGINA As String = Utilidades.ObtenerNombrePaginaActual()
  Private RECARGAR_PAGINA As String = NOMBRE_PAGINA & "_RECARGAR"

  Private Enum eFunciones
    Ver
  End Enum

  Private Enum eTabla As Integer
    T00_ListadoPlanificacion = 0
    T01_Locales = 1
  End Enum
#End Region

#Region "Metodo Controles ASP"
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim cargadoDesdePopUp As String = Utilidades.IsNull(CType(Session(RECARGAR_PAGINA), String), "0")

    Me.ViewState.Add("idUsuarioTransportista", oUsuario.Id)

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    If cargadoDesdePopUp = "1" Then
      ObtenerDatos()
      ActualizaInterfaz()
    End If

    MostrarMensajeUsuario()
    Session(RECARGAR_PAGINA) = "0"
  End Sub

  Protected Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    GrabarFiltrosEnSesion()
    ObtenerDatos()
    ActualizaInterfaz()
    Sistema.GrabarLogSesion(oUsuario.Id, "Busca planificaciones")
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    If textoMensaje = "" Then
      textoMensaje &= GrabarDatosRegistro(status)
    End If

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "modificado"
          textoMensaje = "{MODIFICADO}Los datos fueron actualizados satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudieron actualizar los datos. Intente nuevamente por favor"
      End Select
    Else
      Select Case status
        Case "bloqueado"
          textoMensaje = "{ERROR}" & textoMensaje
        Case Else
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudieron actualizar los datos. Intente nuevamente por favor.<br />" & textoMensaje
      End Select
    End If

    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje
    Session(RECARGAR_PAGINA) = "1"
    Response.Redirect("./Planificacion.AsignarCamion.aspx")
  End Sub

  Protected Sub btnNotificarAsignacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNotificarAsignacion.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    textoMensaje &= NotificarAsignacion(status)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "modificado"
          textoMensaje = "{MODIFICADO}La asignaci&oacute;n de camiones fue notificada satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se notificar la asignaci&oacute;n de camiones. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se notificar la asignaci&oacute;n de camiones. Intente nuevamente por favor.<br />" & textoMensaje
    End If

    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje
    Session(RECARGAR_PAGINA) = "1"
    Response.Redirect("./Planificacion.AsignarCamion.aspx")
  End Sub

#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim dcFiltros As Dictionary(Of String, String) = CType(Session(NOMBRE_PAGINA), Dictionary(Of String, String))
    Dim cargadoDesdePopUp As String = Utilidades.IsNull(CType(Session(RECARGAR_PAGINA), String), "0")
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario, nroTransporte, fechaPresentacion, codLocal, horaPresentacion, minutoPresentacion As String

    If (cargadoDesdePopUp = "1") Then
      If (Not dcFiltros Is Nothing) Then
        Me.txtNroTransporte.Text = dcFiltros.Item("nroTransporte")
        Me.txtFechaPresentacion.Text = dcFiltros.Item("fechaPresentacion")
      End If
    End If

    'obtiene valores de los controles
    idUsuario = oUsuario.Id
    nroTransporte = Utilidades.IsNull(Me.txtNroTransporte.Text, "-1")
    fechaPresentacion = Utilidades.IsNull(Me.txtFechaPresentacion.Text, "-1")
    codLocal = Utilidades.IsNull(Me.ddlLocal.SelectedValue, "-1")
    horaPresentacion = Utilidades.IsNull(Me.wucHoraInicio.SelectedValueHora, "-1")
    minutoPresentacion = Utilidades.IsNull(Me.wucHoraInicio.SelectedValueMinutos, "-1")

    'obtiene listado
    ds = PlanificacionTransportista.ObtenerListado(idUsuario, nroTransporte, fechaPresentacion, "-1", "-1", "-1", codLocal, horaPresentacion, minutoPresentacion)

    'guarda el resultado en session
    Me.ViewState.Add("ds", ds)
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim dv As New DataView
    Dim tablaDatos As String

    tablaDatos = DibujarTablaDatos()
    Me.ltlTablaDatos.Text = tablaDatos
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    'verifica los permisos sobre los controles
    VerificarPermisos()

    Me.wucHoraInicio.SelectedValueHora = ""
    Me.wucHoraInicio.SelectedValueMinutos = ""
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensaje.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' dibuja tabla con la planificacion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarTablaDatos() As String
    Dim html As String = ""
    Dim template As String = ""
    Dim ds As New DataSet
    Dim dsCombo As New DataSet
    Dim dt As New DataTable
    Dim totalRegistros As Integer
    Dim sb As New StringBuilder
    Dim templateAux, nroTransporte, fechaPresentacion, carga, tipoCamion, patenteTracto, nombreConductor, locales, successPatenteTracto, successPatenteTrailer, successCriterio, successObservacion, templateSelect As String
    Dim successNombreConductor, successCelular, successRutConductor, idPlanificacion, patenteTrailer, celular, rutConductor, cerradoPorTransportista, cerradoPorTransportistaAux, criterio, observacion As String
    Dim disabledPatenteTracto, disabledPatenteTrailer, disabledNombreConductor, disabledCelular, disabledRutConductor, disabledCriterio, disabledObservacion, camion As String
    Dim htmlSinRegistro As String = "<em>Sin dato</em>"

    Try
      ds = Me.ViewState.Item("ds")

      template &= "<tr>"
      template &= "  <td>{FECHA_PRESENTACION}</td>"
      template &= "  <td><span data-target=""nro-transporte"" data-prefijoControl=""{PREFIJO_CONTROL}"">{NRO_TRANSPORTE}</span></td>"
      template &= "  <td>{LOCALES}</td>"
      template &= "  <td>{CAMION}</td>"
      template &= "  <td><input type=""text"" id=""{PREFIJO_CONTROL}_txtPatenteTracto"" value=""{PATENTE_TRACTO}"" class=""form-control input-sm {SUCCESS_PATENTE_TRACTO}"" MaxLength=""6"" {DISABLED_PATENTE_TRACTO} /><div id=""{PREFIJO_CONTROL}_divError""></div><span id=""{PREFIJO_CONTROL}_hErrorPTracto"" runat=""server""></span></td>"
      template &= "  <td><input type=""text"" id=""{PREFIJO_CONTROL}_txtPatenteTrailer"" value=""{PATENTE_TRAILER}"" class=""form-control input-sm {SUCCESS_PATENTE_TRAILER}"" MaxLength=""6"" {DISABLED_PATENTE_TRAILER} /><span id=""{PREFIJO_CONTROL}_hErrorPTrailer"" runat=""server""></span></td>"
      template &= "  <td><input type=""text"" id=""{PREFIJO_CONTROL}_txtNombreConductor"" value=""{NOMBRE_CONDUCTOR}"" class=""form-control input-sm {SUCCESS_NOMBRE_CONDUCTOR}"" data-action=""autocomplete"" data-prefijoControl=""{PREFIJO_CONTROL}"" {DISABLED_NOMBRE_CONDUCTOR} /></td>"
      template &= "  <td><input type=""text"" id=""{PREFIJO_CONTROL}_txtRutConductor"" value=""{RUT_CONDUCTOR}"" class=""form-control input-sm {SUCCESS_RUT_CONDUCTOR}"" data-action=""autocomplete"" data-prefijoControl=""{PREFIJO_CONTROL}"" {DISABLED_RUT_CONDUCTOR} /></td>"
      template &= "  <td><input type=""text"" id=""{PREFIJO_CONTROL}_txtCelular"" value=""{CELULAR}"" class=""form-control input-sm {SUCCESS_CELULAR}"" {DISABLED_CELULAR} /></td>"
      template &= "  <td>{SELECT_CRITERIO}</td>"
      template &= "  <td><input type=""text"" id=""{PREFIJO_CONTROL}_txtObservacion"" value=""{OBSERVACION}"" class=""form-control input-sm {SUCCESS_OBSERVACION}"" {DISABLED_OBSERVACION} MaxLength=""500""/></td>"

      template &= "</tr>"

      dt = ds.Tables(eTabla.T00_ListadoPlanificacion)
      totalRegistros = dt.Rows.Count

      If (totalRegistros = 0) Then
        Me.pnlBotones.Visible = False
        html = Utilidades.MostrarMensajeUsuario("No hay planificaciones asociadas", Utilidades.eTipoMensajeAlert.Warning, True)
      Else
        sb.Append("<div class=""small"">")
        sb.Append("<table class=""table table-condensed table-bordered"">")
        sb.Append("  <thead>")
        sb.Append("    <tr>")
        sb.Append("      <th style=""width:130px"">Fecha Presentaci&oacute;n</th>")
        sb.Append("      <th style=""width:70px"">IdMaster</th>")
        sb.Append("      <th>Tienda</th>")
        sb.Append("      <th style=""width:120px"">Cami&oacute;n</th>")
        sb.Append("      <th style=""width:80px"">Tracto</th>")
        sb.Append("      <th style=""width:80px"">Remolque</th>")
        sb.Append("      <th style=""width:100px"">Operador</th>")
        sb.Append("      <th style=""width:100px"">Rut Operador</th>")
        sb.Append("      <th style=""width:90px"">Celular</th>")
        sb.Append("      <th style=""width:220px"">Criterio</th>")
        sb.Append("      <th style=""width:200px"">Observación</th>")
        sb.Append("    </tr>")
        sb.Append("  </thead>")
        sb.Append("  <tbody>")

        'asume que la planificacion no esta cerrada
        cerradoPorTransportista = "0"
        cerradoPorTransportistaAux = "1"

        dsCombo = Sistema.ObtenerComboDinamico("CriterioPlanificacion", "-1")

        For Each dr As DataRow In dt.Rows

          templateAux = template
          idPlanificacion = dr.Item("IdPlanificacion")
          nroTransporte = dr.Item("NroTransporte")
          fechaPresentacion = dr.Item("FechaPresentacion")
          carga = dr.Item("Carga")
          tipoCamion = dr.Item("TipoCamion")
          patenteTracto = dr.Item("PatenteTracto")
          patenteTrailer = dr.Item("PatenteTrailer")
          nombreConductor = dr.Item("NombreConductor")
          rutConductor = dr.Item("RutConductor")
          celular = dr.Item("Celular")
          cerradoPorTransportista = dr.Item("CerradoPorTransportista")
          criterio = dr.Item("criterio")
          observacion = dr.Item("observacion")
          successPatenteTracto = IIf(String.IsNullOrEmpty(patenteTracto) Or cerradoPorTransportista = "1", "", "alert-success")
          successPatenteTrailer = IIf(String.IsNullOrEmpty(patenteTrailer) Or cerradoPorTransportista = "1", "", "alert-success")
          successNombreConductor = IIf(String.IsNullOrEmpty(nombreConductor) Or cerradoPorTransportista = "1", "", "alert-success")
          successRutConductor = IIf(String.IsNullOrEmpty(rutConductor) Or cerradoPorTransportista = "1", "", "alert-success")
          successCelular = IIf(String.IsNullOrEmpty(celular) Or cerradoPorTransportista = "1", "", "alert-success")
          successCriterio = IIf(String.IsNullOrEmpty(criterio) Or cerradoPorTransportista = "1", "", "alert-success")
          successObservacion = IIf(String.IsNullOrEmpty(observacion) Or cerradoPorTransportista = "1", "", "alert-success")
          disabledPatenteTracto = IIf(cerradoPorTransportista = "0", "", "disabled=""disabled""")
          disabledPatenteTrailer = IIf(cerradoPorTransportista = "0", "", "disabled=""disabled""")
          disabledNombreConductor = IIf(cerradoPorTransportista = "0", "", "disabled=""disabled""")
          disabledRutConductor = IIf(cerradoPorTransportista = "0", "", "disabled=""disabled""")
          disabledCelular = IIf(cerradoPorTransportista = "0", "", "disabled=""disabled""")
          disabledCriterio = IIf(cerradoPorTransportista = "0", "", "disabled=""disabled""")
          disabledObservacion = IIf(cerradoPorTransportista = "0", "", "disabled=""disabled""")

          'obtiene los locales asociados al NroTransporte
          locales = ObtenerLocalesPorNroTransporte(ds, nroTransporte, idPlanificacion)

          'formatea valores
          camion = "<strong>Tipo</strong>: " & IIf(String.IsNullOrEmpty(tipoCamion), htmlSinRegistro, Server.HtmlEncode(tipoCamion))
          camion &= "<br /><strong>Carga</strong>: " & IIf(String.IsNullOrEmpty(carga), htmlSinRegistro, Server.HtmlEncode(carga))

          templateSelect = DibujarComboCriterio(dsCombo, criterio)

          templateAux = templateAux.Replace("{SELECT_CRITERIO}", templateSelect)
          templateAux = templateAux.Replace("{ID_PLANIFICACION}", idPlanificacion)
          templateAux = templateAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
          templateAux = templateAux.Replace("{FECHA_PRESENTACION}", fechaPresentacion)
          templateAux = templateAux.Replace("{CAMION}", camion)
          templateAux = templateAux.Replace("{LOCALES}", locales)
          templateAux = templateAux.Replace("{PATENTE_TRACTO}", patenteTracto)
          templateAux = templateAux.Replace("{PATENTE_TRAILER}", patenteTrailer)
          templateAux = templateAux.Replace("{NOMBRE_CONDUCTOR}", Server.HtmlEncode(nombreConductor))
          templateAux = templateAux.Replace("{RUT_CONDUCTOR}", rutConductor)
          templateAux = templateAux.Replace("{CELULAR}", celular)
          templateAux = templateAux.Replace("{OBSERVACION}", observacion)
          templateAux = templateAux.Replace("{SUCCESS_PATENTE_TRACTO}", successPatenteTracto)
          templateAux = templateAux.Replace("{SUCCESS_PATENTE_TRAILER}", successPatenteTrailer)
          templateAux = templateAux.Replace("{SUCCESS_NOMBRE_CONDUCTOR}", successNombreConductor)
          templateAux = templateAux.Replace("{SUCCESS_RUT_CONDUCTOR}", successRutConductor)
          templateAux = templateAux.Replace("{SUCCESS_CELULAR}", successCelular)
          templateAux = templateAux.Replace("{SUCCESS_CRITERIO}", successCriterio)
          templateAux = templateAux.Replace("{SUCCESS_OBSERVACION}", successObservacion)
          templateAux = templateAux.Replace("{DISABLED_PATENTE_TRACTO}", disabledPatenteTracto)
          templateAux = templateAux.Replace("{DISABLED_PATENTE_TRAILER}", disabledPatenteTrailer)
          templateAux = templateAux.Replace("{DISABLED_NOMBRE_CONDUCTOR}", disabledNombreConductor)
          templateAux = templateAux.Replace("{DISABLED_RUT_CONDUCTOR}", disabledRutConductor)
          templateAux = templateAux.Replace("{DISABLED_CELULAR}", disabledCelular)
          templateAux = templateAux.Replace("{DISABLED_CRITERIO}", disabledCriterio)
          templateAux = templateAux.Replace("{DISABLED_OBSERVACION}", disabledObservacion)
          templateAux = templateAux.Replace("{PREFIJO_CONTROL}", idPlanificacion & "_" & nroTransporte) 'dejarlo siempre al final
          sb.Append(templateAux)

          If (cerradoPorTransportista = "0") Then
            cerradoPorTransportistaAux = "0"
          End If
        Next

        sb.Append("  </tbody>")
        sb.Append("</table>")
        sb.Append("</div>")
        html = sb.ToString()

        Me.pnlBotones.Visible = IIf(cerradoPorTransportistaAux = "0", True, False)
      End If
    Catch ex As Exception
      Me.pnlBotones.Visible = False
      html = Utilidades.MostrarMensajeUsuario(ex.Message, Utilidades.eTipoMensajeAlert.Danger, False)
    End Try

    Return html
  End Function

  Private Function DibujarComboCriterio(ByVal ds As DataSet, ByVal valorSelected As String) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim texto, valor, seleccionar As String
    Dim dr As DataRow

    'dibuja combo
    sb.Append("<select id=""{PREFIJO_CONTROL}_ddlCriterio"" runat=""server"" class=""form-control chosen-select {SUCCESS_CRITERIO}"" {DISABLED_CRITERIO}>")

    Try

      For Each dr In ds.Tables(0).Rows
        texto = dr.Item("Texto")
        valor = dr.Item("Valor")
        seleccionar = dr.Item("Seleccionar")

        If (valorSelected = "") Then 'Nuevo

          If (seleccionar = "1") Then
            sb.Append("<option value=""" & valor & """ Selected>" & Server.HtmlEncode(texto) & "</option>")
          Else
            sb.Append("<option value=""" & valor & """>" & Server.HtmlEncode(texto) & "</option>")
          End If

        Else

          If (valor = valorSelected) Then 'Modificando
            sb.Append("<option value=""" & valor & """ Selected>" & Server.HtmlEncode(texto) & "</option>")
          Else
            sb.Append("<option value=""" & valor & """>" & Server.HtmlEncode(texto) & "</option>")
          End If

        End If

      Next
    Catch ex As Exception
      'no hace nada
    End Try

    sb.Append("  </select>")

    html = sb.ToString()
    Return html
  End Function

  ''' <summary>
  ''' obtiene los locales asociados al NroTransporte
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="nroTransporte"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerLocalesPorNroTransporte(ByVal ds As DataSet, ByVal nroTransporte As String, ByVal idPlanificacion As String) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim htmlSinRegistro As String = "<em class=""help-block"">No tiene tiendas asociadas</em>"
    Dim dt As DataTable
    Dim templateAux, idPlanificacionTransportista, nombreLocal, codigoLocal, ordenEntrega, json As String
    Dim sb As New StringBuilder
    Dim listadoJSON As New List(Of Dictionary(Of String, Integer))
    Dim dcIdPlanificacionTransportista As New Dictionary(Of String, Integer)

    Try
      template &= "<div>"
      template &= "  {ORDEN_ENTREGA}) {NOMBRE_LOCAL}"
      template &= "</div>"

      dt = ds.Tables(eTabla.T01_Locales)
      Dim filtroListado = From row As DataRow In dt.Rows Where
                          row.Item("NroTransporte") = nroTransporte _
                          And row.Item("IdPlanificacion") = idPlanificacion

      If (filtroListado.Count = 0) Then
        html = htmlSinRegistro
      Else
        For Each dr As DataRow In filtroListado
          dcIdPlanificacionTransportista = New Dictionary(Of String, Integer)
          templateAux = template
          idPlanificacionTransportista = dr.Item("IdPlanificacionTransportista")
          nombreLocal = dr.Item("nombreLocal")
          codigoLocal = dr.Item("codigoLocal")
          ordenEntrega = dr.Item("ordenEntrega")

          'formatea valores
          nombreLocal = nombreLocal & IIf(String.IsNullOrEmpty(codigoLocal), "", " - " & codigoLocal)

          templateAux = templateAux.Replace("{ORDEN_ENTREGA}", ordenEntrega)
          templateAux = templateAux.Replace("{NOMBRE_LOCAL}", nombreLocal)
          sb.Append(templateAux)

          dcIdPlanificacionTransportista.Add("IdPlanificacionTransportista", idPlanificacionTransportista)
          listadoJSON.Add(dcIdPlanificacionTransportista)
        Next

        json = MyJSON.ConvertObjectToJSON(listadoJSON)
        sb.Append("<input type=""hidden"" id=""{PREFIJO_CONTROL}_txtJSONIdPlanificacionTransportista"" value=""" & Server.HtmlEncode(json) & """ />")
        html = sb.ToString()
      End If
    Catch ex As Exception
      html = htmlSinRegistro
    End Try

    Return html
  End Function

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosRegistro(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim json, idPlanificacionTransportista, patenteTracto, patenteTrailer, nombreConductor, rutConductor, celular, dv, criterio, observacion As String
    Dim objJSON, row As Object
    Dim oPlanificacionTransportista As New PlanificacionTransportista

    Try
      json = Me.txtJSONAsignarCamion.Value
      objJSON = MyJSON.ConvertJSONToObject(json)

      For Each row In objJSON
        dv = ""
        idPlanificacionTransportista = MyJSON.ItemObject(row, "IdPlanificacionTransportista")
        patenteTracto = MyJSON.ItemObject(row, "PatenteTracto")
        patenteTrailer = MyJSON.ItemObject(row, "PatenteTrailer")
        nombreConductor = MyJSON.ItemObject(row, "NombreConductor")
        rutConductor = MyJSON.ItemObject(row, "RutConductor")
        celular = MyJSON.ItemObject(row, "Celular")
        criterio = MyJSON.ItemObject(row, "Criterio")
        observacion = MyJSON.ItemObject(row, "Observacion")

        'formatea valores
        patenteTracto = patenteTracto.Trim()
        patenteTrailer = patenteTrailer.Trim()
        nombreConductor = nombreConductor.Trim()
        celular = celular.Trim()

        Utilidades.SepararRut(rutConductor, dv)
        If (Not String.IsNullOrEmpty(rutConductor)) Then rutConductor = rutConductor & "-" & dv

        'graba valores
        oPlanificacionTransportista = New PlanificacionTransportista(idPlanificacionTransportista)
        oPlanificacionTransportista.PatenteTracto = Sistema.Capitalize(patenteTracto, Sistema.eTipoCapitalizacion.TodoMayuscula)
        oPlanificacionTransportista.PatenteTrailer = Sistema.Capitalize(patenteTrailer, Sistema.eTipoCapitalizacion.TodoMayuscula)
        oPlanificacionTransportista.NombreConductor = Sistema.Capitalize(nombreConductor, Sistema.eTipoCapitalizacion.NombrePropio)
        oPlanificacionTransportista.RutConductor = rutConductor
        oPlanificacionTransportista.Celular = celular
        oPlanificacionTransportista.Criterio = criterio
        oPlanificacionTransportista.Observacion = observacion
        oPlanificacionTransportista.Actualizar()
      Next

      Sistema.GrabarLogSesion(oUsuario.Id, "Graba asignación de camiones")
      status = "modificado"
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la asignaci&oacute;n de camiones.<br />" & ex.Message
      status = "error"
    End Try

    Return msgError
  End Function

  ''' <summary>
  ''' graba los filtros de busqueda en sesion
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarFiltrosEnSesion()
    Dim dcFiltros As New Dictionary(Of String, String)

    Try
      dcFiltros.Add("nroTransporte", Me.txtNroTransporte.Text)
      dcFiltros.Add("fechaPresentacion", Me.txtFechaPresentacion.Text)

      Session(NOMBRE_PAGINA) = dcFiltros
    Catch ex As Exception
      Session(NOMBRE_PAGINA) = Nothing
    End Try
  End Sub

  ''' <summary>
  ''' notifica por email la asignacion de camiones y finaliza el proceso
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function NotificarAsignacion(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim idPlanificacionTransportista, fechaPresentacion, json As String
    Dim statusGrabar As Integer
    Dim objJSON As Object
    Dim oPlanificacionTransportista As New PlanificacionTransportista
    Dim listIdPlanificacionTransprotista As New ArrayList
    Try
      json = Me.txtJSONAsignarCamion.Value
      objJSON = MyJSON.ConvertJSONToObject(json)
      fechaPresentacion = Me.txtFechaPresentacion.Text

      For Each row In objJSON
        idPlanificacionTransportista = MyJSON.ItemObject(row, "IdPlanificacionTransportista")
        oPlanificacionTransportista = New PlanificacionTransportista(idPlanificacionTransportista)

        If (oPlanificacionTransportista.PatenteTracto.Length <> 0) Then

          If (Not oPlanificacionTransportista.CerradoPorTransportista) Then
            statusGrabar = PlanificacionTransportista.CerrarAsignacionCamiones(idPlanificacionTransportista)

            'guardo id para envio correo
            listIdPlanificacionTransprotista.Add(idPlanificacionTransportista)

            Sistema.GrabarLogSesion(oUsuario.Id, "Notifica la asignación de camiones: IdUsuarioTransportista " & oUsuario.Id & " / idPlanificacionTransportista " & idPlanificacionTransportista)
            status = "modificado"

          End If

        End If

      Next

      'envia email con la asignacion de camiones
      If (listIdPlanificacionTransprotista.Count > 0) Then
        Dim listId As String = String.Join(",", listIdPlanificacionTransprotista.ToArray(Type.GetType("System.String")))
        InformeMail.AsignacionCamiones(listId)
      End If


    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la asignaci&oacute;n de camiones.<br />" & ex.Message
      status = "error"
    End Try

    Return msgError
  End Function


#End Region

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Mantenedor.PerfilListado.aspx.vb" Inherits="webtransportewalmartmx.Mantenedor_PerfilListado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/perfil.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Perfil</p>
      </div>
      <div class="col-xs-4 text-right">
        <div id="btnNuevo" runat="server" class="btn btn-success" onclick="Perfil.abrirFormulario({ idPerfil:'-1' })"><span class="glyphicon glyphicon-plus"></span>&nbsp;Crear Nuevo Perfil</div>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-4">
              <label class="control-label">Nombre</label>
              <asp:TextBox ID="txtFiltroNombre" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Llave</label>
              <asp:TextBox ID="txtFiltroLlave" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Perfil.validarFormularioBusqueda())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>

          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="IdPerfil" HeaderText="ID" SortExpression="IdPerfil" HeaderStyle-Width="60px" />
              <asp:BoundField DataField="NombrePerfil" HeaderText="Nombre" SortExpression="NombrePerfil" />
              <asp:BoundField DataField="Llave" HeaderText="Llave" SortExpression="Llave" HeaderStyle-Width="150px" />
              <asp:BoundField DataField="PaginaInicio" HeaderText="P&aacute;gina Inicio" SortExpression="PaginaInicio" HeaderStyle-Width="350px" />
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="110px" />
                <ItemTemplate>
                  <input type="hidden" id="txtIdPerfil" runat="server" value='<%# Bind("IdPerfil") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>      
      </div>

    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

    });
  </script>

</asp:Content>

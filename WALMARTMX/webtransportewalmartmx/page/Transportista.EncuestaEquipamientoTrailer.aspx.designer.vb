﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Transportista_EncuestaEquipamientoTrailer

  '''<summary>
  '''Control lblProveedor.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblProveedor As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control pnlMensaje.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensaje As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlMensajeAcceso.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeAcceso As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlContenido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlContenido As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlMensajeUsuario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeUsuario As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlFormulario.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlFormulario As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control rbSi_1_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_1_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_1_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_1_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_1_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_1_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_1_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_1_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_1_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_1_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_1_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_1_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_1_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_1_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_1_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_1_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_1_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_1_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_1_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_1_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_1_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_1_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_1_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_1_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_1_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_1_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_1_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_1_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_1_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_1_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_1_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_1_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_1_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_1_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_1_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_1_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_2_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_2_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_2_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_2_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_2_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_2_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_2_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_2_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_2_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_2_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_2_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_2_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_2_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_2_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_2_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_2_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_2_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_2_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_2_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_2_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_2_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_2_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_2_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_2_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_2_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_2_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_2_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_2_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_2_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_2_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_2_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_2_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_2_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_2_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_2_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_2_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_2_7.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_2_7 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_2_7.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_2_7 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_2_7.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_2_7 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_2_8.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_2_8 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_2_8.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_2_8 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_2_8.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_2_8 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_3_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_3_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_3_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_3_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_3_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_3_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_3_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_3_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_3_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_3_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_3_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_3_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_3_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_3_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_3_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_3_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_3_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_3_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_3_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_3_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_3_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_3_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_3_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_3_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_4_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_4_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_4_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_4_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_4_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_4_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_4_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_4_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_4_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_4_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_4_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_4_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_5_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_5_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_5_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_5_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_5_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_5_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_5_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_5_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_5_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_5_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_5_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_5_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_5_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_5_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_5_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_5_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_5_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_5_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_5_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_5_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_5_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_5_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_5_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_5_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_5_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_5_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_5_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_5_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_5_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_5_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_6_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_6_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_6_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_6_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_6_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_6_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_6_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_6_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_6_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_6_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_6_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_6_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_6_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_6_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_6_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_6_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_6_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_6_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_6_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_6_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_6_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_6_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_6_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_6_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_6_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_6_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_6_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_6_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_6_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_6_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_7_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_7_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_7_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_7_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_7_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_7_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_7_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_7_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_7_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_7_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_7_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_7_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_8_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_8_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_8_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_8_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_8_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_8_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_8_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_8_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_8_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_8_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_8_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_8_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_8_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_8_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_8_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_8_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_8_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_8_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_9_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_9_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_9_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_9_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_9_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_9_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_9_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_9_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_9_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_9_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_9_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_9_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_9_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_9_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_9_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_9_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_9_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_9_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_10_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_10_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_10_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_10_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_10_1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_10_1 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_10_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_10_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_10_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_10_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_10_2.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_10_2 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_10_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_10_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_10_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_10_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_10_3.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_10_3 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_10_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_10_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_10_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_10_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_10_4.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_10_4 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_10_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_10_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_10_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_10_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_10_5.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_10_5 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_10_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_10_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_10_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_10_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_10_6.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_10_6 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbSi_10_7.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbSi_10_7 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNo_10_7.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNo_10_7 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control rbNa_10_7.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents rbNa_10_7 As Global.System.Web.UI.WebControls.RadioButton

  '''<summary>
  '''Control btnGrabar.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents btnGrabar As Global.System.Web.UI.WebControls.LinkButton
End Class

﻿Imports CapaNegocio
Imports System.Data

Public Class Transportista_EncuestaDatos
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    Grabar
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idTransportista As String

    'obtiene valores de los controles
    idTransportista = oUsuario.Id

    'obtiene datos
    ds = Encuesta.ObtenerProveedores()

    'guarda el resultado en session
    If ds Is Nothing Then
      Session("ds") = Nothing
    Else
      Session("ds") = ds
    End If
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim textoMensaje As String

    ds = CType(Session("ds"), DataSet)

    If (Not ValidarEncuestaRealizada()) Then

      Me.pnlFormulario.Visible = True
      Me.pnlMensajeUsuario.Visible = False

      Me.btnGrabar.Visible = True

      'actualiza el multiselect
      If Not ds Is Nothing Then
        With Me.msProveedores
          .DataSourcePadre = ds.Tables(0)
          .DataTextFieldPadre = "Nombre"
          .DataValueFieldPadre = "Id"
          .DataBindPadre()
        End With
      End If

    Else
      Me.pnlFormulario.Visible = False
      Me.pnlMensajeUsuario.Visible = True
      Me.btnGrabar.Visible = False

      textoMensaje = "Los datos de la Encuesta ya fueron ingresados.<br />"
      Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje
    End If
    
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Grabar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
    End If

    MostrarMensajeUsuario()
  End Sub

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarDatos()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idEncuesta As Integer = "-1"
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""


    If (rbNo.Checked) Then
      textoMensaje &= GrabarDatosPatentesSinGps(status)
    Else
      textoMensaje &= GrabarEncuesta(status)
    End If

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}Los datos fueron ingresados satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}Los datos fueron actualizados satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la informacion. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la informacion. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    'Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idEncuesta = "-1", "", "[ID: " & idEncuesta & "] ") & textoMensaje
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    Response.Redirect("./Transportista.EncuestaDatos.aspx")
  End Sub

  ''' <summary>
  ''' graba los datos de la encuesta
  ''' </summary>
  ''' <param name="status"></param>
  ''' <returns></returns>
  Private Function GrabarDatosPatentesSinGps(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""

    Dim objJSON As Object
    Dim jsonDatos, patente As String

    Try
      jsonDatos = hJson.Value
      objJSON = MyJSON.ConvertJSONToObject(jsonDatos)

      For Each row In objJSON
        patente = MyJSON.ItemObject(row, "patente")
        status = Encuesta.GrabarPatentesSinGps(oUsuario.Id, patente)
      Next

      If (status = "-200") Then
        status = "error"
      Else
        Sistema.GrabarLogSesion(oUsuario.Id, "Se graba Encuesta: placas sin gps")
        status = "ingresado"
      End If

    Catch ex As Exception
      msgError = "Error al grabar Encuesta: placas sin gps.<br/>"
    End Try

    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos de la encuesta
  ''' </summary>
  ''' <param name="status"></param>
  ''' <returns></returns>
  Private Function GrabarEncuesta(ByRef status As String) As String
    Dim msgError As String = ""
    Dim listadoIdProveedores As String
    Try

      'crea la session con los proveedores y envia a la encuesta
      listadoIdProveedores = Me.msProveedores.ValueListadoId
      Session("listadoIdProveedores") = listadoIdProveedores
      Response.Redirect("./Transportista.EncuestaEquipamientoTrailer.aspx")

      status = "ingresado"
    Catch ex As Exception
      msgError = "Error al grabar Encuesta: placas sin gps.<br/>"
    End Try

    'retorna valor
    Return msgError
  End Function

  Protected Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
    GrabarDatos()
  End Sub

  Private Function ValidarEncuestaRealizada() As Boolean
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim totalRegistro As Integer

    totalRegistro = Encuesta.ValidarEncuestaRealizada(oUsuario.Id)
    If (totalRegistro > 0) Then
      Return True
    End If

    totalRegistro = Encuesta.ValidarPatentesSinGps(oUsuario.Id)
    If (totalRegistro > 0) Then
      Return True
    End If

    Return False
  End Function
End Class
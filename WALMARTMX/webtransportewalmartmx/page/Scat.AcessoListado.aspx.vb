﻿Public Class Scat_AcessoListado
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim dt As New DataTable
    dt.Columns.Add("Nombre")
    dt.Columns.Add("Actividad")
    dt.Columns.Add("Area")
    dt.Columns.Add("FechaHoraIngreso")
    dt.Columns.Add("TiempoEnLocal")
    dt.Columns.Add("Vehiculo")
    dt.Columns.Add("ControlSalidaAcceso")
    dt.Columns.Add("Entrada")

    Dim row As DataRow = dt.NewRow()
    row.Item("Nombre") = "Juan Perez"
    row.Item("Actividad") = ""
    row.Item("Area") = ""
    row.Item("FechaHoraIngreso") = "23/11/2015 17:21"
    row.Item("Vehiculo") = ""
    row.Item("ControlSalidaAcceso") = ""
    row.Item("Entrada") = ""

    dt.Rows.Add(row)

    Me.gvPrincipal.DataSource = dt
    Me.gvPrincipal.DataBind()

    'Status Local
    dt = New DataTable
    dt.Columns.Add("Nombre")
    dt.Columns.Add("NroPersonas")
    row = dt.NewRow()
    row.Item("Nombre") = "Juan Perez"
    row.Item("NroPersonas") = "15"

    dt.Rows.Add(row)

    Me.gvStatusLocal.DataSource = dt
    Me.gvStatusLocal.DataBind()

    'Status Local
    dt = New DataTable
    dt.Columns.Add("Info")
    row = dt.NewRow()
    row.Item("Info") = "Manual Aplicación"
    dt.Rows.Add(row)
    row = dt.NewRow()
    row.Item("Info") = "Manual Aplicación Web"
    dt.Rows.Add(row)

    Me.gvInformacion.DataSource = dt
    Me.gvInformacion.DataBind()
  End Sub

End Class
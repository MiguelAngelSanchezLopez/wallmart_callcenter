﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Mantenedor.PaginaDetalle.aspx.vb" Inherits="webtransportewalmartmx.Mantenedor_PaginaDetalle" %>
<%@ Register Src="~/wuc/wucMultiselectCombinados.ascx" TagName="wucMultiselectCombinados" TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle P&aacute;gina</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/pagina.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtIdPagina" runat="server" value="-1" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">DATOS P&Aacute;GINA</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-4">
                    <label class="control-label">T&iacute;tulo<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtTitulo" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-xs-4">
                    <label class="control-label">Nombre Archivo<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtNombreArchivo" runat="server" MaxLength="1028" CssClass="form-control"></asp:TextBox>
                    <span id="hErrorNombreArchivo" runat="server"></span>
                  </div>
                  <div class="col-xs-4">
                    <label class="control-label">Categor&iacute;a<strong class="text-danger">&nbsp;*</strong></label>
                    <div class="input-group">
                      <asp:TextBox ID="txtCategoria" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                      <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-4">
                    <asp:CheckBox ID="chkMostrarEnMenu" runat="server" CssClass="control-label" Text="&nbsp;Mostrar en Men&uacute;" />
                  </div>
                  <div class="col-xs-4">
                    <asp:CheckBox ID="chkEstado" runat="server" CssClass="control-label" Text="&nbsp;Activo" />
                  </div>
                </div>
              </div>
            </div>
          </div>        

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">FUNCIONES</h3></div>
            <div class="panel-body">
              <div id="Div1" class="table-responsive" runat="server"><asp:Literal ID="hListadoFunciones" runat="server"></asp:Literal></div>
            </div>
          </div> 

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">PERFILES ASOCIADOS</h3></div>
            <div class="panel-body">
              <uc3:wucMultiselectCombinados id="msPerfiles" runat="server" TextLabelPadre="Seleccione Perfil" VisibleLabelAyuda="true" VisibleCheckTodosPadre="false" VisibleCheckTodosHija="false" RowsListBoxHija="10" RowsListBoxPadre="10"></uc3:wucMultiselectCombinados>
            </div>
          </div> 
        
        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Pagina.cerrarPopUpPagina('1')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnEliminar" runat="server" CssClass="btn btn-lg btn-danger" OnClientClick="return(Pagina.validarEliminar())"><span class="glyphicon glyphicon-trash"></span>&nbsp;Eliminar</asp:LinkButton>
          <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Pagina.validarFormularioPagina())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Crear</asp:LinkButton>
          <asp:LinkButton ID="btnModificar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Pagina.validarFormularioPagina())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Modificar</asp:LinkButton>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        setTimeout(function () {
          jQuery("#<%=Me.txtCategoria.ClientID%>").autocomplete({
            source: '../webAjax/waPagina.aspx?op=AutocompletarCategoria',
            minLength: 1
          });
        }, 100);

      });
    </script>
  </form>
</body>
</html>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Scat.ValidacionSalida.aspx.vb" Inherits="webtransportewalmartmx.Scat_ValidacionSalida" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
 <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Validar Salida</p>
      </div>
    </div>
  </div>

  <asp:Panel ID="pnlMensaje" runat="server">
  </asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" runat="server">
    </asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
       <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">DATOS SALIDA</h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label class="control-label">Nombre Operador<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtNombreConductor" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>            
              </div>
              <div class="col-xs-3">
                <label class="control-label">Rut Operador<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtRutConductor" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
                <span class="help-block">(sin puntos ej:12345678-K)</span>
              </div>      
              <div class="col-xs-3">
                <label class="control-label">L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtEmpresaTransportista" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>            
              </div>
              <div class="col-xs-3">
                <label class="control-label">Rut L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtRutTransportista" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
                <span class="help-block">(sin puntos ej:12345678-K)</span>
              </div>
            </div>
          </div>
           <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label class="control-label">Tipo Camión<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtTipoCamion" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Placa Tracto<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox5" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>            
              </div>
              <div class="col-xs-3">
                <label class="control-label">Placa Remolque<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox6" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Datos Consistentes?</label><br />
                <asp:CheckBox ID="chkDatosConsistenteSi" runat="server" CssClass="control-label" Text="&nbsp;Si" />&nbsp;
                <asp:CheckBox ID="chkDatosConsistenteNo" runat="server" CssClass="control-label" Text="&nbsp;No" />
              </div>
            </div>
           </div>

        </div>
      </div>       
    </asp:Panel>
   </div>     
   
   <div class="form-group text-center">
    <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="#"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
    <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="#"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
   </div>
</asp:Content>

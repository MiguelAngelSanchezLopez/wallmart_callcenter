﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master"
  CodeBehind="Ecommerce.ListadoViaje.aspx.vb" Inherits="webtransportewalmartmx.Ecommerce_Listado" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  
  <!-- SCRIPT -->
  <script src="../js/jquery.select2.js" type="text/javascript"></script>
  <script src="../js/ecommerce.js" type="text/javascript"></script>
  <script src="../js/loading.js" type="text/javascript"></script>
  <script src="../js/jquery.mask.js" type="text/javascript"></script>

  <!-- CSS -->
  <link href="../css/loading.css" rel="stylesheet" type="text/css" />
  <link href="../css/font-awesome.min.css" rel="stylesheet" />
  <link href="../css/build.css" rel="stylesheet" />
  
  <!--div de carga-->
  <div id="div_load">
    <div id="div_loading" class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-12 text-center">
            <h4 id="hLoading">
            </h4>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 text-center">
            <img id="imgLoading" style="width: 54px; height: 54px;" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--div de carga-->

  <asp:Panel ID="pnlMensajeAcceso" runat="server">
  </asp:Panel>

  <asp:Panel ID="pnlContenido" runat="server">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Listado Viajes Ecommerce</h3>
      </div>
      <div class="panel-body" style="padding-bottom: 0">
        <div class="form-group">
          <div class="row">
            <div class="col-xs-2">
              <label class="control-label">
                Fecha Desde</label>
              <asp:TextBox ID="txtFechaDesde" runat="server" MaxLength="10" CssClass="form-control date-pick">
              </asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">
                Fecha Hasta</label>
              <asp:TextBox ID="txtFechaHasta" runat="server" MaxLength="10" CssClass="form-control date-pick">
              </asp:TextBox>
              <div id="lblMensajeErrorFechaHasta" runat="server">
              </div>
            </div>
            <div class="col-xs-2">
              <label class="control-label">
                PKT</label>
              <asp:TextBox ID="txtPkt" runat="server" CssClass="form-control">
              </asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">
                UPC</label>
              <asp:TextBox ID="txtUpc" runat="server" CssClass="form-control">
              </asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">
                Ciudad</label>
              <uc1:wuccombo id="ddlCiudad" runat="server" fuentedatos="Tabla" tipocombo="Ciudad" itemseleccione="true" cssclass="form-control chosen-select">
              </uc1:wuccombo>
            </div>
            <div class="col-xs-1">
              <label class="control-label">
                ZIP</label>
              <asp:TextBox ID="txtZip" runat="server" CssClass="form-control">
              </asp:TextBox>
            </div>
            <div class="col-xs-1">
              <label class="control-label">
                Zona</label>
              <asp:TextBox ID="txtZona" runat="server" CssClass="form-control">
              </asp:TextBox>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-xs-2">
              <label class="control-label">
                Order Number</label>
              <asp:TextBox ID="txtOrderNumber" runat="server" CssClass="form-control">
              </asp:TextBox>
            </div>
            <div class="col-xs-1">
              <label class="control-label">
                Nro. Filas</label>
              <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control"
                MaxLength="4">25</asp:TextBox>
            </div>
            <div class="col-xs-1">
              <label class="control-label">
                &nbsp;</label><br />
              <asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn btn-primary" OnClientClick="Ecommerce.buscarViaje(event)">
                <span class="glyphicon glyphicon-search"></span>&nbsp;Buscar
              </asp:LinkButton>
            </div>
          </div>
        </div>
      </div>
    </div>
  
    <!-- RESULTADO BUSQUEDA -->
    <div class="form-group">
      <div class="row">
        <div class="col-xs-2">
          <asp:Label ID="lblRegistros" runat="server" Text="Total de Registros: " class="label label-info"
            Font-Size="9pt"></asp:Label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <asp:Panel ID="pnlMensajeUsuario" runat="server">
        </asp:Panel>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0"
          AllowSorting="True" CssClass="table table-striped table-bordered" 
          EnableModelValidation="True" DataKeyNames="Id,Asignado">
          <Columns>
            <asp:TemplateField HeaderText="#">
              <HeaderStyle Width="40px" />
              <ItemTemplate>
                <%# Container.DataItemIndex + 1%>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
            <asp:BoundField DataField="OrderNumber" HeaderText="Order Number" SortExpression="OrderNumber" />
            <asp:BoundField DataField="Pkt" HeaderText="PKT" SortExpression="Pkt" />
            <asp:BoundField DataField="Upc" HeaderText="UPC" SortExpression="UPC" />
            <asp:BoundField DataField="Ciudad" HeaderText="Ciudad" SortExpression="Ciudad" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" SortExpression="Zip"></asp:BoundField>
            <asp:BoundField DataField="Zona" HeaderText="Zona" SortExpression="Zona"></asp:BoundField>
            <asp:BoundField DataField="Placa" HeaderText="Placa" SortExpression="Placa"></asp:BoundField>
            <asp:BoundField DataField="Conductor" HeaderText="Conductor" SortExpression="Conductor"></asp:BoundField>
            <asp:BoundField DataField="EstadoViaje" HeaderText="Estado Viaje" SortExpression="EstadoViaje"></asp:BoundField>
            <asp:BoundField DataField="Cajas" HeaderText="# Cajas" SortExpression="Cajas"></asp:BoundField>
                        <asp:BoundField DataField="Cedis" HeaderText="Cedis" SortExpression="Cedis"></asp:BoundField>
             <asp:TemplateField HeaderText="Mapa">
              <ItemTemplate>
                  <asp:LinkButton ID="btnMapa" runat="server" CssClass="btn btn-info btn-sm" title="Ver Mapa"><span class="glyphicon glyphicon-pushpin"></span></asp:LinkButton>
              </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
              <ItemTemplate>

                <div id="dvSeleccionar" runat="server">
                  <div class="checkbox checkbox-success" style="margin-bottom: 0px; margin-top: 0px; float:left;">
                    <asp:CheckBox id="chkSeleccionar" runat="server" Text=" " onchange="Ecommerce.habilitarGrabar(event)" />
                  </div>
                  <div style="float:left;">
                    <asp:Label id="lblCorrelativo" runat="server" class="badge alert-info" Text="0"></asp:Label>
                    <asp:HiddenField id="hCorrelativo" runat="server"></asp:HiddenField>
                  </div>
                </div>

              </ItemTemplate>
              <HeaderStyle Width="70px" />
            </asp:TemplateField>
          </Columns>
        </asp:GridView>
      </div>
    </div>

    <!--botones-->
    <div class="row">
      <div class="col-xs-12 text-center">
        <asp:LinkButton ID="btnAsignar" runat="server" CssClass="btn btn-success" OnClientClick="Ecommerce.showModal(event)" disabled="disabled"><span class="glyphicon glyphicon-check"></span>&nbsp;Asignar Datos</asp:LinkButton>
        <asp:LinkButton ID="btnLimpiar" runat="server" CssClass="btn btn-primary" disabled="disabled"><span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Limpiar</asp:LinkButton>
        <%--<asp:LinkButton ID="btnLimpiar" runat="server" CssClass="btn btn-primary" OnClientClick="Ecommerce.quitarSeleccion(event)" disabled="disabled"><span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Limpiar</asp:LinkButton>--%>
      </div>
    </div>
  </asp:Panel>

  <!--Modal-Inicio-->
  <div class="modal fade" id="mAsignarDatos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            Ingreso de Placa y Operador</h5>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-4">
                <label class="control-label">
                  Fecha Programación</label>
                <asp:TextBox ID="txtFechaProgramacion" runat="server" MaxLength="10" CssClass="form-control date-pick">
                </asp:TextBox>
              </div>
              <div class="col-xs-3">
                <label class="control-label">
                  Cantidad de PKT</label>
                <asp:TextBox ID="txtCantidadPKT" runat="server" CssClass="form-control">
                </asp:TextBox>
              </div>
              <div class="col-xs-3">
                <label class="control-label">
                  Zona</label>
                <asp:TextBox ID="txtZonaAsignada" runat="server" CssClass="form-control">
                </asp:TextBox>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label class="control-label">
                  Placa</label>
                <asp:TextBox ID="txtPlaca" runat="server" CssClass="form-control" onChange="Ecommerce.obtenerInfoPatente();">
                </asp:TextBox>
              </div>
              <div class="col-xs-5">
                <label class="control-label">
                  Conductor</label>
                <asp:TextBox ID="txtConductor" runat="server" CssClass="form-control">
                </asp:TextBox>
              </div>
            </div>
          </div>

          <div id="dvInfo" class="form-group" style="display:none">
            <div class="row">
              <div class="col-xs-12">

                <div class="alert alert-info" role="alert">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                      <asp:Label ID="lblTransportista" runat="server" style="font-size:small"/>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-12">
                      <asp:Label ID="lblSeñalGps" runat="server" style="font-size:small"/>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-12">
                      <asp:Label ID="lblEnCD" runat="server" style="font-size:small"/>
                    </div>
                  </div>
                </div>

              </div>
                  
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-primary" OnClientClick="Ecommerce.grabarDetalle(event)"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Grabar</asp:LinkButton>
        </div>
      </div>
    </div>
  </div>
  <!--Modal-Fin-->

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });


      $("#ctl00_cphContent_txtFechaDesde").mask("00/00/0000", { placeholder: "__/__/__" });
      $("#ctl00_cphContent_txtFechaHasta").mask("00/00/0000", { placeholder: "__/__/__" });
      $("#ctl00_cphContent_txtFechaProgramacion").mask("00/00/0000", { placeholder: "__/__/__" });
      
    });
  </script>
</asp:Content>

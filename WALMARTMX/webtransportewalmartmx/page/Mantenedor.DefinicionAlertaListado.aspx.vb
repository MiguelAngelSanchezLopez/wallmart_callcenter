﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_DefinicionAlertaListado
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario, textoFiltroNombre, gestionarPorCemtra As String

    'obtiene valores de los controles
    idUsuario = oUsuario.Id
    textoFiltroNombre = Utilidades.IsNull(Me.txtFiltroNombre.Text, "-1")
    gestionarPorCemtra = IIf(Me.chkGestionarPorCemtra.Checked, "1", "0")

    'obtiene listado
    ds = Alerta.ObtenerListadoDefinicionAlertas(idUsuario, textoFiltroNombre, gestionarPorCemtra)

    'guarda el resultado en session
    If ds Is Nothing Then
      Session("dv") = Nothing
    Else
      Session("dv") = ds.Tables(0).DefaultView
    End If
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim dv As New DataView
    Dim totalRegistros As Integer

    dv = CType(Session("dv"), DataView)
    If dv Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = dv.Table.Rows.Count
    End If

    If totalRegistros > 0 Then
      Me.pnlMensajeUsuario.Visible = False
      Me.pnlHolderGrilla.Visible = True
      'carga la grilla a utilizar
      Me.gvPrincipal.Visible = True
      Me.gvPrincipal.DataSource = dv
      Me.gvPrincipal.DataBind()
      Utilidades.MostrarTotalRegistrosGrilla(dv, Me.lblTotalRegistros)
    Else
      Me.pnlHolderGrilla.Visible = False
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = "No se encontraron registros"
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)
      'oculta la grilla
      Me.gvPrincipal.Visible = False
    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim registrosPorPagina As String = Me.txtFiltroRegistrosPorPagina.Text

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    '-- inicializacion de controles que NO dependen del postBack --
    If registrosPorPagina = "" Then
      Me.txtFiltroRegistrosPorPagina.Text = Utilidades.ObtenerRegistrosPorPaginaDefault()
    End If

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnNuevo.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' metodo general para la paginacion de la grilla
  ''' </summary>
  Private Sub GridViewPageIndexChanging(ByVal gridView As GridView, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    Try
      'Cambia de pagina
      gridView.PageIndex = e.NewPageIndex
      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  ''' <summary>
  ''' metodo general para ordenar segun grilla seleccionada
  ''' </summary>
  Private Sub GridViewSorting(ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    Dim oUtilidades As New Utilidades
    Dim setearOrdenInicial As Boolean = False
    Try
      'Recupera el dataset para ordenar
      Dim dv As DataView = CType(Session("dv"), DataView)

      'determina el campo por el cual se ordenara
      Dim strOrdenarPor As String = e.SortExpression

      'Si ya se habia ordenado por ese campo, ordena pero en forma descendente
      If Not dv Is Nothing Then
        If strOrdenarPor = dv.Sort Then
          strOrdenarPor = strOrdenarPor & " DESC"
        End If
        'Ordena la grilla de acuerdo a la columna seleccionada
        dv.Sort = strOrdenarPor
      End If

      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim cargadoDesdePopUp As String = Me.txtCargaDesdePopUp.Value

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    If cargadoDesdePopUp = "1" Then
      ObtenerDatos()
      ActualizaInterfaz()
    End If

    MostrarMensajeUsuario()
    Me.txtCargaDesdePopUp.Value = "0"
  End Sub

  Protected Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim pageSize As String = Trim(txtFiltroRegistrosPorPagina.Text)
    If pageSize = "" Or pageSize = "0" Then
      pageSize = Utilidades.ObtenerRegistrosPorPaginaDefault()
      txtFiltroRegistrosPorPagina.Text = pageSize
    End If
    'asigna total de registros por pagina
    Me.gvPrincipal.PageSize = pageSize
    'Muestra la primera pagina del paginador
    Me.gvPrincipal.PageIndex = 0

    ObtenerDatos()
    ActualizaInterfaz()
    Sistema.GrabarLogSesion(oUsuario.Id, "Busca definición de alertas")
  End Sub

  Protected Sub gvPrincipal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrincipal.PageIndexChanging
    'llama metodo general para paginar
    GridViewPageIndexChanging(Me.gvPrincipal, e)
  End Sub

  Protected Sub gvPrincipal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPrincipal.RowDataBound
    Dim lblOpciones As Label
    Dim txtIdAlertaDefinicion, txtTieneRegistrosAsociados As HtmlControls.HtmlInputHidden
    Dim indicePrimeraFila As Integer = Utilidades.ObtenerIndicePrimeraFilaGrilla(Me.gvPrincipal)
    Dim indiceFila As Integer
    Dim opciones As String = ""
    Dim idAlertaDefinicion, tieneRegistrosAsociados As String

    If e.Row.RowType = DataControlRowType.DataRow Then
      txtIdAlertaDefinicion = CType(e.Row.FindControl("txtIdAlertaDefinicion"), HtmlControls.HtmlInputHidden)
      txtTieneRegistrosAsociados = CType(e.Row.FindControl("txtTieneRegistrosAsociados"), HtmlControls.HtmlInputHidden)
      lblOpciones = CType(e.Row.FindControl("lblOpciones"), Label)

      idAlertaDefinicion = txtIdAlertaDefinicion.Value
      tieneRegistrosAsociados = txtTieneRegistrosAsociados.Value

      If (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)) Then
        opciones &= "<button type=""button"" class=""btn btn-default"" onclick=""Alerta.abrirFormularioDefinicionAlerta({ idAlertaDefinicion:'" & idAlertaDefinicion & "' })""><span class=""glyphicon glyphicon-pencil""></span></button>&nbsp;"
      End If
      If (tieneRegistrosAsociados = "0" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)) Then
        opciones &= "<button type=""button"" class=""btn btn-default"" onclick=""Alerta.eliminarRegistroDefinicionAlerta({ idAlertaDefinicion:'" & idAlertaDefinicion & "' })""><span class=""glyphicon glyphicon-trash""></span></button>&nbsp;"
      End If

      lblOpciones.Text = opciones

      'limpia controles
      txtIdAlertaDefinicion.Value = ""

      'asigna indice de la fila
      indiceFila = indicePrimeraFila + e.Row.RowIndex
      'asigna valor del indice de la fila al control
      Utilidades.SetControlIndiceFila(e.Row, "lblIndiceFila", indiceFila)
    End If
  End Sub

  Protected Sub gvPrincipal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPrincipal.Sorting
    'llama metodo general para ordenar
    GridViewSorting(e)
  End Sub

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master"
  CodeBehind="Supervisor.ReportePulsoDiario.aspx.vb" Inherits="webtransportewalmartmx.Supervisor_ReportePulsoDiario" %>

<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.mask.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>
  <script type="text/javascript" src="../js/graficos/highcharts.js"></script>
  <script type="text/javascript" src="../js/graficos/exporting.js"></script>
  <script type="text/javascript" src="../js/grafico.js"></script>
  <script type="text/javascript" src="../js/usuario.js"></script>
  <asp:Panel ID="pnlMensaje" runat="server">
  </asp:Panel>
  <div>
    <div class="form-group text-center">
      <div class="row">
        <div class="col-xs-4">
          &nbsp;
        </div>
        <div class="col-xs-4">
          <p class="h1">Pulso Diario</p>
        </div>
      </div>            
    </div>

    <asp:Panel ID="pnlMensajeAcceso" runat="server">
    </asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="form-group">
        <div class="panel panel-default">
          <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
          <div class="panel-body">
            <div class="row">
              <div class="col-xs-2">
                <label class="control-label">Fecha Inicial</label>
                <asp:TextBox ID="TxFechaInicio" runat="server" MaxLength="10" CssClass="form-control input-sm date-pick"></asp:TextBox>
                <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 01/01/2017)</div>
                <div id="lblMensajeErrorFechaInicio"></div>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Fecha Final</label>
                <asp:TextBox ID="TxFechaTermino" runat="server" MaxLength="10" CssClass="form-control input-sm date-pick"></asp:TextBox>
                <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 31/01/2017)</div>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Ejecutivo</label>
                <uc1:wuccombo id="ddlOperador" runat="server" fuentedatos="Tabla" tipocombo="TeleOperadores" itemtodos="true" cssclass="form-control chosen-select"></uc1:wuccombo>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Turno</label>
                <uc1:wuccombo id="ddlTurno" runat="server" fuentedatos="TipoGeneral" tipocombo="TurnoTeleOperador" itemtodos="true" cssclass="form-control chosen-select"></uc1:wuccombo>
              </div>
              <div class="col-xs-2">
                <label class="control-label">&nbsp;</label><br />
                <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary btn-sm" OnClientClick="return(Usuario.validarFormularioPulsoDiario())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <center>
        <div class="form-group sist-font-size-11">
          <asp:Panel ID="pnlMensajeUsuario" runat="server"></asp:Panel>
          <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Visible="false">
            <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>
            <table border="0">
              <tr>
                <td style="padding-right: 60px">
                  <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="False" PagerSettings-Position="TopAndBottom" PagerStyle-Font-Size="Larger" AllowSorting="False" CssClass="table table-striped table-bordered">
                    <Columns>
                      <asp:TemplateField HeaderText="#">
                        <HeaderStyle Width="40px" />
                        <ItemTemplate>
                          <%# Container.DataItemIndex + 1%>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField DataField="NombreCompleto" HeaderText="Nombre Usuario" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                      <asp:BoundField DataField="Alertas" HeaderText="Alertas" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                      <asp:BoundField DataField="NS" HeaderText="NS <= 7 Min" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                      <asp:BoundField DataField="NS_Percent" HeaderText="% NS" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                      <asp:BoundField DataField="Tmo" HeaderText="TMO" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                      <asp:BoundField DataField="totalTime" HeaderText="Tiempo" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    </Columns>
                  </asp:GridView>
                </td>
              </tr>
            </table>
          </asp:Panel>
        </div>
        <div class="form-group">
          <center>
            <table border="0" cellpadding="5" cellspacing="0" width="100%">
              <tr valign="top">
                <td align="center" style="width: 100%;">
                  <div id="hGrafico" style="width: 100%;"></div>
                </td>
               </tr>
            </table>
          </center>
        </div>
        <div class="form-group text-center">
          <div id="lblMensajeGeneracionReporte"></div>
          <asp:HiddenField ID="HdData" runat="Server" />
        </div>
      </center>
    </asp:Panel>
  </div>
  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });

      jQuery(".chosen-select").select2();
      jQuery("#" + Sistema.PREFIJO_CONTROL + "TxFechaInicio").mask("00/00/0000", { placeholder: "__/__/__" });
      jQuery("#" + Sistema.PREFIJO_CONTROL + "TxFechaTermino").mask("00/00/0000", { placeholder: "__/__/__" });
    });
   
  </script>
</asp:Content>

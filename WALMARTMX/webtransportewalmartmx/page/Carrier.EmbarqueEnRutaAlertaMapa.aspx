﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Carrier.EmbarqueEnRutaAlertaMapa.aspx.vb" Inherits="webtransportewalmartmx.Carrier_EmbarqueEnRutaAlertaMapa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title></title>
  <link id="styleBootstrap" rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyDdOQbBcxfGCicAna5gjq1NVPdE-T4KDL8"></script>-->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8"></script>
  
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>

  <style type="text/css">
    html { height: 100% }
    body { height: 100%; margin: 0; padding: 0 }
  </style>

  <script type="text/javascript">
    function getVarsUrl() {
      var url = location.search.replace("?", "");
      var arrUrl = url.split("&");
      var urlObj = {};
      for (var i = 0; i < arrUrl.length; i++) {
        var x = arrUrl[i].split("=");
        urlObj[x[0]] = x[1]
      }
      return urlObj;
    }

    function actualizaInterfazSobrestadiaPatio(opciones) {
      var elemContenedorMapa, json, latDestino, lonDestino, latActual, lonActual, paginaActual, map;

      try {
        latActual = opciones.latitud;
        lonActual = opciones.longitud;
        paginaActual = Sistema.obtenerPaginaActual();

        elemContenedorMapa = "mapCanvas";
        var myLatLonActual = new google.maps.LatLng(latActual, lonActual);
        var mapOptions = {
          zoom: 18,
          center: myLatLonActual
        };
        map = new google.maps.Map(document.getElementById(elemContenedorMapa), mapOptions);

        var markerPosicionActual = new google.maps.Marker({
          position: myLatLonActual,
          map: map,
          icon: "../img/warning.png"
        });

        //ejemplo poligono patio LT
        /*
        var poligono = [
            new google.maps.LatLng(19.623218, -99.028007),
            new google.maps.LatLng(19.622420, -99.029182),
            new google.maps.LatLng(19.621334, -99.028624),
            new google.maps.LatLng(19.623077, -99.027562)
        ];

        var miPoligono = new google.maps.Polygon({
          paths: poligono,
          strokeColor: "#83BBF3",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#83BBF3",
          fillOpacity: 0.4
        });
        miPoligono.setMap(map);
        */

      } catch (e) {
        alert("Exception: actualizaInterfazSobrestadiaPatio\n" + e);
      }
    }

  </script>

</head>
<body>
  <div id="mapCanvas" class="sist-map-canvas-carrier">
    <center>
      <strong>cargando mapa...</strong>
    </center>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      setTimeout(function () {
        var variables = getVarsUrl();
        actualizaInterfazSobrestadiaPatio({ latitud: variables.lat, longitud: variables.lon });
      }, 500);
    });
  </script>
</body>
</html>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Buscador.DondeEstaCamion.aspx.vb" Inherits="webtransportewalmartmx.Buscador_DondeEstaCamion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY"></script>-->
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8"></script>
  <script language="javascript" type="text/javascript" src="../js/trazaviaje.js"></script>
  
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>
  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="form-group">
        <div class="row">
          <div class="col-xs-6 text-right">
            <p class="h1">¿D&oacute;nde est&aacute; el cami&oacute;n?</p>
          </div>
          <div class="col-xs-6">
            <div>
              <div class="row">
                <div class="col-xs-6">
                  <div>
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span>&nbsp; <label>PLACA</label></span>
                      <input type="text" id="txtPatente" class="form-control input-lg text-center" value="" placeholder="Ingrese placa" />
                    </div>
                    <div id="lblMensajeErrorPatente"></div>
                  </div>
                </div>
                <div class="col-xs-4">
                  <div>
                    <div id="btnConsultar" class="btn btn-lg btn-block btn-success" onclick="Sistema.dondeEstaCamion()"><span class="glyphicon glyphicon-globe"></span>&nbsp;Consultar</div>
                  </div>
                </div>
                <div class="col-xs-2">&nbsp;</div>
              </div>
            </div>
          </div>
        </div>            
      </div>

      <div class="form-group">
        <div id="map-canvas" class="sist-map-canvas-donde-esta-camion">
          <div class="text-center">
            <div class="sist-padding-top-20"></div>
            <div class="form-group"><img src="../img/sin_mapa.png" alt="" /></div>
          </div>                  
        </div>

        <br />
        <div id="hTablaDatos" class="form-group small"></div>
      </div>

    </asp:Panel>
  </div>

</asp:Content>
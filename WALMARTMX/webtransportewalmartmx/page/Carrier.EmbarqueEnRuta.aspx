﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Carrier.EmbarqueEnRuta.aspx.vb" Inherits="webtransportewalmartmx.Carrier_EmbarqueEnRuta" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title></title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/carrier.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container">
      <div class="form-group text-center">
      <p class="h2">
        Embarque en ruta</p>
      </div>

      <div class="panel panel-default">
      <div class="panel-heading">
        <span class="panel-title"><strong>INFORMACION EMBARQUE</strong></span></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              IdEmbarque</label>
            <div>
              <asp:Label ID="lblSemaforo" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              IdMaster</label>
            <div>
              <asp:Label ID="lblIdMaster" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              Cedis</label>
            <div>
              <asp:Label ID="lblCedis" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              Placa Remolque</label>
            <div>
              <asp:Label ID="lblPlacaRemolque" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              Placa Tracto</label>
            <div>
              <asp:Label ID="lblPlacaTracto" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              Determinante</label>
            <div>
              <asp:Label ID="lblDeterminante" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              Tiempo del viaje</label>
            <div>
              <asp:Label ID="lblTiempoViaje" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              Tiempo estimado arribo</label>
            <div>
              <asp:Label ID="lblEta" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              Kilómetros recorridos</label>
            <div>
              <asp:Label ID="lblKilometrosRecorridos" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
          </div>
          <div class="col-xs-6 col-sm-3 col-md-2">
            <label class="control-label">
              Total Alertas</label>
            <div>
              <asp:Label ID="lblTotalAlertas" runat="server" /></div>
            <div class="small text-muted">
              &nbsp;</div>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group text-center">
      <p class="h2">
        Alertas generadas</p>
      </div>
     <div><strong><asp:Label ID="lblRegistros" runat="server"></asp:Label></strong></div>
    <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" 
      CellPadding="0" AllowPaging="True" AllowSorting="True" 
      CssClass="table table-striped table-bordered" EnableModelValidation="True" 
      DataKeyNames="Lat,Lon">
      <columns>
        <asp:TemplateField HeaderText="#">
          <HeaderStyle Width="40px" />
          <ItemTemplate>
                <%# Container.DataItemIndex + 1%>
          </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="DescripcionAlerta" HeaderText="Alerta" SortExpression="DescripcionAlerta" />
        <asp:BoundField DataField="FechaAlerta" HeaderText="Fecha" SortExpression="FechaAlerta" />
        <asp:BoundField DataField="Gestionada" HeaderText="Gestionada" SortExpression="Gestionada" />
        <asp:TemplateField HeaderText="Opciones">
          <ItemStyle Wrap="True"/>
          <ItemTemplate>            
            <div id="btnVerMapa" runat="server" class="btn btn-primary"><span class="glyphicon glyphicon-globe"></span></div>
            <%--<asp:LinkButton ID="btnVerMapa" runat="server" CssClass="btn btn-primary"><span class="glyphicon glyphicon-globe"></span></asp:LinkButton>--%>
          </ItemTemplate>
        </asp:TemplateField>
      </columns>
    </asp:GridView>
    <%--<table class="table table-condensed">
        <thead>
          <tr>
          <th>
            Alerta
          </th>
          <th>
            Fecha
          </th>
          <th>
            Gestionada
          </th>
          <th>
            Mapa
          </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
            SOBRETIEMPO EN RUTA
          </td>
          <td>
            09/01/2017 09:42 hrs.
            </td>
            <td>
            SI
            </td>
            <td>
            
            </td>
          </tr>
        </tbody>
    </table>--%>
    </div>
  </form>
</body>
</html>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Scat.ValidacionAccesoFinal.aspx.vb" Inherits="webtransportewalmartmx.Scat_ValidacionAccesoFinal" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
  <script src="../js/jquery.select2.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Validar Acceso Final</p>
      </div>
    </div>
  </div>

  <asp:Panel ID="pnlMensaje" runat="server">
  </asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" runat="server">
    </asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">CONTROL OPERADOR</h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-4">
                <label class="control-label">Nombre Operador</label>
                <asp:TextBox ID="txtNombre" runat="server" MaxLength="50" CssClass="form-control" ReadOnly=true></asp:TextBox>            
              </div>
              <div class="col-xs-2">
                <label class="control-label">RUT</label>
                <asp:TextBox ID="txtRut" runat="server" Columns="20" MaxLength="20" CssClass="form-control" ReadOnly=true></asp:TextBox>
                <span class="help-block">(sin puntos ej:12345678-K)</span>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Estado</label>
                <asp:TextBox ID="txtEstado" runat="server" MaxLength="50" CssClass="form-control" ReadOnly=true></asp:TextBox>
              </div>
            </div>
          </div>
        </div>
      </div>

       <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Control Camión</h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label class="control-label">L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtEmpresaTransportista" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>            
              </div>
              <div class="col-xs-2">
                <label class="control-label">Rut L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtRutTransportista" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
                <span class="help-block">(sin puntos ej:12345678-K)</span>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Tipo Camión<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtTipoCamion" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Fecha Entrada<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtFechaEntrada" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Hora Entrada<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtHoraEntrada" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                <span class="help-block">(ejm: 09:00)</span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label class="control-label">Placa Tracto<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox1" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>            
              </div>
              <div class="col-xs-3">
                <label class="control-label">Señal Satelital Tracto<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox3" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Placa Remolque<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox4" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Señal Satelital Remolque<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="TextBox2" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-xs-3">
                <label class="control-label">Entrada de Camión<strong class="text-danger">&nbsp;*</strong></label>
                <uc1:wucCombo ID="ddlEntradaCamion" runat="server" FuenteDatos="TipoGeneral" TipoCombo="Scat.EntradaCamion" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>            
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group text-center">
        <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="#"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
        <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="#"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
      </div>
    </asp:Panel>
  </div>
  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });
    });

  </script>
</asp:Content>

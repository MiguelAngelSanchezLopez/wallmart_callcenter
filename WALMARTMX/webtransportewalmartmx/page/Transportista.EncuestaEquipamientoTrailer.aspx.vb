﻿Imports CapaNegocio
Imports System.Data

Public Class Transportista_EncuestaEquipamientoTrailer
  Inherits System.Web.UI.Page

  Dim listadoIdProveedores As String = ""

#Region "Enum"
  Private Enum eFunciones
    Ver
    Grabar
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim arrProveedores As New ArrayList
    Dim listadoProveedores As String

    listadoProveedores = Session("listadoIdProveedores")

    If (listadoProveedores <> "") Then

      'Obtiene los proveedores
      arrProveedores.AddRange(Split(listadoProveedores, ","))

      'obtiene datos
      ds = Encuesta.ObtenerProveedores(arrProveedores(0))

      'elimina el proveedor
      arrProveedores.RemoveAt(0)
      Session("listadoIdProveedores") = String.Join(",", arrProveedores.ToArray(Type.GetType("System.String")))

      'guarda el resultado en session
      If ds Is Nothing Then
        Session("ds") = Nothing
      Else
        Session("ds") = ds
      End If
    Else
      Dim TextoMensaje As String = "{INGRESADO}Se han realizado todas las encuestas de proveedores"
      Session(Utilidades.KEY_SESION_MENSAJE) = TextoMensaje

      pnlFormulario.Visible = False

    End If
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim idProveedor As Integer
    Dim proveedor As String

    ds = CType(Session("ds"), DataSet)

    If ds Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = ds.Tables(0).Rows.Count
    End If

    If totalRegistros > 0 Then
      dr = ds.Tables(0).Rows(0)
      idProveedor = dr.Item("Id")
      proveedor = dr.Item("Nombre")

      Me.ViewState.Item("idProveedor") = idProveedor
      lblProveedor.Text = proveedor.ToUpper()

      'If (realizada) Then
      '  Me.pnlFormulario.Visible = False
      '  Me.pnlMensajeUsuario.Visible = True
      '  Me.btnGrabar.Visible = False

      '  Dim TextoMensaje As String = "{INGRESADO}La encuesta fue realizada con exito. "
      '  TextoMensaje &= "Fecha Envio " & FechaCreacion

      '  Session(Utilidades.KEY_SESION_MENSAJE) = TextoMensaje

      'Else
      '  Me.pnlFormulario.Visible = True
      '  Me.pnlMensajeUsuario.Visible = False

      '  Me.btnGrabar.Visible = True
      'End If
    Else
      Me.pnlFormulario.Visible = True
      Me.pnlMensajeUsuario.Visible = False

      Me.btnGrabar.Visible = True
    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Grabar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarDatos()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idEncuesta As Integer = "-1"
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosEncuesta(status, idEncuesta)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La ENCUESTA fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}LA ENCUESTA fue actualizado satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la ENCUESTA. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la ENCUESTA. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    'Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idEncuesta = "-1", "", "[ID: " & idEncuesta & "] ") & textoMensaje
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    Response.Redirect("./Transportista.EncuestaEquipamientoTrailer.aspx")
  End Sub

  ''' <summary>
  ''' graba los datos de la encuesta
  ''' </summary>
  ''' <param name="status"></param>
  ''' <param name="idEncuesta"></param>
  ''' <returns></returns>
  Private Function GrabarDatosEncuesta(ByRef status As String, ByRef idEncuesta As Integer) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim ds As DataSet = Session("ds")

    Try

      Dim idProveedor As Integer = Me.ViewState.Item("idProveedor")

      'graba todas las respuestas
      Encuesta.GrabarEncuesta("1", "1.1", Me.rbSi_1_1.Checked, Me.rbNo_1_1.Checked, Me.rbNa_1_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("1", "1.2", Me.rbSi_1_2.Checked, Me.rbNo_1_2.Checked, Me.rbNa_1_2.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("1", "1.3", Me.rbSi_1_3.Checked, Me.rbNo_1_3.Checked, Me.rbNa_1_3.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("1", "1.4", Me.rbSi_1_4.Checked, Me.rbNo_1_4.Checked, Me.rbNa_1_4.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("1", "1.5", Me.rbSi_1_5.Checked, Me.rbNo_1_5.Checked, Me.rbNa_1_5.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("1", "1.6", Me.rbSi_1_6.Checked, Me.rbNo_1_6.Checked, Me.rbNa_1_6.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("2", "2.1", Me.rbSi_2_1.Checked, Me.rbNo_2_1.Checked, Me.rbNa_2_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("2", "2.2", Me.rbSi_2_2.Checked, Me.rbNo_2_2.Checked, Me.rbNa_2_2.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("2", "2.3", Me.rbSi_2_3.Checked, Me.rbNo_2_3.Checked, Me.rbNa_2_3.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("2", "2.4", Me.rbSi_2_4.Checked, Me.rbNo_2_4.Checked, Me.rbNa_2_4.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("2", "2.5", Me.rbSi_2_5.Checked, Me.rbNo_2_5.Checked, Me.rbNa_2_5.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("2", "2.6", Me.rbSi_2_6.Checked, Me.rbNo_2_6.Checked, Me.rbNa_2_6.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("2", "2.7", Me.rbSi_2_7.Checked, Me.rbNo_2_7.Checked, Me.rbNa_2_7.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("2", "2.8", Me.rbSi_2_8.Checked, Me.rbNo_2_8.Checked, Me.rbNa_2_8.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("3", "3.1", Me.rbSi_3_1.Checked, Me.rbNo_3_1.Checked, Me.rbNa_3_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("3", "3.2", Me.rbSi_3_2.Checked, Me.rbNo_3_2.Checked, Me.rbNa_3_2.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("3", "3.3", Me.rbSi_3_3.Checked, Me.rbNo_3_3.Checked, Me.rbNa_3_3.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("3", "3.4", Me.rbSi_3_4.Checked, Me.rbNo_3_4.Checked, Me.rbNa_3_4.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("4", "4.1", Me.rbSi_4_1.Checked, Me.rbNo_4_1.Checked, Me.rbNa_4_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("4", "4.2", Me.rbSi_4_2.Checked, Me.rbNo_4_2.Checked, Me.rbNa_4_2.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("5", "5.1", Me.rbSi_5_1.Checked, Me.rbNo_5_1.Checked, Me.rbNa_5_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("5", "5.2", Me.rbSi_5_2.Checked, Me.rbNo_5_2.Checked, Me.rbNa_5_2.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("5", "5.3", Me.rbSi_5_3.Checked, Me.rbNo_5_3.Checked, Me.rbNa_5_3.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("5", "5.4", Me.rbSi_5_4.Checked, Me.rbNo_5_4.Checked, Me.rbNa_5_4.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("5", "5.5", Me.rbSi_5_5.Checked, Me.rbNo_5_5.Checked, Me.rbNa_5_5.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("6", "6.1", Me.rbSi_6_1.Checked, Me.rbNo_6_1.Checked, Me.rbNa_6_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("6", "6.2", Me.rbSi_6_2.Checked, Me.rbNo_6_2.Checked, Me.rbNa_6_2.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("6", "6.3", Me.rbSi_6_3.Checked, Me.rbNo_6_3.Checked, Me.rbNa_6_3.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("6", "6.4", Me.rbSi_6_4.Checked, Me.rbNo_6_4.Checked, Me.rbNa_6_4.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("6", "6.5", Me.rbSi_6_5.Checked, Me.rbNo_6_5.Checked, Me.rbNa_6_5.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("7", "7.1", Me.rbSi_7_1.Checked, Me.rbNo_7_1.Checked, Me.rbNa_7_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("7", "7.2", Me.rbSi_7_2.Checked, Me.rbNo_7_2.Checked, Me.rbNa_7_2.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("8", "8.1", Me.rbSi_8_1.Checked, Me.rbNo_8_1.Checked, Me.rbNa_8_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("8", "8.2", Me.rbSi_8_2.Checked, Me.rbNo_8_2.Checked, Me.rbNa_8_2.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("8", "8.3", Me.rbSi_8_3.Checked, Me.rbNo_8_3.Checked, Me.rbNa_8_3.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("9", "9.1", Me.rbSi_9_1.Checked, Me.rbNo_9_1.Checked, Me.rbNa_9_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("9", "9.2", Me.rbSi_9_2.Checked, Me.rbNo_9_2.Checked, Me.rbNa_9_2.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("9", "9.3", Me.rbSi_9_3.Checked, Me.rbNo_9_3.Checked, Me.rbNa_9_3.Checked, idProveedor, oUsuario.Id)

      Encuesta.GrabarEncuesta("10", "10.1", Me.rbSi_10_1.Checked, Me.rbNo_10_1.Checked, Me.rbNa_10_1.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("10", "10.2", Me.rbSi_10_2.Checked, Me.rbNo_10_2.Checked, Me.rbNa_10_2.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("10", "10.3", Me.rbSi_10_3.Checked, Me.rbNo_10_3.Checked, Me.rbNa_10_3.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("10", "10.4", Me.rbSi_10_4.Checked, Me.rbNo_10_4.Checked, Me.rbNa_10_4.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("10", "10.5", Me.rbSi_10_5.Checked, Me.rbNo_10_5.Checked, Me.rbNa_10_5.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("10", "10.6", Me.rbSi_10_6.Checked, Me.rbNo_10_6.Checked, Me.rbNa_10_6.Checked, idProveedor, oUsuario.Id)
      Encuesta.GrabarEncuesta("10", "10.7", Me.rbSi_10_7.Checked, Me.rbNo_10_7.Checked, Me.rbNa_10_7.Checked, idProveedor, oUsuario.Id)

      If (idEncuesta = "-200") Then
        status = "error"
      Else
        Sistema.GrabarLogSesion(oUsuario.Id, "Se graba encuesta Equipos GPS: ")
        status = "ingresado"
      End If

    Catch ex As Exception
      msgError = "Error al grabar datos de la encuesta.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  Protected Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
    GrabarDatos()
  End Sub
End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionOperacion.DatoExtra.aspx.vb" Inherits="webtransportewalmartmx.GestionOperacion_DatoExtra" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Gesti&oacute;n Operaci&oacute;n datos extras</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.mask.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/gestionOperacion.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtIdRegistro" runat="server" value="-1" />
    <input type="hidden" id="txtEntidad" runat="server" value="" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Actualizar datos extras"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-primary sist-display-none" data-bloqueVisible="DatosLlegada">
            <div class="panel-heading"><h3 class="panel-title">DATOS LLEGADA VIAJE</h3></div>
            <div class="panel-body">
              <div>
                <div class="row">
                  <div class="col-xs-2 sist-display-none" data-campoVisible="FechaLlegada">
                    <label class="control-label">Fecha Llegada</label>
                    <asp:TextBox ID="txtFechaLlegada" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
                    <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 01/01/2016)</div>
                  </div>
                  <div class="col-xs-2 sist-display-none" data-campoVisible="FechaLlegada">
                    <label class="control-label">Hora Llegada</label>
                    <asp:TextBox ID="txtHoraLlegada" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                    <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 08:15)</div>
                  </div>
                  <div class="col-xs-2 sist-display-none" data-campoVisible="FechaLlegada">
                    <label class="control-label">Fecha Salida</label>
                    <asp:TextBox ID="txtFechaSalida" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
                    <div><asp:Label ID="lblMensajeErrorFechaSalida" runat="server"></asp:Label></div>
                    <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 01/01/2016)</div>
                  </div>
                  <div class="col-xs-2 sist-display-none" data-campoVisible="FechaLlegada">
                    <label class="control-label">Hora Salida</label>
                    <asp:TextBox ID="txtHoraSalida" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                    <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 08:15)</div>
                  </div>
                </div>
                <div class="sist-display-none" data-campoVisible="Observacion">
                  <label class="control-label">Observaci&oacute;n</label>
                  <asp:TextBox ID="txtObservacionLlegada" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-primary sist-display-none" data-bloqueVisible="CargaSuelta">
            <div class="panel-heading"><h3 class="panel-title">DATOS CARGA SUELTA</h3></div>
            <div class="panel-body">
              <div>
                <div class="row">
                  <div class="col-xs-2">
                    <label class="control-label">Cantidad pallet</label>
                    <asp:TextBox ID="txtCantidadPallet" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Peso real</label>
                    <asp:TextBox ID="txtPesoReal" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Orden Compra</label>
                    <asp:TextBox ID="txtOrdenCompra" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-xs-2 sist-display-none" data-campoVisible="Exportacion">
                    <label class="control-label">Sello</label>
                    <asp:TextBox ID="txtNroSello" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-xs-4 sist-display-none" data-campoVisible="Exportacion">
                    <label class="control-label">Contenedor</label>
                    <asp:TextBox ID="txtNroContenedor" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="GestionOperacion.cerrarPopUp('0')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(GestionOperacion.validarFormularioDatoExtra())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".date-pick").datepicker({
          changeMonth: true,
          changeYear: true
        });

        jQuery("#txtFechaLlegada").mask("00/00/0000", { placeholder: "__/__/__" });
        jQuery("#txtHoraLlegada").mask("00:00", { placeholder: "__:__" });
        jQuery("#txtFechaSalida").mask("00/00/0000", { placeholder: "__/__/__" });
        jQuery("#txtHoraSalida").mask("00:00", { placeholder: "__:__" });
      });
    </script>

  </form>
</body>
</html>

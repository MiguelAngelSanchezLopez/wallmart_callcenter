﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Mantenedor.ContactoDetalle.aspx.vb" Inherits="webtransportewalmartmx.Mantenedor_ContactoDetalle" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle Contacto</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtIdEscalamientoPorAlertaContacto" runat="server" value="-1" />
    <input type="hidden" id="txtIdEscalamientoPorAlerta" runat="server" value="-1" />
    <input type="hidden" id="txtIdEscalamientoPorAlertaGrupoContacto" runat="server" value="-1" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">
          <div class="form-group">
            <label class="control-label">Contacto<strong class="text-danger">&nbsp;*</strong></label>
            <uc1:wucCombo id="ddlContacto" runat="server" FuenteDatos="Tabla" TipoCombo="ContactoEscalamiento" ItemSeleccione="true" CssClass="form-control chosen-select" funcionOnChange="Alerta.asignarCargoContacto()"></uc1:wucCombo>
          </div>
          <div class="form-group">
            <label class="control-label">Cargo<strong class="text-danger">&nbsp;*</strong></label>
            <asp:TextBox ID="txtCargo" runat="server" MaxLength="255" CssClass="form-control"></asp:TextBox>
          </div>
        </asp:Panel>
        
        <div class="form-group text-center sist-clear-both">
          <br />
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Alerta.cerrarPopUp()"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <div id="btnEliminar" runat="server" class="btn btn-lg btn-danger"><span class="glyphicon glyphicon-trash"></span>&nbsp;Eliminar</div>
          <div id="btnCrear" runat="server" class="btn btn-lg btn-primary" onclick="Alerta.validarFormularioContacto()"><span class="glyphicon glyphicon-ok"></span>&nbsp;Crear</div>
          <div id="btnModificar" runat="server" class="btn btn-lg btn-primary" onclick="Alerta.validarFormularioContacto()"><span class="glyphicon glyphicon-ok"></span>&nbsp;Modificar</div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".chosen-select").select2();

      });
    </script>

  </form>
</body>
</html>

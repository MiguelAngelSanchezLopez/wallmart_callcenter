﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master"
  CodeBehind="Scat.AcessoListado.aspx.vb" Inherits="webtransportewalmartmx.Scat_AcessoListado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">
          Control de Acceso</p>
      </div>
    </div>
  </div>
  <asp:Panel ID="pnlMensaje" runat="server">
  </asp:Panel>
  <div>
    <asp:Panel ID="pnlMensajeAcceso" runat="server">
    </asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            FILTROS DE B&Uacute;SQUEDA</h3>
        </div>
        <div class="panel-body">
          <div class="form-group row">
            <div class="col-xs-3">
              <label class="control-label">
                Nombre</label>
              <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-3">
              <br />
              <asp:CheckBox ID="chkOmiteEntrada" runat="server" CssClass="control-label" Text="&nbsp;Omitir Asistencia de Entrada" />
            </div>
            <div class="col-xs-3">
              <br />
              <asp:CheckBox ID="chkOmiteSalida" runat="server" CssClass="control-label" Text="&nbsp;Omitir Asistencia de Salida" />
            </div>
            <div class="col-xs-3">
              <label class="control-label">
                &nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Alerta.validarFormularioBusqueda())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
            </div>
          </div>
        </div>
      </div>
      <!-- RESULTADO -->
      <div class="form-group">
        <asp:Panel ID="pnlMensajeUsuario" runat="server">
        </asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%">
          <div>
            <strong>
              <asp:Label ID="lblTotalRegistros" runat="server"></asp:Label></strong></div>
          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0"
            AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered"
            EnableModelValidation="True">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField HeaderText="Nombre" SortExpression="Nombre" HeaderStyle-Width="250px"
                DataField="Nombre">
                <HeaderStyle Width="250px" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Area" SortExpression="Area" HeaderStyle-Width="160px"
                DataField="Area">
                <HeaderStyle Width="160px" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Actividad" SortExpression="Actividad" DataField="Actividad" />
              <asp:BoundField HeaderText="Fecha Hora Ingreso" SortExpression="FechaHoraIngreso"
                HeaderStyle-Width="160px" DataField="FechaHoraIngreso">
                <HeaderStyle Width="160px" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Tiempo en Tienda" SortExpression="TiempoEnLocal" HeaderStyle-Width="140px"
                DataField="TiempoEnLocal">
                <HeaderStyle Width="140px" />
              </asp:BoundField>
              <asp:BoundField HeaderText="Vehículo" SortExpression="Vehiculo" DataField="Vehiculo" />
              <asp:BoundField HeaderText="Control Salida Acceso" SortExpression="ControlSalidaAcceso"
                DataField="ControlSalidaAcceso" />
              <asp:BoundField HeaderText="Entrada" SortExpression="Entrada" DataField="Entrada" />
            </Columns>
          </asp:GridView>
        </asp:Panel>
      </div>
      <div class="form-group">
        <div class="col-xs-8">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">
                Status Tienda</h3>
            </div>
            <div class="panel-body">
              <div class="form-group row">
                <div class="col-xs-12">
                  <asp:GridView ID="gvStatusLocal" runat="server" AutoGenerateColumns="False" CellPadding="0"
                    AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered"
                    EnableModelValidation="True">
                    <Columns>
                      <asp:TemplateField HeaderText="#">
                        <HeaderStyle Width="40px" />
                        <ItemTemplate>
                          <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                        </ItemTemplate>
                      </asp:TemplateField>
                      <asp:BoundField HeaderText="Nombre" SortExpression="Nombre" HeaderStyle-Width="250px"
                        DataField="Nombre">
                        <HeaderStyle Width="250px" />
                      </asp:BoundField>
                      <asp:BoundField HeaderText="Numero Personas" SortExpression="NroPersonas" HeaderStyle-Width="160px"
                        DataField="NroPersonas">
                        <HeaderStyle Width="160px" />
                      </asp:BoundField>
                    </Columns>
                  </asp:GridView>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">
                Información</h3>
            </div>
            <div class="panel-body">
              <div class="form-group row">
                <div class="col-xs-12">
                  <asp:GridView ID="gvInformacion" runat="server" AutoGenerateColumns="False" CellPadding="0"
                    AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered"
                    EnableModelValidation="True" ShowHeader="False">
                    <Columns>
                      <asp:BoundField HeaderText="" SortExpression="" HeaderStyle-Width="250px" DataField="Info">
                        <HeaderStyle Width="250px" />
                      </asp:BoundField>
                    </Columns>
                  </asp:GridView>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </asp:Panel>
  </div>
</asp:Content>

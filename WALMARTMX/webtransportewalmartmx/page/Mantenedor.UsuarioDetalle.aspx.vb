﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_UsuarioDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    CrearTelefonoAdicional
    ModificarTelefonoAdicional
    EliminarTelefonoAdicional
  End Enum

  Private Enum eTabla As Integer
    T00_Detalle = 0
    T01_TelefonosPorUsuario = 1
    T02_TransportistasAsociados = 2
    T03_CentroDistribucionAsociados = 3
    T04_LocalesAsociados = 4
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idUsuario As String = Me.ViewState.Item("idUsuario")

    Try
      'obtiene detalle del usuario
      ds = Usuario.ObtenerDetalle(idUsuario)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario As String = Me.ViewState.Item("idUsuario")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim username, password, rut, nombre, paterno, materno As String
    Dim email, telefono, loginDias, estado, idPerfil, llavePerfil, emailConCopia, idUsuarioTransportista As String
    Dim jsonTelefonosPorUsuario, gestionarSoloCemtra As String
    Dim super, notificarPorEmailHorarioNocturno As Boolean
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    username = ""
    password = ""
    rut = ""
    nombre = ""
    paterno = ""
    materno = ""
    email = ""
    telefono = ""
    loginDias = ""
    super = False
    estado = ""
    idPerfil = ""
    llavePerfil = ""
    emailConCopia = ""
    notificarPorEmailHorarioNocturno = False
    jsonTelefonosPorUsuario = "[]"
    idUsuarioTransportista = "-1"
    gestionarSoloCemtra = "Todas"

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(eTabla.T00_Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(eTabla.T00_Detalle).Rows(0)
          username = dr.Item("Username")
          rut = dr.Item("RutCompleto")
          nombre = dr.Item("Nombre")
          paterno = dr.Item("Paterno")
          materno = dr.Item("Materno")
          email = dr.Item("Email")
          telefono = dr.Item("Telefono")
          loginDias = dr.Item("LoginDias")
          super = dr.Item("Super")
          estado = dr.Item("Estado")
          idPerfil = dr.Item("IdPerfil")
          llavePerfil = dr.Item("LlavePerfil")
          emailConCopia = dr.Item("EmailConCopia")
          notificarPorEmailHorarioNocturno = dr.Item("NotificarPorEmailHorarioNocturno")
          idUsuarioTransportista = dr.Item("IdUsuarioTransportista")
          gestionarSoloCemtra = dr.Item("GestionarSoloCemtra")

          If (ds.Tables(eTabla.T01_TelefonosPorUsuario).Rows.Count > 0) Then
            jsonTelefonosPorUsuario = MyJSON.ConvertDataTableToJSON(ds.Tables(eTabla.T01_TelefonosPorUsuario))
          End If

        End If
      End If
    Catch ex As Exception
      username = ""
      password = ""
      rut = ""
      nombre = ""
      paterno = ""
      materno = ""
      email = ""
      telefono = ""
      loginDias = ""
      super = False
      estado = ""
      idPerfil = ""
      llavePerfil = ""
      emailConCopia = ""
      notificarPorEmailHorarioNocturno = False
      jsonTelefonosPorUsuario = "[]"
      idUsuarioTransportista = "-1"
      gestionarSoloCemtra = "Todas"
    End Try

    'asigna valores
    Me.txtIdUsuario.Value = idUsuario
    Me.txtUsername.Text = username
    Me.txtRUT.Text = rut
    Me.txtNombre.Text = nombre
    Me.txtPaterno.Text = paterno
    Me.txtMaterno.Text = materno
    Me.txtEmail.Text = email
    Me.txtTelefono.Text = telefono
    Me.chkEstado.Checked = IIf(idUsuario = "-1", True, IIf(estado = "1", True, False))
    Me.lblTituloFormulario.Text = IIf(idUsuario = "-1", "Nuevo ", "Modificar ") & "Usuario"
    Me.lblIdRegistro.Text = IIf(idUsuario = "-1", "", "[ID: " & idUsuario & "]")
    Me.txtIdPerfilActual.Value = idPerfil
    Me.txtLlavePerfil.Value = llavePerfil
    Me.txtEmailConCopia.Text = emailConCopia.Replace(";", "; ")
    Me.chkNotificarPorEmailHorarioNocturno.Checked = notificarPorEmailHorarioNocturno
    Me.txtJSONTelefonosPorUsuario.Value = jsonTelefonosPorUsuario
    Me.ddlTransportistaAsociado.SelectedValue = IIf(idUsuarioTransportista = "-1", "", idUsuarioTransportista)
    Me.rbtnTeleoperadorTodasLasAlertas.Checked = IIf(gestionarSoloCemtra = "Todas", True, False)
    Me.rbtnTeleoperadorSoloCemtra.Checked = IIf(gestionarSoloCemtra = "SoloCemtra", True, False)
    Me.rbtnTeleoperadorExcluirCemtra.Checked = IIf(gestionarSoloCemtra = "ExcluirCemtra", True, False)

    FormatearLoginDias(loginDias, idUsuario)
    FiltrarComboPerfil(idUsuario, oUsuario.PerfilLlave, idPerfil)

    'carga los datos en el multiselect
    CargarMultiselect(ds, eTabla.T02_TransportistasAsociados)
    CargarMultiselect(ds, eTabla.T03_CentroDistribucionAsociados)
    CargarMultiselect(ds, eTabla.T04_LocalesAsociados)

    'muestra la matriz de permiso
    If idUsuario <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) Then
      sbScript.Append("Usuario.dibujarMatrizPermiso('" & Me.hMatrizPermiso.ClientID & "','" & idPerfil & "','" & idUsuario & "','0','" & llavePerfil & "'); Usuario.mostrarControlesPerfil({ limpiarControles: false });")
    End If
    sbScript.Append("Usuario.dibujarTelefonosPorUsuario();")
    Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "actualizaInterfaz")
  End Sub

  ''' <summary>
  ''' inicializa el combo perfil segun los filtros seleccionados
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <param name="llavePerfil"></param>
  ''' <param name="idPerfilSeleccionado"></param>
  ''' <remarks>Por VSR, 10/09/2009</remarks>
  Private Sub FiltrarComboPerfil(ByVal idUsuario As String, ByVal llavePerfil As String, ByVal idPerfilSeleccionado As String)
    Dim ds As New DataSet
    Dim item As ListItem

    Try
      If idUsuario = "-1" Then
        ds = Perfil.ObtenerListadoPorTipoPerfil(llavePerfil)
      Else
        ds = Perfil.ObtenerListadoPorTipoPerfil("-1")
      End If

      If ds Is Nothing Then
        Throw New Exception("No se pudo obtener el listado de perfiles")
      Else
        'asigna valores
        With Me.ddlPerfil
          .DataSource = ds
          'Establece propiedades del combobox que son la mismas para todos los tipos
          .DataTextField = "Texto"
          .DataValueField = "Valor"
          .DataBind()

          'asigna item "seleccione" al comienzo del combo
          item = New ListItem("--Seleccione--", "")
          'Se deja seleccionado el item Todos
          If String.IsNullOrEmpty(idPerfilSeleccionado) Then
            item.Selected = True
          Else
            .SelectedValue = idPerfilSeleccionado
          End If
          .Items.Insert(0, item)

          'verifica si el control se habilita o no
          If idUsuario = "-1" Then
            .Enabled = True
          Else
            .Enabled = IIf(llavePerfil = Perfil.KEY_ADMINISTRADOR, True, False)
          End If

          .Attributes.Add("onchange", "Usuario.validarPermisos()")
        End With
      End If
    Catch ex As Exception
      'asigna valores
      With Me.ddlPerfil
        'asigna item "seleccione" al comienzo del combo
        item = New ListItem("--Seleccione--", "")
        'Se deja seleccionado el item Todos
        item.Selected = True
        .Items.Insert(0, item)
      End With
    End Try
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()

    Me.ddlTeleoperadorTransportistasAsociados.Attributes.Add("placeholder", "Escriba un nombre")
    Me.ddlCentroDistribucionAsociados.Attributes.Add("placeholder", "Escriba un nombre")
    Me.ddlLocalesAsociados.Attributes.Add("placeholder", "Escriba un nombre")
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim idUsuario As String = Me.ViewState.Item("idUsuario")
    Dim dcJSON As New Dictionary(Of String, Boolean)

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idUsuario = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idUsuario <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnTelefonoAdicional.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.CrearTelefonoAdicional.ToString) Or _
                                      Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.ModificarTelefonoAdicional.ToString) Or _
                                      Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.EliminarTelefonoAdicional.ToString)
    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If

    'constuye json para los permisos de los telefonos adicionales
    dcJSON.Add(eFunciones.CrearTelefonoAdicional.ToString, Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.CrearTelefonoAdicional.ToString))
    dcJSON.Add(eFunciones.ModificarTelefonoAdicional.ToString, Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.ModificarTelefonoAdicional.ToString))
    dcJSON.Add(eFunciones.EliminarTelefonoAdicional.ToString, Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.EliminarTelefonoAdicional.ToString))
    Me.txtJSONPermisosTelefonoAdicional.Value = MyJSON.ConvertObjectToJSON(dcJSON)

  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el usuario en base de datos
  ''' </summary>
  ''' <param name="status"></param>
  ''' <param name="idUsuario"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Private Function GrabarDatosUsuario(ByRef status As String, ByRef idUsuario As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim username, password, nombre, paterno, materno As String
    Dim email, telefono, loginDias, super, estado, idPerfil, listadoPermisos, jsonDatosPerfil, jsonTelefonoPorUsuario As String
    Dim dv As String = ""
    Dim rut As String = ""
    Dim objJSON As Object

    Try
      username = Utilidades.IsNull(Me.txtUsername.Text, "")
      password = Utilidades.IsNull(Me.txtPassword.Text, "")
      nombre = Utilidades.IsNull(Me.txtNombre.Text, "")
      paterno = Utilidades.IsNull(Me.txtPaterno.Text, "")
      materno = Utilidades.IsNull(Me.txtMaterno.Text, "")
      email = Utilidades.IsNull(Me.txtEmail.Text, "")
      telefono = Utilidades.IsNull(Me.txtTelefono.Text, "")
      loginDias = FormatearLoginDias("", IIf(idUsuario = "-1", "0", idUsuario))
      super = IIf(Me.txtLlavePerfil.Value = Perfil.KEY_ADMINISTRADOR, "1", "0")
      estado = IIf(Me.chkEstado.Checked, "1", "0")
      idPerfil = Me.ddlPerfil.SelectedValue
      listadoPermisos = Request("chk_GrupoFunciones")
      rut = Utilidades.IsNull(Me.txtRUT.Text, "")
      jsonDatosPerfil = Utilidades.IsNull(Me.txtJSONDatosPerfil.Value, "[]")
      jsonTelefonoPorUsuario = Utilidades.IsNull(Me.txtJSONTelefonosPorUsuario.Value, "[]")

      'formatea valores
      password = IIf(String.IsNullOrEmpty(password), "", Criptografia.obtenerSHA1(password))

      If idUsuario = "-1" Then
        status = Usuario.GrabarDatos(idUsuario, rut, dv, nombre, paterno, materno, username, password, email, _
                                     telefono, loginDias, super, estado, idPerfil, listadoPermisos)
        If status = "duplicado" Then idUsuario = "-1" Else Sistema.GrabarLogSesion(oUsuario.Id, "Crea nuevo usuario: " & username)
      Else
        status = Usuario.ModificarDatos(idUsuario, rut, dv, nombre, paterno, materno, username, password, email, _
                                        telefono, loginDias, super, estado, idPerfil, listadoPermisos)
        Sistema.GrabarLogSesion(oUsuario.Id, "Modifica usuario: " & username)
      End If

      'graba datos especiales por perfil
      If (idUsuario <> "-1") Then
        'graba datos extras
        objJSON = MyJSON.ConvertJSONToObject(jsonDatosPerfil)
        GrabarDatosExtras(idUsuario, objJSON)

        'graba telefonos
        objJSON = MyJSON.ConvertJSONToObject(jsonTelefonoPorUsuario)
        GrabarTelefonosPorUsuario(idUsuario, objJSON)
      End If
    Catch ex As Exception
      msgError = "Error al grabar datos del usuario.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' formatea la entrada o salida del loginDias. Si loginDias es distinto a "" entonces asigna valor a los check, sino obtiene el valor de los check
  ''' y construye un string con ellos
  ''' </summary>
  ''' <param name="loginDias"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Private Function FormatearLoginDias(ByVal loginDias As String, ByVal idUsuario As String) As String
    Dim dias As String = ""
    Try
      If idUsuario = "-1" Then loginDias = "1111111"
      'construye un string con los valores
      If String.IsNullOrEmpty(loginDias) Then
        dias &= IIf(Me.chkLunes.Checked, "1", "0")
        dias &= IIf(Me.chkMartes.Checked, "1", "0")
        dias &= IIf(Me.chkMiercoles.Checked, "1", "0")
        dias &= IIf(Me.chkJueves.Checked, "1", "0")
        dias &= IIf(Me.chkViernes.Checked, "1", "0")
        dias &= IIf(Me.chkSabado.Checked, "1", "0")
        dias &= IIf(Me.chkDomingo.Checked, "1", "0")
      Else
        'asigna los valores a los controles
        Me.chkLunes.Checked = IIf(loginDias.Substring(0, 1) = "1", True, False)
        Me.chkMartes.Checked = IIf(loginDias.Substring(1, 1) = "1", True, False)
        Me.chkMiercoles.Checked = IIf(loginDias.Substring(2, 1) = "1", True, False)
        Me.chkJueves.Checked = IIf(loginDias.Substring(3, 1) = "1", True, False)
        Me.chkViernes.Checked = IIf(loginDias.Substring(4, 1) = "1", True, False)
        Me.chkSabado.Checked = IIf(loginDias.Substring(5, 1) = "1", True, False)
        Me.chkDomingo.Checked = IIf(loginDias.Substring(6, 1) = "1", True, False)
      End If
    Catch ex As Exception
      dias = ""
    End Try
    Return dias
  End Function

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarUsuario()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario As String = Me.ViewState.Item("idUsuario")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosUsuario(status, idUsuario)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}El USUARIO fue ingresado satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}El USUARIO fue actualizado satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}El USUARIO ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el USUARIO. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el USUARIO. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idUsuario = "-1", "", "[ID: " & idUsuario & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Usuario.cerrarPopUpUsuario('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idUsuario
      Response.Redirect("./Mantenedor.UsuarioDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' graba datos extras asociados al usuario
  ''' </summary>
  ''' <param name="idUsuario"></param>
  ''' <param name="objJSON"></param>
  ''' <remarks></remarks>
  Private Sub GrabarDatosExtras(ByVal idUsuario As String, ByVal objJSON As Object)
    Dim status, emailConCopia, notificarPorEmailHorarioNocturno, idTransportistaAsociado, gestionarSoloCemtra, teleoperadorTransportistasAsociados As String
    Dim centroDistribucionAsociados, localesAsociados As String
    Dim row As Object
    Dim totalRegistros As Integer

    Try
      totalRegistros = MyJSON.LengthObject(objJSON)
      If (totalRegistros > 0) Then
        row = objJSON(0)
        emailConCopia = MyJSON.ItemObject(row, "emailConCopia")
        notificarPorEmailHorarioNocturno = MyJSON.ItemObject(row, "notificarPorEmailHorarioNocturno")
        idTransportistaAsociado = Utilidades.IsNull(MyJSON.ItemObject(row, "idTransportistaAsociado"), "-1")
        gestionarSoloCemtra = MyJSON.ItemObject(row, "gestionarSoloCemtra")
        teleoperadorTransportistasAsociados = MyJSON.ItemObject(row, "teleoperadorTransportistasAsociados")
        centroDistribucionAsociados = MyJSON.ItemObject(row, "centroDistribucionAsociados")
        localesAsociados = MyJSON.ItemObject(row, "localesAsociados")

        'formatea valores
        emailConCopia = emailConCopia.Replace(",", ";").Replace(" ", "")

        status = Usuario.GrabarDatosExtras(idUsuario, emailConCopia, notificarPorEmailHorarioNocturno, idTransportistaAsociado, gestionarSoloCemtra, _
                                           teleoperadorTransportistasAsociados, centroDistribucionAsociados, localesAsociados)
        If (status = Sistema.eCodigoSql.Error) Then
          Throw New Exception("No se pudo grabar los datos extras del usuario")
        End If
      End If
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' graba los telefonos asociados al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarTelefonosPorUsuario(ByVal idUsuario As String, ByVal objJSON As Object)
    Dim status, idTelefonoPorUsuario, telefono, horaInicio, horaTermino, listado As String
    Dim row As Object
    Dim totalRegistros As Integer
    Dim lsListado As New List(Of String)

    Try
      totalRegistros = MyJSON.LengthObject(objJSON)

      '-------------------------------------------------------------
      'busca los telefonos que no esten referenciados y los elimina
      lsListado.Add("-1")
      For Each row In objJSON
        idTelefonoPorUsuario = MyJSON.ItemObject(row, "IdTelefonoPorUsuario")
        lsListado.Add(idTelefonoPorUsuario)
      Next
      listado = String.Join(",", lsListado.ToArray())
      'elimina los registros que no estan referenciados en la lista
      status = Usuario.EliminarTelefonosPorUsuarioNoReferenciados(idUsuario, listado)
      If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo eliminar los escalamientos que no están referenciados")

      If (totalRegistros > 0) Then
        For Each row In objJSON
          idTelefonoPorUsuario = MyJSON.ItemObject(row, "IdTelefonoPorUsuario")
          telefono = MyJSON.ItemObject(row, "Telefono")
          horaInicio = MyJSON.ItemObject(row, "HoraInicio")
          horaTermino = MyJSON.ItemObject(row, "HoraTermino")
          status = Usuario.GrabarTelefonoPorUsuario(idTelefonoPorUsuario, idUsuario, telefono, horaInicio, horaTermino)
          If (status = Sistema.eCodigoSql.Error) Then
            Throw New Exception("No se pudo grabar los teléfonos asociados al usuario")
          End If
        Next
      End If
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' carga los datos en el multiselect seleccionado
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CargarMultiselect(ByVal ds As DataSet, indice As eTabla)
    Dim ddlCombo As ListBox
    Dim dv As DataView
    Dim dt As DataTable
    Dim filtro As String

    Try
      Select Case indice
        Case eTabla.T02_TransportistasAsociados
          ddlCombo = Me.ddlTeleoperadorTransportistasAsociados
        Case eTabla.T03_CentroDistribucionAsociados
          ddlCombo = Me.ddlCentroDistribucionAsociados
        Case eTabla.T04_LocalesAsociados
          ddlCombo = Me.ddlLocalesAsociados
        Case Else
          ddlCombo = Nothing
      End Select

      dv = ds.Tables(indice).DefaultView
      ddlCombo.DataSource = dv
      ddlCombo.DataTextField = "Texto"
      ddlCombo.DataValueField = "Valor"
      ddlCombo.DataBind()

      'marca las opciones seleccionadas
      dt = ds.Tables(indice)
      For Each item As ListItem In ddlCombo.Items
        filtro = "Valor = " & item.Value & " AND Seleccionado = 1"
        item.Selected = (dt.Select(filtro).Length > 0)
      Next

    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idUsuario", idUsuario)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarUsuario()
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarUsuario()
  End Sub

End Class
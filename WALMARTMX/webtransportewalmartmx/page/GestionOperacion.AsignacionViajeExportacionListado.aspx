﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="GestionOperacion.AsignacionViajeExportacionListado.aspx.vb" Inherits="webtransportewalmartmx.GestionOperacion_AsignacionViajeExportacionListado" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.mask.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>
  <script type="text/javascript" src="../js/gestionOperacion.js"></script>

  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>
  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="form-group">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title sist-font-size-12"><strong>ASIGNACION VIAJE EXPORTACION</strong> > FILTROS DE B&Uacute;SQUEDA</h3>
          </div>

          <div id="hFiltroBuscador" runat="server" class="panel-body">
            <div class="row">
              <div class="col-xs-2">
                <label class="control-label">Fecha Presentaci&oacute;n</label>
                <asp:TextBox ID="txtFechaPresentacion" runat="server" MaxLength="10" CssClass="form-control input-sm date-pick"></asp:TextBox>
                <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 01/01/2016)</div>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Hora Presentaci&oacute;n</label>
                <asp:TextBox ID="txtHoraPresentacion" runat="server" MaxLength="5" CssClass="form-control input-sm"></asp:TextBox>
                <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 08:15)</div>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Fecha Programaci&oacute;n</label>
                <asp:TextBox ID="txtFechaProgramacion" runat="server" MaxLength="10" CssClass="form-control input-sm date-pick"></asp:TextBox>
                <div class="help-block small sist-margin-cero sist-margin-bottom-5">(ejm: 01/01/2016)</div>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Orden Servicio</label>
                <asp:TextBox ID="txtNroOrdenServicio" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
              </div>
              <div class="col-xs-2">
                <label class="control-label">ID Embarque</label>
                <asp:TextBox ID="txtNroGuiaDespacho" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
              </div>
              <div class="col-xs-2">
                <label class="control-label">S&oacute;lo con ID Embarque</label>
                <div><asp:CheckBox ID="chkSoloGuiaDespacho" runat="server" Text="" /></div>
              </div>
            </div>
            <div class="row sist-padding-bottom-5">
              <div class="col-xs-2">
                <label class="control-label">ETIS / EPORTIS</label>
                <uc1:wucCombo id="ddlClasificacionConductor" runat="server" FuenteDatos="Tabla" TipoCombo="ClasificacionConductor" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Tipo Contenedor</label>
                <uc1:wucCombo id="ddlTipoContenedor" runat="server" FuenteDatos="Tabla" TipoCombo="ExportacionMarcaContenedor" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Naviera</label>
                <uc1:wucCombo id="ddlNombreNaviera" runat="server" FuenteDatos="Tabla" TipoCombo="ExportacionNombreNaviera" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Medida</label>
                <uc1:wucCombo id="ddlDimension" runat="server" FuenteDatos="Tabla" TipoCombo="ExportacionMedidaContenedor" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Puerto Descarga</label>
                <uc1:wucCombo id="ddlPuertoDescarga" runat="server" FuenteDatos="Tabla" TipoCombo="ExportacionNombrePuertoDescarga" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Puerto Embarque</label>
                <uc1:wucCombo id="ddlPuertoEmbarque" runat="server" FuenteDatos="Tabla" TipoCombo="ExportacionNombrePuertoEmbarque" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-2">
                <label class="control-label">Contenedor</label>
                <asp:TextBox ID="txtContenedor" runat="server" MaxLength="255" CssClass="form-control input-sm"></asp:TextBox>
              </div>
              <div class="col-xs-1">
                <label class="control-label">Nro. Filas</label>
                <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control input-sm" MaxLength="4"></asp:TextBox>
              </div>
              <div class="col-xs-1">
                <label class="control-label">&nbsp;</label><br />
                <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary btn-sm" OnClientClick="return(GestionOperacion.validarFormularioBusquedaAsignacionViajeExportacion())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group sist-font-size-11">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>
          <table border="0">
            <tr>
              <td style="padding-right:60px">
                <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" PagerSettings-Position="TopAndBottom" PagerStyle-Font-Size="Larger" AllowSorting="True" CssClass="table table-striped table-bordered"
                              DataKeyNames="IdExportacion,PatenteTracto,PatenteTrailer,NombreConductor,TelefonoConductor,NumeroOrdenServicio,VersionMO,FechaLlegada,ObservacionLlegada,CantidadPallet,PesoReal,OrdenCompra,NumeroLineaMO,FechaHoraPresentacion,FechaSalida,NumeroSello,NumeroContenedorCargaSuelta,ReportabilidadPatenteTracto,ReportabilidadPatenteTrailer,ReportabilidadPatenteTractoFecha,ReportabilidadPatenteTrailerFecha,FechaAsignacionConductor">
                  <Columns>
                    <asp:TemplateField HeaderText="#">
                      <HeaderStyle Width="40px" />
                      <ItemTemplate>
                        <%# Container.DataItemIndex + 1%>
                      </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Opciones">
                      <ItemTemplate>
                        <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Orden Servicio" SortExpression="NumeroOrdenServicio" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                      <ItemTemplate>
                        <asp:Label ID="lblNumeroOrdenServicio" runat="server"></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="NumeroGuiaDespacho" HeaderText="ID Embarque" SortExpression="NumeroGuiaDespacho" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="NombreCliente" HeaderText="Cliente" SortExpression="NombreCliente" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="LugarPresentacion" HeaderText="Lugar de Presentaci&oacute;n" SortExpression="LugarPresentacion" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="SegundoDestino" HeaderText="Segundo Destino" SortExpression="SegundoDestino" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="FechaHoraPresentacion" HeaderText="Fecha Presentaci&oacute;n" SortExpression="FechaHoraPresentacionOrden" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="MedidaContenedor" HeaderText="Medida" SortExpression="MedidaContenedor" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="PesoNetoCarga" HeaderText="Peso" SortExpression="PesoNetoCarga" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="NumeroContenedor" HeaderText="Contenedor" SortExpression="NumeroContenedor" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="DepositoRetiro" HeaderText="Dep&oacute;sito Retiro" SortExpression="DepositoRetiro" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="Reserva" HeaderText="Reserva" SortExpression="Reserva" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="NombreNave" HeaderText="Nave" SortExpression="NombreNave" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="FechaHoraProgramacion" HeaderText="Fecha Programaci&oacute;n" SortExpression="FechaHoraProgramacion" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" SortExpression="Observaciones" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:BoundField DataField="EstadoViaje" HeaderText="Estado Viaje" SortExpression="EstadoViaje" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                    <asp:TemplateField HeaderText="Operador" SortExpression="NombreConductor" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                      <ItemTemplate>
                        <asp:Label ID="lblConductor" runat="server"></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Datos Llegada" SortExpression="NumeroOrdenServicio" HeaderStyle-Wrap="false">
                      <ItemTemplate>
                        <asp:Label ID="lblDatosLlegada" runat="server"></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Observación Llegada" SortExpression="NumeroOrdenServicio" HeaderStyle-Wrap="false">
                      <ItemTemplate>
                        <asp:Label ID="lblDatosObservacionLlegada" runat="server"></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Datos Carga Suelta" SortExpression="NumeroOrdenServicio" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                      <ItemTemplate>
                        <asp:Label ID="lblDatosCargaSuelta" runat="server"></asp:Label>
                      </ItemTemplate>
                    </asp:TemplateField>

                  </Columns>
                </asp:GridView>
              </td>
            </tr>
          </table>
        </asp:Panel>
      </div>

      <div class="form-group text-center">
        <div id="btnExportar" runat="server" visible="false" class="btn btn-success btn-lg" onclick="Reporte.exportar({ prefijoControl: Sistema.PREFIJO_CONTROL, invocadoDesde: Reporte.FORMULARIO_VIAJE_EXPORTACION_LISTADO, llaveReporte: 'ViajeExportacionBuscador' })"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar datos</div>
        <div id="lblMensajeGeneracionReporte"></div>
      </div>

    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });

      jQuery(".chosen-select").select2();
      jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaPresentacion").mask("00/00/0000", { placeholder: "__/__/__" });
      jQuery("#" + Sistema.PREFIJO_CONTROL + "txtHoraPresentacion").mask("00:00", { placeholder: "__:__" });
      jQuery("#" + Sistema.PREFIJO_CONTROL + "txtFechaProgramacion").mask("00/00/0000", { placeholder: "__/__/__" });
    });
  </script>

</asp:Content>

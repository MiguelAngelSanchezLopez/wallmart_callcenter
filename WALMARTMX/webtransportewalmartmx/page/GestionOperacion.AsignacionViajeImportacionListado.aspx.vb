﻿Imports CapaNegocio
Imports System.Data

Public Class GestionOperacion_AsignacionViajeImportacionListado
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    MostrarFiltroETISEPORTIS
    ExportarExcel
    AsignarConductor
    Observacion
    DatosHorarioLlegada
    DatosCargaSuelta
  End Enum
#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim cargadoDesdePopUp As String = Me.txtCargaDesdePopUp.Value

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    If cargadoDesdePopUp = "1" Then
      ObtenerDatos()
      ActualizaInterfaz()
    End If

    MostrarMensajeUsuario()
    Me.txtCargaDesdePopUp.Value = "0"
  End Sub

  Protected Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim pageSize As String = Trim(txtFiltroRegistrosPorPagina.Text)
    If pageSize = "" Or pageSize = "0" Then
      pageSize = Utilidades.ObtenerRegistrosPorPaginaDefault()
      txtFiltroRegistrosPorPagina.Text = pageSize
    End If
    'asigna total de registros por pagina
    Me.gvPrincipal.PageSize = pageSize
    'Muestra la primera pagina del paginador
    Me.gvPrincipal.PageIndex = 0

    ObtenerDatos()
    ActualizaInterfaz()
    Sistema.GrabarLogSesion(oUsuario.Id, "Busca asignaciones viaje importación")
  End Sub

  Protected Sub gvPrincipal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrincipal.PageIndexChanging
    'llama metodo general para paginar
    GridViewPageIndexChanging(Me.gvPrincipal, e)
  End Sub

  Protected Sub gvPrincipal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPrincipal.RowDataBound
    Dim lblOpciones, lblConductor, lblDatosLlegada, lblDatosObservacionLlegada, lblDatosCargaSuelta, lblNumeroOrdenServicio As Label
    Dim opciones As String = ""
    Dim idImportacion, patenteTracto, patenteTrailer, patentes, nombreConductor, telefonoConductor, datosConductor, numeroOrdenServicio, versionMO As String
    Dim fechaLlegada, observacionLlegada, cantidadPallet, pesoReal, ordenCompra, datosLlegada, datosObservacionLlegada, datosCargaSuelta, numeroLineaMO, fechaHoraPresentacion As String
    Dim fechaSalida, nroSello, nroContenedorCargaSuelta, datosNumeroOrdenServicio, reportabilidadPatenteTracto, reportabilidadPatenteTrailer As String
    Dim reportabilidadPatenteTractoFecha, reportabilidadPatenteTrailerFecha, horaAsignacionConductor As String
    Dim fechaAsignacionConductor As DateTime

    If e.Row.RowType = DataControlRowType.DataRow Then
      lblOpciones = CType(e.Row.FindControl("lblOpciones"), Label)
      lblConductor = CType(e.Row.FindControl("lblConductor"), Label)
      lblDatosLlegada = CType(e.Row.FindControl("lblDatosLlegada"), Label)
      lblDatosObservacionLlegada = CType(e.Row.FindControl("lblDatosObservacionLlegada"), Label)
      lblDatosCargaSuelta = CType(e.Row.FindControl("lblDatosCargaSuelta"), Label)
      lblNumeroOrdenServicio = CType(e.Row.FindControl("lblNumeroOrdenServicio"), Label)
      patentes = ""
      datosConductor = ""
      datosLlegada = ""
      datosObservacionLlegada = ""
      datosCargaSuelta = ""
      datosNumeroOrdenServicio = ""
      idImportacion = gvPrincipal.DataKeys(e.Row.RowIndex).Values("IdImportacion")
      patenteTracto = gvPrincipal.DataKeys(e.Row.RowIndex).Values("PatenteTracto")
      patenteTrailer = gvPrincipal.DataKeys(e.Row.RowIndex).Values("PatenteTrailer")
      nombreConductor = gvPrincipal.DataKeys(e.Row.RowIndex).Values("NombreConductor")
      telefonoConductor = gvPrincipal.DataKeys(e.Row.RowIndex).Values("TelefonoConductor")
      numeroOrdenServicio = gvPrincipal.DataKeys(e.Row.RowIndex).Values("NumeroOrdenServicio")
      versionMO = gvPrincipal.DataKeys(e.Row.RowIndex).Values("VersionMO")
      fechaLlegada = gvPrincipal.DataKeys(e.Row.RowIndex).Values("FechaLlegada")
      observacionLlegada = gvPrincipal.DataKeys(e.Row.RowIndex).Values("ObservacionLlegada")
      cantidadPallet = gvPrincipal.DataKeys(e.Row.RowIndex).Values("CantidadPallet")
      pesoReal = gvPrincipal.DataKeys(e.Row.RowIndex).Values("PesoReal")
      ordenCompra = gvPrincipal.DataKeys(e.Row.RowIndex).Values("OrdenCompra")
      numeroLineaMO = gvPrincipal.DataKeys(e.Row.RowIndex).Values("NumeroLineaMO")
      fechaHoraPresentacion = gvPrincipal.DataKeys(e.Row.RowIndex).Values("FechaHoraPresentacion")
      fechaSalida = gvPrincipal.DataKeys(e.Row.RowIndex).Values("FechaSalida")
      nroSello = gvPrincipal.DataKeys(e.Row.RowIndex).Values("NumeroSello")
      nroContenedorCargaSuelta = gvPrincipal.DataKeys(e.Row.RowIndex).Values("NumeroContenedorCargaSuelta")
      reportabilidadPatenteTracto = gvPrincipal.DataKeys(e.Row.RowIndex).Values("ReportabilidadPatenteTracto")
      reportabilidadPatenteTractoFecha = gvPrincipal.DataKeys(e.Row.RowIndex).Values("ReportabilidadPatenteTractoFecha")
      reportabilidadPatenteTrailer = gvPrincipal.DataKeys(e.Row.RowIndex).Values("ReportabilidadPatenteTrailer")
      reportabilidadPatenteTrailerFecha = gvPrincipal.DataKeys(e.Row.RowIndex).Values("ReportabilidadPatenteTrailerFecha")
      fechaAsignacionConductor = gvPrincipal.DataKeys(e.Row.RowIndex).Values("FechaAsignacionConductor")

      '-----------------------------------
      datosNumeroOrdenServicio &= "<div class=""sist-padding-bottom-3""><strong>Orden Servicio:</strong> " & numeroOrdenServicio & "</div>"
      datosNumeroOrdenServicio &= "<div class=""sist-padding-bottom-3""><strong>L&iacute;nea:</strong> " & numeroLineaMO & "</div>"
      datosNumeroOrdenServicio &= "<div class=""sist-padding-bottom-3""><strong>Versi&oacute;n:</strong> " & versionMO & "</div>"
      lblNumeroOrdenServicio.Text = datosNumeroOrdenServicio

      '-----------------------------------
      reportabilidadPatenteTracto = Utilidades.ObtenerSemaforoReportabilidad(reportabilidadPatenteTracto, reportabilidadPatenteTractoFecha)
      reportabilidadPatenteTrailer = "" 'Utilidades.ObtenerSemaforoReportabilidad(reportabilidadPatenteTrailer, reportabilidadPatenteTrailerFecha)

      If (Not String.IsNullOrEmpty(patenteTracto)) Then patentes &= IIf(String.IsNullOrEmpty(patentes), "<strong>Tracto</strong>: " & patenteTracto, "<br /><strong>Tracto</strong>: " & patenteTracto) & " " & reportabilidadPatenteTracto
      If (Not String.IsNullOrEmpty(patenteTrailer)) Then patentes &= IIf(String.IsNullOrEmpty(patentes), "<strong>Remolque</strong>: " & patenteTrailer, "<br /><strong>Remolque</strong>: " & patenteTrailer) & " " & reportabilidadPatenteTrailer
      datosConductor &= "<div class=""sist-padding-bottom-3"">" & nombreConductor & "</div>"
      datosConductor &= "<div class=""sist-padding-bottom-3"">" & patentes & "</div>"
      datosConductor &= "<div class=""sist-padding-bottom-3"">" & IIf(String.IsNullOrEmpty(telefonoConductor), "", "<strong>Tel&eacute;fono</strong>: " & telefonoConductor) & "</div>"
      lblConductor.Text = datosConductor

      '-----------------------------------
      If (Not String.IsNullOrEmpty(fechaLlegada)) Then datosLlegada &= "<div style=""width:120px"" div class=""sist-padding-bottom-3""><strong>Fecha llegada:</strong> " & fechaLlegada & "</div>"
      If (Not String.IsNullOrEmpty(fechaSalida)) Then datosLlegada &= "<div style=""width:120px"" div class=""sist-padding-bottom-3""><strong>Fecha salida:</strong> " & fechaSalida & "</div>"
      'If (Not String.IsNullOrEmpty(observacionLlegada)) Then datosLlegada &= "<div style=""width:300px"" class=""sist-padding-bottom-3""><strong>Observaci&oacute;n:</strong> " & observacionLlegada & "</div>"
      lblDatosLlegada.Text = datosLlegada

      '-----------------------------------
      If (Not String.IsNullOrEmpty(observacionLlegada)) Then datosObservacionLlegada &= "<div style=""width:300px"" class=""sist-padding-bottom-3""><strong>Observaci&oacute;n:</strong> " & observacionLlegada & "</div>"
      lblDatosObservacionLlegada.Text = datosObservacionLlegada

      '-----------------------------------
      If (cantidadPallet <> "-1") Then datosCargaSuelta &= "<div class=""sist-padding-bottom-3""><strong>Cantidad pallet:</strong> " & cantidadPallet & "</div>"
      If (pesoReal <> "-1") Then datosCargaSuelta &= "<div class=""sist-padding-bottom-3""><strong>Peso real:</strong> " & pesoReal & "</div>"
      If (ordenCompra <> "-1") Then datosCargaSuelta &= "<div class=""sist-padding-bottom-3""><strong>Orden compra:</strong> " & ordenCompra & "</div>"
      If (nroSello <> "") Then datosCargaSuelta &= "<div class=""sist-padding-bottom-3""><strong>Nro Sello:</strong> " & nroSello & "</div>"
      If (nroContenedorCargaSuelta <> "") Then datosCargaSuelta &= "<div class=""sist-padding-bottom-3""><strong>Nro Contenedor:</strong> " & nroContenedorCargaSuelta & "</div>"
      lblDatosCargaSuelta.Text = datosCargaSuelta

      '-----------------------------------
      opciones &= "<div class=""btn-group dropup"">"
      opciones &= "  <button type=""button"" class=""btn btn-sm btn-default dropdown-toggle"" data-toggle=""dropdown"">"
      opciones &= "    <span class=""glyphicon glyphicon-pencil""></span>"
      opciones &= "  </button>"
      opciones &= "  <ul class=""dropdown-menu"" role=""menu"">"
      If (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.AsignarConductor.ToString)) Then opciones &= "    <li><a href=""javascript:;"" onclick=""GestionOperacion.abrirFormularioAsignacionViajeImportacion({ idImportacion:'" & idImportacion & "' })"" class=""sist-font-size-12""><span class=""glyphicon glyphicon-user""></span>&nbsp;&nbsp;Asignar Operador</a></li>"
      opciones &= "    <li><a href=""javascript:;"" onclick=""GestionOperacion.verHistorialAsignacionViaje({ numeroOrdenServicio:'" & numeroOrdenServicio & "', numeroLineaMO:'" & numeroLineaMO & "', versionMO:'" & versionMO & "', entidad: GestionOperacion.IMPORTACION })"" class=""sist-font-size-12""><span class=""glyphicon glyphicon-list-alt""></span>&nbsp;&nbsp;Ver historial MO</a></li>"
      If (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Observacion.ToString)) Then opciones &= "    <li><a href=""javascript:;"" onclick=""GestionOperacion.abrirFormularioDatoExtra({ idRegistro:'" & idImportacion & "', entidad: GestionOperacion.IMPORTACION, bloque: GestionOperacion.BLOQUE_DATOS_LLEGADA, fechaHoraPresentacion:'" & fechaHoraPresentacion & "', campoMostrar: GestionOperacion.CAMPO_MOSTRAR_OBSERVACION })"" class=""sist-font-size-12""><span class=""glyphicon glyphicon-edit""></span>&nbsp;&nbsp;Observaci&oacute;n</a></li>"
      If (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.DatosHorarioLlegada.ToString)) Then opciones &= "    <li><a href=""javascript:;"" onclick=""GestionOperacion.abrirFormularioDatoExtra({ idRegistro:'" & idImportacion & "', entidad: GestionOperacion.IMPORTACION, bloque: GestionOperacion.BLOQUE_DATOS_LLEGADA, fechaHoraPresentacion:'" & fechaHoraPresentacion & "', campoMostrar: GestionOperacion.CAMPO_MOSTRAR_FECHA_LLEGADA })"" class=""sist-font-size-12""><span class=""glyphicon glyphicon-time""></span>&nbsp;&nbsp;Datos horario llegada</a></li>"
      If (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.DatosCargaSuelta.ToString)) Then opciones &= "    <li><a href=""javascript:;"" onclick=""GestionOperacion.abrirFormularioDatoExtra({ idRegistro:'" & idImportacion & "', entidad: GestionOperacion.IMPORTACION, bloque: GestionOperacion.BLOQUE_CARGA_SUELTA, fechaHoraPresentacion:'" & fechaHoraPresentacion & "', campoMostrar:'' })"" class=""sist-font-size-12""><span class=""glyphicon glyphicon-road""></span>&nbsp;&nbsp;Datos carga suelta</a></li>"
      opciones &= "  </ul>"
      opciones &= "</div>"
      lblOpciones.Text = opciones

      If (Not String.IsNullOrEmpty(nombreConductor)) Then
        horaAsignacionConductor = fechaAsignacionConductor.ToString("HHmm")
        If (horaAsignacionConductor > "1500") Then
          e.Row.Attributes.Add("class", "danger")
        Else
          e.Row.Attributes.Add("class", "success")
        End If
      End If
    End If
  End Sub

  Protected Sub gvPrincipal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPrincipal.Sorting
    'llama metodo general para ordenar
    GridViewSorting(e)
  End Sub

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario, fechaPresentacion, horaPresentacion, nombreCliente, tipoCarga, dimension, retiro, puertoDescarga, diasLibres, nroOrdenServicio As String
    Dim fechaProgramacion, clasificacionConductor, nroGuiaDespacho, soloGuiaDespacho, contenedor As String

    'obtiene valores de los controles
    idUsuario = oUsuario.Id
    tipoCarga = "-1"
    fechaPresentacion = Utilidades.IsNull(Me.txtFechaPresentacion.Text, "-1")
    horaPresentacion = Utilidades.IsNull(Me.txtHoraPresentacion.Text, "-1")
    nombreCliente = Utilidades.IsNull(Me.ddlNombreCliente.SelectedValue, "-1")
    dimension = Utilidades.IsNull(Me.ddlDimension.SelectedValue, "-1")
    retiro = Utilidades.IsNull(Me.ddlRetiro.SelectedValue, "-1")
    puertoDescarga = Utilidades.IsNull(Me.ddlPuertoDescarga.SelectedValue, "-1")
    diasLibres = Utilidades.IsNull(Me.ddlDiasLibres.SelectedValue, "-1")
    nroOrdenServicio = Utilidades.IsNull(Me.txtNroOrdenServicio.Text, "-1")
    fechaProgramacion = Utilidades.IsNull(Me.txtFechaProgramacion.Text, "-1")
    clasificacionConductor = Utilidades.IsNull(Me.ddlClasificacionConductor.SelectedValue, "-1")
    nroGuiaDespacho = Utilidades.IsNull(Me.txtNroGuiaDespacho.Text, "-1")
    soloGuiaDespacho = IIf(Me.chkSoloGuiaDespacho.Checked, "1", "0")
    contenedor = Utilidades.IsNull(Me.txtContenedor.Text, "-1")

    'obtiene listado
    ds = GestionOperacion.ObtenerListadoAsignacionViajesImportacion(idUsuario, fechaPresentacion, horaPresentacion, nombreCliente, tipoCarga, dimension, retiro, puertoDescarga, _
                                                                    diasLibres, nroOrdenServicio, fechaProgramacion, clasificacionConductor, nroGuiaDespacho, soloGuiaDespacho, contenedor)

    'guarda el resultado en session
    If ds Is Nothing Then
      Session("dv") = Nothing
    Else
      Session("dv") = ds.Tables(0).DefaultView
    End If
    oUtilidades.GrabarObjectSession(Reporte.KEY_SESION_DATASET, ds)
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim dv As New DataView
    Dim totalRegistros As Integer

    dv = CType(Session("dv"), DataView)
    If dv Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = dv.Table.Rows.Count
    End If

    If totalRegistros > 0 Then
      Me.pnlMensajeUsuario.Visible = False
      Me.pnlHolderGrilla.Visible = True
      Me.btnExportar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.ExportarExcel.ToString)
      'carga la grilla a utilizar
      Me.gvPrincipal.Visible = True
      Me.gvPrincipal.DataSource = dv
      Me.gvPrincipal.DataBind()
      Utilidades.MostrarTotalRegistrosGrilla(dv, Me.lblTotalRegistros)
    Else
      Me.pnlHolderGrilla.Visible = False
      Me.pnlMensajeUsuario.Visible = True
      Me.btnExportar.Visible = False
      Dim TextoMensaje As String = "No se encontraron registros"
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)
      'oculta la grilla
      Me.gvPrincipal.Visible = False
    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim registrosPorPagina As String = Me.txtFiltroRegistrosPorPagina.Text

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    '-- inicializacion de controles que NO dependen del postBack --
    If registrosPorPagina = "" Then
      Me.txtFiltroRegistrosPorPagina.Text = Utilidades.ObtenerRegistrosPorPaginaDefault()
    End If

    Me.ddlClasificacionConductor.SelectedValue = oUsuario.ClasificacionConductor
    Me.ddlClasificacionConductor.Enabled = False

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.ddlClasificacionConductor.Enabled = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                                           Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.MostrarFiltroETISEPORTIS.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' metodo general para la paginacion de la grilla
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GridViewPageIndexChanging(ByVal gridView As GridView, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    Try
      'Cambia de pagina
      gridView.PageIndex = e.NewPageIndex
      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  ''' <summary>
  ''' metodo general para ordenar segun grilla seleccionada
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GridViewSorting(ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    Dim oUtilidades As New Utilidades
    Dim setearOrdenInicial As Boolean = False
    Try
      'Recupera el dataset para ordenar
      Dim dv As DataView = CType(Session("dv"), DataView)

      'determina el campo por el cual se ordenara
      Dim strOrdenarPor As String = e.SortExpression

      'Si ya se habia ordenado por ese campo, ordena pero en forma descendente
      If Not dv Is Nothing Then
        If strOrdenarPor = dv.Sort Then
          strOrdenarPor = strOrdenarPor & " DESC"
        End If
        'Ordena la grilla de acuerdo a la columna seleccionada
        dv.Sort = strOrdenarPor
      End If

      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{SIN_DATOS}") > 0 Then
          mensaje = mensaje.Replace("{SIN_DATOS}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Warning, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensaje.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region

End Class
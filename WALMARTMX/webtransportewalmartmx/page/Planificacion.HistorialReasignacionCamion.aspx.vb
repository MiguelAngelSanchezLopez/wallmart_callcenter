﻿Imports CapaNegocio
Imports System.Data

Public Class Planificacion_HistorialReasignacionCamion
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlanificacion As String = Me.ViewState.Item("idPlanificacion")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")

    'obtiene listado
    ds = PlanificacionTransportista.ObtenerHistorialReasignacionCamion(idPlanificacion, nroTransporte)

    'guarda el resultado en session
    Me.ViewState.Add("ds", ds)
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim ds As DataSet = Me.ViewState.Item("ds")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")

    Me.lblIdRegistro.Text = "[IdMaster: " & nroTransporte & "]"
    Me.ltlTablaDatos.Text = DibujarTabla(ds)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' dibuja el listado de historial de reasignacion del camion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarTabla(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim totalRegistros As Integer
    Dim sb As New StringBuilder
    Dim templateAux, htmlSinRegistro, nroTransporte, fechaModificacion, campoModificado, observacion, modificadoPor As String

    Try
      template &= "<tr>"
      template &= "  <td>{FECHA_MODIFICACION}</td>"
      template &= "  <td>{MODIFICADO_POR}</td>"
      template &= "  <td>{CAMPO_MODIFICADO}</td>"
      template &= "  <td>{OBSERVACION}</td>"
      template &= "</tr>"

      htmlSinRegistro = Utilidades.MostrarMensajeUsuario("No hay historial asociado", Utilidades.eTipoMensajeAlert.Warning)

      If (ds Is Nothing) Then
        html = htmlSinRegistro
      Else
        totalRegistros = ds.Tables(0).Rows.Count

        If (totalRegistros = 0) Then
          html = htmlSinRegistro
        Else
          sb.Append("<div class=""small"">")
          sb.Append("<table class=""table table-condensed table-striped"">")
          sb.Append("  <thead>")
          sb.Append("    <tr>")
          sb.Append("      <th style=""width:150px"">Fecha Modificaci&oacute;n</th>")
          sb.Append("      <th style=""width:200px"">Modificado Por</th>")
          sb.Append("      <th>Campos Modificados</th>")
          sb.Append("      <th style=""width:350px"">Observaci&oacute;n</th>")
          sb.Append("    </tr>")
          sb.Append("  </thead>")
          sb.Append("  <tbody>")

          For Each dr As DataRow In ds.Tables(0).Rows
            templateAux = template
            nroTransporte = dr.Item("NroTransporte")
            fechaModificacion = dr.Item("FechaModificacion")
            campoModificado = dr.Item("CampoModificado")
            observacion = dr.Item("Observacion")
            modificadoPor = dr.Item("ModificadoPor")

            'formatea valores
            campoModificado = DibujarCamposModificados(campoModificado)

            templateAux = templateAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
            templateAux = templateAux.Replace("{FECHA_MODIFICACION}", fechaModificacion)
            templateAux = templateAux.Replace("{MODIFICADO_POR}", Server.HtmlEncode(modificadoPor))
            templateAux = templateAux.Replace("{CAMPO_MODIFICADO}", campoModificado)
            templateAux = templateAux.Replace("{OBSERVACION}", Server.HtmlEncode(observacion))
            sb.Append(templateAux)
          Next

          sb.Append("  </tbody>")
          sb.Append("</table>")
          sb.Append("</div>")
          html = sb.ToString()
        End If
      End If

    Catch ex As Exception
      html = Utilidades.MostrarMensajeUsuario(ex.Message, Utilidades.eTipoMensajeAlert.Danger)
    End Try

    Return html
  End Function

  ''' <summary>
  ''' dibuja los campos modificados en formato json
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarCamposModificados(ByVal json As String) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim templateLocales As String = ""
    Dim htmlSinDato As String = "<em>Sin dato</em>"
    Dim objJSON, row, rowObjectJSON, objValorActual, objValorModificado As Object
    Dim templateAux, campoModificado, valorActual, valorModificado As String
    Dim sb As New StringBuilder
    Dim sbLocales As New StringBuilder
    Dim formatearHtmlEncode As Boolean

    Try
      template &= "<li class=""sist-padding-bottom-3"">"
      template &= "  <strong>- {CAMPO_MODIFICADO}:</strong> se cambia &quot;{VALOR_ACTUAL}&quot; por &quot;{VALOR_MODIFICADO}&quot;"
      template &= "</li>"

      templateLocales &= "<li class=""sist-padding-bottom-3"">"
      templateLocales &= "  <strong>- {CAMPO_MODIFICADO}:</strong><br />&nbsp;&nbsp;se cambia {VALOR_ACTUAL}&nbsp;&nbsp;por {VALOR_MODIFICADO}"
      templateLocales &= "</li>"

      objJSON = MyJSON.ConvertJSONToObject(json)

      If (MyJSON.LengthObject(objJSON) > 0) Then
        sb.Append("<ul class=""list-unstyled"">")

        For Each row In objJSON
          campoModificado = MyJSON.ItemObject(row, "CampoModificado")

          Select Case campoModificado
            Case "Locales"
              templateAux = templateLocales
              formatearHtmlEncode = False

              objValorActual = MyJSON.ItemObjectJSON(row, "ValorActual")
              objValorModificado = MyJSON.ItemObjectJSON(row, "ValorModificado")

              'obtiene los locales del valor actual
              sbLocales = New StringBuilder
              sbLocales.Append("<ul>")
              For Each rowObjectJSON In objValorActual
                sbLocales.Append("<li>" & MyJSON.ItemObject(rowObjectJSON, "OrdenEntrega") & ") " & MyJSON.ItemObject(rowObjectJSON, "NombreLocal") & "</li>")
              Next
              sbLocales.Append("</ul>")
              valorActual = sbLocales.ToString()

              'obtiene los locales del valor modificado
              sbLocales = New StringBuilder
              sbLocales.Append("<ul>")
              For Each rowObjectJSON In objValorModificado
                sbLocales.Append("<li>" & MyJSON.ItemObject(rowObjectJSON, "OrdenEntrega") & ") " & MyJSON.ItemObject(rowObjectJSON, "NombreLocal") & "</li>")
              Next
              sbLocales.Append("</ul>")
              valorModificado = sbLocales.ToString()
            Case Else
              templateAux = template
              formatearHtmlEncode = True

              valorActual = MyJSON.ItemObject(row, "ValorActual")
              valorModificado = MyJSON.ItemObject(row, "ValorModificado")
          End Select

          'formatea valores
          If (formatearHtmlEncode) Then
            valorActual = IIf(String.IsNullOrEmpty(valorActual), htmlSinDato, Server.HtmlEncode(valorActual))
            valorModificado = IIf(String.IsNullOrEmpty(valorModificado), htmlSinDato, Server.HtmlEncode(valorModificado))
          Else
            valorActual = IIf(String.IsNullOrEmpty(valorActual), htmlSinDato, valorActual)
            valorModificado = IIf(String.IsNullOrEmpty(valorModificado), htmlSinDato, valorModificado)
          End If

          templateAux = templateAux.Replace("{CAMPO_MODIFICADO}", campoModificado)
          templateAux = templateAux.Replace("{VALOR_ACTUAL}", valorActual)
          templateAux = templateAux.Replace("{VALOR_MODIFICADO}", valorModificado)
          sb.Append(templateAux)
        Next

        sb.Append("</ul>")
        html = sb.ToString()

      End If
    Catch ex As Exception
      html = ""
    End Try

    Return html
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlanificacion As String = Utilidades.IsNull(Request("idPlanificacion"), "-1")
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")

    Me.ViewState.Add("idPlanificacion", idPlanificacion)
    Me.ViewState.Add("nroTransporte", nroTransporte)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

End Class
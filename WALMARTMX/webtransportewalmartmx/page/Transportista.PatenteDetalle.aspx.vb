﻿Imports CapaNegocio
Imports System.Data

Public Class Transportista_PatenteDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

  Enum eTabla As Integer
    T00_Patentes = 0
  End Enum

  Public Const ENTIDAD_DOC_TRANSPORTISTA As String = "DocumentoConductor"

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks>Por VSR, 23/01/2009</remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idPlacaPatente As Integer = Me.ViewState.Item("idPlacaPatente")
    Dim idTransportista As Integer = Me.ViewState.Item("idTransportista")

    Try
      'obtiene detalle del conductor
      ds = Transportista.ObtenerDetallePatente(idPlacaPatente)

    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlacaPatente As String = Me.ViewState.Item("idPlacaPatente")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim placaPatente, marca, tipo As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim html = ""
    Dim dvTipoDocumento As New DataView

    'inicializa controles en vacio
    idPlacaPatente = ""
    placaPatente = ""
    marca = ""
    tipo = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(eTabla.T00_Patentes).Rows.Count

        If totalRegistros > 0 Then
          dr = ds.Tables(0).Rows(0)
          placaPatente = dr.Item("placaPatente")
          marca = dr.Item("marca")
          tipo = dr.Item("tipo")
        End If

      End If
    Catch ex As Exception
      idPlacaPatente = ""
      placaPatente = ""
      marca = ""
      tipo = ""
    End Try

    'asigna valores
    Me.txtPlacaPatente1.Text = Left(placaPatente, 2)
    Me.txtPlacaPatente2.Text = Right(Left(placaPatente, 4), 2)
    Me.txtPlacaPatente3.Text = Right(placaPatente, 2)
    Me.txtMarca.Text = marca
    Me.ddlTipo.Value = tipo

    'Dim sbScript As New StringBuilder
    'If (Not String.IsNullOrEmpty(sbScript.ToString())) Then Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "scriptActualizaInterfaz", False)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim idPlacaPatente As String = Me.ViewState.Item("idPlacaPatente")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idPlacaPatente = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             idPlacaPatente <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el usuario en base de datos
  ''' </summary>
  ''' <param name="status"></param>
  ''' <param name="idPlacaPatente"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Private Function GrabarDatosPatente(ByRef status As String, ByRef idPlacaPatente As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim placaPatente, marca, tipo As String
    Dim dv As String = ""
    Dim idTransportista As String

    Try
      idPlacaPatente = Utilidades.IsNull(Me.txtIdConductor.Value, "")
      idTransportista = Utilidades.IsNull(oUsuario.Id, "")
      placaPatente = Utilidades.IsNull(Me.txtPlacaPatente1.Text, "")
      placaPatente &= Utilidades.IsNull(Me.txtPlacaPatente2.Text, "")
      placaPatente &= Utilidades.IsNull(Me.txtPlacaPatente3.Text, "")
      marca = Utilidades.IsNull(Me.txtMarca.Text, "")
      tipo = Me.ddlTipo.Value

      status = Transportista.GrabarDatosPatente(idPlacaPatente, placaPatente, marca, tipo, oUsuario.Id)

      If idPlacaPatente = "-1" Then
        status = "ingresado"
        Sistema.GrabarLogSesion(oUsuario.Id, "Crea nueva Placa: " & idPlacaPatente)
      Else
        status = "modificado"
        Sistema.GrabarLogSesion(idPlacaPatente, "Modifica Placa: " & idPlacaPatente)
      End If

    Catch ex As Exception
      msgError = "Error al grabar datos de la placa.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarPatente()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlacaPatente As String = Me.ViewState.Item("idPlacaPatente")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""
    Dim IdTipoDocumento As String = ""

    textoMensaje &= GrabarDatosPatente(status, idPlacaPatente)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La Placa fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La Placa fue actualizada satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}La Placa ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la Placa. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la Placa. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idPlacaPatente = "-1", "", "[ID: " & idPlacaPatente & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Transportista.cerrarPopUpFichaConductor('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idPlacaPatente
      Response.Redirect("./Transportista.PatenteDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlacaPatente As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idPlacaPatente", idPlacaPatente)
    Me.ViewState.Add("idTransportista", oUsuario.Id)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If

  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarPatente()
  End Sub
End Class
﻿Imports CapaNegocio

Public Class Carrier_Home
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If (oUsuario.PerfilLlave = Carrier.PERFIL_LINEA_TRANSPORTE) Then
      dvTransportista.Visible = True
    Else
      dvTransportista.Visible = False
      hIdTransportista.Value = oUsuario.IdTransportista
    End If

  End Sub

End Class
﻿Imports CapaNegocio
Imports System.Data

Public Class GestionOperacion_AsignacionConductorManual
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Asignar
  End Enum

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim rutConductorAsignado As String = Utilidades.IsNull(Request("rutConductorAsignado"), "")
    Dim cargadoDesdePopUp As String = Me.txtCargaDesdePopUp.Value

    Me.ViewState.Add("rutConductorAsignado", rutConductorAsignado)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    If cargadoDesdePopUp = "1" Then
      ObtenerDatos()
      ActualizaInterfaz()
    End If

    MostrarMensajeUsuario()
    Me.txtCargaDesdePopUp.Value = "0"
  End Sub

  Protected Sub btnFiltrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    ObtenerDatos()
    ActualizaInterfaz()
    Sistema.GrabarLogSesion(oUsuario.Id, "Busca operadores para asignación manual")
  End Sub


#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim rutConductorAsignado As String = Me.ViewState.Item("rutConductorAsignado")
    Dim nombre, rut, patente As String

    Try
      nombre = Utilidades.IsNull(Me.txtNombre.Text, "-1")
      rut = Utilidades.IsNull(Me.txtRut.Text, "-1")
      patente = Utilidades.IsNull(Me.txtPatente.Text, "-1")

      ds = GestionOperacion.ObtenerListadoConductoresAsignacionManual(rutConductorAsignado, nombre, rut, patente)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ActualizaInterfaz()
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim sbScript As New StringBuilder

    Me.ltlTablaDatos.Text = DibujarConductores(ds)

    sbScript.AppendLine("setTimeout(function () {")
    sbScript.AppendLine("  jQuery(""#tblListado"").dataTable({ bPaginate: false, bInfo: false, bSort: false, bFilter: false });")
    sbScript.AppendLine("  GestionOperacion.seleccionarConductor({ prefijoControl: '0'});")
    sbScript.AppendLine("}, 100);")
    Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "actualizaInterfaz", True)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Me.btnGrabar.Visible = False

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' dibuja el listado de conductores de SCAT
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarConductores(ByVal ds As DataSet) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim totalRegistros As Integer
    Dim sb As New StringBuilder
    Dim templateAux, htmlSinRegistro, indice, nombreConductor, rutConductor, patentes, patenteTracto, patenteTrailer, telefono As String
    Dim json, classDestacado, telefonoLabel, fechaLicenciaConducir, fechaRevisionTecnica, mensajeVencimiento, displayRadioButtonSeleccionar As String
    Dim reportabilidadPatenteTracto, reportabilidadPatenteTrailer, reportabilidadPatenteTractoFecha, reportabilidadPatenteTrailerFecha As String
    Dim dtDatoConductor As DataTable
    Dim fechaLicenciaConducirVencida, fechaRevisionTecnicaVencida As Boolean

    Try
      template &= "<tr id=""{PREFIJO_CONTROL}_trFila"" data-type=""fila-conductor"" data-indice=""{INDICE}"" class=""{CLASS_DESTACADO}"">"
      template &= "  <td class=""text-center"">"
      template &= "{LABEL_INDICE}"
      template &= "<input type=""hidden"" id=""{PREFIJO_CONTROL}_txtJSONDatosConductor"" value=""{JSON}"" />"
      template &= "  </td>"
      template &= "  <td>"
      template &= "   <div>{NOMBRE_CONDUCTOR}</div>"
      template &= "   <div>{MENSAJE_VENCIMIENTO}</div>"
      template &= "  </td>"
      template &= "  <td>"
      template &= "    <div id=""{PREFIJO_CONTROL}_lblPatente"" onclick=""GestionOperacion.modificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Patente' })"">{PATENTES}</div>"
      template &= "    <div id=""{PREFIJO_CONTROL}_hPatente"" class=""sist-display-none"">"
      template &= "      <div class=""input-group input-group-sm""><span class=""input-group-addon""><strong>Tracto</strong></span><input type=""text"" id=""{PREFIJO_CONTROL}_txtPatenteTracto"" value=""{PATENTE_TRACTO}"" class=""form-control input-sm"" /></div>"
      template &= "      <div class=""sist-padding-top-3""></div>"
      template &= "      <div class=""input-group input-group-sm""><span class=""input-group-addon""><strong>Remolque</strong></span><input type=""text"" id=""{PREFIJO_CONTROL}_txtPatenteTrailer"" value=""{PATENTE_TRAILER}"" class=""form-control input-sm"" /></div>"
      template &= "      <div class=""text-center sist-padding-top-3"">"
      template &= "        <a href=""javascript:;"" onclick=""GestionOperacion.grabarModificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Patente' })""><img src=""../img/ico-grabar.png"" alt="""" /></a>"
      template &= "        <a href=""javascript:;"" onclick=""GestionOperacion.cancelarModificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Patente' })""><img src=""../img/ico-cancelar.png"" alt="""" /></a>"
      template &= "      </div>"
      template &= "    </div>"
      template &= "  </td>"
      template &= "  <td class=""text-center"">"
      template &= "    <div id=""{PREFIJO_CONTROL}_lblTelefono"" onclick=""GestionOperacion.modificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Telefono' })"">{TELEFONO_LABEL}</div>"
      template &= "    <div id=""{PREFIJO_CONTROL}_hTelefono"" class=""sist-display-none"">"
      template &= "      <div><input type=""text"" id=""{PREFIJO_CONTROL}_txtTelefono"" value=""{TELEFONO_TEXTBOX}"" class=""form-control input-sm"" /></div>"
      template &= "      <div class=""text-center sist-padding-top-3"">"
      template &= "        <a href=""javascript:;"" onclick=""GestionOperacion.grabarModificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Telefono' })""><img src=""../img/ico-grabar.png"" alt="""" /></a>"
      template &= "        <a href=""javascript:;"" onclick=""GestionOperacion.cancelarModificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Telefono' })""><img src=""../img/ico-cancelar.png"" alt="""" /></a>"
      template &= "      </div>"
      template &= "    </div>"
      template &= "  </td>"
      template &= "  <td class=""text-center""><input type=""radio"" id=""{PREFIJO_CONTROL}_rbtnSeleccionar"" name=""groupConductores"" value=""{INDICE}"" onclick=""GestionOperacion.seleccionarConductor({ prefijoControl: '{PREFIJO_CONTROL}' })"" class=""{DISPLAY_RADIOBUTTON_SELECCIONAR}"" /></td>"
      template &= "</tr>"

      htmlSinRegistro = Utilidades.MostrarMensajeUsuario("No hay operadores asociados", Utilidades.eTipoMensajeAlert.Warning)

      If (ds Is Nothing) Then
        html = htmlSinRegistro
        Me.btnGrabar.Visible = False
      Else
        totalRegistros = ds.Tables(0).Rows.Count

        If (totalRegistros = 0) Then
          html = htmlSinRegistro
          Me.btnGrabar.Visible = False
        Else
          Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                                 Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Asignar.ToString)

          sb.Append("<table id=""tblListado"" class=""table table-condensed table-hover"">")
          sb.Append("  <thead>")
          sb.Append("    <tr>")
          sb.Append("      <th style=""width:100px"" class=""text-center"">#</th>")
          sb.Append("      <th style=""width:270px"">Nombre Operador</th>")
          sb.Append("      <th style=""width:270px"">Placas</th>")
          sb.Append("      <th style=""width:90px"">Tel&eacute;fono</th>")
          sb.Append("      <th style=""width:80px"" class=""text-center"">Seleccionar</th>")
          sb.Append("    </tr>")
          sb.Append("  </thead>")
          sb.Append("  <tbody>")

          For Each dr As DataRow In ds.Tables(0).Rows
            templateAux = template
            patentes = ""
            mensajeVencimiento = ""
            displayRadioButtonSeleccionar = ""
            indice = dr.Item("Indice")
            nombreConductor = dr.Item("NombreConductor")
            rutConductor = dr.Item("RutConductor")
            patenteTracto = dr.Item("PatenteTracto")
            patenteTrailer = dr.Item("PatenteTrailer")
            telefono = dr.Item("Telefono")
            fechaLicenciaConducir = dr.Item("FechaLicenciaConducir")
            fechaRevisionTecnica = dr.Item("FechaRevisionTecnica")
            reportabilidadPatenteTracto = dr.Item("ReportabilidadPatenteTracto")
            reportabilidadPatenteTractoFecha = dr.Item("ReportabilidadPatenteTractoFecha")
            reportabilidadPatenteTrailer = dr.Item("ReportabilidadPatenteTrailer")
            reportabilidadPatenteTrailerFecha = dr.Item("ReportabilidadPatenteTrailerFecha")

            'convierte datarow en json
            dtDatoConductor = New DataTable
            dtDatoConductor = ds.Tables(0).Clone()
            dtDatoConductor.ImportRow(dr)
            dtDatoConductor.AcceptChanges()
            json = MyJSON.ConvertDataTableToJSON(dtDatoConductor)

            'formatea valores
            If (String.IsNullOrEmpty(fechaLicenciaConducir)) Then
              fechaLicenciaConducirVencida = False
            Else
              fechaLicenciaConducirVencida = IIf(DateDiff(DateInterval.Day, Herramientas.MyNow(), Convert.ToDateTime(fechaLicenciaConducir)) < 0, True, False)
            End If

            If (String.IsNullOrEmpty(fechaRevisionTecnica)) Then
              fechaRevisionTecnicaVencida = False
            Else
              fechaRevisionTecnicaVencida = IIf(DateDiff(DateInterval.Day, Herramientas.MyNow(), Convert.ToDateTime(fechaRevisionTecnica)) < 0, True, False)
            End If

            reportabilidadPatenteTracto = Utilidades.ObtenerSemaforoReportabilidad(reportabilidadPatenteTracto, reportabilidadPatenteTractoFecha)
            reportabilidadPatenteTrailer = "" 'Utilidades.ObtenerSemaforoReportabilidad(reportabilidadPatenteTrailer, reportabilidadPatenteTrailerFecha)

            patentes &= IIf(String.IsNullOrEmpty(patentes), "<strong>Tracto</strong>: <span id=""{PREFIJO_CONTROL}_lblPatenteTracto"">" & patenteTracto & "</span>", "<br /><strong>Tracto</strong>: <span id=""{PREFIJO_CONTROL}_lblPatenteTracto"">" & patenteTracto & "</span>") & " " & IIf(String.IsNullOrEmpty(patenteTracto), "", reportabilidadPatenteTracto)
            patentes &= IIf(String.IsNullOrEmpty(patentes), "<strong>Remolque</strong>: <span id=""{PREFIJO_CONTROL}_lblPatenteTrailer"">" & patenteTrailer & "</span>", "<br /><strong>Remolque</strong>: <span id=""{PREFIJO_CONTROL}_lblPatenteTrailer"">" & patenteTrailer & "</span>") & " " & IIf(String.IsNullOrEmpty(patenteTrailer), "", reportabilidadPatenteTrailer)
            nombreConductor = Server.HtmlEncode(nombreConductor) & IIf(Not String.IsNullOrEmpty(rutConductor), "<div class=""help-block sist-margin-cero sist-font-size-11""><em>Rut: " & rutConductor & "</em></div>", "")
            telefonoLabel = IIf(String.IsNullOrEmpty(telefono), GestionOperacion.SIN_TELEFONO, telefono)
            classDestacado = IIf(indice = "0", "sist-font-bold", "")
            classDestacado &= " " & IIf(fechaLicenciaConducirVencida Or fechaRevisionTecnicaVencida, "text-danger", "")

            If (fechaLicenciaConducirVencida Or fechaRevisionTecnicaVencida) Then
              displayRadioButtonSeleccionar = "sist-display-none"

              If (fechaLicenciaConducirVencida) Then
                mensajeVencimiento &= "<div class=""sist-padding-top-3 sist-font-bold""><em>- Licencia de conducir est&aacute; vencida (Fecha: " & fechaLicenciaConducir & ")</em></div>"
              End If

              If (fechaRevisionTecnicaVencida) Then
                mensajeVencimiento &= "<div class=""sist-padding-top-3 sist-font-bold""><em>- Revisi&oacute;n t&eacute;cnica est&aacute; vencida (Fecha: " & fechaRevisionTecnica & ")</em></div>"
              End If
            End If

            templateAux = templateAux.Replace("{INDICE}", indice)
            templateAux = templateAux.Replace("{LABEL_INDICE}", IIf(indice = "0", "ASIGNADO", indice))
            templateAux = templateAux.Replace("{NOMBRE_CONDUCTOR}", nombreConductor)
            templateAux = templateAux.Replace("{MENSAJE_VENCIMIENTO}", mensajeVencimiento)
            templateAux = templateAux.Replace("{PATENTE_TRACTO}", patenteTracto)
            templateAux = templateAux.Replace("{PATENTE_TRAILER}", patenteTrailer)
            templateAux = templateAux.Replace("{PATENTES}", patentes)
            templateAux = templateAux.Replace("{TELEFONO}", telefono)
            templateAux = templateAux.Replace("{TELEFONO_LABEL}", telefonoLabel)
            templateAux = templateAux.Replace("{JSON}", Server.HtmlEncode(json))
            templateAux = templateAux.Replace("{CLASS_DESTACADO}", classDestacado)
            templateAux = templateAux.Replace("{DISPLAY_RADIOBUTTON_SELECCIONAR}", displayRadioButtonSeleccionar)
            templateAux = templateAux.Replace("{PREFIJO_CONTROL}", indice)
            sb.Append(templateAux)
          Next

          sb.Append("  </tbody>")
          sb.Append("</table>")
          html = sb.ToString()
        End If
      End If
    Catch ex As Exception
      html = Utilidades.MostrarMensajeUsuario(ex.Message, Utilidades.eTipoMensajeAlert.Danger)
    End Try

    Return html
  End Function

#End Region

End Class
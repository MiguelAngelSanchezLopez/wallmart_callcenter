﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Discrepancia.Detalle.aspx.vb" Inherits="webtransportewalmartmx.Discrepancia_Detalle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle Discrepancia</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/discrepancia.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtNroTransporte" runat="server" />
    <input type="hidden" id="txtCodigoLocal" runat="server" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Informaci&oacute;n en Ruta"></asp:Label>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">INFORMACION DE LA DISCREPANCIA</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-3">
                    <label class="control-label">IdMaster</label>
                    <div><asp:Label ID="lblNroTransporte" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">DM</label>
                    <div><asp:Label ID="lblDM" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Tienda</label>
                    <div><asp:Label ID="lblNombreLocal" runat="server"></asp:Label></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">AUDITORIA DE CARGA CEDIS</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div id="pnlMensajeCarga" runat="server">
                  <em class="help-block">No hay informaci&oacute;n de carga</em>
                </div>
                <div id="pnlDatosCarga" runat="server" class="row">
                  <div class="col-xs-3">
                    <label class="control-label">Fecha</label>
                    <div><asp:Label ID="lblCargaFecha" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Realizado por</label>
                    <div><asp:Label ID="lblCargaRealizadoPor" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Informe</label>
                    <div id="btnGenerarPDFCarga"><asp:Label ID="lblCargaInformePDF" runat="server"></asp:Label></div>
                    <div id="lblMensajeGenerarPDFCarga"></div>
                  </div>
                  <div class="col-xs-3 sist-display-none">
                    <label class="control-label">Ver fotos</label>
                    <div><asp:Label ID="lblCargaVerFotos" runat="server" Text="Pendiente"></asp:Label></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">AUDITORIA DE RECEPCION TIENDA</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div id="pnlMensajeRecepcion" runat="server">
                  <em class="help-block">No hay informaci&oacute;n de recepci&oacute;n</em>
                </div>
                <div id="pnlDatosRecepcion" runat="server" class="row">
                  <div class="col-xs-3">
                    <label class="control-label">Fecha</label>
                    <div><asp:Label ID="lblRecepcionFecha" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Realizado por</label>
                    <div><asp:Label ID="lblRecepcionRealizadoPor" runat="server"></asp:Label></div>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Informe</label>
                    <div id="btnGenerarPDFRecepcion"><asp:Label ID="lblRecepcionInformePDF" runat="server"></asp:Label></div>
                    <div id="lblMensajeGenerarPDFRecepcion"></div>
                  </div>
                  <div class="col-xs-3 sist-display-none">
                    <label class="control-label">Ver fotos</label>
                    <div><asp:Label ID="lblRecepcionVerFotos" runat="server" Text="Pendiente"></asp:Label></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">ESTADO LINEA DE TRANSPORTE</h3></div>
            <div class="panel-body">
              <div class="form-group">
			          <asp:Label ID="lblMensajeEstadoTransportista" runat="server"></asp:Label>
                <asp:Panel ID="pnlEstadoTransportista" runat="server" Visible="false">
                  <div class="form-group">
                    <label class="control-label">Observaci&oacute;n</label>
                    <div><asp:Label ID="lblEstadoTransportistaObservacion" runat="server"></asp:Label></div>
                  </div>
                  <div>
                    <label class="control-label">Estado L&iacute;nea de Transporte</label>
                    <div>
                      <asp:DataList ID="dlEstadoTransportista" runat="server" RepeatColumns="1" DataKeyField="NombreEstado" Width="100%">
                        <ItemTemplate>
                          <div>
                            - <asp:Label ID="lblEstadoTransportistaNombre" runat="server" Text='<%#Container.DataItem("NombreEstado")%>'></asp:Label>
                          </div>
                        </ItemTemplate>
                      </asp:DataList>
                    </div>
                  </div>
                </asp:Panel>    
              </div>
            </div>
          </div>

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">INFORMACION DE RUTA</h3></div>
            <div class="panel-body">
              <div class="form-group">
			          <asp:Label ID="lblMensajeGrilla" runat="server"></asp:Label>
                <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Visible="false">
                  <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered">
                    <Columns>
                      <asp:BoundField DataField="NombreAlerta" HeaderText="Alerta" SortExpression="NombreAlerta" ItemStyle-Width="450px" />
                      <asp:BoundField DataField="Total" HeaderText="Total" SortExpression="Total"  />
                    </Columns>
                  </asp:GridView>

                  <div class="form-group text-center">
                    <div id="btnExportar" runat="server" class="btn btn-success" onclick="Reporte.exportar({ prefijoControl: '', invocadoDesde: Reporte.FORMULARIO_DISCREPANCIA_DETALLE, llaveReporte: 'HistorialAlertasDiscrepancia' })"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar alertas</div>
                    <div id="lblMensajeGeneracionReporte"></div>
                  </div>

                </asp:Panel>    
              </div>
            </div>
          </div>

        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Discrepancia.cerrarPopUp()"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
        </div>
      </div>
    </div>

  </form>
</body>
</html>

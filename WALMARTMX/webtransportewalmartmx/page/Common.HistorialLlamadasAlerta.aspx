﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Common.HistorialLlamadasAlerta.aspx.vb" Inherits="webtransportewalmartmx.Common_HistorialLlamadasAlerta" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle Alerta</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" runat="server" id="txtCargaInicialRealizada" value="0" />
    <input type="hidden" runat="server" id="txtNroEscalamiento" value="-1" />
    <div class="container sist-margin-top-10">

      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Font-Bold="true" Text="Historial de Llamadas"></asp:Label>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">
          <asp:Panel ID="hHistorialLlamadas" runat="server" CssClass="small"></asp:Panel>
          <asp:Panel ID="hCargando" runat="server"></asp:Panel>
        </asp:Panel>
        
        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Alerta.cerrarPopUp()"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <div id="btnRecargar" runat="server" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Recargar llamadas</div>
        </div>
      </div>
    </div>

  </form>
</body>
</html>

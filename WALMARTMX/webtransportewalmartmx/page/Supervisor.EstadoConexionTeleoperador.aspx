﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Supervisor.EstadoConexionTeleoperador.aspx.vb" Inherits="webtransportewalmartmx.Supervisor_EstadoConexionTeleoperador" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/graficos/highcharts.js"></script>
  <script type="text/javascript" src="../js/graficos/exporting.js"></script>
  <script type="text/javascript" src="../js/grafico.js"></script>
  <script type="text/javascript" src="../js/usuario.js"></script>
  
  <div class="form-group text-center">
    <p class="h1">Monitoreo de Alertas Visibilidad</p>
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">
      <input type="hidden" id="txtJSONDatos" value="[]" />

      <div class="form-group">
        <div class="small">
          <table id="tableDatos" class="table table-striped table-condensed">
            <thead>
              <tr>
                <th>Nombre Usuario</th>
                <th style="width:120px;">Estado</th>
                <th style="width:320px;">&Uacute;ltima acci&oacute;n</th>
                <th style="width:140px;">Fecha &uacute;ltima acci&oacute;n</th>
                <th style="width:140px;">Tiempo &uacute;ltima acci&oacute;n</th>
                <th style="width:160px;">Fecha &uacute;ltimo inicio sesi&oacute;n</th>
                <th style="width:140px;">Tiempo inicio sesi&oacute;n</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          <div id="hTablaSinDatos"></div>
        </div>
      </div>

      <div class="form-group">
        <div class="row">
          <div class="col-xs-6">
            <div class="row">
              <div class="col-xs-6">
                <div id="hGrafico"></div>
              </div>
            </div>
          </div>
          <div class="col-xs-6">
            <div id="hGrafico2"></div>
          </div>
        </div>
      </div>

       <div class="form-group">
       <div class="row">
          <div class="col-xs-12">
            <h3>Total de Alertas</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
              <div class="small">
                <table id="tableTotalAlertas" class="table table-striped table-condensed">
            <thead>
              <tr>
                <th style="width:250px;">Tipo de Alerta</th>
                <th style="width:100px;">Alertas Atendidas</th>
                <th style="width:100px;">Alertas pendientes de atender</th>
                <th style="width:100px;">Total</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
                <div id="hTablaSinDatosTotalAlertas"></div>
              </div>
          </div>
        </div>
      </div>

      <div class="form-group">
       <div class="row">
          <div class="col-xs-12">
            <h3>Alertas Atendidas</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
              <div class="small">
                <table id="tableTotalAlertasAtendidas" class="table table-striped table-condensed">
            <thead>
              <tr>
                <th style="width:150px;">Usuario</th>
                <th style="width:100px;">Alertas Atendidas</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
                <div id="hTablaSinDatosTotalAlertasAtendidas"></div>
              </div>
          </div>
        </div>
      </div>



      <script type="text/javascript">
        jQuery(document).ready(function () {
          Usuario.obtenerEstadoConexionTeleoperador();
          Usuario.SETINTERVAL_ESTADO_CONEXION_TELEOPERADOR = setInterval(function () { Usuario.obtenerEstadoConexionTeleoperador(); }, Usuario.SETINTERVAL_MILISEGUNDOS_ESTADO_CONEXION_TELEOPERADOR);

          Usuario.dibujarTablaEstadoConexionTeleoperador({ cemtra: false });
          Usuario.SETINTERVAL_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR = setInterval(function () { Usuario.dibujarTablaEstadoConexionTeleoperador({ cemtra: false }); }, Usuario.SETINTERVAL_MILISEGUNDOS_DIBUJAR_ESTADO_CONEXION_TELEOPERADOR);
        });
      </script>
    </asp:Panel>
  </div>

</asp:Content>
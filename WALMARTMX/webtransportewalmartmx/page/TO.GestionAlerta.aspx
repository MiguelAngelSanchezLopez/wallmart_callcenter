﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="TO.GestionAlerta.aspx.vb" Inherits="webtransportewalmartmx.TO_GestionAlerta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY"></script>-->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8"></script>
  <script type="text/javascript" src="../js/jquery.customscrollbar.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/ion.sound.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>

  <div class="form-group">
    <input type="hidden" id="txtSimulacionActiveX" value="" />
    <input type="hidden" id="txtIdAlerta" value="" />
    <input type="hidden" id="txtIdAlertaHija" value="" />
    <input type="hidden" id="txtIdAlertaGestionAcumulada" value="" />
    <input type="hidden" id="txtNombreAlerta" value="" />
    <input type="hidden" id="txtProximoEscalamiento" value="" />
    <input type="hidden" id="txtTotalEscalamientosAlerta" value="" />
    <input type="hidden" id="txtLatTracto" value="" />
    <input type="hidden" id="txtLonTracto" value="" />
    <input type="hidden" id="txtAlertaMapa" value="" />
    <input type="hidden" id="txtIdRegistroNuevoEnCombo" value="" />
    <input type="hidden" id="txtDestinoLocal" value="" />
    <input type="hidden" id="txtDestinoLocalCodigo" value="" />
    <input type="hidden" id="txtCallCenterTelefono" value="" />
    <input type="hidden" id="txtCallCenterIdLlamada" value="" />
    <input type="hidden" id="txtCallCenterEstadoLlamada" value="-1" />
    <input type="hidden" id="txtCallCenterFinalizarLlamadaModoManual" value="0" />
    <input type="hidden" id="txtCallCenterIdEscalamientoPorAlertaGrupoContacto" value="" />
    <input type="hidden" id="txtAuxiliar" value="" />
    <input type="hidden" id="txtIdFormato" value="" />

    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="row">

        <!-- COLUMNA IZQUIERDA -->
        <div class="col-xs-3">
          <div class="panel panel-info">
            <div class="panel-heading text-center">
              <div class="h3 panel-title"><strong>MAPA ALERTA</strong></div>
              <small id="hFechaHoy" class="sist-font-size-11"></small>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div id="hMapaCanvas" class="sist-margin-bottom-5">
                  <div id="map-canvas" class="sist-map-canvas">
                    <div class="text-center">
                      <div class="sist-padding-top-20"></div>
                      <div class="form-group"><img src="../img/sin_mapa.png" alt="" /></div>
                      <div class="help-block h3"><em>No hay mapa para visualizar</em></div>
                    </div>                  
                  </div>
                  <div id="lblPermisoZona" class="text-center"></div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-info">
            <div class="panel-heading text-center">
              <div class="h3 panel-title"><strong>SEÑAL GPS</strong></div>
            </div>
            <div class="panel-body sist-padding-5">
              <div class="row">
                <div class="col-xs-6 text-center">
                  <div id="hReportabilidadTrailer"></div>
                </div>
                <div class="col-xs-6 text-center">
                  <div id="hReportabilidadTracto"></div>
                </div>
              </div>
              <div class="text-center small sist-padding-top-5">&Uacute;ltima fecha de reporte</div>
            </div>
          </div>
        </div>

        <!-- COLUMNA DERECHA -->
        <div class="col-xs-9">
          <div id="hAtendiendoAlerta" class="panel panel-success">
            <div class="panel-heading"><h3 class="panel-title"><strong>[IdAlerta: <span id="lblIdAlertaHija"></span>] ATENDIENDO ALERTA: <em><span id="lblTituloNombreAlerta"></span></em></strong></h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="col-xs-4 sist-padding-cero">
                  <div id="hPasosEscalamientos" class="sist-margin-bottom-5"></div>
                </div>
                <div class="col-xs-8 text-right">
                  <div class="btn-group text-left">
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                      <span class="glyphicon glyphicon-time"></span>&nbsp;Auxiliares <span class="caret"></span>
                    </button>
                    <ul id="ulAuxiliares" class="dropdown-menu" role="menu">
                      <!-- las opciones se dibujan en forma dinamica al cargar los datos de la alerta -->
                    </ul>
                  </div>
                  <button id="btnPopover" type="button" class="btn btn-default" data-toggle="popover" data-trigger="click" data-placement="bottom"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;Ver detalle</button>
                  <button id="btnMapa" type="button" class="btn btn-default" onclick="Alerta.verPosicionEnMapa({ desdeFormulario: Alerta.FORMULARIO_TO_GESTION_ALERTA, sistemaDestino: 'QAnalytics' })"><span class="glyphicon glyphicon-globe"></span>&nbsp;Ver Mapa</button>
                  <button id="btnAlertasAtendidasHoy" type="button" class="btn btn-sm btn-success sist-display-none" onclick="Alerta.verAlertasAtendidasHoy()"><span class="glyphicon glyphicon-search"></span>&nbsp;Alertas atendidas hoy</button>

                  <div id="popoverTitulo" class="hidden"><strong><span id="lblPopoverTitulo"></span></strong></div>
                  <div id="popoverContenido" class="hidden">
                    <small>
                      <strong>Prioridad: </strong><span id="lblPopoverPrioridad"></span><br />
                      <strong>Escalamientos realizados: </strong><span id="lblPopoverNroEscalamientoActual" class="label label-success sist-font-size-11">-</span><br />
                      <strong>IdMaster: </strong><span id="lblPopoverNroTransporte"></span><br />
                      <strong>IdEmbarque: </strong><span id="lblPopoverIdEmbarque"></span><br />
                      <strong>Formato: </strong><span id="lblPopoverNombreFormato"></span><br />
                      <strong>Origen: </strong><span id="lblPopoverOrigen"></span><br />
                      <strong>Destino: </strong><span id="lblPopoverDestino"></span><br />
                      <strong>Tel&eacute;fono Tienda: </strong><span id="lblPopoverTelefonoLocal"></span><br />
                      <strong>Operador: </strong><span id="lblPopoverNombreConductor"></span><br />
                      <strong>L&iacute;nea de Transporte: </strong><span id="lblPopoverNombreTransportista"></span><br />
                      <strong>Placa Tracto: </strong><span id="lblPopoverPatenteTracto"></span><br />
                      <strong>Placa Remolque: </strong><span id="lblPopoverPatenteTrailer"></span><br />
                      <strong>Velocidad: </strong><span id="lblPopoverVelocidad"></span> km/hr<br />
                      <strong>Tipo Viaje: </strong><span id="lblPopoverTipoViaje"></span><br />
                      <strong>Lat Tracto: </strong><span id="lblPopoverLatTracto"></span><br />
                      <strong>Lon Tracto: </strong><span id="lblPopoverLonTracto"></span><br />
                      <strong>Fecha Creaci&oacute;n: </strong><span id="lblPopoverFechaHoraCreacion"></span><br />
                      <strong>Tipo Alerta: </strong><span id="lblPopoverTipoAlerta"></span><br />
                      <strong>Tipo Flota: </strong><span id="lblPopoverTipoFlota"></span><br />
                      <strong>Ventana Horaria: </strong><span id="lblPopoverVentanaHoraria"></span><br />
                      <strong>Hora de Cita: </strong><span id="lblPopoverHoraCita"></span><br />
                      <div class="text-center"><span id="lblPopoverPermiso"></span></div>
                    </small>
                  </div>

                </div>
                <hr class="sist-clear-both sist-margin-cero" />
              </div>
              
              <div class="form-group sist-clear-both">
                <div id="lblAlertaReprogramada" class="text-center text-danger sist-font-size-16 sist-padding-bottom-5 sist-display-none"><em><strong>(ALERTA REPROGRAMADA)</strong></em></div>

                <div class="sist-titulo-seccion">REGISTRO ESCALAMIENTO <label class="label label-success sist-font-size-14"><strong>NRO. <span id="lblProximoEscalamiento">-</span></strong></label></div>
                
                <div class="small sist-clear-both">
                  <div id="hContactosEscalamiento"></div>
                  <hr />

                  <div id="hBotones" class="form-group sist-display-none">
                    <div class="row">
                      <div class="col-xs-10 text-center">
                        <span class="text-danger sist-font-size-30 sist-display-none"><em><strong>Tiempo de atenci&oacute;n:</strong> <span id="hCronometro">00:00:00</span></em></span>
                      </div>
                      <div class="col-xs-2 text-right">
                        <div id="btnGrabar" runat="server" class="btn btn-primary btn-block btn-lg" onclick="Alerta.validarFormularioAlerta_TeleOperador()"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</div>
                      </div>
                    </div>
                  </div>
                  <div id="hMensajeGrabando" class="form-group text-right sist-display-none">
                    <span class="text-muted sist-font-size-24"><em><strong>Grabando informaci&oacute;n, espere un momento por favor ...</strong></em></span>
                  </div>

                  <br />
                  <div class="form-group">
                    <div class="sist-titulo-seccion">HISTORIAL GESTION DE ALERTAS</div>
                    <div id="hHistorialEscalamientos" class="table-responsive sist-height-historial-escalamientos-sin-datos-to">
                      <div id="hTablaHistorialEscalamientos"><em class="help-block">No hay gestiones asociados</em></div>
                    </div>
                    <hr />
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery("#btnPopover").popover({
        html: true,
        content: function () { return jQuery('#popoverContenido').html(); },
        title: function () { return jQuery('#popoverTitulo').html(); }
      });

      ion.sound({
        sounds: [
          { name: Alerta.SONIDO_ALARMA_NUEVA },
          { name: Alerta.SONIDO_ALARMA_SIGUIENTE_ESCALAMIENTO }
        ],
        preload: true
      });

      Sistema.mostrarFechaActual({ elem_contenedor: "hFechaHoy", mostrarReloj: true });
      setInterval(function () { Sistema.mostrarFechaActual({ elem_contenedor: "hFechaHoy", mostrarReloj: true }); }, 1000);

      Alerta.obtenerAlertasPorAtender({ cargaDatos: "inicial", revisarAlertaAsignadaPreviamente: true });
      Alerta.SETINTERVAL_ALERTAS_POR_ATENDER = setInterval(function () { Alerta.obtenerAlertasPorAtender({ cargaDatos: "update", revisarAlertaAsignadaPreviamente: false }); }, Alerta.SETINTERVAL_MILISEGUNDOS_ALERTAS_POR_ATENDER);
    });
  </script>

</asp:Content>

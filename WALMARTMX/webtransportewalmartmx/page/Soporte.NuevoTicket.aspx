﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Soporte.NuevoTicket.aspx.vb" Inherits="webtransportewalmartmx.Soporte_NuevoTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.mask.js"></script>
  <script type="text/javascript" src="../js/combo.js"></script>
  <script type="text/javascript" src="../js/soporte.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Enviar Ticket</p>
      </div>
      <div class="col-xs-4 text-right">
        &nbsp;
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server" CssClass="sist-font-size-16 text-center"></asp:Panel>

  <div id="hMensajeEnvioTicket" runat="server" class="sist-font-size-16 text-center sist-display-none"></div>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-default">
        <div class="panel-heading"><span class="panel-title"><strong>DATOS PERSONALES</strong></span></div>
        <div class="panel-body">
          <div class="row sist-margin-bottom-5">
            <div class="col-xs-4">
              <label class="control-label">Nombre<strong class="text-danger">&nbsp;*</strong></label>
              <asp:TextBox ID="txtNombrePersona" runat="server" MaxLength="255" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-xs-4">
              <label class="control-label">Tel&eacute;fono</label>
              <asp:TextBox ID="txtTelefono" runat="server" MaxLength="255" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-4">
              <label class="control-label">Email</label>
              <asp:TextBox ID="txtEmail" runat="server" MaxLength="255" CssClass="form-control"></asp:TextBox>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading"><span class="panel-title"><strong>DATOS DEL TICKET</strong></span></div>
        <div class="panel-body">
          <div class="form-group row">
            <div class="col-xs-4">
              <label class="control-label">Aplicaci&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
              <select id="ddlAplicacion" runat="server" class="form-control chosen-select" onchange="Soporte.mostrarCampoOtro({ invocadoDesde: 'Aplicacion' })">
                <option value="">--Seleccione--</option>
              </select>
              <div id="hAplicacionOtro" runat="server" class="sist-padding-top-3 sist-display-none">
                <asp:TextBox ID="txtAplicacionOtro" runat="server" CssClass="form-control"></asp:TextBox>
                <div class="help-block sist-margin-cero small">(escriba otra Aplicaci&oacute;n)</div>
              </div>
            </div>
            <div class="col-xs-4">
              <label class="control-label">M&oacute;dulo<strong class="text-danger">&nbsp;*</strong></label>
              <select id="ddlModulo" runat="server" class="form-control chosen-select" onchange="Soporte.mostrarCampoOtro({ invocadoDesde: 'Modulo' })">
                <option value="">--Seleccione--</option>
              </select>
              <div id="hModuloOtro" runat="server" class="sist-padding-top-3 sist-display-none">
                <asp:TextBox ID="txtModuloOtro" runat="server" CssClass="form-control"></asp:TextBox>
                <div class="help-block sist-margin-cero small">(escriba otro M&oacute;dulo)</div>
              </div>
            </div>
            <div class="col-xs-4">
              <label class="control-label">Motivo Ticket<strong class="text-danger">&nbsp;*</strong></label>
              <select id="ddlMotivoTicket" runat="server" class="form-control chosen-select" onchange="Soporte.mostrarCampoOtro({ invocadoDesde: 'MotivoTicket' })">
                <option value="">--Seleccione--</option>
              </select>
              <div id="hMotivoTicketOtro" runat="server" class="sist-padding-top-3 sist-display-none">
                <asp:TextBox ID="txtMotivoTicketOtro" runat="server" CssClass="form-control"></asp:TextBox>
                <div class="help-block sist-margin-cero small">(escriba otro Motivo Ticket)</div>
              </div>
            </div>
          </div>
          <div>
            <label class="control-label">Observaci&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
            <asp:TextBox ID="txtObservacion" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
          </div>
        </div>
      </div>

      <div id="hBotones" runat="server" class="form-group text-center">
        <button type="button" id="btnGrabar" class="btn btn-lg btn-primary" onclick="Soporte.validarFormularioNuevoTicket()"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</button>
      </div>

      <div id="hMensajeGrabando" runat="server" class="form-group text-center sist-display-none">
        <span class="text-muted sist-font-size-24"><em><strong>Enviando ticket, espere un momento por favor ...</strong></em></span>
      </div>
    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();
      setTimeout(function () { Soporte.inicializarControles(); }, 500); 
    });
  </script>

</asp:Content>


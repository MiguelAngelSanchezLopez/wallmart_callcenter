﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Supervisor.ReporteListado.aspx.vb" Inherits="webtransportewalmartmx.Supervisor_ReporteListado" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables.bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>

  <input type="hidden" id="txtLlaveReporte" value="" />
  <input type="hidden" id="txtFormatoDocumento" value="" />

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Reportes</p>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">SELECCIONE EL REPORTE</h3></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-3">
              <label class="control-label">Tipo Reporte</label>
              <uc1:wucCombo id="ddlReporte" runat="server" FuenteDatos="Tabla" TipoCombo="Reporte" ItemSeleccione="true" CssClass="form-control chosen-select" funcionOnChange="Reporte.mostrarControles()"></uc1:wucCombo>
            </div>
            <div id="hFormato" class="col-xs-1 sist-display-none" data-visible="control-reporte">
              <label class="control-label">Formato</label>
              <div id="lblFormato"><img src="../img/ico-desconocido.png" alt="desconocido" title="desconocido" /></div>
            </div>
            <div id="hFechaDesde" class="col-xs-2 sist-display-none" data-visible="control-reporte">
              <label class="control-label">Fecha Desde</label>
              <input type="text" id="txtFechaDesde" class="form-control date-pick" maxlength="10" />
            </div>
            <div id="hFechaHasta" class="col-xs-2 sist-display-none" data-visible="control-reporte">
              <label class="control-label">Fecha Hasta</label>
              <input type="text" id="txtFechaHasta" class="form-control date-pick" maxlength="10" />
              <div id="lblMensajeErrorFechaHasta"></div>
            </div>
            <div id="hBotonFiltrar" class="col-xs-4 sist-display-none" data-visible="control-reporte">
              <label class="control-label">&nbsp;</label><br />
              <div id="btnFiltrar" class="btn btn-primary" onclick="Reporte.validarFormulario()"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</div>
            </div>
          </div>
        </div>
      </div>

      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <div id="hTablaDatos" class="table-responsive" style="width: 1300px;overflow-x: auto;white-space: nowrap;" ></div>
      </div>

      <div class="form-group text-center">
        <div id="btnExportar" runat="server" class="btn btn-lg btn-success sist-display-none" onclick="Reporte.exportar({ invocadoDesde: Reporte.FORMULARIO_SUPERVISOR_REPORTE_LISTADO })"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar datos</div>
        <div id="lblMensajeGeneracionReporte"></div>
      </div>

    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });

    });
  </script>

</asp:Content>


﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Transportista.RespuestaAlertaRojaListado.aspx.vb" Inherits="webtransportewalmartmx.Transportista_RespuestaAlertaRojaListado" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/transportista.js"></script>
  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Respuestas Alertas Rojas</p>
      </div>
    </div>
  </div>
  <asp:Panel ID="pnlMensaje" runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-2">
              <label class="control-label">IdMaster</label>
              <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-3">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Transportista.validarFormularioBusquedaRespuesta())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
          </div>
        </div>
      </div>

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group">
        <asp:Panel ID="pnlMensajeUsuario" runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>
          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered" EnableModelValidation="True" DataKeyNames="IdAlertaRojaEstadoActual,NroTransporte">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <%# Container.DataItemIndex + 1%>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="NroTransporte" HeaderText="IdMaster" SortExpression="NroTransporte" />
              <asp:BoundField DataField="Fecha" HeaderText="Fecha solicitud de revisión" SortExpression="Fecha" />
              <asp:TemplateField HeaderText="Opciones">
                <HeaderStyle Width="150px" />
                <ItemStyle Wrap="True" Width="110px" />
                <ItemTemplate>
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>
      </div>
    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

    });
  </script>
</asp:Content>

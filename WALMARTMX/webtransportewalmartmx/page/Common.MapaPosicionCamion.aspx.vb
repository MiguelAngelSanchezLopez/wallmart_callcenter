﻿Imports CapaNegocio
Imports System.Data

Public Class Common_MapaPosicionCamion
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    GrabarRecepcionCamion
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim patente As String = Me.ViewState.Item("patente")

    'obtiene listado
    ds = TrazaViaje.ObtenerHistorialRecepcionCamion(nroTransporte, patente)

    'guarda el resultado en session
    If ds Is Nothing Then
      Session("dv") = Nothing
    Else
      Session("dv") = ds.Tables(0).DefaultView
    End If
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim patente As String = Me.ViewState.Item("patente")
    Dim ws As New wsQanalytics.Service
    Dim respuesta As wsQanalytics.Estructura
    Dim latTracto, lonTracto As String
    Dim dv As New DataView
    Dim totalRegistros As Integer

    respuesta = ws.Metodo(patente)
    Sistema.GrabarLogSesion(oUsuario.Id, "Consulta posición del camión: Placa " & patente)

    'asigna valores
    If (String.IsNullOrEmpty(respuesta.Error)) Then
      latTracto = respuesta.Lat
      lonTracto = respuesta.Lon
      Utilidades.RegistrarScript(Me.Page, "Sistema.verGoogleMaps({ elem_contenedor_mapa: '" & Me.hMapaCanvas.ClientID & "', latTracto: '" & latTracto & "', lonTracto: '" & lonTracto & "' });", Utilidades.eRegistrar.FINAL, "actualizaInterfaz", False)
    End If

    dv = CType(Session("dv"), DataView)
    If dv Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = dv.Table.Rows.Count
    End If

    If totalRegistros > 0 Then
      Me.pnlHolderGrilla.Visible = True
      'carga la grilla a utilizar
      Me.gvPrincipal.Visible = True
      Me.gvPrincipal.DataSource = dv
      Me.gvPrincipal.DataBind()
      Utilidades.MostrarTotalRegistrosGrilla(dv, Me.lblTotalRegistros)
    Else
      Me.pnlHolderGrilla.Visible = False
      'oculta la grilla
      Me.gvPrincipal.Visible = False
    End If

    Me.lblTituloFormulario.Text = "<strong>Posici&oacute;n cami&oacute;n: <span class=""text-danger"">Placa Tracto " & patente & "</span></strong>"
    Me.ltlTablaDatos.Text = Me.MostrarDatosPosicionCamion(respuesta)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.pnlFormulario.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                               Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.GrabarRecepcionCamion.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                           Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.GrabarRecepcionCamion.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosRegistro(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim patente As String = Me.ViewState.Item("patente")
    Dim estado, observacion As String

    Try
      estado = Utilidades.IsNull(Me.ddlEstadoRecepcionCamion.SelectedValue, "")
      observacion = Utilidades.IsNull(Me.txtObservacion.Text, "")

      If (Not String.IsNullOrEmpty(estado)) Then
        Sistema.GrabarLogSesion(oUsuario.Id, "Graba recepción del camión: IdMaster " & nroTransporte & " / Placa Tracto " & patente)
        status = TrazaViaje.GrabarHistorialRecepcionCamion(oUsuario.Id, nroTransporte, patente, estado, observacion)
      End If
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la recepción del camión.<br />" & ex.Message
      status = "error"
    End Try

    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim patente As String = Me.ViewState.Item("patente")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""
    Dim jsonContacto As String = ""

    'grabar valores
    textoMensaje &= GrabarDatosRegistro(status)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La recepción del camión fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La recepción del camión fue actualizada satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la recepción del camión. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la recepción del camión. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = "[IdMaster: " & nroTransporte & " / Placa Tracto: " & patente & "] " & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "TrazaViaje.cerrarPopUpMapaPosicionCamion('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "nroTransporte=" & nroTransporte & "&patente=" & Server.UrlEncode(patente)
      Response.Redirect("./Common.MapaPosicionCamion.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' dibuja los datos de la posicion del camion
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function MostrarDatosPosicionCamion(ByVal respuesta As wsQanalytics.Estructura) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim fechaConsultaGPS, fechaRespuestaGPS As String

    Try
      If (Not String.IsNullOrEmpty(respuesta.Error)) Then
        html = "<div class=""alert alert-warning text-center""><strong>" & respuesta.Error & "</strong></div>"
      Else
        fechaConsultaGPS = respuesta.Fh_dato.Replace("-", "/")
        fechaRespuestaGPS = respuesta.Fecha_Procesa.Replace("-", "/")

        template &= "<table class=""table"">"
        template &= "  <thead>"
        template &= "    <tr>"
        template &= "      <th>PLACA</th>"
        template &= "      <th>LAT TRACTO</th>"
        template &= "      <th>LON TRACTO</th>"
        template &= "      <th>VELOCIDAD</th>"
        template &= "      <th>FECHA ULTIMA CONSULTA GPS</th>"
        template &= "      <th>FECHA ULTIMA RESPUESTA GPS</th>"
        template &= "    </tr>"
        template &= "  </thead>"
        template &= "  <tbody>"
        template &= "    <tr>"
        template &= "      <td>{PATENTE}</td>"
        template &= "      <td>{LAT_TRACTO}</td>"
        template &= "      <td>{LON_TRACTO}</td>"
        template &= "      <td>{VELOCIDAD} km/hr</td>"
        template &= "      <td>{FECHA_CONSULTA_GPS} hrs.</td>"
        template &= "      <td>{FECHA_RESPUESTA_GPS} hrs.</td>"
        template &= "    </tr>"
        template &= "  </tbody>"
        template &= "</table>"

        template = template.Replace("{PATENTE}", respuesta.Patente)
        template = template.Replace("{LAT_TRACTO}", respuesta.Lat)
        template = template.Replace("{LON_TRACTO}", respuesta.Lon)
        template = template.Replace("{VELOCIDAD}", respuesta.Velocidad)
        template = template.Replace("{FECHA_CONSULTA_GPS}", fechaConsultaGPS)
        template = template.Replace("{FECHA_RESPUESTA_GPS}", fechaRespuestaGPS)
        html = template
      End If
    Catch ex As Exception
      html = "<div class=""alert alert-danger text-center""><strong>" & ex.Message & "</strong></div>"
    End Try

    Return html
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "")
    Dim patente As String = Utilidades.IsNull(Server.UrlDecode(Request("patente")), "")

    Me.ViewState.Add("nroTransporte", nroTransporte)
    Me.ViewState.Add("patente", patente)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
      ObtenerDatos()
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    GrabarRegistro()
  End Sub

  Protected Sub gvPrincipal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPrincipal.RowDataBound
    Dim indicePrimeraFila As Integer = Utilidades.ObtenerIndicePrimeraFilaGrilla(Me.gvPrincipal)
    Dim indiceFila As Integer

    If e.Row.RowType = DataControlRowType.DataRow Then
      'asigna indice de la fila
      indiceFila = indicePrimeraFila + e.Row.RowIndex
      'asigna valor del indice de la fila al control
      Utilidades.SetControlIndiceFila(e.Row, "lblIndiceFila", indiceFila)
    End If
  End Sub

End Class
﻿Imports System.Data
Imports CapaNegocio

Public Class Common_VerArchivo
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim argCrypt As String = Request("a")
    Dim textoDesencriptado As String = Criptografia.DesencriptarTripleDES(argCrypt)
    Dim arrParametros As Array = Criptografia.ObtenerArrayQueryString(textoDesencriptado)
    Dim idArchivo As String = Criptografia.RequestQueryString(arrParametros, "id")
    Dim soloMostrar As String = Utilidades.IsNull(Criptografia.RequestQueryString(arrParametros, "soloMostrar"), "0")
    Dim usarThumb As String = Utilidades.IsNull(Criptografia.RequestQueryString(arrParametros, "usarThumb"), "0")
    Dim anchoMaxThumb As String = Utilidades.IsNull(Criptografia.RequestQueryString(arrParametros, "anchoMaxThumb"), Utilidades.AnchoMaximoThumbnail())
    Dim altoMaxThumb As String = Utilidades.IsNull(Criptografia.RequestQueryString(arrParametros, "altoMaxThumb"), Utilidades.AltoMaximoThumbnail())
    Dim ds As New DataSet
    Dim contenido() As Byte
    Dim contenType, nombre As String
    Dim foto As System.Drawing.Image = Nothing

    'obtiene datos
    Try
      ds = Archivo.ObtenerDetallePorId(idArchivo)
      contenido = ds.Tables(0).Rows(0).Item("Contenido")
      nombre = ds.Tables(0).Rows(0).Item("Nombre")
      contenType = ds.Tables(0).Rows(0).Item("ContentType")

      If usarThumb = "1" Then
        foto = Archivo.Bytes2Image(contenido)
        foto = Archivo.GeneraThumbnail(foto, anchoMaxThumb, altoMaxThumb)
        contenido = Archivo.Image2Bytes(foto)
      End If

      'los carga en la pagina
      Response.Buffer = True
      Response.Clear()
      If soloMostrar = 0 Then Response.AppendHeader("content-disposition", "attachment; filename=" & nombre.Replace(" ", "_"))
      Response.ContentType = contenType
      Response.BinaryWrite(contenido)
      Response.End()
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

End Class
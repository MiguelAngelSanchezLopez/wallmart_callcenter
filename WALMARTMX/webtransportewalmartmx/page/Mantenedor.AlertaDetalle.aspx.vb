﻿Imports CapaNegocio
Imports System.Data
Imports System.Web.Script.Serialization

Public Class Mantenedor_AlertaDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

  Dim SL_GRUPO_CONTACTO As New SortedList

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks>Por VSR, 23/01/2009</remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idAlertaConfiguracion As String = Me.ViewState.Item("idAlertaConfiguracion")

    Try
      'obtiene detalle del usuario
      ds = Alerta.ObtenerDetalleConfiguracion(idAlertaConfiguracion)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaConfiguracion As String = Me.ViewState.Item("idAlertaConfiguracion")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim idFormato, idAlertaDefinicionPorFormato, prioridad, frecuenciaRepeticionMinutos, activo, tieneAlertasAsociadas, tipoAlerta, enabledComboNombre As String
    Dim fechaInicio, horaInicio, minutosInicio As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    idFormato = ""
    idAlertaDefinicionPorFormato = "-1"
    prioridad = ""
    frecuenciaRepeticionMinutos = "-1"
    activo = "0"
    tieneAlertasAsociadas = "0"
    tipoAlerta = ""
    fechaInicio = ""
    horaInicio = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(Alerta.eTablaConfiguracion.T00_Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(Pagina.eTabla.Detalle).Rows(0)
          idFormato = dr.Item("IdFormato")
          idAlertaDefinicionPorFormato = dr.Item("IdAlertaDefinicionPorFormato")
          prioridad = dr.Item("Prioridad")
          frecuenciaRepeticionMinutos = dr.Item("FrecuenciaRepeticionMinutos")
          activo = dr.Item("Activo")
          tieneAlertasAsociadas = dr.Item("TieneAlertasAsociadas")
          tipoAlerta = dr.Item("TipoAlerta")
          fechaInicio = dr.Item("FechaInicio")
          horaInicio = dr.Item("HoraInicio")
        End If
      End If
    Catch ex As Exception
      idFormato = ""
      idAlertaDefinicionPorFormato = "-1"
      prioridad = ""
      frecuenciaRepeticionMinutos = "-1"
      activo = "0"
      tieneAlertasAsociadas = "0"
      tipoAlerta = ""
      fechaInicio = ""
      horaInicio = ""
    End Try

    'asigna valores
    enabledComboNombre = IIf(idAlertaDefinicionPorFormato = "-1", "true", "false")
    Me.txtIdAlertaConfiguracion.Value = idAlertaConfiguracion
    Me.ddlFormato.SelectedValue = idFormato
    Me.ddlFormato.Enabled = IIf(idAlertaConfiguracion = "-1", True, False)
    Me.txtIdFormatoActual.Value = idFormato
    Me.ddlPrioridad.SelectedValue = prioridad
    Me.txtFrecuenciaRepeticion.Text = IIf(frecuenciaRepeticionMinutos = "-1", "", frecuenciaRepeticionMinutos)
    Me.ddlTipoAlerta.SelectedValue = tipoAlerta
    Me.chkActivo.Checked = IIf(idAlertaConfiguracion = "-1", True, IIf(activo = "1", True, False))
    Me.txtFechaInicio.Text = fechaInicio
    Me.txtFechaInicioAnterior.Value = fechaInicio
    Me.txtHoraInicioAnterior.Value = horaInicio
    Me.lblTituloFormulario.Text = IIf(idAlertaConfiguracion = "-1", "Nueva ", "Modificar ") & "Alerta"
    Me.lblIdRegistro.Text = IIf(idAlertaConfiguracion = "-1", "", "[ID: " & idAlertaConfiguracion & "]")

    If (String.IsNullOrEmpty(horaInicio)) Then
      minutosInicio = ""
    Else
      minutosInicio = Right(horaInicio, 2)
      horaInicio = Left(horaInicio, 2)
    End If
    Me.wucHoraInicio.SelectedValueHora = horaInicio
    Me.wucHoraInicio.SelectedValueMinutos = minutosInicio

    'primero verifica si tiene permiso para ver el boton y luego si tiene alertas asociadas
    Me.btnEliminar.Visible = Me.btnEliminar.Visible And IIf(tieneAlertasAsociadas = "1", False, True)

    'construye json con los escalamientos de la alerta
    Me.txtJSONEscalamientos.Value = Me.ConstruirJSONEscalamientos(ds)

    'construye json con la secuencia de padre-hijo
    Me.txtJSONPadreHijo.Value = Me.ConstruirJSONPadreHijo(ds)

    sbScript.Append("Alerta.activarControlesPorFormato({ elem_contenedor: '" & Me.ddlFormato.ClientID & "_ddlCombo', idAlertaDefinicionPorFormato: '" & idAlertaDefinicionPorFormato & "', enabled: " & enabledComboNombre & " });")
    If (idAlertaConfiguracion <> "-1") Then sbScript.Append("Alerta.dibujarEscalamientos();")

    If (Not String.IsNullOrEmpty(sbScript.ToString())) Then Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "scriptActualizaInterfaz", False)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()

    Me.ddlFormato.funcionOnChange = "Alerta.activarControlesPorFormato({ elem_contenedor: '" & Me.ddlFormato.ClientID & "_ddlCombo', idAlertaDefinicionPorFormato: '-1', enabled: true })"
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim idAlertaConfiguracion As String = Me.ViewState.Item("idAlertaConfiguracion")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idAlertaConfiguracion = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idAlertaConfiguracion <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnEliminar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             idAlertaConfiguracion <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef idAlertaConfiguracion As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim prioridad, frecuenciaRepeticionMinutos, activo, tipoAlerta, idAlertaDefinicionPorFormato, fechaInicio, horaInicio, minutosInicio As String

    Try
      idAlertaDefinicionPorFormato = Request(Me.ddlNombre.ClientID)
      prioridad = Utilidades.IsNull(Me.ddlPrioridad.SelectedValue, "")
      frecuenciaRepeticionMinutos = Utilidades.IsNull(Me.txtFrecuenciaRepeticion.Text, "-1")
      tipoAlerta = Utilidades.IsNull(Me.ddlTipoAlerta.SelectedValue, "")
      activo = IIf(Me.chkActivo.Checked, "1", "0")
      fechaInicio = Me.txtFechaInicio.Text
      horaInicio = Utilidades.IsNull(Me.wucHoraInicio.SelectedValueHora, "00")
      minutosInicio = Utilidades.IsNull(Me.wucHoraInicio.SelectedValueMinutos, "00")

      If (String.IsNullOrEmpty(fechaInicio)) Then
        fechaInicio = "-1"
      Else
        fechaInicio = fechaInicio & " " & horaInicio & ":" & minutosInicio
      End If

      If idAlertaConfiguracion = "-1" Then Sistema.GrabarLogSesion(oUsuario.Id, "Crea nueva configuración alerta: IdAlertaDefinicionPorFormato " & idAlertaDefinicionPorFormato) Else Sistema.GrabarLogSesion(oUsuario.Id, "Modifica configuración alerta: IdAlertaDefinicionPorFormato " & idAlertaDefinicionPorFormato)
      status = Alerta.GrabarDatosConfiguracion(idAlertaConfiguracion, idAlertaDefinicionPorFormato, prioridad, frecuenciaRepeticionMinutos, activo, tipoAlerta, fechaInicio)
      If status = "duplicado" Then idAlertaConfiguracion = "-1"

      If (idAlertaConfiguracion > 0) Then
        Me.GrabarDatosRegistro_Escalamientos(idAlertaConfiguracion)
      End If
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la actividad.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaConfiguracion As String = Me.ViewState.Item("idAlertaConfiguracion")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    textoMensaje &= GrabarDatosRegistro(status, idAlertaConfiguracion)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La ALERTA fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La ALERTA fue actualizada satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}La ALERTA ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la ALERTA. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la ALERTA. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idAlertaConfiguracion = "-1", "", "[ID: " & idAlertaConfiguracion & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Alerta.cerrarPopUpMantenedor('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idAlertaConfiguracion
      Response.Redirect("./Mantenedor.AlertaDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' elimina los datos del registro
  ''' </summary>
  ''' <returns></returns>
  Private Function EliminarRegistro(ByRef status As String, ByVal idAlertaConfiguracion As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""

    Try
      status = Alerta.EliminarDatosConfiguracion(idAlertaConfiguracion)
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina configuración alerta: IdAlertaConfiguracion " & idAlertaConfiguracion)
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo eliminar la alerta<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' construye json con los escalamientos de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConstruirJSONEscalamientos(ByVal ds As DataSet) As String
    Dim json As String = "[]"
    Dim idAlertaConfiguracion As String = Me.ViewState.Item("idAlertaConfiguracion")
    Dim dt As DataTable
    Dim totalRegistros As Integer
    Dim idEscalamientoPorAlerta As String
    Dim serializer As JavaScriptSerializer = New JavaScriptSerializer()
    Dim objColumna As Dictionary(Of String, Object)
    Dim contador As Integer = 0
    Dim sb As New StringBuilder
    Dim objListado As Object
    Dim contadoGrupos As Integer = 0

    Try
      If (idAlertaConfiguracion <> "-1") Then
        dt = ds.Tables(Alerta.eTablaConfiguracion.T01_Escalamientos)
        totalRegistros = dt.Rows.Count

        If (totalRegistros > 0) Then
          sb.Append("[")
          For Each dr As DataRow In dt.Rows
            idEscalamientoPorAlerta = dr.Item("idEscalamientoPorAlerta")

            objColumna = New Dictionary(Of String, Object)
            For Each col As DataColumn In dt.Columns
              objColumna.Add(col.ColumnName, dr(col))
            Next

            'obtiene los grupos asociados al escalamiento
            objListado = Me.ConstruirJSONEscalamientos_Grupos(ds, idEscalamientoPorAlerta, contadoGrupos)
            objColumna.Add("ListadoGrupos", objListado)

            sb.Append(serializer.Serialize(objColumna))
            If (contador < totalRegistros - 1) Then sb.Append(",")
            contador += 1
          Next
          sb.Append("]")
          json = sb.ToString()
        End If
      End If
    Catch ex As Exception
      json = "[]"
    End Try

    Return json
  End Function

  ''' <summary>
  ''' construye json con los grupos del escalamiento
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConstruirJSONEscalamientos_Grupos(ByVal ds As DataSet, ByVal idEscalamientoPorAlerta As String, ByRef contadoGrupos As Integer) As Object
    Dim dt As DataTable
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim totalRegistros As Integer
    Dim objColumna As Dictionary(Of String, Object)
    Dim objJSON As New List(Of Object)
    Dim sb As New StringBuilder
    Dim objListado As Object
    Dim idEscalamientoPorAlertaGrupoContacto As String

    Try
      dt = ds.Tables(Alerta.eTablaConfiguracion.T02_GrupoContactos)
      totalRegistros = dt.Rows.Count

      If (totalRegistros > 0) Then
        dv = dt.DefaultView
        dv.RowFilter = "idEscalamientoPorAlerta = " & idEscalamientoPorAlerta
        dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""

        dt = dsFiltrado.Tables(0)
        For Each dr As DataRow In dt.Rows
          contadoGrupos += 1
          idEscalamientoPorAlertaGrupoContacto = dr.Item("idEscalamientoPorAlertaGrupoContacto")

          objColumna = New Dictionary(Of String, Object)
          For Each col As DataColumn In dt.Columns
            objColumna.Add(col.ColumnName, dr(col))
          Next

          'crea columna CODIGO para grabar el valor generado durante la modificacion en el formulario
          objColumna.Add("Codigo", "GP-" & contadoGrupos)

          'obtiene los contactos asociados al grupo
          objListado = Me.ConstruirJSONEscalamientos_Grupos_Contactos(ds, idEscalamientoPorAlertaGrupoContacto)
          objColumna.Add("ListadoContactos", objListado)

          'obtiene los script asociados al grupo
          objListado = Me.ConstruirJSONEscalamientos_Grupos_Script(ds, idEscalamientoPorAlertaGrupoContacto)
          objColumna.Add("ListadoScript", objListado)

          objJSON.Add(objColumna)
        Next
      End If
    Catch ex As Exception
      objJSON = New List(Of Object)
    End Try

    Return objJSON
  End Function

  ''' <summary>
  ''' construye json con los contactos del grupo
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConstruirJSONEscalamientos_Grupos_Contactos(ByVal ds As DataSet, ByVal idEscalamientoPorAlertaGrupoContacto As String) As Object
    Dim dt As DataTable
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim totalRegistros As Integer
    Dim objColumna As Dictionary(Of String, Object)
    Dim objJSON As New List(Of Object)
    Dim sb As New StringBuilder

    Try
      dt = ds.Tables(Alerta.eTablaConfiguracion.T03_UsuarioGrupoContactos)
      totalRegistros = dt.Rows.Count

      If (totalRegistros > 0) Then
        dv = dt.DefaultView
        dv.RowFilter = "idEscalamientoPorAlertaGrupoContacto = " & idEscalamientoPorAlertaGrupoContacto
        dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""

        dt = dsFiltrado.Tables(0)
        For Each dr As DataRow In dt.Rows
          objColumna = New Dictionary(Of String, Object)
          For Each col As DataColumn In dt.Columns
            objColumna.Add(col.ColumnName, dr(col))
          Next

          objJSON.Add(objColumna)
        Next
      End If
    Catch ex As Exception
      objJSON = New List(Of Object)
    End Try

    Return objJSON
  End Function

  ''' <summary>
  ''' construye json con los script del grupo
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConstruirJSONEscalamientos_Grupos_Script(ByVal ds As DataSet, ByVal idEscalamientoPorAlertaGrupoContacto As String) As Object
    Dim dt As DataTable
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim totalRegistros As Integer
    Dim objColumna As Dictionary(Of String, Object)
    Dim objJSON As New List(Of Object)
    Dim sb As New StringBuilder

    Try
      dt = ds.Tables(Alerta.eTablaConfiguracion.T04_Script)
      totalRegistros = dt.Rows.Count

      If (totalRegistros > 0) Then
        dv = dt.DefaultView
        dv.RowFilter = "idEscalamientoPorAlertaGrupoContacto = " & idEscalamientoPorAlertaGrupoContacto
        dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""

        dt = dsFiltrado.Tables(0)
        For Each dr As DataRow In dt.Rows
          objColumna = New Dictionary(Of String, Object)
          For Each col As DataColumn In dt.Columns
            objColumna.Add(col.ColumnName, dr(col))
          Next

          objJSON.Add(objColumna)
        Next

      End If
    Catch ex As Exception
      objJSON = New List(Of Object)
    End Try

    Return objJSON
  End Function

  ''' <summary>
  ''' graba los datos de los escalamientos
  ''' </summary>
  ''' <param name="idAlertaConfiguracion"></param>
  ''' <remarks></remarks>
  Private Sub GrabarDatosRegistro_Escalamientos(ByVal idAlertaConfiguracion As String)
    Dim json, idEscalamientoPorAlerta, orden, emailCopia As String
    Dim objJSON, row, objListadoGrupos As Object
    Dim lsListado As New List(Of String)
    Dim listado, status As String

    Try
      json = Me.txtJSONEscalamientos.Value
      objJSON = MyJSON.ConvertJSONToObject(json)

      '-------------------------------------------------------------
      'busca los escalamientos que no esten referenciados y los elimina
      lsListado.Add("-1")
      For Each row In objJSON
        idEscalamientoPorAlerta = MyJSON.ItemObject(row, "IdEscalamientoPorAlerta")
        lsListado.Add(idEscalamientoPorAlerta)
      Next
      listado = String.Join(",", lsListado.ToArray())
      'elimina los registros que no estan referenciados en la lista
      status = Alerta.EliminarConfiguracionEscalamientosNoReferenciados(idAlertaConfiguracion, listado)
      If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo eliminar los escalamientos que no están referenciados")

      '-------------------------------------------------------------
      'recorre los escalamientos
      For Each row In objJSON
        idEscalamientoPorAlerta = MyJSON.ItemObject(row, "IdEscalamientoPorAlerta")
        orden = MyJSON.ItemObject(row, "Orden")
        emailCopia = MyJSON.ItemObject(row, "EmailCopia")
        objListadoGrupos = MyJSON.ItemObjectJSON(row, "ListadoGrupos")

        'formatea valores
        idEscalamientoPorAlerta = IIf(idEscalamientoPorAlerta < 0, -1, idEscalamientoPorAlerta)
        emailCopia = emailCopia.Replace(",", ";")

        'graba registro
        idEscalamientoPorAlerta = Alerta.GrabarConfiguracionEscalamiento(idEscalamientoPorAlerta, idAlertaConfiguracion, orden, emailCopia)

        If (idEscalamientoPorAlerta < 0) Then
          Throw New Exception("No se pudo grabar el escalamiento")
        Else
          Me.GrabarDatosRegistro_Escalamientos_Grupos(idEscalamientoPorAlerta, objListadoGrupos)
        End If
      Next
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' graba los datos de los grupos del escalamiento
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarDatosRegistro_Escalamientos_Grupos(ByVal idEscalamientoPorAlerta As String, ByVal objListadoGrupos As Object)
    Dim idEscalamientoPorAlertaGrupoContacto, nombreGrupo, ordenGrupo, mostrarCuandoEscalamientoAnteriorNoContesta As String
    Dim row, objListadoContactos, objListadoScript As Object
    Dim listado, status, tieneDependenciaGrupoEscalamientoAnterior, idGrupoContactoEscalamientoAnterior, idGrupoKey As String
    Dim mostrarSiempre, idGrupoContactoPadre, notificarPorEmail, horaAtencionInicio, horaAtencionTermino As String
    Dim lsListado As New List(Of String)

    Try
      '-------------------------------------------------------------
      'busca los grupos que no esten referenciados y los elimina
      lsListado.Add("-1")
      For Each row In objListadoGrupos
        idEscalamientoPorAlertaGrupoContacto = MyJSON.ItemObject(row, "IdEscalamientoPorAlertaGrupoContacto")
        lsListado.Add(idEscalamientoPorAlertaGrupoContacto)
      Next
      listado = String.Join(",", lsListado.ToArray())
      'elimina los registros que no estan referenciados en la lista
      status = Alerta.EliminarConfiguracionGruposNoReferenciados(idEscalamientoPorAlerta, listado)
      If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo eliminar los grupos que no están referenciados")

      '-------------------------------------------------------------
      'recorre los grupos
      For Each row In objListadoGrupos
        idEscalamientoPorAlertaGrupoContacto = MyJSON.ItemObject(row, "IdEscalamientoPorAlertaGrupoContacto")
        idGrupoKey = MyJSON.ItemObject(row, "IdEscalamientoPorAlertaGrupoContacto")
        nombreGrupo = MyJSON.ItemObject(row, "NombreGrupo")
        ordenGrupo = MyJSON.ItemObject(row, "OrdenGrupo")
        mostrarCuandoEscalamientoAnteriorNoContesta = MyJSON.ItemObject(row, "MostrarCuandoEscalamientoAnteriorNoContesta")
        tieneDependenciaGrupoEscalamientoAnterior = MyJSON.ItemObject(row, "TieneDependenciaGrupoEscalamientoAnterior")
        idGrupoContactoEscalamientoAnterior = MyJSON.ItemObject(row, "IdGrupoContactoEscalamientoAnterior")
        mostrarSiempre = MyJSON.ItemObject(row, "MostrarSiempre")
        idGrupoContactoPadre = MyJSON.ItemObject(row, "IdGrupoContactoPadre")
        notificarPorEmail = MyJSON.ItemObject(row, "NotificarPorEmail")
        horaAtencionInicio = MyJSON.ItemObject(row, "HoraAtencionInicio")
        horaAtencionTermino = MyJSON.ItemObject(row, "HoraAtencionTermino")
        objListadoContactos = MyJSON.ItemObjectJSON(row, "ListadoContactos")
        objListadoScript = MyJSON.ItemObjectJSON(row, "ListadoScript")

        'asigna idEscalamientoPorAlertaGrupoContacto en arreglo para saber con que ID quedo grabado en la base de datos para luego usarlo cuando un registro lo tenga asociado
        SL_GRUPO_CONTACTO.Add(idGrupoKey, -1)

        'formatea valores
        idEscalamientoPorAlertaGrupoContacto = IIf(idEscalamientoPorAlertaGrupoContacto < 0, -1, idEscalamientoPorAlertaGrupoContacto)
        idGrupoContactoEscalamientoAnterior = IIf(tieneDependenciaGrupoEscalamientoAnterior = 0, "-1", IIf(idGrupoContactoEscalamientoAnterior < 0, -1, idGrupoContactoEscalamientoAnterior))
        idGrupoContactoPadre = IIf(mostrarSiempre = 1, "-1", IIf(idGrupoContactoPadre < 0, -1, idGrupoContactoPadre))
        horaAtencionInicio = horaAtencionInicio.Replace(":", "")
        horaAtencionTermino = horaAtencionTermino.Replace(":", "")

        'graba registro
        idEscalamientoPorAlertaGrupoContacto = Alerta.GrabarConfiguracionGrupoContacto(idEscalamientoPorAlertaGrupoContacto, idEscalamientoPorAlerta, nombreGrupo, ordenGrupo, mostrarCuandoEscalamientoAnteriorNoContesta, _
                                                                                       tieneDependenciaGrupoEscalamientoAnterior, idGrupoContactoEscalamientoAnterior, mostrarSiempre, idGrupoContactoPadre, notificarPorEmail, _
                                                                                       horaAtencionInicio, horaAtencionTermino)

        If (idEscalamientoPorAlertaGrupoContacto < 0) Then
          Throw New Exception("No se pudo grabar el grupo de contacto")
        Else
          'actualiza idEscalamientoPorAlertaGrupoContacto en arreglo con el ID generado desde base datos
          SL_GRUPO_CONTACTO.Item(idGrupoKey) = idEscalamientoPorAlertaGrupoContacto

          Me.GrabarDatosRegistro_Escalamientos_Grupos_Contactos(idEscalamientoPorAlertaGrupoContacto, objListadoContactos)
          Me.GrabarDatosRegistro_Escalamientos_Grupos_Script(idEscalamientoPorAlertaGrupoContacto, objListadoScript)
        End If
      Next

      '-------------------------------------------------------------------------------------
      'recorre los grupos y graba las asociaciones de los grupos con dependencia y padres
      For Each row In objListadoGrupos
        idEscalamientoPorAlertaGrupoContacto = MyJSON.ItemObject(row, "IdEscalamientoPorAlertaGrupoContacto")
        tieneDependenciaGrupoEscalamientoAnterior = MyJSON.ItemObject(row, "TieneDependenciaGrupoEscalamientoAnterior")
        idGrupoContactoEscalamientoAnterior = MyJSON.ItemObject(row, "IdGrupoContactoEscalamientoAnterior")
        mostrarSiempre = MyJSON.ItemObject(row, "MostrarSiempre")
        idGrupoContactoPadre = MyJSON.ItemObject(row, "IdGrupoContactoPadre")

        'formatea valores
        idEscalamientoPorAlertaGrupoContacto = SL_GRUPO_CONTACTO.Item(idEscalamientoPorAlertaGrupoContacto)
        idGrupoContactoEscalamientoAnterior = IIf(tieneDependenciaGrupoEscalamientoAnterior = 0, "-1", SL_GRUPO_CONTACTO.Item(idGrupoContactoEscalamientoAnterior)) 'obtiene id del grupo que tiene relacionado
        If (String.IsNullOrEmpty(idGrupoContactoEscalamientoAnterior)) Then idGrupoContactoEscalamientoAnterior = "-1"
        idGrupoContactoPadre = IIf(mostrarSiempre = 1, "-1", SL_GRUPO_CONTACTO.Item(idGrupoContactoPadre)) 'obtiene id del grupo que tiene relacionado
        If (String.IsNullOrEmpty(idGrupoContactoPadre)) Then idGrupoContactoPadre = "-1"

        'graba registro
        idEscalamientoPorAlertaGrupoContacto = Alerta.ActualizarAsociacionGrupo(idEscalamientoPorAlertaGrupoContacto, idGrupoContactoEscalamientoAnterior, idGrupoContactoPadre)
      Next

    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' graba los datos de los contactos del grupo
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarDatosRegistro_Escalamientos_Grupos_Contactos(ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal objListadoContactos As Object)
    Dim idEscalamientoPorAlertaContacto, idUsuario, cargo, ordenContacto As String
    Dim row As Object
    Dim lsListado As New List(Of String)
    Dim listado, status As String

    Try
      '-------------------------------------------------------------
      'busca los contactos que no esten referenciados y los elimina
      lsListado.Add("-1")
      For Each row In objListadoContactos
        idEscalamientoPorAlertaContacto = MyJSON.ItemObject(row, "IdEscalamientoPorAlertaContacto")
        lsListado.Add(idEscalamientoPorAlertaContacto)
      Next
      listado = String.Join(",", lsListado.ToArray())
      'elimina los registros que no estan referenciados en la lista
      status = Alerta.EliminarConfiguracionContactosNoReferenciados(idEscalamientoPorAlertaGrupoContacto, listado)
      If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo eliminar los contactos que no están referenciados")

      '-------------------------------------------------------------
      'recorre los contactos
      For Each row In objListadoContactos
        idEscalamientoPorAlertaContacto = MyJSON.ItemObject(row, "IdEscalamientoPorAlertaContacto")
        idUsuario = MyJSON.ItemObject(row, "IdUsuario")
        cargo = MyJSON.ItemObject(row, "Cargo")
        ordenContacto = MyJSON.ItemObject(row, "OrdenContacto")

        'formatea valores
        idEscalamientoPorAlertaContacto = IIf(idEscalamientoPorAlertaContacto < 0, -1, idEscalamientoPorAlertaContacto)

        'graba registro
        idEscalamientoPorAlertaContacto = Alerta.GrabarConfiguracionContacto(idEscalamientoPorAlertaContacto, idEscalamientoPorAlertaGrupoContacto, idUsuario, cargo, ordenContacto)

        If (idEscalamientoPorAlertaContacto < 0) Then
          Throw New Exception("No se pudo grabar el contacto")
        End If
      Next
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' graba los datos de los script del grupo
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarDatosRegistro_Escalamientos_Grupos_Script(ByVal idEscalamientoPorAlertaGrupoContacto As String, ByVal objListadoScript As Object)
    Dim idScript, nombre, descripcion, activo As String
    Dim row As Object
    Dim lsListado As New List(Of String)
    Dim listado, status As String

    Try
      '-------------------------------------------------------------
      'busca los script que no esten referenciados y los elimina
      lsListado.Add("-1")
      For Each row In objListadoScript
        idScript = MyJSON.ItemObject(row, "IdScript")
        lsListado.Add(idScript)
      Next
      listado = String.Join(",", lsListado.ToArray())
      'elimina los registros que no estan referenciados en la lista
      status = Alerta.EliminarConfiguracionScriptNoReferenciados(idEscalamientoPorAlertaGrupoContacto, listado)
      If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo eliminar los script que no están referenciados")

      For Each row In objListadoScript
        idScript = MyJSON.ItemObject(row, "IdScript")
        nombre = MyJSON.ItemObject(row, "Nombre")
        descripcion = MyJSON.ItemObject(row, "Descripcion")
        activo = MyJSON.ItemObject(row, "Activo")

        'formatea valores
        idScript = IIf(idScript < 0, -1, idScript)

        'graba registro
        idScript = Alerta.GrabarConfiguracionScript(idScript, idEscalamientoPorAlertaGrupoContacto, nombre, descripcion, activo)

        If (idScript < 0) Then
          Throw New Exception("No se pudo grabar el script")
        End If
      Next
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' construye json con la secuencia de padre-hijo
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConstruirJSONPadreHijo(ByVal ds As DataSet) As String
    Dim json As String = "[]"
    Dim idAlertaConfiguracion As String = Me.ViewState.Item("idAlertaConfiguracion")
    Dim dt As DataTable
    Dim dv As DataView
    Dim dsGruposPadre As New DataSet
    Dim totalRegistros As Integer
    Dim dicPadre As New Dictionary(Of String, Object)
    Dim lstJSON As New List(Of Object)
    Dim arrayGroupBy As New ArrayList
    Dim objJSONGruposPadre As Object

    Try
      If (idAlertaConfiguracion <> "-1") Then
        dt = ds.Tables(Alerta.eTablaConfiguracion.T02_GrupoContactos)
        totalRegistros = dt.Rows.Count

        If (totalRegistros > 0) Then
          dv = dt.DefaultView
          dv.RowFilter = "IdGrupoContactoPadre = -1"
          dsGruposPadre = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
          dv.RowFilter = ""

          'calcula nuevamente la cantidad de registros luego de realizar el filtro
          dt = dsGruposPadre.Tables(0)
          totalRegistros = dt.Rows.Count

          If (totalRegistros > 0) Then
            'agrupa los grupos padres por cada escalamiento
            arrayGroupBy = Utilidades.ObtenerArrayGroupByDeDataTable(dt, "idEscalamientoPorAlerta")

            For Each idEscalamientoPorAlerta As String In arrayGroupBy
              objJSONGruposPadre = Me.ConstruirJSONPadreHijo_ListadoPadres(idEscalamientoPorAlerta, dsGruposPadre, ds)

              dicPadre = New Dictionary(Of String, Object)
              dicPadre.Add("IdEscalamientoPorAlerta", idEscalamientoPorAlerta)
              dicPadre.Add("ListadoGruposPadre", objJSONGruposPadre)

              lstJSON.Add(dicPadre)
            Next

            json = MyJSON.ConvertObjectToJSON(lstJSON)
          End If
        End If
      End If
    Catch ex As Exception
      json = "[]"
    End Try

    Return json
  End Function

  ''' <summary>
  ''' construye el listado de grupos PADRE
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConstruirJSONPadreHijo_ListadoPadres(ByVal idEscalamientoPorAlerta As String, ByVal dsGruposPadre As DataSet, ByVal ds As DataSet) As Object
    Dim dt, dtHijo As DataTable
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim totalRegistros As Integer
    Dim dicPadre As New Dictionary(Of String, Object)
    Dim dicHijo As New Dictionary(Of String, Object)
    Dim lstJSONPadre As New List(Of Object)
    Dim lstJSONHijo As New List(Of Object)
    Dim idEscalamientoPorAlertaGrupoContacto, idGrupoContactoPadre As String
    Dim tieneHijos As Boolean

    Try
      dt = dsGruposPadre.Tables(0)
      dv = dt.DefaultView
      dv.RowFilter = "IdEscalamientoPorAlerta = " & idEscalamientoPorAlerta
      dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      dv.RowFilter = ""

      'calcula nuevamente la cantidad de registros luego de realizar el filtro
      dt = dsFiltrado.Tables(0)
      totalRegistros = dt.Rows.Count

      If (totalRegistros > 0) Then
        For Each dr As DataRow In dt.Rows
          tieneHijos = True
          idEscalamientoPorAlertaGrupoContacto = dr.Item("IdEscalamientoPorAlertaGrupoContacto")
          lstJSONHijo = New List(Of Object)

          'agrega el primer padre encontrado
          dicHijo = New Dictionary(Of String, Object)
          dicHijo.Add("IdEscalamientoPorAlertaGrupoContactoHijo", idEscalamientoPorAlertaGrupoContacto)
          dicHijo.Add("IdGrupoContactoPadre", "-1")
          lstJSONHijo.Add(dicHijo)

          'revisa si el padre tiene mas hijos en el dataset principal
          Do While (tieneHijos)
            dtHijo = ds.Tables(Alerta.eTablaConfiguracion.T02_GrupoContactos)
            dv = dtHijo.DefaultView
            dv.RowFilter = "IdGrupoContactoPadre = " & idEscalamientoPorAlertaGrupoContacto
            dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

            'cuenta cuantos hijos encontro
            totalRegistros = dsFiltrado.Tables(0).Rows.Count
            If (totalRegistros = 0) Then
              tieneHijos = False
            Else
              idEscalamientoPorAlertaGrupoContacto = dsFiltrado.Tables(0).Rows(0).Item("IdEscalamientoPorAlertaGrupoContacto")
              idGrupoContactoPadre = dsFiltrado.Tables(0).Rows(0).Item("IdGrupoContactoPadre")
              dicHijo = New Dictionary(Of String, Object)
              dicHijo.Add("IdEscalamientoPorAlertaGrupoContactoHijo", idEscalamientoPorAlertaGrupoContacto)
              dicHijo.Add("IdGrupoContactoPadre", idGrupoContactoPadre)
              lstJSONHijo.Add(dicHijo)
            End If

          Loop

          'agrega toda la descendencia del padre
          dicPadre = New Dictionary(Of String, Object)
          dicPadre.Add("ListadoGruposHijo", lstJSONHijo)
          lstJSONPadre.Add(dicPadre)
        Next

      End If
    Catch ex As Exception
      lstJSONPadre = New List(Of Object)
    End Try

    Return lstJSONPadre
  End Function


#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaConfiguracion As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idAlertaConfiguracion", idAlertaConfiguracion)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaConfiguracion As String = Me.ViewState.Item("idAlertaConfiguracion")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim eliminadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= EliminarRegistro(status, idAlertaConfiguracion)

    If textoMensaje <> "" Then
      eliminadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If eliminadoConExito Then
      Select Case status
        Case "eliminado"
          textoMensaje = "{ELIMINADO}La ALERTA fue eliminada satisfactoriamente. Complete el formulario para ingresar una nueva"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar la ALERTA. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar la ALERTA. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    queryStringPagina = "id=" & IIf(status = "eliminado", "-1", idAlertaConfiguracion)
    Response.Redirect("./Mantenedor.AlertaDetalle.aspx?" & queryStringPagina)

  End Sub

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Transportista.EncuestaDatos.aspx.vb" Inherits="webtransportewalmartmx.Transportista_EncuestaDatos" %>
<%@ Register Src="~/wuc/wucMultiselectCombinados.ascx" TagName="wucMultiselectCombinados" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/transportista.js"></script>

  <input id="hJson" type="hidden" value="" runat="server"/>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Equipamiento Remolque</p>
      </div>     
      <br />
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    
    <asp:Panel ID="pnlContenido" Runat="server">
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>      
        <asp:Panel ID="pnlFormulario" runat="server">
          
            <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">DATOS ENCUESTA</h3></div>
            <div class="panel-body">

            <div class="form-group">
              <div class="row">
              <div class="col-xs-2">
                <label class="control-label">SU FLOTA POSEE GPS?</label><br />
                <asp:RadioButton ID="rbSi" runat="server" CssClass="control-label" Text="&nbsp;Si" GroupName="rb" onclick="Transportista.mostrarDiv({div:'proveedores'});"/>&nbsp;&nbsp;
                <asp:RadioButton ID="rbNo" runat="server" CssClass="control-label" Text="&nbsp;No" GroupName="rb" onclick="Transportista.mostrarDiv({div:'patentes'});"/>&nbsp;&nbsp;
              </div>
              </div>
            </div>
            
            <!-- Patentes -->
            <div id="dvPatentes" style="display:none">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <div><label class="control-label">Placa<strong class="text-danger">&nbsp;*</strong></label></div>
                    <div class="col-sm-1 sist-padding-cero">
                      <asp:TextBox ID="txtPatente1" runat="server" MaxLength="2" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-1 sist-padding-cero">
                      <asp:TextBox ID="txtPatente2" runat="server" MaxLength="2" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-sm-1 sist-padding-cero">
                      <asp:TextBox ID="txtPatente3" runat="server" MaxLength="2" CssClass="form-control"></asp:TextBox>                                   
                    </div>
                    <div class="col-sm-9 sist-padding-uno">
                      <div id="btnAgregar" runat="server" class="btn btn-success" onclick="Transportista.agregarPatente()"><span class="glyphicon glyphicon-plus"></span>&nbsp;Agregar</div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6">
                    <input id="hPatente" type="hidden" value="" runat="server"/>
                    <div id="divErrorPatente"></div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <table id="tbPatente" class="table table-condensed">
                      <thead>
                      <tr>
                      <th style="width:350px">Placa</th>
                      <th style="width:120px">Opciones</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr class="sist-display-none" id="{PREFIJO_CONTROL}_tr">
                        <td>
	                        <input type="hidden" value="{VALOR}" class="id-patente" />{TEXTO}
                        </td>
                        <td><button type="button" class="btn btn-default" onclick="Transportista.eliminarPatente({ patente:'{VALOR}' })"><span class="glyphicon glyphicon-trash"></span></button>&nbsp;</td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            
            </div>

            <!-- Proveedores -->
            <div id="dvProveedores" style="display:none">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-8">
                  
                   <uc2:wucMultiselectCombinados id="msProveedores" runat="server" TextLabelPadre="Proveedor" VisibleLabelAyuda="true" VisibleCheckTodosPadre="false" VisibleCheckTodosHija="false" RowsListBoxHija="10" RowsListBoxPadre="10"></uc2:wucMultiselectCombinados>
                   <div id="lblMensajeErrormsEstado"></div>

                  </div>
                </div>
              </div>

            </div>
            </div>
          </div>

          <div class="form-group text-center">
            <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="Transportista.ValidarPatente()"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
          </div>
      </asp:Panel>
      
      </div>
      </asp:Panel>
   </div>          
 


  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

    });
  </script>

</asp:Content>

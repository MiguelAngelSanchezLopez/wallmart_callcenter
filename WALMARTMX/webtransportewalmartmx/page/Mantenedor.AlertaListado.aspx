﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Mantenedor.AlertaListado.aspx.vb" Inherits="webtransportewalmartmx.Mantenedor_AlertaListado" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Alertas</p>
      </div>
      <div class="col-xs-4 text-right">
        <div id="btnNuevo" runat="server" class="btn btn-success" onclick="Alerta.abrirFormularioMantenedor({ idAlertaConfiguracion:'-1' })"><span class="glyphicon glyphicon-plus"></span>&nbsp;Configurar Nueva Alerta</div>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body small">
          <div class="row">
            <div class="col-xs-2">
              <label class="control-label">Nombre</label>
              <asp:TextBox ID="txtFiltroNombre" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Formato</label>
              <uc1:wucCombo id="ddlFiltroFormato" runat="server" FuenteDatos="Tabla" TipoCombo="Formato" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Tipo Alerta</label>
              <uc1:wucCombo id="ddlFiltroTipoAlerta" runat="server" FuenteDatos="TipoGeneral" TipoCombo="TipoAlerta" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Prioridad</label>
              <uc1:wucCombo id="ddlFiltroPrioridad" runat="server" FuenteDatos="Tabla" TipoCombo="Prioridad" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-1">
              <label class="control-label">Activos</label>
              <div class="checkbox"><asp:CheckBox ID="chkFiltroSoloActivos" runat="server" /></div>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-1">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Alerta.validarFormularioBusquedaMantenedor())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>

          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="IdAlertaConfiguracion" HeaderText="ID" SortExpression="IdAlertaConfiguracion" HeaderStyle-Width="80px" />
              <asp:BoundField DataField="NombreAlerta" HeaderText="Nombre" SortExpression="NombreAlerta" />
              <asp:BoundField DataField="NombreFormato" HeaderText="Formato" SortExpression="NombreFormato" />
              <asp:BoundField DataField="TipoAlerta" HeaderText="Tipo Alerta" SortExpression="TipoAlerta" />
              <asp:BoundField DataField="Prioridad" HeaderText="Prioridad" SortExpression="Prioridad" HeaderStyle-Width="100px" />
              <asp:BoundField DataField="Activo" HeaderText="Activo" SortExpression="Activo" HeaderStyle-Width="100px" />
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="110px" />
                <ItemTemplate>
                  <input type="hidden" id="txtIdAlertaConfiguracion" runat="server" value='<%# Bind("IdAlertaConfiguracion") %>' />
                  <input type="hidden" id="txtTieneAlertasAsociadas" runat="server" value='<%# Bind("TieneAlertasAsociadas") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>      
      </div>

    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

    });
  </script>

</asp:Content>

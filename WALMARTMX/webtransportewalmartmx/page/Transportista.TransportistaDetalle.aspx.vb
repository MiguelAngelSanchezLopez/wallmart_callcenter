﻿Imports CapaNegocio
Imports System.Data

Public Class Transportista_TransportistaDetalle
  Inherits System.Web.UI.Page

  Dim listadoIdProveedores As String = ""



#Region "Enum"
  Private Enum eFunciones
    Ver
    Grabar
  End Enum

  Private Enum eTabla As Integer
    T00_Detalle = 0
    T01_FichaTransportista = 1
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idTransportista = oUsuario.Id

    'obtiene datos
    ds = Transportista.ObtenerDetalle(idTransportista)

    'guarda el resultado en session
    If ds Is Nothing Then
      Session("ds") = Nothing
    Else
      Session("ds") = ds
    End If

  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim idFicha, VolumenFlota As Integer
    Dim rutEmpresa, NombreEmpresa, NombreDueño, Telefono1Dueño, Telefono2Dueño, EmailDueño, NombreGerenteOperacion, TelefonoGerenteOperacion, EmailGerenteOperacion, Telefono1Escalamiento, Telefono2Escalamiento, Telefono3Escalamiento, Telefono4Escalamiento As String

    ds = CType(Session("ds"), DataSet)

    If ds Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = ds.Tables(eTabla.T00_Detalle).Rows.Count
    End If

    idFicha = -1
    rutEmpresa = ""
    NombreEmpresa = ""
    VolumenFlota = 0
    NombreDueño = ""
    Telefono1Dueño = ""
    Telefono2Dueño = ""
    EmailDueño = ""
    NombreGerenteOperacion = ""
    TelefonoGerenteOperacion = ""
    EmailGerenteOperacion = ""
    Telefono1Escalamiento = ""
    Telefono2Escalamiento = ""
    Telefono3Escalamiento = ""
    Telefono4Escalamiento = ""

    If totalRegistros > 0 Then

      dr = ds.Tables(eTabla.T00_Detalle).Rows(0)
      rutEmpresa = dr.Item("Rut")
      NombreEmpresa = dr.Item("Nombre")

      totalRegistros = ds.Tables(eTabla.T01_FichaTransportista).Rows.Count
      If totalRegistros > 0 Then
        dr = ds.Tables(eTabla.T01_FichaTransportista).Rows(0)
        idFicha = dr.Item("id")
        VolumenFlota = dr.Item("VolumenFlota")
        NombreDueño = dr.Item("NombreDueño")
        Telefono1Dueño = dr.Item("Telefono1Dueño")
        Telefono2Dueño = dr.Item("Telefono2Dueño")
        EmailDueño = dr.Item("EmailDueño")
        NombreGerenteOperacion = dr.Item("NombreGerenteOperacion")
        TelefonoGerenteOperacion = dr.Item("TelefonoGerenteOperacion")
        EmailGerenteOperacion = dr.Item("EmailGerenteOperacion")
        Telefono1Escalamiento = dr.Item("Telefono1Escalamiento")
        Telefono2Escalamiento = dr.Item("Telefono2Escalamiento")
        Telefono3Escalamiento = dr.Item("Telefono3Escalamiento")
        Telefono4Escalamiento = dr.Item("Telefono4Escalamiento")
      End If

    End If

    Me.txtRutEmpresa.Text = rutEmpresa
    Me.txtNombreEmpresa.Text = NombreEmpresa
    Me.txtVolumen.Text = VolumenFlota
    Me.txtNombreDueño.Text = NombreDueño
    Me.txtTelefono1Dueño.Text = Telefono1Dueño
    Me.txtTelefono2Dueño.Text = Telefono2Dueño
    Me.txtEmailDueño.Text = EmailDueño
    Me.txtNombreGerente.Text = NombreGerenteOperacion
    Me.txtTelefonoGerente.Text = TelefonoGerenteOperacion
    Me.txtEmailGerente.Text = EmailGerenteOperacion
    Me.txtTelefono1Escalamiento.Text = Telefono1Escalamiento
    Me.txtTelefono2Escalamiento.Text = Telefono2Escalamiento
    Me.txtTelefono3Escalamiento.Text = Telefono3Escalamiento
    Me.txtTelefono4Escalamiento.Text = Telefono4Escalamiento
    Me.ViewState.Item("idFicha") = idFicha
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Grabar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarDatos()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idTransportista As Integer = "-1"
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosDetalle(status)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La Línea de Transporte fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La Línea de Transporte fue actualizada satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la Línea de Transporte. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la Línea de Transporte. Intente nuevamente por favor.<br />" & textoMensaje
    End If

    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idTransportista = "-1", "", "[ID: " & idTransportista & "] ") & textoMensaje

    Response.Redirect(HttpContext.Current.Request.Url.LocalPath)
  End Sub

  ''' <summary>
  ''' graba los datos de la encuesta
  ''' </summary>
  ''' <param name="status"></param>
  ''' <returns></returns>
  Private Function GrabarDatosDetalle(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim ds As DataSet = Session("ds")
    Dim idFicha, idTransportista, VolumenFlota As Integer
    Dim NombreDueño, Telefono1Dueño, Telefono2Dueño, EmailDueño, NombreGerenteOperacion, TelefonoGerenteOperacion, EmailGerenteOperacion, Telefono1Escalamiento, Telefono2Escalamiento, Telefono3Escalamiento, Telefono4Escalamiento As String


    Try
      idFicha = Me.ViewState.Item("idFicha")
      idTransportista = oUsuario.Id

      VolumenFlota = Utilidades.IsNull(Me.txtVolumen.Text, "")
      NombreDueño = Utilidades.IsNull(Me.txtNombreDueño.Text, "")
      Telefono1Dueño = Utilidades.IsNull(Me.txtTelefono1Dueño.Text, "")
      Telefono2Dueño = Utilidades.IsNull(Me.txtTelefono2Dueño.Text, "")
      EmailDueño = Utilidades.IsNull(Me.txtEmailDueño.Text, "")
      NombreGerenteOperacion = Utilidades.IsNull(Me.txtNombreGerente.Text, "")
      TelefonoGerenteOperacion = Utilidades.IsNull(Me.txtTelefonoGerente.Text, "")
      EmailGerenteOperacion = Utilidades.IsNull(Me.txtEmailGerente.Text, "")
      Telefono1Escalamiento = Utilidades.IsNull(Me.txtTelefono1Escalamiento.Text, "")
      Telefono2Escalamiento = Utilidades.IsNull(Me.txtTelefono2Escalamiento.Text, "")
      Telefono3Escalamiento = Utilidades.IsNull(Me.txtTelefono3Escalamiento.Text, "")
      Telefono4Escalamiento = Utilidades.IsNull(Me.txtTelefono4Escalamiento.Text, "")

      If (idFicha = -1) Then
        status = "ingresado"
      Else
        status = "modificado"
      End If

      idFicha = Transportista.GrabarDatos(idFicha, VolumenFlota, NombreDueño, Telefono1Dueño, Telefono2Dueño, EmailDueño, NombreGerenteOperacion, TelefonoGerenteOperacion, EmailGerenteOperacion, Telefono1Escalamiento, Telefono2Escalamiento, Telefono3Escalamiento, Telefono4Escalamiento, idTransportista)
      Sistema.GrabarLogSesion(oUsuario.Id, "Se graba Ficha Línea de Transporte")

      If (idFicha = "-200") Then
        status = "error"
        Sistema.GrabarLogSesion(oUsuario.Id, "Error al grabar datos de ficha Línea de Transporte")
      End If

    Catch ex As Exception
      msgError = "Error al grabar datos de ficha Línea de Transporte.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  Protected Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
    GrabarDatos()
  End Sub
End Class
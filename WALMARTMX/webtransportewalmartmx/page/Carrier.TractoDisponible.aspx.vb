﻿Imports System.Web.Script.Serialization

Public Class Carrier_TractoDisponible
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim jsonString As String = Request("json")
    Dim jss As New JavaScriptSerializer
    Dim json As Object = jss.Deserialize(Of Object)(jsonString)

    lblCedis.Text = json.item("NombreCompletoCedis")
    lblPlacaTracto.Text = json.item("PlacaTracto")
    lblFechaLLegada.Text = json.item("FechaLlegada")
    lblTiempoEspera.Text = json.item("TiempoEspera")
    End Sub

End Class
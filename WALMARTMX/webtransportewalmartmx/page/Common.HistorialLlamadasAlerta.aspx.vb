﻿Imports CapaNegocio
Imports System.Data

Public Class Common_HistorialLlamadasAlerta
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim sbScript As New StringBuilder
    Dim idAlerta As String = Me.ViewState.Item("idAlerta")
    Dim nroEscalamiento As String = Me.ViewState.Item("nroEscalamiento")

    sbScript.Append("Alerta.obtenerDetalle({ idAlerta: '" & idAlerta & "', desdeFormulario: Alerta.FORMULARIO_COMMON_HISTORIAL_LLAMADAS_ALERTA });")
    Me.txtNroEscalamiento.Value = nroEscalamiento

    If (Not String.IsNullOrEmpty(sbScript.ToString())) Then
      Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "script_ActualizaInterfaz", False)
    End If

    Sistema.GrabarLogSesion(oUsuario.Id, "Visualiza historial llamadas alerta: IdAlerta " & idAlerta)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim idAlerta As String = Me.ViewState.Item("idAlerta")

    Me.btnRecargar.Attributes.Add("onclick", "Alerta.obtenerDetalle({ idAlerta: '" & idAlerta & "', desdeFormulario: Alerta.FORMULARIO_COMMON_HISTORIAL_LLAMADAS_ALERTA })")

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = True 'Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlerta As String = Utilidades.IsNull(Request("idAlerta"), "-1")
    Dim nroEscalamiento As String = Utilidades.IsNull(Request("nroEscalamiento"), "-1")

    Me.ViewState.Add("idAlerta", idAlerta)
    Me.ViewState.Add("nroEscalamiento", nroEscalamiento)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ActualizaInterfaz()
      InicializaControles(Page.IsPostBack)
    End If
  End Sub
End Class
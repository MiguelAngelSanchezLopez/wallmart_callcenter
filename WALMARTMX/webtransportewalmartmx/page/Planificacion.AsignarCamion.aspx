﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Planificacion.AsignarCamion.aspx.vb" Inherits="webtransportewalmartmx.Planificacion_AsignarCamion" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<%@ Register Src="../wuc/wucHora.ascx" TagName="wucHora" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/planificacion.js"></script>

  <div>
    <input type="hidden" id="txtJSONAsignarCamion" runat="server" value="[]" />
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="form-group small">
        <div class="panel panel-default">
          <div class="panel-heading"><h3 class="panel-title sist-font-size-12"><strong>ASIGNAR CAMI&Oacute;N</strong> > FILTROS DE B&Uacute;SQUEDA</h3></div>
          <div class="panel-body">
            <div class="row">
              <div class="col-xs-2">
                <label class="control-label">Fecha Presentaci&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtFechaPresentacion" runat="server" MaxLength="10" CssClass="form-control input-sm date-pick"></asp:TextBox>
              </div>
              <div class="col-xs-2">
                <label class="control-label">IdMaster</label>
                <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="150" CssClass="form-control input-sm"></asp:TextBox>
              </div>
                <div class="col-xs-2">
                <label class="control-label">Hora Presentaci&oacute;n</label>
                <uc2:wucHora ID="wucHoraInicio" runat="server" />
                <div id="lblMensajeErrorHora" runat="server"></div>
              </div>
               <div class="col-xs-3">
                <label class="control-label">Tienda</label>
                <uc1:wucCombo id="ddlLocal" runat="server" FuenteDatos="Tabla" TipoCombo="Local" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div class="col-xs-2">
                <label class="control-label">&nbsp;</label><br />
                <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary btn-sm" OnClientClick="return(Planificacion.validarFormularioBusquedaAsignarCamion())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
              </div>
            </div>
          </div>
        </div>        
      </div>

      <!-- RESULTADO BUSQUEDA -->
      <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>
      <div class="form-group">
        <asp:Literal ID="ltlTablaDatos" runat="server"></asp:Literal>
      </div>

      <!-- BOTONES -->
      <asp:Panel ID="pnlBotones" runat="server" CssClass="form-group text-right" Visible="false">
        <asp:LinkButton ID="btnNotificarAsignacion" runat="server" CssClass="btn btn-primary" OnClientClick="return(Planificacion.notificarAsignacionCamiones())"><span class="glyphicon glyphicon-envelope"></span>&nbsp;Notificar y cerrar asignaci&oacute;n</asp:LinkButton>
        <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-success" OnClientClick="return(Planificacion.grabarAsignarCamion())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
      </asp:Panel>

      <script type="text/javascript">
        jQuery(document).ready(function () {
          jQuery(".date-pick").datepicker({
            changeMonth: true,
            changeYear: true
          });

          jQuery(".chosen-select").select2();

          setTimeout(function () {
            jQuery("input[data-action=autocomplete]").autocomplete({
              source: '../webAjax/waSistema.aspx?op=AutocompletarConductorTransportista&idUsuarioTransportista=<%=Me.ViewState.Item("idUsuarioTransportista")%>',
              select: function (event, json) { Planificacion.cargarDatosConductor({ data: json, objInput: jQuery(this), invocadoDesde: Planificacion.FORMULARIO_ASIGNAR_CAMION }); },
              minLength: 1,
              create: function () {
                jQuery(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                  var autocomplete_template = "";
                  autocomplete_template += "<span class=\"sist-font-size-12\" style=\"font-family:Arial\">";
                  autocomplete_template += "<strong>{NOMBRE_CONDUCTOR}</strong><br />";
                  autocomplete_template += "<span class=\"small\"><em>Rut: {RUT_CONDUCTOR} / Celular: {TELEFONO}</em></span>";
                  autocomplete_template += "</span>";

                  //reemplaza marcas
                  autocomplete_template = autocomplete_template.replace("{NOMBRE_CONDUCTOR}", item.nombreConductor);
                  autocomplete_template = autocomplete_template.replace("{RUT_CONDUCTOR}", item.rutConductor);
                  autocomplete_template = autocomplete_template.replace("{TELEFONO}", item.telefono);

                  return jQuery("<li>")
                         .append("<a>" + autocomplete_template + "</a>")
                         .appendTo(ul);
                };
              }
            });
          }, 100);

        });
      </script>

    </asp:Panel>
  </div>

</asp:Content>

﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TO.AgregarContacto.aspx.vb" Inherits="webtransportewalmartmx.TO_AgregarContacto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Agregar Contacto</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">
          <div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-4">
                  <label class="control-label">Nombre</label>
                  <asp:TextBox ID="txtNombre" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Paterno</label>
                  <asp:TextBox ID="txtPaterno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Materno</label>
                  <asp:TextBox ID="txtMaterno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-4">
                  <label class="control-label">Tel&eacute;fono</label>
                  <asp:TextBox ID="txtTelefono" runat="server" MaxLength="9" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-xs-4">
                  <label class="control-label">Email</label>
                  <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
            </div>
          </div> 

        </asp:Panel>

        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Alerta.cerrarPopUp()"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Alerta.validarFormularioAgregarContacto())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Crear</asp:LinkButton>
        </div>
      </div>
    </div>

  </form>
</body>
</html>

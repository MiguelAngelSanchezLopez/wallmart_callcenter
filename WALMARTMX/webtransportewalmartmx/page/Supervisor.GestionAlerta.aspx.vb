﻿Imports CapaNegocio
Imports System.Data

Public Class Supervisor_GestionAlerta
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    PestanaAlertasPorCerrar
    PestanaAlertasSinAtender
    GrabarGestionAlerta
  End Enum
#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 24/07/2009</remarks>
  Private Sub ActualizaInterfaz()
    'introducir codigo
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 24/07/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim permisoPestanaAlertasPorCerrar, permisoPestanaAlertasSinAtender As String

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.tabAlertasPorCerrar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                                     Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.PestanaAlertasPorCerrar.ToString)
    Me.tabAlertasSinAtender.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                                      Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.PestanaAlertasSinAtender.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                           Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.GrabarGestionAlerta.ToString)

    Me.txtPermisoPestanaAlertasPorCerrar.Value = IIf(Me.tabAlertasPorCerrar.Visible, "1", "0")
    Me.txtPermisoPestanaAlertasSinAtender.Value = IIf(Me.tabAlertasSinAtender.Visible, "1", "0")
    Me.liPestanaAlertasPorCerrar.Visible = Me.tabAlertasPorCerrar.Visible
    Me.liPestanaAlertasSinAtender.Visible = Me.tabAlertasSinAtender.Visible

    permisoPestanaAlertasPorCerrar = Me.txtPermisoPestanaAlertasPorCerrar.Value
    permisoPestanaAlertasSinAtender = Me.txtPermisoPestanaAlertasSinAtender.Value

    If (permisoPestanaAlertasPorCerrar = "1") Then
      Me.liPestanaAlertasPorCerrar.Attributes.Add("class", "active")
      Me.tabAlertasPorCerrar.Attributes.Add("class", "tab-pane active")
    Else
      Me.liPestanaAlertasSinAtender.Attributes.Add("class", "active")
      Me.tabAlertasSinAtender.Attributes.Add("class", "tab-pane active")
    End If


    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
    End If

  End Sub


End Class
﻿Imports CapaNegocio
Imports System.Data

Public Class Planificacion_ReasignarCamion
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
  End Enum

  Private Enum eTabla As Integer
    T00_Detalle = 0
    T01_ListadoLocales = 1
  End Enum
#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim idPlanificacion As String = Me.ViewState.Item("idPlanificacion")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")

    'obtiene listado
    ds = PlanificacionTransportista.ObtenerDetalle(idPlanificacion, nroTransporte)

    'guarda el resultado en session
    Me.ViewState.Add("ds", ds)
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlanificacion As String = Me.ViewState.Item("idPlanificacion")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim ds As DataSet = Me.ViewState.Item("ds")
    Dim dt As DataTable
    Dim idUsuarioTransportista, patenteTracto, patenteTrailer, nombreConductor, rutConductor, celular, jsonLocales, idPlanificacionTransportista As String
    Dim carga, tipoCamion, fechaPresentacion, horaPresentacion, existeTrazaViaje As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder
    Dim oPlanificacionTransportista As PlanificacionTransportista

    'inicializa controles en vacio
    idPlanificacionTransportista = "-1"
    idUsuarioTransportista = "-1"
    carga = ""
    tipoCamion = ""
    patenteTracto = ""
    patenteTrailer = ""
    nombreConductor = ""
    rutConductor = ""
    celular = ""
    fechaPresentacion = ""
    horaPresentacion = ""
    existeTrazaViaje = "0"

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(eTabla.T00_Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(eTabla.T00_Detalle).Rows(0)
          idPlanificacionTransportista = dr.Item("IdPlanificacionTransportista")
          idUsuarioTransportista = dr.Item("IdUsuarioTransportista")
          carga = dr.Item("Carga")
          tipoCamion = dr.Item("TipoCamion")
          patenteTracto = dr.Item("PatenteTracto")
          patenteTrailer = dr.Item("PatenteTrailer")
          nombreConductor = dr.Item("NombreConductor")
          rutConductor = dr.Item("RutConductor")
          celular = dr.Item("Celular")
          fechaPresentacion = dr.Item("FechaPresentacion")
          horaPresentacion = dr.Item("HoraPresentacion")
          existeTrazaViaje = dr.Item("ExisteTrazaViaje")
        End If
      End If
    Catch ex As Exception
      idPlanificacionTransportista = "-1"
      idUsuarioTransportista = "-1"
      carga = ""
      tipoCamion = ""
      patenteTracto = ""
      patenteTrailer = ""
      nombreConductor = ""
      rutConductor = ""
      celular = ""
      fechaPresentacion = ""
      horaPresentacion = ""
      existeTrazaViaje = "0"
    End Try

    'asigna valores
    Me.lblTituloFormulario.Text = IIf(idPlanificacion = "-1", "Nueva Programaci&oacute;n", "Reasignar cami&oacute;n")
    Me.lblIdRegistro.Text = IIf(nroTransporte = "-1", "", "[IdMaster: " & nroTransporte & "]")
    Me.txtIdPlanificacion.Value = idPlanificacion
    Me.ddlTransportista.SelectedValue = idUsuarioTransportista
    Me.ddlTransportista_ValorActual.Value = idUsuarioTransportista
    Me.txtCarga.Text = carga
    Me.txtCarga_ValorActual.Value = carga
    Me.txtTipoCamion.Text = tipoCamion
    Me.txtTipoCamion_ValorActual.Value = tipoCamion
    Me.txtPatenteTracto.Text = patenteTracto
    Me.txtPatenteTracto_ValorActual.Value = patenteTracto
    Me.txtPatenteTrailer.Text = patenteTrailer
    Me.txtPatenteTrailer_ValorActual.Value = patenteTrailer
    Me.txtNombreConductor.Text = nombreConductor
    Me.txtNombreConductor_ValorActual.Value = nombreConductor
    Me.txtRutConductor.Text = rutConductor
    Me.txtRutConductor_ValorActual.Value = rutConductor
    Me.txtCelular.Text = celular
    Me.txtCelular_ValorActual.Value = celular
    Me.txtNroTransporte.Text = IIf(idPlanificacion = "-1", "", nroTransporte)
    Me.txtNroTransporte_ValorActual.Value = IIf(idPlanificacion = "-1", "", nroTransporte)
    Me.txtNroTransporte.Enabled = IIf(idPlanificacion = "-1", True, False)
    Me.txtFechaPresentacion.Text = fechaPresentacion
    Me.txtFechaPresentacion_ValorActual.Value = fechaPresentacion
    Me.txtFechaPresentacion.Enabled = IIf(idPlanificacion = "-1", True, False)
    Me.txtHoraPresentacion.Text = horaPresentacion
    Me.txtHoraPresentacion_ValorActual.Value = horaPresentacion
    Me.txtHoraPresentacion.Enabled = IIf(idPlanificacion = "-1", True, False)
    Me.btnModificar.Visible = IIf(existeTrazaViaje = "1", False, True)

    dt = ds.Tables(eTabla.T01_ListadoLocales)
    jsonLocales = MyJSON.ConvertDataTableToJSON(dt)
    Me.txtJSONLocalesSeleccionados.Value = jsonLocales
    Me.txtJSONLocalesSeleccionados_ValorActual.Value = jsonLocales

    'dibuja tabla con los locales asociados
    DibujaTablaLocalesAsociados(ds, eTabla.T01_ListadoLocales)

    'obtiene datos extras de la planificacion y lo deja en memoria para utilizarlo cuando se graben nuevos locales
    oPlanificacionTransportista = New PlanificacionTransportista(idPlanificacionTransportista)
    Me.ViewState.Add("oPlanificacionTransportistaCopia", oPlanificacionTransportista)

    sbScript.Append("Planificacion.asignarAutocompletarConductor()")
    Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "script_ActualizaInterfaz", False)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    Dim idPlanificacion As String = Me.ViewState.Item("idPlanificacion")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idPlanificacion = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idPlanificacion <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim json, idPlanificacionTransportista, idUsuarioTransportista, patenteTracto, patenteTrailer, nombreConductor, rutConductor, celular As String
    Dim campoModificado, observacion, idLocal, ordenEntrega, listado, carga, tipoCamion, nuevoNroTransporte, fechaPresentacion, horaPresentacion As String
    Dim objJSON, row As Object
    Dim oPlanificacionTransportista As New PlanificacionTransportista
    Dim statusGrabar As Integer
    Dim dv As String = ""
    Dim oPlanificacionTransportistaCopia As New PlanificacionTransportista
    Dim lsListado As New List(Of String)
    Dim idPlanificacion As String = Me.ViewState.Item("idPlanificacion")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim idPlanificacionNueva As Integer = -1

    Try
      oPlanificacionTransportistaCopia = Me.ViewState.Item("oPlanificacionTransportistaCopia")
      json = Me.txtJSONLocalesSeleccionados.Value
      objJSON = MyJSON.ConvertJSONToObject(json)

      idUsuarioTransportista = Utilidades.IsNull(Me.ddlTransportista.SelectedValue, "-1")
      carga = Utilidades.IsNull(Me.txtCarga.Text, "")
      tipoCamion = Utilidades.IsNull(Me.txtTipoCamion.Text, "")
      patenteTracto = Utilidades.IsNull(Me.txtPatenteTracto.Text, "")
      patenteTrailer = Utilidades.IsNull(Me.txtPatenteTrailer.Text, "")
      nombreConductor = Utilidades.IsNull(Me.txtNombreConductor.Text, "")
      rutConductor = Utilidades.IsNull(Me.txtRutConductor.Text, "")
      celular = Utilidades.IsNull(Me.txtCelular.Text, "")
      nuevoNroTransporte = Utilidades.IsNull(Me.txtNroTransporte.Text, "")
      fechaPresentacion = Utilidades.IsNull(Me.txtFechaPresentacion.Text, "")
      horaPresentacion = Utilidades.IsNull(Me.txtHoraPresentacion.Text, "")
      'VSR, PROFECIA NRO 20150918-1509: VAN A SOLICITAR AGREGAR MAS DE UNA OBSERVACION CON LA NUEVA MODIFICACION DEL COMBO (antes era texto libre)
      'FECHA CUMPLIMIENTO PROFECIA: Por definir
      observacion = Utilidades.IsNull(Me.ddlObservacion.SelectedValue, "")

      'formatea valores
      carga = carga.Trim()
      tipoCamion = tipoCamion.Trim()
      patenteTracto = patenteTracto.Trim()
      patenteTrailer = patenteTrailer.Trim()
      nombreConductor = nombreConductor.Trim()
      celular = celular.Trim()
      fechaPresentacion = fechaPresentacion & " " & horaPresentacion

      'Utilidades.SepararRut(rutConductor, dv)
      'If (Not String.IsNullOrEmpty(rutConductor)) Then rutConductor = rutConductor & "-" & dv

      '-------------------------------------------------------------
      'busca las planificaciones (o locales) que no esten referenciados y los elimina
      lsListado.Add("-1")
      For Each row In objJSON
        idPlanificacionTransportista = MyJSON.ItemObject(row, "IdPlanificacionTransportista")
        lsListado.Add(idPlanificacionTransportista)
      Next
      listado = String.Join(",", lsListado.ToArray())
      'elimina los registros que no estan referenciados en la lista
      status = PlanificacionTransportista.EliminarPlanificacionesNoReferenciadas(idPlanificacion, nroTransporte, listado)
      If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo eliminar las planficaciones que no están referenciadas")

      'si es una nueva programacion entonces se crea un nuevo registro en tabla Planificacion
      If (idPlanificacion = "-1") Then
        Dim oPlanificacion As New Planificacion
        oPlanificacion.IdUsuarioCreacion = oUsuario.Id
        oPlanificacion.FechaCreacion = Herramientas.MyNow()
        oPlanificacion.GuardarNuevo()
        idPlanificacionNueva = oPlanificacion.Id
      End If

      '-------------------------------------------------------------
      'graba o actualiza las planificaciones (locales)
      For Each row In objJSON
        idPlanificacionTransportista = MyJSON.ItemObject(row, "IdPlanificacionTransportista")
        idLocal = MyJSON.ItemObject(row, "IdLocal")
        ordenEntrega = MyJSON.ItemObject(row, "OrdenEntrega")

        If (idPlanificacionTransportista = "-1") Then
          oPlanificacionTransportista = oPlanificacionTransportistaCopia
        Else
          oPlanificacionTransportista = New PlanificacionTransportista(idPlanificacionTransportista)
        End If

        oPlanificacionTransportista.IdUsuarioTransportista = idUsuarioTransportista
        oPlanificacionTransportista.Carga = Sistema.Capitalize(carga, Sistema.eTipoCapitalizacion.TodoMayuscula)
        oPlanificacionTransportista.TipoCamion = Sistema.Capitalize(tipoCamion, Sistema.eTipoCapitalizacion.TodoMayuscula)
        oPlanificacionTransportista.PatenteTracto = Sistema.Capitalize(patenteTracto, Sistema.eTipoCapitalizacion.TodoMayuscula)
        oPlanificacionTransportista.PatenteTrailer = Sistema.Capitalize(patenteTrailer, Sistema.eTipoCapitalizacion.TodoMayuscula)
        oPlanificacionTransportista.NombreConductor = Sistema.Capitalize(nombreConductor, Sistema.eTipoCapitalizacion.NombrePropio)
        oPlanificacionTransportista.RutConductor = rutConductor
        oPlanificacionTransportista.Celular = celular
        oPlanificacionTransportista.OrdenEntrega = ordenEntrega
        oPlanificacionTransportista.NroTransporte = nuevoNroTransporte

        If (idPlanificacionTransportista = "-1") Then
          'si es una nueva programacion entonces se actualiza el idPlanificacion del objeto de la copia
          If (idPlanificacion = "-1") Then
            oPlanificacionTransportista.IdPlanificacion = idPlanificacionNueva
            oPlanificacionTransportista.FechaPresentacion = Convert.ToDateTime(fechaPresentacion)
            oPlanificacionTransportista.Observacion = "Ingresado manualmente el " & Replace(Herramientas.MyNow().ToString("dd/MM/yyyy H:mm"), "-", "/") & " hrs."
            oPlanificacionTransportista.CerradoPorTransportista = True
          End If
          oPlanificacionTransportista.IdLocal = idLocal
          oPlanificacionTransportista.GuardarNuevo()
        Else
          oPlanificacionTransportista.Actualizar()
        End If
      Next

      'obtiene listado de campos modificados
      campoModificado = ObtenerListadoCamposModificados(oPlanificacionTransportista)

      'si hay cambios entonces lo graba
      If (Not String.IsNullOrEmpty(campoModificado) And oPlanificacionTransportista.Existe) Then
        statusGrabar = PlanificacionTransportista.GrabarHistorialReasignacionCamion(oPlanificacionTransportista, oUsuario, campoModificado, observacion)
      End If

      If (idPlanificacion = "-1") Then
        Sistema.GrabarLogSesion(oUsuario.Id, "Crea nueva planificación manualmente: IdMaster " & nuevoNroTransporte)
        status = "ingresado"

        'envia email informativo al transportista con la planificacion ingresada
        Dim dsPlanificacion As DataSet = Planificacion.ObtenerCarga(idPlanificacionNueva)
        InformeMail.PlanificacionCamiones(dsPlanificacion, "CARGA_MANUAL")
      Else
        Sistema.GrabarLogSesion(oUsuario.Id, "Graba reasignación de camión")
        status = "modificado"
      End If
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la reasignaci&oacute;n del cami&oacute;n.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' obtiene listado de campos modificados
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerListadoCamposModificados(ByVal oPlanificacionTransportista As PlanificacionTransportista) As String
    Dim listado As String = ""
    Dim listCambios As New List(Of Dictionary(Of String, Object))
    Dim dicCampoModificado As New Dictionary(Of String, Object)
    Dim status As Integer
    Dim idUsuarioTransportistaValorActual, idUsuarioTransportistaModificado As String
    Dim oTransportistaActual, oTransportistaModificado As Usuario

    Try
      '---------------------------------------------------
      'construye el historial de cambio
      If (Me.ddlTransportista.SelectedValue <> Me.ddlTransportista_ValorActual.Value) Then
        idUsuarioTransportistaValorActual = Me.ddlTransportista_ValorActual.Value
        idUsuarioTransportistaModificado = oPlanificacionTransportista.IdUsuarioTransportista
        oTransportistaActual = New Usuario(idUsuarioTransportistaValorActual)
        oTransportistaModificado = New Usuario(idUsuarioTransportistaModificado)

        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Línea de Transporte")
        dicCampoModificado.Add("ValorActual", oTransportistaActual.NombreCompleto.Trim())
        dicCampoModificado.Add("ValorModificado", oTransportistaModificado.NombreCompleto.Trim())
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtCarga.Text.Trim().ToUpper() <> Me.txtCarga_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Carga")
        dicCampoModificado.Add("ValorActual", Me.txtCarga_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.Carga)
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtTipoCamion.Text.Trim().ToUpper() <> Me.txtTipoCamion_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Tipo Camión")
        dicCampoModificado.Add("ValorActual", Me.txtTipoCamion_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.TipoCamion)
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtPatenteTracto.Text.Trim().ToUpper() <> Me.txtPatenteTracto_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Placa Tracto")
        dicCampoModificado.Add("ValorActual", Me.txtPatenteTracto_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.PatenteTracto)
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtPatenteTrailer.Text.Trim().ToUpper() <> Me.txtPatenteTrailer_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Placa Remolque")
        dicCampoModificado.Add("ValorActual", Me.txtPatenteTrailer_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.PatenteTrailer)
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtNombreConductor.Text.Trim().ToUpper() <> Me.txtNombreConductor_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Nombre Operador")
        dicCampoModificado.Add("ValorActual", Me.txtNombreConductor_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.NombreConductor)
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtRutConductor.Text.Trim().ToUpper() <> Me.txtRutConductor_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Rut Operador")
        dicCampoModificado.Add("ValorActual", Me.txtRutConductor_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.RutConductor)
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtCelular.Text.Trim().ToUpper() <> Me.txtCelular_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Celular")
        dicCampoModificado.Add("ValorActual", Me.txtCelular_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.Celular)
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtFechaPresentacion.Text.Trim().ToUpper() <> Me.txtFechaPresentacion_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Fecha Presentación")
        dicCampoModificado.Add("ValorActual", Me.txtFechaPresentacion_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", Replace(oPlanificacionTransportista.FechaPresentacion.ToString("dd/MM/yyyy"), "-", "/"))
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtHoraPresentacion.Text.Trim().ToUpper() <> Me.txtHoraPresentacion_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Hora Presentación")
        dicCampoModificado.Add("ValorActual", Me.txtHoraPresentacion_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.FechaPresentacion.ToString("H:mm"))
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtNroTransporte.Text.Trim().ToUpper() <> Me.txtNroTransporte_ValorActual.Value.Trim().ToUpper()) Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "IdMaster")
        dicCampoModificado.Add("ValorActual", Me.txtNroTransporte_ValorActual.Value)
        dicCampoModificado.Add("ValorModificado", oPlanificacionTransportista.NroTransporte)
        listCambios.Add(dicCampoModificado)
      End If

      If (Me.txtHayCambioLocales.Value = "1") Then
        dicCampoModificado = New Dictionary(Of String, Object)
        dicCampoModificado.Add("CampoModificado", "Tiendas")
        dicCampoModificado.Add("ValorActual", MyJSON.ConvertJSONToObject(Me.txtJSONLocalesSeleccionados_ValorActual.Value))
        dicCampoModificado.Add("ValorModificado", MyJSON.ConvertJSONToObject(Me.txtJSONLocalesSeleccionados.Value))
        listCambios.Add(dicCampoModificado)

        'asocia el historial de reasignacion al nuevo NroTransporte
        status = PlanificacionTransportista.AsociarHistorialAlNuevoNroTransporte(Me.txtNroTransporte_ValorActual.Value, oPlanificacionTransportista.NroTransporte)
      End If

      If (listCambios.Count > 0) Then
        listado = MyJSON.ConvertObjectToJSON(listCambios)
      End If
    Catch ex As Exception
      listado = ""
    End Try

    Return listado
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlanificacion As String = Me.ViewState.Item("idPlanificacion")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim llaveRecargarPagina As String = Me.ViewState.Item("llaveRecargarPagina")
    Dim nombrePagina As String = Me.ViewState.Item("nombrePagina")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""
    Dim opcionesJSON As String = ""

    'grabar valores
    textoMensaje &= GrabarDatosRegistro(status)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La reasignaci&oacute;n del cami&oacute;n fue ingresado satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La reasignaci&oacute;n del cami&oacute;n fue actualizada satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo reasignar el cami&oacute;n. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo reasignar el cami&oacute;n. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      If (status = "ingresado") Then
        Dim dcFiltros As New Dictionary(Of String, String)
        dcFiltros.Add("nroTransporte", Me.txtNroTransporte.Text)
        dcFiltros.Add("fechaPresentacion", Me.txtFechaPresentacion.Text)
        Session(nombrePagina) = dcFiltros
      End If
      Session(llaveRecargarPagina) = "1"
      Utilidades.RegistrarScript(Me.Page, "Planificacion.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina &= "idPlanificacion=" & idPlanificacion
      queryStringPagina &= "nroTransporte=" & nroTransporte
      queryStringPagina &= "&llaveRecargarPagina=" & llaveRecargarPagina
      Response.Redirect("./Planificacion.ReasignarCamion.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' dibuja tabla con los locales asociados
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub DibujaTablaLocalesAsociados(ByVal ds As DataSet, indice As eTabla)
    Dim oUtilidades As New Utilidades
    Dim template As String = ""
    Dim html As String = ""
    Dim dt As DataTable
    Dim totalRegistros As Integer
    Dim htmlSinRegistro As String = "<em class=""help-block"">No hay tiendas asociadas</em>"
    Dim nombreTabla As String = "tblLocalesAsociados"
    Dim sb As New StringBuilder
    Dim nombreLocal, idLocal, ordenEntrega, templateAux, templateOpciones, templateOpcionesAux As String

    Try
      template &= "<tr id=""{PREFIJO_CONTROL}_tr"" {CLASS_TR}>"
      template &= "  <td>"
      template &= "    <input type=""hidden"" id=""{PREFIJO_CONTROL}_txtIdLocal"" value=""{ID_LOCAL}"" data-targetLocal=""id-local"" data-prefijoControl=""{PREFIJO_CONTROL}"" />"
      template &= "    {NOMBRE_LOCAL}"
      template &= "  </td>"
      template &= "  <td>"
      template &= "    <input type=""text"" id=""{PREFIJO_CONTROL}_txtOrdenEntrega"" value=""{ORDEN_ENTREGA}"" class=""form-control input-sm"" />"
      template &= "    <span id=""{PREFIJO_CONTROL}_hErrorOrdenEntrega""></span>"
      template &= "  </td>"
      template &= "  <td class=""text-center"">{OPCIONES}</td>"
      template &= "</tr>"

      templateOpciones = "<button type=""button"" class=""btn btn-default"" onclick=""Planificacion.eliminarLocalAsociado({ idLocal:'{ID_LOCAL}' })""><span class=""glyphicon glyphicon-trash""></span></button>&nbsp;"

      dt = ds.Tables(indice)
      totalRegistros = dt.Rows.Count

      sb.Append("<table id=""" & nombreTabla & """ class=""table table-condensed"">")
      sb.Append("  <thead>")
      sb.Append("    <tr>")
      sb.Append("      <th>Tienda</th>")
      sb.Append("      <th style=""width:150px"">Secuencia</th>")
      sb.Append("      <th style=""width:120px"" class=""text-center"">Opciones</th>")
      sb.Append("    </tr>")
      sb.Append("  </thead>")
      sb.Append("  <tbody>")

      'agrega fila para despues clonarla cuando se quiera agregar un nuevo perfil
      templateAux = template
      templateOpcionesAux = templateOpciones

      templateAux = templateAux.Replace("{CLASS_TR}", " class=""sist-display-none""")
      templateAux = templateAux.Replace("{OPCIONES}", templateOpcionesAux)
      sb.Append(templateAux)

      'dibuja los perfiles asociados
      If (totalRegistros > 0) Then
        For Each dr As DataRow In dt.Rows
          templateAux = template
          templateOpcionesAux = templateOpciones
          idLocal = dr.Item("IdLocal")
          nombreLocal = dr.Item("NombreLocal")
          ordenEntrega = dr.Item("OrdenEntrega")

          'formate valores
          ordenEntrega = IIf(ordenEntrega = "-1", "", ordenEntrega)

          templateOpcionesAux = templateOpcionesAux.Replace("{ID_LOCAL}", idLocal)
          templateAux = templateAux.Replace("{CLASS_TR}", "")
          templateAux = templateAux.Replace("{NOMBRE_LOCAL}", Server.HtmlEncode(nombreLocal))
          templateAux = templateAux.Replace("{PREFIJO_CONTROL}", idLocal)
          templateAux = templateAux.Replace("{ID_LOCAL}", idLocal)
          templateAux = templateAux.Replace("{ORDEN_ENTREGA}", ordenEntrega)
          templateAux = templateAux.Replace("{OPCIONES}", templateOpcionesAux)
          sb.Append(templateAux)
        Next
      End If

      sb.Append("  </tbody>")
      sb.Append("</table>")
      html = sb.ToString()
    Catch ex As Exception
      html = "<div class=""alert alert-danger text-center""><strong>" & ex.Message & "</strong></div>"
    End Try

    Me.ltlTablaLocales.Text = html
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlanificacion As String = Utilidades.IsNull(Request("idPlanificacion"), "-1")
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")
    Dim llaveRecargarPagina As String = Utilidades.IsNull(Request("llaveRecargarPagina"), "")
    Dim nombrePagina As String = Utilidades.IsNull(Request("nombrePagina"), "")

    Me.ViewState.Add("idPlanificacion", idPlanificacion)
    Me.ViewState.Add("nroTransporte", nroTransporte)
    Me.ViewState.Add("llaveRecargarPagina", llaveRecargarPagina)
    Me.ViewState.Add("nombrePagina", nombrePagina)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

End Class
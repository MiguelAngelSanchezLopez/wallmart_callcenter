﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionOperacion.AsignacionConductorManual.aspx.vb" Inherits="webtransportewalmartmx.GestionOperacion_AsignacionConductorManual" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Asignaci&oacute;n Operador manual</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../js/jquery.dataTables.bootstrap.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/gestionOperacion.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Asignar Operador Manual"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">
          <div class="panel panel-primary">
            <div class="panel-heading sist-padding-cero sist-padding-5"><h3 class="panel-title sist-font-size-14">FILTROS DE BUSQUEDA</h3></div>
            <div class="panel-body sist-padding-cero sist-padding-5 sist-padding-left-10">
              <div class="row">
                <div class="col-xs-4">
                  <label class="control-label">Nombre Operador</label>
                  <asp:TextBox ID="txtNombre" runat="server" MaxLength="255" CssClass="form-control input-sm"></asp:TextBox>
                </div>
                <div class="col-xs-3">
                  <label class="control-label">Rut Operador</label>
                  <asp:TextBox ID="txtRut" runat="server" MaxLength="20" CssClass="form-control input-sm"></asp:TextBox>
                  <div class="help-block sist-margin-cero">(sin puntos ej:12345678-K)</div>
                </div>
                <div class="col-xs-2">
                  <label class="control-label">Placa</label>
                  <asp:TextBox ID="txtPatente" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                </div>
                <div class="col-xs-1">
                  <label class="control-label">&nbsp;</label><br />
                  <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary btn-sm" OnClientClick="return(GestionOperacion.validarFormularioBusquedaAsignacionManual())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
                </div>
              </div>
            </div>
          </div>

          <div class="small">
            <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
            <input type="hidden" id="txtJSONDatosConductor" runat="server" value="[]" size="150" />
            <asp:Literal ID="ltlTablaDatos" runat="server"></asp:Literal>
          </div>
          <div id="hMensajeErrorTablaDatos"></div>
          <br />
        </asp:Panel>

        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="GestionOperacion.cerrarPopUp('0')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <div id="btnGrabar" runat="server" class="btn btn-lg btn-primary" onclick="GestionOperacion.validarFormularioAsignacionConductorManual()"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".show-tooltip").tooltip();
      });
    </script>

  </form>
</body>
</html>

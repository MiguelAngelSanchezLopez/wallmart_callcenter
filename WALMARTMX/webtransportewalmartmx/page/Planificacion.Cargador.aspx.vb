﻿Imports CapaNegocio
Imports System.Data

Public Class Planificacion_Cargador
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Const NOMBRE_CARPETA_CARGA_PLANIFICACION As String = "CargaPlanificacion"
  Private Const SESION_LISTADO_ERRORES As String = "SESION_LISTADO_ERRORES"
  Private Const SESION_ID_PLANIFICACION As String = "SESION_ID_PLANIFICACION"
  Private COLUMNA_00_RUT_TRANSPORTISTA As String = "RUT TRANSPORTISTA"
  Private COLUMNA_01_RAZON_SOCIAL As String = "RAZON SOCIAL"
  Private COLUMNA_02_FECHA_PRESENTACION As String = "FECHA PRESENTACION"
  Private COLUMNA_03_HORA_PRESENTACION As String = "HORA PRESENTACION"
  Private COLUMNA_04_CARGA As String = "CARGA"
  Private COLUMNA_05_NRO_TRANSPORTE As String = "NUMERO TRANSPORTE"
  Private COLUMNA_06_SECUENCIA As String = "SECUENCIA"
  Private COLUMNA_07_LOCAL As String = "LOCAL"
  Private COLUMNA_08_DESCRIPCION_LOCAL As String = "DESCRIPCION LOCAL"
  Private COLUMNA_09_TIPO_CAMION As String = "TIPO  CAMION"
  Private COLUMNA_10_TOTAL As String = "TOTAL"

  Dim DATASET_TABLAS_BASICAS As New DataSet
  Dim DATATABLE_CARGA_CSV As New DataTable

  Private Enum eFunciones
    Ver
  End Enum

  Private Enum eTabla As Integer
    T00_ListadoPlanificacion = 0
    T01_Locales = 1
  End Enum

#End Region

#Region "Metodo Controles ASP"
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    If Not IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      InicializaControles(Page.IsPostBack)
    End If

    ActualizaInterfaz()
    MostrarMensajeUsuario()
  End Sub

  Protected Sub btnValidar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    Sistema.GrabarLogSesion(oUsuario.Id, "Valida carga de planificación")
    ValidarArchivo()
    Response.Redirect("./Planificacion.Cargador.aspx")
  End Sub

#End Region

#Region "Metodos Privados"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim listadoErrores As New List(Of String)
    Dim dibujaErrores As Boolean = False
    Dim idPlanificacion As String
    Dim ds As New DataSet

    'revisa si hay errores para mostrarlos
    listadoErrores = CType(Session(SESION_LISTADO_ERRORES), List(Of String))
    If (Not listadoErrores Is Nothing) Then
      If (listadoErrores.Count > 0) Then
        dibujaErrores = True
        MostrarListadoErrores()
      End If
    End If

    'si no tiene errores, dibuja la carga realizada
    If (Not dibujaErrores) Then
      idPlanificacion = CType(Session(SESION_ID_PLANIFICACION), String)
      If (Not idPlanificacion Is Nothing) Then
        If (idPlanificacion > "0") Then
          ds = Planificacion.ObtenerCarga(idPlanificacion)
          MostrarCargaRealizada(idPlanificacion, ds)
          InformeMail.PlanificacionCamiones(ds, "CARGA_AUTOMATICA")
        End If
      End If
    End If

  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    '-- inicializacion de controles que dependen del postBack --
    If Not isPostBack Then
      'colocar controles
    End If

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String
    Dim ocultarAutomaticamente As Boolean = True

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensaje.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
          ocultarAutomaticamente = False
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensaje.Visible = False
    End Try

    If (ocultarAutomaticamente) Then
      Utilidades.RegistrarScript(Me.Page, "setTimeout(function(){document.getElementById(""" & Me.pnlMensaje.ClientID & """).style.display = ""none"";},5000);", Utilidades.eRegistrar.FINAL, "mostrarMensaje")
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' valida el archivo y si esta bien entonces graba los datos
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub ValidarArchivo()
    Dim msgError As String = ""
    Dim esValido As Boolean = True
    Dim nombreArchivo As String = ""
    Dim extension As String = ""
    Dim pathFile As String = ""
    Dim pathFolder As String = ""
    Dim oUtilidades As New Utilidades()
    Dim ListaIds As String = ""

    Try
      nombreArchivo = Archivo.ObtenerNombreArchivoDesdeRuta(Me.fileCarga.FileName)
      nombreArchivo = Archivo.CrearNombreUnicoArchivo() & "_" & nombreArchivo
      pathFolder = Utilidades.RutaLogsSitio & "\" & NOMBRE_CARPETA_CARGA_PLANIFICACION

      'crea carpeta si es que no existe
      Archivo.CrearDirectorioEnDisco(pathFolder)

      'ruta archivo
      pathFile = pathFolder & "\" & nombreArchivo

      'envie al servidor
      Me.fileCarga.PostedFile.SaveAs(pathFile)

      'verifica que el archivo sea valido
      esValido = ValidarFormatoArchivo(pathFile)

      'graba el resultado de la validacion
      If Not esValido Then
        msgError = "{ERROR}El archivo tiene errores, favor revisar y volver a cargar"
      Else
        msgError = GrabarDatosCarga()
      End If
      Session(Utilidades.KEY_SESION_MENSAJE) = msgError
    Catch ex As Exception
      Session(Utilidades.KEY_SESION_MENSAJE) = ex.Message
    End Try
  End Sub

  ''' <summary>
  ''' valida el formato para el tipo de encuesta Evaluacion de desempeño
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ValidarFormatoArchivo(ByVal pathFile As String) As Boolean
    Dim retorno As Boolean
    Dim extension As String
    Dim src As New CsvDataSource.CsvDataSource
    Dim NUMERO_COLUMNAS As Integer = 11
    Dim totalRegistros, i As Integer
    Dim nombreColumna, rutTransportista, nombreTransportista, fechaPresentacion, horaPresentacion, carga, nroTransporte As String
    Dim ordenEntrega, codigoLocal, nombreLocal, tipoCamion As String
    Dim columnasQueFaltan As String = ""
    Dim validacionDatosFila As String = ""
    Dim dr As DataRow
    Dim listadoErrores As New List(Of String)

    Try
      DATASET_TABLAS_BASICAS = Planificacion.ObtenerTablasBasicasSistema()

      '-----------------------------------------------
      'valida la extension
      extension = Archivo.ObtenerExtension(pathFile)
      If (extension.ToLower() <> "csv") Then
        listadoErrores.Add("El tipo de archivo no es v&aacute;lido, debe ser un archivo *.CSV")
      Else
        '-----------------------------------------------
        'valida si existe el archivo en disco
        If Not Archivo.ExisteArchivoEnDisco(pathFile) Then
          listadoErrores.Add("El tipo de archivo no es v&aacute;lido, debe ser un archivo *.CSV")
        Else
          DATATABLE_CARGA_CSV = src.ReadFile(pathFile, True, ";")

          '---------------------------------------
          'valida total de registros
          totalRegistros = DATATABLE_CARGA_CSV.Rows.Count
          If totalRegistros = 0 Then listadoErrores.Add("El archivo no contiene registros")

          '---------------------------------------
          'valida el numero de columnas
          If DATATABLE_CARGA_CSV.Columns.Count <> NUMERO_COLUMNAS Then listadoErrores.Add("El n&uacute;mero de columnas no es válido, deben ser " & NUMERO_COLUMNAS)

          '---------------------------------------
          'valida nombre de columnas
          If DATATABLE_CARGA_CSV.Columns.Count = NUMERO_COLUMNAS Then
            For i = 0 To NUMERO_COLUMNAS - 1
              nombreColumna = DATATABLE_CARGA_CSV.Columns(i).ColumnName.ToUpper()
              Select Case i
                Case 0 : If nombreColumna <> COLUMNA_00_RUT_TRANSPORTISTA Then columnasQueFaltan &= "- " & COLUMNA_00_RUT_TRANSPORTISTA & "<br />"
                Case 1 : If nombreColumna <> COLUMNA_01_RAZON_SOCIAL Then columnasQueFaltan &= "- " & COLUMNA_01_RAZON_SOCIAL & "<br />"
                Case 2 : If nombreColumna <> COLUMNA_02_FECHA_PRESENTACION Then columnasQueFaltan &= "- " & COLUMNA_02_FECHA_PRESENTACION & "<br />"
                Case 3 : If nombreColumna <> COLUMNA_03_HORA_PRESENTACION Then columnasQueFaltan &= "- " & COLUMNA_03_HORA_PRESENTACION & "<br />"
                Case 4 : If nombreColumna <> COLUMNA_04_CARGA Then columnasQueFaltan &= "- " & COLUMNA_04_CARGA & "<br />"
                Case 5 : If nombreColumna <> COLUMNA_05_NRO_TRANSPORTE Then columnasQueFaltan &= "- " & COLUMNA_05_NRO_TRANSPORTE & "<br />"
                Case 6 : If nombreColumna <> COLUMNA_06_SECUENCIA Then columnasQueFaltan &= "- " & COLUMNA_06_SECUENCIA & "<br />"
                Case 7 : If nombreColumna <> COLUMNA_07_LOCAL Then columnasQueFaltan &= "- " & COLUMNA_07_LOCAL & "<br />"
                Case 8 : If nombreColumna <> COLUMNA_08_DESCRIPCION_LOCAL Then columnasQueFaltan &= "- " & COLUMNA_08_DESCRIPCION_LOCAL & "<br />"
                Case 9 : If nombreColumna <> COLUMNA_09_TIPO_CAMION Then columnasQueFaltan &= "- " & COLUMNA_09_TIPO_CAMION & "<br />"
                Case 10 : If nombreColumna <> COLUMNA_10_TOTAL Then columnasQueFaltan &= "- " & COLUMNA_10_TOTAL & "<br />"
              End Select
            Next
            If Not String.IsNullOrEmpty(columnasQueFaltan) Then listadoErrores.Add("Las siguientes columnas faltan:<br />" & columnasQueFaltan)

            '---------------------------------------
            'valida los registros ingresado
            If totalRegistros > 0 And String.IsNullOrEmpty(columnasQueFaltan) Then
              For i = 0 To totalRegistros - 1
                dr = DATATABLE_CARGA_CSV.Rows(i)
                rutTransportista = dr.Item(COLUMNA_00_RUT_TRANSPORTISTA)
                nombreTransportista = dr.Item(COLUMNA_01_RAZON_SOCIAL)
                fechaPresentacion = dr.Item(COLUMNA_02_FECHA_PRESENTACION)
                horaPresentacion = dr.Item(COLUMNA_03_HORA_PRESENTACION)
                carga = dr.Item(COLUMNA_04_CARGA)
                nroTransporte = dr.Item(COLUMNA_05_NRO_TRANSPORTE)
                ordenEntrega = dr.Item(COLUMNA_06_SECUENCIA)
                codigoLocal = dr.Item(COLUMNA_07_LOCAL)
                nombreLocal = dr.Item(COLUMNA_08_DESCRIPCION_LOCAL)
                tipoCamion = dr.Item(COLUMNA_09_TIPO_CAMION)
                validacionDatosFila = ""

                'formatea valores
                rutTransportista = rutTransportista.Replace(" ", "").Replace(".", "").Replace("-", "")
                If (rutTransportista.StartsWith("0")) Then rutTransportista = rutTransportista.Substring(1, rutTransportista.Length - 1)
                fechaPresentacion = fechaPresentacion.Replace("-", "")
                nroTransporte = CInt(nroTransporte)
                ordenEntrega = CInt(ordenEntrega)
                codigoLocal = CInt(codigoLocal)

                '-------------------------------------------------
                'COLUMNA_00_RUT_TRANSPORTISTA obligatorio
                If String.IsNullOrEmpty(rutTransportista.Trim()) Then
                  validacionDatosFila &= COLUMNA_00_RUT_TRANSPORTISTA & " es requerido / "
                  'Else
                  '  If Not Utilidades.EsRut(rutTransportista) Then
                  '    validacionDatosFila &= COLUMNA_00_RUT_TRANSPORTISTA & " no es v&aacute;lido / "
                  '  Else
                  '    If Not ExisteRegistroEnTablaBasica(DATASET_TABLAS_BASICAS, Planificacion.eTablasBasicas.T00_Transportistas, "RutCompleto", rutTransportista) Then validacionDatosFila &= COLUMNA_00_RUT_TRANSPORTISTA & " el valor no existe en sistema / "
                  '  End If
                End If

                '-------------------------------------------------
                'COLUMNA_01_RAZON_SOCIAL obligatorio
                If String.IsNullOrEmpty(nombreTransportista.Trim()) Then validacionDatosFila &= COLUMNA_01_RAZON_SOCIAL & " es requerido / "

                '-------------------------------------------------
                'COLUMNA_02_FECHA_PRESENTACION obligatorio
                If String.IsNullOrEmpty(fechaPresentacion.Trim()) Then
                  validacionDatosFila &= COLUMNA_02_FECHA_PRESENTACION & " es requerido / "
                Else
                  Dim arrFecha As String() = fechaPresentacion.Split(".")
                  If (arrFecha.Length = 3) Then
                    fechaPresentacion = arrFecha(2) & arrFecha(1) & arrFecha(0)
                  End If

                  If Not Utilidades.EsFechaISO(fechaPresentacion) Then
                    validacionDatosFila &= COLUMNA_02_FECHA_PRESENTACION & " no es v&aacute;lida / "
                  End If
                End If

                '-------------------------------------------------
                'COLUMNA_03_HORA_PRESENTACION obligatorio
                If String.IsNullOrEmpty(horaPresentacion.Trim()) Then
                  validacionDatosFila &= COLUMNA_03_HORA_PRESENTACION & " es requerido / "
                Else
                  horaPresentacion = "00" & horaPresentacion
                  horaPresentacion = Left(Right(horaPresentacion, 8), 5)

                  If Not Utilidades.EsHora(horaPresentacion) Then
                    validacionDatosFila &= COLUMNA_03_HORA_PRESENTACION & " no es v&aacute;lida / "
                  End If
                End If

                '-------------------------------------------------
                'COLUMNA_04_CARGA obligatorio
                If String.IsNullOrEmpty(carga.Trim()) Then validacionDatosFila &= COLUMNA_04_CARGA & " es requerido / "

                '-------------------------------------------------
                'COLUMNA_05_NRO_TRANSPORTE obligatorio
                If String.IsNullOrEmpty(nroTransporte.Trim()) Then
                  validacionDatosFila &= COLUMNA_05_NRO_TRANSPORTE & " es requerido / "
                Else
                  If Not Utilidades.EsNumerico(nroTransporte) Then
                    validacionDatosFila &= COLUMNA_05_NRO_TRANSPORTE & " no es v&aacute;lida / "
                  Else
                    If ExisteRegistroEnTablaBasica(DATASET_TABLAS_BASICAS, Planificacion.eTablasBasicas.T02_PlanificacionesCargadas, "NroTransporte", nroTransporte) Then validacionDatosFila &= COLUMNA_05_NRO_TRANSPORTE & " ya fue asignado anteriormente / "
                  End If
                End If

                '-------------------------------------------------
                'COLUMNA_06_SECUENCIA obligatorio
                If String.IsNullOrEmpty(ordenEntrega.Trim()) Then
                  validacionDatosFila &= COLUMNA_06_SECUENCIA & " es requerido / "
                Else
                  If Not Utilidades.EsNumerico(ordenEntrega) Then
                    validacionDatosFila &= COLUMNA_06_SECUENCIA & " no es v&aacute;lida / "
                  End If
                End If

                '-------------------------------------------------
                'COLUMNA_07_LOCAL obligatorio
                If String.IsNullOrEmpty(codigoLocal.Trim()) Then
                  validacionDatosFila &= COLUMNA_07_LOCAL & " es requerido / "
                  'Else
                  '  If Not Utilidades.EsNumerico(codigoLocal) Then
                  '    validacionDatosFila &= COLUMNA_07_LOCAL & " no es v&aacute;lido / "
                  '  Else
                  '    If Not ExisteRegistroEnTablaBasica(DATASET_TABLAS_BASICAS, Planificacion.eTablasBasicas.T01_Locales, "CodigoInterno", codigoLocal) Then validacionDatosFila &= COLUMNA_07_LOCAL & " el valor no existe en sistema / "
                  '  End If
                End If

                '-------------------------------------------------
                'COLUMNA_08_DESCRIPCION_LOCAL obligatorio
                If String.IsNullOrEmpty(nombreLocal.Trim()) Then validacionDatosFila &= COLUMNA_08_DESCRIPCION_LOCAL & " es requerido / "

                '-------------------------------------------------
                'COLUMNA_09_TIPO_CAMION obligatorio
                If String.IsNullOrEmpty(tipoCamion.Trim()) Then validacionDatosFila &= COLUMNA_09_TIPO_CAMION & " es requerido / "

                '---------------------------------------------
                'devuelve los errores encontrados
                If Not String.IsNullOrEmpty(validacionDatosFila) Then listadoErrores.Add("<strong>Fila " & (i + 2) & "</strong>: " & Utilidades.EliminaUltimoCaracterTexto(validacionDatosFila, 2))
              Next

            End If '<-- If totalRegistros > 0 And String.IsNullOrEmpty(columnasQueFaltan)
          End If '<-- If DATATABLE_CARGA_CSV.Columns.Count = NUMERO_COLUMNAS
        End If '<-- If Not Archivo.ExisteArchivoEnDisco(pathFile)
      End If '<-- If (extension.ToLower() <> "csv")

      If listadoErrores.Count = 0 Then
        retorno = True
      Else
        retorno = False
      End If
    Catch ex As Exception
      listadoErrores.Add(ex.Message)
      retorno = False
    End Try

    'graba el listado de errores en sesion
    Session(SESION_LISTADO_ERRORES) = listadoErrores
    Return retorno
  End Function

  ''' <summary>
  ''' verifica si el valor existe en los datos extraidos de la base
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ExisteRegistroEnTablaBasica(ByVal ds As DataSet, ByVal indiceTabla As Planificacion.eTablasBasicas, ByVal nombreColumna As String, ByVal valorComparar As String) As Boolean
    Dim existe As Boolean

    Try
      'se iniciliza el valor como que no existe el registro
      existe = False

      'formatea valores
      valorComparar = IIf(String.IsNullOrEmpty(valorComparar), "-1", valorComparar)
      Dim table As DataTable = ds.Tables(indiceTabla)
      Dim query = From row As DataRow In table.Rows Where row.Item(nombreColumna) = valorComparar

      For Each item In query
        existe = True
        Exit For
      Next
    Catch ex As Exception
      existe = False
    End Try
    Return existe
  End Function

  ''' <summary>
  ''' graba los datos de la carga
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosCarga() As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As New Usuario
    Dim msgError As String = ""
    Dim oPlanificacion As New Planificacion
    Dim oPlanificacionTransportista As New PlanificacionTransportista
    Dim rutTransportista, nombreTransportista, fechaPresentacion, horaPresentacion, carga, nroTransporte As String
    Dim ordenEntrega, codigoLocal, nombreLocal, tipoCamion, idLocal, idUsuarioTransportista As String
    Dim sb As New StringBuilder
    Dim indice As Integer = 0
    Dim listadoErrores As New List(Of String)
    'Dim fechaNula As New DateTime(1900, 1, 1)

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()

      'graba la planificacion
      oPlanificacion.IdUsuarioCreacion = oUsuario.Id
      oPlanificacion.FechaCreacion = Herramientas.MyNow()
      oPlanificacion.GuardarNuevo()

      'graba la planificacion del transportista
      For Each dr As DataRow In DATATABLE_CARGA_CSV.Rows
        oPlanificacionTransportista = New PlanificacionTransportista()

        rutTransportista = dr.Item(COLUMNA_00_RUT_TRANSPORTISTA)
        nombreTransportista = dr.Item(COLUMNA_01_RAZON_SOCIAL)
        fechaPresentacion = dr.Item(COLUMNA_02_FECHA_PRESENTACION)
        horaPresentacion = dr.Item(COLUMNA_03_HORA_PRESENTACION)
        carga = dr.Item(COLUMNA_04_CARGA)
        nroTransporte = dr.Item(COLUMNA_05_NRO_TRANSPORTE)
        ordenEntrega = dr.Item(COLUMNA_06_SECUENCIA)
        codigoLocal = dr.Item(COLUMNA_07_LOCAL)
        nombreLocal = dr.Item(COLUMNA_08_DESCRIPCION_LOCAL)
        tipoCamion = dr.Item(COLUMNA_09_TIPO_CAMION)

        'formatea valores
        rutTransportista = rutTransportista.Replace(" ", "").Replace(".", "").Replace("-", "")
        If (rutTransportista.StartsWith("0")) Then rutTransportista = rutTransportista.Substring(1, rutTransportista.Length - 1)
        fechaPresentacion = fechaPresentacion.Replace("-", "")
        nroTransporte = CInt(nroTransporte)
        ordenEntrega = CInt(ordenEntrega)
        codigoLocal = CInt(codigoLocal)

        'obtiene valores de las tablas basica
        idLocal = ObtenerRegistroDeTablaBasica(DATASET_TABLAS_BASICAS, Planificacion.eTablasBasicas.T01_Locales, "CodigoInterno", "IdLocal", codigoLocal)
        idUsuarioTransportista = ObtenerRegistroDeTablaBasica(DATASET_TABLAS_BASICAS, Planificacion.eTablasBasicas.T00_Transportistas, "RutCompleto", "IdUsuario", rutTransportista)

        'graba el registro
        oPlanificacionTransportista.Carga = carga
        oPlanificacionTransportista.FechaPresentacion = Convert.ToDateTime(fechaPresentacion & " " & horaPresentacion)
        oPlanificacionTransportista.IdLocal = idLocal
        oPlanificacionTransportista.IdPlanificacion = oPlanificacion.Id
        oPlanificacionTransportista.IdUsuarioTransportista = idUsuarioTransportista
        oPlanificacionTransportista.NroTransporte = nroTransporte
        oPlanificacionTransportista.OrdenEntrega = ordenEntrega
        oPlanificacionTransportista.TipoCamion = tipoCamion
        oPlanificacionTransportista.PatenteTracto = ""
        oPlanificacionTransportista.PatenteTrailer = ""
        oPlanificacionTransportista.NombreConductor = ""
        oPlanificacionTransportista.RutConductor = ""
        oPlanificacionTransportista.Celular = ""
        oPlanificacionTransportista.CerradoPorTransportista = False
        oPlanificacionTransportista.Anden = ""
        oPlanificacionTransportista.ConfirmacionAnden = False
        oPlanificacionTransportista.Finalizada = False
        oPlanificacionTransportista.BloqueadoPorFueraHorario = False

        Try
          oPlanificacionTransportista.GuardarNuevo()
        Catch ex As Exception
          listadoErrores.Add("<strong>Fila " & (indice + 2) & "</strong>: No se pudo grabar los datos debido a - " & ex.Message)
        End Try

        indice += 1
      Next

      If listadoErrores.Count > 0 Then
        msgError = "{ERROR}Se produjo un error y algunos registros no se pudieron grabar correctamente"
      Else
        msgError = "La carga fue grabada correctamente"
        Session(SESION_ID_PLANIFICACION) = oPlanificacion.Id
        Sistema.GrabarLogSesion(oUsuario.Id, "Graba carga planificación: IdPlanificacion " & oPlanificacion.Id)
      End If

      'graba el listado de errores en sesion
      Session(SESION_LISTADO_ERRORES) = listadoErrores
      Return msgError
    Catch ex As Exception
      Throw New Exception("{ERROR}" & ex.Message)
    End Try

  End Function

  ''' <summary>
  ''' obtiene valor de un campo especificado en una de las tablas basicas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function ObtenerRegistroDeTablaBasica(ByVal ds As DataSet, ByVal indiceTabla As Planificacion.eTablasBasicas, ByVal nombreColumnaFiltro As String, _
                                                      ByVal nombreColumnaRetornoValor As String, ByVal valorBuscar As String) As String
    Dim valor As String = ""

    Try
      valorBuscar = IIf(String.IsNullOrEmpty(valorBuscar), "-1", valorBuscar)
      Dim table As DataTable = ds.Tables(indiceTabla)
      Dim filas = From row As DataRow In table.Rows Where row.Item(nombreColumnaFiltro) = valorBuscar

      For Each row In filas
        valor = row.Item(nombreColumnaRetornoValor)
        Exit For
      Next
    Catch ex As Exception
      valor = ""
    End Try

    Return valor
  End Function

  ''' <summary>
  ''' muestra el listado de errores
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarListadoErrores()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim listadoErrores As New List(Of String)
    Dim sb As New StringBuilder

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      listadoErrores = CType(Session(SESION_LISTADO_ERRORES), List(Of String))

      If (Not listadoErrores Is Nothing) Then
        If (listadoErrores.Count > 0) Then
          Sistema.GrabarLogSesion(oUsuario.Id, "Muestra errores carga planificación")

          sb.Append("<div class=""form-group""><strong>Se encontraron los siguientes errores:</strong></div>")
          sb.Append("<div>")
          sb.Append("  <ul>")
          For Each texto As String In listadoErrores
            sb.Append("<li>" & texto & "</li>")
          Next
          sb.Append("  </ul>")
          sb.Append("</div>")

          Me.ltlTablaDatos.Visible = True
          Me.ltlTablaDatos.Text = Utilidades.MostrarMensajeUsuario(sb.ToString(), Utilidades.eTipoMensajeAlert.Danger, False, "text-left")
        End If
      End If
    Catch ex As Exception
      Me.ltlTablaDatos.Visible = False
    End Try

    Session(SESION_LISTADO_ERRORES) = Nothing
  End Sub

  ''' <summary>
  ''' muestra la carga realizada
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarCargaRealizada(ByVal idPlanificacion As String, ByVal ds As DataSet)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim html As String = ""
    Dim dt As New DataTable
    Dim sb As New StringBuilder
    Dim template As String = ""
    Dim totalRegistros As Integer
    Dim templateAux, nombreTransportista, rutTransportista, nroTransporte, fechaPresentacion, carga, tipoCamion, locales As String

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()

      template &= "<tr>"
      template &= "  <td>{ID_PLANIFICACION}</td>"
      template &= "  <td>{NOMBRE_TRANSPORTISTA}</td>"
      template &= "  <td>{FECHA_PRESENTACION}</td>"
      template &= "  <td>{NRO_TRANSPORTE}</td>"
      template &= "  <td>{LOCALES}</td>"
      template &= "  <td>{CARGA}</td>"
      template &= "  <td>{TIPO_CAMION}</td>"
      template &= "</tr>"

      dt = ds.Tables(eTabla.T00_ListadoPlanificacion)
      totalRegistros = dt.Rows.Count

      If (totalRegistros > 0) Then
        Sistema.GrabarLogSesion(oUsuario.Id, "Muestra datos cargados de la planificación: IdPlanificacion " & idPlanificacion)

        sb.Append("<div class=""small"">")
        sb.Append("<table class=""table table-condensed table-bordered"">")
        sb.Append("  <thead>")
        sb.Append("    <tr>")
        sb.Append("      <th style=""width:100px"">IDPlanificacion</th>")
        sb.Append("      <th>L&iacute;nea de Transporte</th>")
        sb.Append("      <th style=""width:170px"">Fecha Hora Presentaci&oacute;n</th>")
        sb.Append("      <th style=""width:120px"">IdMaster</th>")
        sb.Append("      <th>Tienda</th>")
        sb.Append("      <th style=""width:120px"">Carga</th>")
        sb.Append("      <th style=""width:100px"">Tipo Cami&oacute;n</th>")
        sb.Append("    </tr>")
        sb.Append("  </thead>")
        sb.Append("  <tbody>")

        For Each dr As DataRow In dt.Rows
          templateAux = template
          idPlanificacion = dr.Item("IdPlanificacion")
          nombreTransportista = dr.Item("NombreTransportista")
          rutTransportista = dr.Item("RutTransportista")
          nroTransporte = dr.Item("NroTransporte")
          fechaPresentacion = dr.Item("FechaPresentacion")
          carga = dr.Item("Carga")
          tipoCamion = dr.Item("TipoCamion")

          'obtiene los locales asociados al NroTransporte
          locales = ObtenerLocalesPorNroTransporte(ds, nroTransporte, idPlanificacion)

          'formatea valores
          nombreTransportista = Server.HtmlEncode(nombreTransportista)
          nombreTransportista &= IIf(String.IsNullOrEmpty(rutTransportista), "", "<em class=""help-block small sist-margin-cero""><strong>Rut</strong>: " & Server.HtmlEncode(rutTransportista) & "</em>")

          templateAux = templateAux.Replace("{ID_PLANIFICACION}", idPlanificacion)
          templateAux = templateAux.Replace("{NOMBRE_TRANSPORTISTA}", nombreTransportista)
          templateAux = templateAux.Replace("{NRO_TRANSPORTE}", nroTransporte)
          templateAux = templateAux.Replace("{FECHA_PRESENTACION}", fechaPresentacion)
          templateAux = templateAux.Replace("{CARGA}", Server.HtmlEncode(carga))
          templateAux = templateAux.Replace("{TIPO_CAMION}", Server.HtmlEncode(tipoCamion))
          templateAux = templateAux.Replace("{LOCALES}", locales)
          sb.Append(templateAux)
        Next

        sb.Append("  </tbody>")
        sb.Append("</table>")
        sb.Append("</div>")
        Me.ltlTablaDatos.Text = sb.ToString()
      End If
    Catch ex As Exception
      Me.ltlTablaDatos.Visible = False
    End Try

    Session(SESION_ID_PLANIFICACION) = Nothing
  End Sub

  ''' <summary>
  ''' obtiene los locales asociados al NroTransporte
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="nroTransporte"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ObtenerLocalesPorNroTransporte(ByVal ds As DataSet, ByVal nroTransporte As String, ByVal idPlanificacion As String) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim htmlSinRegistro As String = "<em class=""help-block"">No tiene tiendas asociadas</em>"
    Dim dt As DataTable
    Dim templateAux, nombreLocal, codigoLocal, ordenEntrega As String
    Dim sb As New StringBuilder

    Try
      template &= "<div>"
      template &= "  {ORDEN_ENTREGA}) {NOMBRE_LOCAL}"
      template &= "</div>"

      dt = ds.Tables(eTabla.T01_Locales)
      Dim filtroListado = From row As DataRow In dt.Rows Where
                          row.Item("NroTransporte") = nroTransporte _
                          And row.Item("IdPlanificacion") = idPlanificacion

      If (filtroListado.Count = 0) Then
        html = htmlSinRegistro
      Else
        For Each dr As DataRow In filtroListado
          templateAux = template
          nombreLocal = dr.Item("nombreLocal")
          codigoLocal = dr.Item("codigoLocal")
          ordenEntrega = dr.Item("ordenEntrega")

          'formatea valores
          nombreLocal = nombreLocal & IIf(String.IsNullOrEmpty(codigoLocal), "", " - " & codigoLocal)

          templateAux = templateAux.Replace("{ORDEN_ENTREGA}", ordenEntrega)
          templateAux = templateAux.Replace("{NOMBRE_LOCAL}", nombreLocal)
          sb.Append(templateAux)
        Next

        html = sb.ToString()
      End If
    Catch ex As Exception
      html = htmlSinRegistro
    End Try

    Return html
  End Function

#End Region

End Class
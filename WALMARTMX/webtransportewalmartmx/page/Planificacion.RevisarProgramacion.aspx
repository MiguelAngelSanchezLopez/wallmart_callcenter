﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Planificacion.RevisarProgramacion.aspx.vb" Inherits="webtransportewalmartmx.Planificacion_RevisarProgramacion" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<%@ Register Src="../wuc/wucHora.ascx" TagName="wucHora" TagPrefix="uc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphHead" runat="server">
  <style type="text/css">
    .popover{ max-width: 50%; }
  </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/planificacion.js"></script>
  <script type="text/javascript" src="../js/reporte.js"></script>

  <div>
    <input type="hidden" id="txtJSONRevisarProgramacion" runat="server" value="[]" />

    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="form-group small">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="pull-left">
              <h3 class="panel-title sist-font-size-12"><strong>REVISAR PROGRAMACI&Oacute;N DE CAMIONES</strong> > FILTROS DE B&Uacute;SQUEDA</h3>
            </div>
            <div class="pull-right">
              <div id="btnNuevaProgramacion" runat="server" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span>&nbsp;Crear Nueva Programaci&oacute;n</div>
            </div>
            <div class="sist-clear-both"></div>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-xs-2">
                <label class="control-label">Fecha Presentaci&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtFechaPresentacion" runat="server" MaxLength="10" CssClass="form-control input-sm date-pick"></asp:TextBox>
              </div>
              <div class="col-xs-2">
                <label class="control-label">Hora Presentaci&oacute;n</label>
                <uc2:wucHora ID="wucHoraInicio" runat="server" />
                <div id="lblMensajeErrorHora" runat="server"></div>
              </div>
              <div class="col-xs-3">
                <label class="control-label">L&iacute;nea de Transporte</label>
                <uc1:wucCombo id="ddlTransportista" runat="server" FuenteDatos="Tabla" TipoCombo="Transportista" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div class="col-xs-2">
                <label class="control-label">IdMaster</label>
                <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="150" CssClass="form-control input-sm"></asp:TextBox>
              </div>
              <div class="col-xs-3">
                <label class="control-label">Tienda</label>
                <uc1:wucCombo id="ddlLocal" runat="server" FuenteDatos="Tabla" TipoCombo="Local" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
            </div>
            <div class="row">              
              <div class="col-xs-2">
                <label class="control-label">Carga</label>
                <uc1:wucCombo id="ddlCarga" runat="server" FuenteDatos="Tabla" TipoCombo="Carga" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div class="col-xs-1">
                <label class="control-label">Tracto</label>
                <asp:TextBox ID="txtPatenteTracto" runat="server" MaxLength="150" CssClass="form-control input-sm"></asp:TextBox>
              </div>
              <div class="col-xs-1">
                <label class="control-label">Remolque</label>
                <asp:TextBox ID="txtPatenteTrailer" runat="server" MaxLength="150" CssClass="form-control input-sm"></asp:TextBox>
              </div>
              <div class="col-xs-8 text-right">
                <label class="control-label">&nbsp;</label><br />
                <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary btn-sm" OnClientClick="return(Planificacion.validarFormularioBusquedaRevisarProgramacion())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
              </div>
            </div>
          </div>
        </div>        
      </div>

      <!-- RESULTADO BUSQUEDA -->
      <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>
      <div class="form-group">
        <asp:Literal ID="ltlTablaDatos" runat="server"></asp:Literal>
      </div>

      <!-- BOTONES -->
      <asp:Panel ID="pnlBotones" runat="server" CssClass="form-group text-right" Visible="false">
        <div class="pull-right">
          <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-success" OnClientClick="return(Planificacion.grabarRevisarProgramacion())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
        </div>
        <div class="pull-right">
          <div id="lblMensajeGeneracionReporte"></div>
          <div id="btnExportar" runat="server" class="btn btn-primary" onclick="Reporte.exportar({ prefijoControl: Sistema.PREFIJO_CONTROL, invocadoDesde: Reporte.FORMULARIO_REVISAR_PROGRAMACION, llaveReporte: 'Programacion' })"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar programaci&oacute;n</div>
          &nbsp;
        </div>
      </asp:Panel>

      <!-- separador -->
      <div class="form-group sist-clear-both"><br /></div>

      <script type="text/javascript">
        jQuery(document).ready(function () {
          jQuery(".date-pick").datepicker({
            changeMonth: true,
            changeYear: true
          });

          jQuery(".chosen-select").select2();
          jQuery(".show-popover").popover({ html: true });
        });
      </script>

    </asp:Panel>
  </div>

</asp:Content>

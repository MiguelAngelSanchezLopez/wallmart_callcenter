﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Mantenedor.CargadorPoolPlacaRemolques.aspx.vb" Inherits="webtransportewalmartmx.Mantenedor_CargadorPoolPlacaRemolques" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <script type="text/javascript" src="../js/poolplacaRemolques.js"></script>

    <div class="form-group text-center">
        <div class="row">
            <div class="col-xs-4">
                &nbsp;
            </div>
            <div class="col-xs-4">
                <p class="h1">Cargar Pool de Placas de Remolques</p>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlMensaje" runat="server"></asp:Panel>

    <div>
        <asp:Panel ID="pnlMensajeAcceso" runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" runat="server">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>SELECCIONAR ARCHIVO DE CARGA</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <asp:FileUpload ID="fileCarga" runat="server" CssClass="form-control"></asp:FileUpload>
                            <div class="sist-padding-top-5">
                                <a href="../template/PlantillaCargaPoolPlacasRemolques.xlsx">[Descargar plantilla excel]</a>
                            </div>
                        </div>
                        <div id="hBotonValidar" runat="server" class="col-xs-4">
                            <asp:LinkButton ID="btnValidar" runat="server" CssClass="btn btn-success" OnClientClick="return(PoolPlacaRemolque.validarFormularioCarga())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Validar Archivo</asp:LinkButton>
                        </div>
                        <div id="hMensajeBotonValidar" runat="server" class="col-xs-6 sist-display-none">
                            <span class="help-block"><em>... Validando, espere un momento</em></span>
                        </div>
                    </div>

                </div>
            </div>

            <!-- RESULTADO CARGA -->
            <div class="form-group">
                <asp:Literal ID="ltlTablaDatos" runat="server"></asp:Literal>
            </div>

            <asp:Panel ID="pnlBotones" runat="server" Visible="false">
                <div class="form-group text-center">
                    <asp:LinkButton ID="btnCancelar" runat="server" CssClass="btn btn-lg btn-default" OnClientClick="return(PoolPlacaRemolque.confirmar({ tipo: 'Cancelar' }))"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancelar</asp:LinkButton>
                    <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(PoolPlacaRemolque.confirmar({ tipo: 'Grabar' }))"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
                </div>
            </asp:Panel>
            <div id="lblMensajeBotones" runat="server" class="text-muted sist-font-size-24 text-center"></div>

        </asp:Panel>
    </div>

</asp:Content>

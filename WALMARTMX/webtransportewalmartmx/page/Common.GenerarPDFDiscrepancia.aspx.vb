﻿Imports CapaNegocio
Imports System.IO

Public Class Common_GenerarPDFDiscrepancia
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idFormulario As String = Utilidades.IsNull(Request("idFormulario"), "-1")
    Dim tipoInforme As String = Server.UrlDecode(Utilidades.IsNull(Request("tipoInforme"), ""))
    Dim urlInvocadoDesde As String = Server.UrlDecode(Utilidades.IsNull(Request("urlInvocadoDesde"), ""))
    Dim nroTransporte As String = Server.UrlDecode(Utilidades.IsNull(Request("nroTransporte"), ""))
    Dim codigoLocal As String = Server.UrlDecode(Utilidades.IsNull(Request("codigoLocal"), ""))
    Dim nombreLocal As String = Server.UrlDecode(Utilidades.IsNull(Request("nombreLocal"), ""))
    Dim dm As String = Server.UrlDecode(Utilidades.IsNull(Request("dm"), ""))

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      Me.ViewState.Add("idFormulario", idFormulario)
      Me.ViewState.Add("tipoInforme", tipoInforme)
      Me.ViewState.Add("urlInvocadoDesde", urlInvocadoDesde)
      Me.ViewState.Add("nroTransporte", nroTransporte)
      Me.ViewState.Add("codigoLocal", codigoLocal)
      Me.ViewState.Add("nombreLocal", nombreLocal)
      Me.ViewState.Add("dm", dm)
      GenerarPDF()
    End If

  End Sub

  Private Sub GenerarPDF()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim arrListado As New ArrayList
    Dim arrArchivo As String()
    Dim idFormulario As String = Me.ViewState.Item("idFormulario")
    Dim tipoInforme As String = Me.ViewState.Item("tipoInforme")
    Dim urlInvocadoDesde As String = Me.ViewState.Item("urlInvocadoDesde")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim codigoLocal As String = Me.ViewState.Item("codigoLocal")
    Dim nombreLocal As String = Me.ViewState.Item("nombreLocal")
    Dim dm As String = Me.ViewState.Item("dm")
    Dim rutaArchivo As String = ""
    Dim nombreArchivo As String = ""
    Dim mensajeRetorno As String = ""
    Dim dcMensajeRetorno As New Dictionary(Of String, String)
    Dim rutaCarpetaServidor, rutaCarpetaFTP As String
    Dim ftp As New MyFTP
    Dim ftpContenidoDirectorio, ftpListadoArchivos As FTPdirectory

    Try
      Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"
      Me.htmlBody.Visible = False

      Select Case tipoInforme.ToUpper()
        Case "CARGA"
          rutaCarpetaServidor = Discrepancia.RUTA_CARPETA_SERVIDOR_PDF_CARGAR
          rutaCarpetaFTP = Discrepancia.RUTA_CARPETA_FTP_PDF_CARGA
        Case "RECEPCION"
          rutaCarpetaServidor = Discrepancia.RUTA_CARPETA_SERVIDOR_PDF_RECEPCION
          rutaCarpetaFTP = Discrepancia.RUTA_CARPETA_FTP_PDF_RECEPCION
        Case Else
          rutaCarpetaServidor = ""
          rutaCarpetaFTP = ""
      End Select

      If (String.IsNullOrEmpty(rutaCarpetaFTP)) Then
        mensajeRetorno = "PDF " & tipoInforme & ": No existe la carpeta ""PDF_" & tipoInforme & """ en el FTP para buscar el archivo."
        Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"
      Else
        'Crea directorio en servidor si es que no existe
        Archivo.CrearDirectorioEnDisco(rutaCarpetaServidor)

        '--------------------------------------------------------
        'obtiene archivo desde el ftp para ser leido
        ftp = New MyFTP(Constantes.FTP_TRANSPORTE_HOST, Constantes.FTP_TRANSPORTE_USUARIO, Constantes.FTP_TRANSPORTE_PASSWORD)

        'obtiene contenido del directorio
        ftpContenidoDirectorio = ftp.ListDirectoryDetail(rutaCarpetaFTP)

        'filtra los archivos por la extension
        ftpListadoArchivos = ftpContenidoDirectorio.GetFiles("pdf")

        'descarga el archivo
        For Each file As FTPfileInfo In ftpListadoArchivos
          If (file.Filename.EndsWith(idFormulario & ".pdf")) Then
            ftp.Download(file, rutaCarpetaServidor & "\" & file.Filename, True)
            Exit For
          End If
        Next

        '------------------------------------------------------------
        'obtiene archivo para mostrarlo
        arrArchivo = IO.Directory.GetFiles(rutaCarpetaServidor, "*-" & idFormulario & ".pdf")

        If (arrArchivo.Length = 0) Then
          mensajeRetorno = "PDF " & tipoInforme & ": No tiene archivo asociado para descargar."
          Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"
        Else
          rutaArchivo = arrArchivo(0)
          nombreArchivo = IO.Path.GetFileName(rutaArchivo)

          'convierte el archivo que esta en disco en binario para poder mostrarlo en la descarga
          Dim objStream As Object
          objStream = Server.CreateObject("ADODB.Stream")
          objStream.Open()
          objStream.Type = 1 'tipo binario
          objStream.LoadFromFile(rutaArchivo)

          'los carga en la pagina
          Response.Buffer = True
          Response.Clear()
          Response.AppendHeader("content-disposition", "attachment; filename=" & nombreArchivo)
          Response.ContentType = "application/pdf"
          Response.BinaryWrite(objStream.Read)

          objStream.Close()
          objStream = Nothing

          Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "1"
        End If

      End If
    Catch ex As Exception
      mensajeRetorno = ex.Message
      Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"
    End Try

    'si hay un error redirecciona pagina
    If (Not String.IsNullOrEmpty(mensajeRetorno)) Then
      Me.htmlBody.Visible = True
      dcMensajeRetorno.Add("nroTransporte", nroTransporte)
      dcMensajeRetorno.Add("codigoLocal", codigoLocal)
      dcMensajeRetorno.Add("nombreLocal", nombreLocal)
      dcMensajeRetorno.Add("dm", dm)
      dcMensajeRetorno.Add("mensajeRetorno", Utilidades.EscaparComillasJS(mensajeRetorno))
      Session(Discrepancia.KEY_SESION_DISCREPANCIA_MENSAJE) = dcMensajeRetorno

      Utilidades.RegistrarScript(Me.Page, "top.location.href=""" & urlInvocadoDesde & """;", Utilidades.eRegistrar.FINAL, "pageLoad", False)
    End If
  End Sub

End Class
﻿Imports CapaNegocio
Imports System.Data

Public Class TO_AgregarExplicacion
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idExplicacion As String = Me.ViewState.Item("idExplicacion")
    Dim explicacion As String

    'inicializa controles en vacio
    explicacion = ""

    'asigna valores
    Me.txtExplicacion.Text = explicacion
    Me.lblTituloFormulario.Text = "Nueva Explicaci&oacute;n"
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim idExplicacion As String = Me.ViewState.Item("idExplicacion")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idExplicacion = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el usuario en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef idExplicacion As String, ByRef json As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim prefijoControl As String = Me.ViewState.Item("prefijoControl")
    Dim nombre, tipo, extra1, extra2 As String
    Dim dcJSON As New Dictionary(Of String, String)

    Try
      nombre = Utilidades.IsNull(Me.txtExplicacion.Text, "")
      tipo = "Explicacion"
      extra1 = "Normal"
      extra2 = "-1"

      If idExplicacion = "-1" Then
        status = Sistema.GrabarTipoGeneral(nombre, tipo, extra1, extra2)
        If (status = "error") Then Throw New Exception("No se pudo grabar registro Tipo General")

        'construye json de retorno
        dcJSON.Add("Nombre", nombre.Trim())
        dcJSON.Add("PrefijoControl", prefijoControl)
        json = MyJSON.ConvertObjectToJSON(dcJSON)
        Sistema.GrabarLogSesion(oUsuario.Id, "Crea nueva explicación: " & nombre)
      End If

    Catch ex As Exception
      msgError = "Error al grabar registro Tipo General.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idExplicacion As String = Me.ViewState.Item("idExplicacion")
    Dim prefijoControl As String = Me.ViewState.Item("prefijoControl")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""
    Dim json As String = "{}"

    'grabar valores
    textoMensaje &= GrabarDatosRegistro(status, idExplicacion, json)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La EXPLICACION fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La EXPLICACION fue actualizada satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}La EXPLICACION ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la EXPLICACION. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la EXPLICACION. Intente nuevamente por favor.<br />" & textoMensaje
    End If

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Alerta.asignarExplicacionEnCombo(" & json & ");", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

      queryStringPagina &= "prefijoControl=" & Server.UrlEncode(prefijoControl)
      Response.Redirect("./TO.AgregarExplicacion.aspx?" & queryStringPagina)
    End If
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idExplicacion As String = "-1" 'siempre el registro es nuevo
    Dim prefijoControl As String = Utilidades.IsNull(Request("prefijoControl"), "")

    Me.ViewState.Add("idExplicacion", idExplicacion)
    Me.ViewState.Add("prefijoControl", prefijoControl)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ActualizaInterfaz()
      InicializaControles(Page.IsPostBack)
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

End Class
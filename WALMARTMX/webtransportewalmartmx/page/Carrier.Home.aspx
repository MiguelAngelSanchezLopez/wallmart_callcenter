﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Carrier.Home.aspx.vb" Inherits="webtransportewalmartmx.Carrier_Home" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/carrier.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <asp:HiddenField id="hIdTransportista" runat="server">
  </asp:HiddenField>
  <input type="hidden" id="hJsonTractosDisponibles" />
  <input type="hidden" id="hJsonRemolquesEnPatio" />
  <input type="hidden" id="hJsonRemolquesEnCortina" />
  <input type="hidden" id="hJsonEmbarqueEnRuta" />
  <input type="hidden" id="hJsonEmbarqueEnTienda" />
  <input type="hidden" id="hJsonTractoRegresando" />
  <div>
    <ul id="myTab" class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">
        <h3 class="sist-margin-cero">
          Flota Online</h3>
      </a></li>
      <li onclick="Sistema.accederUrl({ url: 'Carrier.Indicadores.aspx' });"><a href="#profile" data-toggle="tab">
        <h3 class="sist-margin-cero">
          Indicadores</h3>
      </a></li>
    </ul>
  </div>

  <div class="sist-padding-top-20">
    <asp:Panel ID="pnlMensajeAcceso" Runat="server">
    </asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">
      <div>
        <!-- TRANSPORTISTA -->
        <div class="form-group" id="dvTransportista" runat="server">
          <div class="row">
            <div class="col-xs-6">
              <label class="control-label">L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
              <uc1:wucCombo id="ddlTransportista" runat="server" FuenteDatos="Tabla" TipoCombo="Transportista" ItemSeleccione="true" CssClass="form-control chosen-select" funcionOnChange="Carrier.cargarLineaTransporte()"></uc1:wucCombo>
              <input type="hidden" id="ddlTransportista_ValorActual" runat="server" />
            </div>
          </div>
        </div>
        <!-- TRACTOS DISPONIBLES -->
        <div class="panel panel-default sist-cursor-pointer">
          <div class="panel-heading">
            <span class="panel-title"><strong>TRACTOS DISPONIBLES: <span id="hTotalTractosDisponibles">
            </span></strong></span>
          </div>
          <div class="panel-body">
            <div class="row" id="dvFiltroTracto">
              <div class="col-xs-12">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                      <span class="glyphicon glyphicon-search"></span>&nbsp;
                    </button>
                  </span>
                  <input id="txtFiltroTracto" type="text" class="form-control" placeholder="Placa Tracto...">
                </div>
              </div>
            </div>
            <br />
            <div id="hTractosDisponibles" class="pre-scrollable" style="max-height: 200px;">
            </div>
          </div>
        </div>        

        <!-- REMOLQUES EN PATIO -->
        <div class="panel panel-default sist-cursor-pointer">
          <div class="panel-heading">
            <span class="panel-title"><strong>REMOLQUES EN PATIO: <span id="hTotalRemolquesEnPatio">
            </span></strong></span>
          </div>
          <div class="panel-body">
            <div class="row" id="dvFiltroRemolquesEnPatio">
              <div class="col-xs-12">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                      <span class="glyphicon glyphicon-search"></span>&nbsp;
                    </button>
                  </span>
                  <input id="txtFiltroRemolquesEnPatio" type="text" class="form-control" placeholder="Placa Remolque...">
                </div>
              </div>
            </div>
            <br />
            <div class="text-danger">
              <div id="dvTiempoExcedidoRemolquesEnPatio">
              </div>
            </div>
            <div class="text-success">
              <div id="dvTiempoRemolquesEnPatio">
              </div>
            </div>
            <div id="hRemolquesEnPatio" class="pre-scrollable" style="max-height: 190px;">
            </div>
          </div>
        </div>        

        <!-- REMOLQUES EN CORTINA -->
        <div class="panel panel-default sist-cursor-pointer">
          <div class="panel-heading">
            <span class="panel-title"><strong>REMOLQUES EN CORTINA: <span id="hTotalRemolquesEnCortina">
            </span></strong></span>
          </div>
          <div class="panel-body">
            <div class="row" id="dvFiltroRemolquesEnCortina">
              <div class="col-xs-12">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                      <span class="glyphicon glyphicon-search"></span>&nbsp;
                    </button>
                  </span>
                  <input id="txtFiltroRemolquesEnCortina" type="text" class="form-control" placeholder="Placa Remolque...">
                </div>
              </div>
            </div>
            <br />
            <div class="text-danger">
              <div id="dvTiempoExcedidoRemolquesEnCortina">
              </div>
            </div>
            <div class="text-success">
              <div id="dvTiempoRemolquesEnCortina">
              </div>
            </div>
            <div id="hRemolquesEnCortina" class="pre-scrollable" style="max-height: 190px;">
            </div>
          </div>
        </div>        

        <!-- EMBARQUES EN RUTA -->
        <div class="panel panel-default sist-cursor-pointer">
          <div class="panel-heading">
            <span class="panel-title"><strong>EMBARQUES EN RUTA: <span id="hTotalEmbarquesEnRuta">
            </span></strong></span>
          </div>
          <div class="panel-body">
            <div class="row" id="dvFiltroEmbarqueEnRuta">
              <div class="col-xs-12">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                      <span class="glyphicon glyphicon-search"></span>&nbsp;
                    </button>
                  </span>
                  <input id="txtFiltroEmbarqueEnRuta" type="text" class="form-control" placeholder="Placa Tracto...">
                </div>
              </div>
            </div>
            <br />
            <div class="text-danger">
              <div id="dvAtrasadosEmbarqueEnRuta">
              </div>
            </div>
            <div class="text-success">
              <div id="dvATiempoEmbarqueEnRuta">
              </div>
            </div>
            <div id="hEmbarquesEnRuta" class="pre-scrollable" style="max-height: 190px;">
            </div>
          </div>
        </div>        

        <!-- EMBARQUES EN TIENDA -->
        <div class="panel panel-default sist-cursor-pointer">
          <div class="panel-heading">
            <span class="panel-title"><strong>EMBARQUES EN TIENDA: <span id="hTotalEmbarquesEnTienda">
            </span></strong></span>
          </div>
          <div class="panel-body">
            <div class="row" id="dvFiltroEmbarqueEnTienda">
              <div class="col-xs-12">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                      <span class="glyphicon glyphicon-search"></span>&nbsp;
                    </button>
                  </span>
                  <input id="txtFiltroEmbarqueEnTienda" type="text" class="form-control" placeholder="Placa Tracto...">
                </div>
              </div>
            </div>
            <br />
            <!-- DESCARGANDO -->
            <p class="sist-font-size-16">
              <strong>Descargando: <span id="hTotalEmbarquesEnTiendaDescargando"></span></strong>
            </p>
            <div class="text-danger">
              <div id="dvAtrasadosDescargando">
              </div>
            </div>
            <div class="text-success">
              <div id="dvATiempoDescargando">
              </div>
            </div>
            <div id="hEmbarquesEnTiendaDescargando" class="pre-scrollable" style="max-height: 190px;">
            </div>
            <div class="sist-clear-both">
            </div>
            <hr />

            <!-- ESPERANDO -->
            <p class="sist-font-size-16">
              <strong>Esperando: <span id="hTotalEmbarquesEnTiendaEsperando"></span></strong>
            </p>
            <div class="text-danger">
              <div id="dvAtrasadosEsperando">
              </div>
            </div>
            <div class="text-success">
              <div id="dvATiempoEsperando">
              </div>
            </div>
            <div id="hEmbarquesEnTiendaEsperando" class="pre-scrollable" style="max-height: 190px;">
            </div>
          </div>
        </div>        

        <!-- TRACTOS REGRESANDO -->
        <div class="panel panel-default sist-cursor-pointer">
          <div class="panel-heading">
            <span class="panel-title"><strong>TRACTOS REGRESANDO: <span id="hTotalTractosRegresando">
            </span></strong></span>
          </div>
          <div class="panel-body">
            <div class="row" id="dvFiltroTractoRegresando">
              <div class="col-xs-12">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                      <span class="glyphicon glyphicon-search"></span>&nbsp;
                    </button>
                  </span>
                  <input id="txtFiltroTractoRegresando" type="text" class="form-control" placeholder="Placa Tracto...">
                </div>
              </div>
            </div>
            <br />
            <div class="text-danger">
              <div id="dvAtrasadosTractoRegresando">
              </div>
            </div>
            <div class="text-success">
              <div id="dvATiempoTractoRegresando">
              </div>
            </div>
            <div id="hTractosRegresando" class="pre-scrollable" style="max-height: 190px;">
            </div>
          </div>
        </div>        

      </div>
    </asp:Panel>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function () {

      jQuery(".chosen-select").select2();

      //Carga de datos
      Carrier.cargaTractosDisponibles();
      Carrier.cargaRemolquesEnPatio();
      Carrier.cargaRemolquesEnCortina();
      Carrier.cargaEmbarquesEnRuta();
      Carrier.cargaEmbarquesEnTienda();
      Carrier.cargaTractosRegresando();
      setTimeout(function () { jQuery(".show-popover").popover({ html: true }); }, 100);

      //Busqueda instantanea
      Carrier.busquedaInstantanea("txtFiltroTracto");
      Carrier.busquedaInstantanea("txtFiltroRemolquesEnPatio");
      Carrier.busquedaInstantanea("txtFiltroRemolquesEnCortina");
      Carrier.busquedaInstantanea("txtFiltroEmbarqueEnRuta");
      Carrier.busquedaInstantanea("txtFiltroEmbarqueEnTienda");
      Carrier.busquedaInstantanea("txtFiltroTractoRegresando");

      setTimeout(function () { jQuery(".show-popover").popover({ html: true }); }, 100);

    });
  </script>

</asp:Content>

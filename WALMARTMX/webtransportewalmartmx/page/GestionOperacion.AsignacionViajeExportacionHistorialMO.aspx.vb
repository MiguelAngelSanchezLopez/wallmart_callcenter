﻿Imports CapaNegocio
Imports System.Data

Public Class GestionOperacion_AsignacionViajeExportacionHistorialMO
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
  End Enum

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim numeroOrdenServicio As String = Utilidades.IsNull(Request("numeroOrdenServicio"), "-1")
    Dim numeroLineaMO As String = Utilidades.IsNull(Request("numeroLineaMO"), "-1")
    Dim versionMO As String = Utilidades.IsNull(Request("versionMO"), "-1")

    Me.ViewState.Add("numeroOrdenServicio", numeroOrdenServicio)
    Me.ViewState.Add("numeroLineaMO", numeroLineaMO)
    Me.ViewState.Add("versionMO", versionMO)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim numeroOrdenServicio As String = Me.ViewState.Item("numeroOrdenServicio")
    Dim numeroLineaMO As String = Me.ViewState.Item("numeroLineaMO")
    Dim versionMO As String = Me.ViewState.Item("versionMO")

    Try
      'obtiene listado
      ds = GestionOperacion.ObtenerHistorialAsignacionViajesExportacion(numeroOrdenServicio, numeroLineaMO, versionMO)
    Catch ex As Exception
      ds = Nothing
    End Try

    If ds Is Nothing Then
      Session("dv") = Nothing
    Else
      Session("dv") = ds.Tables(0).DefaultView
    End If
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim dv As New DataView
    Dim totalRegistros As Integer

    dv = CType(Session("dv"), DataView)
    If dv Is Nothing Then
      totalRegistros = 0
    Else
      totalRegistros = dv.Table.Rows.Count
    End If

    If totalRegistros > 0 Then
      Me.pnlMensajeUsuario.Visible = False
      Me.pnlHolderGrilla.Visible = True
      'carga la grilla a utilizar
      Me.gvPrincipal.Visible = True
      Me.gvPrincipal.DataSource = dv
      Me.gvPrincipal.DataBind()
    Else
      Me.pnlHolderGrilla.Visible = False
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = "No se encontraron registros"
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)
      'oculta la grilla
      Me.gvPrincipal.Visible = False
    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = True 'Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region


End Class
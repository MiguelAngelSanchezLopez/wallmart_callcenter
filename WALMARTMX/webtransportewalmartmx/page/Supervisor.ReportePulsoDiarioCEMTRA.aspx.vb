﻿Imports CapaNegocio
Imports System.Data

Public Class Supervisor_ReportePulsoDiarioCEMTRA
    Inherits System.Web.UI.Page

#Region "Enum"
    Private Enum eFunciones
        Ver
    End Enum
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        VerificarPermisos()
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    End Sub
    ''' <summary>
    ''' verifica los permisos sobre los controles
    ''' </summary>
    Private Sub VerificarPermisos()
        'verifica los permisos para los controles
        Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

        If Not Me.pnlContenido.Visible Then
            Me.pnlMensajeAcceso.Visible = True
            Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
        End If
    End Sub
    Private Sub btnFiltrar_Click(sender As Object, e As System.EventArgs) Handles btnFiltrar.Click
        Dim fechaInicio, fechaTermino, operador, turno As String
        Dim ds As New DataSet
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim idusuario As Integer

        Try
            fechaInicio = Utilidades.IsNull(TxFechaInicio.Text, "-1")
            fechaTermino = Utilidades.IsNull(TxFechaTermino.Text, "-1")
            operador = Utilidades.IsNull(ddlOperador.SelectedValue, "-1")
            turno = Utilidades.IsNull(ddlTurno.SelectedValue, "-1")
            idusuario = Utilidades.IsNull(oUsuario.Id, 0)

            'EXTRAE DATA PARA REPORTE
            ds = Usuario.ReportePulsoDiarioToCemtra(fechaInicio, fechaTermino, operador, turno, idusuario)

            pnlHolderGrilla.Visible = True
            gvPrincipal.Visible = True

            If ds.Tables(0).Rows.Count = 0 Then
                ViewState("datasource") = Nothing
                gvPrincipal.DataSource = ViewState("datasource")
                gvPrincipal.DataBind()
                Dim TextoMensaje As String = "No se encontraron registros"
                Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)
            Else
                ViewState("datasource") = ds
                gvPrincipal.DataSource = ViewState("datasource")
                gvPrincipal.DataBind()
            End If

            Utilidades.RegistrarScript(Me.Page, "setTimeout(function () { Usuario.dibujarPulsoDiarioCEMTRA(); }, 100);", Utilidades.eRegistrar.FINAL, "seek", False)
        Catch ex As Exception

        End Try

    End Sub
 
End Class
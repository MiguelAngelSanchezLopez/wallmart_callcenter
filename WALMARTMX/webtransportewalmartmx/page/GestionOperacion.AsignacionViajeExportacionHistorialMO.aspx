﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GestionOperacion.AsignacionViajeExportacionHistorialMO.aspx.vb" Inherits="webtransportewalmartmx.GestionOperacion_AsignacionViajeExportacionHistorialMO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Historial MO exportaci&oacute;n</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/gestionOperacion.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Historial Mensaje Operativo Exportaci&oacute;n"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">
          <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive small" Visible="false">

            <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="False" AllowSorting="False" CssClass="table table-striped table-bordered">
              <Columns>
                <asp:TemplateField HeaderText="#">
                  <HeaderStyle Width="40px" />
                  <ItemTemplate>
                    <%# Container.DataItemIndex + 1%>
                  </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="NumeroOrdenServicio" HeaderText="Orden Servicio" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="NumeroLineaMO" HeaderText="L&iacute;nea MO" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="VersionMO" HeaderText="Versi&oacute;n MO" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="EstadoLinea" HeaderText="Estado L&iacute;nea" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="NombreCliente" HeaderText="Cliente" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="LugarPresentacion" HeaderText="Lugar de Presentaci&oacute;n" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="SegundoDestino" HeaderText="Segundo Destino" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="FechaHoraPresentacion" HeaderText="Fecha Presentaci&oacute;n" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="MedidaContenedor" HeaderText="Medida" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <%--<asp:BoundField DataField="DescripcionTipoCarga" HeaderText="Tipo" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />--%>
                <asp:BoundField DataField="PesoNetoCarga" HeaderText="Peso" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="NumeroContenedor" HeaderText="Contenedor" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="DepositoRetiro" HeaderText="Dep&oacute;sito Retiro" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="Reserva" HeaderText="Reserva" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="NombreNave" HeaderText="Nave" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="FechaHoraProgramacion" HeaderText="Fecha Programaci&oacute;n" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
              </Columns>
            </asp:GridView>
          </asp:Panel>
        </asp:Panel>
      </div>
    </div>

  </form>
</body>
</html>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Mantenedor.DefinicionAlertaListado.aspx.vb" Inherits="webtransportewalmartmx.Mantenedor_DefinicionAlertaListado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script language="javascript" type="text/javascript" src="../js/alerta.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Definici&oacute;n de Alertas</p>
      </div>
      <div class="col-xs-4 text-right">
        <div id="btnNuevo" runat="server" class="btn btn-success" onclick="Alerta.abrirFormularioDefinicionAlerta({ idAlertaDefinicion:'-1' })"><span class="glyphicon glyphicon-plus"></span>&nbsp;Crear Nueva Definici&oacute;n de Alerta</div>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-3">
              <label class="control-label">Nombre</label>
              <asp:TextBox ID="txtFiltroNombre" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Gestionar por CEMTRA</label>
              <div><asp:CheckBox ID="chkGestionarPorCemtra" runat="server" /></div>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtFiltroRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-3">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Alerta.validarFormularioBusquedaDefinicionAlerta())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>

          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="IdAlertaDefinicion" HeaderText="ID" SortExpression="IdAlertaDefinicion" HeaderStyle-Width="120px" />
              <asp:BoundField DataField="NombreAlerta" HeaderText="Nombre" SortExpression="NombreAlerta" />
              <asp:BoundField DataField="GestionarPorCemtra" HeaderText="Gestionar por CEMTRA" SortExpression="GestionarPorCemtra" />
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="110px" />
                <ItemTemplate>
                  <input type="hidden" id="txtIdAlertaDefinicion" runat="server" value='<%# Bind("IdAlertaDefinicion") %>' />
                  <input type="hidden" id="txtTieneRegistrosAsociados" runat="server" value='<%# Bind("TieneRegistrosAsociados") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>      
      </div>

    </asp:Panel>
  </div>

</asp:Content>
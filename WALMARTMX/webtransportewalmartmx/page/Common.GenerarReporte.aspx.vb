﻿Imports System.Data
Imports CapaNegocio
Imports Syncfusion.XlsIO

Public Class Common_GenerarReporte
  Inherits System.Web.UI.Page

  Public Const FORMULARIO_SUPERVISOR_REPORTE_LISTADO As String = "Supervisor.ReporteListado.aspx"
  Public Const FORMULARIO_OT_ALERTA_ROJA_LISTADO As String = "OT.AlertaRojaListado.aspx"
  Public Const FORMULARIO_REVISAR_PROGRAMACION As String = "Planificacion.RevisarProgramacion.aspx"
  Public Const FORMULARIO_DISCREPANCIA_BUSCADOR As String = "Discrepancia.Buscador.aspx"
  Public Const FORMULARIO_DISCREPANCIA_DETALLE As String = "Discrepancia.Detalle.aspx"
  Public Const FORMULARIO_VIAJE_IMPORTACION_LISTADO As String = "GestionOperacion.AsignacionViajeImportacionListado.aspx"
  Public Const FORMULARIO_VIAJE_EXPORTACION_LISTADO As String = "GestionOperacion.AsignacionViajeExportacionListado.aspx"

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim ds As New DataSet
    Dim dsReporte As New DataSet
    Dim dr As DataRow
    Dim workbook As IWorkbook = Nothing
    Dim idReporte As String = Utilidades.IsNull(Server.UrlDecode(Request("idReporte")), "-1")
    Dim invocadoDesde As String = Utilidades.IsNull(Server.UrlDecode(Request("invocadoDesde")), "Home.Menu.aspx")
    Dim respuesta, llaveReporte As String
    Dim procedimientoAlmacenado As String = ""
    Dim nombreHojas As String() = Nothing
    Dim indiceTablas As Integer() = Nothing

    'limpia el cache del navegador
    oUtilidades.LimpiarCache()
    Try
      Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"

      Select Case invocadoDesde
        Case FORMULARIO_SUPERVISOR_REPORTE_LISTADO
          ds = Reporte.ObtenerDetalle(idReporte)
          dr = ds.Tables(0).Rows(0)
          llaveReporte = dr.Item("Llave")
          procedimientoAlmacenado = dr.Item("ProcedimientoAlmacenado")
          dsReporte = oUtilidades.ObtenerObjectSession(Reporte.KEY_SESION_DATASET)

        Case FORMULARIO_OT_ALERTA_ROJA_LISTADO
          llaveReporte = Utilidades.IsNull(Server.UrlDecode(Request("llaveReporte")), "")
          ds = Reporte.ObtenerDetallePorLLave(llaveReporte)
          dr = ds.Tables(0).Rows(0)
          procedimientoAlmacenado = dr.Item("ProcedimientoAlmacenado")
                    'dsReporte = Reporte.ObtenerDatosGenericos(procedimientoAlmacenado, "-1", "-1", "usuario")
                    dsReporte = Reporte.ObtenerDatosGenericos(procedimientoAlmacenado, "-1", "-1")

                Case FORMULARIO_REVISAR_PROGRAMACION
          llaveReporte = Utilidades.IsNull(Server.UrlDecode(Request("llaveReporte")), "")

          'obtiene valores de los controles
          Dim idUsuario = Utilidades.IsNull(Server.UrlDecode(Request("idUsuario")), "-1")
          Dim nroTransporte = Utilidades.IsNull(Server.UrlDecode(Request("nroTransporte")), "-1")
          Dim fechaPresentacion = Utilidades.IsNull(Server.UrlDecode(Request("fechaPresentacion")), "-1")
          Dim patenteTracto = Utilidades.IsNull(Server.UrlDecode(Request("patenteTracto")), "-1")
          Dim patenteTrailer = Utilidades.IsNull(Server.UrlDecode(Request("patenteTrailer")), "-1")
          Dim carga = Utilidades.IsNull(Server.UrlDecode(Request("carga")), "-1")
          dsReporte = Reporte.ObtenerProgramacion(idUsuario, nroTransporte, fechaPresentacion, patenteTracto, patenteTrailer, carga)

        Case FORMULARIO_DISCREPANCIA_BUSCADOR
          Dim idUsuario = oUsuario.Id
          Dim nroTransporte = Utilidades.IsNull(Server.UrlDecode(Request("nroTransporte")), "-1")
          Dim codigoLocal = Utilidades.IsNull(Server.UrlDecode(Request("codigoLocal")), "-1")
          Dim seccion = Utilidades.IsNull(Server.UrlDecode(Request("seccion")), "-1")
          Dim codigoCentroDistribucion = Utilidades.IsNull(Server.UrlDecode(Request("codigoCentroDistribucion")), "-1")
          Dim dm = Utilidades.IsNull(Server.UrlDecode(Request("dm")), "-1")
          Dim fechaContableDesde = Utilidades.IsNull(Server.UrlDecode(Request("fechaContableDesde")), "-1")
          Dim fechaContableHasta = Utilidades.IsNull(Server.UrlDecode(Request("fechaContableHasta")), "-1")
          Dim resolucionFinal = Utilidades.IsNull(Server.UrlDecode(Request("resolucionFinal")), "-1")
          Dim carga = Utilidades.IsNull(Server.UrlDecode(Request("carga")), "-1")
          Dim diferenciaDias = Utilidades.IsNull(Server.UrlDecode(Request("diferenciaDias")), "-1")
          Dim soloFocoTablet = Utilidades.IsNull(Server.UrlDecode(Request("soloFocoTablet")), "0")
          llaveReporte = Utilidades.IsNull(Server.UrlDecode(Request("llaveReporte")), "")
          dsReporte = Discrepancia.ObtenerListadoExcel(procedimientoAlmacenado, idUsuario, codigoLocal, seccion, codigoCentroDistribucion, dm, fechaContableDesde, fechaContableHasta, resolucionFinal, nroTransporte, carga, diferenciaDias, soloFocoTablet)

        Case FORMULARIO_DISCREPANCIA_DETALLE
          llaveReporte = Utilidades.IsNull(Server.UrlDecode(Request("llaveReporte")), "")
          Dim nroTransporte = Utilidades.IsNull(Server.UrlDecode(Request("nroTransporte")), "-1")
          Dim codigoLocal = Utilidades.IsNull(Server.UrlDecode(Request("codigoLocal")), "-1")
          dsReporte = Discrepancia.ObtenerHistorialAlertas(procedimientoAlmacenado, nroTransporte, codigoLocal)

        Case FORMULARIO_VIAJE_IMPORTACION_LISTADO, FORMULARIO_VIAJE_EXPORTACION_LISTADO
          llaveReporte = Utilidades.IsNull(Server.UrlDecode(Request("llaveReporte")), "")
          dsReporte = oUtilidades.ObtenerObjectSession(Reporte.KEY_SESION_DATASET)

        Case Else
          Throw New Exception("No tiene llave asociada el reporte")
      End Select

      Select Case llaveReporte
        Case "Generico", "AlertasRojasCerradas"
          respuesta = ReporteGenerico(workbook, dsReporte)
        Case "Programacion"
          nombreHojas = {"Programacion"}
          respuesta = ReporteGenerico(workbook, dsReporte, nombreHojas)
        Case "DiscrepanciaBuscador"
          nombreHojas = {"Discrepancias"}
          respuesta = ReporteGenerico(workbook, dsReporte, nombreHojas)
        Case "HistorialAlertasDiscrepancia"
          nombreHojas = {"TOTAL ALERTAS", "DETALLE ALERTAS"}
          respuesta = ReporteGenerico(workbook, dsReporte, nombreHojas)
        Case "ViajeImportacionBuscador"
          nombreHojas = {"VIAJE IMPORTACION"}
          indiceTablas = {1}
          respuesta = ReporteTablasEspecificas(workbook, dsReporte, nombreHojas, indiceTablas)
        Case "ViajeExportacionBuscador"
          nombreHojas = {"VIAJE EXPORTACION"}
          indiceTablas = {1}
          respuesta = ReporteTablasEspecificas(workbook, dsReporte, nombreHojas, indiceTablas)
        Case Else
          Throw New Exception("No tiene llave asociada el reporte")
      End Select
      Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "1"

      Sistema.GrabarLogSesion(oUsuario.Id, "Genera reporte: " & procedimientoAlmacenado)
    Catch ex As Exception
      Session(Reporte.KEY_SESION_MENSAJE) = "{ERROR}Error en Page_Load(). " & ex.Message
      Session(Reporte.KEY_SESION_REPORTE_GENERADO) = "0"
    End Try

    'redirecciona pagina
    Utilidades.RegistrarScript(Me.Page, "top.location.href=""../page/" & invocadoDesde & "?idReporte=" & idReporte & """;", Utilidades.eRegistrar.FINAL, "pageLoad", False)
  End Sub

#Region "Privado"
  ''' <summary>
  ''' genera la hoja del excel con los datos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GenerarHojaExcel(ByRef workbook As IWorkbook, ByRef sheet As IWorksheet, ByVal dsConsultasSql As DataSet, Optional ByVal indiceTablaDataset As Integer = 0)
    Dim indiceFila As Integer = 1
    Dim indiceColumna As Integer = 1

    Try
      sheet.ImportDataTable(dsConsultasSql.Tables(indiceTablaDataset), True, 1, 1)

      With sheet.Range(1, 1, 1, dsConsultasSql.Tables(indiceTablaDataset).Columns.Count).CellStyle
        .Font.Bold = True
      End With

      sheet.UsedRange.AutofitColumns()
      sheet.IsGridLinesVisible = True
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

  ''' <summary>
  ''' lanza el archivo en pantalla para abrirlo / guardarlo
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GenerarArchivoExcel(ByVal nombreArchivo As String, ByVal workbook As IWorkbook, ByRef excelEngine As ExcelEngine)
    Try
      Response.Clear()
      Response.ClearHeaders()
      Response.Buffer = True
      Response.AddHeader("Cache-control", "public")
      Response.ContentEncoding = Encoding.Default

      workbook.SaveAs(nombreArchivo & ".xls", Response, ExcelDownloadType.PromptDialog, ExcelHttpContentType.Excel97)
      workbook.Close()
      excelEngine.Dispose()
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

#End Region

#Region "ReporteGenerico"
  ''' <summary>
  ''' obtiene reporte que tiene como filtro datos comunes (fechaDesde, fechaHasta, entre otros)
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ReporteGenerico(ByVal workbook As IWorkbook, ByVal ds As DataSet, Optional nombreHojas As String() = Nothing) As String
    Dim retorno As String = ""
    Dim excelEngine As ExcelEngine = New ExcelEngine()
    Dim application As IApplication = excelEngine.Excel
    Dim sheet As IWorksheet
    Dim totalRegistros, totalTablas As Integer
    Dim nombreArchivo As String = ""

    Try
      If ds Is Nothing Then
        totalRegistros = 0
      Else
        totalRegistros = ds.Tables(0).Rows.Count
      End If

      If totalRegistros > 0 Then
        nombreArchivo = Archivo.CrearNombreUnicoArchivo(Reporte.NombreEntidad)
        totalTablas = ds.Tables.Count

        If (Not nombreHojas Is Nothing) Then
          workbook = application.Workbooks.Create(nombreHojas)
          For i = 0 To nombreHojas.Length - 1
            sheet = workbook.Worksheets(nombreHojas(i))
            GenerarHojaExcel(workbook, sheet, ds, i)
          Next
        Else
          workbook = application.Workbooks.Create(totalTablas)

          For i = 0 To totalTablas - 1
            sheet = workbook.Worksheets(i)
            GenerarHojaExcel(workbook, sheet, ds, i)
          Next
        End If

        GenerarArchivoExcel(nombreArchivo, workbook, excelEngine)
        retorno = Sistema.eCodigoSql.Exito
      Else
        retorno = Sistema.eCodigoSql.SinDatos
        Session(Reporte.KEY_SESION_MENSAJE) = "No se encontraron registros"
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
      Session(Reporte.KEY_SESION_MENSAJE) = "{ERROR}Error en ReporteGenerico(). " & ex.Message
    End Try
    Return retorno
  End Function
#End Region

#Region "ReporteTablasEspecificas"
  ''' <summary>
  ''' obtiene reporte de tablas especificas
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ReporteTablasEspecificas(ByVal workbook As IWorkbook, ByVal ds As DataSet, ByVal nombreHojas As String(), ByVal indiceTablas As Integer()) As String
    Dim retorno As String = ""
    Dim excelEngine As ExcelEngine = New ExcelEngine()
    Dim application As IApplication = excelEngine.Excel
    Dim sheet As IWorksheet
    Dim totalRegistros, indice As Integer
    Dim nombreArchivo As String = ""

    Try
      If ds Is Nothing Then
        totalRegistros = 0
      Else
        totalRegistros = ds.Tables(0).Rows.Count
      End If

      If totalRegistros > 0 Then
        nombreArchivo = Archivo.CrearNombreUnicoArchivo(Reporte.NombreEntidad)

        workbook = application.Workbooks.Create(nombreHojas)
        For i = 0 To nombreHojas.Length - 1
          sheet = workbook.Worksheets(nombreHojas(i))
          indice = indiceTablas(i)
          GenerarHojaExcel(workbook, sheet, ds, indice)
        Next

        GenerarArchivoExcel(nombreArchivo, workbook, excelEngine)
        retorno = Sistema.eCodigoSql.Exito
      Else
        retorno = Sistema.eCodigoSql.SinDatos
        Session(Utilidades.KEY_SESION_MENSAJE) = "{SIN_DATOS}No se encontraron registros para exportar"
      End If
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
      Session(Utilidades.KEY_SESION_MENSAJE) = "{ERROR}Error en ReporteTablasEspecificas(). " & ex.Message
    End Try
    Return retorno
  End Function
#End Region

End Class
﻿Imports CapaNegocio
Imports System.Data

Public Class OT_RespuestaAlertaRojaDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Grabar
    FinalizarPlanAccion
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idAlertaRojaEstadoActual As String = Me.ViewState.Item("idAlertaRojaEstadoActual")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")

    Try
      ds = Alerta.ObtenerDetalleRespuestaAlertaRoja(nroTransporte, idAlertaRojaEstadoActual, Alerta.ENTIDAD_ALERTA_ROJA_RESPUESTA)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim html = ""
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim nombreTransportista As String = Me.ViewState.Item("nombreTransportista")
    Dim totalRegistros As Integer
    Dim dt As DataTable
    Dim dr As DataRow
    Dim htmlSinRegistro As String = "<div class=""help-block text-center""><em>No tiene alertas asociadas</em></div>"
    Dim alertaRojaEstado, alertaRojaObservacionEstado, montoARecuperar, montoRecuperado, montoDiscrepancia As String

    alertaRojaEstado = ""
    alertaRojaObservacionEstado = ""
    montoDiscrepancia = 0
    montoARecuperar = -1
    montoRecuperado = -1

    Try
      If ds Is Nothing Then
        totalRegistros = 0
      Else
        dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T00_ListadoAlertas)
        totalRegistros = dt.Rows.Count

        dr = dt.Rows(0)
        montoDiscrepancia = dr.Item("MontoDiscrepancia")

        dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T03_EstadoAlertaRoja)
        dr = dt.Rows(0)
        alertaRojaEstado = dr.Item("alertaRojaEstado")
        alertaRojaObservacionEstado = dr.Item("alertaRojaObservacionEstado")
        montoARecuperar = dr.Item("montoUnoTransportista")
        montoRecuperado = dr.Item("montoDosTransportista")
      End If
      Me.ViewState.Add("alertaRojaEstado", alertaRojaEstado)

      If (totalRegistros = 0) Then
        html = htmlSinRegistro
      Else
        html = Me.ConstruirListadoAlertas(ds)
      End If
    Catch ex As Exception
      html = ""
    End Try

    'asigna valores
    Me.lblNombreTransportista.Text = nombreTransportista
    Me.lblIdRegistro.Text = "[IdMaster: " & nroTransporte & "&nbsp;&nbsp;/&nbsp;&nbsp;Monto Discrepancia: $" & montoDiscrepancia & "]"
    Me.ltlListado.Text = html

    Me.txtObservacion.Text = alertaRojaObservacionEstado
    Me.txtMontoUnoTransportista.Text = IIf(montoARecuperar = -1, montoDiscrepancia, montoARecuperar)
    Me.txtMontoDosTransportista.Text = IIf(montoRecuperado = -1, "", montoRecuperado)

    Me.txtObservacion.Enabled = IIf(alertaRojaEstado = Alerta.ESTADO_ALERTA_ROJA_FINALIZADO OrElse alertaRojaEstado = Alerta.ESTADO_ALERTA_ROJA_RECHAZADO, False, True)
    Me.txtMontoUnoTransportista.Enabled = Me.txtObservacion.Enabled
    Me.txtMontoDosTransportista.Enabled = Me.txtObservacion.Enabled

    Me.btnFinalizarPlanAccion.Visible = IIf(alertaRojaEstado = Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION, True, False)
    Me.btnGrabar.Visible = IIf(alertaRojaEstado = Alerta.ESTADO_ALERTA_ROJA_FINALIZADO OrElse alertaRojaEstado = Alerta.ESTADO_ALERTA_ROJA_RECHAZADO, False, Not Me.btnFinalizarPlanAccion.Visible)

    'dibuja los estados del transportista sugeridos
    Me.DibujarEstadoTransportista(ds, alertaRojaEstado, Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION)
    Me.DibujarEstadoTransportista(ds, alertaRojaEstado, Alerta.ESTADO_ALERTA_ROJA_FINALIZADO)

    'carga los item del multiselect
    Me.CargaMultiselectEstadoTransportista(ds, alertaRojaEstado)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                           Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Grabar.ToString)
    Me.btnFinalizarPlanAccion.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                                        Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.FinalizarPlanAccion.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String, ByVal estadoAlertaRoja As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim idAlertaRojaEstadoActual As String = Me.ViewState.Item("idAlertaRojaEstadoActual")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim nombreTransportista As String = Me.ViewState.Item("nombreTransportista")
    Dim rutTransportista As String = Me.ViewState.Item("rutTransportista")
    Dim statusGrabar, listadoIdEstados, observacion, montoUnoTransportista, montoDosTransportista As String
    Dim idAlerta, respuesta As String
    Dim objJSON As Object

    Try
      listadoIdEstados = Me.msEstado.ValueListadoId
      observacion = Me.txtObservacion.Text
      montoUnoTransportista = Utilidades.IsNull(Me.txtMontoUnoTransportista.Text, "0")
      montoDosTransportista = Utilidades.IsNull(Me.txtMontoDosTransportista.Text, "0")

      If (hRechazado.Value = 1) Then
        statusGrabar = Alerta.GrabarEstadoAlertaRoja(idAlertaRojaEstadoActual, oUsuario.Id, nroTransporte, Alerta.ESTADO_ALERTA_ROJA_RECHAZADO)
        If (statusGrabar = Sistema.eCodigoSql.Error) Then Throw New Exception("Error al grabar estado alerta roja")

        'Obtengo el JSON con rechazos
        objJSON = MyJSON.ConvertJSONToObject(hJsonRechazado.Value)

        For Each row In objJSON
          idAlerta = MyJSON.ItemObject(row, "idAlerta")
          respuesta = MyJSON.ItemObject(row, "respuesta")

          status = Alerta.GrabarRespuestaAlertaRoja(oUsuario.Id, nroTransporte, idAlerta, respuesta, True)
          If (status = Sistema.eCodigoSql.Error) Then Throw New Exception("No se pueden grabar las respuestas de rechazo")
        Next
      Else
        statusGrabar = Alerta.GrabarEstadoAlertaRoja(idAlertaRojaEstadoActual, oUsuario.Id, nroTransporte, estadoAlertaRoja)
        If (statusGrabar = Sistema.eCodigoSql.Error) Then Throw New Exception("Error al grabar estado alerta roja")

        statusGrabar = Alerta.GrabarEstadoTransportista(idAlertaRojaEstadoActual, rutTransportista, listadoIdEstados, observacion, montoUnoTransportista, montoDosTransportista, estadoAlertaRoja)
        If (statusGrabar = Sistema.eCodigoSql.Error) Then Throw New Exception("Error al grabar estado de la Línea de Transporte")
      End If

      Sistema.GrabarLogSesion(oUsuario.Id, "Graba respuesta alerta roja: IdMaster " & nroTransporte & " - Línea de Transporte " & nombreTransportista)
      status = "modificado"

    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la alerta roja.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro(ByVal estadoAlertaRoja As String)
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaRojaEstadoActual As String = Me.ViewState.Item("idAlertaRojaEstadoActual")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim nombreTransportista As String = Me.ViewState.Item("nombreTransportista")
    Dim rutTransportista As String = Me.ViewState.Item("rutTransportista")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosRegistro(status, estadoAlertaRoja)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      If (estadoAlertaRoja = Alerta.ESTADO_ALERTA_ROJA_FINALIZADO) Then
        InformeMail.RespuestaAlertaRoja(nroTransporte, idAlertaRojaEstadoActual)
      End If

      'muestra mensaje dependiendo si se grabo o no
      If grabadoConExito Then
        Select Case status
          Case "ingresado"
            textoMensaje = "{INGRESADO}La alerta roja fue ingresada satisfactoriamente"
          Case "modificado"
            textoMensaje = "{MODIFICADO}La alerta roja fue actualizada satisfactoriamente"
          Case "error"
            textoMensaje = "{ERROR}Se produjo un error interno y no se pudo grabar la alerta roja. Intente nuevamente por favor"
        End Select
      Else
        textoMensaje = "{ERROR}Se produjo un error interno y no se pudo grabar la alerta roja. Intente nuevamente por favor.<br />" & textoMensaje
      End If
      Session(Utilidades.KEY_SESION_MENSAJE) = "[IdMaster: " & nroTransporte & "] " & textoMensaje

      If status = "ingresado" Or status = "modificado" Then
        Utilidades.RegistrarScript(Me.Page, "Alerta.cerrarPopUpMantenedor('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
      Else
        queryStringPagina = "idAlertaRojaEstadoActual=" & idAlertaRojaEstadoActual
        queryStringPagina = "&nroTransporte=" & nroTransporte
        queryStringPagina = "&nombreTransportista=" & Server.UrlEncode(nombreTransportista)
        queryStringPagina = "&rutTransportista=" & Server.UrlEncode(rutTransportista)
        Response.Redirect("./OT.RespuestaAlertaRojaDetalle.aspx?" & queryStringPagina)
      End If
    End If
  End Sub

  ''' <summary>
  ''' Construye tabla HTML con las alertas
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <remarks></remarks>
  Private Function ConstruirListadoAlertas(ByVal ds As DataSet) As String
    Dim oUtilidades As New Utilidades
    Dim html As String = ""
    Dim dt As DataTable
    Dim dr As DataRow
    Dim totalRegistros, i As Integer
    Dim idAlerta, nombreAlerta, nombreConductor, patenteTracto, patenteTrailer, localOrigenCodigo, localOrigenDescripcion, localDestinoCodigo As String
    Dim localDestinoDescripcion, tipoViaje, fechaHoraCreacion, botonResponder, patente, alertaMapa As String
    Dim sb As New StringBuilder
    Try
      dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T00_ListadoAlertas)
      totalRegistros = dt.Rows.Count

      sb.AppendLine("<div>")
      sb.AppendLine("  <table class=""table table-condensed"">")
      sb.AppendLine("    <thead>")
      sb.AppendLine("      <tr>")
      sb.AppendLine("        <th style=""width:90px;"">IdAlerta</th>")
      sb.AppendLine("        <th>Alerta</th>")
      sb.AppendLine("        <th>Fecha Alerta</th>")
      sb.AppendLine("        <th>Operador</th>")
      sb.AppendLine("        <th>Placa</th>")
      sb.AppendLine("        <th>Tienda Origen</th>")
      sb.AppendLine("        <th>Tienda Destino</th>")
      sb.AppendLine("        <th>Tipo Viaje</th>")
      sb.AppendLine("        <th style=""width:120px"">Opciones</th>")
      sb.AppendLine("      </tr>")
      sb.AppendLine("    </thead>")
      sb.AppendLine("    <tbody>")

      For i = 0 To totalRegistros - 1
        dr = dt.Rows(i)
        idAlerta = dr.Item("idAlerta")
        nombreAlerta = dr.Item("nombreAlerta")
        nombreConductor = dr.Item("nombreConductor")
        patenteTracto = dr.Item("patenteTracto")
        patenteTrailer = dr.Item("patenteTrailer")
        localOrigenCodigo = dr.Item("localOrigenCodigo")
        localOrigenDescripcion = dr.Item("localOrigenDescripcion")
        localDestinoCodigo = dr.Item("localDestinoCodigo")
        localDestinoDescripcion = dr.Item("localDestinoDescripcion")
        tipoViaje = dr.Item("tipoViaje")
        fechaHoraCreacion = dr.Item("fechaHoraCreacion")
        alertaMapa = dr.Item("alertaMapa")

        botonResponder = "<div id=""" & idAlerta & "_btnVer"" class=""btn btn-success btn-sm sist-width-100-porciento"" onclick=""Alerta.expandirContraer({ elem_holderComentario:'" & idAlerta & "_holderComentario', idAlerta:'" & idAlerta & "' });"" /><span class=""glyphicon glyphicon-eye-open""></span>&nbsp; Ver respuesta</div>"
        patente = IIf(String.IsNullOrEmpty(patenteTracto), "", "Tracto: " & patenteTracto & "&nbsp;&nbsp;") & IIf(String.IsNullOrEmpty(patenteTrailer), "", "Remolque: " & patenteTrailer & "&nbsp;&nbsp;")
        localOrigenDescripcion = localOrigenDescripcion & IIf(String.IsNullOrEmpty(localOrigenCodigo), "", " - " & localOrigenCodigo)
        localDestinoDescripcion = localDestinoDescripcion & IIf(String.IsNullOrEmpty(localDestinoCodigo), "", " - " & localDestinoCodigo)
        alertaMapa = IIf(String.IsNullOrEmpty(alertaMapa), "", "<br /><a href=""" & alertaMapa & """ target=""_blank"">[Ver mapa]</a>")

        sb.AppendLine("   <tr>")
        sb.AppendLine("     <td>" & idAlerta & alertaMapa & "</td>")
        sb.AppendLine("     <td>" & nombreAlerta & "</td>")
        sb.AppendLine("     <td>" & fechaHoraCreacion & "</td>")
        sb.AppendLine("     <td>" & nombreConductor & "</td>")
        sb.AppendLine("     <td>" & patente & "</td>")
        sb.AppendLine("     <td>" & localOrigenDescripcion & "</td>")
        sb.AppendLine("     <td>" & localDestinoDescripcion & "</td>")
        sb.AppendLine("     <td>" & tipoViaje & "</td>")
        sb.AppendLine("     <td style=""text-align:center"">" & botonResponder & "</td>")
        sb.AppendLine("   </tr>")
        sb.AppendLine("   <tr>")
        sb.AppendLine("     <td colspan=""9"" class=""sist-padding-cero"">" & ConstruirTablaDetalle(ds, idAlerta) & "</td>")
        sb.AppendLine("   </tr>")
      Next
      sb.AppendLine("   </tbody>")
      sb.AppendLine(" </table>")
      sb.AppendLine("</div>")

      'asigna texto
      html = sb.ToString()
    Catch ex As Exception
      html = "<div class=""alert alert-danger"">Ha ocurrido un error interno y no se pudo obtener el listado.</div>"
    End Try
    Return html
  End Function

  ''' <summary>
  ''' construye tabla con el detalle
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ''' 
  Private Function ConstruirTablaDetalle(ByVal ds As DataSet, ByVal idAlerta As String) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim tpl As String = "<div class=""sist-padding-expandir-contraer"" style=""display:none;"" id=""" & idAlerta & "_holderComentario"">{CONTENIDO}</div>"

    'Tabla de categorias
    sb.AppendLine(ConstruirTablaCategoriasPorAlerta(ds, idAlerta))

    'Tabla de respuestas
    sb.AppendLine(ConstruirTablaRespuesta(ds, idAlerta))

    html = sb.ToString()

    Return tpl.Replace("{CONTENIDO}", html)

  End Function

  ''' <summary>
  ''' construye tabla con las categorias de la alerta
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  ''' 
  Private Function ConstruirTablaCategoriasPorAlerta(ByVal ds As DataSet, ByVal idAlerta As String) As String
    Dim oUtilidades As New Utilidades
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<div class=""help-block text-center""><em>No tiene categor&iacute;as asociadas</em></div>"
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRowView
    Dim totalRegistros, totalCategorias, i As Integer
    Dim fechaGestion, categoriaAlerta, tipoObservacion, observacion As String
    Dim sb As New StringBuilder

    Try
      If ds Is Nothing Then
        html = htmlSinRegistro
      Else
        dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T01_ListadoCategoriasPorAlerta)
        totalRegistros = dt.Rows.Count
        If totalRegistros = 0 Then
          html = htmlSinRegistro
        Else
          dv = dt.DefaultView
          'filtra el contenido por el idAlerta
          dv.RowFilter = "IdAlerta='" & idAlerta & "'"
          totalCategorias = dv.Count

          If totalCategorias = 0 Then
            html = htmlSinRegistro
          Else
            sb.AppendLine("<div>")
            sb.AppendLine(" <table class=""table table-condensed table-bordered table-striped"">")
            sb.AppendLine("   <thead>")
            sb.AppendLine("     <tr>")
            sb.AppendLine("       <th>Fecha Gesti&oacute;n</th>")
            sb.AppendLine("       <th>Categor&iacute;a</th>")
            sb.AppendLine("       <th>Tipo Observaci&oacute;n</th>")
            sb.AppendLine("       <th>Observaci&oacute;n</th>")
            sb.AppendLine("     </tr>")
            sb.AppendLine("   </thead>")
            sb.AppendLine("   <tbody>")

            For i = 0 To totalCategorias - 1
              dr = dv.Item(i)
              fechaGestion = Utilidades.IsNull(dr.Item("fechaGestion"), "&nbsp;")
              categoriaAlerta = Utilidades.IsNull(dr.Item("categoriaAlerta"), "&nbsp;")
              tipoObservacion = Utilidades.IsNull(dr.Item("tipoObservacion"), "&nbsp;")
              observacion = Utilidades.IsNull(dr.Item("observacion"), "&nbsp;")

              sb.AppendLine("   <tr>")
              sb.AppendLine("     <td>" & fechaGestion & "</td>")
              sb.AppendLine("     <td>" & categoriaAlerta & "</td>")
              sb.AppendLine("     <td>" & tipoObservacion & "</td>")
              sb.AppendLine("     <td>" & observacion & "</td>")
              sb.AppendLine("   </tr>")
            Next
            sb.AppendLine("   </tbody>")
            sb.AppendLine(" </table>")
            sb.AppendLine("</div>")

            'asigna texto
            html = sb.ToString()
          End If
        End If
      End If
    Catch ex As Exception
      html = "<div class=""alert alert-danger"">Ha ocurrido un error interno y no se pudo obtener el listado.</div>"
    End Try
    Return html
  End Function

  Private Function ConstruirTablaRespuesta(ByVal ds As DataSet, ByVal idAlerta As String) As String
    Dim oUtilidades As New Utilidades
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<div class=""help-block text-center""><em>No tiene respuestas asociadas</em></div>"
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRowView
    Dim totalRegistros, totalCategorias, i As Integer
    Dim idAlertaRojaRespuesta, listadoArchivos, respuesta, fecha, rechazo As String
    Dim sb As New StringBuilder
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")
    Dim alertaRojaEstado As String = Me.ViewState.Item("alertaRojaEstado")

    Try
      If ds Is Nothing Then
        html = htmlSinRegistro
      Else
        dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T05_RespuestaTransportista)
        totalRegistros = dt.Rows.Count
        If totalRegistros = 0 Then
          html = htmlSinRegistro
        Else
            dv = dt.DefaultView
          'filtra el contenido por el idAlerta
            dv.RowFilter = "IdAlerta='" & idAlerta & "'"
          totalCategorias = dv.Count

          If totalCategorias = 0 Then
            html = htmlSinRegistro
          Else

            'Listado de respuesta
            '----------------------------------------------------
            sb.AppendLine("<div>")
            sb.AppendLine(" <table class=""table table-condensed table-bordered table-striped"">")
            sb.AppendLine("   <thead>")
            sb.AppendLine("     <tr>")
            sb.AppendLine("       <th>Respuesta</th>")
            sb.AppendLine("       <th>Fecha</th>")
            sb.AppendLine("       <th>Archivos adjuntos</th>")
            sb.AppendLine("     </tr>")
            sb.AppendLine("   </thead>")
            sb.AppendLine("   <tbody>")

            For i = 0 To totalCategorias - 1
              dr = dv.Item(i)

              idAlertaRojaRespuesta = Utilidades.IsNull(dr.Item("id"), 0)
              respuesta = Utilidades.IsNull(dr.Item("Observacion"), "&nbsp;")
              fecha = Utilidades.IsNull(dr.Item("FechaCreacion"), "&nbsp;")
            listadoArchivos = DibujarListadoArchivosAdjuntos(ds, idAlertaRojaRespuesta)
              rechazo = Utilidades.IsNull(dr.Item("Rechazo"), False)

              If (rechazo) Then
                rechazo = "<span class=""text-danger"" style=""font-weight:bold;"">(Rechazo)</span>"
              Else
                rechazo = ""
              End If

              sb.AppendLine("   <tr>")
              sb.AppendLine("     <td>" & respuesta & " " & rechazo & "</td>")
              sb.AppendLine("     <td>" & fecha & "</td>")
              sb.AppendLine("     <td>")
              sb.AppendLine("        <div>" & listadoArchivos & "</div>")
              sb.AppendLine("     </td>")
              sb.AppendLine("   </tr>")
            Next
            sb.AppendLine("   </tbody>")
            sb.AppendLine(" </table>")
            sb.AppendLine("</div>")

            'Rechazo de respuesta
            '----------------------------------------------------
            If (alertaRojaEstado = Alerta.ESTADO_ALERTA_ROJA_ENVIADO_OPERACION_TRANSPORTE) Then
              sb.AppendLine("<div>")
              sb.AppendLine("   <label class=""control-label"">Rechazo Respuesta</label>")
              sb.AppendLine("   <div class=""text-center"">")
              sb.AppendLine("      <textarea id=""txtObservacionR_" & idAlerta & """ rows=""2"" cols=""20"" class=""form-control""></textarea>")
              sb.AppendLine("      </br>")
              sb.AppendLine("      <div id=""btnRechazar_" & idAlerta & """ align=""center"" class=""btn btn-danger btn-sm sist-width-30-porciento"" onclick=""Alerta.rechazarRespuesta({control:'txtObservacionR_" & idAlerta & "', idAlerta:'" & idAlerta & "', nroTransporte:'" & nroTransporte & "' })"" /><span class=""glyphicon glyphicon-ban-circle""></span>&nbsp; Rechazar</div>")
              sb.AppendLine("   </div>")
              sb.AppendLine("</div>")
            End If

            'asigna texto
            html = sb.ToString()
          End If
        End If
      End If
    Catch ex As Exception
      html = "<div class=""alert alert-danger"">Ha ocurrido un error interno y no se pudo obtener el listado.</div>"
    End Try
    Return html
  End Function

  ''' <summary>
  ''' dibuja los archivos adjuntos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarListadoArchivosAdjuntos(ByVal ds As DataSet, ByVal idAlertaRojaRespuesta As String) As String
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<div class=""help-block""><em>No tiene archivos adjuntos asociadas</em></div>"
    Dim dt As DataTable
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim totalRegistros, tamano As Integer
    Dim linkArchivo, idArchivo, extension, contentType, nombreArchivo, queryString As String
    Dim sb As New StringBuilder

    Try
      dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T02_ArchivosAdjuntos)
      dv = dt.DefaultView
      dv.RowFilter = "IdAlertaRojaRespuesta='" & idAlertaRojaRespuesta & "'"
      dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      dv.RowFilter = ""
      totalRegistros = dsFiltrado.Tables(0).Rows.Count

      If (totalRegistros = 0) Then
        html = htmlSinRegistro
      Else
        For Each dr As DataRow In dsFiltrado.Tables(0).Rows
          linkArchivo = ""
          idArchivo = dr.Item("idArchivo")
          extension = dr.Item("extension")
          contentType = dr.Item("contentType")
          nombreArchivo = dr.Item("nombre")
          tamano = dr.Item("tamano")

          queryString = Criptografia.EncriptarTripleDES("id=" & idArchivo)
          linkArchivo &= "<div>"
          linkArchivo &= "  <img src=""" & Utilidades.ObtenerRutaIconoTipoArchivo(extension, contentType) & """ alt=""" & contentType & """ />&nbsp;&nbsp;<a href=""../page/Common.VerArchivo.aspx?a=" & queryString & """>" & nombreArchivo & "&nbsp;&nbsp;(" & Utilidades.ConvertirUnidadMedidaArchivo(tamano, True) & ")</a>"
          linkArchivo &= "</div>"
          sb.AppendLine(linkArchivo)
        Next

        html = sb.ToString()
      End If
    Catch ex As Exception
      html = htmlSinRegistro
    End Try

    Return html
  End Function

  ''' <summary>
  ''' carga los item del multiselect
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CargaMultiselectEstadoTransportista(ByVal ds As DataSet, ByVal alertaRojaEstado As String)
    Dim dt As DataTable
    Dim dv As DataView
    Dim dsEstadoTransportistaPadre As New DataSet
    Dim dsEstadoTransportistaHija As New DataSet
    Dim descripcion As String = ""

    Try
      'actualiza el multiselect
      If Not ds Is Nothing Then
        dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T04_EstadoTransportista)
        dv = dt.DefaultView
        Select Case alertaRojaEstado
          Case Alerta.ESTADO_ALERTA_ROJA_ENVIADO_OPERACION_TRANSPORTE
            descripcion = "Seleccione uno o m&aacute;s estados como <em><strong>propuestas</strong></em> para ser aprobados posteriormente."
            dv.RowFilter = "Sugerida = 0"
            dsEstadoTransportistaPadre = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

            dv.RowFilter = "Sugerida = -1" 'para obtener un dataset vacio
            dsEstadoTransportistaHija = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

          Case Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION
            descripcion = "Seleccione uno o m&aacute;s estados que ser&aacute;n <strong>aplicados finalmente</strong> como plan de acci&oacute;n."
            dv.RowFilter = "Sugerida = 0"
            dsEstadoTransportistaPadre = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

            dv.RowFilter = "Sugerida = 1"
            dsEstadoTransportistaHija = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

          Case Alerta.ESTADO_ALERTA_ROJA_FINALIZADO
            dv.RowFilter = "Aprobada = 0"
            dsEstadoTransportistaPadre = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

            dv.RowFilter = "Aprobada = 1"
            dsEstadoTransportistaHija = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""
          Case Else
            dv.RowFilter = "Sugerida = -1" 'para obtener un dataset vacio
            dsEstadoTransportistaPadre = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

            dv.RowFilter = "Sugerida = -1" 'para obtener un dataset vacio
            dsEstadoTransportistaHija = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""
        End Select

        With Me.msEstado
          .DataSourcePadre = dsEstadoTransportistaPadre.Tables(0)
          .DataTextFieldPadre = "Texto"
          .DataValueFieldPadre = "Id"
          .DataBindPadre()
          .DataSourceHija = dsEstadoTransportistaHija.Tables(0)
          .DataTextFieldHija = "Texto"
          .DataValueFieldHija = "Id"
          .DataBindHija()
        End With

        Me.pnlEstadoTransportistaMultiselect.Visible = IIf(alertaRojaEstado = Alerta.ESTADO_ALERTA_ROJA_FINALIZADO OrElse alertaRojaEstado = Alerta.ESTADO_ALERTA_ROJA_RECHAZADO, False, True)
        Me.lblDescripcionEstadoTransportistaMultiselect.Text = descripcion
      End If
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub

  ''' <summary>
  ''' dibuja los estado de transportistas sugeridos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub DibujarEstadoTransportista(ByVal ds As DataSet, ByVal alertaRojaEstadoActual As String, ByVal estadoAlertaDibujar As String)
    Dim html As String = ""
    Dim dt As DataTable
    Dim dsEstadoTransportistaPadre As New DataSet
    Dim dsEstadoTransportistaHija As New DataSet
    Dim sb As New StringBuilder
    Dim texto As String
    Dim listadoFiltrado As DataRow() = Nothing
    Dim fechaEstadoTransportista As String = ""

    Try
      If ds Is Nothing Then
        Me.pnlEstadoTransportistaSugeridos.Visible = False
        Me.pnlEstadoTransportistaAprobados.Visible = False
      Else
        dt = ds.Tables(Alerta.eTablaRespuestaAlerta.T04_EstadoTransportista)

        Select Case alertaRojaEstadoActual
          Case Alerta.ESTADO_ALERTA_ROJA_ENVIADO_OPERACION_TRANSPORTE
            Me.pnlEstadoTransportistaSugeridos.Visible = False
            Me.pnlEstadoTransportistaAprobados.Visible = False
          Case Else
            '----------------------------------------------------
            'obtiene los datos segun el estado consultado
            Select Case estadoAlertaDibujar
              Case Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION
                listadoFiltrado = dt.Select("Sugerida = 1")
                If (listadoFiltrado.Length > 0) Then fechaEstadoTransportista = listadoFiltrado(0).Item("FechaCreacionSugerida")
              Case Alerta.ESTADO_ALERTA_ROJA_FINALIZADO
                listadoFiltrado = dt.Select("Aprobada = 1")
                If (listadoFiltrado.Length > 0) Then fechaEstadoTransportista = listadoFiltrado(0).Item("FechaCreacionAprobada")
            End Select

            '----------------------------------------------------
            'dibuja los estados
            If (listadoFiltrado.Length = 0) Then
              If (String.IsNullOrEmpty(fechaEstadoTransportista)) Then
                sb.Append("<em class=""help-block"">Transporte cuenta con certificaci&oacute;n de carga, recepci&oacute;n y alertas. De acuerdo a protocolo, s&oacute;lo falta plan de acci&oacute;n definitivo de transporte</em>")
              Else
                sb.Append("<em class=""help-block"">No hay estados de L&iacute;neas de Transporte asociadas</em>")
              End If
            Else
              sb.Append("<ul>")
              For Each row As DataRow In listadoFiltrado
                texto = row.Item("Texto")
                sb.Append("<li>" & Server.HtmlEncode(texto) & "</li>")
              Next
              sb.Append("</ul>")
            End If
            html = sb.ToString()

            '----------------------------------------------------
            'asigna html al control asociado al estado consultado
            fechaEstadoTransportista = IIf(String.IsNullOrEmpty(fechaEstadoTransportista), "", "(el " & fechaEstadoTransportista & ")")
            Select Case estadoAlertaDibujar
              Case Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION
                Me.lblFechaCreacionSugerido.Text = fechaEstadoTransportista
                Me.ltlEstadoTransportistaSugerido.Text = html
                Me.pnlEstadoTransportistaSugeridos.Visible = (alertaRojaEstadoActual = Alerta.ESTADO_ALERTA_ROJA_FINALIZADO OrElse alertaRojaEstadoActual = Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION)
              Case Alerta.ESTADO_ALERTA_ROJA_FINALIZADO
                Me.lblFechaCreacionAprobado.Text = fechaEstadoTransportista
                Me.ltlEstadoTransportistaAprobado.Text = html
                Me.pnlEstadoTransportistaAprobados.Visible = alertaRojaEstadoActual = Alerta.ESTADO_ALERTA_ROJA_FINALIZADO
            End Select

        End Select

      End If
    Catch ex As Exception
      Me.pnlEstadoTransportistaSugeridos.Visible = False
      Me.pnlEstadoTransportistaAprobados.Visible = False
      Exit Sub
    End Try
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idAlertaRojaEstadoActual As String = Utilidades.IsNull(Request("idAlertaRojaEstadoActual"), "-1")
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")
    Dim nombreTransportista As String = Utilidades.IsNull(Server.UrlDecode(Request("nombreTransportista")), "-1")
    Dim rutTransportista As String = Utilidades.IsNull(Server.UrlDecode(Request("rutTransportista")), "-1")

    Me.ViewState.Add("idAlertaRojaEstadoActual", idAlertaRojaEstadoActual)
    Me.ViewState.Add("nroTransporte", nroTransporte)
    Me.ViewState.Add("nombreTransportista", nombreTransportista)
    Me.ViewState.Add("rutTransportista", rutTransportista)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    GrabarRegistro(Alerta.ESTADO_ALERTA_ROJA_POR_APROBAR_PLAN_ACCION)
  End Sub

  Protected Sub btnFinalizarPlanAccion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalizarPlanAccion.Click
    GrabarRegistro(Alerta.ESTADO_ALERTA_ROJA_FINALIZADO)
  End Sub

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Buscador.Alertas.aspx.vb" Inherits="webtransportewalmartmx.Buscador_Alertas" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script language="javascript" type="text/javascript" src="../js/alerta.js"></script>

  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Alertas</p>
      </div>
    </div>            
  </div>
  <asp:Panel ID="pnlMensaje" Runat="server"></asp:Panel>

  <div>
    <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" Runat="server">

      <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3></div>
        <div class="panel-body">
          <div class="form-group row">
            <div class="col-xs-3">
              <label class="control-label">IdAlerta o Nombre Alerta</label>
              <asp:TextBox ID="txtNombre" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-3">
              <label class="control-label">IdMaster</label>
              <asp:TextBox ID="txtNroTransporte" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Formato</label>
              <uc1:wucCombo id="ddlFormato" runat="server" FuenteDatos="Tabla" TipoCombo="Formato" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-3">
              <label class="control-label">L&iacute;nea de Transporte</label>
              <asp:TextBox ID="txtTransportista" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-2">
              <label class="control-label">Tienda Destino</label>
              <asp:TextBox ID="txtLocalDestino" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Prioridad</label>
              <uc1:wucCombo id="ddlPrioridad" runat="server" FuenteDatos="Tabla" TipoCombo="Prioridad" ItemTodos="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Desde</label>
              <asp:TextBox ID="txtFechaDesde" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Hasta</label>
              <asp:TextBox ID="txtFechaHasta" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
              <div id="lblMensajeErrorFechaHasta" runat="server"></div>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Nro. Filas Listado</label>
              <asp:TextBox ID="txtRegistrosPorPagina" runat="server" CssClass="form-control" MaxLength="4"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="btnFiltrar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Alerta.validarFormularioBusqueda())"><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
            </div>
          </div>
        </div>
      </div>        

      <!-- RESULTADO BUSQUEDA -->
      <input type="hidden" id="txtCargaDesdePopUp" runat="server" value="0" />
      <div class="form-group">
			  <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlHolderGrilla" runat="server" CssClass="table-responsive" Width="100%" Visible="false">
          <div><strong><asp:Label ID="lblTotalRegistros" runat="server" Visible="false"></asp:Label></strong></div>

          <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" AllowPaging="True" AllowSorting="True" CssClass="table table-striped table-bordered small">
            <Columns>
              <asp:TemplateField HeaderText="#">
                <HeaderStyle Width="40px" />
                <ItemTemplate>
                  <asp:Label ID="lblIndiceFila" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="IdAlerta" HeaderText="ID" SortExpression="IdAlerta" HeaderStyle-Width="70px"/>
              <asp:BoundField DataField="Clasificacion" HeaderText="Clasificaci&oacute;n" SortExpression="Clasificacion" HeaderStyle-Width="100px" />
              <asp:BoundField DataField="NombreFormato" HeaderText="Formato" SortExpression="NombreFormato" />
              <asp:BoundField DataField="NombreAlerta" HeaderText="Alerta" SortExpression="NombreAlerta" />
              <asp:BoundField DataField="NroTransporte" HeaderText="IdMaster" SortExpression="NroTransporte" HeaderStyle-Width="80px" />
              <asp:TemplateField HeaderText="Prioridad" SortExpression="Prioridad" HeaderStyle-Width="65px">
                <ItemTemplate>
                  <asp:Label ID="lblPrioridad" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:BoundField DataField="NombreTransportista" HeaderText="L&iacute;nea de Transporte" SortExpression="NombreTransportista" />
              <asp:BoundField DataField="LocalDestino" HeaderText="Tienda Destino" SortExpression="LocalDestino" />
              <asp:BoundField DataField="TipoViaje" HeaderText="Tipo Viaje" SortExpression="TipoViaje" HeaderStyle-Width="80px" />
              <asp:BoundField DataField="Fecha" HeaderText="Fecha Creaci&oacute;n" SortExpression="Fecha" HeaderStyle-Width="145px" />
              <asp:TemplateField HeaderText="Escal. Realizados" SortExpression="EscalamientosRealizados" HeaderStyle-Width="80px">
                <ItemTemplate>
                  <asp:Label ID="lblEscalamientosRealizados" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
              <asp:TemplateField HeaderText="Opciones">
                <ItemStyle Wrap="True" Width="120px" />
                <ItemTemplate>
                  <input type="hidden" id="txtIdAlerta" runat="server" value='<%# Bind("IdAlerta") %>' />
                  <input type="hidden" id="txtPrioridad" runat="server" value='<%# Bind("Prioridad") %>' />
                  <input type="hidden" id="txtEscalamientosRealizados" runat="server" value='<%# Bind("EscalamientosRealizados") %>' />
                  <input type="hidden" id="txtAlertaMapa" runat="server" value='<%# Bind("AlertaMapa") %>' />
                  <asp:Label ID="lblOpciones" runat="server"></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>
            </Columns>
          </asp:GridView>
        </asp:Panel>      
      </div>

      <script type="text/javascript">
        jQuery(document).ready(function () {
          jQuery(".chosen-select").select2();

          jQuery(".date-pick").datepicker({
            changeMonth: true,
            changeYear: true
          });

        });
      </script>

    </asp:Panel>
  </div>

</asp:Content>

﻿Imports CapaNegocio
Imports System.Data

Public Class GestionOperacion_AsignacionViajeImportacionDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Grabar
  End Enum

  Private Enum eTabla As Integer
    T00_Detalle = 0
    T01_ListadoConductores = 1
  End Enum

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idImportacion As String = Utilidades.IsNull(Request("idImportacion"), "-1")

    Me.ViewState.Add("idImportacion", idImportacion)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnDesasignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDesasignar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idImportacion As String = Me.ViewState.Item("idImportacion")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim eliminadoConExito As Boolean = True
    Dim queryString As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= DesasignarConductor(status, idImportacion)

    If textoMensaje <> "" Then
      eliminadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If eliminadoConExito Then
      Select Case status
        Case "eliminado"
          textoMensaje = "{ELIMINADO}El Operador fue desasignado satisfactoriamente."
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo desasignar el Operador. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo desasignar el Operador. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    If (status = "eliminado") Then
      Utilidades.RegistrarScript(Me.Page, "GestionOperacion.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryString = "idImportacion=" & idImportacion
      Response.Redirect("./GestionOperacion.AsignacionViajeImportacionDetalle.aspx?" & queryString)
    End If
  End Sub

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idImportacion As String = Me.ViewState.Item("idImportacion")

    Try
      ds = GestionOperacion.ObtenerDetalleAsignacionViajeImportacion(idImportacion)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ActualizaInterfaz()
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim ordenServicio, fechaPresentacion, nombreCliente, dimension, retiro, puertoDescarga, diasLibres, nave, numeroContenedor, direccionDestino As String
    Dim estadoViaje As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    ordenServicio = ""
    fechaPresentacion = ""
    nombreCliente = ""
    dimension = ""
    retiro = ""
    puertoDescarga = ""
    diasLibres = ""
    nave = ""
    numeroContenedor = ""
    direccionDestino = ""
    estadoViaje = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(eTabla.T00_Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(eTabla.T00_Detalle).Rows(0)
          ordenServicio = dr.Item("NumeroOrdenServicio")
          fechaPresentacion = dr.Item("FechaHoraPresentacion")
          nombreCliente = dr.Item("NombreCliente")
          dimension = dr.Item("MedidaContenedor")
          retiro = dr.Item("DirectoIndirecto")
          puertoDescarga = dr.Item("NombrePuertoDescarga")
          diasLibres = dr.Item("DiasDemurrage")
          nave = dr.Item("NombreNave")
          numeroContenedor = dr.Item("NumeroContenedor")
          direccionDestino = dr.Item("DireccionEntregaContenedor")
          estadoViaje = dr.Item("EstadoViaje")
        End If
      End If
    Catch ex As Exception
      ordenServicio = ""
      fechaPresentacion = ""
      nombreCliente = ""
      dimension = ""
      retiro = ""
      puertoDescarga = ""
      diasLibres = ""
      nave = ""
      numeroContenedor = ""
      direccionDestino = ""
      estadoViaje = ""
    End Try

    'asigna valores
    Me.ltlTablaDatos.Text = DibujarConductores(ds, estadoViaje)
    Me.lblOrdenServicio.Text = ordenServicio
    Me.lblFechaPresentacion.Text = fechaPresentacion
    Me.lblNombreCliente.Text = nombreCliente
    Me.lblDimension.Text = dimension
    Me.lblRetiro.Text = retiro
    Me.lblPuertoDescarga.Text = puertoDescarga
    Me.lblDiasLibres.Text = diasLibres
    Me.lblNave.Text = nave
    Me.lblNroContenedor.Text = numeroContenedor
    Me.lblDireccionDestino.Text = direccionDestino
    Me.lblIdRegistro.Text = IIf(estadoViaje = "", "", "[Estado Viaje: " & estadoViaje & "]")

    Me.btnGrabar.Visible = IIf(estadoViaje = "" Or estadoViaje = GestionOperacion.ESTADO_VIAJE_ASIGNADO, Me.btnGrabar.Visible, False)
    Me.btnAsignarConductorManual.Visible = Me.btnGrabar.Visible

    'si el viaje aun no esta en ruta o en el local, entonces muestra boton desasignar segun la visibilidad que se le da dentro del metodo DibujarConductores(), sino se oculta
    Me.btnDesasignar.Visible = IIf(estadoViaje = "" Or estadoViaje = GestionOperacion.ESTADO_VIAJE_ASIGNADO, Me.btnDesasignar.Visible, False)

    If (Me.btnGrabar.Visible) Then
      sbScript.AppendLine("setTimeout(function () {")
      sbScript.AppendLine("  jQuery(""#tblListado"").dataTable({ bPaginate: false, bInfo: false, bSort: false });")
      sbScript.AppendLine("  GestionOperacion.seleccionarConductor({ prefijoControl: '0'});")
      sbScript.AppendLine("}, 100);")
      Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "actualizaInterfaz", True)
    End If
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                           Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Grabar.ToString)
    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim idImportacion As String = Me.ViewState.Item("idImportacion")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryString As String = ""

    'grabar valores
    textoMensaje &= GrabarDatosRegistro(status)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado", "modificado"
          textoMensaje = "{MODIFICADO}La asignaci&oacute;n de Operador fue realizada satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo asignar el Operador. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo asignar el Operador. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    If (status = "ingresado" Or status = "modificado") Then
      Utilidades.RegistrarScript(Me.Page, "GestionOperacion.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryString = "idImportacion=" & idImportacion
      Response.Redirect("./GestionOperacion.AsignacionViajeImportacionDetalle.aspx?" & queryString)
    End If
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosRegistro(ByRef status As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As New Usuario
    Dim msgError As String = ""
    Dim idImportacion As String = Me.ViewState.Item("idImportacion")
    Dim jsonDatosConductor, nombreConductor, rutConductor, telefono, patenteTracto, patenteTrailer, tipoCamion, fechaEntrada As String
    Dim origenDatoTabla, origenDatoIdRegistro As String
    Dim objJSON, row As Object

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      jsonDatosConductor = Me.txtJSONDatosConductor.Value
      objJSON = MyJSON.ConvertJSONToObject(jsonDatosConductor)
      row = objJSON(0)

      nombreConductor = MyJSON.ItemObject(row, "NombreConductor")
      rutConductor = MyJSON.ItemObject(row, "RutConductor")
      telefono = MyJSON.ItemObject(row, "Telefono")
      patenteTracto = MyJSON.ItemObject(row, "PatenteTracto")
      patenteTrailer = MyJSON.ItemObject(row, "PatenteTrailer")
      tipoCamion = MyJSON.ItemObject(row, "TipoCamion")
      fechaEntrada = MyJSON.ItemObject(row, "FechaEntrada")
      origenDatoTabla = MyJSON.ItemObject(row, "OrigenDatoTabla")
      origenDatoIdRegistro = MyJSON.ItemObject(row, "OrigenDatoIdRegistro")

      fechaEntrada = fechaEntrada.Replace(" hrs.", "")
      status = GestionOperacion.GrabarAsignarConductorImportacion(oUsuario.Id, idImportacion, nombreConductor, rutConductor, telefono, patenteTracto, patenteTrailer, tipoCamion, fechaEntrada, _
                                                                  origenDatoTabla, origenDatoIdRegistro)
      Sistema.GrabarLogSesion(oUsuario.Id, "Asigna Operador al viaje de importación: RutConductor " & rutConductor)
    Catch ex As Exception
      msgError = "Error al grabar la asignación del Operador.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' dibuja el listado de conductores de SCAT
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarConductores(ByVal ds As DataSet, ByVal estadoViaje As String) As String
    Dim html As String = ""
    Dim template As String = ""
    Dim totalRegistros As Integer
    Dim sb As New StringBuilder
    Dim templateAux, htmlSinRegistro, indice, nombreConductor, rutConductor, patentes, patenteTracto, patenteTrailer, telefono, tipoCamion, fechaEntrada As String
    Dim json, classDestacado, telefonoLabel, fechaLicenciaConducir, fechaRevisionTecnica, mensajeVencimiento, displayRadioButtonSeleccionar As String
    Dim reportabilidadPatenteTracto, reportabilidadPatenteTrailer, reportabilidadPatenteTractoFecha, reportabilidadPatenteTrailerFecha As String
    Dim dtDatoConductor As DataTable
    Dim fechaLicenciaConducirVencida, fechaRevisionTecnicaVencida As Boolean

    Try
      htmlSinRegistro = Utilidades.MostrarMensajeUsuario("No hay operadores asociados", Utilidades.eTipoMensajeAlert.Warning)

      If (estadoViaje = "" Or estadoViaje = GestionOperacion.ESTADO_VIAJE_ASIGNADO) Then
        template &= "<tr id=""{PREFIJO_CONTROL}_trFila"" data-type=""fila-conductor"" data-indice=""{INDICE}"" class=""{CLASS_DESTACADO}"">"
        template &= "  <td class=""text-center"">"
        template &= "{LABEL_INDICE}"
        template &= "<input type=""hidden"" id=""{PREFIJO_CONTROL}_txtJSONDatosConductor"" value=""{JSON}"" />"
        template &= "  </td>"
        template &= "  <td>{FECHA_ENTRADA}</td>"
        template &= "  <td>"
        template &= "   <div>{NOMBRE_CONDUCTOR}</div>"
        template &= "   <div>{MENSAJE_VENCIMIENTO}</div>"
        template &= "  </td>"
        template &= "  <td>{PATENTES}</td>"
        template &= "  <td class=""text-center"">"
        template &= "    <div id=""{PREFIJO_CONTROL}_lblTelefono"" onclick=""GestionOperacion.modificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Telefono' })"">{TELEFONO_LABEL}</div>"
        template &= "    <div id=""{PREFIJO_CONTROL}_hTelefono"" class=""sist-display-none"">"
        template &= "      <div><input type=""text"" id=""{PREFIJO_CONTROL}_txtTelefono"" value=""{TELEFONO_TEXTBOX}"" class=""form-control input-sm"" /></div>"
        template &= "      <div class=""text-center sist-padding-top-3"">"
        template &= "        <a href=""javascript:;"" onclick=""GestionOperacion.grabarModificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Telefono' })""><img src=""../img/ico-grabar.png"" alt="""" /></a>"
        template &= "        <a href=""javascript:;"" onclick=""GestionOperacion.cancelarModificarDatoFila({ prefijoControl:'{PREFIJO_CONTROL}', campo: 'Telefono' })""><img src=""../img/ico-cancelar.png"" alt="""" /></a>"
        template &= "      </div>"
        template &= "    </div>"
        template &= "  </td>"
        template &= "  <td class=""text-center"">{TIPO_CAMION}</td>"
        template &= "  <td class=""text-center""><input type=""radio"" id=""{PREFIJO_CONTROL}_rbtnSeleccionar"" name=""groupConductores"" value=""{INDICE}"" onclick=""GestionOperacion.seleccionarConductor({ prefijoControl: '{PREFIJO_CONTROL}' })"" class=""{DISPLAY_RADIOBUTTON_SELECCIONAR}"" /></td>"
        template &= "</tr>"
      Else
        template &= "<tr id=""{PREFIJO_CONTROL}_trFila"" data-type=""fila-conductor"" data-indice=""{INDICE}"">"
        template &= "  <td class=""text-center"">{LABEL_INDICE}</td>"
        template &= "  <td>{FECHA_ENTRADA}</td>"
        template &= "  <td>"
        template &= "   <div>{NOMBRE_CONDUCTOR}</div>"
        template &= "   <div>{MENSAJE_VENCIMIENTO}</div>"
        template &= "  </td>"
        template &= "  <td>{PATENTES}</td>"
        template &= "  <td class=""text-center"">{TELEFONO_LABEL}</td>"
        template &= "  <td class=""text-center"">{TIPO_CAMION}</td>"
        template &= "</tr>"
      End If

      If (ds Is Nothing) Then
        html = htmlSinRegistro
      Else
        totalRegistros = ds.Tables(eTabla.T01_ListadoConductores).Rows.Count

        If (totalRegistros = 0) Then
          html = htmlSinRegistro
        Else
          sb.Append("<table id=""tblListado"" class=""table table-condensed table-hover"">")
          sb.Append("  <thead>")
          sb.Append("    <tr>")
          sb.Append("      <th style=""width:100px"">Orden Llegada</th>")
          sb.Append("      <th style=""width:140px"">Fecha Entrada</th>")
          sb.Append("      <th style=""width:400px"">Nombre Operador</th>")
          sb.Append("      <th style=""width:270px"">Placas</th>")
          sb.Append("      <th style=""width:90px"">Tel&eacute;fono</th>")
          sb.Append("      <th style=""width:90px"">Tipo Equipo</th>")
          If (estadoViaje = "" Or estadoViaje = GestionOperacion.ESTADO_VIAJE_ASIGNADO) Then
            sb.Append("      <th style=""width:80px"" class=""text-center"">Seleccionar</th>")
          End If
          sb.Append("    </tr>")
          sb.Append("  </thead>")
          sb.Append("  <tbody>")

          For Each dr As DataRow In ds.Tables(eTabla.T01_ListadoConductores).Rows
            templateAux = template
            patentes = ""
            mensajeVencimiento = ""
            displayRadioButtonSeleccionar = ""
            indice = dr.Item("Indice")
            nombreConductor = dr.Item("NombreConductor")
            rutConductor = dr.Item("RutConductor")
            patenteTracto = dr.Item("PatenteTracto")
            patenteTrailer = dr.Item("PatenteTrailer")
            telefono = dr.Item("Telefono")
            tipoCamion = dr.Item("TipoCamion")
            fechaEntrada = dr.Item("FechaEntrada")
            fechaLicenciaConducir = dr.Item("FechaLicenciaConducir")
            fechaRevisionTecnica = dr.Item("FechaRevisionTecnica")
            reportabilidadPatenteTracto = dr.Item("ReportabilidadPatenteTracto")
            reportabilidadPatenteTractoFecha = dr.Item("ReportabilidadPatenteTractoFecha")
            reportabilidadPatenteTrailer = dr.Item("ReportabilidadPatenteTrailer")
            reportabilidadPatenteTrailerFecha = dr.Item("ReportabilidadPatenteTrailerFecha")

            'convierte datarow en json
            dtDatoConductor = New DataTable
            dtDatoConductor = ds.Tables(eTabla.T01_ListadoConductores).Clone()
            dtDatoConductor.ImportRow(dr)
            dtDatoConductor.AcceptChanges()
            json = MyJSON.ConvertDataTableToJSON(dtDatoConductor)

            'formatea valores
            If (String.IsNullOrEmpty(fechaLicenciaConducir)) Then
              fechaLicenciaConducirVencida = False
            Else
              fechaLicenciaConducirVencida = IIf(DateDiff(DateInterval.Day, Herramientas.MyNow(), Convert.ToDateTime(fechaLicenciaConducir)) < 0, True, False)
            End If

            If (String.IsNullOrEmpty(fechaRevisionTecnica)) Then
              fechaRevisionTecnicaVencida = False
            Else
              fechaRevisionTecnicaVencida = IIf(DateDiff(DateInterval.Day, Herramientas.MyNow(), Convert.ToDateTime(fechaRevisionTecnica)) < 0, True, False)
            End If

            reportabilidadPatenteTracto = Utilidades.ObtenerSemaforoReportabilidad(reportabilidadPatenteTracto, reportabilidadPatenteTractoFecha)
            reportabilidadPatenteTrailer = "" 'Utilidades.ObtenerSemaforoReportabilidad(reportabilidadPatenteTrailer, reportabilidadPatenteTrailerFecha)

            patentes &= IIf(String.IsNullOrEmpty(patentes), "<strong>Tracto</strong>: " & patenteTracto, "<br /><strong>Tracto</strong>: " & patenteTracto) & " " & reportabilidadPatenteTracto
            patentes &= IIf(String.IsNullOrEmpty(patentes), "<strong>Remolque</strong>: " & patenteTrailer, "<br /><strong>Remolque</strong>: " & patenteTrailer) & " " & reportabilidadPatenteTrailer
            nombreConductor = Server.HtmlEncode(nombreConductor) & IIf(Not String.IsNullOrEmpty(rutConductor), "<div class=""help-block sist-margin-cero sist-font-size-11""><em>Rut: " & rutConductor & "</em></div>", "")
            telefonoLabel = IIf(String.IsNullOrEmpty(telefono), GestionOperacion.SIN_TELEFONO, telefono)
            classDestacado = IIf(indice = "0", "sist-font-bold", "")
            classDestacado &= " " & IIf(fechaLicenciaConducirVencida Or fechaRevisionTecnicaVencida, "text-danger", "")

            If (fechaLicenciaConducirVencida Or fechaRevisionTecnicaVencida) Then
              displayRadioButtonSeleccionar = "sist-display-none"

              If (fechaLicenciaConducirVencida) Then
                mensajeVencimiento &= "<div class=""sist-padding-top-3 sist-font-bold""><em>- Licencia de conducir est&aacute; vencida (Fecha: " & fechaLicenciaConducir & ")</em></div>"
              End If

              If (fechaRevisionTecnicaVencida) Then
                mensajeVencimiento &= "<div class=""sist-padding-top-3 sist-font-bold""><em>- Revisi&oacute;n t&eacute;cnica est&aacute; vencida (Fecha: " & fechaRevisionTecnica & ")</em></div>"
              End If
            End If

            templateAux = templateAux.Replace("{INDICE}", indice)
            templateAux = templateAux.Replace("{LABEL_INDICE}", IIf(indice = "0", GestionOperacion.ESTADO_VIAJE_ASIGNADO, indice))
            templateAux = templateAux.Replace("{NOMBRE_CONDUCTOR}", nombreConductor)
            templateAux = templateAux.Replace("{MENSAJE_VENCIMIENTO}", mensajeVencimiento)
            templateAux = templateAux.Replace("{PATENTES}", patentes)
            templateAux = templateAux.Replace("{TELEFONO}", telefono)
            templateAux = templateAux.Replace("{TELEFONO_LABEL}", telefonoLabel)
            templateAux = templateAux.Replace("{TIPO_CAMION}", tipoCamion)
            templateAux = templateAux.Replace("{FECHA_ENTRADA}", fechaEntrada)
            templateAux = templateAux.Replace("{JSON}", Server.HtmlEncode(json))
            templateAux = templateAux.Replace("{CLASS_DESTACADO}", classDestacado)
            templateAux = templateAux.Replace("{DISPLAY_RADIOBUTTON_SELECCIONAR}", displayRadioButtonSeleccionar)
            templateAux = templateAux.Replace("{PREFIJO_CONTROL}", indice)
            sb.Append(templateAux)

            If (indice = "0") Then
              Me.txtRutConductorAsignado.Value = rutConductor
              Me.btnDesasignar.Visible = True
            End If

            'si el viaje ya esta en ruta o llego al local entonces se dibuja solo la primera fila con el conductor asignado
            If (estadoViaje <> "" And estadoViaje <> GestionOperacion.ESTADO_VIAJE_ASIGNADO) Then
              Exit For
            End If
          Next

          sb.Append("  </tbody>")
          sb.Append("</table>")
          html = sb.ToString()
        End If
      End If
    Catch ex As Exception
      html = Utilidades.MostrarMensajeUsuario(ex.Message, Utilidades.eTipoMensajeAlert.Danger)
    End Try

    Return html
  End Function

  ''' <summary>
  ''' desasigna conductor
  ''' </summary>
  ''' <returns></returns>
  Private Function DesasignarConductor(ByRef status As String, ByVal idImportacion As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""

    Try
      status = GestionOperacion.DesasignarConductorImportacion(idImportacion)
      Sistema.GrabarLogSesion(oUsuario.Id, "Desasigna Operador: idImportacion " & idImportacion)
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo desasignar el Operador<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function


#End Region

End Class
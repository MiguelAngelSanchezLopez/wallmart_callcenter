﻿Imports CapaNegocio
Imports System.Data

Public Class Planificacion_InformacionPatio
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim oUtilidades As New Utilidades
    Dim ds As New DataSet
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlanificacion As String = Me.ViewState.Item("idPlanificacion")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")

    'obtiene listado
    ds = PlanificacionTransportista.ObtenerInformacionPatio(idPlanificacion, nroTransporte)

    'guarda el resultado en session
    Me.ViewState.Add("ds", ds)
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim ds As DataSet = Me.ViewState.Item("ds")
    Dim nroTransporte As String = Me.ViewState.Item("nroTransporte")
    Dim confirmacionAnden, anden, fechaConfirmacionAnden, confirmadoAndenPor, sello, fechaLecturaSello, confirmadoLecturaSelloPor As String
    Dim mensajeConfirmacionAnden As String = ""
    Dim mensajeConfirmacionSello As String = ""
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    confirmacionAnden = "NO"
    anden = ""
    fechaConfirmacionAnden = ""
    confirmadoAndenPor = ""
    sello = "-1"
    fechaLecturaSello = ""
    confirmadoLecturaSelloPor = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(Pagina.eTabla.Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(0).Rows(0)
          confirmacionAnden = dr.Item("ConfirmacionAnden")
          anden = dr.Item("Anden")
          fechaConfirmacionAnden = dr.Item("FechaConfirmacionAnden")
          confirmadoAndenPor = dr.Item("ConfirmadoAndenPor")
          sello = dr.Item("Sello")
          fechaLecturaSello = dr.Item("FechaLecturaSello")
          confirmadoLecturaSelloPor = dr.Item("ConfirmadoLecturaSelloPor")
        End If
      End If
    Catch ex As Exception
      confirmacionAnden = "NO"
      anden = ""
      fechaConfirmacionAnden = ""
      confirmadoAndenPor = ""
      sello = "-1"
      fechaLecturaSello = ""
      confirmadoLecturaSelloPor = ""
    End Try

    Me.lblIdRegistro.Text = "[IdMaster: " & nroTransporte & "]"
    '-----------------------------------------------
    'asigna valores Anden
    Me.pnlConfirmacionAnden.Visible = IIf(confirmacionAnden = "SI", True, False)
    Me.ltlMensajeConfirmacionAnden.Visible = Not Me.pnlConfirmacionAnden.Visible
    Me.lblAnden.Text = anden
    Me.lblFechaConfirmacionAnden.Text = fechaConfirmacionAnden
    Me.lblConfirmadoAndenPor.Text = confirmadoAndenPor

    If (confirmacionAnden = "NO") Then
      If (String.IsNullOrEmpty(anden)) Then
        mensajeConfirmacionAnden = Utilidades.MostrarMensajeUsuario("No se ha asignado el and&eacute;n", Utilidades.eTipoMensajeAlert.Warning)
      Else
        mensajeConfirmacionAnden = Utilidades.MostrarMensajeUsuario("No se ha confirmado el and&eacute;n", Utilidades.eTipoMensajeAlert.Danger)
      End If
    End If
    Me.ltlMensajeConfirmacionAnden.Text = mensajeConfirmacionAnden

    '-----------------------------------------------
    'asigna valores Sello
    Me.pnlConfirmacionSello.Visible = IIf(sello = "-1", False, True)
    Me.ltlMensajeConfirmacionSello.Visible = Not Me.pnlConfirmacionSello.Visible
    Me.lblSello.Text = sello
    Me.lblFechaConfirmacionSello.Text = fechaLecturaSello
    Me.lblConfirmadoSelloPor.Text = confirmadoLecturaSelloPor

    If (confirmacionAnden = "SI") Then
      If (sello = "-1") Then
        mensajeConfirmacionSello = Utilidades.MostrarMensajeUsuario("No se ha confirmado la lectura del sello", Utilidades.eTipoMensajeAlert.Warning)
      End If
    Else
      mensajeConfirmacionSello = Utilidades.MostrarMensajeUsuario("No se ha le&iacute;do el sello", Utilidades.eTipoMensajeAlert.Warning)
    End If
    Me.ltlMensajeConfirmacionSello.Text = mensajeConfirmacionSello

  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idPlanificacion As String = Utilidades.IsNull(Request("idPlanificacion"), "-1")
    Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")
    Dim llaveRecargarPagina As String = Utilidades.IsNull(Request("llaveRecargarPagina"), "")

    Me.ViewState.Add("idPlanificacion", idPlanificacion)
    Me.ViewState.Add("nroTransporte", nroTransporte)
    Me.ViewState.Add("llaveRecargarPagina", llaveRecargarPagina)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnActualizarListado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarListado.Click
    Dim llaveRecargarPagina As String = Me.ViewState.Item("llaveRecargarPagina")
    Session(llaveRecargarPagina) = "1"
    Utilidades.RegistrarScript(Me.Page, "Planificacion.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
  End Sub


End Class
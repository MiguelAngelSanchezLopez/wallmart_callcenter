﻿Imports CapaNegocio
Imports System.Data

Public Class Transportista_ConductorDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

  Enum eTabla As Integer
    T00_FichaConductor = 0
    T01_TipoDocumentoXFichaConductor = 1
    T02_TipoDocumento = 2
  End Enum

  Public Const ENTIDAD_DOC_TRANSPORTISTA As String = "DocumentoConductor"

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  ''' <remarks>Por VSR, 23/01/2009</remarks>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idConductor As String = Me.ViewState.Item("idConductor")
    Dim idTransportista As String = Me.ViewState.Item("idTransportista")


    Try
      'textoFiltroTipoDocumento = Utilidades.IsNull(Me.ddlTipoDocumento.SelectedValue, "-1")
      'obtiene detalle del conductor
      ds = Conductor.ObtenerDetalle(idConductor, idTransportista)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idConductor As String = Me.ViewState.Item("idConductor")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim rut, nombre, paterno, materno As String
    Dim nacionalidad, estadoCivil, sexo, email, telefono, telefono2, fechaNacimiento, direccion, fechaIngreso, codTelefono, codTelefono2, nivelEstudio, nacionalidadOtra, idRegion, idComuna As String
    Dim totalRegistros_FichaConductor, totalRegistros_TipoDocumento As Integer
    Dim dr As DataRow
    Dim html = ""
    Dim dvTipoDocumento As New DataView

    'inicializa controles en vacio
    rut = ""
    nombre = ""
    paterno = ""
    materno = ""
    nacionalidad = ""
    estadoCivil = ""
    sexo = ""
    email = ""
    telefono = ""
    telefono2 = ""
    fechaNacimiento = ""
    direccion = ""
    fechaIngreso = ""
    codTelefono = ""
    codTelefono2 = ""
    nivelEstudio = ""
    nacionalidadOtra = ""
    idRegion = "-1"
    idComuna = "-1"

    Try
      If Not ds Is Nothing Then
        totalRegistros_FichaConductor = ds.Tables(eTabla.T00_FichaConductor).Rows.Count
        If totalRegistros_FichaConductor > 0 Then
          dr = ds.Tables(0).Rows(0)
          rut = dr.Item("RutCompleto")
          nombre = dr.Item("Nombre")
          paterno = dr.Item("Paterno")
          materno = dr.Item("Materno")
          nacionalidad = dr.Item("Nacionalidad")
          estadoCivil = dr.Item("EstadoCivil")
          sexo = dr.Item("Sexo")
          email = dr.Item("Email")
          telefono = dr.Item("Telefono")
          telefono2 = dr.Item("Telefono2")
          fechaNacimiento = Utilidades.ConvFechaISOaDDMMAAAA(dr.Item("FechaNacimiento"))
          direccion = dr.Item("Direccion")
          fechaIngreso = Utilidades.ConvFechaISOaDDMMAAAA(dr.Item("FechaIngreso"))
          codTelefono = dr.Item("codTelefono")
          codTelefono2 = dr.Item("codTelefono2")
          nivelEstudio = dr.Item("nivelEstudio")
          idRegion = dr.Item("idRegion")
          idComuna = dr.Item("idComuna")

          If (nacionalidad = "Otra") Then
            divOtra.Style.Add("display", "block")
            nacionalidadOtra = dr.Item("NacionalidadOtra")
          Else
            nacionalidadOtra = ""
          End If

        End If

        If idConductor = -1 Then
          totalRegistros_TipoDocumento = ds.Tables(eTabla.T02_TipoDocumento).Rows.Count
          'totalRegistros_TipoDocumentoXFichaConductor = ds.Tables(eTabla.T01_TipoDocumentoXFichaConductor).Rows.Count
          'If totalRegistros_TipoDocumentoXFichaConductor > 0 And totalRegistros_TipoDocumento > 0 Then

          If totalRegistros_TipoDocumento > 0 Then
            dvTipoDocumento = ds.Tables(eTabla.T01_TipoDocumentoXFichaConductor).DefaultView
            Me.pnlMensajeUsuario.Visible = False
            Me.pnlHolderGrilla.Visible = True
            'carga la grilla a utilizar
            gvPrincipal.Columns(6).Visible = False
            gvPrincipal.Columns(7).Visible = False
            Me.gvPrincipal.Visible = True
            Me.gvPrincipal.DataSource = dvTipoDocumento
            Me.gvPrincipal.DataBind()
            Utilidades.MostrarTotalRegistrosGrilla(dvTipoDocumento, Me.lblTotalRegistros)
          Else
            Me.pnlHolderGrilla.Visible = False
            Me.pnlMensajeUsuario.Visible = True
            Dim TextoMensaje As String = "No se encontraron tipos de documentos"
            Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)
            'oculta la grilla
            Me.gvPrincipal.Visible = False
          End If

        Else
          totalRegistros_TipoDocumento = ds.Tables(eTabla.T01_TipoDocumentoXFichaConductor).Rows.Count

          If totalRegistros_TipoDocumento > 0 Then
            dvTipoDocumento = ds.Tables(eTabla.T01_TipoDocumentoXFichaConductor).DefaultView
            Me.pnlMensajeUsuario.Visible = False
            Me.pnlHolderGrilla.Visible = True
            Me.gvPrincipal.Visible = True
            Me.gvPrincipal.DataSource = dvTipoDocumento
            Me.gvPrincipal.DataBind()
            Utilidades.MostrarTotalRegistrosGrilla(dvTipoDocumento, Me.lblTotalRegistros)
          Else
            Me.pnlHolderGrilla.Visible = False
            Me.pnlMensajeUsuario.Visible = True
            Dim TextoMensaje As String = "No se encontraron tipos de documentos"
            Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Warning)
            'oculta la grilla
            Me.gvPrincipal.Visible = False
          End If

        End If

      End If
    Catch ex As Exception
      idConductor = ""
      rut = ""
      nombre = ""
      paterno = ""
      materno = ""
      nacionalidad = ""
      estadoCivil = ""
      sexo = ""
      email = ""
      telefono = ""
      telefono2 = ""
      fechaNacimiento = ""
      direccion = ""
      fechaIngreso = ""
      codTelefono = ""
      codTelefono2 = ""
      nivelEstudio = ""
      nacionalidadOtra = ""
      idRegion = "-1"
      idComuna = "-1"
    End Try

    'asigna valores
    Me.txtIdConductor.Value = idConductor
    Me.txtRUT.Text = rut
    Me.txtNombre.Text = nombre
    Me.txtPaterno.Text = paterno
    Me.txtMaterno.Text = materno
    Me.txtFechaNacimiento.Text = fechaNacimiento
    Me.txtMail.Text = email
    Me.txtTelefono.Text = telefono
    Me.txtTelefono2.Text = telefono2
    Me.txtDireccion.Text = direccion
    Me.ddlFiltroEstadoCivil.SelectedValue = estadoCivil
    Me.ddlFiltroSexo.SelectedValue = sexo
    Me.ddlFiltroNacionalidad.SelectedValue = nacionalidad
    Me.txtNacionalidadOtra.Text = nacionalidadOtra
    Me.txtFechaIngreso.Text = fechaIngreso
    Me.ddlCodigoTelefono.Value = codTelefono
    Me.ddlCodigoTelefono2.Value = codTelefono2
    Me.ddlNivelEstudio.SelectedValue = nivelEstudio

    Dim sbScript As New StringBuilder
    sbScript.Append("Transportista.cargarComboRegionComuna({ ddlRegion: '" & Me.ddlRegion.ClientID & "', idRegion: '" & idRegion & "', ddlComuna: '" & Me.ddlComuna.ClientID & "', idComuna: '" & idComuna & "' });")
    If (Not String.IsNullOrEmpty(sbScript.ToString())) Then Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "scriptActualizaInterfaz", False)

    ''CargaComboTipoDocumentos(idUsuario, oUsuario.PerfilLlave, idPerfil)

  End Sub

  ''' <summary>
  ''' construye tabla con los Documentos del Conductor
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function ConstruirListadoDocumentos(ByVal ds As DataSet, ByVal idFichaConductor As String, ByVal idTransportista As String) As String
    Dim oUtilidades As New Utilidades
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<div class=""help-block text-center""><em>No tiene Documentos asociadas</em></div>"
    Dim dt_TipoDocumento, dt_TipoDocumentoXFichaConductor As DataTable
    Dim totalRegistros_TipoDocumento, i As Integer
    Dim sb As New StringBuilder
    Dim IdTipoDocumentoTXF As Integer = -1
    Dim IdFichaConductorTXF As Integer = -1
    Dim IdTransportistaTXF As Integer = -1

    Try
      If ds Is Nothing Then
        html = htmlSinRegistro
      Else
        dt_TipoDocumento = ds.Tables(eTabla.T02_TipoDocumento)
        totalRegistros_TipoDocumento = dt_TipoDocumento.Rows.Count
        If totalRegistros_TipoDocumento = 0 Then
          html = htmlSinRegistro
        Else
          dt_TipoDocumentoXFichaConductor = ds.Tables(eTabla.T01_TipoDocumentoXFichaConductor)
          'totalRegistros_TipoDocumentoXFichaConductor = dt_TipoDocumentoXFichaConductor.Rows.Count
          'If totalRegistros_TipoDocumentoXFichaConductor = 0 Then
          '    html = htmlSinRegistro
          'Else
          For Each row_TipoDocumento As DataRow In dt_TipoDocumento.Rows
            For Each row_TipoDocumentoXFichaConductor As DataRow In dt_TipoDocumentoXFichaConductor.Rows
              IdTipoDocumentoTXF = row_TipoDocumentoXFichaConductor("IdTipoDocumento")
              IdFichaConductorTXF = row_TipoDocumentoXFichaConductor("IdFichaConductor")
              IdTransportistaTXF = row_TipoDocumentoXFichaConductor("IdTransportista")
            Next

            If row_TipoDocumento("Id") = IdTipoDocumentoTXF And IdFichaConductorTXF = idFichaConductor And IdTransportistaTXF = idTransportista Then

              'adjuntar archivos
              sb.AppendLine("<div class=""panel panel-default"">")
              sb.AppendLine("  <div class=""panel-heading""><strong>Adjuntar archivos</strong></div>")
              sb.AppendLine("  <div class=""panel-body"">")

              sb.Append("<div class=""clearBoth cssTextoNormal"" style=""padding-bottom:5px;""><span class=""btn btn-default btn-file""><input type=""file""  id=""" & idFichaConductor & "_flArchivo_" & i & """ name=""" & idFichaConductor & "_flArchivo_" & i & """ size=""60"" /></span></div>")

              sb.AppendLine("  </div>")
              sb.AppendLine("</div>")

              'asigna texto
              html = sb.ToString

            Else

              'adjuntar archivos
              sb.AppendLine("<div class=""panel panel-default"">")
              sb.AppendLine("  <div class=""panel-heading""><strong>""" & row_TipoDocumento("NombreDocumento") & """</strong></div>")
              sb.AppendLine("  <div class=""panel-body"">")
              sb.AppendLine("  <div class=""form-group"">")
              sb.AppendLine("  <div class=""row"">")

              sb.AppendLine("  <div class=""col-xs-3"">")
              sb.AppendLine("  <asp:CheckBox ID=""chkEstado" & row_TipoDocumento("Id") & """ runat=""server"" CssClass=""control-label"" Text=""&nbsp;Sin Fecha de Expiracion"" />")
              sb.AppendLine("  </div>")

              sb.AppendLine("  <div class=""col-xs-2"">")
              sb.AppendLine("  <label class=""control-label"">Fecha de Expiración</label>")
              sb.AppendLine("  <asp:TextBox ID=""txtFechaExpiracion" & row_TipoDocumento("Id") & """ runat=""server"" MaxLength=""10"" CssClass=""form-control date-pick""></asp:TextBox>")
              sb.AppendLine("  </div>")

              sb.AppendLine("  <div class=""col-xs-4"">")
              sb.AppendLine("  <label class=""control-label"">Adjuntar Documento</label>")
              sb.Append("<div class=""clearBoth cssTextoNormal"" style=""padding-bottom:5px;""><span class=""btn btn-default btn-file""><input type=""file""  id=""" & idFichaConductor & "_flArchivo_" & row_TipoDocumento("Id") & """ name=""" & idFichaConductor & "_flArchivo_" & row_TipoDocumento("Id") & """ size=""60"" /></span></div>")
              sb.AppendLine("  </div>")

              sb.AppendLine("         </div>")
              sb.AppendLine("     </div>")
              sb.AppendLine("  </div>")
              sb.AppendLine("</div>")

              'asigna texto
              html = sb.ToString

            End If

            i += 1
          Next
          'End If
        End If

      End If

    Catch ex As Exception
      html = "<div class=""alert alert-danger"">Ha ocurrido un error interno y no se pudo obtener el listado.</div>"
    End Try

    Return html 'tpl.Replace("{CONTENIDO}", html)
  End Function


  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim idConductor As String = Me.ViewState.Item("idConductor")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idConductor = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idConductor <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el usuario en base de datos
  ''' </summary>
  ''' <param name="status"></param>
  ''' <param name="idConductor"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Private Function GrabarDatosConductor(ByRef status As String, ByRef idConductor As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim rut, nombre, paterno, materno As String
    Dim nacionalidad, nacionalidadOtra, estadoCivil, sexo, email, codTelefono, telefono, codTelefono2, telefono2, fechaNacimiento, direccion, fechaIngreso, nivelEstudio, idComuna As String
    Dim dv As String = ""
    Dim idTransportista As String

    Try
      idConductor = Utilidades.IsNull(Me.txtIdConductor.Value, "")
      idTransportista = Utilidades.IsNull(oUsuario.Id, "")
      rut = Utilidades.IsNull(Me.txtRUT.Text, "")
      nombre = Utilidades.IsNull(Me.txtNombre.Text, "")
      paterno = Utilidades.IsNull(Me.txtPaterno.Text, "")
      materno = Utilidades.IsNull(Me.txtMaterno.Text, "")

      nacionalidad = Me.ddlFiltroNacionalidad.SelectedValue
      If (nacionalidad = "Otra") Then
        nacionalidadOtra = Utilidades.IsNull(Me.txtNacionalidadOtra.Text, "")
      Else
        nacionalidadOtra = ""
      End If

      estadoCivil = Me.ddlFiltroEstadoCivil.SelectedValue
      sexo = Me.ddlFiltroSexo.SelectedValue
      email = Utilidades.IsNull(Me.txtMail.Text, "")
      codTelefono = Me.ddlCodigoTelefono.Value
      telefono = Utilidades.IsNull(Me.txtTelefono.Text, "")
      codTelefono2 = Me.ddlCodigoTelefono2.Value
      telefono2 = Utilidades.IsNull(Me.txtTelefono2.Text, "")
      fechaNacimiento = Utilidades.ConvFechaDDMMAAAAaISO(Utilidades.IsNull(Me.txtFechaNacimiento.Text, ""))
      direccion = Utilidades.IsNull(Me.txtDireccion.Text, "")
      fechaIngreso = Utilidades.ConvFechaDDMMAAAAaISO(Utilidades.IsNull(Me.txtFechaIngreso.Text, ""))
      nivelEstudio = Me.ddlNivelEstudio.SelectedValue
      idComuna = Request(Me.ddlComuna.ClientID)

      rut = Utilidades.IsNull(Me.txtRUT.Text, "")
      Utilidades.SepararRut(rut, dv)
      'jsonDatosPerfil = Utilidades.IsNull(Me.txtJSONDatosPerfil.Value, "[]")

      If idConductor = "-1" Then
        status = Conductor.GrabarDatos(idConductor, idTransportista, rut, dv, nombre, paterno, materno, nacionalidad, estadoCivil, sexo, _
                                     email, telefono, telefono2, fechaNacimiento, direccion, fechaIngreso, codTelefono, codTelefono2, nivelEstudio, nacionalidadOtra, idComuna)
        If status = "duplicado" Then idConductor = "-1" Else Sistema.GrabarLogSesion(oUsuario.Id, "Crea nuevo Operador: " & idConductor)
      Else
        status = Conductor.ModificarDatos(idConductor, idTransportista, rut, dv, nombre, paterno, materno, nacionalidad, estadoCivil, sexo, _
                                     email, telefono, telefono2, fechaNacimiento, direccion, fechaIngreso, codTelefono, codTelefono2, nivelEstudio, nacionalidadOtra, idComuna)
        Sistema.GrabarLogSesion(idConductor, "Modifica Operador: " & idConductor)
      End If

    Catch ex As Exception
      msgError = "Error al grabar datos del Operador.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba o actualiza el documento en base de datos
  ''' </summary>
  ''' <param name="status"></param>
  ''' <param name="idConductor"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 27/07/2009</remarks>
  Private Function GrabarDatosDocumento(ByRef status As String, ByRef idTipoDocumentoXFichaConductor As String, ByVal IdTipoDocumento As String, ByVal IdTransporte As String, ByVal idConductor As String, ByVal intIndefinidos As Boolean, ByVal txtFechaExpiracion As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim dv As String = ""
    Dim intVerificaTipoDocumentoXFichaConductor As String

    Try
      intVerificaTipoDocumentoXFichaConductor = Conductor.VerificarTipoDocumentoXFichaConductor(idConductor, IdTransporte, IdTipoDocumento)

      If intVerificaTipoDocumentoXFichaConductor = "0" Then
        status = Conductor.GrabarDatosDocumento(idTipoDocumentoXFichaConductor, IdTipoDocumento, IdTransporte, idConductor, intIndefinidos, txtFechaExpiracion)
        Sistema.GrabarLogSesion(oUsuario.Id, "Crea nuevo Documento: " & idConductor)
      ElseIf intVerificaTipoDocumentoXFichaConductor <> "0" Then
        status = Conductor.ModificarDatosDocumento(intVerificaTipoDocumentoXFichaConductor, IdTipoDocumento, IdTransporte, idConductor, intIndefinidos, txtFechaExpiracion)
        idTipoDocumentoXFichaConductor = intVerificaTipoDocumentoXFichaConductor
        Sistema.GrabarLogSesion(oUsuario.Id, "Modifica Documento: " & idConductor)
      End If

    Catch ex As Exception
      msgError = "Error al grabar documento.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarConductor()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idConductor As String = Me.ViewState.Item("idConductor")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""
    Dim postedfile As FileUpload
    Dim IdTipoDocumento As String = ""
    Dim chkIndefinido As HtmlControls.HtmlInputCheckBox
    Dim chkEliminar As HtmlControls.HtmlInputCheckBox
    Dim htmlFechaExpiracion As System.Web.UI.WebControls.TextBox
    Dim lblArchivo As Label
    Dim bolIndefinido As Boolean
    Dim txtFechaExpiracion As String
    Dim IdTipoDocumentoXFichaConductor As Integer
    Dim idArchivo As Integer
    Dim boolEliminar As Boolean
    Dim intVerificaTipoDocumentoXFichaConductor As Integer

    textoMensaje &= GrabarDatosConductor(status, idConductor)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      For Each row As GridViewRow In Me.gvPrincipal.Rows
        IdTipoDocumento = row.Cells(1).Text
        postedfile = CType(row.FindControl("flArchivo"), FileUpload)
        chkIndefinido = CType(row.FindControl("chkIndefinido"), HtmlControls.HtmlInputCheckBox)
        htmlFechaExpiracion = CType(row.FindControl("txtFechaExpiracion"), System.Web.UI.WebControls.TextBox)
        txtFechaExpiracion = htmlFechaExpiracion.Text
        chkEliminar = CType(row.FindControl("chkEliminar"), HtmlControls.HtmlInputCheckBox)
        lblArchivo = CType(row.FindControl("lblArchivo"), Label)

        If chkEliminar.Checked = True Then
          boolEliminar = True
        Else
          boolEliminar = False
        End If
        If chkIndefinido.Checked = True Then
          bolIndefinido = True
        Else
          bolIndefinido = False
        End If

        'Validaciones
        If bolIndefinido = True Then
          txtFechaExpiracion = ""
        End If

        If bolIndefinido = False And txtFechaExpiracion = "" Then
          bolIndefinido = True
        End If
        'Fin Validaciones

        If lblArchivo.Text <> "" And boolEliminar = True Then
          intVerificaTipoDocumentoXFichaConductor = Conductor.VerificarTipoDocumentoXFichaConductor(idConductor, oUsuario.Id, IdTipoDocumento)
          textoMensaje &= EliminarDatosDocumento(status, intVerificaTipoDocumentoXFichaConductor)
        Else
          If (postedfile.HasFile) Then
            textoMensaje &= GrabarDatosDocumento(status, IdTipoDocumentoXFichaConductor, IdTipoDocumento, oUsuario.Id, idConductor, bolIndefinido, txtFechaExpiracion)
            If textoMensaje <> "" Then
              grabadoConExito = False
            Else
              If status = "ingresado" Then
                idArchivo = -1
                GrabaContenidoFileUpload(postedfile.PostedFile, IdTipoDocumentoXFichaConductor, oUsuario.Id, idArchivo)
              ElseIf status = "modificado" Then
                idArchivo = Conductor.VerificarIdArchivo(IdTipoDocumentoXFichaConductor)
                If idArchivo = "0" Then
                  idArchivo = "-1"
                End If
                GrabaContenidoFileUpload(postedfile.PostedFile, IdTipoDocumentoXFichaConductor, oUsuario.Id, idArchivo)
              End If
            End If
          End If
        End If
      Next


    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}El Operador fue ingresado satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}El Operador fue actualizado satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}El Operador ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el Operador. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el Operador. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idConductor = "-1", "", "[ID: " & idConductor & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Transportista.cerrarPopUpFichaConductor('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idConductor
      Response.Redirect("./Transportista.ConductorDetalle.aspx?" & queryStringPagina)
    End If
  End Sub


  'Elimina el documento en base de datos
  Private Function EliminarDatosDocumento(ByRef status As String, ByVal idTipoDocumentoXFichaConductor As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim dv As String = ""

    Try
      status = Conductor.EliminarDocumento(idTipoDocumentoXFichaConductor)
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina Documento: " & idTipoDocumentoXFichaConductor)

    Catch ex As Exception
      msgError = "Error al eliminar documento.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba contenido del archivo en base de datos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabaContenidoFileUpload(ByVal postedFile As HttpPostedFile, ByVal IdTipoDocumentoXFichaConductor As String, ByVal idTransportista As String, ByRef idArchivo As String)

    Dim nombreArchivo, ruta, extension, ContentType As String
    Dim oUtilidades As New Utilidades
    Dim tamano, orden As Integer
    Dim maxTamanoArchivoEnKb As Integer = Archivo.TamanoMaximoUploadEnKb()
    Dim tamanoEnKb As Integer
    Dim oRedimension As New Archivo.Redimension
    Dim anchoMax, altoMax As Integer
    Dim dicArchivo As New Dictionary(Of String, Integer)

    Try
      tamano = postedFile.ContentLength
      tamanoEnKb = (tamano / 1024)
      anchoMax = CType(Utilidades.AnchoMaximoImagen(), Integer)
      altoMax = CType(Utilidades.AltoMaximoImagen(), Integer)

      If (tamano > 0) And (tamanoEnKb <= maxTamanoArchivoEnKb) Then
        ruta = postedFile.FileName
        nombreArchivo = Archivo.ObtenerNombreArchivoDesdeRuta(postedFile.FileName)
        extension = Archivo.ObtenerExtension(nombreArchivo)
        ContentType = postedFile.ContentType
        orden = 1

        Dim Contenido(tamano) As Byte
        'lee el archivo en el arreglo de arreglo de Byte Contenido
        postedFile.InputStream.Read(Contenido, 0, tamano)

        If Archivo.EsTipoArchivo(ContentType) = Archivo.eTipoArchivo.image Then
          'asigna valores originales
          With oRedimension
            .Contenido = Contenido
            .ContentType = ContentType
            .Extension = extension
            .NombreArchivo = nombreArchivo
            .Tamano = tamano
          End With
          Contenido = Archivo.RedimensionarImagen2Bytes(Contenido, anchoMax, altoMax, False, oRedimension)
          'asigna valores despues de la redimension
          With oRedimension
            tamano = .Tamano
            nombreArchivo = .NombreArchivo
            extension = .Extension
            ContentType = .ContentType
          End With
        End If

        'guarda archivo
        idArchivo = Archivo.GrabarArchivoEnBD(idTransportista, nombreArchivo, ruta, tamano, extension, ContentType, Contenido, orden, idArchivo)

        If idArchivo <> -200 Then
          Archivo.AsociarIdArchivoConIdRegistro(ENTIDAD_DOC_TRANSPORTISTA, IdTipoDocumentoXFichaConductor, idArchivo)
        End If
      End If
    Catch ex As Exception
      idArchivo = -200
    End Try

  End Sub


#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idConductor As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idConductor", idConductor)
    Me.ViewState.Add("idTransportista", oUsuario.Id)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If

  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarConductor()
  End Sub

  ''' <summary>
  ''' metodo general para la paginacion de la grilla
  ''' </summary>
  ''' <param name="gridView"></param>
  ''' <param name="e"></param>
  ''' <remarks>VSR, 24/07/2009</remarks>
  Private Sub GridViewPageIndexChanging(ByVal gridView As GridView, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
    Try
      'Cambia de pagina
      gridView.PageIndex = e.NewPageIndex
      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  ''' <summary>
  ''' metodo general para ordenar segun grilla seleccionada
  ''' </summary>
  ''' <param name="e"></param>
  ''' <remarks>Por VSR, 24/07/2009</remarks>
  Private Sub GridViewSorting(ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    Dim oUtilidades As New Utilidades
    Dim setearOrdenInicial As Boolean = False
    Try
      'Recupera el dataset para ordenar
      Dim dv As DataView = CType(Session("dv"), DataView)

      'determina el campo por el cual se ordenara
      Dim strOrdenarPor As String = e.SortExpression

      'Si ya se habia ordenado por ese campo, ordena pero en forma descendente
      If Not dv Is Nothing Then
        If strOrdenarPor = dv.Sort Then
          strOrdenarPor = strOrdenarPor & " DESC"
        End If
        'Ordena la grilla de acuerdo a la columna seleccionada
        dv.Sort = strOrdenarPor
      End If

      'Refresca la grilla con lo que tiene el dataTable de la session
      ActualizaInterfaz()
    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Dim TextoMensaje As String = ex.Message
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  Protected Sub gvPrincipal_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrincipal.PageIndexChanging
    'llama metodo general para paginar
    GridViewPageIndexChanging(Me.gvPrincipal, e)
  End Sub

  Protected Sub gvPrincipal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPrincipal.RowDataBound

    Dim lblArchivo As Label
    Dim textOpciones As String = ""
    Dim textArchivo As String = ""
    Dim icono As String = ""
    Dim idArchivo As String = ""
    Dim idDocumento As String = ""
    Dim nombreArchivo As String = ""
    Dim extension As String = ""
    Dim contentType As String = ""
    Dim tamano As String = ""
    Dim queryString As String = ""
    Dim textoArchivo As String = ""
    Dim indefinido As String = ""
    Dim FechaExpiracion As String = ""
    Dim indicaIndefinido As String = ""
    Dim indicaFechaExpiracion As String = ""
    Dim chkIndefinido As HtmlControls.HtmlInputCheckBox
    Dim htmlFechaExpiracion As System.Web.UI.WebControls.TextBox

    If e.Row.RowType = DataControlRowType.DataRow Then

      If Not IsDBNull(gvPrincipal.DataKeys(e.Row.RowIndex).Values("idArchivo")) Then
        If Not IsDBNull(gvPrincipal.DataKeys(e.Row.RowIndex).Values("idArchivo")) Then idArchivo = gvPrincipal.DataKeys(e.Row.RowIndex).Values("idArchivo")
        If Not IsDBNull(gvPrincipal.DataKeys(e.Row.RowIndex).Values("NombreArchivo")) Then nombreArchivo = gvPrincipal.DataKeys(e.Row.RowIndex).Values("NombreArchivo")
        'If Not IsDBNull(gvPrincipal.DataKeys(e.Row.RowIndex).Values("IdDocTransportista")) Then idDocumento = gvPrincipal.DataKeys(e.Row.RowIndex).Values("IdDocTransportista")
        If Not IsDBNull(gvPrincipal.DataKeys(e.Row.RowIndex).Values("Extension")) Then extension = gvPrincipal.DataKeys(e.Row.RowIndex).Values("Extension")
        If Not IsDBNull(gvPrincipal.DataKeys(e.Row.RowIndex).Values("ContentType")) Then contentType = gvPrincipal.DataKeys(e.Row.RowIndex).Values("ContentType")
        If Not IsDBNull(gvPrincipal.DataKeys(e.Row.RowIndex).Values("Tamano")) Then tamano = gvPrincipal.DataKeys(e.Row.RowIndex).Values("Tamano")

        indicaIndefinido = gvPrincipal.DataKeys(e.Row.RowIndex).Values("Indefinido")
        chkIndefinido = CType(e.Row.FindControl("chkIndefinido"), HtmlControls.HtmlInputCheckBox)
        If indicaIndefinido = True Then
          chkIndefinido.Checked = True
        Else
          chkIndefinido.Checked = False
        End If

        indicaFechaExpiracion = gvPrincipal.DataKeys(e.Row.RowIndex).Values("FechaExpiracion")
        htmlFechaExpiracion = CType(e.Row.FindControl("txtFechaExpiracion"), System.Web.UI.WebControls.TextBox)
        htmlFechaExpiracion.Text = indicaFechaExpiracion

        queryString = Criptografia.EncriptarTripleDES("id=" & idArchivo)

        lblArchivo = CType(e.Row.FindControl("lblArchivo"), Label)
        textoArchivo = "<img src=""" & Utilidades.ObtenerRutaIconoTipoArchivo(extension, contentType) & """ alt=""" & contentType & """ />&nbsp;&nbsp;<a href=""../page/Common.VerArchivo.aspx?a=" & queryString & """>" & nombreArchivo & "&nbsp;&nbsp;(" & Utilidades.ConvertirUnidadMedidaArchivo(tamano, True) & ")</a>"

        lblArchivo.Text = textoArchivo

      End If

      'If (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)) Then
      '    textOpciones &= "<button type=""button"" class=""btn btn-default"" onclick=""Transportista.abrirFormulario({ formulario:'Mantenedor.Asignar.Documento', idDocumento:'" & idDocumento & "' })""><span class=""glyphicon glyphicon-ok""></span></button>&nbsp;"
      'End If

      'If (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)) Then
      '    textOpciones &= "<button type=""button"" class=""btn btn-default"" onclick=""Transportista.abrirFormulario({ formulario:'Mantenedor.Documento', idDocumento:'" & idDocumento & "' })""><span class=""glyphicon glyphicon-pencil""></span></button>&nbsp;"
      'End If

      'If (Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)) Then
      '    textOpciones &= "<button type=""button"" class=""btn btn-default"" onclick=""Transportista.eliminarDocumento({ idDocumento:'" & idDocumento & "' })""><span class=""glyphicon glyphicon-trash""></span></button>&nbsp;"
      'End If


    End If
  End Sub

  Protected Sub gvPrincipal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPrincipal.Sorting
    'llama metodo general para ordenar
    GridViewSorting(e)
  End Sub

  Protected Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
    GrabarConductor()
  End Sub
End Class
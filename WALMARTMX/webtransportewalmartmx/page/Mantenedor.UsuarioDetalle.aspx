﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Mantenedor.UsuarioDetalle.aspx.vb" Inherits="webtransportewalmartmx.Mantenedor_UsuarioDetalle" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<%@ Register Src="../wuc/wucHora.ascx" TagName="wucHora" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle Usuario</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/usuario.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtIdUsuario" runat="server" value="-1" />
    <input type="hidden" id="txtJSONDatosPerfil" runat="server" value="[]" />
    <input type="hidden" id="txtIdPerfilActual" runat="server" value="0" />
    <input type="hidden" id="txtEliminarAsignaciones" runat="server" value="0" />
    <input type="hidden" id="txtLlavePerfil" runat="server" value="" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">DATOS ACCESO</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-3">
                    <label class="control-label">Perfil<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:DropDownList ID="ddlPerfil" runat="server" CssClass="form-control chosen-select"></asp:DropDownList>
                  </div>
                  <div class="col-xs-3">
                    <label class="control-label">Usuario<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtUsername" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                    <span id="hErrorUsername" runat="server"></span>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Clave<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" CssClass="form-control" TextMode="Password"></asp:TextBox>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Repetir Clave<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtRepetirPassword" runat="server" MaxLength="50" CssClass="form-control" TextMode="Password"></asp:TextBox>
                    <span id="hErrorRepetirPassword" runat="server"></span>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">&nbsp;</label><br />
                    <asp:CheckBox ID="chkEstado" runat="server" CssClass="control-label" Text="&nbsp;Activo" />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label">Conectarse los d&iacute;as</label>
                <div class="row">
                  <div class="col-xs-1"><asp:CheckBox ID="chkLunes" runat="server" Text="&nbsp;Lun" CssClass="control-label" /></div>
                  <div class="col-xs-1"><asp:CheckBox ID="chkMartes" runat="server" Text="&nbsp;Mar" CssClass="control-label" /></div>
                  <div class="col-xs-1"><asp:CheckBox ID="chkMiercoles" runat="server" Text="&nbsp;Mie" CssClass="control-label" /></div>
                  <div class="col-xs-1"><asp:CheckBox ID="chkJueves" runat="server" Text="&nbsp;Jue" CssClass="control-label" /></div>
                  <div class="col-xs-1"><asp:CheckBox ID="chkViernes" runat="server" Text="&nbsp;Vie" CssClass="control-label" /></div>
                  <div class="col-xs-1"><asp:CheckBox ID="chkSabado" runat="server" Text="&nbsp;Sab" CssClass="control-label" /></div>
                  <div class="col-xs-1"><asp:CheckBox ID="chkDomingo" runat="server" Text="&nbsp;Dom" CssClass="control-label" /></div>
                </div>
              </div>
            </div>
          </div>        

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">DATOS PERSONALES</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-4">
                    <label class="control-label">Nombre<strong class="text-danger">&nbsp;*</strong></label>
                    <asp:TextBox ID="txtNombre" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-xs-4">
                    <label class="control-label">Paterno</label>
                    <asp:TextBox ID="txtPaterno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-xs-4">
                    <label class="control-label">Materno</label>
                    <asp:TextBox ID="txtMaterno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-4">
                    <label class="control-label">RUT</label>
                    <asp:TextBox ID="txtRUT" runat="server" Columns="20" CssClass="form-control"></asp:TextBox>
                    <span id="hErrorRUT" runat="server"></span>
                    <span class="help-block">(sin puntos ej:12345678-K)</span>
                  </div>
                  <div class="col-xs-4">
                    <label class="control-label">Email</label>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                  </div>
                  <div class="col-xs-4">
                    <div><label class="control-label">Tel&eacute;fono</label></div>
                    <div class="col-sm-5 sist-padding-cero">
                      <asp:TextBox ID="txtTelefono" runat="server" MaxLength="4000" CssClass="form-control"></asp:TextBox>
                      <span id="hErrorTelefono" runat="server"></span>
                      <span class="help-block">(separados por coma &quot;,&quot;)</span>
                      <input type="hidden" id="txtJSONTelefonosPorUsuario" runat="server" value="[]" />
                      <input type="hidden" id="txtJSONPermisosTelefonoAdicional" runat="server" value="{}" />
                    </div>
                    <div class="col-sm-7 sist-padding-cero sist-padding-left-5">
                      <div class="btn-group dropup">
	                      <button type="button" id="btnTelefonoAdicional" runat="server" class="btn btn-success dropdown-toggle sist-width-300" data-toggle="dropdown">
		                      <span class="glyphicon glyphicon-earphone"></span>&nbsp;Tel&eacute;fonos adicionales <span id="lblTotalTelefonosPorUsuario" runat="server" class="badge alert-danger">0</span> <span class="caret"></span>
	                      </button>
	                      <ul id="ulListadoTelefonos" class="dropdown-menu sist-width-300" role="menu"></ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 

          <div id="hDatosEspecialPorPerfil" runat="server" class="panel panel-primary sist-display-none" data-visible="control-perfil">
            <div class="panel-heading"><h3 class="panel-title">DATOS ESPECIALES POR PERFIL</h3></div>
            <div class="panel-body">
              <div id="hNotificarPorEmailHorarioNocturno" runat="server" class="row sist-padding-bottom-10">
                <div class="col-xs-4 sist-display-none" data-visible="control-perfil">
                  <div><asp:CheckBox ID="chkNotificarPorEmailHorarioNocturno" runat="server" Text="&nbsp;Notificar por email en horario nocturno" CssClass="control-label" /></div>
                </div>
              </div>
              <div id="hEmailConCopia" runat="server" class="sist-display-none sist-padding-bottom-10" data-visible="control-perfil">
                <label class="control-label">Email con copia</label>
                <asp:TextBox ID="txtEmailConCopia" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="2"></asp:TextBox>
                <span class="help-block">(puede escribir m&aacute;s de uno separado por &quot;;&quot;)</span>
              </div>
              <div id="hLineaTransporteAsociada" runat="server" class="sist-display-none sist-padding-bottom-10" data-visible="control-perfil">
                <label class="control-label">L&iacute;nea de Transporte asociada<strong class="text-danger">&nbsp;*</strong></label>
                <uc1:wucCombo id="ddlTransportistaAsociado" runat="server" FuenteDatos="Tabla" TipoCombo="Transportista" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
              </div>
              <div id="hTeleoperador" runat="server" class="sist-display-none sist-padding-bottom-10" data-visible="control-perfil">
                <div class="form-group">
                  <label class="control-label">Gestionar alertas</label>
                  <div>
                    <asp:RadioButton ID="rbtnTeleoperadorTodasLasAlertas" runat="server" Text="&nbsp;Todas las alertas" GroupName="groupTeleoperador" />&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtnTeleoperadorSoloCemtra" runat="server" Text="&nbsp;S&oacute;lo CEMTRA" GroupName="groupTeleoperador" />&nbsp;&nbsp;
                    <asp:RadioButton ID="rbtnTeleoperadorExcluirCemtra" runat="server" Text="&nbsp;Excluir CEMTRA" GroupName="groupTeleoperador" />&nbsp;&nbsp;
                  </div>
                </div>
                <div>
                  <label class="control-label">Seleccione L&iacute;neas de Transporte a gestionar</label>
                  <asp:ListBox ID="ddlTeleoperadorTransportistasAsociados" runat="server" CssClass="form-control chosen-select" SelectionMode="Multiple"></asp:ListBox>
                  <div class="help-block">(si no selecciona ninguna gestionar&aacute; todas las l&iacute;neas de transporte)</div>
                </div>
              </div>
              <div id="hCentroDistribucion" runat="server" class="sist-display-none sist-padding-bottom-10" data-visible="control-perfil">
                <div>
                  <label id="lblCentroDistribucionLabel" class="control-label"></label>
                  <asp:ListBox ID="ddlCentroDistribucionAsociados" runat="server" CssClass="form-control chosen-select" SelectionMode="Multiple"></asp:ListBox>
                  <div id="lblCentroDistribucionHelpBlock" class="help-block"></div>
                </div>
              </div>
              <div id="hLocales" runat="server" class="sist-display-none sist-padding-bottom-10" data-visible="control-perfil">
                <div>
                  <label class="control-label">Seleccione tiendas para asociar</label>
                  <asp:ListBox ID="ddlLocalesAsociados" runat="server" CssClass="form-control chosen-select" SelectionMode="Multiple"></asp:ListBox>
                  <div class="help-block"></div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">PERMISOS</h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div id="hMatrizPermiso" runat="server"></div>
              </div>
            </div>
          </div> 

        </asp:Panel>

        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Usuario.cerrarPopUpUsuario('1')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Usuario.validarFormularioUsuario())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Crear</asp:LinkButton>
          <asp:LinkButton ID="btnModificar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Usuario.validarFormularioUsuario())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Modificar</asp:LinkButton>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".chosen-select").select2();

      });
    </script>

  </form>
</body>
</html>

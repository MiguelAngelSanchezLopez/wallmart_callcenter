﻿Imports System.Data.SqlClient
Imports System.IO
Imports CapaNegocio
Imports System.Data.OleDb

Public Class Ecommerce_CargaViaje
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum
#End Region

#Region "Funciones / Métodos"
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensaje.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensaje, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  Private Shared Function LoadDataTable(ByVal filePath As String, ByVal extension As String) As DataTable
    Dim conStr As String = ""
    Dim cmdExcel As New OleDbCommand()
    Dim oda As New OleDbDataAdapter()
    Dim dtExcel As New DataTable()

    conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source= {0};Extended Properties=""Excel 12.0;hdr=YES;imex=1"""
    conStr = String.Format(conStr, filePath)

    'Read Data from First Sheet
    Dim nameSheet As String
    Dim connExcel As New OleDbConnection(conStr)
    cmdExcel.Connection = connExcel
    connExcel.Open()
    'obtengo nombre de la hoja
    nameSheet = connExcel.GetSchema("Tables").Rows(0)("TABLE_NAME")
    cmdExcel.CommandText = "SELECT * From [" + nameSheet + "]"
    oda.SelectCommand = cmdExcel
    oda.Fill(dtExcel)
    connExcel.Close()

    Return dtExcel

  End Function

  Private Shared Function ValidateFile(ByVal dtExcel As DataTable) As String
    Dim cantCaracteres As Integer = 6
    Dim columns As New List(Of String)
    columns.Add("Patente")
    columns.Add("TipoRampla")
    columns.Add("NroUnidad")
    columns.Add("RutTransportista")
    columns.Add("Transportista")

    'Valida error en la obtención
    '----------------------------
    If (dtExcel Is Nothing) Then
      Return "Ha ocurrido un error al intentar obtener los datos del archivo."
    End If

    'Valida que existan registros
    '----------------------------
    If (dtExcel.Rows.Count = 0) Then
      Return "No se encontraron registros para cargar."
    End If

    'Valida cantidad de columnas
    '----------------------------
    If dtExcel.Columns.Count < columns.Count Then
      Return "El n&uacute;mero de columnas no es válido, deben ser " & columns.Count
    End If

    'Valida que esten todas las columnas
    '------------------------------------------
    Dim listaColumnas As New List(Of String)
    For Each nombre As String In columns
      If Not dtExcel.Columns.Contains(nombre) Then
        listaColumnas.Add("- " & nombre)
      End If
    Next

    If (listaColumnas.Count > 0) Then
      Return "Faltan las siguientes columnas:<br />" & String.Join("<br />", listaColumnas.ToArray())
    End If

    'Valida valor de los campos
    '--------------------------
    Dim listaPatentes As New List(Of String)
    Dim listaCampos As New List(Of String)
    Dim listaRutTransportista As New List(Of String)
    Dim listaTipo As New List(Of String)
    Dim index As Integer = 2 '1 es la cabecera

    For Each row As DataRow In dtExcel.Rows
      Dim patente As String = IIf(IsDBNull(row.Item("Patente")), String.Empty, row.Item("Patente"))
      Dim tipoRampla As String = IIf(IsDBNull(row.Item("TipoRampla")), String.Empty, row.Item("TipoRampla"))
      Dim transporista As String = IIf(IsDBNull(row.Item("Transportista")), String.Empty, row.Item("Transportista"))
      Dim rutTransportista As String = IIf(IsDBNull(row.Item("RutTransportista")), String.Empty, row.Item("RutTransportista"))

      'Campos obligatorios
      Dim campos As String = ""
      If (String.IsNullOrEmpty(patente)) Then
        campos += " Patente,"
      End If

      If (String.IsNullOrEmpty(tipoRampla)) Then
        campos += " TipoRampla,"
      End If

      If (String.IsNullOrEmpty(transporista)) Then
        campos += " Transporista,"
      End If

      If (String.IsNullOrEmpty(rutTransportista)) Then
        campos += " RutTransportista,"
      End If

      If (Not String.IsNullOrEmpty(campos)) Then
        campos = campos.Substring(0, campos.Length - 1)
        listaCampos.Add(" - Fila " & index.ToString() & ":" & campos)
      End If
      index += 1

      'Largo patente
      If (patente.Length <> 0 AndAlso patente.Length <> cantCaracteres) Then
        listaPatentes.Add(" - Fila " & index.ToString() & ": Patente " & patente)
      End If

      'Valida exista tipo
      'If (Not String.IsNullOrEmpty(tipoRampla) AndAlso Not ExisteTipo(tipoRampla)) Then
      '  listaTipo.Add(" - Fila " & index.ToString() & ": TipoRampla " & tipoRampla)
      'End If

      'Valida exista rutTransportista en bd
      'If (Not String.IsNullOrEmpty(rutTransportista) AndAlso Not ExisteRutTransportista(rutTransportista)) Then
      '  listaRutTransportista.Add(" - Fila " & index.ToString() & ": RutTransportista " & rutTransportista)
      'End If

    Next

    If (listaCampos.Count > 0) Then
      Return "Los siguientes campos son necesarios:<br />" & String.Join("<br />", listaCampos.ToArray())
    End If

    If (listaPatentes.Count > 0) Then
      Return "Las siguientes patentes no cumplen el largo permitido (" & cantCaracteres & " caracteres):<br />" & String.Join("<br />", listaPatentes.ToArray())
    End If

    If (listaTipo.Count > 0) Then
      Return "Los siguientes tipos no existen como registro de Tipo Rampla:<br />" & String.Join("<br />", listaTipo.ToArray())
    End If

    If (listaRutTransportista.Count > 0) Then
      Return "Los siguientes rut no existen como registro de Transportista:<br />" & String.Join("<br />", listaRutTransportista.ToArray())
    End If

    Return String.Empty

  End Function
#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    VerificarPermisos()

    If Not IsPostBack Then

      Dim oUtilidades As New Utilidades
      Sistema.GrabarLogSesion(oUtilidades.ObtenerUsuarioSession().Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())

    End If
  End Sub
End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master"
  CodeBehind="Ecommerce.CargaViaje.aspx.vb" Inherits="webtransportewalmartmx.Ecommerce_CargaViaje" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
  
  <!--css-->
  <link href="../css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
  <link href="../css/loading.css" rel="stylesheet" type="text/css" />

  <!--js-->
  <script src="../js/ecommerce.js" type="text/javascript"></script>
  <script src="../js/vendor/jquery.ui.widget.js"></script>
  <script src="../js/jquery.iframe-transport.js"></script>
  <script src="../js/jquery.fileupload.js"></script>
  <script src="../js/loading.js" type="text/javascript"></script>
    
  <!--DIV LOADING-->
  <div id="div_load">
    <div id="div_loading" class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-12 text-center">
            <h4>
              Cargando datos, espere por favor...</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 text-center">
            <img src="../img/cargando.gif" style="width: 54px; height: 54px;" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--------------->
  <asp:Panel ID="pnlMensaje" runat="server">
  </asp:Panel>
  <div>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <strong>Archivo Viajes Ecommerce</strong></h3>
        </div>
        <div class="panel-body">
          <div class="form-group" id="dvFupload">
            <div class="row">
              <div class="col-xs-12">
                <p class="text-muted">
                  Haga clic en el boton "Subir archivo" y seleccione el archivo que desea cargar en
                  sistema. <a href="../template/ViajeEcommerce.xlsx">[Haga clic aqui]</a> para descargar
                  el formato
                </p>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-5">
                <span class="btn btn-success fileinput-button"><i class="glyphicon glyphicon-plus">
                </i><span>Subir archivo</span>
                  <input id="fupload" type="file" name="files[]">
                </span>&nbsp;</div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-xs-12" id="dvMensaje">
              </div>
            </div>
          </div>
        </div>
      </div>
    </asp:Panel>
  </div>
  <script type="text/javascript">
    $(function () {
      $('#fupload').fileupload({
        url: '<%= ResolveUrl("FileUploadHandler.ashx") %>',
        add: function (e, data) {

          var ext = data.originalFiles[0].name.split('.').pop().toLowerCase();
          if ($.inArray(ext, ['xls', 'xlsx']) === -1) {
            Ecommerce.showMensaje("danger", "La extensión <strong>." + ext + "</strong> del archivo no es válida.");
          }
          else {
            $("#dvMensaje").empty();
            data.submit();
          }
        },
        done: function (e, data) {

          if (data.result) { //ok

            //termina de subir el archivo y se comienza la carga del dt
            Ecommerce.loadFileExcelViajesEcommerce(data.originalFiles[0].name);

          } else { //error
            Ecommerce.showMensaje("danger", "Ocurrio un error mientras se intentaba subir el archivo al servidor, intente nuevamente..");
          }
        }
      });

    }).ajaxStart(
            function () {
              $('#div_load').show();

            }
        ).ajaxStop(
            function () {
              $('#div_load').hide();
            });
  </script>
</asp:Content>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master" CodeBehind="Carrier.Indicadores.aspx.vb" Inherits="webtransportewalmartmx.Carrier_Indicadores" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/carrier.js"></script>
  <script src="../js/sistema.js" type="text/javascript"></script>
  <div>
    <ul id="myTab" class="nav nav-tabs">
      <li onclick="Sistema.accederUrl({ url: 'Carrier.Home.aspx' });">
        <a href="#home" data-toggle="tab"><h3 class="sist-margin-cero">Flota Online</h3></a>
      </li>
      <li class="active">
        <a href="#profile" data-toggle="tab"><h3 class="sist-margin-cero">Indicadores</h3></a>
      </li>
    </ul>
  </div>
  <div class="sist-padding-top-20">
    <asp:Panel ID="pnlMensajeAcceso" runat="server"></asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">FILTROS DE B&Uacute;SQUEDA</h3>
        </div>
        <div class="panel-body">
          <div class="form-group row">
            <div class="col-xs-3" id="hComboTransportista" runat="server">
              <label class="control-label">L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
              <uc1:wucCombo id="ddlTransportista" runat="server" FuenteDatos="Tabla" TipoCombo="Transportista" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
            </div>
            <div class="col-xs-3">
              <label class="control-label">Informe</label>
              <uc1:wuccombo id="dropInforme" runat="server" fuentedatos="TipoGeneral" tipocombo="CarrierInforme" itemtodos="true" cssclass="form-control chosen-select"></uc1:wuccombo>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Desde</label>
              <asp:TextBox ID="txtFechaDesde" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
            </div>
            <div class="col-xs-2">
              <label class="control-label">Fecha Hasta</label>
              <asp:TextBox ID="txtFechaHasta" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
              <div id="lblMensajeErrorFechaHasta" runat="server"></div>
            </div>
            <div class="col-xs-2">
              <label class="control-label">&nbsp;</label><br />
              <asp:LinkButton ID="BtBuscar" runat="server" CssClass="btn btn-primary" OnClientClick="return(Carrier.validarControlesGenerico())" ><span class="glyphicon glyphicon-search"></span>&nbsp;Buscar</asp:LinkButton>
            </div>
          </div>
        </div>
      </div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Listado</h3>
        </div>
        <div class="panel-body">
          <div class="form-group">
            <asp:GridView ID="gvPrincipal" runat="server" AutoGenerateColumns="False" CellPadding="0" AllowPaging="True" CssClass="table table-striped table-bordered" EnableModelValidation="True" PageSize="15">
              <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" />
                <asp:BoundField DataField="Nombre" HeaderText="Informe" />
                <asp:BoundField DataField="Fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" />
                <asp:TemplateField HeaderText="PDF">
                  <ItemTemplate>
                    <asp:ImageButton ID="ImgPdf" runat="server" ImageUrl="~/img/ico-pdf.png" OnClick="ImgPdf_Click" />
                  </ItemTemplate>
                </asp:TemplateField>
              </Columns>
              <EmptyDataTemplate>
                <div>No se encontraron registros asociados</div>
              </EmptyDataTemplate>
            </asp:GridView>
          </div>
        </div>
      </div>
    </asp:Panel>
  </div>
  <script type="text/javascript">
    jQuery(document).ready(function () {
      jQuery(".chosen-select").select2();

      jQuery(".date-pick").datepicker({
        changeMonth: true,
        changeYear: true
      });
    });
  </script>
</asp:Content>

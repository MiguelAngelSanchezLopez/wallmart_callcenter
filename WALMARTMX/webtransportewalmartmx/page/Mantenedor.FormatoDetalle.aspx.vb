﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_FormatoDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

  Private Enum eTabla As Integer
    T00_Detalle = 0
    T01_PerfilesPorLocal = 1
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idFormato As String = Me.ViewState.Item("idFormato")

    Try
      ds = Formato.ObtenerDetalle(idFormato)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idFormato As String = Me.ViewState.Item("idFormato")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim nombre, activo, tieneRegistrosAsociados, llave As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    nombre = ""
    activo = "1"
    tieneRegistrosAsociados = "0"
    llave = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(eTabla.T00_Detalle).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(eTabla.T00_Detalle).Rows(0)
          nombre = dr.Item("NombreFormato")
          activo = dr.Item("Activo")
          tieneRegistrosAsociados = dr.Item("TieneRegistrosAsociados")
          llave = dr.Item("Llave")
        End If
      End If
    Catch ex As Exception
      nombre = ""
      activo = "1"
      tieneRegistrosAsociados = "0"
      llave = ""
    End Try

    'asigna valores
    Me.txtIdFormato.Value = idFormato
    Me.txtNombre.Text = nombre
    Me.chkActivo.Checked = IIf(idFormato = "-1", True, IIf(activo = "1", True, False))
    Me.lblTituloFormulario.Text = IIf(idFormato = "-1", "Nuevo ", "Modificar ") & "Formato"
    Me.lblIdRegistro.Text = IIf(idFormato = "-1", "", "[ID: " & idFormato & "]")
    Me.txtLlave.Text = llave

    'primero verifica si tiene permiso para ver el boton y luego si tiene alertas asociadas
    Me.btnEliminar.Visible = Me.btnEliminar.Visible And IIf(tieneRegistrosAsociados = "1", False, True)

    'dibuja tabla con los perfiles asociados
    DibujaTablaPerfilesAsociados(ds, eTabla.T01_PerfilesPorLocal)

  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    Dim idFormato As String = Me.ViewState.Item("idFormato")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idFormato = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idFormato <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnEliminar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             idFormato <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef idFormato As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim nombre, activo, llave As String

    Try
      nombre = Utilidades.IsNull(Me.txtNombre.Text, "")
      llave = Utilidades.IsNull(Me.txtLlave.Text, "")
      activo = IIf(Me.chkActivo.Checked, "1", "0")

      If idFormato = "-1" Then Sistema.GrabarLogSesion(oUsuario.Id, "Crea nuevo Formato: " & nombre) Else Sistema.GrabarLogSesion(oUsuario.Id, "Modifica Formato: " & nombre)
      status = Formato.GrabarDatos(idFormato, nombre, activo, llave)
      If status = "duplicado" Then
        idFormato = "-1"
      Else
        GrabarPerfilesSeleccionados(idFormato)
      End If

    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar el Formato.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idFormato As String = Me.ViewState.Item("idFormato")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosRegistro(status, idFormato)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}El FORMATO fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}El FORMATO fue actualizada satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}El Nombre del FORMATO ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el FORMATO. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el FORMATO. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idFormato = "-1", "", "[ID: " & idFormato & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Formato.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idFormato
      Response.Redirect("./Mantenedor.FormatoDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' elimina los datos del registro
  ''' </summary>
  ''' <returns></returns>
  Private Function EliminarRegistro(ByRef status As String, ByVal idFormato As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""

    Try
      status = Formato.EliminarDatos(idFormato)
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina formato: IdFormato " & idFormato)
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo eliminar el Formato<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' dibuja tabla con los perfiles asociados
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub DibujaTablaPerfilesAsociados(ByVal ds As DataSet, indice As eTabla)
    Dim oUtilidades As New Utilidades
    Dim template As String = ""
    Dim html As String = ""
    Dim dt As DataTable
    Dim dv As DataView
    Dim dsFiltrado As New DataSet
    Dim totalRegistros As Integer
    Dim htmlSinRegistro As String = "<em class=""help-block"">No hay perfiles asociados</em>"
    Dim nombreTabla As String = "tblPerfilesAsociados"
    Dim sb As New StringBuilder
    Dim texto, valor, templateAux, jsonPerfiles, comboAsociarPerfiles, templateOpciones, templateOpcionesAux As String

    Try
      template &= "<tr id=""{PREFIJO_CONTROL}_tr"" {CLASS_TR}>"
      template &= "  <td>"
      template &= "    <input type=""hidden"" id=""{PREFIJO_CONTROL}_txtIdPerfil"" value=""{VALOR}"" data-tipo=""id-perfil"" />"
      template &= "    {TEXTO}"
      template &= "  </td>"
      template &= "  <td>{OPCIONES}</td>"
      template &= "</tr>"

      templateOpciones = "<button type=""button"" class=""btn btn-default"" onclick=""Formato.eliminarPerfilAsociado({ idPerfil:'{VALOR}' })""><span class=""glyphicon glyphicon-trash""></span></button>&nbsp;"

      dt = ds.Tables(indice)
      totalRegistros = dt.Rows.Count

      sb.Append("<table id=""" & nombreTabla & """ class=""table table-condensed"">")
      sb.Append("  <thead>")
      sb.Append("    <tr>")
      sb.Append("      <th>Perfil</th>")
      sb.Append("      <th style=""width:120px"">Opciones</th>")
      sb.Append("    </tr>")
      sb.Append("  </thead>")
      sb.Append("  <tbody>")

      'agrega fila para despues clonarla cuando se quiera agregar un nuevo perfil
      templateAux = template
      templateOpcionesAux = templateOpciones

      templateAux = templateAux.Replace("{CLASS_TR}", " class=""sist-display-none""")
      templateAux = templateAux.Replace("{OPCIONES}", templateOpcionesAux)
      sb.Append(templateAux)

      'dibuja los perfiles asociados
      If (totalRegistros > 0) Then
        dv = dt.DefaultView
        dv.RowFilter = "Seleccionado = 1"
        dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        dv.RowFilter = ""
        totalRegistros = dsFiltrado.Tables(0).Rows.Count

        If (totalRegistros > 0) Then
          For Each dr As DataRow In dsFiltrado.Tables(0).Rows
            templateAux = template
            templateOpcionesAux = templateOpciones
            texto = dr.Item("Texto")
            valor = dr.Item("Valor")

            templateOpcionesAux = templateOpcionesAux.Replace("{VALOR}", valor)

            templateAux = templateAux.Replace("{CLASS_TR}", "")
            templateAux = templateAux.Replace("{TEXTO}", Server.HtmlEncode(texto))
            templateAux = templateAux.Replace("{PREFIJO_CONTROL}", valor)
            templateAux = templateAux.Replace("{VALOR}", valor)
            templateAux = templateAux.Replace("{OPCIONES}", templateOpcionesAux)
            sb.Append(templateAux)
          Next

        End If
      End If

      sb.Append("  </tbody>")
      sb.Append("</table>")

      'dibuja combo para seleccionar nuevos perfiles
      comboAsociarPerfiles = DibujarComboAsociarPerfil(ds, nombreTabla)
      sb.Append(comboAsociarPerfiles)
      html = sb.ToString()

      jsonPerfiles = MyJSON.ConvertDataTableToJSON(dt)
    Catch ex As Exception
      html = "<div class=""alert alert-danger text-center""><strong>" & ex.Message & "</strong></div>"
      jsonPerfiles = "[]"
    End Try

    Me.ltlTablaPerfilesPorLocal.Text = html
    Me.txtJSONPerfiles.Value = jsonPerfiles
  End Sub

  ''' <summary>
  ''' dibuja combo con los perfiles para asociar
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarComboAsociarPerfil(ByVal ds As DataSet, ByVal nombreTabla As String) As String
    Dim html As String = ""
    Dim sb As New StringBuilder
    Dim sbHtml As New StringBuilder
    Dim texto, valor As String
    Dim dr As DataRow
    Dim dv As DataView
    Dim dsFiltrado As New DataSet

    sb.Append("  <select id=""ddlPerfil"" class=""form-control input-sm chosen-select"">")
    sb.Append("    <option value="""">--Seleccione--</option>")

    Try
      dv = ds.Tables(eTabla.T01_PerfilesPorLocal).DefaultView
      dv.RowFilter = "Seleccionado = 0"
      dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
      dv.RowFilter = ""

      For Each dr In dsFiltrado.Tables(0).Rows
        texto = dr.Item("Texto")
        valor = dr.Item("Valor")

        sb.Append("<option value=""" & Server.HtmlEncode(valor) & """>" & Server.HtmlEncode(texto) & "</option>")
      Next
    Catch ex As Exception
      'no hace nada
    End Try

    sb.Append("  </select>")

    sbHtml.Append("<div class=""row"">")
    sbHtml.Append("  <div class=""col-xs-4"">")
    sbHtml.Append("    <label class=""control-label"">Seleccione un perfil para asociar</label>")
    sbHtml.Append(sb.ToString())
    sbHtml.Append("  </div>")
    sbHtml.Append("  <div class=""col-xs-2"">")
    sbHtml.Append("    <label class=""control-label"">&nbsp;</label><br />")
    sbHtml.Append("    <button type=""button"" class=""btn btn-success"" onclick=""Formato.agregarFilaPerfilAsociado({ elem_tabla:'" & nombreTabla & "' })""><span class=""glyphicon glyphicon-plus""></span>&nbsp;Agregar perfil</button>")
    sbHtml.Append("  </div>")
    sbHtml.Append("</div>")

    html = sbHtml.ToString()
    Return html
  End Function

  ''' <summary>
  ''' graba los perfiles seleccionados
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarPerfilesSeleccionados(ByVal idFormato As String)
    Dim status, json, idPerfil As String
    Dim objJSON, row As Object

    Try
      'elimina los perfiles asociados
      status = Formato.EliminarPerfilesAsociados(idFormato)

      If (status = Sistema.eCodigoSql.Error) Then
        Throw New Exception("No se pudo eliminar los perfiles asociados")
      Else
        json = Me.txtJSONPerfilesSeleccionados.Value
        objJSON = MyJSON.ConvertJSONToObject(json)

        For Each row In objJSON
          idPerfil = MyJSON.ItemObject(row, "IdPerfil")
          status = Formato.GrabarPerfilAsociado(idFormato, idPerfil)
        Next

      End If
    Catch ex As Exception
      Exit Sub
    End Try
  End Sub
#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idFormato As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idFormato", idFormato)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idFormato As String = Me.ViewState.Item("idFormato")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim eliminadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= EliminarRegistro(status, idFormato)

    If textoMensaje <> "" Then
      eliminadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If eliminadoConExito Then
      Select Case status
        Case "eliminado"
          textoMensaje = "{ELIMINADO}El FORMATO fue eliminado satisfactoriamente. Complete el formulario para ingresar uno nuevo"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar el FORMATO. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar el FORMATO. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    queryStringPagina = "id=" & IIf(status = "eliminado", "-1", idFormato)
    Response.Redirect("./Mantenedor.FormatoDetalle.aspx?" & queryStringPagina)

  End Sub

End Class
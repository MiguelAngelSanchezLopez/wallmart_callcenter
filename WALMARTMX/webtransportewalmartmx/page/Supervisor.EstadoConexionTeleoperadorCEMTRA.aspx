﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master"
    CodeBehind="Supervisor.EstadoConexionTeleoperadorCEMTRA.aspx.vb" Inherits="webtransportewalmartmx.Supervisor_EstadoConexionTeleoperadorCEMTRA" %>

<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <script type="text/javascript" src="../js/graficos/highcharts.js"></script>
    <script type="text/javascript" src="../js/graficos/exporting.js"></script>
    <script type="text/javascript" src="../js/grafico.js"></script>
    <script type="text/javascript" src="../js/usuario.js"></script>
    <script type="text/javascript" src="../js/jquery.select2.js"></script>
    <div class="form-group text-center">
        <p class="h1">
            Monitoreo de Alertas CEMTRA</p>
    </div>
    <asp:Panel ID="pnlMensaje" Runat="server">
    </asp:Panel>
    <div>
        <asp:Panel ID="pnlMensajeAcceso" Runat="server">
        </asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">
            <input type="hidden" id="txtJSONDatos" value="[]" />
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-3">
                        <label class="control-label">
                            Seleccione Cedis:</label>
                        <uc1:wuccombo id="ddlCentroDistribucion" runat="server" fuentedatos="Tabla" tipocombo="CentroDistribucion_POR_ID_USUARIO_210818"
                            itemtodos="true" cssclass="form-control chosen-select" funciononchange="Usuario.cagarDatosCemtra()"></uc1:wuccombo>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="small">
                    <table id="tableDatos" class="table table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    Nombre Usuario
                                </th>
                                <th style="width: 120px;">
                                    Estado
                                </th>
                                <th style="width: 320px;">
                                    &Uacute;ltima acci&oacute;n
                                </th>
                                <th style="width: 140px;">
                                    Fecha &uacute;ltima acci&oacute;n
                                </th>
                                <th style="width: 140px;">
                                    Tiempo &uacute;ltima acci&oacute;n
                                </th>
                                <th style="width: 160px;">
                                    Fecha &uacute;ltimo inicio sesi&oacute;n
                                </th>
                                <th style="width: 140px;">
                                    Tiempo inicio sesi&oacute;n
                                </th>
                                <th style="width: 250px;">
                                    Centro Distribuci&oacute;n
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div id="hTablaSinDatos">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <table border="0" cellpadding="5" cellspacing="0" width="100%">
                    <tr valign="top">
                        <td style="width: 660px;">
                            <div id="hGrafico" style="width: 650px; height: 250px">
                            </div>
                        </td>
                        <td>
                            <table border="0" cellpadding="5" cellspacing="0" class="sist-font-size-20">
                                <tr>
                                    <td>
                                        Total alertas recibidas las &uacute;ltimas 4 hrs
                                    </td>
                                    <td>
                                        : <strong><span id="lblTotalAlertasRecibidas"></span></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total alertas atendidas las &uacute;ltimas 4 hrs
                                    </td>
                                    <td>
                                        : <strong><span id="lblTotalAlertasAtendidas"></span></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nivel contactabilidad
                                    </td>
                                    <td>
                                        : <strong><span id="lblNivelContactabilidad"></span>%</strong>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="mCentroDistribucion" class="modal fade">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Centro Distribución</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <span id="mCentroDistribucionTexto">texto modal</span>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                jQuery(document).ready(function () {

                    Usuario.cagarDatosCemtra();

                    jQuery(".chosen-select").select2();

                });
            </script>
        </asp:Panel>
    </div>
</asp:Content>

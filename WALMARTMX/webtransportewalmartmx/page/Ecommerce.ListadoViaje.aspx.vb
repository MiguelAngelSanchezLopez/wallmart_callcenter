﻿Imports CapaNegocio

Public Class Ecommerce_Listado
  Inherits System.Web.UI.Page

#Region "Enum"
  Private Enum eFunciones
    Ver
    Nuevo
  End Enum
#End Region

#Region "Funciones y métodos"

  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  Private Sub ObtenerDatos()

    Dim oUtilidades As New Utilidades
    Dim dtResult As New DataTable
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()

    'Obtiene los campos de filtro
    Dim fechaDesde As String = IIf(String.IsNullOrEmpty(txtFechaDesde.Text), Nothing, txtFechaDesde.Text)
    Dim fechaHasta As String = IIf(String.IsNullOrEmpty(txtFechaHasta.Text), Nothing, txtFechaHasta.Text)
    Dim pkt As String = IIf(String.IsNullOrEmpty(txtPkt.Text), Nothing, txtPkt.Text)
    Dim upc As String = IIf(String.IsNullOrEmpty(txtUpc.Text), Nothing, txtUpc.Text)
    Dim ciudad As String = IIf(String.IsNullOrEmpty(ddlCiudad.SelectedValue), Nothing, ddlCiudad.SelectedValue)
    Dim zip As String = IIf(String.IsNullOrEmpty(txtZip.Text), Nothing, txtZip.Text)
    Dim zona As String = IIf(String.IsNullOrEmpty(txtZona.Text), Nothing, txtZona.Text)
    Dim numeroOrden As String = IIf(String.IsNullOrEmpty(txtOrderNumber.Text), Nothing, txtOrderNumber.Text)
    'IIf(ddlTipo.SelectedValue = "-1", Nothing, ddlTipo.SelectedValue)

    'obtiene listado
    dtResult = Viaje.ObtenerEcommerce(fechaDesde, fechaHasta, pkt, upc, ciudad, zip, zona, numeroOrden)

    'guarda el resultado en session
    If dtResult Is Nothing Then
      ViewState.Add("dtResult", Nothing)
    Else
      ViewState.Add("dtResult", dtResult)
    End If
  End Sub

  Private Sub CargarDatos()
    Dim dtResult As DataTable = CType(ViewState.Item("dtResult"), DataTable)

    If (dtResult Is Nothing OrElse dtResult.Rows.Count = 0) Then
      pnlMensajeUsuario.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, "No se encontraron registros", Utilidades.eTipoMensajeAlert.Warning)

      'Limpia controles
      lblRegistros.Visible = False
      gvPrincipal.DataSource = Nothing
      gvPrincipal.DataBind()

    Else
      'Carga los registros
      gvPrincipal.DataSource = dtResult
      gvPrincipal.DataBind()

      'Muestra cantidad registros
      lblRegistros.Visible = True
      lblRegistros.Text = "Total de Registros: " & dtResult.Rows.Count.ToString()

      btnAsignar.Visible = True
      btnLimpiar.Visible = True
    End If

  End Sub

#End Region

#Region "Eventos"
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    VerificarPermisos()

    If Not IsPostBack Then
      Dim oUtilidades As New Utilidades
      Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())

      btnAsignar.Visible = False
      btnLimpiar.Visible = False
      lblRegistros.Visible = False
    End If

  End Sub

  Protected Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim pageSize As String = Trim(txtFiltroRegistrosPorPagina.Text)
    If pageSize = "" Or pageSize = "0" Then
      pageSize = Utilidades.ObtenerRegistrosPorPaginaDefault()
      txtFiltroRegistrosPorPagina.Text = pageSize
    End If

    'asigna total de registros por pagina
    Me.gvPrincipal.PageSize = pageSize
    'Muestra la primera pagina del paginador
    Me.gvPrincipal.PageIndex = 0

    ObtenerDatos()
    CargarDatos()

    'Oculta div de carga
    ScriptManager.RegisterStartupScript(Me, GetType(Page), "script1", "Loading.hide()", True)
  End Sub

  Protected Sub gvPrincipal_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPrincipal.RowDataBound

    If e.Row.RowType = DataControlRowType.DataRow Then
      Dim dvSeleccionar As HtmlControl = CType(e.Row.FindControl("dvSeleccionar"), HtmlControl)
      Dim asignado As Boolean = gvPrincipal.DataKeys(e.Row.RowIndex).Values("Asignado")

      If (asignado) Then
        dvSeleccionar.Visible = False
      Else
        dvSeleccionar.Visible = True
      End If

    End If

  End Sub

  Protected Sub gvPrincipal_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvPrincipal.Sorting

    Try

      'Recupera el dataset para ordenar
      Dim dv As DataView = CType(ViewState.Item("dtResult"), DataTable).DefaultView

      'determina el campo por el cual se ordenara
      Dim strOrdenarPor As String = e.SortExpression

      'Si ya se habia ordenado por ese campo, ordena pero en forma descendente
      If Not dv Is Nothing Then
        If strOrdenarPor = dv.Sort Then
          strOrdenarPor = strOrdenarPor & " DESC"
        End If
        'Ordena la grilla de acuerdo a la columna seleccionada
        dv.Sort = strOrdenarPor
      End If

      'Refresca la grilla con lo que tiene el dataTable de la session
      CargarDatos()

    Catch ex As Exception
      'Muestra error al usuario
      Me.pnlMensajeUsuario.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, ex.Message, Utilidades.eTipoMensajeAlert.Danger)
    End Try
  End Sub

  'Protected Sub gvPrincipal_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPrincipal.PageIndexChanging
  '  Try
  '    'Cambia de pagina
  '    gvPrincipal.PageIndex = e.NewPageIndex
  '    'Refresca la grilla con lo que tiene el dataTable de la session
  '    CargarDatos()
  '  Catch ex As Exception
  '    'Muestra error al usuario
  '    Me.pnlMensajeUsuario.Visible = True
  '    Dim TextoMensaje As String = ex.Message
  '    Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, TextoMensaje, Utilidades.eTipoMensajeAlert.Danger)
  '  End Try
  'End Sub
#End Region

  Protected Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click

    For Each row As GridViewRow In gvPrincipal.Rows
      Dim id As Integer = gvPrincipal.DataKeys(row.RowIndex).Values("Id")
      Dim chk As CheckBox = row.FindControl("chkSeleccionar")
      Dim hCorrelativo As HiddenField = row.FindControl("hCorrelativo")

      If (chk.Checked) Then

        'Obtiene los datos
        Dim fechaProgramacion As String = IIf(String.IsNullOrEmpty(txtFechaProgramacion.Text), Nothing, txtFechaProgramacion.Text)
        Dim cantidadPkt As Integer = IIf(String.IsNullOrEmpty(txtCantidadPKT.Text), Nothing, txtCantidadPKT.Text)
        Dim zona As String = IIf(String.IsNullOrEmpty(txtZonaAsignada.Text), Nothing, txtZonaAsignada.Text)
        Dim placa As String = IIf(String.IsNullOrEmpty(txtPlaca.Text), Nothing, txtPlaca.Text)
        Dim conductor As String = IIf(String.IsNullOrEmpty(txtConductor.Text), Nothing, txtConductor.Text)
        Dim correlativo As String = IIf(String.IsNullOrEmpty(hCorrelativo.Value), Nothing, hCorrelativo.Value)

        'Guarda los datos
        Dim oViaje As New Viaje
        oViaje.Id = id
        oViaje.FechaProgramacion = fechaProgramacion
        oViaje.CantidadPkt = cantidadPkt
        oViaje.Zona = zona
        oViaje.Placa = placa
        oViaje.Conductor = conductor
        oViaje.Correlativo = correlativo

        Viaje.Grabar(oViaje)

      End If

    Next

    ObtenerDatos()
    CargarDatos()

  End Sub

  Protected Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
    CargarDatos()
  End Sub
End Class
﻿Imports CapaNegocio
Imports System.Data

Public Class TO_AgregarContacto
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario As String = Me.ViewState.Item("idUsuario")
    Dim agrupadoEn As String = Me.ViewState.Item("agrupadoEn")
    Dim nombreConductor As String = Me.ViewState.Item("nombreConductor")
    Dim nombreTransportista As String = Me.ViewState.Item("nombreTransportista")
    Dim nombre, paterno, materno As String

    'inicializa controles en vacio
    nombre = ""
    paterno = ""
    materno = ""

    'asigna nombre segun grupo
    Select Case agrupadoEn.ToUpper()
      Case Alerta.GRUPO_CONTACTO_CONDUCTOR
        If (Not String.IsNullOrEmpty(nombreConductor)) Then nombre = nombreConductor
      Case Alerta.GRUPO_CONTACTO_TRANSPORTISTA
        If (Not String.IsNullOrEmpty(nombreTransportista)) Then nombre = nombreTransportista
    End Select

    'asigna valores
    Me.txtNombre.Text = nombre
    Me.txtPaterno.Text = paterno
    Me.txtMaterno.Text = materno
    Me.lblTituloFormulario.Text = "Nuevo " & agrupadoEn
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim idUsuario As String = Me.ViewState.Item("idUsuario")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idUsuario = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el usuario en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef idUsuario As String, ByRef jsonContacto As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim agrupadoEn As String = Me.ViewState.Item("agrupadoEn")
    Dim localDestinoCodigo As String = Me.ViewState.Item("localDestinoCodigo")
    Dim localDestinoNombre As String = Me.ViewState.Item("localDestinoNombre")
    Dim localOrigenCodigo As String = Me.ViewState.Item("localOrigenCodigo")
    Dim localOrigenNombre As String = Me.ViewState.Item("localOrigenNombre")
    Dim prefijoControl As String = Me.ViewState.Item("prefijoControl")
    Dim nombreConductor As String = Me.ViewState.Item("nombreConductor")
    Dim nombreTransportista As String = Me.ViewState.Item("nombreTransportista")
    Dim rutConductor As String = Me.ViewState.Item("rutConductor")
    Dim rutTransportista As String = Me.ViewState.Item("rutTransportista")
    Dim llavePerfil As String = Me.ViewState.Item("llavePerfil")
    Dim tipoGrupo As String = Me.ViewState.Item("tipoGrupo")
    Dim idFormato As String = Me.ViewState.Item("idFormato")
    Dim username, password, nombre, paterno, materno As String
    Dim email, telefono, loginDias, super, estado, idPerfil, listadoPermisos As String
    Dim statusAsociacion As String = ""
    Dim dv As String = ""
    Dim rut As String = ""
    Dim oPerfil As Perfil
    Dim dcJSON As New Dictionary(Of String, String)

    Try
      username = Me.CrearUsernameUnico(agrupadoEn, llavePerfil)
      password = Usuario.CLAVE_POR_DEFECTO
      nombre = Sistema.Capitalize(Utilidades.IsNull(Me.txtNombre.Text, ""), Sistema.eTipoCapitalizacion.TodoMayuscula)
      paterno = Sistema.Capitalize(Utilidades.IsNull(Me.txtPaterno.Text, ""), Sistema.eTipoCapitalizacion.TodoMayuscula)
      materno = Sistema.Capitalize(Utilidades.IsNull(Me.txtMaterno.Text, ""), Sistema.eTipoCapitalizacion.TodoMayuscula)
      email = Sistema.Capitalize(Utilidades.IsNull(Me.txtEmail.Text, ""), Sistema.eTipoCapitalizacion.TodoMinuscula)
      telefono = Utilidades.IsNull(Me.txtTelefono.Text, "")
      loginDias = "0000000"
      super = "0"
      estado = "1"
      oPerfil = New Perfil(llavePerfil)
      idPerfil = oPerfil.Id
      listadoPermisos = ""

      'formatea rut segun el grupo
      Select Case agrupadoEn.ToUpper()
        Case Alerta.GRUPO_CONTACTO_CONDUCTOR
          If (Not String.IsNullOrEmpty(rutConductor)) Then
            rut = rutConductor
            dv = Utilidades.CalcularDigitoVerificador(rutConductor)
          End If
        Case Alerta.GRUPO_CONTACTO_TRANSPORTISTA
          If (Not String.IsNullOrEmpty(rutTransportista)) Then
            rut = rutTransportista
            Utilidades.SepararRut(rut, dv)
          End If
      End Select

      If idUsuario = "-1" Then
        status = Usuario.GrabarDatos(idUsuario, rut, dv, nombre, paterno, materno, username, password, email, _
                                     telefono, loginDias, super, estado, idPerfil, listadoPermisos)
        If status = "duplicado" Then idUsuario = "-1"

        'grabar asignacion especial segun el perfil del usuario
        Select Case tipoGrupo
          Case CentroDistribucion.NombreEntidad
            statusAsociacion = Alerta.AsociarCentroDistribucionUsuario(idUsuario, localOrigenCodigo, localOrigenNombre, llavePerfil)
          Case Local.NombreEntidad
            statusAsociacion = Alerta.AsociarLocalUsuario(idUsuario, localDestinoCodigo, localDestinoNombre, llavePerfil, idFormato)
          Case Else
            statusAsociacion = ""
        End Select

        If statusAsociacion = CStr(Sistema.eCodigoSql.Error) Then Throw New Exception("No se pudo grabar los datos del contacto")

        'construye json de retorno
        dcJSON.Add("IdEscalamientoPorAlertaContacto", idUsuario)
        dcJSON.Add("NombreUsuario", Sistema.Capitalize(Trim(nombre & " " & paterno & " " & materno), Sistema.eTipoCapitalizacion.NombrePropio))
        dcJSON.Add("Telefono", telefono)
        dcJSON.Add("Email", email)
        dcJSON.Add("Cargo", oPerfil.Nombre)
        dcJSON.Add("HoraInicio", "")
        dcJSON.Add("HoraTermino", "")
        dcJSON.Add("TipoGrupo", tipoGrupo)
        jsonContacto = MyJSON.ConvertObjectToJSON(dcJSON)
        Sistema.GrabarLogSesion(oUsuario.Id, "Crea nuevo contacto: " & agrupadoEn.ToUpper())
      End If

    Catch ex As Exception
      msgError = "Error al grabar datos del contacto.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario As String = Me.ViewState.Item("idUsuario")
    Dim agrupadoEn As String = Me.ViewState.Item("agrupadoEn")
    Dim localDestinoCodigo As String = Me.ViewState.Item("localDestinoCodigo")
    Dim localDestinoNombre As String = Me.ViewState.Item("localDestinoNombre")
    Dim localOrigenCodigo As String = Me.ViewState.Item("localOrigenCodigo")
    Dim localOrigenNombre As String = Me.ViewState.Item("localOrigenNombre")
    Dim prefijoControl As String = Me.ViewState.Item("prefijoControl")
    Dim nombreConductor As String = Me.ViewState.Item("nombreConductor")
    Dim nombreTransportista As String = Me.ViewState.Item("nombreTransportista")
    Dim rutConductor As String = Me.ViewState.Item("rutConductor")
    Dim rutTransportista As String = Me.ViewState.Item("rutTransportista")
    Dim llavePerfil As String = Me.ViewState.Item("llavePerfil")
    Dim tipoGrupo As String = Me.ViewState.Item("tipoGrupo")
    Dim idFormato As String = Me.ViewState.Item("idFormato")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""
    Dim jsonContacto As String = ""
    Dim json As String

    'grabar valores
    textoMensaje &= GrabarDatosRegistro(status, idUsuario, jsonContacto)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}El CONTACTO fue ingresado satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}El CONTACTO fue actualizado satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}El CONTACTO ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el CONTACTO. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el CONTACTO. Intente nuevamente por favor.<br />" & textoMensaje
    End If

    If status = "ingresado" Or status = "modificado" Then
      json = "{"
      json &= " prefijoControl: """ & prefijoControl & """"
      json &= ",jsonContacto: " & jsonContacto
      json &= "}"

      Utilidades.RegistrarScript(Me.Page, "Alerta.asignarContactoEnCombo(" & json & ");", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

      queryStringPagina &= "prefijoControl=" & Server.UrlEncode(prefijoControl)
      queryStringPagina &= "&agrupadoEn=" & Server.UrlEncode(agrupadoEn)
      queryStringPagina &= "&localDestinoCodigo=" & Server.UrlEncode(localDestinoCodigo)
      queryStringPagina &= "&localDestinoNombre=" & Server.UrlEncode(localDestinoNombre)
      queryStringPagina &= "&localOrigenCodigo=" & Server.UrlEncode(localOrigenCodigo)
      queryStringPagina &= "&localOrigenNombre=" & Server.UrlEncode(localOrigenNombre)
      queryStringPagina &= "&nombreConductor=" & Server.UrlEncode(nombreConductor)
      queryStringPagina &= "&rutConductor=" & Server.UrlEncode(rutConductor)
      queryStringPagina &= "&nombreTransportista=" & Server.UrlEncode(nombreTransportista)
      queryStringPagina &= "&rutTransportista=" & Server.UrlEncode(rutTransportista)
      queryStringPagina &= "&llavePerfil=" & Server.UrlEncode(llavePerfil)
      queryStringPagina &= "&tipoGrupo=" & Server.UrlEncode(tipoGrupo)
      queryStringPagina &= "&idFormato=" & Server.UrlEncode(idFormato)
      Response.Redirect("./TO.AgregarContacto.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' crea un username unico para el usuario ingresado
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CrearUsernameUnico(ByVal agrupadoEn As String, ByRef llavePerfil As String) As String
    Dim username = ""

    Try
      username = llavePerfil & "_" & Archivo.CrearNombreUnicoArchivo("", False)
    Catch ex As Exception
      username = Perfil.KEY_GENERICO
      llavePerfil = Perfil.KEY_GENERICO
    End Try

    Return username
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idUsuario As String = "-1" 'siempre el usuario es nuevo
    Dim agrupadoEn As String = Utilidades.IsNull(Request("agrupadoEn"), "")
    Dim localDestinoCodigo As String = Utilidades.IsNull(Request("localDestinoCodigo"), "")
    Dim localDestinoNombre As String = Utilidades.IsNull(Request("localDestinoNombre"), "")
    Dim localOrigenCodigo As String = Utilidades.IsNull(Request("localOrigenCodigo"), "")
    Dim localOrigenNombre As String = Utilidades.IsNull(Request("localOrigenNombre"), "")
    Dim prefijoControl As String = Utilidades.IsNull(Request("prefijoControl"), "")
    Dim nombreConductor As String = Utilidades.IsNull(Request("nombreConductor"), "")
    Dim rutConductor As String = Utilidades.IsNull(Request("rutConductor"), "")
    Dim nombreTransportista As String = Utilidades.IsNull(Request("nombreTransportista"), "")
    Dim rutTransportista As String = Utilidades.IsNull(Request("rutTransportista"), "")
    Dim llavePerfil As String = Utilidades.IsNull(Request("llavePerfil"), "")
    Dim tipoGrupo As String = Utilidades.IsNull(Request("tipoGrupo"), "")
    Dim idFormato As String = Utilidades.IsNull(Request("idFormato"), "")

    Me.ViewState.Add("idUsuario", idUsuario)
    Me.ViewState.Add("agrupadoEn", agrupadoEn)
    Me.ViewState.Add("localDestinoCodigo", localDestinoCodigo)
    Me.ViewState.Add("localDestinoNombre", localDestinoNombre)
    Me.ViewState.Add("localOrigenCodigo", localOrigenCodigo)
    Me.ViewState.Add("localOrigenNombre", localOrigenNombre)
    Me.ViewState.Add("prefijoControl", prefijoControl)
    Me.ViewState.Add("nombreConductor", nombreConductor)
    Me.ViewState.Add("rutConductor", rutConductor)
    Me.ViewState.Add("nombreTransportista", nombreTransportista)
    Me.ViewState.Add("rutTransportista", rutTransportista)
    Me.ViewState.Add("llavePerfil", llavePerfil)
    Me.ViewState.Add("tipoGrupo", tipoGrupo)
    Me.ViewState.Add("idFormato", idFormato)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ActualizaInterfaz()
      InicializaControles(Page.IsPostBack)
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

End Class
﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/MasterPage.Master"
  CodeBehind="Scat.Enrolar.aspx.vb" Inherits="webtransportewalmartmx.Scat_Enrolar" %>
  <%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
  <script src="../js/jquery.select2.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
  <div class="form-group text-center">
    <div class="row">
      <div class="col-xs-4">
        &nbsp;
      </div>
      <div class="col-xs-4">
        <p class="h1">Enrolar</p>
      </div>
    </div>
  </div>
  <asp:Panel ID="pnlMensaje" runat="server">
  </asp:Panel>
  <div>
    <asp:Panel ID="pnlMensajeAcceso" runat="server">
    </asp:Panel>
    <asp:Panel ID="pnlContenido" runat="server">
      <div class="panel panel-primary">
        <div class="panel-heading">
      <h3 class="panel-title">
        INFORMACION PERSONAL</h3>
    </div>
        <div class="panel-body">
      <div class="form-group">
        <div class="row">
          <div class="col-xs-3 text-center">
            <div class="form-group">
              <div class="row">
              <div class="col-xs-12">
                <img src="../img/desconocido.png" alt="" />
              </div>
            </div>
            </div>
            <div class="form-group">
              <div class="row">
              <div class="col-xs-12">
                <asp:LinkButton ID="btnFoto" runat="server" CssClass="btn btn-success" OnClientClick="#"><span class="glyphicon glyphicon-camera"></span>&nbsp;Agregar Foto</asp:LinkButton>
              </div>
            </div>
            </div>
          </div>
          <div class="col-xs-9">
            <div class="form-group">
              <div class="row">
              <div class="col-xs-4">
                <label class="control-label">Rut L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtRutTransportista" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
                <span class="help-block">(sin puntos ej:12345678-K)</span>
              </div>            
            </div>
            </div>
            <div class="form-group">  
              <div class="row">
              <div class="col-xs-6">
                <label class="control-label">L&iacute;nea de Transporte<strong class="text-danger">&nbsp;*</strong></label>
                <asp:TextBox ID="txtTransportista" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
              </div>
            </div>
            </div>
          </div>
        </div>

      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-xs-2">
            <label class="control-label">Nombre<strong class="text-danger">&nbsp;*</strong></label>
            <asp:TextBox ID="txtNombre" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>            
          </div>
          <div class="col-xs-2">
            <label class="control-label">Paterno<strong class="text-danger">&nbsp;*</strong></label>
            <asp:TextBox ID="txtPaterno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
          </div>
          <div class="col-xs-2">
            <label class="control-label">Materno</label>
            <asp:TextBox ID="txtMaterno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
          </div>
          <div class="col-xs-2">
            <label class="control-label">RUT<strong class="text-danger">&nbsp;*</strong></label>
            <asp:TextBox ID="txtRut" runat="server" Columns="20" MaxLength="20" CssClass="form-control"></asp:TextBox>
            <span class="help-block">(sin puntos ej:12345678-K)</span>
         </div>
          <div class="col-xs-2">
            <label class="control-label">Fecha Nacimiento<strong class="text-danger">&nbsp;*</strong></label>
            <asp:TextBox ID="txtFechaNacimiento" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
         </div>
          <div class="col-xs-2">
            <label class="control-label">Sexo<strong class="text-danger">&nbsp;*</strong></label>
            <uc1:wucCombo ID="ddlFiltroSexo" runat="server" FuenteDatos="TipoGeneral" TipoCombo="Sexo" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-xs-2">
            <label class="control-label">Fecha Enrolamiento</label>
            <asp:TextBox ID="txtFechaEnrolamiento" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
          </div>
           <div class="col-xs-3">
            <label class="control-label">Licencia Conducir</label>
            <asp:TextBox ID="txtLicenciaConducir" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
          </div>
          <div class="col-xs-3">
            <label class="control-label">Cédula de Identidad</label>
            <asp:TextBox ID="TextxtCedulaIdentidad" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
          </div><div class="col-xs-3">
            <label class="control-label">EPP</label>
            <asp:TextBox ID="txtEPP" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
          </div>
          
        </div>
      </div>
    </div>

        <div class="form-group text-center">
      <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="#"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
      <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="#"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
    </div>
      </div>
    </asp:Panel>
  </div>
  <script type="text/javascript">
     jQuery(document).ready(function () {
       jQuery(".chosen-select").select2();

       jQuery(".date-pick").datepicker({
         changeMonth: true,
         changeYear: true
       });
     });

  </script>
</asp:Content>

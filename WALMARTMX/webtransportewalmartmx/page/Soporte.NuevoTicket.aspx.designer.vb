﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Soporte_NuevoTicket

  '''<summary>
  '''Control pnlMensaje.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensaje As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control hMensajeEnvioTicket.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hMensajeEnvioTicket As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control pnlMensajeAcceso.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlMensajeAcceso As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control pnlContenido.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents pnlContenido As Global.System.Web.UI.WebControls.Panel

  '''<summary>
  '''Control txtNombrePersona.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtNombrePersona As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtTelefono.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtTelefono As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtEmail.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtEmail As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control ddlAplicacion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ddlAplicacion As Global.System.Web.UI.HtmlControls.HtmlSelect

  '''<summary>
  '''Control hAplicacionOtro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hAplicacionOtro As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control txtAplicacionOtro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtAplicacionOtro As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control ddlModulo.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ddlModulo As Global.System.Web.UI.HtmlControls.HtmlSelect

  '''<summary>
  '''Control hModuloOtro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hModuloOtro As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control txtModuloOtro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtModuloOtro As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control ddlMotivoTicket.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents ddlMotivoTicket As Global.System.Web.UI.HtmlControls.HtmlSelect

  '''<summary>
  '''Control hMotivoTicketOtro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hMotivoTicketOtro As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control txtMotivoTicketOtro.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtMotivoTicketOtro As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control txtObservacion.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents txtObservacion As Global.System.Web.UI.WebControls.TextBox

  '''<summary>
  '''Control hBotones.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hBotones As Global.System.Web.UI.HtmlControls.HtmlGenericControl

  '''<summary>
  '''Control hMensajeGrabando.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents hMensajeGrabando As Global.System.Web.UI.HtmlControls.HtmlGenericControl
End Class

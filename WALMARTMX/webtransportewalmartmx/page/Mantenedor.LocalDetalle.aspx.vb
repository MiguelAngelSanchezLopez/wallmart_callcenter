﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_LocalDetalle
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idLocal As String = Me.ViewState.Item("idLocal")

    Try
      ds = Local.ObtenerDetalle(idLocal)
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idLocal As String = Me.ViewState.Item("idLocal")
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim nombre, codigo, idFormato, activo, maximoHorasAlertaActiva, etapa, focoAlto, focoTablet, telefono As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    nombre = ""
    codigo = ""
    idFormato = "-1"
    activo = "1"
    maximoHorasAlertaActiva = "-1"
    etapa = "-1"
    focoAlto = "0"
    focoTablet = "0"
    telefono = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(0).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(0).Rows(0)
          nombre = dr.Item("NombreLocal")
          codigo = dr.Item("CodigoInterno")
          idFormato = dr.Item("IdFormato")
          activo = dr.Item("Activo")
          maximoHorasAlertaActiva = dr.Item("MaximoHorasAlertaActiva")
          etapa = dr.Item("Etapa")
          focoAlto = dr.Item("FocoAlto")
          focoTablet = dr.Item("FocoTablet")
          telefono = dr.Item("Telefono")
        End If
      End If
    Catch ex As Exception
      nombre = ""
      codigo = ""
      idFormato = "-1"
      activo = "1"
      maximoHorasAlertaActiva = "-1"
      etapa = "-1"
      focoAlto = "0"
      focoTablet = "0"
      telefono = ""
    End Try

    'asigna valores
    Me.txtCategoria.Value = Local.NombreEntidad
    Me.txtIdLocal.Value = idLocal
    Me.txtNombre.Text = nombre
    Me.ddlFormato.SelectedValue = idFormato
    Me.txtCodigo.Text = codigo
    Me.chkActivo.Checked = IIf(idLocal = "-1", True, IIf(activo = "1", True, False))
    Me.lblTituloFormulario.Text = IIf(idLocal = "-1", "Nueva ", "Modificar ") & "Tienda"
    Me.lblIdRegistro.Text = IIf(idLocal = "-1", "", "[ID: " & idLocal & "]")
    Me.txtMaximoHorasAlertaActiva.Text = IIf(maximoHorasAlertaActiva = "-1", "", maximoHorasAlertaActiva)
    Me.txtEtapa.Text = IIf(etapa = "-1", "", etapa)
    Me.chkFocoAlto.Checked = IIf(idLocal = "-1", False, IIf(focoAlto = "1", True, False))
    Me.chkFocoTablet.Checked = IIf(idLocal = "-1", False, IIf(focoTablet = "1", True, False))
    Me.txtTelefono.Text = telefono

    'Dibuja los cargos asociados al formato y local
    Utilidades.RegistrarScript(Me.Page, "Local.dibujarComboCargosPorFormato();", Utilidades.eRegistrar.FINAL, False)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    Dim idLocal As String = Me.ViewState.Item("idLocal")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          idLocal = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              idLocal <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnEliminar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             idLocal <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef idLocal As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim nombre, idFormato, codigo, activo, maximoHorasAlertaActiva, listadoUsuariosCargos, etapa, focoAlto, focoTablet, telefono As String

    Try
      nombre = Utilidades.IsNull(Me.txtNombre.Text, "")
      idFormato = Utilidades.IsNull(Me.ddlFormato.SelectedValue, "-1")
      codigo = Utilidades.IsNull(Me.txtCodigo.Text, "")
      activo = IIf(Me.chkActivo.Checked, "1", "0")
      maximoHorasAlertaActiva = Utilidades.IsNull(Me.txtMaximoHorasAlertaActiva.Text, "-1")
      listadoUsuariosCargos = Utilidades.IsNull(Me.txtListadoUsuariosCargos.Value, "")
      etapa = Utilidades.IsNull(Me.txtEtapa.Text, "-1")
      focoAlto = IIf(Me.chkFocoAlto.Checked, "1", "0")
      focoTablet = IIf(Me.chkFocoTablet.Checked, "1", "0")
      telefono = Utilidades.IsNull(Me.txtTelefono.Text, "")
      telefono = telefono.Replace(" ", "").Replace(";", ",").Replace(",", ", ")

      If idLocal = "-1" Then Sistema.GrabarLogSesion(oUsuario.Id, "Crea nueva tienda: " & nombre & " - " & codigo) Else Sistema.GrabarLogSesion(oUsuario.Id, "Modifica tienda: " & nombre & " - " & codigo)
      status = Local.GrabarDatos(idLocal, nombre, idFormato, codigo, activo, maximoHorasAlertaActiva, listadoUsuariosCargos, etapa, focoAlto, focoTablet, telefono)
      If status = "duplicado" Then idLocal = "-1"
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo grabar la tienda.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idLocal As String = Me.ViewState.Item("idLocal")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= GrabarDatosRegistro(status, idLocal)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}La Tienda fue ingresada satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}La Tienda fue actualizada satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}El Nombre de la Tienda ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la Tienda. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar la Tienda. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = IIf(idLocal = "-1", "", "[ID: " & idLocal & "] ") & textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "Local.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "id=" & idLocal
      Response.Redirect("./Mantenedor.LocalDetalle.aspx?" & queryStringPagina)
    End If
  End Sub

  ''' <summary>
  ''' elimina los datos del registro
  ''' </summary>
  ''' <returns></returns>
  Private Function EliminarRegistro(ByRef status As String, ByVal idLocal As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""

    Try
      status = Local.EliminarDatos(idLocal)
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina tienda: IdLocal " & idLocal)
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo eliminar la tienda<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idLocal As String = Utilidades.IsNull(Request("id"), "-1")

    Me.ViewState.Add("idLocal", idLocal)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idLocal As String = Me.ViewState.Item("idLocal")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim eliminadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores y retornar nuevo rut
    textoMensaje &= EliminarRegistro(status, idLocal)

    If textoMensaje <> "" Then
      eliminadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If eliminadoConExito Then
      Select Case status
        Case "eliminado"
          textoMensaje = "{ELIMINADO}La Tienda fue eliminada satisfactoriamente. Complete el formulario para ingresar una nueva"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar la Tienda. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo eliminar la Tienda. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    queryStringPagina = "id=" & IIf(status = "eliminado", "-1", idLocal)
    Response.Redirect("./Mantenedor.LocalDetalle.aspx?" & queryStringPagina)

  End Sub

End Class
﻿Imports CapaNegocio
Imports System.Data

Public Class Mantenedor_TelefonoPorUsuario
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Crear
    Modificar
    Eliminar
  End Enum

#End Region

#Region "Private"
  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks>Por VSR, 03/10/2008</remarks>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idTelefonoPorUsuario As String = Me.ViewState.Item("idTelefonoPorUsuario")
    Dim indiceJSON As String = Me.ViewState.Item("indiceJSON")
    Dim sb As New StringBuilder

    'inicializa controles
    Me.txtTelefono.Text = ""
    Me.wucHoraInicio.SelectedValueHora = ""
    Me.wucHoraInicio.SelectedValueMinutos = ""
    Me.wucHoraTermino.SelectedValueHora = ""
    Me.wucHoraTermino.SelectedValueMinutos = ""

    Me.lblTituloFormulario.Text = IIf(indiceJSON = "-1", "Nuevo ", "Modificar ") & "Teléfono"
    sb.Append("Usuario.cargarDatosTelefonoPorUsuario({ idTelefonoPorUsuario:'" & idTelefonoPorUsuario & "',  indiceJSON:'" & indiceJSON & "' })")
    Utilidades.RegistrarScript(Me.Page, sb.ToString(), Utilidades.eRegistrar.FINAL, "actualizaInterfaz", False)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  ''' <param name="isPostBack"></param>
  ''' <remarks>Por VSR, 26/03/2009</remarks>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    Dim indiceJSON As String = Me.ViewState.Item("indiceJSON")

    Me.btnEliminar.Attributes.Add("onclick", "parent.Usuario.eliminarTelefonoPorUsuario({ indiceJSON: '" & indiceJSON & "', invocadoDesde: Usuario.FORMULARIO_TELEFONO_POR_USUARIO })")
    Me.btnCrear.Attributes.Add("onclick", "return(Usuario.validarFormularioTelefonoPorUsuario({ indiceJSON: '" & indiceJSON & "' }))")
    Me.btnModificar.Attributes.Add("onclick", "return(Usuario.validarFormularioTelefonoPorUsuario({ indiceJSON: '" & indiceJSON & "' }))")

    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  ''' <remarks>Por VSR, 04/08/2009</remarks>
  Private Sub VerificarPermisos()
    Dim indiceJSON As String = Me.ViewState.Item("indiceJSON")

    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnCrear.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                          indiceJSON = "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Crear.ToString)
    Me.btnModificar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                              indiceJSON <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Modificar.ToString)
    Me.btnEliminar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                             indiceJSON <> "-1" And Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Eliminar.ToString)

    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el usuario en base de datos
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef jsonTelefono As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idTelefonoPorUsuario As String = Me.ViewState.Item("idTelefonoPorUsuario")
    Dim msgError As String = ""
    Dim telefono, horaInicio, minutosInicio, horaTermino, minutosTermino As String
    Dim dcJSON As New Dictionary(Of String, String)

    Try
      telefono = Utilidades.IsNull(Me.txtTelefono.Text, "")
      horaInicio = Utilidades.IsNull(Me.wucHoraInicio.SelectedValueHora, "")
      minutosInicio = Utilidades.IsNull(Me.wucHoraInicio.SelectedValueMinutos, "")
      horaTermino = Utilidades.IsNull(Me.wucHoraTermino.SelectedValueHora, "")
      minutosTermino = Utilidades.IsNull(Me.wucHoraTermino.SelectedValueMinutos, "")

      'construye json de retorno
      dcJSON.Add("IdTelefonoPorUsuario", idTelefonoPorUsuario)
      dcJSON.Add("Telefono", telefono)
      dcJSON.Add("HoraInicio", horaInicio & minutosInicio)
      dcJSON.Add("HoraTermino", horaTermino & minutosTermino)
      jsonTelefono = MyJSON.ConvertObjectToJSON(dcJSON)

      If idTelefonoPorUsuario = "-1" Then
        status = "ingresado"
      Else
        status = "modificado"
      End If

    Catch ex As Exception
      msgError = "Error al grabar datos del tel&eacute;fono.<br/>"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idTelefonoPorUsuario As String = Me.ViewState.Item("idTelefonoPorUsuario")
    Dim indiceJSON As String = Me.ViewState.Item("indiceJSON")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""
    Dim jsonTelefono As String = ""
    Dim json As String

    'grabar valores
    textoMensaje &= GrabarDatosRegistro(status, jsonTelefono)
    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}El TELEFONO fue ingresado satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}El TELEFONO fue actualizado satisfactoriamente"
        Case "duplicado"
          textoMensaje = "{DUPLICADO}El TELEFONO ya existe. Ingrese otro por favor"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el TELEFONO. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo ingresar el TELEFONO. Intente nuevamente por favor.<br />" & textoMensaje
    End If

    If status = "ingresado" Or status = "modificado" Then
      json = "{"
      json &= " indiceJSON: """ & indiceJSON & """"
      json &= ",jsonTelefono: " & jsonTelefono
      json &= "}"

      Utilidades.RegistrarScript(Me.Page, "Usuario.grabarTelefonoPorPersona(" & json & ");", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

      queryStringPagina &= "idTelefonoPorUsuario=" & Server.UrlEncode(idTelefonoPorUsuario)
      queryStringPagina &= "&indiceJSON=" & Server.UrlEncode(indiceJSON)
      Response.Redirect("./Mantenedor.TelefonoPorUsuario.aspx?" & queryStringPagina)
    End If
  End Sub

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim indiceJSON As String = Utilidades.IsNull(Request("indiceJSON"), "-1")
    Dim idTelefonoPorUsuario As String = Utilidades.IsNull(Request("idTelefonoPorUsuario"), "-1")

    Me.ViewState.Add("indiceJSON", indiceJSON)
    Me.ViewState.Add("idTelefonoPorUsuario", idTelefonoPorUsuario)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ActualizaInterfaz()
      InicializaControles(Page.IsPostBack)
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnCrear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrear.Click
    GrabarRegistro()
  End Sub

  Protected Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
    GrabarRegistro()
  End Sub

End Class
﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OT.RespuestaAlertaRojaDetalle.aspx.vb" Inherits="webtransportewalmartmx.OT_RespuestaAlertaRojaDetalle" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<%@ Register Src="~/wuc/wucMultiselectCombinados.ascx" TagName="wucMultiselectCombinados" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Respuesta Alerta Roja</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>
</head>
<body>
  <form id="form1" runat="server">
    <asp:HiddenField ID="hRechazado" runat="server" value="0"/>
    <asp:HiddenField ID="hJsonRechazado" runat="server" value="[]"/>
    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3" Text="Respuesta alerta roja L&iacute;nea de Transporte:" Font-Bold="true"></asp:Label>&nbsp;<asp:Label ID="lblNombreTransportista" runat="server" CssClass="text-danger sist-font-size-24" Font-Bold="true"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger sist-font-size-20" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server" CssClass="small">
          <asp:Literal ID="ltlListado" runat="server"></asp:Literal>
        </asp:Panel>
        
        <asp:Panel ID="pnlEstadoTransportista" runat="server">
          <div class="panel panel-primary">
            <div class="panel-heading"><strong>Estado L&iacute;nea de Transporte</strong></div>
            <div class="panel-body">

              <asp:Panel ID="pnlEstadoTransportistaSugeridos" runat="server">
                <div><strong>Estado L&iacute;nea de Transporte <span class="label label-warning">SUGERIDOS</span></strong>&nbsp;<em class="small"><asp:Label ID="lblFechaCreacionSugerido" runat="server"></asp:Label></em></div>
                <div class="form-group">
                  <asp:Literal ID="ltlEstadoTransportistaSugerido" runat="server"></asp:Literal>
                </div>
                <hr />
              </asp:Panel>

              <asp:Panel ID="pnlEstadoTransportistaAprobados" runat="server">
                <div><strong>Estado L&iacute;nea de Transporte <span class="label label-success">APROBADOS</span></strong>&nbsp;<em class="small"><asp:Label ID="lblFechaCreacionAprobado" runat="server"></asp:Label></em></div>
                <div class="form-group">
                  <asp:Literal ID="ltlEstadoTransportistaAprobado" runat="server"></asp:Literal>
                </div>
                <hr />
              </asp:Panel>

              <asp:Panel ID="pnlEstadoTransportistaMultiselect" runat="server">
                <div class="form-group"><asp:Label ID="lblDescripcionEstadoTransportistaMultiselect" runat="server"></asp:Label></div>
                <uc2:wucMultiselectCombinados id="msEstado" runat="server" TextLabelPadre="Estado" VisibleLabelAyuda="true" VisibleCheckTodosPadre="false" VisibleCheckTodosHija="false" RowsListBoxHija="10" RowsListBoxPadre="10"></uc2:wucMultiselectCombinados>
                <div id="lblMensajeErrormsEstado"></div>
                <hr />
              </asp:Panel>

              <div class="form-group row">
                <div class="col-xs-3">
                  <label class="control-label">Monto a Recuperar</label>
                  <asp:TextBox ID="txtMontoUnoTransportista" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-xs-3">
                  <label class="control-label">Monto Recuperado</label>
                  <asp:TextBox ID="txtMontoDosTransportista" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
                  <div id="hMensajeErrorMontoDosTransportista"></div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label">Observaci&oacute;n</label>
                <asp:TextBox ID="txtObservacion" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="2"></asp:TextBox>
              </div>
            </div>
          </div>
        </asp:Panel>

        <div class="form-group text-center">
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Alerta.cerrarPopUp()"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnGrabar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Alerta.validarFormularioRespuestaAlertaRoja())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Grabar</asp:LinkButton>
          <asp:LinkButton ID="btnFinalizarPlanAccion" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Alerta.validarFormularioRespuestaAlertaRoja())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Finalizar y Aprobar Plan de acci&oacute;n</asp:LinkButton>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".chosen-select").select2();

      });
    </script>

  </form>
</body>
</html>

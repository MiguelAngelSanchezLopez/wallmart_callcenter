﻿Imports CapaNegocio
Imports System.Data

Public Class GestionOperacion_DatoExtra
  Inherits System.Web.UI.Page

#Region "Constantes y Enum"
  Private Enum eFunciones
    Ver
    Grabar
  End Enum

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idRegistro As String = Utilidades.IsNull(Request("idRegistro"), "-1")
    Dim entidad As String = Utilidades.IsNull(Request("entidad"), "")
    Dim bloque As String = Utilidades.IsNull(Request("bloque"), "")
    Dim fechaHoraPresentacion As String = Server.UrlDecode(Utilidades.IsNull(Request("fechaHoraPresentacion"), ""))
    Dim campoMostrar As String = Utilidades.IsNull(Request("campoMostrar"), "")

    Me.ViewState.Add("idRegistro", idRegistro)
    Me.ViewState.Add("entidad", entidad)
    Me.ViewState.Add("bloque", bloque)
    Me.ViewState.Add("fechaHoraPresentacion", fechaHoraPresentacion)
    Me.ViewState.Add("campoMostrar", campoMostrar)

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(oUsuario.Id, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())
      ObtenerDatos()
      InicializaControles(Page.IsPostBack)
      ActualizaInterfaz()
      MostrarMensajeUsuario()
    End If
  End Sub

  Protected Sub btnGrabar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    GrabarRegistro()
  End Sub

#Region "Private"
  ''' <summary>
  ''' obtiene los registros desde la base de datos
  ''' </summary>
  Private Sub ObtenerDatos()
    Dim ds As New DataSet
    Dim idRegistro As String = Me.ViewState.Item("idRegistro")
    Dim entidad As String = Me.ViewState.Item("entidad")

    Try
      Select Case entidad
        Case GestionOperacion.IMPORTACION
          ds = GestionOperacion.ObtenerDatoExtraImportacion(idRegistro)
        Case GestionOperacion.EXPORTACION
          ds = GestionOperacion.ObtenerDatoExtraExportacion(idRegistro)
        Case Else
          ds = Nothing
      End Select
    Catch ex As Exception
      ds = Nothing
    End Try
    Session("ds") = ds
  End Sub

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  Private Sub ActualizaInterfaz()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim ds As DataSet = CType(Session("ds"), DataSet)
    Dim idRegistro As String = Me.ViewState.Item("idRegistro")
    Dim entidad As String = Me.ViewState.Item("entidad")
    Dim bloque As String = Me.ViewState.Item("bloque")
    Dim fechaHoraPresentacion As String = Me.ViewState.Item("fechaHoraPresentacion")
    Dim campoMostrar As String = Me.ViewState.Item("campoMostrar")
    Dim fechaLlegada, horaLlegada, observacionLlegada, cantidadPallet, pesoReal, ordenCompra, fechaSalida, horaSalida, nroSello, nroContenedor As String
    Dim totalRegistros As Integer
    Dim dr As DataRow
    Dim sbScript As New StringBuilder

    'inicializa controles en vacio
    fechaLlegada = ""
    horaLlegada = ""
    observacionLlegada = ""
    cantidadPallet = "-1"
    pesoReal = "-1"
    ordenCompra = "-1"
    fechaSalida = ""
    horaSalida = ""
    nroSello = ""
    nroContenedor = ""

    Try
      If Not ds Is Nothing Then
        totalRegistros = ds.Tables(0).Rows.Count
        If totalRegistros > 0 Then
          dr = ds.Tables(0).Rows(0)
          fechaLlegada = dr.Item("FechaLlegada")
          horaLlegada = dr.Item("HoraLlegada")
          observacionLlegada = dr.Item("ObservacionLlegada")
          cantidadPallet = dr.Item("CantidadPallet")
          pesoReal = dr.Item("PesoReal")
          ordenCompra = dr.Item("OrdenCompra")
          fechaSalida = dr.Item("FechaSalida")
          horaSalida = dr.Item("HoraSalida")
          nroSello = dr.Item("NumeroSello")
          nroContenedor = dr.Item("NumeroContenedor")
        End If
      End If
    Catch ex As Exception
      fechaLlegada = ""
      horaLlegada = ""
      observacionLlegada = ""
      cantidadPallet = "-1"
      pesoReal = "-1"
      ordenCompra = "-1"
      fechaSalida = ""
      horaSalida = ""
      nroSello = ""
      nroContenedor = ""
    End Try

    'asigna valores
    Me.lblIdRegistro.Text = entidad.ToUpper() & " / Fecha Presentaci&oacute;n: " & fechaHoraPresentacion
    Me.txtIdRegistro.Value = idRegistro
    Me.txtEntidad.Value = entidad
    Me.txtHoraLlegada.Text = horaLlegada
    Me.txtObservacionLlegada.Text = observacionLlegada
    Me.txtCantidadPallet.Text = IIf(cantidadPallet = "-1", "", cantidadPallet)
    Me.txtPesoReal.Text = IIf(pesoReal = "-1", "", pesoReal)
    Me.txtOrdenCompra.Text = IIf(ordenCompra = "-1", "", ordenCompra)
    Me.txtHoraSalida.Text = horaSalida
    Me.txtNroSello.Text = nroSello
    Me.txtNroContenedor.Text = nroContenedor

    If (String.IsNullOrEmpty(fechaLlegada)) Then
      If (campoMostrar = GestionOperacion.CAMPO_MOSTRAR_FECHA_LLEGADA) Then
        Me.txtFechaLlegada.Text = Left(fechaHoraPresentacion, 10)
      Else
        Me.txtFechaLlegada.Text = fechaLlegada
      End If
    Else
      Me.txtFechaLlegada.Text = fechaLlegada
    End If

    If (String.IsNullOrEmpty(fechaSalida)) Then
      If (campoMostrar = GestionOperacion.CAMPO_MOSTRAR_FECHA_LLEGADA) Then
        Me.txtFechaSalida.Text = Left(fechaHoraPresentacion, 10)
      Else
        Me.txtFechaSalida.Text = fechaSalida
      End If
    Else
      Me.txtFechaSalida.Text = fechaSalida
    End If

    sbScript.Append("GestionOperacion.actualizaInterfazDatoExtra({ bloque:'" & bloque & "', entidad:'" & entidad & "', campoMostrar:'" & campoMostrar & "' });")
    Utilidades.RegistrarScript(Me.Page, sbScript.ToString(), Utilidades.eRegistrar.FINAL, "script_ActualizaInterfaz", False)
  End Sub

  ''' <summary>
  ''' inicializa algunos controles cuando se carga por primera vez la pagina
  ''' </summary>
  Private Sub InicializaControles(ByVal isPostBack As Boolean)
    'verifica los permisos sobre los controles
    VerificarPermisos()
  End Sub

  ''' <summary>
  ''' verifica los permisos sobre los controles
  ''' </summary>
  Private Sub VerificarPermisos()
    'verifica los permisos para los controles
    Me.pnlContenido.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString)
    Me.btnGrabar.Visible = Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Ver.ToString) And _
                           Utilidades.TienePermiso(Utilidades.ObtenerNombrePaginaActual, eFunciones.Grabar.ToString)
    If Not Me.pnlContenido.Visible Then
      Me.pnlMensajeAcceso.Visible = True
      Utilidades.setPanelMensajeUsuario(Me.pnlMensajeAcceso, "No tiene permiso para ver esta p&aacute;gina", Utilidades.eTipoMensajeAlert.Danger)
    End If
  End Sub

  ''' <summary>
  ''' muestra mensaje al usuario
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MostrarMensajeUsuario()
    Dim mensaje As String

    Try
      mensaje = Utilidades.IsNull(Session(Utilidades.KEY_SESION_MENSAJE), "")
      If (Not String.IsNullOrEmpty(mensaje)) Then

        'asigna mensaje al panel
        Me.pnlMensajeUsuario.Visible = True

        'dependiendo de la marca especial que tenga el sistema es como se muestra el mensaje al usuario
        If InStr(mensaje, "{INGRESADO}") > 0 Then
          mensaje = mensaje.Replace("{INGRESADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{MODIFICADO}") > 0 Then
          mensaje = mensaje.Replace("{MODIFICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{ELIMINADO}") > 0 Then
          mensaje = mensaje.Replace("{ELIMINADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        ElseIf InStr(mensaje, "{DUPLICADO}") > 0 Then
          mensaje = mensaje.Replace("{DUPLICADO}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        ElseIf InStr(mensaje, "{ERROR}") > 0 Then
          mensaje = mensaje.Replace("{ERROR}", "")
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Danger)
        Else
          Utilidades.setPanelMensajeUsuario(Me.pnlMensajeUsuario, mensaje, Utilidades.eTipoMensajeAlert.Success, True)
        End If
      End If

    Catch ex As Exception
      Me.pnlMensajeUsuario.Visible = False
    End Try

    Session(Utilidades.KEY_SESION_MENSAJE) = Nothing
  End Sub

  ''' <summary>
  ''' graba o actualiza el registro en base de datos
  ''' </summary>
  Private Function GrabarDatosRegistro(ByRef status As String, ByRef idRegistro As String, ByVal entidad As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim msgError As String = ""
    Dim fechaLlegada, horaLlegada, observacionLlegada, cantidadPallet, pesoReal, ordenCompra, fechaSalida, horaSalida, nroSello, nroContenedor As String

    Try
      fechaLlegada = Utilidades.IsNull(Me.txtFechaLlegada.Text, "")
      horaLlegada = Utilidades.IsNull(Me.txtHoraLlegada.Text, "")
      observacionLlegada = Utilidades.IsNull(Me.txtObservacionLlegada.Text, "")
      cantidadPallet = Utilidades.IsNull(Me.txtCantidadPallet.Text, "-1")
      pesoReal = Utilidades.IsNull(Me.txtPesoReal.Text, "-1")
      ordenCompra = Utilidades.IsNull(Me.txtOrdenCompra.Text, "-1")
      fechaSalida = Utilidades.IsNull(Me.txtFechaSalida.Text, "")
      horaSalida = Utilidades.IsNull(Me.txtHoraSalida.Text, "")
      nroSello = Utilidades.IsNull(Me.txtNroSello.Text, "")
      nroContenedor = Utilidades.IsNull(Me.txtNroContenedor.Text, "")

      Sistema.GrabarLogSesion(oUsuario.Id, "Actualiza datos extras: IdRegistro " & idRegistro & " / Entidad " & entidad)
      pesoReal = pesoReal.Replace(".", ",")

      Select Case entidad
        Case GestionOperacion.IMPORTACION
          status = GestionOperacion.GrabarDatosExtraImportacion(idRegistro, fechaLlegada, horaLlegada, observacionLlegada, cantidadPallet, pesoReal, ordenCompra, _
                                                                fechaSalida, horaSalida, nroSello, nroContenedor)
        Case GestionOperacion.EXPORTACION
          status = GestionOperacion.GrabarDatosExtraExportacion(idRegistro, fechaLlegada, horaLlegada, observacionLlegada, cantidadPallet, pesoReal, ordenCompra, _
                                                                fechaSalida, horaSalida, nroSello, nroContenedor)
      End Select
    Catch ex As Exception
      msgError = "Se produjo un error interno, no se pudo actualizar los datos extras.<br />" & ex.Message
      status = "error"
    End Try
    'retorna valor
    Return msgError
  End Function

  ''' <summary>
  ''' graba los datos del registro
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GrabarRegistro()
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim idRegistro As String = Me.ViewState.Item("idRegistro")
    Dim entidad As String = Me.ViewState.Item("entidad")
    Dim bloque As String = Me.ViewState.Item("bloque")
    Dim fechaHoraPresentacion As String = Me.ViewState.Item("fechaHoraPresentacion")
    Dim campoMostrar As String = Me.ViewState.Item("campoMostrar")
    Dim status As String = ""
    Dim textoMensaje As String = ""
    Dim grabadoConExito As Boolean = True
    Dim queryStringPagina As String = ""

    'grabar valores
    textoMensaje &= GrabarDatosRegistro(status, idRegistro, entidad)

    If textoMensaje <> "" Then
      grabadoConExito = False
    Else
      'hace algo bien
    End If

    'muestra mensaje dependiendo si se grabo o no
    If grabadoConExito Then
      Select Case status
        Case "ingresado"
          textoMensaje = "{INGRESADO}Los datos fueron ingresados satisfactoriamente"
        Case "modificado"
          textoMensaje = "{MODIFICADO}Los datos fue actualizados satisfactoriamente"
        Case "error"
          textoMensaje = "{ERROR}Se produjo un error interno y no se pudo actualizar los datos. Intente nuevamente por favor"
      End Select
    Else
      textoMensaje = "{ERROR}Se produjo un error interno y no se pudo actualizar los datos. Intente nuevamente por favor.<br />" & textoMensaje
    End If
    Session(Utilidades.KEY_SESION_MENSAJE) = textoMensaje

    If status = "ingresado" Or status = "modificado" Then
      Utilidades.RegistrarScript(Me.Page, "GestionOperacion.cerrarPopUp('1');", Utilidades.eRegistrar.FINAL, "cerrarPopUp")
    Else
      queryStringPagina = "idRegistro=" & idRegistro
      queryStringPagina &= "&entidad=" & entidad
      queryStringPagina &= "&bloque=" & bloque
      queryStringPagina &= "&fechaHoraPresentacion=" & Server.UrlDecode(fechaHoraPresentacion)
      queryStringPagina &= "&campoMostrar=" & campoMostrar
      Response.Redirect("./GestionOperacion.DatoExtra.aspx?" & queryStringPagina)
    End If
  End Sub

#End Region

End Class
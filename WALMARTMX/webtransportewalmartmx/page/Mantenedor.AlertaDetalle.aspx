﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Mantenedor.AlertaDetalle.aspx.vb" Inherits="webtransportewalmartmx.Mantenedor_AlertaDetalle" %>
<%@ Register Src="../wuc/wucCombo.ascx" TagName="wucCombo" TagPrefix="uc1" %>
<%@ Register Src="../wuc/wucHora.ascx" TagName="wucHora" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Detalle P&aacute;gina</title>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" type="text/css" href="../css/jquery.ui.css" />
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-cerulean.css" />
  <link rel="stylesheet" type="text/css" href="../css/fancybox.css" />
  <link rel="stylesheet" type="text/css" href="../css/select2.css" />
  <link rel="stylesheet" type="text/css" href="../css/sistema.css" />
  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.ui.js"></script>
  <script type="text/javascript" src="../js/bootstrap.js"></script>
  <script type="text/javascript" src="../js/jquery.select2.js"></script>
  <script type="text/javascript" src="../js/jquery.fancybox.js"></script>
  <script type="text/javascript" src="../js/jquery.mask.js"></script>
  <script type="text/javascript" src="../js/sistema.js"></script>
  <script type="text/javascript" src="../js/validacion.js"></script>
  <script type="text/javascript" src="../js/combo.js"></script>
  <script type="text/javascript" src="../js/alerta.js"></script>

  <style type="text/css">
    .popover{ max-width:60%; }
  </style>
</head>
<body>
  <form id="form1" runat="server">
    <input type="hidden" id="txtIdAlertaConfiguracion" runat="server" value="-1" />
    <input type="hidden" id="txtJSONEscalamientos" runat="server" value="" />
    <input type="hidden" id="txtTotalGrupos" runat="server" value="0" />
    <input type="hidden" id="txtJSONPadreHijo" runat="server" value="" />
    <input type="hidden" id="txtJSONPerfilesPorFormato" runat="server" value="[]" />
    <input type="hidden" id="txtIdFormatoActual" runat="server" value="" />
    <input type="hidden" id="txtFechaInicioAnterior" runat="server" value="" />
    <input type="hidden" id="txtHoraInicioAnterior" runat="server" value="" />

    <div class="container sist-margin-top-10">
      <div class="form-group text-center">
        <asp:Label ID="lblTituloFormulario" runat="server" CssClass="h3"></asp:Label>
        <div><asp:Label ID="lblIdRegistro" runat="server" CssClass="text-danger" Font-Bold="true"></asp:Label></div>
        <asp:Panel ID="pnlMensajeUsuario" Runat="server"></asp:Panel>
      </div>

      <div class="form-group">
        <asp:Panel ID="pnlMensajeAcceso" Runat="server"></asp:Panel>
        <asp:Panel ID="pnlContenido" Runat="server">

          <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><strong>DATOS ALERTA</strong></h3></div>
            <div class="panel-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-3">
                    <label class="control-label">Formato<strong class="text-danger">&nbsp;*</strong></label>
                    <uc1:wucCombo id="ddlFormato" runat="server" FuenteDatos="Tabla" TipoCombo="Formato" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
                  </div>
                  <div class="col-xs-5">
                    <label class="control-label">Nombre<strong class="text-danger">&nbsp;*</strong></label>
                    <select id="ddlNombre" runat="server" class="form-control" data-categoria="combo-nombre-alerta"><option value="">--Seleccione--</option></select>
                    <span id="hErrorNombre" runat="server"></span>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Tipo Alerta<strong class="text-danger">&nbsp;*</strong></label>
                    <uc1:wucCombo id="ddlTipoAlerta" runat="server" FuenteDatos="TipoGeneral" TipoCombo="TipoAlerta" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Prioridad<strong class="text-danger">&nbsp;*</strong></label>
                    <uc1:wucCombo id="ddlPrioridad" runat="server" FuenteDatos="Tabla" TipoCombo="Prioridad" ItemSeleccione="true" CssClass="form-control chosen-select"></uc1:wucCombo>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-2">
                    <label class="control-label">Frecuencia Repetici&oacute;n<strong class="text-danger">&nbsp;*</strong></label>
                    <div class="input-group">
                      <asp:TextBox ID="txtFrecuenciaRepeticion" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                      <span class="input-group-addon">min.</span>
                    </div>
                    <div id="hMensajeErrorFrecuenciaRepeticion"></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Fecha Inicio</label>
                    <asp:TextBox ID="txtFechaInicio" runat="server" MaxLength="10" CssClass="form-control date-pick"></asp:TextBox>
                    <div id="lblMensajeErrorFechaInicio" runat="server"></div>
                  </div>
                  <div class="col-xs-2">
                    <label class="control-label">Hora Inicio</label>
                    <uc2:wucHora ID="wucHoraInicio" runat="server" />
                    <div id="lblMensajeErrorHoraInicio" runat="server"></div>
                  </div>
                  <div class="col-xs-1">
                    <label class="control-label">Activo</label>
                    <div class="sist-padding-top-8">
                      <asp:CheckBox ID="chkActivo" runat="server" CssClass="control-label" />
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>        

          <div>
            <div id="hEscalamientos"></div>

            <!-- boton nuevo escalamiento -->
            <div class="btn btn-primary pull-right" onclick="Alerta.agregarEscalamiento({ json: null })"><span class="glyphicon glyphicon-plus"></span>&nbsp;Agregar nuevo escalamiento</div>
          </div>
        </asp:Panel>
        
        <div class="form-group text-center sist-clear-both">
          <br />
          <div id="btnCerrar" runat="server" class="btn btn-lg btn-default" onclick="Alerta.cerrarPopUpMantenedor('1')"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar</div>
          <asp:LinkButton ID="btnEliminar" runat="server" CssClass="btn btn-lg btn-danger" OnClientClick="return(Alerta.validarEliminarMantenedor())"><span class="glyphicon glyphicon-trash"></span>&nbsp;Eliminar</asp:LinkButton>
          <asp:LinkButton ID="btnCrear" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Alerta.validarFormularioMantenedor())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Crear</asp:LinkButton>
          <asp:LinkButton ID="btnModificar" runat="server" CssClass="btn btn-lg btn-primary" OnClientClick="return(Alerta.validarFormularioMantenedor())"><span class="glyphicon glyphicon-ok"></span>&nbsp;Modificar</asp:LinkButton>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      jQuery(document).ready(function () {
        jQuery(".chosen-select").select2();

        jQuery(".date-pick").datepicker({
          changeMonth: true,
          changeYear: true
        });

      });
    </script>

  </form>
</body>
</html>


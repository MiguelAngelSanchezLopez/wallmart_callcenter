﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Carrier_EmbarqueEnRuta

  '''<summary>
  '''Control Head1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

  '''<summary>
  '''Control form1.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

  '''<summary>
  '''Control lblSemaforo.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblSemaforo As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblIdMaster.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblIdMaster As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblCedis.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblCedis As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblPlacaRemolque.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblPlacaRemolque As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblPlacaTracto.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblPlacaTracto As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblDeterminante.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblDeterminante As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblTiempoViaje.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTiempoViaje As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblEta.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblEta As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblKilometrosRecorridos.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblKilometrosRecorridos As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblTotalAlertas.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblTotalAlertas As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control lblRegistros.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents lblRegistros As Global.System.Web.UI.WebControls.Label

  '''<summary>
  '''Control gvPrincipal.
  '''</summary>
  '''<remarks>
  '''Campo generado automáticamente.
  '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
  '''</remarks>
  Protected WithEvents gvPrincipal As Global.System.Web.UI.WebControls.GridView
End Class

﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports CapaNegocio
Imports System.Web.Script.Serialization

Public Class wmEcommerce
  Inherits System.Web.UI.Page

  Private Shared Function ValidateColumns(ByVal dtExcel As DataTable) As String
    Dim columns As New List(Of String)
    Dim columnsNotFound As New List(Of String)
    columns.Add("fecha")
    columns.Add("ordernumber")
    columns.Add("pkt")
    columns.Add("upc")
    columns.Add("styledesc")
    columns.Add("qty")
    columns.Add("shiptoname")
    columns.Add("customrcdexpansionfield")
    columns.Add("shipto_state")
    columns.Add("zip")
    columns.Add("tel")
    columns.Add("domicilio")

    'Valida error en la obtención
    If (dtExcel Is Nothing) Then
      Return "Ha ocurrido un error al intentar obtener los datos del archivo."
    End If

    'Valida que existan registros
    If (dtExcel.Rows.Count = 0) Then
      Return "No se encontraron registros para cargar."
    End If

    'Valida cantidad de columnas
    If dtExcel.Columns.Count < columns.Count Then
      Return "El n&uacute;mero de columnas no es válido, deben ser " & columns.Count
    End If

    'Valida que esten todas las columnas
    For Each nombre As String In columns
      If Not dtExcel.Columns.Contains(nombre) Then
        columnsNotFound.Add("- " & nombre)
      End If
    Next

    If (columnsNotFound.Count > 0) Then
      Return "Faltan las siguientes columnas:<br />" & String.Join("<br />", columnsNotFound.ToArray())
    End If

    Return String.Empty

  End Function

  Private Shared Function LoadDataTable(ByVal filePath As String, ByVal extension As String) As DataTable
    Dim conStr As String = ""
    Dim cmdExcel As New OleDbCommand()
    Dim oda As New OleDbDataAdapter()
    Dim dtExcel As New DataTable()

    conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source= {0};Extended Properties=""Excel 12.0;hdr=YES;imex=1"""
    conStr = String.Format(conStr, filePath)

    'Read Data from First Sheet
    Dim nameSheet As String
    Dim connExcel As New OleDbConnection(conStr)
    cmdExcel.Connection = connExcel
    connExcel.Open()
    'obtengo nombre de la hoja
    nameSheet = connExcel.GetSchema("Tables").Rows(0)("TABLE_NAME")
    cmdExcel.CommandText = "SELECT * From [" + nameSheet + "]"
    oda.SelectCommand = cmdExcel
    oda.Fill(dtExcel)
    connExcel.Close()

    Return dtExcel

  End Function

#Region "WebMethod"
  <System.Web.Services.WebMethod()> _
  Public Shared Function LoadFileExcel(ByVal fileName As String) As String
    Try
      Dim status As String
      Dim oUtilidades As New Utilidades
      Dim filePath As String = System.Web.HttpContext.Current.Server.MapPath("~/fileTemp/" + fileName)
      Dim extension As String = Path.GetExtension(filePath)
      Dim dtExcel As DataTable = LoadDataTable(filePath, extension)

      status = ValidateColumns(dtExcel)
      If (status = String.Empty) Then

        'Agrega campo idUsuario y nombre archivo
        dtExcel.Columns.Add("IdUsuarioCreacion")
        dtExcel.Columns.Add("NombreArchivo")

        For Each row As DataRow In dtExcel.Rows
          row("IdUsuarioCreacion") = oUtilidades.ObtenerUsuarioSession().Id
          row("NombreArchivo") = fileName
        Next

        'Se guarda en session para la asignacion
        HttpContext.Current.Session("dtExcel") = dtExcel

        'Borra el archivo en disco
        Dim deletFile As FileInfo = New FileInfo(filePath)
        deletFile.Delete()
      End If

      Return status

    Catch ex As Exception
      Return ex.Message
    End Try

  End Function

  <System.Web.Services.WebMethod()> _
  Public Shared Function SaveDataTable() As String

    Try

      'Borrar los archivos de la tabla
      'Carrier.EliminarReclamoPaso()

      'Cargo los nuevos datos
      Dim dtExcel As DataTable = HttpContext.Current.Session("dtExcel")
      Using bulkCopy As SqlBulkCopy = New SqlBulkCopy(Conexion.StringConexion)
        bulkCopy.DestinationTableName = "dbo.Ecommerce_Viajes"
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("FECHA", "Fecha"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ORDERNUMBER", "OrderNumber"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("PKT", "Pkt"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("UPC", "Upc"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("STYLEDESC", "StyleDesc"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("QTY", "Qty"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("SHIPTONAME", "ShipToName"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("CUSTOMRCDEXPANSIONFIELD", "CustomRcdExpansionField"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("SHIPTO_STATE", "ShipToState"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("ZIP", "Zip"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("TEL", "Tel"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("DOMICILIO", "Domicilio"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("IdUsuarioCreacion", "IdUsuarioCreacion"))
        bulkCopy.ColumnMappings.Add(New SqlBulkCopyColumnMapping("NombreArchivo", "NombreArchivo"))
        bulkCopy.BulkCopyTimeout = 0
        ' Write from the source to the destination.
        bulkCopy.WriteToServer(dtExcel)
      End Using

      'Guarda Log
      Dim oUtilidades As New Utilidades
      Sistema.GrabarLogSesion(oUtilidades.ObtenerUsuarioSession().Id, "Se graba registros en tabla Ecommerce_Viajes")

      'Elimina la variable
      HttpContext.Current.Session.Remove("dtExcel")

      Return String.Empty

    Catch ex As Exception
      Return ex.Message
    End Try

  End Function

  <System.Web.Services.WebMethod()> _
  Public Shared Function ObtenerInfoPatente(ByVal patente As String) As String

    Try
      Dim dDatos As New Dictionary(Of String, String)
      Dim dsResult As DataSet = Viaje.ObtenerInfoPatenteEcommerce(patente)

      If (Not dsResult Is Nothing) Then

        If (dsResult.Tables(0).Rows.Count > 0) Then
          Dim señalGps As String = dsResult.Tables(0).Rows(0).Item("SeñalGps")
          dDatos.Add("SeñalGps", señalGps)
        End If

        If (dsResult.Tables(1).Rows.Count > 0) Then
          Dim cedis As String = dsResult.Tables(1).Rows(0).Item("Cedis")
          dDatos.Add("Cedis", cedis)
        End If

        If (dsResult.Tables(2).Rows.Count > 0) Then
          Dim nombreTransportista As String = dsResult.Tables(2).Rows(0).Item("NombreTransportista")
          dDatos.Add("NombreTransportista", nombreTransportista)
        End If

      End If

      Dim serializer As JavaScriptSerializer = New JavaScriptSerializer()

      Return serializer.Serialize(dDatos)

    Catch ex As Exception
      Return String.Empty
    End Try

  End Function

#End Region
End Class
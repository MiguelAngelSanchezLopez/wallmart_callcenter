﻿Imports CapaNegocio
Imports CapaNegocio.Herramientas
Imports System.Web.Services
Imports System.Web.Script.Services

Public Class api_test
  Inherits System.Web.UI.Page

#Region "Metodos ASPX"
  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

  End Sub
#End Region

#Region "WebMethod"
  <WebMethod()>
  <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
  Public Shared Function TestMetodo(ByVal texto As String) As Object
    Dim oRespuesta As New TipoDatos.Respuesta
    Dim oRetorno As New Dictionary(Of String, Object)
    Dim oDato As New Dictionary(Of String, Object)
    Dim oListado As New List(Of Dictionary(Of String, Object))
    Dim mensajeSinDatos As String = "No contiene ningun texto"
    Dim arrTexto As String()

    Try
      If (String.IsNullOrEmpty(texto)) Then
        oListado = Nothing
        oRespuesta.Codigo = Sistema.eCodigoSql.SinDatos
        oRespuesta.Descripcion = mensajeSinDatos
      Else
        arrTexto = texto.Split(" ")
        'obtiene listado de FAQ
        For Each s As String In arrTexto
          oDato = New Dictionary(Of String, Object)
          oDato.Add("Palabra", s)
          oListado.Add(oDato)
        Next

        oRespuesta.Codigo = Sistema.eCodigoSql.Exito
        oRespuesta.Descripcion = "El proceso fue realizado satisfactoriamente"
      End If
    Catch ex As Exception
      oListado = Nothing
      oRespuesta.Codigo = Sistema.eCodigoSql.Error
      oRespuesta.Descripcion = ex.Message
    End Try

    oRetorno.Add("Listado", oListado)
    oRetorno.Add("Respuesta", oRespuesta)
    Return oRetorno
  End Function
#End Region

End Class
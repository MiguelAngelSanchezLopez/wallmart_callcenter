﻿Imports CapaNegocio

Public Class InformeAlertasRojas
  Inherits System.Web.UI.Page

  ''' <summary>
  ''' actualiza los controles de la pagina segun los datos obtenidos
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ActualizaInterfaz()
    Dim rutaPlantilla, templatePlantilla As String
    Dim htmlItem As String
    Dim ds As New DataSet
    Dim sb As New StringBuilder
    Dim urlBase As String
    Dim rutTransportista As String
    Dim argCrypt As String = Request.QueryString("a")
    Dim textoDesencriptado As String = Criptografia.DesencriptarTripleDES(argCrypt)

    'Reemplazo caracteres
    textoDesencriptado = textoDesencriptado.Replace("\N", "Ñ")

    Dim arrParametros As Array = Criptografia.ObtenerArrayQueryString(textoDesencriptado)

    Try
      'idInforme = Me.ViewState.Item("idInforme")
      urlBase = Utilidades.ObtenerUrlBase()
      rutaPlantilla = Server.MapPath("../template/AlertasRojas.template.html")
      If Not Archivo.ExisteArchivoEnDisco(rutaPlantilla) Then
        templatePlantilla = ""
      Else
        templatePlantilla = Archivo.ObtenerStringPlantilla(rutaPlantilla)
      End If

      rutTransportista = Criptografia.RequestQueryString(arrParametros, "rutTransportista")
      ds = Alerta.ObtenerDetalleAlertasRojasPorTransportista(rutTransportista, False)

      If Not ds Is Nothing Then

        htmlItem = ObtenerHTMLItem(ds, templatePlantilla)

        sb.AppendLine(htmlItem)

        Me.ltlContenido.Text = sb.ToString()

      End If
    Catch ex As Exception
      'Exit Sub
      Me.ltlContenido.Text = ex.Message
    End Try
  End Sub

  Private Function ObtenerHTMLItem(ByVal ds As DataSet, ByVal templatePlantilla As String) As String
    Dim templateTabla, templateDetalle, htmlTabla, htmlDetalle, html As String
    Dim dv As DataView
    Dim dr As DataRow
    Dim dsFiltrado As New DataSet
    Dim sbGeneral, sb As New StringBuilder
    Dim contadorIndice As Integer = 0
    Dim htmlContenido As String = ""
    Dim idAlerta, nroTransporte, montoDiscrepancia, patenteTracto, nombreTransportista, nombreConductor, categoriaAlerta, fechaCreacion, observacion, cdOrigen, localDestino, mapaAlerta As String

    Try
      html = templatePlantilla
      templateTabla = Utilidades.ExtraeTextoEntreDosPalabras(templatePlantilla, "<!-- begin_tabla -->", "<!-- end_tabla -->")
      templateDetalle = Utilidades.ExtraeTextoEntreDosPalabras(templatePlantilla, "<!-- begin_row -->", "<!-- end_row -->")

      '-------------------------------------------------------------------------------------------
      ' GENERAL
      '-------------------------------------------------------------------------------------------
      html = html.Replace("{$URL_BASE}", Utilidades.ObtenerUrlBase())
      html = html.Replace("{$FECHA_SISTEMA}", Utilidades.ConvFechaISOaDDMMAAAA(Utilidades.FechaISO))
      html = html.Replace("{$HORA_SISTEMA}", Utilidades.ConvHoraISOaHHMM(Utilidades.HoraISO))

      For Each dr In ds.Tables(0).Rows

        sb.Length = 0

        '-------------------------------------------------------------------------------------------
        ' Cabecera
        '-------------------------------------------------------------------------------------------
        nombreTransportista = dr("nombreTransportista")
        nroTransporte = dr("nroTransporte")
        montoDiscrepancia = dr("MontoDiscrepancia")

        htmlTabla = templateTabla

        html = html.Replace("{$NOMBRE_TRANSPORTISTA}", nombreTransportista)
        htmlTabla = htmlTabla.Replace("{$NRO_TRANSPORTE}", nroTransporte)
        htmlTabla = htmlTabla.Replace("{$MONTO_DISCREPANCIA}", montoDiscrepancia)

        'Filtro el detalle por numero de viaje
        dv = ds.Tables(1).DefaultView
        dv.RowFilter = "nroTransporte = '" & nroTransporte & "'"

        'Recorro el detalle
        For Each drView In dv
          '-------------------------------------------------------------------------------------------
          ' Detalle
          '-------------------------------------------------------------------------------------------
          idAlerta = drView.Item("idAlerta")
          patenteTracto = drView.Item("patenteTracto")
          nombreConductor = drView.Item("nombreConductor")
          categoriaAlerta = drView.Item("categoriaAlerta")
          fechaCreacion = drView.Item("FechaCreacion")
          observacion = drView.Item("Observacion")
          cdOrigen = drView.Item("CDOrigen")
          localDestino = drView.Item("LocalDestino")
          mapaAlerta = drView.Item("UrlMapa")

          'construye el html del item
          htmlDetalle = templateDetalle

          'reemplaza marcas especiales
          htmlDetalle = htmlDetalle.Replace("{$ID_ALERTA}", idAlerta)
          htmlDetalle = htmlDetalle.Replace("{$CATEGORIA_ALERTA}", categoriaAlerta)
          htmlDetalle = htmlDetalle.Replace("{$NOMBRE_CONDUCTOR}", nombreConductor)
          htmlDetalle = htmlDetalle.Replace("{$PATENTE_TRACTO}", patenteTracto)
          htmlDetalle = htmlDetalle.Replace("{$FECHA_CREACION}", fechaCreacion)
          htmlDetalle = htmlDetalle.Replace("{$OSERVACION}", observacion)
          htmlDetalle = htmlDetalle.Replace("{$CD_ORIGEN}", cdOrigen)
          htmlDetalle = htmlDetalle.Replace("{$LOCAL_DESTINO}", localDestino)
          htmlDetalle = htmlDetalle.Replace("{$URL_ALERTA}", mapaAlerta)

          sb.AppendLine(htmlDetalle)

        Next

        'Agrego el detalle
        htmlTabla = htmlTabla.Replace(templateDetalle, sb.ToString())

        sbGeneral.AppendLine(htmlTabla)
      Next

      html = html.Replace(templateTabla, sbGeneral.ToString())

      'Reemplazo acentos
      html = Herramientas.ReemplazarAcentos(html)

      Me.ltlContenido.Text = html

      Return html
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Function

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim argCrypt As String = Request.QueryString("a")
    Dim textoDesencriptado As String = Criptografia.DesencriptarTripleDES(argCrypt)
    Dim arrParametros As Array = Criptografia.ObtenerArrayQueryString(textoDesencriptado)
    Dim idUsuario As String = Criptografia.RequestQueryString(arrParametros, "idUsuario")

    If Not Page.IsPostBack Then
      Sistema.GrabarLogSesion(idUsuario, "Ingresa página: " & Utilidades.ObtenerNombrePaginaActual())

      ActualizaInterfaz()
    End If
  End Sub

End Class
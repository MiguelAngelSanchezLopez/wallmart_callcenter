﻿Public Class Oops
  Inherits System.Web.UI.Page

  Protected Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error

    Dim ex As System.Exception = Server.GetLastError()

    Trace.Write("Mensaje", ex.Message)
    Trace.Write("Origen", ex.Source)
    Trace.Write("Trace", ex.StackTrace)

    Response.Write("<p>Ocurrió un error al mostrar la pagina de error...</p>")
    Response.End()

    Context.ClearError()

  End Sub

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    DivDebug.Visible = False
    DisplayDetailedError()

  End Sub

  Function DisplayDetailedError() As Boolean

    Trace.Write("Oops.aspx", "Despliegue error con debug")

    Dim strMessage As String = String.Empty
    Dim strSource As String = String.Empty
    Dim strStackTrace As String = String.Empty
    Dim strQueryString As String = String.Empty
    Dim strDate As String = String.Empty

    ' el error esta guardado con cualquiera de los metodos del webconfig
    ' <add key="customErrorMethod" value="application/context/cookie/querystring/off" />
    Dim objErrorIOFactory As New Msdn.ErrorIO.ErrorIOFactory
    Dim objErrorBasket As Msdn.ErrorIO.IErrorIOHandler
    objErrorBasket = objErrorIOFactory.Create(System.Configuration.ConfigurationManager.AppSettings("paginaErrorMetodo"))
    objErrorBasket.Retrieve(strMessage, strSource, strStackTrace, strDate, strQueryString)

    ' cuando terminamos el despliegue de los datos del error, limpiamos la variable que lo trae
    objErrorBasket.Clear()

    strStackTrace = Regex.Replace(strStackTrace, "\n", "<br />")
    lblErrorTitulo.Text = "Información de Debug"
    lblErrorMensajeError.Text = strMessage
    lblErrorURLOrigen.Text = strSource
    lblErrorStack.Text = strStackTrace
    lblErrorFecha.Text = strDate
    lnkTrace.NavigateUrl = "#"

    Return True

  End Function

End Class
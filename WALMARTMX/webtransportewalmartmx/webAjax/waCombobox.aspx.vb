﻿Imports CapaNegocio

Public Class waCombobox
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim opcion As String = Server.UrlDecode(Request("op"))
    Dim respuesta As String

    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      Select Case opcion
        Case "DibujarCombo"
          respuesta = DibujarCombo()
        Case Else
          respuesta = Sistema.eCodigoSql.Error
      End Select
      Response.Write(respuesta)
    Catch ex As Exception
      Response.Write(Sistema.eCodigoSql.Error)
    End Try
  End Sub

#Region "DibujarCombo"
  ''' <summary>
  ''' obtiene listado de alertas por atender
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function DibujarCombo() As String
    Dim retorno As String = ""
    Dim ds As New DataSet
    Dim filtroId As String = ""
    Dim tipoCombo As String = Utilidades.IsNull(Request("tipoCombo"), "")
    Dim fuenteDatos As String = Utilidades.IsNull(Request("fuenteDatos"), "")

    Try
      If fuenteDatos = "TipoGeneral" Then
        ds = Sistema.ObtenerComboEstatico(tipoCombo)
      Else
        filtroId = Utilidades.IsNull(Request("filtroId"), "-1")
        ds = Sistema.ObtenerComboDinamico(tipoCombo, filtroId)
      End If

      retorno = MyJSON.ConvertDataTableToJSON(ds.Tables(0))
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try

    Return retorno
  End Function
#End Region
End Class
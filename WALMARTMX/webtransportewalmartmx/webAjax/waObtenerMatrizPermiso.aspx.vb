﻿Imports CapaNegocio
Imports System.Data

Partial Public Class waObtenerMatrizPermiso
  Inherits System.Web.UI.Page

#Region "Privado"
  ''' <summary>
  ''' dibuja la matriz de permiso asociada al perfil
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="desmarcarTodasFunciones"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 28/07/2009</remarks>
  Private Function DibujarMatrizPermiso(ByVal ds As DataSet, ByVal desmarcarTodasFunciones As String) As String
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<span class=""help-block""><i>No tiene p&aacute;ginas asociadas el perfil</i></span>"
    Dim totalRegistros, i As Integer
    Dim dr As DataRow
    Dim sb As New StringBuilder
    Dim idCategoria, nombreCategoria As String

    Try
      If ds Is Nothing Then
        html = htmlSinRegistro
      Else
        totalRegistros = ds.Tables(Perfil.ePagina.ListadoCategorias).Rows.Count
        If totalRegistros = 0 Then
          html = htmlSinRegistro
        Else
          sb.AppendLine("<div id=""hContenedorCategorias"">")
          sb.AppendLine("<input type=""hidden"" id=""txtTotalCategorias"" value=""" & totalRegistros & """ />")
          'recorre las categorias para ir dibujando su contenido
          For i = 0 To totalRegistros - 1
            dr = ds.Tables(Perfil.ePagina.ListadoCategorias).Rows(i)
            idCategoria = dr.Item("IdCategoria")
            nombreCategoria = dr.Item("NombreCategoria")
            sb.AppendLine(DibujarCategorias(ds, idCategoria, nombreCategoria, i + 1, desmarcarTodasFunciones))
          Next
          sb.AppendLine("</div>")
          'retorna valor
          html = sb.ToString
        End If
      End If
    Catch ex As Exception
      html = Sistema.eCodigoSql.Error
    End Try
    Return html
  End Function

  ''' <summary>
  ''' dibuja las categorias con sus paginas
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="idCategoria"></param>
  ''' <param name="nombreCategoria"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 28/07/2009</remarks>
  Private Function DibujarCategorias(ByVal ds As DataSet, ByVal idCategoria As String, ByVal nombreCategoria As String, _
                                     ByVal indiceCicloCategoria As String, ByVal desmarcarTodasFunciones As String) As String
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<span class=""help-block""><i>No tiene p&aacute;ginas asociadas a la categor&iacute;a</i></span>"
    Dim totalRegistros, i As Integer
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim sb As New StringBuilder
    Dim dsPaginas As New DataSet
    Dim idPagina, tituloPagina As String
    Dim prefijoControl As String = "Cat" & indiceCicloCategoria

    Try
      'obtiene listado de paginas
      dt = ds.Tables(Perfil.ePagina.ListadoPaginas)
      If dt Is Nothing Then
        html = htmlSinRegistro
      Else
        'filtra la vista por la categoria
        dv = dt.DefaultView
        dv.RowFilter = "IdCategoria = " & idCategoria
        'convierte la vista en dataset
        dsPaginas = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        totalRegistros = dsPaginas.Tables(0).Rows.Count
        If totalRegistros = 0 Then
          html = htmlSinRegistro
        Else
          sb.AppendLine("<div id=""" & prefijoControl & "_hTituloCategoria"" class=""panel panel-default"">")
          sb.AppendLine("  <div class=""panel-heading""><strong class=""text-muted"">CATEGOR&Iacute;A: " & nombreCategoria & "</strong></div>")
          sb.AppendLine("  <div class=""panel-body"">")
          sb.AppendLine("    <div id=""" & prefijoControl & "_hContenedorPagina"" class=""table-responsive"">")
          sb.AppendLine("      <input type=""hidden"" id=""" & prefijoControl & "_txtTotalPaginas"" value=""" & totalRegistros & """ />")

          sb.AppendLine("      <table class=""table"">")
          sb.AppendLine("        <thead>")
          sb.AppendLine("          <th style=""width:250px"">P&aacute;gina</th>")
          sb.AppendLine("          <th>Funciones</th>")
          sb.AppendLine("        </thead>")
          sb.AppendLine("        <tbody>")

          'recorre las paginas para ir dibujando su contenido
          For i = 0 To totalRegistros - 1
            dr = dsPaginas.Tables(0).Rows(i)
            idPagina = dr.Item("IdPagina")
            tituloPagina = dr.Item("TituloPagina")
            sb.AppendLine(DibujarPaginas(ds, idCategoria, idPagina, tituloPagina, indiceCicloCategoria, i + 1, desmarcarTodasFunciones))
          Next

          sb.AppendLine("        </tbody>")
          sb.AppendLine("      </table>")
          sb.AppendLine("    </div>")
          sb.AppendLine("  </div>")
          sb.AppendLine("</div>")

          'retorna valor
          html = sb.ToString
        End If
      End If
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
    Return html
  End Function

  ''' <summary>
  ''' dibuja las paginas con sus funciones
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <param name="idCategoria"></param>
  ''' <param name="idPagina"></param>
  ''' <param name="tituloPagina"></param>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 28/07/2009</remarks>
  Private Function DibujarPaginas(ByVal ds As DataSet, ByVal idCategoria As String, ByVal idPagina As String, ByVal tituloPagina As String, _
                                  ByVal indiceCicloCategoria As String, ByVal indiceCicloPagina As String, ByVal desmarcarTodasFunciones As String) As String
    Dim html As String = ""
    Dim htmlSinRegistro As String = "<span class=""help-block""><i>No tiene funciones asociadas a la p&aacute;gina</i></span>"
    Dim tagCheck As String = "<input id=""{$ID_CHECK}"" type=""checkbox"" name=""{$NAME_CHECK}"" value=""{$VALUE_CHECK}"" {$CHECKED} />&nbsp;<label for=""{$ID_CHECK}""><span style=""font-weight:normal"">{$LABEL_CHECK}</span></label>&nbsp;&nbsp;&nbsp;&nbsp;"
    Dim totalRegistros, i As Integer
    Dim dt As DataTable
    Dim dv As DataView
    Dim dr As DataRow
    Dim sb As New StringBuilder
    Dim dsFunciones As New DataSet
    Dim idFuncion, nombreFuncion, tagCheckAux, idCheck, nameCheck, perteneceFuncion, checked As String
    Dim textoTotalFuncionesAsignadas As String
    Dim prefijoControl As String = "Cat" & indiceCicloCategoria & "_Pag" & indiceCicloPagina
    Dim contadorFuncionesAsignadas As Integer = 0
    Dim idUsuario As String = Server.UrlDecode(Utilidades.IsNull(Request("idu"), "-1"))

    Try
      'obtiene listado de funciones
      dt = ds.Tables(Perfil.ePagina.ListadoFuncionesPagina)
      If dt Is Nothing Then
        html = htmlSinRegistro
      Else
        'filtra la vista por la categoria
        dv = dt.DefaultView
        dv.RowFilter = "IdCategoria = " & idCategoria & " AND IdPagina = " & idPagina
        'convierte la vista en dataset
        dsFunciones = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
        totalRegistros = dsFunciones.Tables(0).Rows.Count

        If totalRegistros = 0 Then
          html = htmlSinRegistro
        Else
          sb.AppendLine("<tr>")
          sb.AppendLine("  <td>")
          sb.AppendLine(tituloPagina & "<br /><small class=""help-block"">{TOTAL_FUNCIONES_ASIGNADAS}</small>")
          sb.AppendLine("  <input type=""hidden"" id=""" & prefijoControl & "_txtTotalFunciones"" value=""" & totalRegistros & """ />")
          sb.AppendLine("  </td>")
          sb.AppendLine("  <td>")

          'recorre las paginas para ir dibujando su contenido
          For i = 0 To totalRegistros - 1
            dr = dsFunciones.Tables(0).Rows(i)
            tagCheckAux = tagCheck
            idFuncion = dr.Item("IdFuncion")
            nombreFuncion = dr.Item("NombreFuncion")
            perteneceFuncion = dr.Item("PerteneceFuncion")
            'si no tiene funciones asignadas entonces retorna mensaje
            If (totalRegistros = 1 And idFuncion = "0") Then
              sb.AppendLine(htmlSinRegistro)
            Else
              'construye nombres especiales
              idCheck = prefijoControl & "_chkFuncion" & (i + 1)
              nameCheck = "chk_GrupoFunciones"
              checked = IIf(desmarcarTodasFunciones = "1" OrElse perteneceFuncion = "0", "", "checked=""checked""")
              If desmarcarTodasFunciones = "0" And perteneceFuncion = "1" Then contadorFuncionesAsignadas += 1

              'reemplaza las marcas especiales
              tagCheckAux = tagCheckAux.Replace("{$ID_CHECK}", idCheck)
              tagCheckAux = tagCheckAux.Replace("{$NAME_CHECK}", nameCheck)
              tagCheckAux = tagCheckAux.Replace("{$VALUE_CHECK}", idFuncion)
              tagCheckAux = tagCheckAux.Replace("{$LABEL_CHECK}", nombreFuncion)
              tagCheckAux = tagCheckAux.Replace("{$CHECKED}", checked)

              'agrega nuevo string
              sb.AppendLine(tagCheckAux)
            End If
          Next

          sb.AppendLine("    <input type=""hidden"" id=""" & prefijoControl & "_txtTotalFuncionesAsignadas"" value=""" & contadorFuncionesAsignadas & """ />")
          sb.AppendLine("  </td>")
          sb.AppendLine("</tr>")

          'retorna valor
          html = sb.ToString()
          If idUsuario = "-1" Then
            textoTotalFuncionesAsignadas = ""
          Else
            textoTotalFuncionesAsignadas = "(tiene " & contadorFuncionesAsignadas & IIf(contadorFuncionesAsignadas <= 1, " funci&oacute;n asignada", " funciones asignadas") & " de " & totalRegistros & ")"
          End If
          'reemplaza marcas especiales
          html = html.Replace("{TOTAL_FUNCIONES_ASIGNADAS}", textoTotalFuncionesAsignadas)
        End If
      End If
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
    Return html
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim idPerfil As String = Server.UrlDecode(Utilidades.IsNull(Request("idp"), "-1"))
    Dim idUsuario As String = Server.UrlDecode(Utilidades.IsNull(Request("idu"), "-1"))
    Dim eliminarAsignaciones As String = Server.UrlDecode(Utilidades.IsNull(Request("eli"), "0"))
    Dim html As String = ""
    Dim ds As New DataSet

    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      ds = Perfil.ObtenerPaginasAsociadas(idPerfil, idUsuario)
      'dibuja la matriz
      html = DibujarMatrizPermiso(ds, eliminarAsignaciones)
    Catch ex As Exception
      html = Sistema.eCodigoSql.Error
    End Try
    Response.Write(html)
  End Sub

End Class
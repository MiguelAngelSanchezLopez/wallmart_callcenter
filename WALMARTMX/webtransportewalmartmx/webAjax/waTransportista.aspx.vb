﻿Imports CapaNegocio
Imports System.Data

Partial Public Class waTransportista
  Inherits System.Web.UI.Page

#Region "EliminarDocumento"
  ''' <summary>
  ''' elimina una pagina
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EliminarDocumento(ByVal idDocumento As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim retorno As String = ""

    Try
      retorno = Transportista.EliminarDocumento(idDocumento)
      Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}El Documento fue eliminado satisfactoriamente"
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina Documento: ID " & idDocumento)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function


#End Region


#Region "EliminarPagina"
  ''' <summary>
  ''' elimina una pagina
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EliminarPagina(ByVal idConductor As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim retorno As String = ""

    Try
      retorno = Conductor.EliminarDatos(idConductor)
      Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}El Operador fue eliminado satisfactoriamente"
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina Operador: IdConductor " & idConductor)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

#End Region

#Region "EliminarPatente"
  ''' <summary>
  ''' elimina una pagina
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EliminarPatente(ByVal id As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim retorno As String = ""

    Try
      retorno = Transportista.EliminarPatente(id)
      Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}La Placa fue eliminada satisfactoriamente"
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina placa: Id " & id)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim opcion As String = Server.UrlDecode(Request("op"))


    Dim respuesta As String
    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      Select Case opcion
        Case "EliminarDocumento"
          Dim idDocumento As String = Server.UrlDecode(Request("idDocumento"))
          respuesta = EliminarDocumento(idDocumento)
        Case "EliminarPagina"
          Dim idConductor As String = Server.UrlDecode(Request("idConductor"))
          respuesta = EliminarPagina(idConductor)
        Case "EliminarPatente"
          Dim id As String = Server.UrlDecode(Request("id"))
          respuesta = EliminarPatente(id)
        Case Else
          respuesta = Sistema.eCodigoSql.Error
      End Select
      Response.Write(respuesta)
    Catch ex As Exception
      Response.Write(Sistema.eCodigoSql.Error)
    End Try
  End Sub


End Class
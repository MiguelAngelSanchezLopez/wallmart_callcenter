﻿Imports CapaNegocio
Imports System.Data

Partial Public Class waPerfil
  Inherits System.Web.UI.Page

#Region "ObtenerLlavePerfil"
  ''' <summary>
  ''' obtiene la llave relacionada al perfil
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 09/09/2009</remarks>
  Private Function ObtenerLlavePerfil() As String
    Dim html As String
    Dim idPerfil As Integer = Utilidades.IsNull(Request("idp"), "-1")
    Dim oPerfil As Perfil

    Try
      oPerfil = New Perfil(idPerfil)
      If oPerfil.Existe Then
        html = oPerfil.Llave
      Else
        html = Sistema.eCodigoSql.Error
      End If
    Catch ex As Exception
      html = Sistema.eCodigoSql.Error
    End Try
    Return html
  End Function
#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    'limpia el cache del navegador
    Dim oUtilidades As New Utilidades
    oUtilidades.LimpiarCache()

    Try
      Dim opcion As String = Request("op")
      Dim html As String = ""

      Select Case opcion
        Case "ObtenerLlavePerfil"
          html = ObtenerLlavePerfil()
        Case Else
          html = Sistema.eCodigoSql.Error
      End Select
      Response.Write(html)
    Catch ex As Exception
      Response.Write(Sistema.eCodigoSql.Error)
    End Try
  End Sub

End Class
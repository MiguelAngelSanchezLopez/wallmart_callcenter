﻿Imports CapaNegocio
Imports System.Data

Public Class waSistema
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario
        Dim opcion As String = Server.UrlDecode(Request("op"))
        Dim respuesta As String
        'limpia el cache del navegador
        oUtilidades.LimpiarCache()

        Try
            oUsuario = oUtilidades.ObtenerUsuarioSession()
            Select Case opcion
                Case "EliminarFormato"
                    respuesta = EliminarFormato()
                Case "EliminarLocal"
                    respuesta = EliminarLocal()
                Case "EliminarCentroDistribucion"
                    respuesta = EliminarCentroDistribucion()
                Case "ObtenerMensajeUsuario"
                    respuesta = ObtenerMensajeUsuario()
                Case "DondeEstaCamion"
                    respuesta = DondeEstaCamion()
                Case "ObtenerEstadoConexionTeleoperador"
                    respuesta = ObtenerEstadoConexionTeleoperador()
                Case "ObtenerEstadoConexionTeleoperadorCemtra"
                    respuesta = ObtenerEstadoConexionTeleoperadorCemtra()
                Case "DibujarComboCargosPorFormato"
                    respuesta = DibujarComboCargosPorFormato()
                Case "DibujarComboCargosCentroDistribucion"
                    respuesta = DibujarComboCargosCentroDistribucion()
                Case "EliminarPlanificacionTransportista"
                    respuesta = EliminarPlanificacionTransportista()
                Case "ValidarPatenteBloqueada"
                    respuesta = ValidarPatenteBloqueada()
                Case "AutocompletarLocalPlanficacionTransportista"
                    respuesta = AutocompletarLocalPlanficacionTransportista()
                Case "AutocompletarConductorTransportista"
                    respuesta = AutocompletarConductorTransportista()
                Case "ValidarPertenecePatenteTransportista"
                    respuesta = ValidarPertenecePatenteTransportista()
                Case "ValidarPatenteEmiteReportabilidad"
                    respuesta = ValidarPatenteEmiteReportabilidad()
                Case "ActualizarRegistroEnBaseDatos"
                    respuesta = ActualizarRegistroEnBaseDatos()
                Case "ObtenerUsuarioConectadoJSON"
                    respuesta = ObtenerUsuarioConectadoJSON()
                Case "obtenerReportepulsoDiario"
                    respuesta = obtenerReportepulsoDiario()
                Case "obtenerReportepulsoDiarioCEMTRA"
                    respuesta = obtenerReportepulsoDiarioCEMTRA()
                Case Else
                    respuesta = Sistema.eCodigoSql.Error
            End Select
            Response.Write(respuesta)
        Catch ex As Exception
            Response.Write(Sistema.eCodigoSql.Error)
        End Try
    End Sub

#Region "Compartidos"
    ''' <summary>
    ''' obtiene los combos con los cargos
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerComboCargos(ByVal ds As DataSet) As String
        Dim listado As String = ""
        Dim template, templateAux, nombrePerfil, llavePerfil, comboCargo As String
        Dim sb As New StringBuilder
        Dim T01_Perfiles As Integer = 0
        Dim T02_ListadoUsuarios As Integer = 1
        Dim dv As DataView
        Dim dsFiltrado As New DataSet

        Try
            template = "<div class=""col-xs-12 sist-padding-bottom-10"">" &
                       "  <label class=""control-label"">{NOMBRE_PERFIL}</label>" &
                       "  <div>{COMBO}</div>" &
                       "</div>"

            For Each dr As DataRow In ds.Tables(T01_Perfiles).Rows
                templateAux = template
                nombrePerfil = dr.Item("NombrePerfil")
                llavePerfil = dr.Item("LlavePerfil")

                'filtra los usuarios segun el perfil a dibujar
                dv = ds.Tables(T02_ListadoUsuarios).DefaultView
                dv.RowFilter = "LlavePerfil = '" & llavePerfil & "'"
                dsFiltrado = Utilidades.CrearDataSetDesdeDataViewConRowFilter(dv)
                dv.RowFilter = ""
                comboCargo = DibujarComboCargos(llavePerfil, dsFiltrado)

                'reemplaza marcas
                templateAux = templateAux.Replace("{NOMBRE_PERFIL}", Server.HtmlEncode(nombrePerfil))
                templateAux = templateAux.Replace("{COMBO}", comboCargo)
                sb.Append(templateAux)
            Next

            listado = sb.ToString()
            Return listado
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    ''' <summary>
    ''' dibuja combo con el cargo seleccionado
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DibujarComboCargos(ByVal prefijoControl As String, ByVal ds As DataSet) As String
        Dim html As String = ""
        Dim sb As New StringBuilder
        Dim template, templateAux, texto, valor, prefijoControlCombo, seleccionado, selected As String
        Dim dr As DataRow

        template = "<option value=""{VALOR}"" {SELECTED}>{TEXTO}</option>"
        prefijoControlCombo = prefijoControl & IIf(String.IsNullOrEmpty(prefijoControl), "", "_")

        'dibuja combo
        sb.Append("<div class=""sist-padding-cero"">")
        sb.Append("  <select id=""" & prefijoControlCombo & "ddlCargo"" class=""form-control input-sm"" data-categoria=""combo-cargo"" placeholder=""Seleccione"" multiple>")

        Try
            For Each dr In ds.Tables(0).Rows
                templateAux = template
                texto = dr.Item("NombreUsuario")
                valor = dr.Item("IdUsuario")
                seleccionado = dr.Item("Seleccionado")
                selected = IIf(seleccionado = "1", "selected", "")

                templateAux = templateAux.Replace("{VALOR}", valor)
                templateAux = templateAux.Replace("{TEXTO}", Server.HtmlEncode(texto))
                templateAux = templateAux.Replace("{SELECTED}", selected)

                sb.Append(templateAux)
            Next
        Catch ex As Exception
            'no hace nada
        End Try

        sb.Append("  </select>")
        sb.Append("</div>")

        html = sb.ToString()
        Return html
    End Function

#End Region

#Region "EliminarFormato"
    ''' <summary>
    ''' elimina un formato
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function EliminarFormato() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim idFormato As String = Utilidades.IsNull(Request("idFormato"), "-1")

        Try
            retorno = Formato.EliminarDatos(idFormato)
            Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}El FORMATO fue eliminado satisfactoriamente"
            Sistema.GrabarLogSesion(oUsuario.Id, "Elimina formato: IdFormato " & idFormato)
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try
        Return retorno
    End Function

#End Region

#Region "EliminarLocal"
    ''' <summary>
    ''' elimina un local
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function EliminarLocal() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim idLocal As String = Utilidades.IsNull(Request("idLocal"), "-1")

        Try
            retorno = Local.EliminarDatos(idLocal)
            Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}La Tienda fue eliminada satisfactoriamente"
            Sistema.GrabarLogSesion(oUsuario.Id, "Elimina tienda: IdLocal " & idLocal)
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try
        Return retorno
    End Function

#End Region

#Region "EliminarCentroDistribucion"
    ''' <summary>
    ''' elimina un centro distribucion
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function EliminarCentroDistribucion() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim idCentroDistribucion As String = Utilidades.IsNull(Request("idCentroDistribucion"), "-1")

        Try
            retorno = CentroDistribucion.EliminarDatos(idCentroDistribucion)
            Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}El CENTRO DE DISTRIBUCION fue eliminado satisfactoriamente"
            Sistema.GrabarLogSesion(oUsuario.Id, "Elimina centro de distribución: IdCentroDistribucion " & idCentroDistribucion)
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try
        Return retorno
    End Function

#End Region

#Region "ObtenerMensajeUsuario"
    ''' <summary>
    ''' obtiene mensaje usuario desde la tabla Configuracion
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerMensajeUsuario() As String
        Dim retorno As String = ""

        Try
            retorno = Configuracion.Leer("webMensajeUsuario")
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try
        Return retorno
    End Function

#End Region

#Region "DondeEstaCamion"
    ''' <summary>
    ''' obtiene posicion actual del camion segun patente consultada
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DondeEstaCamion() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim patente As String = Server.UrlDecode(Utilidades.IsNull(Request("patente"), ""))
        Dim ws As New wsQanalytics.Service
        Dim respuesta As wsQanalytics.Estructura

        Try
            respuesta = ws.Metodo(patente)
            retorno = MyJSON.ConvertObjectToJSON(respuesta)
            Sistema.GrabarLogSesion(oUsuario.Id, "Consulta posición del camión: Placa " & patente)
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try
        Return retorno
    End Function

#End Region

#Region "ObtenerEstadoConexionTeleoperador"
    ''' <summary>
    ''' obtiene el estado de conexion de los teleoperadores
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerEstadoConexionTeleoperador() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As New Usuario
        Dim retorno As String = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim jsonDatosGraficoTorta, jsonDatosGraficoTorta2 As String

        Try
            ds = Usuario.ObtenerEstadoConexionTeleoperador()

            'obtiene datos para dibujar grafico de torta
            jsonDatosGraficoTorta = Grafico.ObtenerJSONTorta(ds.Tables(1), "ALERTAS PENDIENTES POR GESTIONAR", String.Empty, "NombreAlerta", "Porcentaje", "false", "600", "350")
            jsonDatosGraficoTorta2 = Grafico.ObtenerJSONTorta(ds.Tables(2), "ALERTAS ATENDIDAS", String.Empty, "Usuario", "Porcentaje", "false", "600", "350")

            sb.Append("{")
            sb.Append(" ""listadoUsuarios"":" & MyJSON.ConvertDataTableToJSON(ds.Tables(0)))
            sb.Append(",""datosGraficoTorta"": [" & jsonDatosGraficoTorta & "]")
            sb.Append(",""datosGraficoTorta2"": [" & jsonDatosGraficoTorta2 & "]")
            sb.Append(",""listadoTotalAlertas"":" & MyJSON.ConvertDataTableToJSON(ds.Tables(3)))
            sb.Append(",""listadoTotalAlertasAtendidas"":" & MyJSON.ConvertDataTableToJSON(ds.Tables(4)))
            sb.Append("}")

            retorno = sb.ToString()
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        Return retorno
    End Function

    ''' <summary>
    ''' obtiene el estado de conexion de los teleoperadores de Cemtra
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerEstadoConexionTeleoperadorCemtra() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As New Usuario
        Dim retorno As String = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim jsonDatosGraficoTorta As String
        Dim codCentroDistribucion As String = Utilidades.IsNull(Request("codCentroDistribucion"), "-1")

        Try
            Dim oUtilidades1 As New Utilidades
            Dim oUsuario1 As Usuario
            oUsuario1 = oUtilidades.ObtenerUsuarioSession()

            ds = Usuario.ObtenerEstadoConexionTeleoperadorCemtra(codCentroDistribucion, oUsuario1.Id)

            'ds = Usuario.ObtenerEstadoConexionTeleoperadorCemtra(codCentroDistribucion)

            'obtiene datos para dibujar grafico de torta
            jsonDatosGraficoTorta = Grafico.ObtenerJSONTorta(ds.Tables(1), "Porcentaje estado conexión", "xxx", "Estado", "Porcentaje", "false", "650", "250")

            sb.Append("{")
            sb.Append(" ""listadoUsuarios"":" & MyJSON.ConvertDataTableToJSON(ds.Tables(0)))
            sb.Append(",""datosGraficoTorta"": [" & jsonDatosGraficoTorta & "]")
            sb.Append(",""contactabilidad"":" & MyJSON.ConvertDataTableToJSON(ds.Tables(2)))
            'nuevas lineas para corregir problema listado alertas 130818
            sb.Append(",""listadoTotalAlertas"":" & MyJSON.ConvertDataTableToJSON(ds.Tables(3)))
            sb.Append(",""listadoTotalAlertasAtendidas"":" & MyJSON.ConvertDataTableToJSON(ds.Tables(4)))
            'fin nuevas lineas para corregir problema listado alertas 130818
            sb.Append("}")

            retorno = sb.ToString()
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        Return retorno
    End Function

#End Region

    Private Function obtenerReportepulsoDiario() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As New Usuario
        Dim retorno As String = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim jsonDatosGraficoTorta As String
        Dim idusuario As Integer

        Try
            oUsuario = oUtilidades.ObtenerUsuarioSession()
            idusuario = Utilidades.IsNull(oUsuario.Id, 0)

            ds = Usuario.ReportePulsoDiario(Request("FecI"), Request("FecF"), Request("To"), Request("Turno"), idusuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                jsonDatosGraficoTorta = Grafico.ObtenerJSONTorta(ds.Tables(0), "Porcentajes de Eficiencia", "% de Eficiencia", "NombreCompleto", "NS_Percent", "false", "650", "400")

                sb.Append("{")
                sb.Append(" ""datosGraficoTorta"": [" & jsonDatosGraficoTorta & "]")
                sb.Append("}")
            Else
                sb.Append("")
            End If

            retorno = sb.ToString()
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        Return retorno
    End Function

    Private Function obtenerReportepulsoDiarioCEMTRA() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As New Usuario
        Dim retorno As String = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim jsonDatosGraficoTorta As String
        Dim idusuario As Integer

        Try
            oUsuario = oUtilidades.ObtenerUsuarioSession()
            idusuario = Utilidades.IsNull(oUsuario.Id, 0)

            ds = Usuario.ReportePulsoDiarioToCemtra(Request("FecI"), Request("FecF"), Request("To"), Request("Turno"), idusuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                jsonDatosGraficoTorta = Grafico.ObtenerJSONTorta(ds.Tables(0), "Porcentajes de Eficiencia", "% de Eficiencia", "NombreCompleto", "NS_Percent", "false", "650", "400")

                sb.Append("{")
                sb.Append(" ""datosGraficoTorta"": [" & jsonDatosGraficoTorta & "]")
                sb.Append("}")
            Else
                sb.Append("")
            End If

            retorno = sb.ToString()
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        Return retorno
    End Function

#Region "DibujarComboCargosPorFormato"
    ''' <summary>
    ''' obtiene los cargos asociados al formato y local, y dibuja los combos
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DibujarComboCargosPorFormato() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As New Usuario
        Dim retorno As String = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim idFormato As String = Utilidades.IsNull(Request("idFormato"), "-1")
        Dim idLocal As String = Utilidades.IsNull(Request("idLocal"), "-1")
        Dim categoria As String = Utilidades.IsNull(Request("categoria"), "-1")
        Dim htmlSinRegistro As String = "<em class=""help-block"">No hay cargos asociados para mostrar</em>"
        Dim T01_Perfiles As Integer = 0
        Dim T02_ListadoUsuarios As Integer = 1
        Dim totalRegistros As Integer
        Dim listadoCargos As String

        Try
            If (idFormato = "-1") Then
                sb.Append("<em class=""help-block"">Seleccione un formato para mostrar los cargos asociados</em>")
            Else
                ds = Local.ObtenerCargosPorFormato(idFormato, idLocal, categoria)
                If (ds Is Nothing) Then
                    sb.Append(htmlSinRegistro)
                Else
                    totalRegistros = ds.Tables(T01_Perfiles).Rows.Count
                    If (totalRegistros = 0) Then
                        sb.Append(htmlSinRegistro)
                    Else
                        listadoCargos = ObtenerComboCargos(ds)

                        sb.Append("<div class=""form-group"">")
                        sb.Append("  <div class=""row"">")
                        sb.Append(listadoCargos)
                        sb.Append("  </div>")
                        sb.Append("</div>")
                    End If
                End If
            End If

            retorno = sb.ToString()
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        Return retorno
    End Function

#End Region

#Region "DibujarComboCargosCentroDistribucion"
    ''' <summary>
    ''' obtiene los cargos asociados al centro distribucion, y dibuja los combos
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function DibujarComboCargosCentroDistribucion() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As New Usuario
        Dim retorno As String = ""
        Dim ds As New DataSet
        Dim sb As New StringBuilder
        Dim idCentroDistribucion As String = Utilidades.IsNull(Request("idCentroDistribucion"), "-1")
        Dim categoria As String = Utilidades.IsNull(Request("categoria"), "-1")
        Dim htmlSinRegistro As String = "<em class=""help-block"">No hay cargos asociados para mostrar</em>"
        Dim T01_Perfiles As Integer = 0
        Dim T02_ListadoUsuarios As Integer = 1
        Dim totalRegistros As Integer
        Dim listadoCargos As String

        Try
            ds = CentroDistribucion.ObtenerCargos(idCentroDistribucion, categoria)
            If (ds Is Nothing) Then
                sb.Append(htmlSinRegistro)
            Else
                totalRegistros = ds.Tables(T01_Perfiles).Rows.Count
                If (totalRegistros = 0) Then
                    sb.Append(htmlSinRegistro)
                Else
                    listadoCargos = ObtenerComboCargos(ds)

                    sb.Append("<div class=""form-group"">")
                    sb.Append("  <div class=""row"">")
                    sb.Append(listadoCargos)
                    sb.Append("  </div>")
                    sb.Append("</div>")
                End If
            End If

            retorno = sb.ToString()
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        Return retorno
    End Function


#End Region

#Region "EliminarPlanificacionTransportista"
    ''' <summary>
    ''' elimina la planificacion de un transportista
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function EliminarPlanificacionTransportista() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim idPlanificacion As String = Utilidades.IsNull(Request("idPlanificacion"), "-1")
        Dim nroTransporte As String = Utilidades.IsNull(Request("nroTransporte"), "-1")
        Dim llaveRecargarPagina As String = Server.UrlDecode(Utilidades.IsNull(Request("llaveRecargarPagina"), "-1"))

        Try
            retorno = PlanificacionTransportista.EliminarDatos(idPlanificacion, nroTransporte)
            Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}La planificaci&oacute;n fue eliminada satisfactoriamente"
            Session(llaveRecargarPagina) = "1"
            Sistema.GrabarLogSesion(oUsuario.Id, "Elimina planificación Línea de Transporte: IdPlanificacion " & idPlanificacion & " / IdMaster " & nroTransporte)
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try
        Return retorno
    End Function

#End Region

#Region "ValidarPatenteBloqueada"
    ''' <summary>
    ''' elimina la planificacion de un transportista
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidarPatenteBloqueada() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim patente As String = Utilidades.IsNull(Request("patente"), "-1")
        Dim ds As DataSet

        Try
            ds = Transportista.ObtenerPatenteBloqueada(patente)

            If (ds Is Nothing) Then
                Return "1"
            ElseIf (ds.Tables(0).Rows.Count > 0) Then
                Return "-1"
            Else
                Return "1"
            End If
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try
        Return retorno
    End Function

#End Region

#Region "AutocompletarLocalPlanficacionTransportista"
    ''' <summary>
    ''' obtiene listado de categorias para poder autocompletar
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AutocompletarLocalPlanficacionTransportista() As String
        Dim retorno As String = ""
        Dim textoBusquedaOriginal As String = Request("term")
        Dim textoBusqueda As String = Request("term").ToLower
        Dim totalRegistros As Integer
        Dim i As Integer = 0
        Dim ds As New DataSet
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario
        Dim sb As New StringBuilder
        Dim templateJSON As String = ""
        Dim auxTemplateJSON As String = ""
        Dim idLocal, nombreLocal, nombreConEstilo As String

        Try
            templateJSON &= "{"
            templateJSON &= " ""label"":""{label}"""
            templateJSON &= ",""value"":""{value}"""
            templateJSON &= ",""nombreLocal"":""{nombreLocal}"""
            templateJSON &= "}"

            ds = PlanificacionTransportista.AutocompletarLocales(textoBusquedaOriginal.Replace("%", ""))
            If ds Is Nothing Then
                totalRegistros = 0
            Else
                totalRegistros = ds.Tables(0).Rows.Count
            End If

            If totalRegistros > 0 Then
                For Each dr In ds.Tables(0).Rows
                    auxTemplateJSON = templateJSON
                    idLocal = dr.Item("IdLocal")
                    nombreLocal = dr.Item("NombreLocal")

                    'formatea valores
                    nombreConEstilo = nombreLocal

                    'reemplaza las marcas por los valores reales
                    auxTemplateJSON = auxTemplateJSON.Replace("{label}", nombreConEstilo)
                    auxTemplateJSON = auxTemplateJSON.Replace("{value}", idLocal)
                    auxTemplateJSON = auxTemplateJSON.Replace("{nombreLocal}", Utilidades.EscaparComillasJS(nombreLocal))

                    sb.Append(auxTemplateJSON)
                    If (i < totalRegistros - 1) Then sb.Append(",")
                    i += 1
                Next
            Else
                sb.Append("")
            End If
        Catch ex As Exception
            oUsuario = oUtilidades.ObtenerUsuarioSession()
            If (oUsuario Is Nothing) Then
                sb.Append("{""label"":""<span class='cssMensajeError'>Su sesi&oacute;n ha finalizado.<br/>Cierre la sesi&oacute;n actual y vuelva a ingresar para poder operar de manera correcta en el sistema.</span>"",""value"":""""}")
            Else
                sb.Append("{""label"":""<span class='cssMensajeError'>Ha ocurrido un error interno y no se pudo obtener el valor.<br/>Int&eacute;ntelo m&aacute;s tarde por favor.</span>"",""value"":""""}")
            End If
        End Try

        'asigna valor
        retorno = "[" & sb.ToString() & "]"
        Return retorno
    End Function
#End Region

#Region "AutocompletarConductorTransportista"
    ''' <summary>
    ''' obtiene listado de conductores asociado al transportista
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function AutocompletarConductorTransportista() As String
        Dim retorno As String = ""
        Dim textoBusquedaOriginal As String = Request("term")
        Dim textoBusqueda As String = Request("term").ToLower
        Dim idUsuarioTransportista As String = Utilidades.IsNull(Request("idUsuarioTransportista"), "-1")
        Dim totalRegistros As Integer
        Dim i As Integer = 0
        Dim ds As New DataSet
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario
        Dim sb As New StringBuilder
        Dim templateJSON As String = ""
        Dim auxTemplateJSON As String = ""
        Dim nombreConductor, rutConductor, telefono, nombreConEstilo As String

        Try
            templateJSON &= "{"
            templateJSON &= " ""label"":""{label}"""
            templateJSON &= ",""value"":""{value}"""
            templateJSON &= ",""nombreConductor"":""{nombreConductor}"""
            templateJSON &= ",""rutConductor"":""{rutConductor}"""
            templateJSON &= ",""telefono"":""{telefono}"""
            templateJSON &= "}"

            ds = PlanificacionTransportista.AutocompletarConductoresPorTransportista(textoBusquedaOriginal.Replace("%", ""), idUsuarioTransportista)
            If ds Is Nothing Then
                totalRegistros = 0
            Else
                totalRegistros = ds.Tables(0).Rows.Count
            End If

            If totalRegistros > 0 Then
                For Each dr In ds.Tables(0).Rows
                    auxTemplateJSON = templateJSON
                    nombreConductor = dr.Item("NombreConductor")
                    rutConductor = dr.Item("RutConductor")
                    telefono = dr.Item("Telefono")

                    'formatea valores
                    nombreConductor = nombreConductor.Trim()
                    rutConductor = rutConductor.Trim()
                    telefono = telefono.Trim()
                    nombreConEstilo = nombreConductor

                    'reemplaza las marcas por los valores reales
                    auxTemplateJSON = auxTemplateJSON.Replace("{label}", nombreConEstilo)
                    auxTemplateJSON = auxTemplateJSON.Replace("{value}", nombreConductor)
                    auxTemplateJSON = auxTemplateJSON.Replace("{nombreConductor}", Utilidades.EscaparComillasJS(nombreConductor))
                    auxTemplateJSON = auxTemplateJSON.Replace("{rutConductor}", Utilidades.EscaparComillasJS(rutConductor))
                    auxTemplateJSON = auxTemplateJSON.Replace("{telefono}", Utilidades.EscaparComillasJS(telefono))

                    sb.Append(auxTemplateJSON)
                    If (i < totalRegistros - 1) Then sb.Append(",")
                    i += 1
                Next
            Else
                sb.Append("")
            End If
        Catch ex As Exception
            oUsuario = oUtilidades.ObtenerUsuarioSession()
            If (oUsuario Is Nothing) Then
                sb.Append("{""label"":""<span class='cssMensajeError'>Su sesi&oacute;n ha finalizado.<br/>Cierre la sesi&oacute;n actual y vuelva a ingresar para poder operar de manera correcta en el sistema.</span>"",""value"":""""}")
            Else
                sb.Append("{""label"":""<span class='cssMensajeError'>Ha ocurrido un error interno y no se pudo obtener el valor.<br/>Int&eacute;ntelo m&aacute;s tarde por favor.</span>"",""value"":""""}")
            End If
        End Try

        'asigna valor
        retorno = "[" & sb.ToString() & "]"
        Return retorno
    End Function
#End Region

#Region "ValidarPertenecePatenteTransportista"
    ''' <summary>
    ''' valida que la patente pertenezca al transportista
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidarPertenecePatenteTransportista() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim idTransportista As String = Utilidades.IsNull(Request("idTransportista"), "-1")
        Dim patente As String = Utilidades.IsNull(Request("patente"), "-1")
        Dim pertenecePatente As Integer

        Try
            pertenecePatente = Transportista.PertenecePatente(idTransportista, patente)

            If (pertenecePatente = Sistema.eCodigoSql.Error) Then
                retorno = "1"
            Else
                retorno = pertenecePatente
            End If
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        '[Retorno: "1" -> Valido | "-1" -> No Valido]
        Return retorno
    End Function

#End Region

#Region "ValidarPatenteEmiteReportabilidad"
    ''' <summary>
    ''' valida que la patente emita reportabilidad
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidarPatenteEmiteReportabilidad() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim idTransportista As String = Utilidades.IsNull(Request("idTransportista"), "-1")
        Dim patente As String = Utilidades.IsNull(Request("patente"), "-1")
        Dim pertenecePatente As Integer

        Try
            pertenecePatente = Transportista.EmiteReportabilidad(idTransportista, patente)

            If (pertenecePatente = Sistema.eCodigoSql.Error) Then
                retorno = "1"
            Else
                retorno = pertenecePatente
            End If
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        '[Retorno: "1" -> Valido | "-1" -> No Valido]
        Return retorno
    End Function

#End Region

#Region "ActualizarRegistroEnBaseDatos"
    ''' <summary>
    ''' actualiza registro en base de datos
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ActualizarRegistroEnBaseDatos() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
        Dim retorno As String = ""
        Dim origenDatoTabla As String = Utilidades.IsNull(Request("origenDatoTabla"), "")
        Dim origenDatoIdRegistro As String = Utilidades.IsNull(Request("origenDatoIdRegistro"), "")
        Dim telefono As String = Utilidades.IsNull(Request("telefono"), "")
        Dim patenteTracto As String = Utilidades.IsNull(Request("patenteTracto"), "")
        Dim patenteTrailer As String = Utilidades.IsNull(Request("patenteTrailer"), "")

        Try
            retorno = GestionOperacion.ActualizarRegistroEnBaseDatos(origenDatoTabla, origenDatoIdRegistro, telefono, patenteTracto, patenteTrailer)
            Sistema.GrabarLogSesion(oUsuario.Id, "Actualizar registro en base datos: OrigenDatoTabla " & origenDatoTabla & " | OrigenDatoIdRegistro " & origenDatoIdRegistro & " | Telefono " & telefono)
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try
        Return retorno
    End Function

#End Region

#Region "ObtenerUsuarioConectadoJSON"
    ''' <summary>
    ''' obtiene informacion del usuario conectado en formato JSON
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerUsuarioConectadoJSON() As String
        Dim oUtilidades As New Utilidades
        Dim oUsuario As New Usuario
        Dim retorno As String = ""

        Try
            oUsuario = oUtilidades.ObtenerUsuarioSession()
            retorno = Herramientas.ObtenerJSONUsuario(oUsuario, Utilidades.ObtenerUrlBase())
        Catch ex As Exception
            retorno = Sistema.eCodigoSql.Error
        End Try

        Return retorno
    End Function

#End Region

End Class
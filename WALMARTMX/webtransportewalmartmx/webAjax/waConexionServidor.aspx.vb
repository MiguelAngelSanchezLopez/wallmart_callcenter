﻿Imports System.Data
Imports CapaNegocio
Imports System.Net
Imports System.Net.NetworkInformation

Partial Public Class waConexionServidor
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim totalRegistros As Integer
    Dim ds As New DataSet
    Dim oUtilidades As New Utilidades

    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      Dim conexionDisponible As Boolean = NetworkInterface.GetIsNetworkAvailable()

      If conexionDisponible Then
        ds = Sistema.ValidarConexionServidor()
        totalRegistros = ds.Tables(0).Rows.Count
        Response.Write(Sistema.eCodigoSql.Exito)
      Else
        Response.Write(Sistema.eCodigoSql.SinConexion)
      End If
    Catch ex As Exception
      Response.Write(Sistema.eCodigoSql.Error)
    End Try
  End Sub


End Class
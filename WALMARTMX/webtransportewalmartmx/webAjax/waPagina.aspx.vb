﻿Imports CapaNegocio
Imports System.Data

Partial Public Class waPagina
  Inherits System.Web.UI.Page

#Region "AutocompletarCategoria"
  ''' <summary>
  ''' obtiene listado de categorias para poder autocompletar
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function AutocompletarCategoria() As String
    Dim retorno As String = ""
    Dim textoBusquedaOriginal As String = Request("term")
    Dim textoBusqueda As String = Request("term").ToLower
    Dim totalRegistros, i As Integer
    Dim valor, valorConEstilo As String
    Dim ds As New DataSet
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim sb As New StringBuilder
    Dim templateJSON As String = "{""label"":""{LABEL}"",""value"":""{VALUE}""}"
    Dim auxTemplateJSON As String = ""

    Try
      ds = Pagina.AutocompletarCategoria(textoBusquedaOriginal.Replace("%", ""))
      If ds Is Nothing Then
        totalRegistros = 0
      Else
        totalRegistros = ds.Tables(0).Rows.Count
      End If

      If totalRegistros > 0 Then
        For i = 0 To totalRegistros - 1
          auxTemplateJSON = templateJSON
          'asigna valores del dataset
          valor = Trim(ds.Tables(0).Rows(i).Item("Nombre"))
          valorConEstilo = valor.ToLower()

          'reemplaza las marcas por los valores reales
          auxTemplateJSON = auxTemplateJSON.Replace("{LABEL}", valorConEstilo)
          auxTemplateJSON = auxTemplateJSON.Replace("{VALUE}", Utilidades.EscaparComillasJS(valor))
          sb.Append(auxTemplateJSON)
          If (i < totalRegistros - 1) Then sb.Append(",")
        Next
      Else
        sb.Append("")
      End If
    Catch ex As Exception
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      If oUsuario Is Nothing Then
        sb.Append("{""label"":""Su sesión ha finalizado. Cierre la sesión actual y vuelva a ingresar para poder operar de manera correcta en el sistema"",""value"":""""}")
      Else
        sb.Append("{""label"":""Ha ocurrido un error interno y no se pudo obtener el valor. Inténtelo más tarde por favor"",""value"":""""}")
      End If
    End Try
    'asigna valor
    retorno = "[" & sb.ToString & "]"
    Return retorno
  End Function
#End Region

#Region "VerificarEliminacionFuncion"
  Private Function VerificarEliminacionFuncion() As String
    Dim retorno As String = ""
    Dim idFuncion As String = Utilidades.IsNull(Request("idFuncion"), "-1")

    Try
      retorno = Pagina.VerificarEliminacionFuncion(idFuncion)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function
#End Region

#Region "EliminarPagina"
  ''' <summary>
  ''' elimina una pagina
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EliminarPagina(ByVal idPagina As String) As String
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario = oUtilidades.ObtenerUsuarioSession()
    Dim retorno As String = ""

    Try
      retorno = Pagina.EliminarDatos(idPagina)
      Session(Utilidades.KEY_SESION_MENSAJE) = "{ELIMINADO}La PAGINA fue eliminada satisfactoriamente"
      Sistema.GrabarLogSesion(oUsuario.Id, "Elimina página: IdPagina " & idPagina)
    Catch ex As Exception
      retorno = Sistema.eCodigoSql.Error
    End Try
    Return retorno
  End Function

#End Region

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim oUsuario As Usuario
    Dim opcion As String = Server.UrlDecode(Request("op"))
    Dim idPagina As String = Server.UrlDecode(Request("idPagina"))
    Dim respuesta As String
    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      oUsuario = oUtilidades.ObtenerUsuarioSession()
      Select Case opcion
        Case "AutocompletarCategoria"
          respuesta = AutocompletarCategoria()
        Case "VerificarEliminacionFuncion"
          respuesta = VerificarEliminacionFuncion()
        Case "EliminarPagina"
          respuesta = EliminarPagina(idPagina)
        Case Else
          respuesta = Sistema.eCodigoSql.Error
      End Select
      Response.Write(respuesta)
    Catch ex As Exception
      Response.Write(Sistema.eCodigoSql.Error)
    End Try
  End Sub


End Class
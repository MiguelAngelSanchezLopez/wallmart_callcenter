﻿Imports CapaNegocio

Partial Public Class waComprobarDatoDuplicado
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Dim oUtilidades As New Utilidades
    Dim id As String = Server.UrlDecode(Utilidades.IsNull(Request("id"), "-1"))
    Dim valor As String = Server.UrlDecode(Utilidades.IsNull(Request("valor"), "-1"))
    Dim opcion As String = Server.UrlDecode(Utilidades.IsNull(Request("op"), "-1"))
    Dim existe As String = 0
    Dim rut As String = ""
    Dim dv As String = ""

    'limpia el cache del navegador
    oUtilidades.LimpiarCache()

    Try
      rut = Utilidades.IsNull(valor, "")

      Select Case opcion
        Case "Usuario.Rut"
          If (String.IsNullOrEmpty(rut)) Then
            rut = "-1"
          Else
            Utilidades.SepararRut(rut, dv)
            rut = rut & dv
          End If
          existe = Usuario.VerificarDuplicidad(id, rut, "Rut")
        Case "Usuario.Username"
          existe = Usuario.VerificarDuplicidad(id, valor, "Username")
        Case "Pagina.NombreArchivo"
          existe = Pagina.VerificarDuplicidad(id, valor, "NombreArchivo")
        Case "Alerta.Nombre"
          Dim idFormato As String = Server.UrlDecode(Utilidades.IsNull(Request("idFormato"), "-1"))
          existe = Alerta.VerificarDuplicidad(id, valor, idFormato, "Nombre")
        Case "Alerta.NombreDefinicionAlerta"
          existe = Alerta.VerificarDuplicidad(id, valor, "-1", "DefinicionAlerta")
        Case "Formato.Nombre"
          existe = Formato.VerificarDuplicidad(id, valor, "Nombre")
        Case "Formato.Llave"
          existe = Formato.VerificarDuplicidad(id, valor, "Llave")
        Case "Local.Codigo"
          existe = Local.VerificarDuplicidad(id, valor, "Codigo")
        Case "CentroDistribucion.Nombre"
          existe = CentroDistribucion.VerificarDuplicidad(id, valor, "Nombre")
        Case "CentroDistribucion.Codigo"
          existe = CentroDistribucion.VerificarDuplicidad(id, valor, "Codigo")
        Case "Perfil.Nombre"
          existe = Perfil.VerificarDuplicidad(id, valor, "Nombre")
        Case "Perfil.Llave"
          existe = Perfil.VerificarDuplicidad(id, valor, "Llave")
        Case "PlanificacionTransportista.NroTransporte"
          Dim nroTransporteValorActual As String = Server.UrlDecode(Utilidades.IsNull(Request("nroTransporteValorActual"), "-1"))
          existe = PlanificacionTransportista.VerificarDuplicidad(id, valor, nroTransporteValorActual, "NroTransporte")
      End Select
    Catch ex As Exception
      existe = 0
    End Try
    Response.Write(existe)
  End Sub

End Class
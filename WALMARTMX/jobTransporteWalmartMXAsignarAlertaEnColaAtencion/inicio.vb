﻿Imports CapaNegocio
Imports System.Data
Imports System.Text
Imports System.Configuration

Module inicio

#Region "Configuracion Job"
  Const NOMBRE_JOB As String = "jobTransporteWalmartMXAsignarAlertaEnColaAtencion"
  Const KEY As String = "AsignarAlertaEnColaAtencion"
#End Region

#Region "Enum"
  Enum eTablaConfiguracion As Integer
    EmailPara = 0
    EmailCC = 1
    DiasEjecucion = 2
    AsuntoEmail = 3
    CuerpoEmail = 4
  End Enum

#End Region

#Region "Privado"
  ''' <summary>
  ''' determina si se puede ejecutar la consola en el dia actual
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function EjecutarDiaMes() As Boolean
    Dim sePuedeEjecutar As Boolean = False
    Dim dsConfiguracionXML As New DataSet
    Dim diasEjecucion, incluirFinMes As String
    Dim diaMesActual As String = Herramientas.MyNow().Day
    Dim ultimoDiaMes As Date

    Try
      dsConfiguracionXML = Herramientas.LeerXML("configuracion.xml")
      diasEjecucion = dsConfiguracionXML.Tables(eTablaConfiguracion.DiasEjecucion).Rows(0).Item("diasEjecucion")
      incluirFinMes = dsConfiguracionXML.Tables(eTablaConfiguracion.DiasEjecucion).Rows(0).Item("incluirFinMes")

      If (InStr(diasEjecucion, "{-1}") > 0) Or (InStr(diasEjecucion, "{" & diaMesActual & "}") > 0) Then
        sePuedeEjecutar = True
      Else
        If incluirFinMes.Trim.ToUpper = "TRUE" Then
          ultimoDiaMes = Herramientas.ObtenerUltimoDiaMes(Herramientas.MyNow())
          If Day(ultimoDiaMes) = diaMesActual Then
            sePuedeEjecutar = True
          End If
        End If
      End If
    Catch ex As Exception
      sePuedeEjecutar = False
    End Try
    Return sePuedeEjecutar
  End Function

  ''' <summary>
  ''' graba los registros en cola de atencion
  ''' </summary>
  ''' <param name="ds"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GrabarAlertasEnColaAtencion(ByVal ds As DataSet) As String
    Dim msgError As String = ""
    Dim oAlertaEnColaAtencion As New Alerta.AlertaEnColaAtencion
    Dim idAlerta, idAlertaHija, localDestino, idFormato, ordenPrioridad As Integer
    Dim nroTransporte, idEmbarque As Int64
    Dim nombreAlerta, prioridad, fechaHoraCreacionDMA As String
    Dim fechaHoraCreacion As DateTime
    Dim status As Integer
    Dim sb As New StringBuilder

    Try
      For Each dr As DataRow In ds.Tables(0).Rows
        idAlerta = dr.Item("IdAlerta")
        idAlertaHija = dr.Item("IdAlertaHija")
        nroTransporte = dr.Item("NroTransporte")
        idEmbarque = dr.Item("IdEmbarque")
        nombreAlerta = dr.Item("NombreAlerta")
        localDestino = dr.Item("LocalDestino")
        idFormato = dr.Item("IdFormato")
        prioridad = dr.Item("Prioridad")
        ordenPrioridad = dr.Item("OrdenPrioridad")
        fechaHoraCreacion = dr.Item("FechaHoraCreacion")
        fechaHoraCreacionDMA = dr.Item("FechaHoraCreacionDMA")

        oAlertaEnColaAtencion.IdAlerta = idAlerta
        oAlertaEnColaAtencion.IdAlertaHija = idAlertaHija
        oAlertaEnColaAtencion.NroTransporte = nroTransporte
        oAlertaEnColaAtencion.IdEmbarque = idEmbarque
        oAlertaEnColaAtencion.NombreAlerta = nombreAlerta
        oAlertaEnColaAtencion.LocalDestino = localDestino
        oAlertaEnColaAtencion.IdFormato = idFormato
        oAlertaEnColaAtencion.Prioridad = prioridad
        oAlertaEnColaAtencion.OrdenPrioridad = ordenPrioridad
        oAlertaEnColaAtencion.FechaHoraCreacion = fechaHoraCreacion
        oAlertaEnColaAtencion.FechaHoraCreacionDMA = fechaHoraCreacionDMA

        status = Alerta.GrabarAlertaEnColaAtencion(oAlertaEnColaAtencion)
        If (status = Sistema.eCodigoSql.Error) Then
          sb.AppendLine("IdAlerta: " & idAlerta & " / IdAlertaHija: " & idAlertaHija & " no se pudo grabar<br />")
        End If
      Next

      If (sb.Length > 0) Then
        msgError = sb.ToString()
      End If
    Catch ex As Exception
      msgError = ex.Message
    End Try

    Return msgError
  End Function

#End Region

  Sub Main()
    Dim msgProceso As String = ""
    'construye fecha inicio
    Dim ds As New DataSet
    Dim dsConfiguracionXML As New DataSet
    Dim bodyMensaje As String
    Dim contenidoLog As String = ""
    Dim ColeccionEmailsPara, ColeccionEmailsCC As System.Net.Mail.MailAddressCollection
    Dim sb As New StringBuilder
    Dim contadorErrores As Integer = 0
    Dim nombreArchivo, asunto As String
    Dim listadoMailCC As String
    Dim listadoMailPara As String = ""
    Dim fechaIniISO, fechaFinISO As String
    Dim totalRegistros As Integer = 0
    Dim msgError As String = ""

    'inicia la ejecucion
    Console.WriteLine("Iniciando tarea: " & NOMBRE_JOB)
    Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
    Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug.ToString)
    Try
      If EjecutarDiaMes() Then
        'crea el archivo en disco
        nombreArchivo = Archivo.CrearNombreUnicoArchivo(KEY)
        'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
        dsConfiguracionXML = Herramientas.LeerXML("configuracion.xml")

        'construye las fechas para el filtro
        fechaIniISO = "-1"
        fechaFinISO = "-1"

        'obtiene listado de planifcaciones
        ds = Sistema.ConsultaGenerica(fechaIniISO, fechaFinISO, "spu_Job_AsignarAlertaEnColaAtencion")
        totalRegistros = ds.Tables(0).Rows.Count

        If (totalRegistros > 0) Then
          msgError = GrabarAlertasEnColaAtencion(ds)

          If Not String.IsNullOrEmpty(msgError) Then
            'construye cuerpo del mail
            bodyMensaje = dsConfiguracionXML.Tables(eTablaConfiguracion.CuerpoEmail).Rows(0).Item("texto")
            bodyMensaje = bodyMensaje.Replace("{BR}", vbCrLf)
            bodyMensaje = bodyMensaje.Replace("{LISTADO}", msgError)

            'construye listado de email de destinos
            If Herramientas.MailModoDebug Then
              ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas())
              ColeccionEmailsCC = Nothing
            Else
              'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
              listadoMailPara = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailPara, "email")
              listadoMailCC = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailCC, "email")
              listadoMailCC &= IIf(String.IsNullOrEmpty(listadoMailCC), Herramientas.MailAdministrador(), ";" & Herramientas.MailAdministrador())
              'construye las colecciones
              ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
              ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailCC)
            End If
            'crea contenido para crear el log
            contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(msgError & "<br/><div>Enviado a: " & listadoMailPara & "</div>", NOMBRE_JOB)

            'envia el mail y retorna mensaje de exito(mensaje vacio) o de error
            asunto = dsConfiguracionXML.Tables(eTablaConfiguracion.AsuntoEmail).Rows(0).Item("texto")

            msgProceso = Mail.Enviar(Constantes.mailHost, Constantes.mailEmailEmisor, Constantes.mailPasswordEmailEmisor, Constantes.mailEmailNombre, ColeccionEmailsPara, asunto, bodyMensaje, False, ColeccionEmailsCC, False)
            If msgProceso <> "" Then
              contadorErrores = contadorErrores + 1
              contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & msgProceso, NOMBRE_JOB)
            End If
            'limpia variable para los nuevos email
            ColeccionEmailsPara = Nothing
            ColeccionEmailsCC = Nothing
          End If
        End If
      Else
        contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Tarea no realizada porque no estaba programada para ejecutarse el día de hoy", NOMBRE_JOB)
      End If
    Catch ex As Exception
      contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & ex.Message, NOMBRE_JOB)
      contadorErrores = 1
      totalRegistros = 1
    End Try
    Console.WriteLine("Fin tarea: " & NOMBRE_JOB)

    'crea log del proceso
    If Not String.IsNullOrEmpty(contenidoLog) Then
      Dim sufijoArchivo As String = IIf(contadorErrores > 0, "EJECUTADO_CON_ERRORES", "EJECUTADO_CON_EXITO")
      nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
      Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
    End If
  End Sub

End Module

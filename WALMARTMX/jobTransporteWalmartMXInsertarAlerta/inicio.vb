﻿Imports CapaNegocio
Imports System.Data
Imports System.Text
Imports System.Configuration

Module inicio

#Region "Configuracion Job"
  Const NOMBRE_JOB As String = "jobTransporteWalmartMXInsertarAlerta"
  Const KEY As String = "InsertarAlerta"
#End Region

  Sub Main()
    Dim msgProceso As String = ""
    'construye fecha inicio
    Dim ds As New DataSet
    Dim contenidoLog, nombreArchivo As String
    Dim contadorErrores, totalRegistros As Integer
    Dim ws As New wsAltotrackWalmartMX.Service
    Dim wsdAlerta As New wsAltotrackWalmartMX.wsdAlerta
    Dim LoginSoap As New wsAltotrackWalmartMX.Autentificacion

    Dim Id As Integer
    Dim Response As String

    Dim NroTransporte, IdEmbarque, OrigenCodigo, PatenteTracto, PatenteTrailer, RutTransportista, NombreTransportista, RutConductor, NombreConductor As String
    Dim FechaUltimaTransmision, DescripcionAlerta, FechaInicioAlerta, LatTrailer, LonTrailer, LatTracto, LonTracto, TipoAlerta, Permiso, Criticidad As String
    Dim TipoAlertaDescripcion, AlertaMapa, NombreZona, Velocidad, EstadoGPSTracto, EstadoGPSRampla, EstadoGPS, LocalDestino, TipoPunto, Temp1, Temp2 As String
    Dim DistanciaTT, TransportistaTrailer, CantidadSatelites, Ocurrencia, GrupoAlerta As String

    'inicia la ejecucion
    Console.WriteLine("Iniciando tarea: " & NOMBRE_JOB)
    Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
    Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug.ToString)

    Try

      '1) construir metodo para obterner alertas
      ds = Sistema.GetAlertas("Track_GetAlertasWebService_Temp")
      totalRegistros = ds.Tables(0).Rows.Count

      LoginSoap.Usuario = "QAnalytics"
      LoginSoap.Clave = "ALKTM78D258"

      If totalRegistros > 0 Then
        '2) recorrer datatset e insertar datos
        For Each dr As DataRow In ds.Tables(0).Rows
          Id = Convert.ToInt32(dr.Item("Id"))
          NroTransporte = dr.Item("NroTransporte")
          IdEmbarque = dr.Item("IdEmbarque")
          OrigenCodigo = dr.Item("OrigenCodigo")
          PatenteTracto = dr.Item("PatenteTracto")
          PatenteTrailer = dr.Item("PatenteTrailer")
          RutTransportista = dr.Item("RutTransportista")
          NombreTransportista = dr.Item("NombreTransportista")
          RutConductor = dr.Item("RutConductor")
          NombreConductor = dr.Item("NombreConductor")
          FechaUltimaTransmision = dr.Item("FechaUltimaTransmision")
          DescripcionAlerta = dr.Item("DescripcionAlerta")
          FechaInicioAlerta = dr.Item("FechaInicioAlerta")
          LatTrailer = dr.Item("LatTrailer")
          LonTrailer = dr.Item("LonTrailer")
          LatTracto = dr.Item("LatTracto")
          LonTracto = dr.Item("LonTracto")
          TipoAlerta = dr.Item("TipoAlerta")
          Permiso = dr.Item("Permiso")
          Criticidad = dr.Item("Criticidad")
          TipoAlertaDescripcion = dr.Item("TipoAlertaDescripcion")
          AlertaMapa = dr.Item("AlertaMapa")
          NombreZona = dr.Item("NombreZona")
          Velocidad = dr.Item("Velocidad")
          EstadoGPSTracto = dr.Item("EstadoGPSTracto")
          EstadoGPSRampla = dr.Item("EstadoGPSRampla")
          EstadoGPS = dr.Item("EstadoGPS")
          LocalDestino = dr.Item("LocalDestino")
          TipoPunto = dr.Item("TipoPunto")
          Temp1 = dr.Item("Temp1")
          Temp2 = dr.Item("Temp2")
          DistanciaTT = dr.Item("DistanciaTT")
          TransportistaTrailer = dr.Item("TransportistaTrailer")
          CantidadSatelites = dr.Item("CantidadSatelites")
          Ocurrencia = dr.Item("Ocurrencia")
          GrupoAlerta = "GrupoAlerta - " + dr.Item("GrupoAlerta")

          ws.AutentificacionValue = LoginSoap
          wsdAlerta = ws.Alertas(NroTransporte, IdEmbarque, OrigenCodigo, PatenteTracto, PatenteTrailer, RutTransportista, NombreTransportista, RutConductor, NombreConductor, _
                                 FechaUltimaTransmision, DescripcionAlerta, FechaInicioAlerta, LatTrailer, LonTrailer, LatTracto, LonTracto, TipoAlerta, Permiso, _
                                 Criticidad, TipoAlertaDescripcion, AlertaMapa, NombreZona, Velocidad, EstadoGPSTracto, EstadoGPSRampla, EstadoGPS, LocalDestino, _
                                 TipoPunto, Temp1, Temp2, DistanciaTT, TransportistaTrailer, CantidadSatelites, Ocurrencia, GrupoAlerta)

          '3) eliminar registro
          Response = Sistema.EliminarAlerta(Id)

        Next

        contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("El ingreso de alertas se realizó satisfactoriamente", NOMBRE_JOB)
      Else
        contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("No hay alertas por insertar", NOMBRE_JOB)
      End If

    Catch ex As Exception
      contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & ex.Message, NOMBRE_JOB)
      contadorErrores = 1
    End Try
    Console.WriteLine("Fin tarea: " & NOMBRE_JOB)

    'crea log del proceso
    If (contadorErrores > 0) Then
      Dim sufijoArchivo As String = "EJECUTADO_CON_ERRORES"
      nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
      Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
    End If

  End Sub

End Module

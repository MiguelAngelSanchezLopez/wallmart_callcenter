﻿Imports CapaNegocio
Imports System.Data
Imports System.Text
Imports System.Configuration
Imports Syncfusion.XlsIO

Module inicio

#Region "Configuracion Job"
  Const NOMBRE_JOB As String = "jobTransporteWalmartMXRevisarCargaPoolPlacas"
  Const KEY As String = "RevisarCargaPoolPlacas"
#End Region

#Region "Enum"
  Enum eTablaConfiguracion As Integer
    EmailPara = 0
    EmailCC = 1
    DiasEjecucion = 2
    AsuntoEmail = 3
    CuerpoEmail = 4
  End Enum

#End Region

#Region "Privado"
  ''' <summary>
  ''' determina si se puede ejecutar la consola en el dia actual
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks>Por VSR, 03/08/2009</remarks>
  Private Function EjecutarDiaMes() As Boolean
    Dim sePuedeEjecutar As Boolean = False
    Dim dsConfiguracionXML As New DataSet
    Dim diasEjecucion, incluirFinMes As String
    Dim diaMesActual As String = Herramientas.MyNow().Day
    Dim ultimoDiaMes As Date

    Try
      dsConfiguracionXML = Herramientas.LeerXML("configuracion.xml")
      diasEjecucion = dsConfiguracionXML.Tables(eTablaConfiguracion.DiasEjecucion).Rows(0).Item("diasEjecucion")
      incluirFinMes = dsConfiguracionXML.Tables(eTablaConfiguracion.DiasEjecucion).Rows(0).Item("incluirFinMes")

      If (InStr(diasEjecucion, "{-1}") > 0) Or (InStr(diasEjecucion, "{" & diaMesActual & "}") > 0) Then
        sePuedeEjecutar = True
      Else
        If incluirFinMes.Trim.ToUpper = "TRUE" Then
          ultimoDiaMes = Herramientas.ObtenerUltimoDiaMes(Herramientas.MyNow())
          If Day(ultimoDiaMes) = diaMesActual Then
            sePuedeEjecutar = True
          End If
        End If
      End If
    Catch ex As Exception
      sePuedeEjecutar = False
    End Try
    Return sePuedeEjecutar
  End Function

  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GenerarExcel(ByVal ds As DataSet, ByVal nombreArchivo As String, ByVal dsConfiguracion As DataSet) As Boolean
    Dim fueGenerado As Boolean = False
    Dim excelEngine As ExcelEngine = New ExcelEngine()
    Dim application As IApplication = excelEngine.Excel
    Dim workbook As IWorkbook

    Try
      application.UseNativeStorage = False

      Dim nombreHoja As String() = {"POOL PLACAS"}
      workbook = application.Workbooks.Create(nombreHoja)

      Dim sheet0 As IWorksheet = workbook.Worksheets(nombreHoja(0))
      GenerarHojaExcel(workbook, sheet0, ds.Tables(0))

      workbook.SaveAs(Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls")
      workbook.Close()
      fueGenerado = True

      Return fueGenerado
    Catch ex As Exception
      Throw New Exception(ex.Message)
    Finally
      excelEngine.Dispose()
    End Try
  End Function

  ''' <summary>
  ''' genera el archivo excel
  ''' </summary>
  ''' <param name="workbook"></param>
  ''' <param name="sheet"></param>
  ''' <param name="dt"></param>
  ''' <remarks></remarks>
  Private Sub GenerarHojaExcel(ByRef workbook As IWorkbook, ByRef sheet As IWorksheet, ByVal dt As DataTable)
    Dim fueGenerado As Boolean = False

    Try
      sheet.ImportDataTable(dt, True, 1, 1)
      With sheet.Range(1, 1, 1, dt.Columns.Count).CellStyle
        .Font.Bold = True
      End With

      sheet.UsedRange.AutofitColumns()
      sheet.IsGridLinesVisible = True
      fueGenerado = True
    Catch ex As Exception
      Throw New Exception(ex.Message)
    End Try
  End Sub

#End Region

  Sub Main()
    Dim msgProceso As String = ""
    'construye fecha inicio
    Dim ds As New DataSet
    Dim dsConfiguracionXML As New DataSet
    Dim body, bodyMensaje As String
    Dim contenidoLog As String = ""
    Dim ColeccionEmailsPara, ColeccionEmailsCC, ColeccionEmailsCCO As System.Net.Mail.MailAddressCollection
    Dim sb As New StringBuilder
    Dim contadorErrores As Integer = 0
    Dim nombreArchivo, asunto As String
    Dim listadoMailCC, listadoMailCCO As String
    Dim listadoMailPara As String = ""
    Dim fueGenerado As Boolean
    Dim fechaIniISO, fechaFinISO As String
    Dim arrayGroupByIdUsuario As New ArrayList
    Dim totalRegistros As Integer
    Dim idUsuarioCedis, listadoCedisAsociados, emailEncargado, cedis, determinante, fechaCarga, cuerpoCorreo As String
    Dim arrCedisAsociados As List(Of String)
    Dim dv, dvUsuario As New DataView
    Dim dsFiltrado, dsFiltradoUsuario As New DataSet
    Dim dr As DataRow
    Dim T00_ListadoCarga As Integer = 0
    Dim T01_ListadoEncargadosCedis As Integer = 1
    Dim arrBodyMensaje As List(Of String)
    Dim fechaActual As String = Replace(Herramientas.MyNow().ToString("dd/MM/yyyy"), "-", "/")

    'inicia la ejecucion
    Console.WriteLine("Iniciando tarea: " & NOMBRE_JOB)
    Console.WriteLine("Ambiente: " & Herramientas.ObtenerAmbienteEjecucion)
    Console.WriteLine("Modo Debug: " & Herramientas.MailModoDebug.ToString)

    Try
      If EjecutarDiaMes() Then
        'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
        dsConfiguracionXML = Herramientas.LeerXML("configuracion.xml")

        'construye las fechas para el filtro
        fechaIniISO = "-1"
        fechaFinISO = "-1"

        'obtiene listado de imputados
        ds = Sistema.ConsultaGenerica(fechaIniISO, fechaFinISO, "spu_Job_RevisarCargaPoolPlacas")
        totalRegistros = ds.Tables(T00_ListadoCarga).Rows.Count

        If (totalRegistros > 0) Then
          'obtiene idUsuarios unicos
          arrayGroupByIdUsuario = Herramientas.ObtenerArrayGroupByDeDataTable(ds.Tables(T01_ListadoEncargadosCedis), "IdUsuario")

          'recorre los usuarios para obtener los cedis asignados y filtrar los datos de la carga para enviarlo por email
          For Each idUsuarioCedis In arrayGroupByIdUsuario
            'crea el archivo en disco
            nombreArchivo = Archivo.CrearNombreUnicoArchivo(KEY)
            arrCedisAsociados = New List(Of String)
            arrBodyMensaje = New List(Of String)

            Dim query = From row As DataRow In ds.Tables(T01_ListadoEncargadosCedis).Rows Where row.Item("IdUsuario") = idUsuarioCedis
            For Each row In query
              arrCedisAsociados.Add(row.Item("CodigoInterno"))
            Next
            listadoCedisAsociados = String.Join(",", arrCedisAsociados.ToArray())

            'filtra la carga de placas
            dv = ds.Tables(T00_ListadoCarga).DefaultView
            dv.RowFilter = "DETERMINANTE IN (" & listadoCedisAsociados & ")"
            dsFiltrado = Herramientas.CrearDataSetDesdeDataViewConRowFilter(dv)
            dv.RowFilter = ""

            'filtra los datos del usuario
            dvUsuario = ds.Tables(T01_ListadoEncargadosCedis).DefaultView
            dvUsuario.RowFilter = "IdUsuario = " & idUsuarioCedis
            dsFiltradoUsuario = Herramientas.CrearDataSetDesdeDataViewConRowFilter(dvUsuario)
            dvUsuario.RowFilter = ""
            emailEncargado = dsFiltradoUsuario.Tables(0).Rows(0).Item("Email")

            'construye el body del mensaje
            For Each dr In dsFiltrado.Tables(0).Rows
              cuerpoCorreo = "- El pool para el cedis de {CEDIS} no fue cargado el día de hoy, esto nos obliga a trabajar con el pool cargado el {FECHA_CARGA}"
              cedis = dr.Item("CEDIS")
              determinante = dr.Item("DETERMINANTE")
              fechaCarga = dr.Item("FECHA CARGA")

              cuerpoCorreo = cuerpoCorreo.Replace("{CEDIS}", cedis & " - " & determinante)
              cuerpoCorreo = cuerpoCorreo.Replace("{FECHA_CARGA}", fechaCarga)

              If (Not arrBodyMensaje.Contains(cuerpoCorreo)) Then
                arrBodyMensaje.Add(cuerpoCorreo)
              End If
            Next
            cuerpoCorreo = String.Join("<br /><br />", arrBodyMensaje.ToArray())

            'envia informacion
            fueGenerado = GenerarExcel(dsFiltrado, nombreArchivo, dsConfiguracionXML)
            If fueGenerado Then
              'construye cuerpo del mail
              bodyMensaje = dsConfiguracionXML.Tables(eTablaConfiguracion.CuerpoEmail).Rows(0).Item("texto")
              bodyMensaje = bodyMensaje.Replace("{BR}", "<br />")
              bodyMensaje = bodyMensaje.Replace("{LISTADO}", cuerpoCorreo)
              bodyMensaje = bodyMensaje.Replace("{FECHA_ACTUAL}", fechaActual)

              'constuye contenido del excel y del log
              body = Herramientas.ObtenerTablaHTMLDesdeDataSet(dsFiltrado, 0)

              'construye listado de email de destinos
              If Herramientas.MailModoDebug Then
                ColeccionEmailsPara = Mail.ConstruirCollectionMail(Herramientas.MailPruebas)
                ColeccionEmailsCC = Nothing
                ColeccionEmailsCCO = Nothing
              Else
                'lee el archivo de configuracion para obtener listado de mail si existen y construye las lista de email
                listadoMailPara = emailEncargado
                listadoMailPara &= IIf(String.IsNullOrEmpty(listadoMailPara), _
                                       Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailPara, "email"), _
                                       ";" & Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailPara, "email"))
                listadoMailCC = Herramientas.ObtenerListadoMailDesdeDataSet(dsConfiguracionXML, eTablaConfiguracion.EmailCC, "emailCC")
                listadoMailCCO = Herramientas.MailAdministrador()

                'construye las colecciones
                ColeccionEmailsPara = Mail.ConstruirCollectionMail(listadoMailPara)
                ColeccionEmailsCC = Mail.ConstruirCollectionMail(listadoMailCC)
                ColeccionEmailsCCO = Mail.ConstruirCollectionMail(listadoMailCCO)
              End If
              'crea contenido para crear el log
              contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML(body & "<br/><div>Enviado a: " & listadoMailPara & "</div>", NOMBRE_JOB)
              Dim path As String = Constantes.logPathDirectorioConsolas & "\" & NOMBRE_JOB & "\" & nombreArchivo & ".xls"
              'envia el mail y retorna mensaje de exito(mensaje vacio) o de error
              asunto = dsConfiguracionXML.Tables(eTablaConfiguracion.AsuntoEmail).Rows(0).Item("texto")
              asunto = asunto.Replace("{FECHA_ACTUAL}", fechaActual)

              msgProceso = Mail.Enviar(Constantes.mailHost, Constantes.mailEmailEmisor, Constantes.mailPasswordEmailEmisor, Constantes.mailEmailNombre, ColeccionEmailsPara, asunto, bodyMensaje, True, ColeccionEmailsCC, True, path, "", ColeccionEmailsCCO)
              If msgProceso <> "" Then
                contadorErrores = contadorErrores + 1
                contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & msgProceso, NOMBRE_JOB)
              End If
              'limpia variable para los nuevos email
              ColeccionEmailsPara = Nothing
              ColeccionEmailsCC = Nothing
            Else
              contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("No hay registros", NOMBRE_JOB)
            End If
          Next '<-- For Each idUsuarioCedis In arrayGroupByIdUsuario
        Else
          contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("No hay registros", NOMBRE_JOB)
        End If
      Else
        contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Tarea no realizada porque no estaba programada para ejecutarse el día de hoy", NOMBRE_JOB)
      End If
    Catch ex As Exception
      contenidoLog = Herramientas.ObtenerContenidoLogConTablaHTML("Ocurrió el siguiente error:<br>" & ex.Message, NOMBRE_JOB)
      contadorErrores = 1
    End Try
    Console.WriteLine("Fin tarea: " & NOMBRE_JOB)

    'crea log del proceso
    Dim sufijoArchivo As String = IIf(contadorErrores > 0, "EJECUTADO_CON_ERRORES", "EJECUTADO_CON_EXITO")
    nombreArchivo = Archivo.CrearNombreUnicoArchivo(sufijoArchivo)
    Archivo.CrearArchivoTextoEnDisco(contenidoLog, Constantes.logPathDirectorioConsolas, NOMBRE_JOB, nombreArchivo, "html")
  End Sub

End Module
